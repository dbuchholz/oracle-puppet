30-mar-2017 - dbuchholz

This is the full set of Oracle puppet classes and a subset of the Eagle classes used to isntall
Oracle 12c & ASM databases in Eagle Access.

Start with eagle::oracle12c_base.pp

The rest of the classes are included from there. A new host can be setup with that single class.

