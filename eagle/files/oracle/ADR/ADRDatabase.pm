package ADRDatabase;


# Provide functions to create RestoreDataTempFiles_<targetSid>.rcv

# Funtions:
# preReqDatabaseEntries - makes sure entries are in the databases table 
# preReqPaceVersions - makes sure pace versions are the same
# preReqStarVersions - makes sure pace versions are the same
# preReqOracleVersions - makes sure oracle versions are the same
# preReqASMBkpFile -  make sure <backupLocation>/asm_metadata_<backupSet> exists
# shutDownDatabase - Shuts down the target database
# startDatabaseRman - starts up the database via rman
# startDatabaseSql - starts up the database via sql
# deleteDatabase - deletes the database, leaves it down
# deleteDatabaseLUN2ASM - deletes the database, leaves it down for LUN2ASM refresh type
# autoExtend - sets autoextend for file_name from dba_datafiles based on tablespace size
# compileDatabase - compiles the database 
# setFlashBack - turns flashback on or off
# setOpenResetLogs - open database is reset log mode
# setOpenResetLogsUpgrade - Open and Resetlogs in upgrade mode
# setBlockChangeTracking - sets block change tracking on or off
# setSupplementalLog - sets the supplemental log
# createSpFile - creates the spfile
# setGlobalName - sets the globalname
# changeDatabaseId - changes the database id to the target via nid
# unregisterOldDatabase - unregisters the old database with the rman catalog 
# runSql - runs a sql statement
# all others should be considered private


use strict;
use POSIX qw(WNOHANG);
use File::Path;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;

##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      targetSid - target sid to create the RestoreDataTempFiles for
#      clientDbh - client database handle
#
##############################################################################

sub new
{
    my ($class, $targetSid, $sourceSid, $isASM, $bbDbh, $clientDbh, $clientDbPW, $sqlScriptsDir, $refreshQueueInst) = @_;

    my $self = {
        workDir => "$ENV{ADR_LOCATION}/$refreshQueueInst" . "_workDir",
        targetSid => $targetSid,
        sourceSid => $sourceSid,
        bbDbh => $bbDbh,
        clientDbh => $clientDbh,
        clientDbPW => $clientDbPW,
        sqlScriptsDir => "$ENV{ADR_LOCATION}/$sqlScriptsDir",
        refreshQueueInst => $refreshQueueInst,
        hidePW => "********",
        sourceASMBackupFile => undef,
        isASM => $isASM
    };

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: preReqDatabaseEntries
#      makes sure entries are in the databases table
#      
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqDatabaseEntries
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqDatabaseEntries\n";
    $$logStr .= "Getting database entries from the database\n";

    my $sqlStatement=<<EOSql;
SELECT sid 
  FROM databases
 WHERE sid IN (\'$self->{sourceSid}\', \'$self->{targetSid}\')
   AND dataguard = 'N'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting database entries: $DBI::errstr\n";
        return 0;
    }

    # should only be two
    my $sid1 = $sth->fetchrow_array();
    my $sid2 = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 2 row while getting database entries, but got more\n";
        return 0;
    }

    my $foundSourceSid = 0;
    my $foundTargetSid = 0;

    if (defined $sid1 && $sid1 =~ /$self->{sourceSid}/ ||
        defined $sid2 && $sid2 =~ /$self->{sourceSid}/)
    {
        $foundSourceSid = 1;        
    }

    if (defined $sid1 && $sid1 =~ /$self->{targetSid}/ ||
           defined $sid2 && $sid2 =~ /$self->{targetSid}/)
    {
        $foundTargetSid = 1;        
    }

    my $retVal = 1;
    if (!$foundSourceSid)
    {
        $$logStr .= "Source $self->{sourceSid} is missing from the databases table\n";
        $retVal = 0;
    }

    if (!$foundTargetSid)
    {
        $$logStr .= "Target $self->{targetSid} is missing from the databases table\n";
        $retVal = 0;
    }

    $$logStr .= "Source and target are both in the databases table\n" if ($retVal);
    return $retVal;
}

##############################################################################
#
#  Function: preReqEnginesDown
#      makes sure entries are in the databases table
#      
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqEnginesDown
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqEnginesDown\n";
    $$logStr .= "Getting connection counts for ESTAR and PACE_MASTERDBO from the database\n";

    my $sqlStatement=<<EOSql;
SELECT username, COUNT(*)
  FROM v\$session 
 WHERE username IN (\'ESTAR\', \'PACE_MASTERDBO\')
GROUP BY username
HAVING COUNT(*) > 10
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting session entries: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $user, \my $connCount);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding users and connection counts: $DBI::errstr\n";
        return 0;
    }

    my $retVal = 1;
    while ($sth->fetch())
    {
        $$logStr .= "There are $connCount connections for $user\n";
        $retVal = 0;
    }

    if (!$retVal)
    {
        $$logStr .= "ERROR: Engines are running\n";
    }
    else
    {
        $$logStr .= "There are no engines are running\n";
    }

    return $retVal;
}

# TODO, use helper function for code reuse for following 3
##############################################################################
#
#  Function: preReqPaceVersions
#      makes sure pace versions are the same
#      
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqPaceVersions
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqPaceVersions\n";
    $$logStr .= "Getting source and target pace versions from the Big Brother database to check if versions are the same\n";

    my $sqlStatement=<<EOSql;
SELECT sid, 
       pace_version
  FROM databases 
 WHERE sid IN (\'$self->{sourceSid}\', \'$self->{targetSid}\')
   AND dataguard = 'N'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting pace versions: $DBI::errstr\n";
        return 0;
    }

    # should only be two
    my ($sid1, $paceVersion1) = $sth->fetchrow_array();
    my ($sid2, $paceVersion2) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 2 row while getting pace versions, but got more\n";
        return 0;
    }

    my $sourcePaceVersion = '';
    my $targetPaceVersion = '';

    if ($sid1 =~ /$self->{sourceSid}/)
    {
        $sourcePaceVersion = $paceVersion1; 
        $targetPaceVersion = $paceVersion2;
    }
    else 
    {
        $sourcePaceVersion = $paceVersion2;
        $targetPaceVersion = $paceVersion1; 
    }

    my $bothDefined = 1;
    if (!defined $sourcePaceVersion)
    {
        $$logStr .= "The source pace version is not in the database\n";
        $bothDefined = 0;
    }

    if (!defined $targetPaceVersion)
    {
        $$logStr .= "The target pace version is not in the database\n";
        $bothDefined = 0;
    }
    return 0 if (!$bothDefined);
 
    my $retVal;
    if ($sourcePaceVersion ne $targetPaceVersion)
    {
        $$logStr .=  "Pace Versions are not the same.  Source is $sourcePaceVersion and target is $targetPaceVersion\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .=  "Pace Versions are the same: $targetPaceVersion\n"; 
        $retVal = 1;
        
    }

    return $retVal;    
}

 
##############################################################################
#
#  Function: preReqStarVersions
#      makes sure star versions are the same
#      
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqStarVersions
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqStarVersions\n";
    $$logStr .= "Getting source and target star versions the Big Brother database to check if versions are the same\n";

    my $sqlStatement=<<EOSql;
SELECT sid, 
       star_version
  FROM databases 
 WHERE sid IN (\'$self->{sourceSid}\', \'$self->{targetSid}\')
   AND dataguard = 'N'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting star versions: $DBI::errstr\n";
        return 0;
    }

    # should only be two
    my ($sid1, $starVersion1) = $sth->fetchrow_array();
    my ($sid2, $starVersion2) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 2 row while getting star versions, but got more\n";
        return 0;
    }

    my $sourceStarVersion = '';
    my $targetStarVersion = '';

    if ($sid1 =~ /$self->{sourceSid}/)
    {
        $sourceStarVersion = $starVersion1; 
        $targetStarVersion = $starVersion2;
    }
    else 
    {
        $sourceStarVersion = $starVersion2; 
        $targetStarVersion = $starVersion1; 
    }

    my $bothDefined = 1;
    if (!defined $sourceStarVersion)
    {
        $$logStr .= "The source star version is not in the database\n";
        $bothDefined = 0;
    }

    if (!defined $targetStarVersion)
    {
        $$logStr .= "The target star version is not in the database\n";
        $bothDefined = 0;
    }
    return 0 if (!$bothDefined);
 
    my $retVal;
    if ($sourceStarVersion ne $targetStarVersion)
    {
        $$logStr .=  "Star Versions are not the same.  Source is $sourceStarVersion and target is $targetStarVersion\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .=  "Star Versions are the same: $targetStarVersion\n"; 
        $retVal = 1;
        
    }

    return $retVal;    
}

##############################################################################
#
#  Function: preReqOracleVersions
#      makes sure oracle versions are the same
#      
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqOracleVersions
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqOracleVersions\n";
    $$logStr .= "Getting source and target oracle versions from the Big Brother database to check if versions are the same\n";

    my $sqlStatement=<<EOSql;
SELECT sid, 
       oracle_version
  FROM databases 
 WHERE sid IN (\'$self->{sourceSid}\', \'$self->{targetSid}\')
   AND dataguard = 'N'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting oracle versions: $DBI::errstr\n";
        return 0;
    }

    # should only be two
    my ($sid1, $oracleVersion1) = $sth->fetchrow_array();
    my ($sid2, $oracleVersion2) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 2 row while getting oracle versions, but got more\n";
        return 0;
    }

    my $sourceOracleVersion = '';
    my $targetOracleVersion = '';

    if ($sid1 =~ /$self->{sourceSid}/)
    {
        $sourceOracleVersion = $oracleVersion1; 
        $targetOracleVersion = $oracleVersion2;
    }
    else 
    {
        $sourceOracleVersion = $oracleVersion2;
        $targetOracleVersion = $oracleVersion1; 
    }

    my $bothDefined = 1;
    if (!defined $sourceOracleVersion)
    {
        $$logStr .= "The source oracle version is not in the database\n";
        $bothDefined = 0;
    }

    if (!defined $targetOracleVersion)
    {
        $$logStr .= "The target oracle version is not in the database\n";
        $bothDefined = 0;
    }
    return 0 if (!$bothDefined);
 
    my $retVal;
    if ($sourceOracleVersion ne $targetOracleVersion)
    {
        $$logStr .=  "Oracle Versions are not the same.  Source is $sourceOracleVersion and target is $targetOracleVersion\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .=  "Oracle Versions are the same: $targetOracleVersion\n"; 
        $retVal = 1;
        
    }

    return $retVal;    
}

##############################################################################
#
#  Function: preReqASMBkpFile
#      make sure <backupLocation>/asm_metadata_<backupSet> exists
#  Args:
#
# Pre-requisites:
#     Only for CMP new build
#
##############################################################################
sub preReqASMBkpFile
{
    my ($self, $logStr, $backupLocation, $backupSet) = @_;

    $$logStr .= "In preReqASMBkpFile\n";

    if (!$self->{isASM})
    {
        $$logStr .= "The source database is not ASM.  Not checking file\n";
        return 1;
    }

    $self->{sourceASMBackupFile} = "$backupLocation/asm_metadata_$backupSet";

    $$logStr .= "Checking ASM backup file $self->{sourceASMBackupFile}\n";

    if (!-e $self->{sourceASMBackupFile})
    {
        $$logStr .= "$backupLocation/asm_metadata_" . $backupSet . " does not exist\n";
        return 0;
    }

    $$logStr .= "Found ASM backup file\n"; 
    return 1;
}

##############################################################################
#
#  Function: shutdownDatabase
#      shut down the database.
#
#  Args:
#    type: I - immediate, A - abort
#
# Pre-requisites:
#
##############################################################################

sub shutdownDatabase
{
    my ($self, $logStr, $type) = @_;

    $$logStr .= "In shutdownDatabase for $type\n";
 
    my $shutDown = '';
    if ($type eq 'I')
    {
        $shutDown = 'immediate';
    } 
    elsif ($type eq 'A')
    {
        $shutDown = 'abort';
    }
    else
    {
        $$logStr .= "Incorrect type, $type.  Type must be I (immediate) or A (abort)\n";
        return 0;
    }
 
    my $dbRunning = isDatabaseRunning($self, $logStr);
    if (!defined $dbRunning)
    {
        #logStr is already filled out
        return 0;
    }

    if ($dbRunning)
    {
        $$logStr .= "Database was running, so will shutdown\n";
        # TODO. will dbi work?  for now use system

        # disconnect first, will reconnect in the main adr object
        $$logStr .= "Disconnect from the client database\n";
        $self->{clientDbh}->disconnect() if (defined $self->{clientDbh});  

        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRShutdownDBSql_" . getDateTimeStr() . ".log";
        if (!runSqlAsSysdba($logStr, "SHUTDOWN $shutDown", $logFile))
        {
            #logStr will already be set
            return 0;
        }

        $dbRunning = isDatabaseRunning($self, $logStr);
        if (!defined $dbRunning)
        {
            #logStr is already filled out
            return 0;
        }
      
        if ($dbRunning)
        {
            $$logStr .= "Failed shutting database down.\n";
            return 0;
        } 
        else
        {
            $$logStr .= "Shutdown successful\n";
        }

    }
    else
    {
        $$logStr .= "Database was not running, so no need to shutdown\n";
    }
    
    return 1;
}


##############################################################################
#
#  Function: shutdownASMDatabase
#      shut down the database.
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub shutdownASMDatabase
{
    my ($self, $logStr) = @_;

    $$logStr .= "In shutdownASMDatabase\n";
 
    my $dbRunning = isDatabaseRunning($self, $logStr);
    if (!defined $dbRunning)
    {
        #logStr is already filled out
        return 0;
    }

    if ($dbRunning)
    {
        $$logStr .= "Database was running, so will shutdown\n";

        $$logStr .= "Disconnect from the client database\n";
        $self->{clientDbh}->disconnect() if (defined $self->{clientDbh});  

        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRASMExclRestrictDBSql_" . getDateTimeStr() . ".log";

        my $tmpSqlFile = "$self->{workDir}/tmp_$$.sql";
        if (!open (TMP_FH, "> $tmpSqlFile"))
        {
            $$logStr .=  "Cannot open $tmpSqlFile: $!\n";
            return 0;
        }

        $$logStr .= "About to run shutdown immediate and startup mount exclusive restrict in a temporary file\n";

        print TMP_FH "SHUTDOWN IMMEDIATE\nSTARTUP MOUNT EXCLUSIVE RESTRICT\n";
        close TMP_FH; 
   
        if (!runSqlAsSysdba($logStr, "\@$tmpSqlFile", $logFile))
        {
            #logStr will already be set
            return 0;
        }

        unlink $tmpSqlFile;

        my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
        $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRASMDropDBRman_" . getDateTimeStr() . ".log";

        my $cmd = "$runRman \"target /\" \"drop database noprompt\;\" $logFile";

        $$logStr .= "About to run $cmd\n";

        if (!open (RMAN_PIPE, "$cmd |"))
        {
            $$logStr .= "Cannot execute command.  Error is $!\n";
            return 0;
        }
        
        my @outp = <RMAN_PIPE>;
        close RMAN_PIPE;

        if (!($? >> 8))
        {
            $$logStr .= "$cmd failed.  Error is $!\n";
            return 0;
        }
    
        if (!checkLogForErrors($logStr, $logFile, 'rman'))
        {
            #$$logStr will be set
            return 0;
        }

        $$logStr .= "rman successful\n";

        $dbRunning = isDatabaseRunning($self, $logStr);
        if (!defined $dbRunning)
        {
            ##logStr is already filled out
            return 0;
        }
        
        if ($dbRunning)
        {
            $$logStr .= "Failed shutting database down.\n";
            return 0;
        } 
        else
        {
            $$logStr .= "Shutdown successful\n";
        }

    } else
    {
        $$logStr .= "ERROR: Database was not running.\n";
        return 0;
    }
     
    $$logStr .= "Shutdown successful.\n";
    return 1;
}

##############################################################################
##
##  Function: shutdownDatabaseLUN2ASM
##      shut down the database.
##
##  Args:
##
## Pre-requisites:
##
###############################################################################

sub shutdownDatabaseLUN2ASM
{
    my ($self, $logStr, $createPfileFromSpfile) = @_;

    $$logStr .= "In shutdownDatabaseLUN2ASM\n";

    my $dbRunning = isDatabaseRunning($self, $logStr);
    if (!defined $dbRunning)
    {
        #logStr is already filled out
        return 0;
    }

    if ($dbRunning)
    {
        $$logStr .= "Database was running, so will shutdown\n";

        $$logStr .= "Disconnect from the client database\n";
        $self->{clientDbh}->disconnect() if (defined $self->{clientDbh});

        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRLUN2ASMExclRestrictDBSql_" . getDateTimeStr() . ".log";

        my $tmpSqlFile = "$self->{workDir}/tmp_$$.sql";
        if (!open (TMP_FH, "> $tmpSqlFile"))
        {
            $$logStr .=  "Cannot open $tmpSqlFile: $!\n";
            return 0;
        }

        $$logStr .= "About to run shutdown immediate and startup mount exclusive restrict in a temporary file\n";

        print TMP_FH "SHUTDOWN IMMEDIATE\nSTARTUP MOUNT EXCLUSIVE RESTRICT\n";
        close TMP_FH;

        if (!runSqlAsSysdba($logStr, "\@$tmpSqlFile", $logFile))
        {
            #logStr will already be set
            return 0;
        }

        unlink $tmpSqlFile;

        my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
        my $cmd;
        $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRASMDropDBRman_" . getDateTimeStr() . ".log";
#        my $cmd = "$runRman \"target /\" \"crosscheck archivelog all\;delete noprompt archivelog all\;drop database noprompt\;\" $logFile";
        if ($createPfileFromSpfile=~ /createPfileFromSpfile/i)
        {
        
            $cmd = "$runRman \"target /\" \"sql 'create pfile from spfile'\;crosscheck archivelog all\;drop database noprompt\;\" $logFile";
        }
        elsif(!defined $createPfileFromSpfile)
        {
            $cmd = "$runRman \"target /\" \"crosscheck archivelog all\;drop database noprompt\;\" $logFile";
        }
        else
        {
            $$logStr .= "Provided argument to function, $createPfileFromSpfile, was not recognised as a valid one\n";
            return 0;
        }
        $$logStr .= "About to run $cmd\n";

        if (!open (RMAN_PIPE, "$cmd |"))
        {
            $$logStr .= "Cannot execute command.  Error is $!\n";
            return 0;
        }

        my @outp = <RMAN_PIPE>;
        close RMAN_PIPE;

        if (!($? >> 8))
        {
            $$logStr .= "$cmd failed.  Error is $!\n";
            return 0;
        }

        if (!checkLogForErrors($logStr, $logFile, 'rman'))
        {
            #$$logStr will be set
            return 0;
        }

        $$logStr .= "rman successful\n";

        $dbRunning = isDatabaseRunning($self, $logStr);
        if (!defined $dbRunning)
        {
            ##logStr is already filled out
            return 0;
        }

        if ($dbRunning)
        {
            $$logStr .= "Failed shutting database down.\n";
            return 0;
        }
        else
        {
            $$logStr .= "Shutdown successful\n";
        }

    } else
    {
        $$logStr .= "ERROR: Database was not running.\n";
        return 0;
    }

    $$logStr .= "Shutdown successful.\n";
    return 1;
}


##############################################################################
#
#  Function: startDatabaseRman
#      Starts up the database via rman
#
#  Args:
#
# Pre-requisites:
#
#
##############################################################################

# TODO, shutdown if it is running?  For now just exit with error
sub startDatabaseRman
{

    my ($self, $logStr, $type) = @_;

    $$logStr .= "In startDatabaseRman\n";

    my $dbRunning = isDatabaseRunning($self, $logStr);
    if (!defined $dbRunning)
    {
        #logStr is already filled out
        return 0;
    }

    my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRStartDBRman_" . getDateTimeStr() . ".log";

    my $pfile = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

    if (!-e $pfile)
    {
            $$logStr .= "Can't find $pfile.  Either createInitOraFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
    }

    my $rmanCmd = "\"startup nomount pfile='$pfile'\""; 
    my $cmd = "$runRman \"target=/ nocatalog\" $rmanCmd $logFile";

    $$logStr .= "About to run $cmd\n";

    if (!open (RMAN_PIPE, "$cmd |"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return 0;
    }
    
    my @outp = <RMAN_PIPE>;

    close RMAN_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'rman'))
    {
        #$$logStr will be set
        return 0;
    }

    $$logStr .= "rman successful\n";
    return 1;

}

##############################################################################
#
#  Function: startDatabaseSql
#      Starts up the database via sql
#
#  Args:
#    type: M - mount, N - nomount, B - blank
#    usePfile: 1 if startup should be using pfile, undefined or 0 if should not be using pfile
#
# Pre-requisites:
#
##############################################################################

# TODO, shutdown if it is running?  For now just exit with error
sub startDatabaseSql
{
    my ($self, $logStr, $type, $usePfile) = @_;

    $usePfile = 0 if (!defined $usePfile);
    $$logStr .= "In startDatabaseSql for type: $type usePfile: $usePfile\n";

    if ($usePfile && $type eq 'B')
    {
        $$logStr .= "Cannot use pfile for type $type.  Can only use pfile for types 'M' - mount or 'N' - nomount" ;
        return 0;
    }
 
    my $startUp = '';
    if ($type eq 'M')
    {
        $startUp = 'mount';
    } 
    elsif ($type eq 'N')
    {
        $startUp = 'nomount';
    }
    elsif ($type ne 'B')
    {
        $$logStr .= "Incorrect type, $type.  Type must be M (mount), N (nomount) or B (blank)\n";
        return 0;
    }

    my $pfileStr = "";

    if ($usePfile)
    {
        my $pfile = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

        if (!-e $pfile)
        {
                $$logStr .= "Can't find $pfile.  Either createInitOraFile has not ";
                $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
                return 0;
        }

        $pfileStr = "pfile='$pfile'";
    }
 
    my $dbRunning = isDatabaseRunning($self, $logStr);
    if (!defined $dbRunning)
    {
        #logStr is already filled out
        return 0;
    }

    if ($dbRunning)
    {
        $$logStr .= "ERROR: Database is already running.\n";
        return 0;
    }
    else
    {
        # TODO. will dbi work?  for now use system


        # must use 'as sysdba' to do the shutdown
        # will reconnect to in the main object
        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRStartDBSql_" . getDateTimeStr() . ".log";
        if (!runSqlAsSysdba($logStr, "STARTUP $startUp $pfileStr", $logFile))
        {
            #logStr will already be set
            return 0;
        }

        $dbRunning = isDatabaseRunning($self, $logStr);
        if (!defined $dbRunning)
        {
            #logStr is already filled out
            return 0;
        }
      
        if (!$dbRunning)
        {
            $$logStr .= "Failed starting database.\n";
            return 0;
        } 
        else
        {
            $$logStr .= "Startup successful\n";
        }
    }
    
    return 1;
}

##############################################################################
#
#  Function: deleteDatabase
#      shuts down the database and deletes database files
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub deleteDatabase
{
    my ($self, $logStr, $threadCheckSeconds) = @_;

    $$logStr .= "In deleteDatabase\n";
  
    my $retVal = 0;
    if ($self->{isASM})
    {
        $retVal = deleteASMDatabase($self, $logStr);
    }
    else
    {
        $retVal = deleteStandardDatabase($self, $logStr, $threadCheckSeconds);
    }

    $$logStr .= "Finished deleting\n" if ($retVal);
    return $retVal;
}

sub deleteASMDatabase
{
    my ($self, $logStr) = @_;

    $$logStr .= "In deleteASMDatabase\n";

    $$logStr .= "Getting list of directories to clearout.\n";
    # get list of files to delete
    # get list of directories to cleanout
    my $sqlStatement=<<EOSql;
SELECT DISTINCT value
  FROM v\$parameter
 WHERE name IN ('user_dump_dest','core_dump_dest','background_dump_dest','audit_file_dest')
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting directories to clean out: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $name);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding names: $DBI::errstr\n";
        return 0;
    }

    my @cleanoutDirs;
    while ($sth->fetch())
    {
        $name =~ s/^LOCATION=//;
        push (@cleanoutDirs, $name);
    }
    $sth->finish();

    # shutdown abort the database
    if (!shutdownASMDatabase($self, $logStr))
    {
        # logStr is alreay filled out
        return 0;
    }

    # run rman command


    # clear out directories
    $$logStr .= "About to clean out directories\n";
    foreach my $dir (@cleanoutDirs)
    {
        $$logStr .= "About to clean out $dir\n";
        rmtree($dir, {keep_root => 1, error => \my $err});
        if (defined @$err)
        {
            $$logStr .= "Failed while cleaning out dirs: ";
            foreach my $diag (@$err)
            {
                my ($file, $message) = %$diag;
                if ($file eq '')
                {
                    $$logStr .= $message . "\n";
                }
                else
                {
                    $$logStr .= "file: $file - $message\n";
                }
            }
            return 0;
        }

        # there is a bug in older versions of perl where the base directory is
        # deleted.  Handle this by recreating the directory if it is gone.
        if (!-d $dir)
        {
            $$logStr .= "$dir does not exist, creating it\n";

            eval {mkpath($dir)};
            if ($@)
            {
                $$logStr .= "Cannot make directory $dir: $@\n";
                return 0;
            }
        }
    }
    $$logStr .= "Clean out successful\n";

    return 1;
}

##############################################################################
##
##  Function: deleteDatabaseLUN2ASM
##      shuts down the database and deletes database files for LUN-to-ASM refresh type
##
##  Args:
##
## Pre-requisites:
##
###############################################################################
#
sub deleteDatabaseLUN2ASM
{
    my ($self, $logStr, $createPfileFromSpfile) = @_;

    $$logStr .= "In deleteDatabaseLUN2ASM\n";

    $$logStr .= "Getting list of directories to clearout.\n";
    # get list of files to delete
    # get list of directories to cleanout
    my $sqlStatement=<<EOSql;
SELECT DISTINCT value
  FROM v\$parameter
 WHERE name IN ('user_dump_dest','core_dump_dest','background_dump_dest','audit_file_dest')
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting directories to clean out: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $name);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding names: $DBI::errstr\n";
        return 0;
    }

    my @cleanoutDirs;
    while ($sth->fetch())
    {
        $name =~ s/^LOCATION=//;
        push (@cleanoutDirs, $name);
    }
    $sth->finish();

    # shutdown abort the database
    if (!shutdownDatabaseLUN2ASM($self, $logStr, $createPfileFromSpfile))
    {
        # logStr is alreay filled out
        return 0;
    }

    # run rman command


    # clear out directories
    $$logStr .= "About to clean out directories\n";
    foreach my $dir (@cleanoutDirs)
    {
        $$logStr .= "About to clean out $dir\n";
        rmtree($dir, {keep_root => 1, error => \my $err});
        if (defined @$err)
        {
            $$logStr .= "Failed while cleaning out dirs: ";
            foreach my $diag (@$err)
            {
                my ($file, $message) = %$diag;
                if ($file eq '')
                {
                    $$logStr .= $message . "\n";
                }
                else
                {
                    $$logStr .= "file: $file - $message\n";
                }
            }
            return 0;
        }

        # there is a bug in older versions of perl where the base directory is
        # deleted.  Handle this by recreating the directory if it is gone.
        if (!-d $dir)
        {
            $$logStr .= "$dir does not exist, creating it\n";

            eval {mkpath($dir)};
            if ($@)
            {
                $$logStr .= "Cannot make directory $dir: $@\n";
                return 0;
            }
        }
    }
    $$logStr .= "Clean out successful\n";

    return 1;
 
}

sub deleteStandardDatabase
{
    my ($self, $logStr, $threadCheckSeconds) = @_;

    $$logStr .= "In deleteStandardDatabase\n";

    $$logStr .= "Getting list of files to delete\n";
    # get list of files to delete
    my $sqlStatement=<<EOSql;
SELECT name FROM v\$datafile
 UNION
SELECT name FROM v\$controlfile
 UNION
SELECT name FROM v\$tempfile
 UNION
SELECT member FROM v\$logfile
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting files to delete: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $name);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding names: $DBI::errstr\n";
        return 0;
    }

    # setup mulitdimensioal array to handle multithreading
    # each internal array will handle files to delete for one thread

    my @lunFiles;
    my $curLun;
    my @tmpArray;

    $$logStr .= "List of files to delete:\n";
    while ($sth->fetch())
    {
        # always put into the current tmp array.  If the file is not on the
        # lun handle in any thread because the file will be small        

        $$logStr .= "$name\n";

        if ($name =~ /^(\/\D{6}\d-\d\d)/)
        {
            # file is on a lun
            my $lun = $1;
            $curLun = $lun if (!defined $curLun);

            if ($lun ne $curLun)
            {
                # completed processing one lun.      
                
                my @tmpArrCopy  = @tmpArray;
                push (@lunFiles, \@tmpArrCopy);
 
                undef @tmpArray;
                $curLun = $lun;  
            }
        }
        push (@tmpArray, $name);             
    }
    $sth->finish();

    push (@lunFiles, \@tmpArray);

    $$logStr .= "Getting list of directories to clearout.\n";
    # get list of files to delete
    # get list of directories to cleanout
    $sqlStatement=<<EOSql;
SELECT DISTINCT value 
  FROM v\$parameter 
 WHERE name IN ('background_dump_dest','core_dump_dest','user_dump_dest',
                'audit_file_dest','log_archive_dest_1')
EOSql
 
    $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting directories to clean out: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\$name);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding names: $DBI::errstr\n";
        return 0;
    }

    my @cleanoutDirs;
    while ($sth->fetch())
    {
        $name =~ s/^LOCATION=//;
        push (@cleanoutDirs, $name);
    }
    $sth->finish();

    # shutdown abort the database
    if (!shutdownDatabase($self, $logStr, 'A'))
    {
        # logStr is alreay filled out
        return 0;
    }

    $$logStr .= "About to fork process for delete\n";

    my @pids;

    foreach my $files (@lunFiles)
    {
        my $pid = fork();
        if (!defined $pid)
        {
            $$logStr .= "Could not fork process to delete files\n";
            return 0; 
        }
        elsif ($pid)
        {
             # This is the parent
             push (@pids, $pid);
        }
        else
        {
            # this is the child

            if (!unlink @{$files})
            {
                # the delete failed, save error string in a file, no
                # error checking for file here because the child
                # can't write to the log string.
                if (open (TMP_FH, "> $self->{workDir}/tmp_delete_$$.txt"))
                {
                    print TMP_FH "$!";
                    close TMP_FH;
                }
                exit 0;
            }
            exit 1; 
        }
    }

    # All child process code will have been completed by
    # this point, so this is the parent.  Check on the 
    # status of the child processes.
  
    $$logStr .= "Waiting for child processes to complete.  Will wait for $threadCheckSeconds seconds between each check\n"; 

    my @tmpPids;
    my $errStr;

    while (@pids)
    {
        sleep $threadCheckSeconds;

        foreach my $pid (@pids)
        {
            if (!waitpid ($pid, WNOHANG))
            {
                # zero indicates the child process is still running
                push (@tmpPids, $pid);
            }
            else
            {
                $$logStr .= "Child process with pid $pid completed";

                if (!$?)
                {
                    if (open (TMP_FH, "< $self->{workDir}/tmp_delete_$pid.txt"))
                    {
                        # will be one line
                        $errStr = <TMP_FH>;
                        close TMP_FH;
                        # no need to check for error
                        unlink "$self->{workDir}/tmp_delete_$pid.txt";
                        
                    }
                    else
                    {
                        $errStr = "Cannot get error string\n";
                    }

                    $$logStr .= " with error: $errStr\n"; 
 
               }
               else
               {
                  $$logStr .= " successfully\n"; 
               }
            }
        }
        
        @pids = @tmpPids; 
        undef @tmpPids;       
    }

    if (defined $errStr)
    { 
        $$logStr .= "Delete unsuccessful\n";
        return 0;
    }

    $$logStr .= "Delete successful\n";
 
    # clear out directories
    $$logStr .= "About to clean out directories\n";
    foreach my $dir (@cleanoutDirs)
    {
        $$logStr .= "About to clean out $dir\n";
        rmtree($dir, {keep_root => 1, error => \my $err});
        if (defined @$err)
        {
            $$logStr .= "Failed while cleaning out dirs: ";
            foreach my $diag (@$err) 
            {
                my ($file, $message) = %$diag;
                if ($file eq '') 
                {
                    $$logStr .= $message . "\n";
                }
                else 
                {
                    $$logStr .= "file: $file - $message\n";
                }
            }
            return 0;
        }

        # there is a bug in older versions of perl where the base directory is
        # deleted.  Handle this by recreating the directory if it is gone.
        if (!-d $dir)
        {
            $$logStr .= "$dir does not exist, creating it\n";

            eval {mkpath($dir)};
            if ($@)
            {
                $$logStr .= "Cannot make directory $dir: $@\n";
                return 0;
            }
        }
    }
    $$logStr .= "Clean out successful\n";

    return 1;
}

##############################################################################
#
#  Function: compileDatabase
#     compiles the database 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub compileDatabase
{
    my ($self, $logStr) = @_;

    $$logStr .= "In compileDatabase\n";
    $$logStr .= "About to compile\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRCompileDBSql_" . getDateTimeStr() . ".log";
    if (!runSqlAsSysdba($logStr, "\@$ENV{ORACLE_HOME}/rdbms/admin/utlrp.sql", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    my $invalidLog = "$self->{workDir}/ADRInvalidObjects_" . getDateTimeStr() . ".log";

    $$logStr .= "About to print invalid objects into $invalidLog\n"; 

    my $sqlStatement=<<EOSql;
SELECT owner,
       object_type,
       object_name 
  FROM dba_objects 
 WHERE status <> 'VALID' 
ORDER by owner, object_type, object_name
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting invalid files: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $owner, \my $type, \my $name);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding owner, type and names: $DBI::errstr\n";
        return 0;
    }

    open (FH, "> $invalidLog");

    while ($sth->fetch())
    {
        printf FH ("%-20s%-20s%s\n", $owner, $type, $name);
    }
    $sth->finish(); 

    if (!defined $owner)
    {
        print FH  "There are no invalid objects\n";
    }

    close FH;

    $$logStr .= "Compilation successful\n";
    return 1;

}

##############################################################################
#
#  Function: autoExtend
#      sets autoextend for file_name from dba_datafiles based on tablespace size
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub autoExtend
{
    my ($self, $logStr) = @_;

    $$logStr .= "In autoExtend\n";

    $$logStr .= "Getting tablespaces and sizes\n";
    # get list of files to delete
    my $sqlStatement=<<EOSql;
SELECT tablespace_name,
       sum(bytes)/1024/1024 
  FROM dba_data_files 
 GROUP BY tablespace_name
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting tablespaces and sizes: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $tableSpaceName, \my $size);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding tablespaces and sizes: $DBI::errstr\n";
        return 0;
    }

    # set hash to avoid having too many statement handles open at once
    my %tSpaceAutoExtend;
    while ($sth->fetch())
    {
        # extendSize is initially set to the max.  This will be for 
        # tablespace sizes greater than 131068

        my $extendSize = 2048;

        if ($size < 32768)
        {
            $extendSize = 512;
        }
        elsif ($size <= 65534)
        {
            $extendSize = 1024;
        }
        elsif ($size <= 131068)
        {
            $extendSize = 1536;
        }   

        $tSpaceAutoExtend{$tableSpaceName} = $extendSize;
    }
    $sth->finish();
  
     # prepare once

     $$logStr .= "Getting filenames for each tablespace\n";
     $sqlStatement=<<EOSql;
SELECT file_name 
  FROM dba_data_files
 WHERE tablespace_name = ?
EOSql
 
    $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "About to set autoextend\n";
    foreach my $tableSpace (keys %tSpaceAutoExtend)
    {
        $rv = $sth->execute($tableSpace);
        if (!defined $rv)
        {
           $$logStr .= "Failed while getting filenames for $$tableSpace: $DBI::errstr\n";
           return 0;
        }

        $rv = $sth->bind_columns(\my $filename);
    
        if (!defined $rv)
        {
            $$logStr .= "Failed while binding filenames: $DBI::errstr\n";
            return 0;
        }

        while ($sth->fetch())
        {
            $sqlStatement = "ALTER DATABASE DATAFILE \'$filename\' AUTOEXTEND ON NEXT ";
            $sqlStatement .= $tSpaceAutoExtend{$tableSpace} . "M MAXSIZE UNLIMITED";

            $rv = $self->{clientDbh}->do($sqlStatement);
            if (!defined $rv)
            {
                $$logStr .= "Failed while setting autoextend: DBI::errstr\n";
                return 0;
            }
        } 
    }
    $$logStr .= "Setting autoextend successful\n";
    $sth->finish();
   
    return 1;
}

##############################################################################
#
#  Function: setFlashback
#      turn flashback on of off
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub setFlashback
{
    my ($self, $logStr, $action) = @_;

    $$logStr .= "In setFlashback for action, $action\n";

    if ($action !~ /^on/i && $action !~ /^off/i)
    {
        $$logStr .= "Uknown action, $action.  Action should be Off or On\n";
        return 0;
    }

    if (!runSql($self, $logStr, "ALTER DATABASE FLASHBACK $action"))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Set flashback successful\n";
    return 1;
}

##############################################################################
#
#  Function: setOpenResetLogs
#      open database in reset log mode
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub setOpenResetLogs
{
    my ($self, $logStr) = @_;

    $$logStr .= "In setOpenResetLogs\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADROpenResetLogsSql_" . getDateTimeStr() . ".log";
    if (!runSqlAsSysdba($logStr, "ALTER DATABASE OPEN RESETLOGS", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Set open resetlogs successful\n";
    return 1;
}

##############################################################################
##
##  Function: setOpenResetLogs
##      open database in reset log mode
##
##  Args:
##
## Pre-requisites:
##
###############################################################################

sub setOpenResetLogsUpgrade
{
    my ($self, $logStr) = @_;
    $$logStr .= "In setOpenResetLogsUpgrade\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADROpenResetLogsUpgradeSql_" . getDateTimeStr() . ".log";
    if (!runSqlAsSysdba($logStr, "ALTER DATABASE OPEN RESETLOGS UPGRADE", $logFile))
    {
        #logStr will already be set
        #        return 0;
    }
    $$logStr .= "Open and resetlogs in upgrade mode successful\n";
    return 1;
}

##############################################################################
#
#  Function: setBlockChangeTracking
#      sets block change tracking on or off
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub setBlockChangeTracking
{
    my ($self, $logStr, $action) = @_;

    $$logStr .= "In setBlockChangeTracking for action, $action\n";

    if ($action =~ /^off$/i)
    {
        $$logStr .= "About to disable block change tracking\n";
        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRBlockChangeTrackingDBSql_" . getDateTimeStr() . ".log";

        # ignore if the error is ORA-19759
        my %ignoreErrors = ('ORA-19759' => 1);

        if (!runSqlAsSysdba($logStr, "ALTER DATABASE DISABLE BLOCK CHANGE TRACKING", $logFile, \%ignoreErrors))
        {
            #logStr will already be set
            return 0;
        }

        $$logStr .= "Disable block change tracking successful\n";
    }
    elsif ($action =~ /^on$/i)
    {
        $$logStr .= "About to enable block change tracking\n";
        use File::Basename;

        $$logStr .= "About to get directory name\n";

        my $sqlStatement=<<EOSql;
SELECT file_name 
  FROM dba_data_files 
 WHERE tablespace_name = \'SYSTEM\'
EOSql

        my $sth = $self->{clientDbh}->prepare($sqlStatement);
    
        if (!defined $sth)
        {
            $$logStr .= "Failed while preparing: $DBI::errstr\n";
            return 0;
        }
    
        my $rv = $sth->execute();
        if (!defined $rv)
        {
            $$logStr .= "Failed while getting directory name: $DBI::errstr\n";
            return 0;
        }

        # just take the first one
        my $dir = dirname($sth->fetchrow_array());
    
        if (!defined $dir)
        {
            $$logStr .= "SYSTEM entry is missing from dba_data_files\n";
            return 0;
        }

        $$logStr .= "Directory name is $dir\nAbout to get block change tracking status\n";    
    
        $sqlStatement=<<EOSql;
SELECT status
  FROM v\$block_change_tracking 
EOSql
    
        $sth = $self->{clientDbh}->prepare($sqlStatement);
    
        if (!defined $sth)
        {
            $$logStr .= "Failed while preparing: $DBI::errstr\n";
            return 0;
        }
    
        $rv = $sth->execute();
        if (!defined $rv)
        {
            $$logStr .= "Failed while getting block change tracking: $DBI::errstr\n";
            return 0;
        }
    
        # should only be one
        my $status = $sth->fetchrow_array;
    
        if ($sth->fetchrow_array())
        {
            $$logStr .=  "Expecing 1 row while getting block change tracking, but got more\n";
            return 0;
        }
    
        $$logStr .= "Block change tracking status is $status\n";
     
        if ($status ne 'DISABLED')
        {
            $$logStr .= "Disable block change tracking\n";
            if (!setBlockChangeTracking($self, $logStr, 'off'))
            {
                # logStr has already been set
                return 0;
            }
        }

        $$logStr .= "About to enable block change tracking\n";
        $sqlStatement = "ALTER DATABASE ENABLE BLOCK CHANGE TRACKING USING FILE ";
        $sqlStatement .= "'$dir/block_change_tracking.dbf' REUSE";
        if (!runSql($self, $logStr, $sqlStatement))
        {
            #logStr will already be set
            return 0;
        }
    

        $$logStr .=  "Enable block change tracking successful";
    }
    else
    {
        $$logStr .= "Uknown action, $action.  Action should be Off or On\n";
        return 0;
    }

    return 1;
}

##############################################################################
#
#  Function: setSupplementalLog
#      sets the supplemental log 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub setSupplementalLog
{
    my ($self, $logStr) = @_;

    $$logStr .= "In setSupplementalLog\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRSupLogSql_" . getDateTimeStr() . ".log";

    if (!runSqlAsSysdba($logStr, "ALTER DATABASE ADD SUPPLEMENTAL LOG DATA", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    if (!runSqlAsSysdba($logStr, "ALTER DATABASE ADD SUPPLEMENTAL LOG DATA(primary key) COLUMNS", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Setting supplemental log successful\n";
    return 1;
}

##############################################################################
#
#  Function: createSpFile
#      create the spfile from the pfile 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub createSpFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createSpFile\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRCreateSpSql_" . getDateTimeStr() . ".log";
    if (!runSqlAsSysdba($logStr, "CREATE spfile FROM pfile", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Creating spfile successful\n";
    return 1;
}

##############################################################################
#
#  Function: setGlobalName
#      sets the global name 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub setGlobalName
{
    my ($self, $logStr) = @_;

    $$logStr .= "In setGlobalName\n";

    if (!runSql($self, $logStr, "ALTER DATABASE RENAME global_name to $self->{targetSid}"))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Setting  global name successful\n";
    return 1;
}

##############################################################################
#
#  Function: changeDatabaseId
#      changes the database id to the target via nid 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub changeDatabaseId
{
    my ($self, $logStr) = @_;

    $$logStr .= "In changeDatabaseId\n";

    my $nid = "$ENV{ORACLE_HOME}/bin/nid";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRChangeDBIdNid_" . getDateTimeStr() . ".log";
    my $cmd = "$nid target=system/$self->{clientDbPW} dbname=$self->{targetSid} LOGFILE=$logFile";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    if (!open (NID_PIPE, "| $cmd"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return 0;
    }

    $$logStr .= "Entering Y\n";
    print NID_PIPE "Y";

    close NID_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'nid'))
    {
        #$$logStr will be set
        return 0;
    }

    $$logStr .= "Changing database id successful\n";
    return 1;
}

##############################################################################
#
#  Function: unregisterOldDatabase
#      unregister the old database with the rman catalog 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test
sub unregisterOldDatabase
{
    my ($self, $logStr, $rmanCatalogSid, $rmanCatalogUser) = @_;

    $$logStr .= "In unregisterOldDatabase\n";

    $$logStr .= "About to get RMAN password\n";
    # get rman password and user from big brother
    my $sqlStatement=<<EOSql;
SELECT bb_get(nvl(u.temp_code, u.code))
  FROM databases d,
       user_codes u
 WHERE d.instance = u.db_instance
   AND d.sid = \'$rmanCatalogSid\'
   AND u.username = upper(\'$rmanCatalogUser\')
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting rman password: $DBI::errstr\n";
        return 0;
    }

    # should only be one
    my $rmanCatalogPW = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while getting rman password, but got more\n";
        return 0;
    }

    my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRListIncarnationRMAN_" . getDateTimeStr() . ".log";

    my $rmanCmd = "\"list incarnation of database $self->{targetSid}\;\"";
    #my $cmd = "$runRman \"target / catalog=$rmanCatalogUser/$rmanCatalogPW\@$rmanCatalogSid\" $rmanCmd $logFile";
    my $cmd = "$runRman \"target / catalog $rmanCatalogUser" . "[rman]/$rmanCatalogPW\@$rmanCatalogSid\" $rmanCmd $logFile";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$rmanCatalogPW\@/$self->{hidePW}\@/;

    $$logStr .= "About to run $hidePWCmd\n";

    if (!open (RMAN_PIPE, "$cmd |"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <RMAN_PIPE>;
    close RMAN_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'rman'))
    {
        #$$logStr will be set
        return 0;
    }

    $$logStr .= "Getting db keys and ids from log file\n";

    my @fileTxt = (qr/^List of Database Incarnations$/,
                   qr/^DB Key\s+Inc Key\s+DB Name\s+DB ID\s+STATUS\s+Reset\s+SCN\s+Reset Time$/,
                   qr/^-------\s+-------\s+--------\s+----------------\s+---\s+----------\s+----------$/,
                   qr/NOPARSE/);  #This is needed so it doens't match blank lines

    if (!open (RMAN_FH, "< $logFile"))
    {
        $$logStr .= "Cannot open $logFile: $!\n";
        return 0;
    }

    my $foundIdx = 0;
    my $curDbId = 0;
    my %dbHash;

    while (<RMAN_FH>)
    {
        if (/^connected to target database: \w+ \(DBID=(\d+)\)$/) 
        {
            $curDbId = $1;
        }
        elsif (/^$fileTxt[$foundIdx]$/) 
        {
            if (!$curDbId)
            {
                $$logStr .= "Did not find the current DBID.\n";
                return 0;
            }
         
            $foundIdx++;
        }
        elsif ($foundIdx == @fileTxt - 1)
        {
            # Start parsing here.  Have already read leading lines 
            if (/^(\d+)\s+\d+\s+\w+\s+(\d+)/)
            {
                my $dbKey = $1;
                my $dbId = $2;

                # use a hash with a comma separate string as key
                # to handle duplicates
                $dbHash{"$dbKey,$dbId"} = 1 if($curDbId != $dbId);
            }
            elsif (/^$/)
            {
                # end of list - break out of loop
                last;
            }
            else
            {
                $$logStr .= "Parsing of $logFile failed.   Line is $_\n";
                return 0;
            }
        }
        elsif ($foundIdx)
        {
             $$logStr .= "Found $_, but expect next line to be, $fileTxt[$foundIdx]\n";
             return 0;    
        }
    }
    
    close RMAN_FH;

    $$logStr .= "Getting db keys and ids from log file successful\n";

    if ($foundIdx)
    {
        my $sqlPlus = "$ENV{ORACLE_HOME}/bin/sqlplus";
        $cmd = "$sqlPlus -s $rmanCatalogUser" . "[rman]/$rmanCatalogPW\@$rmanCatalogSid";

        $hidePWCmd = $cmd;
        $hidePWCmd =~ s/$rmanCatalogPW\@/$self->{hidePW}\@/;

        $$logStr .= "About to unregister database\n";
        my $count = 1;

        foreach my $dbStr (keys %dbHash)
        {
            $dbStr =~/(\d+),(\d+)/;

            my $dbKey = $1;
            my $dbId = $2;

            # This rman catalog sql plus is not in a separate function because 
            # it will only happen here

            $$logStr .= "About to run $hidePWCmd\n";
    
            my $sqlLogFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRUnregisterSql_" . $count . "_" . getDateTimeStr() . ".log";
    
            if (!open (SQL_PIPE, "| $cmd >> $sqlLogFile"))
            {
                $$logStr .= "Can't execute command.  Error is $!\n";
                return 0;
            }

            my $sqlStatement = "EXECUTE dbms_rcvcat.unregisterdatabase($dbKey, $dbId)";

            $$logStr .= "Running $sqlStatement\n";
            print SQL_PIPE "$sqlStatement\;";
            close SQL_PIPE;

            if (!checkLogForErrors($logStr, $sqlLogFile, 'sql'))
            {
                #$$logStr will be set
                return 0;
            }
        }

        $$logStr .= "Unregistering old database successful\n";
    }
    else
    {
        $$logStr .= "No database to unregister\n";
    }
    return 1;
}

##############################################################################
#
#  Function: runSql
#      run the given sql statement
#
#  Args:
#      sqlStatement - sql statement to execute.  Select will not return anything
#
# Pre-requisites:
#
##############################################################################

sub runSql
{
    my ($self, $logStr, $sqlStatement, $ignoreErrRef) = @_;

    $$logStr .= "In runSql\n";

    # if file, append scripts directory
    if ($sqlStatement =~ /^\@(\S+)/)
    {
        $sqlStatement = "\@$self->{sqlScriptsDir}/$1";
    }

    return runSqlDbi($logStr, $sqlStatement, $self->{clientDbh}, $ignoreErrRef);

}

##############################################################################
#
#  Function: runSqlSysDba
#      run the given sql statement as sysdba
#
#  Args:
#      sqlStatement - sql statement to execute.  Select will not return anything
#
# Pre-requisites:
#
##############################################################################

sub runSqlSysDba
{
    my ($self, $logStr, $sqlStatement) = @_;

    $$logStr .= "In runSqlSysDba\n";

    # if file, append scripts directory
    if ($sqlStatement =~ /^\@(\S+)/)
    {
        $sqlStatement = "\@$self->{sqlScriptsDir}/$1";
    }

    # create a log file.  Can't use sqlStatement as log name since it could
    # be an actual statement instead of a file
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_RunSql_" . getDateTimeStr() . ".log";

    my $i = 1;
 
    while (-e $logFile && $i < 100)
    {
        my $tmpStr = $i . '_';
        # only check to 100, there shouldn't be that many
        $logFile =~ s/RunSql_\d{1,3}_/RunSql_/;
        $logFile =~ s/RunSql_/RunSql_$tmpStr/;
        $i++;
    }

    return runSqlAsSysdba($logStr, $sqlStatement, $logFile);

}


##############################################################################
#
#  Function: isDatabaseRunning
#      Checks to see if the database is running.
#
#  Args: 
#
# Pre-requisites:
#
# Returns 1 if running, 0 if not running and undef for error
#
##############################################################################
sub isDatabaseRunning
{
    my ($self, $logStr) = @_;

    my $grepCmd = "grep ora_smon_$self->{targetSid}";
    my $cmd = "ps -ef |$grepCmd |grep -v \"$grepCmd\"";

    if (!open (PS_PIPE, "$cmd |"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return undef;
    }

    my @outp = <PS_PIPE>;
    close PS_PIPE;

    my $retVal = 0;
    foreach (@outp)
    {
        if (/\d{2}:\d{2}:\d{2} ora_smon_$self->{targetSid}$/)
        {
            $retVal = 1;
        }
    } 

    return $retVal;
}

sub resetClientDbh
{
    my ($self, $logStr, $dbh) = @_;

    $$logStr .= "In resetClientDbh\n";

    $self->{clientDbh} = $dbh;
}

1;
