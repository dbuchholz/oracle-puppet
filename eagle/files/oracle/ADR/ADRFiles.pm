package ADRFiles;

# Provide functions to create RestoreDataTempFiles_<targetSid>.rcv

# Funtions:
# preReqLunSize - make sure the target has enought space on the luns
# preReqASMSize - make sure the target has enought space on the ASM managed DB luns
# preReqStorage - make sure the ASM target has enought space during conversion LUN2ASM
# preReqOraTab - make sure <targetSid> is in the oratab file 
# preReqRestoreDTFile - make sure <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and <sourceSid>_<backupSet>_LOG.TXT exist
# createRestoreDTFile - Create RestoreDataTempFiles_<targetSid>.rcv
# createRestoreDTFileLUN2ASM - Create RestoreDataTempFiles_<targetSid>.rcv for LUN-to-ASM refresh type
# runRestoreDTFile - Runs RestoreDataTempFiles_<targetSid>.rcv via rman
# preReqRestoreRedoFile - make sure <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
# createRestoreRedoFile - Create run_rename_redo.rcv
# createRestoreRedoFileLUN2ASM - Create run_rename_redo.rcv for LUN-to-ASM refresh type.
# runRestoreRedoFile - Runs run_rename_redo.rcv via SQL
# preReqControlFile - make sure <backupLocation>/Restore*ControlFile_<sourceSid>_<backupSet>.rcv and <backupLocation>/cf_<backupSet>*_full_ctrl exists
# createControlFile - Create <sourceSid>_specific_cf.rcv
# runControlFile - Runs <sourceSid>_specific_cf.rcv via rman
# preReqInitOraFile - make sure <backupLocation>/<backupSet>_init<sourceSid>.ora exists
# preReqInitOraLocalFile - make sure that init.eagle under $ORACLE_HOME/dbs directory exists on target machine
# createInitOraFile - Create init<targetSid>.ora
# createInitOraFileLUN2ASM - Create init<targetSid>.ora for LUN-to-ASM refresh type.
# copyInitOraLocalFile - Copy content of local Init Ora File $ENV{ORACLE_HOME}/dbs/init.eagle to $ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora
# updateInitOraFileSid - Update the sid in init<targetSid>.ora to be the target
# createPwFile - Create orapw<targetSid>.ora 
# validateDatafiles - checks for bad datafiles
# preReqOraEnv - Make sure the oracle enviornment has been set
# all others should be considered private
# Note there is no error checking when getting the filename from the wildcarded
# name.  This is because that would have been done when checking prereqs


use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;
use Sys::Hostname;
use File::Path;
use File::Basename;
use File::Copy;

#TODO take out clientDbh
##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      targetSid - target sid to create the RestoreDataTempFiles for
#      sourceSid - source sid used as a base to create RestoreDataTempFiles
#      backupSet - backup set of the source sid
#      backupLocation - location of the source sid backup
#      bbDbh - big brother database handle
#      clientDbh - client database handle
#
##############################################################################

sub new
{
    my ($class, $targetSid, $sourceSid, $refreshQueueInst, $backupSet, $backupLocation, $isASM, $bbDbh, $clientDbh) = @_;

    my $self = {
        workDir => "$ENV{ADR_LOCATION}/$refreshQueueInst" . "_workDir",
        targetSid => $targetSid,
        sourceSid => $sourceSid,
        refreshQueueInst => $refreshQueueInst,
        backupSet => $backupSet, 
        backupLocation => $backupLocation,
        bbDbh => $bbDbh,
        clientDbh => $clientDbh,
        hostName => undef,
        fileSizeHash => undef,    # hash used when creating new restore dt file 
        lunSizeHash => undef,     # hash used when creating new restore dt file 
        dataFileLunHash => undef, # hash used when creating new restore dt file 
        matchingLunHash => undef, # hash used when reusing restore dt file
        receiveChannels  => undef,
        archiveLogDest  => undef,
        sourceArchiveLogDest  => undef,
        canReuseRestoreDTFile  => undef,
        restoreDTFile => undef,
        sourcRestoreDTFile => undef,
        sourceRestoreDTLogFile => undef,
        restoreRedoFile => undef,
        sourceRestoreRedoFile => undef,
        controlFile => undef,
        sourceControlFile => undef,
        sourceFullControlFile => undef,
        initOraFile => undef,
        sourceInitOraFile => undef,
        isASM => $isASM
    };

    $self->{hostName} = hostname();
    # strip out domain
    $self->{hostName} =~ s/\..+\..+//;
 
    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: preReqOraTab
#      make sure <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqOraTab
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqOraTab\n";

    $$logStr .= "Checking Ora tab file\n";

    my $oraTabFile = "/etc/oratab";
    if ($^O ne 'linux')
    {
        #set to unix file
        $oraTabFile = "/var/opt/oracle/oratab";
    }

    if (!open (ORATAB_FH, "< $oraTabFile"))
    {
        $$logStr .= "Cannot open $oraTabFile: $!\n";
        return 0;
    }

    my @oraTabLines = <ORATAB_FH>;
    my @sidLines = grep {/^$self->{targetSid}/} @oraTabLines;
    @sidLines = grep {!/^#/} @sidLines;
    close ORATAB_FH;

    # TODO: error if there there is more than one?  
    if (!@sidLines)
    {
        $$logStr .= "$oraTabFile does not contain an entry for target $self->{targetSid}\n";
        return 0;
    }

    $$logStr .= "$oraTabFile contains an entry for target $self->{targetSid}\n"; 
    return 1;
}

##############################################################################
#
#  Function: preReqRestoreRedoFile
#      make sure <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists 
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub preReqRestoreRedoFile
{

    my ($self, $logStr) = @_;

    $$logStr .= "In preReqRestoreRedoFile\n";

    $$logStr .= "Checking Restore redo file\n";


    $self->{sourceRestoreRedoFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*Redo_" . $self->{backupSet} . ".rcv");

    my $retVal = 1;

    # getFileName returns 0 if the file does not exist
    if (!$self->{sourceRestoreRedoFile})
    {
        $$logStr .= "$self->{backupLocation}/Restore\*Redo_" . $self->{backupSet} . ".rcv does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Found Restore Redo file\n" if ($retVal);

    return $retVal;

}

##############################################################################
#
#  Function: createRestoreRedoFile
#      Create run_rename_redo.rcv.  
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#
##############################################################################
sub createRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createRestoreRedoFile\n";

    if (!defined $self->{sourceRestoreRedoFile})
    {
        # preReqRestoreRedoFile has not been run
        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceRestoreRedoFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*Redo_" . $self->{backupSet} . ".rcv");
    }

    $$logStr .= "Source restore redo file is $self->{sourceRestoreRedoFile}\n";

    $self->{restoreRedoFile} = "$self->{workDir}/run_rename_redo.rcv";

    $$logStr .= "Target restore redo file is $self->{restoreRedoFile}\n";

    my $retVal = 0;
    if ($self->{isASM})
    {
        $retVal = createASMRestoreRedoFile($self, $logStr);
    }
    else
    {
        $retVal = createStandardRestoreRedoFile($self, $logStr);
    }
    return $retVal;
}

sub createASMRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createASMRestoreRedoFile\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreRedoFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{restoreRedoFile}: $!\n";
        return 0;
    }

    $$logStr .= "Replacing xxxxxx with $self->{targetSid}\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        # strip out blank lines
        next if ($line =~ /^\s*$/);

        $line =~ s/xxxxxx/$self->{targetSid}/;
        print TARGETRESTORE_FH "$line";
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;
    $$logStr .= "Create restore redo file successful\n";
    return 1;
}

sub createStandardRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createStandardRestoreRedoFile\n";

    my $canReuseRestoreRedoFile = canReuseRestoreRedoFile($self, $logStr);
  
    if (!defined $canReuseRestoreRedoFile)
    {
        # $logStr is already filled out
        return 0;
    }

    if ($canReuseRestoreRedoFile)
    { 
        if (!reuseRestoreRedoFile($self, $logStr))
        {
            # logStr is already filled out
            return 0;
        }
    }
    else
    {
        if (!createNewRestoreRedoFile($self, $logStr))
        {
            # logStr is already filled out
            return 0;
        }
    }

    return 1;
}

##############################################################################
##
##  Function: createRestoreRedoFileLUN2ASM
##      Create run_rename_redo.rcv for LUN-to-ASM refresh type.
##
##  Args:
##
## Pre-requisites:
##     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
##
###############################################################################

sub createRestoreRedoFileLUN2ASM
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createRestoreRedoFileLUN2ASM\n";

    if (!defined $self->{sourceRestoreRedoFile})
    {
        # preReqRestoreRedoFile has not been run
        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceRestoreRedoFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*Redo_" . $self->{backupSet} . ".rcv");
    }

    $$logStr .= "Source restore redo file is $self->{sourceRestoreRedoFile}\n";

    $self->{restoreRedoFile} = "$self->{workDir}/run_rename_redo.rcv";

    $$logStr .= "Target restore redo file is $self->{restoreRedoFile}\n";

    my $retVal = 0;
    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreRedoFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{restoreRedoFile}: $!\n";
        return 0;
    }
    my $count = 0;
    $$logStr .= "Replacing xxxxxx with $self->{targetSid}\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        # strip out blank lines
        next if ($line =~ /^\s*$/);
        $count++;
        if ($count % 2)
        {
            $line =~ s/\/xxxxxx-\d\d\/oradata\/redo\d{1,2}\w{0,1}\.log/+RED/;
        }
        else
        {
            $line =~ s/\/xxxxxx-\d\d\/oradata\/redo.*\d{1,2}\w{1}\.log/+FRA/;
        }
        print TARGETRESTORE_FH "$line";

    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;
    $$logStr .= "Create restore redo file successful\n";
    return 1;
}

##############################################################################
#
#  Function: runRestoreRedoFile
#      Run run_rename_redo.rcv via sql.  
#
#  Args:
#
# Pre-requisites:
#     run_rename_redo.rcv exists
#
##############################################################################

sub runRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In runRestoreRedoFile\n";

    if (!defined $self->{restoreRedoFile})
    {
        # This is a rerun.  Try to find the restore redo file. 

        my $filename =  "$self->{workDir}/run_rename_redo.rcv";
        if (-e $filename)   
        {
            $self->{restoreRedoFile} = $filename;
        }
        else
        {
            $$logStr .= "Cannot find $filename.  Either createRestoreRedoFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
    }

    $$logStr .= "Restore Redo file is $self->{restoreRedoFile}\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRestoreRedo_" . getDateTimeStr() . ".log";

    if (!runSqlAsSysdba($logStr, "\@$self->{workDir}/run_rename_redo.rcv", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Run of Restore Redo file successful\n";
    return 1;
}

##############################################################################
#
#  Function: preReqRestoreDTFile
#     make sure <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv
#     and <sourceSid>_<backupSet>_LOG.TXT exist 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqRestoreDTFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqRestoreDTFile\n";

    $$logStr .= "Checking LOG.TXT file\n";
    $self->{sourceRestoreDTLogFile} = "$self->{backupLocation}/$self->{backupSet}" . "_LOG.TXT";

    my $retVal = 1;

    if (!-e $self->{sourceRestoreDTLogFile})
    {
        $$logStr .= "$self->{sourceRestoreDTLogFile} does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Checking Restore DataTemp file\n";
    $self->{sourceRestoreDTFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv");

    # getFileName returns 0 if the file does not exist
    if (!$self->{sourceRestoreDTFile})
    {
        $$logStr .= "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Found LOG.TXT and Restore DataTemp files\n" if ($retVal);

    return $retVal;

}

##############################################################################
#
#  Function: createRestoreDTFile
#      Create RestoreDataTempFiles_<targetSid>.rcv.  Check to see if the source
#      and the destination have the same amount of mount points and space.
#      Call the appriate create function depending on the result. 
#
#  Args: 
#      mpPercentThreshold - percent of the amount of space that cannot be
#                           exceeded for each mount point
#      maxReceiveChannels - maximum receive channels allowed.
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
#     <sourceSid>_<backupSet>_LOG.TXT exist
#
##############################################################################
sub createRestoreDTFile
{
    my ($self, $logStr, $mpPercentThreshold, $maxReceiveChannels) = @_;

    $$logStr .= "In createRestoreDTFile\n";

    my $retVal = 0;
    if ($self->{isASM})
    {
        $retVal = createASMRestoreDTFile($self, $logStr);
    }
    else
    {
        $retVal = createStandardRestoreDTFile($self, $logStr, $mpPercentThreshold, $maxReceiveChannels);
    }

    return $retVal;
}

sub createASMRestoreDTFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createASMRestoreDTFile\n";

    if (!defined $self->{sourceRestoreDTFile})
    {
        # preReqRestoreDTFile has not been run

        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceRestoreDTFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv");
    }

    $$logStr .= "Source restore datatemp file is $self->{sourceRestoreDTFile}\n";

    $self->{restoreDTFile} = "$self->{workDir}/RestoreDataTempFiles_" . $self->{targetSid} . ".rcv";

    $$logStr .= "Target restore redo file is $self->{restoreDTFile}\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreDTFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreDTFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreDTFile}"))
    {
        $logStr .= "Cannot open $self->{restoreDTFile}: $!\n";
        return 0;
    }

    $$logStr .= "Replacing xxxxxx with $self->{targetSid}\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        # strip out blank lines
        next if ($line =~ /^\s*$/);

        $line =~ s/xxxxxx/$self->{targetSid}/;
        print TARGETRESTORE_FH "$line";
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;
    $$logStr .= "Create restore data temp file successful\n";
    return 1;
}

##############################################################################
##
##  Function: createRestoreDTFileLUN2ASM
##      Create RestoreDataTempFiles_<targetSid>.rcv.  Check to see if the source
##      and the destination have the same amount of mount points and space.
##      Call the appriate create function depending on the result.
##
##  Args:
##      mpPercentThreshold - percent of the amount of space that cannot be
##                           exceeded for each mount point
##      maxReceiveChannels - maximum receive channels allowed.
##
## Pre-requisites:
##     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
##     <sourceSid>_<backupSet>_LOG.TXT exist
##
###############################################################################
sub createRestoreDTFileLUN2ASM
{
    my ($self, $logStr, $mpPercentThreshold, $maxReceiveChannels) = @_;

    $$logStr .= "In createRestoreDTFileLUN2ASM\n";
    if (!defined $self->{sourceRestoreDTFile})
    {
        # preReqRestoreDTFile has not been run
        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceRestoreDTFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv");
    }
    $$logStr .= "Source restore datatemp file is $self->{sourceRestoreDTFile}\n";

    $self->{restoreDTFile} = "$self->{workDir}/RestoreDataTempFiles_" . $self->{targetSid} . ".rcv";

    $$logStr .= "Target restore data file is $self->{restoreDTFile}\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreDTFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreDTFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreDTFile}"))
    {
        $logStr .= "Cannot open $self->{restoreDTFile}: $!\n";
        return 0;
    }

    my $printAllocateChannel = 0;
    $$logStr .= "Replacing xxxxxx with $self->{targetSid} and setting receive channels\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        if ($line =~ /^(\s*allocate channel \'RCHL)\d+(\' device type disk\;)/)
        {
            if (!$printAllocateChannel)
            {
                # this is the first allocate channel line.
                # print allocate channel lines all at once and ignore
                # subsequent allocate lines
                $printAllocateChannel = 1;
                # my $recChannels = getReceiveChannels($self, $logStr, $maxReceiveChannels);
                my $recChannels = 8;
                if (!defined $recChannels)
                {
                    #logStr is alreay filled out
                    return 0;
                }

                for (my $i = 1; $i <= $recChannels; $i++)
                {
                     print TARGETRESTORE_FH $1 . $i . $2 . "\n";
                }
            }
        }
        else
        {
            if ($line =~ /xxxxxx-(\d\d)/)
            {
                $line =~ s/\/xxxxxx.*dbf/+DATA/;
            }

            print TARGETRESTORE_FH "$line";   
        }
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;
    $$logStr .= "Create restore data temp file successful for LUN2ASM refresh\n";
    
    return 1;
}

sub createStandardRestoreDTFile
{
    my ($self, $logStr, $mpPercentThreshold, $maxReceiveChannels) = @_;

    $$logStr .= "In createStandardRestoreDTFile\n";

    #check if able to reuse existing restore file with simple modification
    if (!defined $self->{canReuseRestoreDTFile})
    {
        $self->{canReuseRestoreDTFile} = canReuseRestoreDTFile($self, $logStr, $mpPercentThreshold);

        if (!defined $self->{canReuseRestoreDTFile})
        {
            # $logStr is already filled out
            return 0;
        }
    }

    if ($self->{canReuseRestoreDTFile})
    {
        $$logStr .= "Reusing source restore datatemp file\n";
        if (!reuseRestoreDTFile($self, $logStr, $maxReceiveChannels))
        {
            # logStr is already filled out
            return 0;
        } 
    }
    else
    {
        $$logStr .= "Building new file\n";
        if (!createNewRestoreDTFile($self, $logStr, $mpPercentThreshold, $maxReceiveChannels))
        {
            # logStr is already filled out
            return 0;
        }
    }

    $$logStr .= "Create restore data temp file successful\n";
    return 1;
}

##############################################################################
#
#  Function: runRestoreDTFile
#      Run RestoreDataTempFiles_<targetSid>.rcv via rman.  
#
#  Args:
#
# Pre-requisites:
#     RestoreDataTempFiles_<targetSid>.rcv exists
#
##############################################################################
sub runRestoreDTFile
{
    my ($self, $logStr, $refQueueDefInst, $logObj) = @_;

    $$logStr .= "In runRestoreDTFile\n";

    if (!defined $self->{restoreDTFile})
    {
        # This is a rerun.  Try to find the restore datatemp file. 

        my $filename =  "$self->{workDir}/RestoreDataTempFiles_$self->{targetSid}.rcv";
        if (-e $filename)   
        {
            $self->{restoreDTFile} = $filename;
        }
        else
        {
            $$logStr .= "Cannot find $filename.  Either createRestoreDTFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
    }

    $$logStr .= "Restore DataTemp file is $self->{restoreDTFile}\n";

    my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRRestoreDT_" . getDateTimeStr() . ".log";

    my $cmd = "$runRman \"target=/ nocatalog\" \@$self->{restoreDTFile} $logFile";

    $$logStr .= "About to run $cmd\n";

    if (!open (RMAN_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }
   
    # monitor the log after sleep to give it a chance to start
  
    sleep 3;
    if (!processRestoreDTLog($self, $logStr, $logFile, $refQueueDefInst, $logObj))
    {
        #$$logStr will be set
        return 0;
    }

    close RMAN_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'rman'))
    {
        #$$logStr will be set
        return 0;
    }

    $$logStr .= "rman successful\n";
    return 1;
}

##############################################################################
#
#  Function: processRestoreDTLog
#      Tails given logfile and updates the database.
#
#  Args: logfile - the rman restore data temp file log
#
# Pre-requisites:
#     should be called by runRestoreDTFile 
#
##############################################################################

sub processRestoreDTLog
{
    my ($self, $logStr, $logFile, $refQueueDefInst, $logObj) = @_;
    
    # no need to fork off a thread since the rman command
    # is already running in a separate thread

    my $cmd = "tail -f -n +1 $logFile";

    $$logStr .= "About to run $cmd\n";

    my $pid;
    if (!($pid = open (RMAN_LOG, "$cmd |")))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my %runningFiles;
    my $tmpChannel = 0;

    while (<RMAN_LOG>)
    {
        if (/^Starting restore at \d\d-\w\w\w-\d\d$/)
        {
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Preliminary steps completed and starting restore");
        }
        elsif (/^Finished restore at \d\d-\w\w\w-\d\d$/)
        {
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Finished restore\n");
        }
        elsif (/^Starting recover at \d\d-\w\w\w-\d\d$/)
        {
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Copy and renames completed and starting recover\n");
        }
        elsif (/^Finished recover at \d\d-\w\w\w-\d\d$/)
        {
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Finished recover\n");
        }
        elsif (/^Recovery Manager complete.$/)
        {
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " RMAN complete\n");
            last;
        }
        elsif (/^channel RCHL(\d): restoring archived log$/)
        {
            $runningFiles{$1} = "archived log";
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Restoring on channel $1: archived log\n");
        }
        elsif (/^channel RCHL(\d+): restoring datafile \d+ to .+\/(.+)$/ || 
              /^channel RCHL(\d+): restoring datafile \d+ to (\+.+)$/)
        {
            # newer versions of oracle
            $runningFiles{$1} = $2;
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Restoring on channel $1: $2\n");
            # update db
        }
        elsif (/^restoring datafile \d+ to .+\/(.+)$/)
        {
            # older versions of oracle
            # previous line has the channel number, saved in tmpChannel
            if (!$tmpChannel)
            {
                $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Something went wrong.  Missing channel number.\n");
            }
            $runningFiles{$tmpChannel} = $1;
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Restoring on channel $tmpChannel: $1\n");
            # update db
        }
        elsif (/^channel RCHL(\d+): specifying datafile\(s\) to restore from backup set$/)
        {
            # in some cases, the next line has the filename without the channel,
            # so need to save the channel
            $tmpChannel = $1;
        }
        elsif (/^destination for restore of datafile \d+: .+\/(.+)$/)
        {
            # previous line has the channel number, saved in tmpChannel
            if (!$tmpChannel)
            {
                $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Something went wrong.  Missing channel number.\n");
            }
            $runningFiles{$tmpChannel} = $1;
            $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Recovering on channel $tmpChannel: $1\n");
        }
        elsif (/^channel RCHL(\d+): restore complete, elapsed time: (\d\d:\d\d:\d\d)/)
        {
            if (exists $runningFiles{$1})
            {
               $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Channel $1: $runningFiles{$1} finished\n");
               delete $runningFiles{$1};
            }
            else
            {
                #error
                $logObj->updateLogDetails($refQueueDefInst, getDateTimeStr('L') . " Error:  Can't find $1 in the hash\n");
            }
        }
        elsif (/^Recovery Manager complete.$/)
        {
        last;
        }
    }

    kill (9, $pid);
    
    close RMAN_LOG;
    
    return 1;
}


##############################################################################
#
#  Function: preReqInitOraFile
#      make sure <backupLocation>/<backupSet>_init<sourceSid>.ora exists 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqInitOraFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqInitOraFile\n";

    $$logStr .= "Checking init ora file\n";
    $self->{sourceInitOraFile} = "$self->{backupLocation}/$self->{backupSet}_init$self->{sourceSid}.ora"; 

    my $retVal = 1;

    if (!-e $self->{sourceInitOraFile})
    {
        $$logStr .= "$self->{sourceInitOraFile} does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Found init ora files\n" if ($retVal);

    return $retVal;
}

##############################################################################
#
#  Function: preReqInitOraLocalFile
#      make sure $ENV{ORACLE_HOME}/dbs/init.eagle exists
#
#  Args:
#
#  Pre-requisites:
#
##############################################################################

sub preReqInitOraLocalFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqInitOraFile\n";

    $$logStr .= "Checking init ora file\n";
    $self->{sourceInitOraFile} = "$ENV{ORACLE_HOME}/dbs/init.eagle";

    my $retVal = 1;

    if (!-e $self->{sourceInitOraFile})
    {
        $$logStr .= "$self->{sourceInitOraFile} does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Found init ora files\n It is located under next location:\t$self->{sourceInitOraFile}\n" if ($retVal);

    return $retVal;
}

##############################################################################
#
#  Function: createInitOraFile
#      Create init<targetSid>.ora.  Will also make any missing directories
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/<backupSet>_init<sourceSid>.ora exists
#
##############################################################################
sub createInitOraFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createInitOraFile\n";

    if (!defined $self->{sourceInitOraFile})
    {
        # preReqRInitOraFile has not been run
        $self->{sourceInitOraFile} = "$self->{backupLocation}/$self->{backupSet}_init$self->{sourceSid}.ora";
    }

    $$logStr .= "Source init ora file is $self->{sourceInitOraFile}\n";

    $self->{initOraFile} = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

    $$logStr .= "Target init ora file is $self->{initOraFile}\n";

    # backup original file first if non ASM

    if (!$self->{isASM})
    {

        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRCreatePfileSql_" . getDateTimeStr() . ".log";

        if (!runSqlAsSysdba($logStr, "CREATE pfile FROM spfile", $logFile))
        {
            #logStr will already be set
            return 0;
        }

        my $backupFile = "$self->{workDir}/$self->{refreshQueueInst}" . "_init$self->{targetSid}.ora_backup";
        $$logStr .= "Moving original init ora file to $backupFile\n";
        if (!move ($self->{initOraFile}, $backupFile))
        {
            $logStr .= "Cannot move $self->{initOraFile}: $!\n";
            return 0;
        }
    }

    my $retVal = 0;
    if ($self->{isASM})
    {
        $retVal = createASMInitOraFile($self, $logStr);
    }
    else
    {
        $retVal = createStandardInitOraFile($self, $logStr);
    }

    return $retVal;
}

sub createASMInitOraFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createASMInitOraFile\n";

    if (!open (SOURCEINIT_FH, "< $self->{sourceInitOraFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceInitOraFile}: $!\n";
        return 0;
    }

    if (!open(TARGETINIT_FH, "> $self->{initOraFile}"))
    {
        $$logStr .=  "Cannot open $self->{initOraFile}: $!\n";
        return 0;
    }

    # ASM TODO: Make sure pass by reference is working
    my @sourceLines = <SOURCEINIT_FH>;

    if (!removeInitOraLines($self, $logStr, \@sourceLines))
    {
        #logstr will be filled int
        return 0;
    }

    $$logStr .= "\nChanging $self->{sourceSid} to $self->{targetSid} for all lines but the ones that have db_name or instance_name\n";

    # for some reason the characters in the array are not allowing
    # foreach to work properly.  Need to use an index
    for (my $i = 0; $i < @sourceLines; $i++)
    {
        # strip out comments
        $sourceLines[$i] =~ s/#.*$//;

        # leave source_sid for db_name and instance_name.  change
        # all other source sids to be the target sid

        if ($sourceLines[$i] !~ /^\*.db_name=/ && $sourceLines[$i] !~ /^\*.instance_name/)
        {
            $sourceLines[$i] =~ s/$self->{sourceSid}/$self->{targetSid}/g;
        }
        print TARGETINIT_FH $sourceLines[$i];
    }

    $$logStr .= "Adding cursor_sharing as EXACT\n";
    print TARGETINIT_FH "*.cursor_sharing='EXACT'\n";
    $$logStr .= "Adding db_unique_name as '$self->{targetSid}'\n";
    print TARGETINIT_FH "*.db_unique_name='$self->{targetSid}'\n";

    close SOURCEINIT_FH;
    close TARGETINIT_FH;

    $$logStr .= "Create init ora file successful\n";
    return 1;

}

##############################################################################
##
##  Function: createInitOraFileLUN2ASM
##      Create init<targetSid>.ora for LUN2ASM refresh type.  Will also make any missing directories
##
##  Args:
##
## Pre-requisites:
##     <backupLocation>/<backupSet>_init<sourceSid>.ora exists
##
###############################################################################
sub createInitOraFileLUN2ASM
{
    my ($self, $logStr, $LUN2ASMdbRecoveryFileDestSize) = @_;

    $$logStr .= "In createInitOraFileLUN2ASM\n";

    if (!defined $self->{sourceInitOraFile})
    {
        # preReqRInitOraFile has not been run
        $self->{sourceInitOraFile} = "$self->{backupLocation}/$self->{backupSet}_init$self->{sourceSid}.ora";
    }

    $$logStr .= "Source init ora file is $self->{sourceInitOraFile}\n";

    $self->{initOraFile} = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

    $$logStr .= "Target init ora file is $self->{initOraFile}\n";

    if (!open (SOURCEINIT_FH, "< $self->{sourceInitOraFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceInitOraFile}: $!\n";
        return 0;
    }

    if (!open(TARGETINIT_FH, "> $self->{initOraFile}"))
    {
        $$logStr .=  "Cannot open $self->{initOraFile}: $!\n";
        return 0;
    }

    # ASM TODO: Make sure pass by reference is working
    my @sourceLines = <SOURCEINIT_FH>;
    $$logStr .= "For LUN2ASM refresh removing defined lines below from InitOra file\n";

    my @toTakeOut;
    @toTakeOut = ('fal_client', 'fal_server', 'log_archive_dest_2', 'log_archive_dest_state_2',
                  'log_archive_dest_state_1','log_archive_dest_state_2','log_archive_format','diagnostic_dest');
    
    $$logStr .= "Taking out the lines with the following,\n";
    # take out lines
    foreach my $keyWord (@toTakeOut)
    {
        $$logStr .= "$keyWord ";
        @sourceLines = grep {!/$keyWord/} @sourceLines
    }

    $$logStr .= "Finished removing lines\n";

    $$logStr .= "\tModifying next lines:\n";
    $$logStr .= "\nChanging $self->{sourceSid} to $self->{targetSid} for all lines but the ones that have db_name or instance_name\n";

    # for some reason the characters in the array are not allowing
    # foreach to work properly.  Need to use an index
    for (my $i = 0; $i < @sourceLines; $i++)
    {
        # strip out comments
        $sourceLines[$i] =~ s/#.*$//;

        if ($sourceLines[$i] !~ /^\*.db_name=/)
        {
            $sourceLines[$i] =~ s/$self->{sourceSid}/$self->{targetSid}/g;
        }

        if ($sourceLines[$i] =~ /log_archive_dest_1/)
        {
            $sourceLines[$i] =~ s/log_archive_dest_1.*$/log_archive_dest_1=\'LOCATION=USE_DB_RECOVERY_FILE_DEST\'/;
            $$logStr .= " \tlog_archive_dest_1 location to *.log_archive_dest_1='LOCATION=USE_DB_RECOVERY_FILE_DEST'\n";
        }

        elsif ($sourceLines[$i] =~ /audit_file_dest/)
        {
           $sourceLines[$i] =~ s/audit_file_dest.*$/audit_file_dest=\'$ENV{ORACLE_BASE}\/admin\/$self->{targetSid}\'/;
            $$logStr .= " \taudit_file_dest to '$ENV{ORACLE_BASE}\/admin\/$self->{targetSid}'\n";
        }

        elsif ($sourceLines[$i] =~ /control_files/)
        {
            $sourceLines[$i] =~ s/control_files.*$/control_files=\'+DATA\/$self->{targetSid}\/controlfile\/control01.ctl\'\,\'+FRA\/$self->{targetSid}\/controlfile\/control02\.ctl\'/;
            $$logStr .= " \tcontrol_files location to '+DATA\/$self->{targetSid}\/controlfile\/control01\.ctl\','+FRA\/$self->{targetSid}\/controlfile/control02.ctl'\n";
        }

        elsif ($sourceLines[$i] =~ /utl_file_dir/)
        {
            $sourceLines[$i] =~ s/utl_file_dir.*$/utl_file_dir=\'$ENV{ORACLE_BASE}\/audit_$self->{targetSid}\'/;
            $$logStr .= " \tutl_file_dir location to '$ENV{ORACLE_BASE}\/audit_$self->{targetSid}'\n";
        }


        print TARGETINIT_FH $sourceLines[$i];
    }

    $$logStr .= "Adding db_create_online_log_dest_1 as '+RED'\n";
    print TARGETINIT_FH "*.db_create_online_log_dest_1='+RED'\n";
    $$logStr .= "Adding db_create_online_log_dest_2 as '+FRA'\n";
    print TARGETINIT_FH "*.db_create_online_log_dest_2='+FRA'\n";
    $$logStr .= "Adding db_unique_name as '$self->{targetSid}'\n";
    print TARGETINIT_FH "*.db_unique_name='$self->{targetSid}'\n";
    $$logStr .= "Adding db_recovery_file_dest as '+FRA'\n";
    print TARGETINIT_FH "*.db_recovery_file_dest='+FRA'\n";
    $$logStr .= "Adding db_create_file_dest as '+DATA'\n";
    print TARGETINIT_FH "*.db_create_file_dest='+DATA'\n";
    $$logStr .= "Adding db_recovery_file_dest_size as $LUN2ASMdbRecoveryFileDestSize\n";
    print TARGETINIT_FH "*.db_recovery_file_dest_size=$LUN2ASMdbRecoveryFileDestSize\n";
    $$logStr .= "Adding *.service_names as $self->{targetSid}.eagleaccess.com\n";
    print TARGETINIT_FH "*.service_names='$self->{targetSid}.eagleaccess.com'\n";
    close SOURCEINIT_FH;
    close TARGETINIT_FH;

    $$logStr .= "Create init ora file successful\n";


    return 1;
}

sub removeInitOraLines
{
    my ($self, $logStr, $sourceLines) = @_;

    $$logStr .= "In removeInitOraLines\n";

    my @toTakeOut;
    if ($self->{isASM})
    {
	@toTakeOut = ('fal_client', 'fal_server', 'standby_archive_dest', 'standby_file_management',
	              'log_archive_dest_2', 'log_archive_dest_state_2', 'log_archive_dest_1',
       		      'db_flashback_retention_target','cursor_sharing');
    }
    else
    {
	@toTakeOut = ('fal_client', 'fal_server', 'standby_archive_dest', 'standby_file_management',
		      'log_archive_dest_2', 'log_archive_dest_state_2', 'log_archive_dest_1',
		      'db_flashback_retention_target', 'db_recovery_file_dest_size',
		      'db_recovery_file_dest','cursor_sharing');
    }
	
    $$logStr .= "Taking out the lines with the following,\n";

    # take out lines
    foreach my $keyWord (@toTakeOut)
    {
        $$logStr .= "$keyWord ";
        @$sourceLines = grep {!/$keyWord/} @$sourceLines
    }

    $$logStr .= "Finished removing lines\n";
    return 1;
}

sub createStandardInitOraFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createStandardInitOraFile\n";

    if (!defined $self->{matchingLunHash})
    {
        # TODO, put in a new function to fill in the hash, for now just call
        # canReuseRestoreDTFile
        my $tmpLogStr;
        if (!defined canReuseRestoreDTFile($self, \$tmpLogStr, 100))
        {
            $$logStr .= "Called canReuseRestoreDTFile to fill in necessary data and got error\n";
            $$logStr .= $tmpLogStr; 
            return 0;
        }
    }

    if (!open (SOURCEINIT_FH, "< $self->{sourceInitOraFile}"))
    { 
        $$logStr .= "Cannot open $self->{sourceInitOraFile}: $!\n";
        return 0;
    }

    if (!open(TARGETINIT_FH, "> $self->{initOraFile}"))
    {
        $$logStr .=  "Cannot open $self->{initOraFile}: $!\n";
        return 0;
    }

    my @sourceLines = <SOURCEINIT_FH>;

    if (!removeInitOraLines($self, $logStr, \@sourceLines))
    {
        #logstr will be filled int
        return 0;
    }

    $$logStr .= "\nChanging $self->{sourceSid} to $self->{targetSid} for all lines but the ones that have db_name or instance_name\n";

    # for some reason the characters in the array are not allowing
    # foreach to work properly.  Need to use an index
    for (my $i = 0; $i < @sourceLines; $i++)
    {

        # strip out comments
        $sourceLines[$i] =~ s/#.*$//;

        # leave source_sid for db_name and instance_name.  change
        # all other source sids to be the target sid

        if ($sourceLines[$i] !~ /^\*.db_name=/ && $sourceLines[$i] !~ /^\*.instance_name/)
        {
            # First handle entire lun string
            if ( $sourceLines[$i] =~ /$self->{sourceSid}-(\d\d)/)
            {
                my $lunNumber = $1;
                $sourceLines[$i] =~ s/$self->{sourceSid}-\d\d/$self->{matchingLunHash}{$lunNumber}/g;
            }
  
            # Catch any non lun strings 
            $sourceLines[$i] =~ s/$self->{sourceSid}/$self->{targetSid}/g;
        }

        # make any directories at this point
        if ($sourceLines[$i] =~ /=('\/.+)/)
        {
            # found a directory in the line.  There could be multiple
            # in the line that are comma delimited

            my @dirs = split(',', $1);
            foreach my $dir (@dirs)
            {
                # strip out quotes
                $dir =~ s/\'//g;

                # strip out end filename if it needed
                if ($dir =~ /\./)
                {
                    $dir = dirname($dir);
                }
               
                if (!-d $dir)
                {
                    $$logStr .= "$dir does not exist, creating it\n";

                    eval {mkpath($dir)};
                    if ($@)
                    {
                        $$logStr .= "Cannot make directory $dir: $@\n";
                        return 0;
                    }
                }
            }
        }
       
        print TARGETINIT_FH "$sourceLines[$i]"; 
    }

    if (!defined $self->{archiveLogDest})
    {
        $self->{archiveLogDest} = getArchiveLogDest($self, $logStr, 'T');
        if (!defined $self->{archiveLogDest})
        {
            #$logStr will already be filled out
            return 0;
        }
    }

    my $archiveLogDestDir = "/" . "$self->{archiveLogDest}/oradata/arch";

    $$logStr .= "Adding log_archive_dest as $archiveLogDestDir\n";
    print TARGETINIT_FH "*.log_archive_dest_1='LOCATION=$archiveLogDestDir'\n";

    # make the archive log destination now if it doesn't exist
    if (! -d $archiveLogDestDir)
    {
        $$logStr .= "$archiveLogDestDir does not exist, creating it\n";

        eval {mkpath($archiveLogDestDir)};
        if ($@)
        {
            $$logStr .= "Cannot make directory $archiveLogDestDir: $@\n";
            return 0;
        }
    }

    $$logStr .= "Adding cursor_sharing as EXACT\n";
    print TARGETINIT_FH "*.cursor_sharing='EXACT'\n";
  
    my $sameHost = isSourceAndTargetHostSame($self, $logStr);
    if (!defined $sameHost)
    {
        # $logStr already filled in
        return 0;
    }   

    if ($sameHost)
    {
        $$logStr .= "The source and target are on the same host so adding db_unique_name\n";
        print TARGETINIT_FH "db_unique_name='$self->{targetSid}'\n"; 
    }
 
    close SOURCEINIT_FH;
    close TARGETINIT_FH;

    $$logStr .= "Create init ora file successful\n";
    return 1;
}

##############################################################################
#
#  Function: copyInitOraLocalFile
#      Copy content of local Init Ora File $ENV{ORACLE_HOME}/dbs/init.eagle 
#      to $ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora
#
#  Args:
#
# Pre-requisites:
#    $ENV{ORACLE_HOME}/dbs/init.eagle exists 
#
##############################################################################
sub copyInitOraLocalFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createInitOraFile\n";

    if (!defined $self->{sourceInitOraFile})
    {
        # preReqRInitOraFile has not been run
        $self->{sourceInitOraFile} = "$ENV{ORACLE_HOME}/dbs/init.eagle";
    }
    $$logStr .= "Source init ora file is $self->{sourceInitOraFile}\n";

    $self->{initOraFile} = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

    $$logStr .= "Target init ora file is $self->{initOraFile}\n";

    if (!copy($self->{sourceInitOraFile},$self->{initOraFile}))
    {
        $$logStr .= "Copy of file $self->{sourceInitOraFile} failed: $!\n";
        return 0;
    }

    $$logStr .= "Create init ora file on target is successful\n";
    return 1;
}


##############################################################################
#
#  Function: updateInitOraFileSid
#      Update the sid in init<targetSid>.ora to be the target 
#
#  Args:
#
# Pre-requisites:
#     init<targetSid>.ora exists
#
##############################################################################
sub updateInitOraFileSid
{
    my ($self, $logStr) = @_;

    $$logStr .= "In updateInitOraFileSid\n";

    if (!defined $self->{initOraFile})
    {
        # This is a rerun.  Try to find the init ora file.

        my $filename = "$ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora";

        if (-e $filename)
        {
            $self->{initOraFile} = $filename;
        }
        else
        {
            $$logStr .= "Cannot find $filename.  Either createInitOraFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
    }

    $$logStr .= "Init ora file is $self->{initOraFile}\n";

    my $tempInitOraFile = "$self->{initOraFile}_temp_$$";
    if (!move ($self->{initOraFile}, $tempInitOraFile))
    {
        $$logStr .= "Cannot move $self->{initOraFile}: $!\n";
        return 0;
    }

    if (!open (INITORA_FH, "> $self->{initOraFile}"))
    {
        $$logStr .= "Cannot open $self->{initOraFile}: $!\n";
        return 0;
    }

    if (!open (TEMP_FH, "< $tempInitOraFile"))
    {
        $$logStr .= "Cannot open $tempInitOraFile: $!\n";
        return 0;
    }

    # change sourceSid to targetSid.  Should be db_name and instance_name.
    # take out db_unique_name if there is one

    $$logStr .= "About to modify init ora file\n";
   
    while (my $line = <TEMP_FH>)
    {
        next if ($line =~ /^db_unique_name/);

        $line =~ s/$self->{sourceSid}/$self->{targetSid}/;
        print INITORA_FH "$line";
    } 

    close INITORA_FH;
    close TEMP_FH;

    unlink $tempInitOraFile; 

    $$logStr .= "Updating init ora file successful\n";
    return 1;
}

##############################################################################
#
#  Function: preReqControlFile
#      make sure <backupLocation>/Restore*ControlFile_<sourceSid>_<backupSet>.rcv 
#      and <backupLocation>/cf_<backupSet>*_full_ctrl exists
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqControlFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqControlFile\n";
    $$logStr .= "Checking control file\n";

    # need to get the exact filename because open won't work with a wildcard
    $self->{sourceControlFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*ControlFile_" . $self->{backupSet} . ".rcv"); 

    my $retVal = 1;

    # getFileName returns 0 if the file does not exist
    if (!$self->{sourceControlFile})
    {
        $$logStr .= "$self->{backupLocation}/Restore\*ControlFile_" . $self->{backupSet} . ".rcv does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Checking full control file\n";
    $self->{sourceFullControlFile} = getFileName($logStr, "$self->{backupLocation}/cf_" . $self->{backupSet} . "\*_full_ctrl");

    # getFileName returns 0 if the file does not exist
    if (!$self->{sourceFullControlFile})
    {
        $$logStr .= "$self->{backupLocation}/cf_" . $self->{backupSet} . "\*_full_ctrl does not exist\n";
        $retVal = 0;
    }

    $$logStr .= "Found LOG.TXT and Restore DataTemp files\n" if ($retVal);

    return $retVal;

}

##############################################################################
#
#  Function: createControlFile
#      Modify Source Restore Control file to create <sourceSid>_specific_cf.rcv.
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*ControlFile_<sourceSid>_<backupSet>.rcv and 
#     <backupLocation>/cf_<backupSet>*_full_ctrl exists
#
##############################################################################

sub createControlFile 
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createControlFile\n";

    if (!defined $self->{sourceControlFile})
    {
        # preReqControlFile has not been run
        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceControlFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*ControlFile_" . $self->{backupSet} . ".rcv");

    }

    $$logStr .= "Source control file is $self->{sourceControlFile}\n";

    $self->{controlFile} = "$self->{workDir}/$self->{sourceSid}_specific_cf.rcv";

    $$logStr .= "Target control file is $self->{controlFile}\n";

    if (!open (SOURCECONTROL_FH, "< $self->{sourceControlFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceControlFile}: $!\n";
        return 0;
    }

    if (!open (TARGETCONTROL_FH, "> $self->{controlFile}"))
    {
        $$logStr .= "Cannot open $self->{controlFile}: $!\n";
        return 0;
    }

    my @sourceLines = <SOURCECONTROL_FH>;
    my $dbidLine = $sourceLines[0];
    close SOURCERESTORE_FH;

    $$logStr .= "dbid line is $dbidLine\n";
    if (!defined $self->{sourceFullControlFile})
    {
        # preReqControlFile has not been run
        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceFullControlFile} = getFileName($logStr, "$self->{backupLocation}/cf_" . $self->{backupSet} . "\*_full_ctrl");

    }
    $$logStr .= "Full source control file is $self->{sourceFullControlFile}\n";

    $$logStr .= "Creating target control file\n";

    print TARGETCONTROL_FH "$dbidLine\nrun \{\n";
    print TARGETCONTROL_FH "        restore controlfile from '";
    print TARGETCONTROL_FH "$self->{sourceFullControlFile}\'\;\n";
    print TARGETCONTROL_FH "        alter database mount\;\n";
    print TARGETCONTROL_FH "        sql \'alter database flashback off\'\;\n\}\n";
    close TARGETRESTORE_FH;

    $$logStr .= "Create target control file successful\n";

    return 1;
}

##############################################################################
#
#  Function: runControlFile
#      Run <sourceSid>_specific_cf.rcv via rman.  
#
#  Args:
#
# Pre-requisites:
#     <sourceSid>_specific_cf.rcv exists
#
##############################################################################

sub runControlFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In runControlFile\n";

    if (!defined $self->{controlFile})
    {
        # This is a rerun.  Try to find the control file. 

        my $filename =  "$self->{workDir}/$self->{sourceSid}_specific_cf.rcv";
        if (-e $filename)   
        {
            $self->{controlFile} = $filename;
        }
        else
        {
            $$logStr .= "Cannot find $filename.  Either createControlFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
    }

    $$logStr .= "Control file is $self->{controlFile}\n";

    my $runRman = "$ENV{ADR_LOCATION}/runRman.ksh";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRControlRman_" . getDateTimeStr() . ".log";

    my $cmd = "$runRman \"target=/ nocatalog\" \@$self->{controlFile} $logFile";

    $$logStr .= "About to run $cmd\n";

    if (!open (RMAN_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <RMAN_PIPE>;
    close RMAN_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'rman'))
    {
        #$$logStr will be set
        return 0;
    }

    $$logStr .= "rman successful\n";
    return 1;
}

##############################################################################
#
#  Function: reuseRestoreRedoFile
#      Modify Source Restore Redo file to create run_rename_redo.rcv.  This is 
#      for mount points and sizes being the same between the source and the 
#      target
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#
##############################################################################

sub reuseRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In reuseRestoreRedoFile\n";

    if (!defined $self->{matchingLunHash})
    {
        my $tmpLogStr;
        if (!defined canReuseRestoreDTFile($self, \$tmpLogStr, 100))
        {
            $$logStr .= "Called canReuseRestoreDTFile to fill in necessary data and got error\n";
            $$logStr .= $tmpLogStr;
            return 0;
        }
    }

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreRedoFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{restoreRedoFile}: $!\n";
        return 0;
    }

    $$logStr .= "Replacing xxxxxx with $self->{targetSid}\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        # strip out blank lines
        next if ($line =~ /^\s*$/);

        if ($line =~ /xxxxxx-(\d\d)/)
        {
            # replace with the matching target lun
            my $lunNumber = $1;
            $line =~ s/xxxxxx-\d\d/$self->{matchingLunHash}{$lunNumber}/;

            # make the directory if it does not exist

            $line =~ / TO \'(.+)\/redo/;

            my $targetDir = $1;
            if (!-d $targetDir)
            {
                $$logStr .= "$targetDir does not exist, creating it\n";

                eval {mkpath($targetDir)};
                if ($@)
                {
                    $$logStr .= "Cannot make directory $targetDir: $@\n";
                    return 0;
                }
            }
 
        }
        print TARGETRESTORE_FH "$line";
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;
    $$logStr .= "Create restore redo file successful\n";
    return 1;

}

##############################################################################
#
#  Function: reuseRestoreDTFile
#      Modify Source Restore file.  This is for mount points and sizes being
#      the same between the source and the target 
#
#  Args:
#      maxReceiveChannels - maximum receive channels allowed.
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv exists
#     And the mount points and sizes are the same for the source and the target
#
##############################################################################

sub reuseRestoreDTFile
{
    my ($self, $logStr, $maxReceiveChannels) = @_;

    $$logStr .= "In reuseRestoreDTFile\n";

    if (!defined $self->{sourceRestoreDTFile})
    {
        # preReqRestoreDTFile has not been run

        # need to get the exact filename because open won't work with a wildcard
        $self->{sourceRestoreDTFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv");
    }

    $$logStr .= "Source restore datatemp file is $self->{sourceRestoreDTFile}\n";

    $self->{restoreDTFile} = "$self->{workDir}/RestoreDataTempFiles_" . $self->{targetSid} . ".rcv";

    $$logStr .= "Target restore redo file is $self->{restoreDTFile}\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreDTFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreDTFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreDTFile}"))
    {
        $logStr .= "Cannot open $self->{restoreDTFile}: $!\n";
        return 0;
    }

    my $printAllocateChannel = 0;

    $$logStr .= "Setting receive channels and replacing xxxxxx with $self->{targetSid}\n";
    while (my $line = <SOURCERESTORE_FH>)
    {
        if ($line =~ /^(\s*allocate channel \'RCHL)\d+(\' device type disk\;)/)
        {
            if (!$printAllocateChannel)
            {
                # this is the first allocate channel line.
                # print allocate channel lines all at once and ignore
                # subsequent allocate lines

                $printAllocateChannel = 1;

                my $recChannels = getReceiveChannels($self, $logStr, $maxReceiveChannels);
                if (!defined $recChannels)
                {
                    #logStr is alreay filled out
                    return 0;
                }

                for (my $i = 1; $i <= $recChannels; $i++)
                {
                     print TARGETRESTORE_FH $1 . $i . $2 . "\n";
                }
            }
        }
        else
        {
            if ($line =~ /xxxxxx-(\d\d)/)
            {
                # replace with the matching target lun
                my $lunNumber = $1;

                if (defined $self->{matchingLunHash}{$lunNumber})
                { 
                    $line =~ s/xxxxxx-\d\d/$self->{matchingLunHash}{$lunNumber}/;
                }
                else 
                {
                    # something went wrong
                    $$logStr .= "ERROR:  Cannot resolve target lun in line, $line\n";
                    return 0;
                }
            }

            print TARGETRESTORE_FH "$line";
        }
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;

    $$logStr .= "Create restore datatemp file successful\n";

    return 1;
}

##############################################################################
#
#  Function: createNewRestoreRedoFile
#      Creates run_rename_redo.rcv putting the redo logs on the first two
#      luns
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#
##############################################################################

sub createNewRestoreRedoFile
{
    my ($self, $logStr, $mpPercentThreshold, $maxReceiveChannels) = @_;

    $$logStr .= "In createNewRestoreDTFile\n";

    if (!defined $self->{lunSizeHash})
    {
        # TODO, put in a new function to fill in the hash, for now just call
        # canReuseRestoreDTFile
        my $tmpLogStr;
        if (!defined canReuseRestoreDTFile($self, \$tmpLogStr, 100))
        {
            $$logStr .= "Called canReuseRestoreDTFile to fill in necessary data and got error\n";
            $$logStr .= $tmpLogStr;
            return 0;
        }

    }

    if (!defined $self->{archiveLogDest})
    {
        $self->{archiveLogDest} = getArchiveLogDest($self, $logStr, 'T');
        if (!defined $self->{archiveLogDest})
        {
            #$logStr will already be filled out
            return 0;
        }
    }

    # sort by alphabetical order
    my @luns = sort {$a cmp $b} keys %{$self->{lunSizeHash}};

    $$logStr .= "Getting first two luns\n";
    # get the first two non archive log destinations

    my @redoLogLuns;
    my $j = 0;

    for (my $i = 0; $i < @luns; $i ++)
    {
        if ($j == 2)
        {
            last;
        }

        if ($luns[$i] eq $self->{archiveLogDest})
        {
            $$logStr .= "Not using $luns[$i] because it is the archive log destination lun\n";
            next;
        } 

        my $lunNumber = $j + 1;
        $$logStr .= "Using $luns[$i] for lun number $lunNumber\n";

        $redoLogLuns[$j] = $luns[$i];
        $j++; 
    }

    if ($j != 2)
    {
        # this shouldn't happen
        $$logStr .= "Could not find first to luns\n";
        return 0;
    }

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreRedoFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{restoreRedoFile}: $!\n";
        return 0;
    }

    $$logStr .= "Building restore redo file\n";
    
    $j = 0;   
    while (my $line = <SOURCERESTORE_FH>)
    {

        # strip out blank lines
        next if ($line =~ /^\s*$/);     
 
        if ($line =~ /^ALTER DATABASE RENAME FILE \'(.+\/)(redo.+\.log)\' TO .+/)
        {
            my $sourceDir = $1;
            my $redoLog = $2;
    
            my $targetDir = "\/$redoLogLuns[$j]\/oradata\/redologs\/";

            if (!-d $targetDir)
            {
                $$logStr .= "$targetDir does not exist, creating it\n";

                eval {mkpath($targetDir)};
                if ($@)
                {
                    $$logStr .= "Cannot make directory $targetDir: $@\n";
                    return 0;
                }
            }

            $line = "ALTER DATABASE RENAME FILE \'$sourceDir" . "$redoLog\' TO \'$targetDir" . "$redoLog\'\;\n"; 
        }
        else
        {
            $$logStr .= "Could not parse $line\n";
            return 0;
        }

        if ($j == 0)
        {
            $j = 1;
        }
        else
        {
            $j = 0;
        }
 
        print TARGETRESTORE_FH "$line";
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;

    $$logStr .= "Building restore redo file successful\n";
    return 1;
}

##############################################################################
#
#  Function: createNewRestoreDTFile
#      Create RestoreDataTempFiles_<targetSid>.rcv based on mount points and file
#      sizes.  This is used only if the source and target have different mount 
#      points and/or sizes
#
#  Args:
#      mpPercentThreshold - percent of the amount of space that cannot be
#                           exceeded for each mount point
#      maxReceiveChannels - maximum receive channels allowed.
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
#     <sourceSid>_<backupSet>_LOG.TXT exist
#
##############################################################################

sub createNewRestoreDTFile
{
    my ($self, $logStr, $mpPercentThreshold, $maxReceiveChannels) = @_;

    $$logStr .= "In createNewRestoreDTFile\n";

    # %fileSize will hold sizes for the filename key

    if (!setFileInfo($self, $logStr))
    {
        # logStr already set
        return 0;
    }

    if (!rmArchLogLunFromHash($self, $logStr))
    {
        # logStr already set
        return 0;
    }

    if (!setDataFileLunHash($self, $logStr, $mpPercentThreshold))
    {
        # logStr already set
        return 0;
    }

    return buildRestoreDTFile($self, $logStr, $maxReceiveChannels);
}

##############################################################################
#
#  Function: setFileSizeHash
#      Sets the fileSizeHash
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/<sourceSid>_<backupSet>_LOG.TXT exists
#
##############################################################################

sub setFileInfo
{   
    my ($self, $logStr) = @_;

     $$logStr .= "In setFileInfo\n";

    # Get the list of files and sizes from the log file.  This is listed in 
    # the LOG.TXT file.  
    #
    # List of Permanent Datafiles
    # ===========================
    # File Size(MB) Tablespace           RB segs Datafile Name
    # ---- -------- -------------------- ------- ------------------------
    # 1    3036     SYSTEM               YES     /utpdev2-02/oradata/system01.dbf
    # The above should be read until the first blank line.  This is repeated for the 
    # temporary files.

    my @fileTxt = (qr/^List of Permanent Datafiles$/,
                   qr/^===========================$/,
                   qr/^File\s+Size\(MB\)\s+Tablespace\s+RB\s+segs\s+Datafile\s+Name$/,
                   qr/^----\s+--------\s+--------------------\s+-------\s+------------------------$/,
                   qr/NOPARSE/);  #This is needed so it doens't match blank lines

    if (!defined $self->{sourceRestoreDTLogFile})
    {
        # preReqRestoreDTFile has not been run
        $self->{sourceRestoreDTLogFile} = "$self->{backupLocation}/$self->{backupSet}" . "_LOG.TXT";
    }

    $$logStr .= "Getting files and sizes from $self->{sourceRestoreDTLogFile}\n";

    if (!open (SOURCELOG_FH, "< $self->{sourceRestoreDTLogFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreDTLogFile}: $!\n";
        return 0;
    }

    my $foundIdx = 0;
    #my $totalSize = 0;

    while (<SOURCELOG_FH>)
    {
       # have not complete the datafile parsing yet

        if (/^$fileTxt[$foundIdx]$/) 
        {
            $foundIdx++;
        }
        elsif ($foundIdx == @fileTxt - 1)
        {
            # Start parsing here.  Have already read leading lines 
            if (/^\d+\s+(\d+)\s+\S+\s+\w+\s+(.+)$/)
            {
                # strip out mountpoint, /utpdev2-02/oradata/system01.dbf becomes
                # oradata/system01.dbf
                my $size = $1;
                my $fileName = $2;
                $fileName =~ s/^\/\w{6}\d-\d{2}\///;

                #$totalSize += $size;

                # setting key to filename and value to size
                if (exists $self->{fileSizeHash}{$fileName})
                {
                    $$logStr .= "ERROR: Duplicate filename, $fileName\n"; 
                    return 0;
                }
     
                $self->{fileSizeHash}{$fileName} = $size; 
            }
            elsif (/^$/)
            {
                last;
            }
            else
            {
                $$logStr .= "Parsing of $self->{sourceRestoreDTLogFile} failed.   Line is $_\n";
                return 0;
            }
        }
        elsif ($foundIdx)
        {
             $$logStr .= "Found $_, but expect next line to be, $fileTxt[$foundIdx]\n";
             return 0;    
        }
    }
    
    close SOURCELOG_FH;

    $$logStr .= "Getting of files and sizes successful\n";
    #print "total size is $totalSize\n";
    return 1;
}

##############################################################################
# OBSOLETE - now using sized from the db - leave for reference
#  Function: setMpSizeHash
#      Sets the lunSizeHash
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setMpSizeHash
{
    my ($self, $logStr) = @_;

    my $extraFlag = 'P';

    if ($^O ne 'linux')
    {
        #set to unix command
        $extraFlag = 'k';
    }

    if (!defined $self->{archiveLogDest})
    {
        $self->{archiveLogDest} = getArchiveLogDest($self, $logStr, 'T');
        if (!defined $self->{archiveLogDest})
        {
            #$logStr will already be filled out
            return 0;
        }
    }

    my $dfCmd = "df -l$extraFlag |grep $self->{targetSid} |grep -v $self->{archiveLogDest}";

    open (DF_PIPE, "$dfCmd |") || die "Cannot execute $dfCmd\nError is $!";

    use integer;
    while (<DF_PIPE>)
    {
        #if (/^.+\s+(\d+)\s+\d+\s+\d+\s+\d{1,3}\%\s+\/(.+)$/) 
        # if the following is not matched, just throw away.  It means the
        # the targetSid was present, but not for the mount points to consider
        if (/^.+\s+(\d+)\s+\d+\s+\d+\s+\d{1,3}\%\s+\/($self->{targetSid}-\d\d)$/) 
        {
            $self->{lunSizeHash}{$2} = $1 / 1024;
        }
    }
    close DF_PIPE;

    return 1;
}

##############################################################################
#
#  Function: getArchiveLogDestTarget
#      Gets the archive log destination
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub getArchiveLogDestTargetOld
{
    my ($self, $logStr) = @_;

    $$logStr .= "In getArchiveLogDestTarget\n";

    $$logStr .= "About to get archive log destination\n";
    my $sqlStatement=<<EOSql;
SELECT value 
  FROM v\$parameter 
 WHERE name=\'log_archive_dest_1\' 
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return undef;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting archive log destination: $DBI::errstr\n";
        return undef;
    }

    # should only be one
    my $value = $sth->fetchrow_array();

    if (!defined $value)
    {
        $$logStr .= "archive log destination is missing from v\$parameter table\n";
        return undef;
    }

    if ($sth->fetchrow_array)
    {
        $$logStr .= "Expecing 1 row while getting archive log destination but found more\n";
        return undef;
    }
    $sth->finish();

    $value =~ /^LOCATION=\/(.+-\d\d)\/oradata\/arch/i;
   
    my $retVal = $1;

    if (!$retVal)
    {
        $$logStr .= "Cannot extract archive log destination from value returned: $value\n";
        return undef;
    }

    $$logStr .= "archive log destination is $retVal\n";

    return $retVal;
}

##############################################################################
#
#  Function: getArchiveLogDest
#      Gets the archive log destination for the source or target
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
#TODO, how to handle new regions w/ not archive log dest yet?

sub getArchiveLogDest
{
    my ($self, $logStr, $type) = @_;

    $$logStr .= "In getArchiveLogDest for type $type\n";

    $$logStr .= "About to get archive log destination\n";

    my $sqlStatement;

    if ($type eq 'S')
    {
        $sqlStatement=<<EOSql;
SELECT p.pvalue 
  FROM db_parameters p, 
       refresh_queue r
 WHERE r.source_db_instance = p.db_instance
   AND r.instance = $self->{refreshQueueInst}
   AND p.parameter=\'log_archive_dest_1\' 
EOSql

    }
    elsif ($type eq 'T')
    {

        $sqlStatement=<<EOSql;
SELECT p.pvalue
  FROM db_parameters p,
       databases d
 WHERE d.instance = p.db_instance
   AND d.sid = \'$self->{targetSid}\'
   AND p.parameter=\'log_archive_dest_1\'
   AND d.dataguard = \'N\'
EOSql

    }
    else
    {
        $$logStr .= "Unknown type.  Type must be S for source or T for target\n";
        return 0;
    }

    my $sth = $self->{bbDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return undef;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting archive log destination: $DBI::errstr\n";
        return undef;
    }

    # should only be one
    my $value = $sth->fetchrow_array();

    if (!defined $value)
    {
        $$logStr .= "archive log destination is missing from v\$parameter table\n";
        return undef;
    }

    if ($sth->fetchrow_array)
    {
        $$logStr .= "Expecing 1 row while getting archive log destination but found more\n";
        return undef;
    }
    $sth->finish();

    $value =~ /^LOCATION=\/(.+-\d\d)\/oradata\/arch/i;

    my $retVal = $1;

    if (!$retVal)
    {
        $$logStr .= "Cannot extract archive log destination from value returned: $value\n";
        return undef;
    }

    $$logStr .= "archive log destination is $retVal\n";

    return $retVal;
}

##############################################################################
#
#  Function: getReceiveChannels
#      Gets the amount of receive channels based on the number of CPUs/2.
#      Maximum is specified in the config file.
#
#  Args:
#      maxReceiveChannels - maximum receive channels allowed.
#
#  Pre-requisites:
#
##############################################################################

sub getReceiveChannels
{
    my ($self, $logStr, $maxReceiveChannels) = @_;

    $$logStr .= "In getReceiveChannels.  maxReceiveChannels is $maxReceiveChannels\n";

    my $recChannels = 0;
    my $cmd = "grep processor /proc/cpuinfo |wc -l";

    if ($^O ne 'linux')
    {
        #note - tested on unix
        $cmd = "psrinfo -p";   # for unix, will just return amount of cpus
    }    
 
    $$logStr .= "About to run $cmd\n";

    if (!open (CPU_PIPE, "$cmd |"))
    {  
        $$logStr .= "Cannot execute $cmd\nError is $!";
        return undef;
    }
 
    # will only return 1 line
    ($recChannels) = <CPU_PIPE>;

    $$logStr .= "There are $recChannels cpus\n";
    close CPU_PIPE;

    if ($recChannels > $maxReceiveChannels)
    {
        $$logStr .= "$recChannels is greater than $maxReceiveChannels\n";
        $recChannels = $maxReceiveChannels;
    }

    $$logStr .= "returning $recChannels\n";
    return $recChannels;
}

##############################################################################
#
#  Function: setDataFileLunHash
#      Sets the dataFileLunHash and make sure the total file size on each mount 
#      point does not exceed the specified percent (from the config file) of 
#      mount point capacity.
#
#  Args: mpPercentThreshold - percent of the amount of space that cannot be
#                             exceeded for each mount point
#
# Pre-requisites:
#
##############################################################################

sub setDataFileLunHash
{
    my ($self, $logStr, $mpPercentThreshold) = @_;

    $$logStr .= "In setDataFileLunHash\n";

    # go through each and assigh to mp based on size. 
    # $fileSizeHash is a hash refrence of file sizes keyed by file name
    # $lunSizeHash is a hash reference of mount point sizes keyed by mount name
    # $dataFileLunHash is a hash refrence of mount points keyed by file name.  This
    # is the hash to be filled ied.

    # array to hold mps ordered by size.  The first element will be the mount
    # point using the least space.  This is recalculated with each iteration.
    my @mps; 
    my $mpIdx = 0;

    # hash to hold size of files assigned to each mount keyed by mount name
    my %mpUseSize;
    my %mpFreeSize;
    foreach my $mp (keys %{$self->{lunSizeHash}})
    {
        $mpUseSize{$mp} = 0;
        $mpFreeSize{$mp} = $self->{lunSizeHash}{$mp};
    }

    $$logStr .= "Distributing datafiles to luns\n";

    # go through each file from highest size to lowest, distributing in mps 
    foreach my $file (reverse sort {$self->{fileSizeHash}{$a} <=> $self->{fileSizeHash}{$b}} keys %{$self->{fileSizeHash}})
    {
       # reorder array by free size, largest will be first.
       @mps = reverse sort {$mpFreeSize{$a} <=> $mpFreeSize{$b}} keys %mpFreeSize;

       # set the files lun to the one have the least space used up
       $self->{dataFileLunHash}{$file} = $mps[$mpIdx];

       # increment the size used on the mount
       $mpUseSize{$mps[$mpIdx]} += $self->{fileSizeHash}{$file};

       # decrement the free size on the mount
       $mpFreeSize{$mps[$mpIdx]} -= $self->{fileSizeHash}{$file};
      
       #$$logStr .= "$file is $self->{fileSizeHash}{$file} and goes to $mps[$mpIdx]\n";
       #foreach my $mp (@mps)
       #{
           #$$logStr .= "$mp is using $mpUseSize{$mp} and has $mpFreeSize{$mp} free\n";
       #} 

    }

    $$logStr .= "Checking to make sure the total size of all files on each lun does not exceed $mpPercentThreshold \% of the lun\n";

    # check to make sure files are not greater than specified % of the mp size
    foreach my $mp (@mps)
    {
         #print "$mp had $mpUseSize{$mp} used: $self->{lunSizeHash}{$mp}\n";
         $$logStr .= "$mp used: $mpUseSize{$mp} total: $self->{lunSizeHash}{$mp}\n";

         if ($mpUseSize{$mp} * 100 / $self->{lunSizeHash}{$mp} >= $mpPercentThreshold)
         {
            $$logStr .= "$mpUseSize{$mp} is greater than $mpPercentThreshold\% of $self->{lunSizeHash}{$mp}\n";
            return 0;
         }
    }

    $$logStr .= "Distribution of datafiles to luns successful\n";
    return 1;
}

##############################################################################
#
#  Function: buildRestoreDTFile
#      Sets the new RestoreDataTempFiles_<targetSid>.rcv  
#
#  Args:
#      maxReceiveChannels - maximum receive channels allowed.
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv exists 
#
##############################################################################
sub buildRestoreDTFile
{
    my ($self, $logStr, $maxReceiveChannels) = @_;

    $$logStr .= "In buildRestoreDTFile\n";

    if (!defined $self->{sourceRestoreDTFile})
    {
        # preReqRestoreDTFile has not been run
        $self->{sourceRestoreDTFile} = getFileName($logStr, "$self->{backupLocation}/Restore\*DataTempFiles_" . $self->{backupSet} . ".rcv");
    }

    $self->{restoreDTFile} = "$self->{workDir}/RestoreDataTempFiles_" . $self->{targetSid} . ".rcv";
    $$logStr .= "Source restore datatemp file is $self->{sourceRestoreDTFile}\n";
    $$logStr .= "Target restore datatemp file is $self->{restoreDTFile}\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreDTFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreDTFile}: $!\n";
        return 0;
    }

    if (!open (TARGETRESTORE_FH, "> $self->{restoreDTFile}"))
    {
        $$logStr .= "Cannot open $self->{restoreDTFile}: $!\n";
        return 0;
    }

    # size does not need to be considered when assigning temp files, so just
    # go in alphabetical order from largest lun to smallest for assignment
    my $tmpLunIdx = 0;
    my @luns = sort {$a cmp $b} keys %{$self->{lunSizeHash}};

    my $printAllocateChannel = 0;

    $$logStr .= "Building restore datatemp file\n";    
    while (<SOURCERESTORE_FH>)
    {
        if (/^(\s*allocate channel \'RCHL)\d+(\' device type disk\;)/)
        {
            if (!$printAllocateChannel)
            {
                # this is the first allocate channel line.
                # print allocate channel lines all at once and ignore
                # subsequent allocate lines

                $printAllocateChannel = 1;
   
                my $recChannels = getReceiveChannels($self, $logStr, $maxReceiveChannels); 
                if (!defined $recChannels)
                {
                    #logStr is alreay filled out
                    return 0;
                }

                for (my $i = 1; $i <= $recChannels; $i++)
                {
                     print TARGETRESTORE_FH $1 . $i . $2 . "\n"; 
                }
            }
        }
        elsif (/^(SET NEWNAME FOR DATAFILE \d+ TO \'\/)xxxxxx-\d\d\/(.+\/.+)(\'\;)/)
        {
            print TARGETRESTORE_FH $1 . $self->{dataFileLunHash}{$2} . "/" . $2 . $3 . "\n";
        }
        elsif (/^(SET NEWNAME FOR TEMPFILE \d+ TO \'\/)xxxxxx-\d\d\/(.+\/.+)(\'\;)/)
        {
            print TARGETRESTORE_FH $1 . $luns[$tmpLunIdx] . "/" . $2 . $3 . "\n";

            if ($tmpLunIdx < @luns - 1)
            {
                $tmpLunIdx++;
            }
            else
            {
                $tmpLunIdx = 0;
            }
        }
        else
        {
            print TARGETRESTORE_FH "$_";
        }
    }

    close SOURCERESTORE_FH;
    close TARGETRESTORE_FH;

    $$logStr .= "Building restore datatemp successful\n";    
    return 1;
}

##############################################################################
#
#  Function: validateDatafiles
#      checks for bad datafiles
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

#TODO test can Poornima generate bad entries?
sub validateDatafiles
{
    my ($self, $logStr) = @_;

    $$logStr .= "In validateDatafiles\n";

    ## must use as sysdba because database is still being initialized

    $$logStr .= "About to get files with a status of RECOVER and OFFLINE\n";

    my $tmpFile = "$self->{workDir}/validateDatafiles_$$.txt";
    my $sqlStatement=<<EOSql;
set pagesize 200;
set linesize 200;
col name format a100;
col status format a20;
set feedback off heading off;
spool $tmpFile;
SELECT file#,
       name,
       status
  FROM v\$datafile
 WHERE status IN ('RECOVER','OFFLINE');
spool off;
EOSql

    my $sqlPlus = "$ENV{ORACLE_HOME}/bin/sqlplus";

    my $cmd = "$sqlPlus -s '/ as sysdba'";

    $$logStr .= "About to run sqlplus\n";
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRValidateDataFilesSql_" . getDateTimeStr() . ".log";

    if (!runSqlAsSysdba($logStr, "$sqlStatement", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    my $retVal;

    if (-s $tmpFile)
    {
        $retVal = 0; 
        # there are some bad entries
        $$logStr .= "Found some problems,\n";

        if (!open(TMP_FH, "< $tmpFile"))
        {
            $$logStr .= "Cannot open $tmpFile: $!\n";
            return 0;
        }

        while (<TMP_FH>)
        {
            next if (/^\s*$/);
            /\s*(\d+)\s+(\S+)\s+(\w+)/;

            my $fileNo = $1; 
            my $fileName = $2; 
            my $status = $3; 

            $$logStr .= "File\#: $fileNo, Name: $fileName, status: $status\n";

        }
        close TMP_FH;
        $$logStr .= "Validate datafiles is unsuccessful\n";
    }
    else
    {
        $retVal = 1; 
        $$logStr .= "No datafiles have problems\n";
        $$logStr .= "Validate datafiles is successful\n";
    }

    return $retVal;

}

##############################################################################

#
#  Function: preReqOraEnv
#      makes sure the oracle enviornment has been set 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqOraEnv
{
    my ($self, $logStr) = @_;

    $$logStr .= "In preReqOraEnv\n";
    my @requiredVars = ('ORACLE_SID', 'ORACLE_HOME', 'PATH', 'ORACLE_BASE');

    my $missingVar = '';

    $$logStr .= "Checking for missing enviornment variables\n";
    foreach my $var (@requiredVars)
    {
        if (!defined $ENV{$var})
        {
            $missingVar .= "$var\n";
        }
    }

    my $retVal = 1;
    if ($missingVar ne '')
    {
        $$logStr .= "The following enviornment variables are missing.  oraenv may not have been sourced.\n";
        $$logStr .= $missingVar;
        $retVal = 0;
    }
    else
    {
       $$logStr .= "There are no missing enviornment variables\n"; 
    }

    return $retVal;
}

##############################################################################
#
#  Function: preReqLunSize
#      Make sure the target has enough space on the luns.  
#
#  Args: 
#      override - If overide is no, then the total allocated size
#                 of the target luns must be greater than or equal
#                 to the total allocated size of the source
#                 If override is yes, then the given percentUsed
#                 of the total allocated size of the target must be 
#                 greater than the total source used size
#      percentUsedThreshold - Percent of the target lun to use for comparison
#                             if override is yes 
#
# Pre-requisites:
#
#  NOTE:  This will check if the total size of the data temp luns of the 
#  target is larger than the total size of the data temp luns of the source.
#  There could still potentially be a failure later if the lun sizes are different
#  and the individual data temp files can't fit into the target lun sizes 
#
##############################################################################

sub preReqLunSize
{
    my ($self, $logStr, $override, $percentUsedThreshold) = @_;

    $$logStr .= "In preReqLunSize for override, $override\n";

    if ($override !~ /yes/i && $override !~ /no/i)
    {
        $$logStr .= "Override is $override but must be yes or no.\n";
        return 0;
    }

    $$logStr .= "Getting source lun size information.\n";

    my $sqlStatement=<<EOSql;
SELECT SUM(s.used_size),
       SUM(s.allocated_size)
  FROM inf_monitor.db_storage s,
       refresh_queue r
 WHERE r.instance = $self->{refreshQueueInst} 
   AND s.db_instance = r.source_db_instance
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }
 
    $$logStr .= "Getting source lun used size total\n";
    my $rv  = $sth->execute();

    if (!defined $rv)
    {
       $$logStr .= "Failed while getting source lun size totals: $DBI::errstr\n";
       return 0;
    }
 
    # should only be one
    my ($sourceUsedLunSize, $sourceAllocLunSize) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while source lun size totals, but got more\n";
        return 0;
    }

    if (!defined $sourceUsedLunSize || !defined $sourceAllocLunSize)
    {
        $$logStr .= "Cannot find source lun data in the database\n";
        return 0;
    }


    $$logStr .= "Source used lun size total is $sourceUsedLunSize\n";
    $$logStr .= "Source allocated lun size total is $sourceAllocLunSize\n";

    $$logStr .= "Getting target lun allocated size total\n";

    $sqlStatement=<<EOSql;
SELECT SUM(s.allocated_size)
  FROM inf_monitor.db_storage s,
       databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{targetSid}\'   
   AND s.db_instance = d.instance
EOSql


    $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv  = $sth->execute();

    if (!defined $rv)
    {
       $$logStr .= "Failed while getting target lun allocated size total: $DBI::errstr\n";
       return 0;
    }

    # should only be one
    my $targetAllocLunSize = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while target lun allocated size total, but got more\n";
        return 0;
    }

    if (!defined $targetAllocLunSize)
    {
        $$logStr .= "Cannot find target lun data in the database\n";
        return 0;
    }

    $$logStr .= "Target lun allocated size total is $targetAllocLunSize\n";

    my $retVal;
    if ($override =~ /no/i)
    {
        $$logStr .= "Override is no: check to make sure the target allocated size is greater than or equal to the source allocated size\n";

        if ($targetAllocLunSize < $sourceAllocLunSize)
        {
            $$logStr .= "The luns on the target are not big enough\n"; 
            $retVal = 0;
        }
        else
        {
            $$logStr .= "The luns on the target are sufficient\n"; 
            $retVal = 1;
        }
    }
    else
    {
        # override is yes
        $$logStr .= "Override is yes: check to make sure that $percentUsedThreshold percent of the target allocated size is greater than or equal to the source used size\n";

        my $percentOfTarget = ($targetAllocLunSize * $percentUsedThreshold) / 100;        
        if ($percentOfTarget < $sourceUsedLunSize)
        {
            $$logStr .= "Source total used size ($sourceUsedLunSize) is greater than $percentUsedThreshold percent of target total allocated size ($percentOfTarget)\n";
            $$logStr .= "The luns on the target are not sufficient\n";
            $retVal = 0;
        }
        else
        {
            $$logStr .= "Source total used size ($sourceUsedLunSize) is less than or equal to $percentUsedThreshold of target total allocated size ($percentOfTarget)\n";
            $$logStr .= "The luns on the target are sufficient\n";
            $retVal = 1;
        }
    } 
 
    # call canReuse now to get the output in this log detail
    if ($retVal && !defined $self->{canReuseRestoreDTFile})
    {
        $self->{canReuseRestoreDTFile} = canReuseRestoreDTFile($self, $logStr, $percentUsedThreshold);

        if (!defined $self->{canReuseRestoreDTFile})
        {
            # $logStr is already filled out
            return 0;
        }
    }

    return $retVal;
}

##############################################################################
#
#  Function: preReqASMSize
#      Make sure the target has enough space on the luns.  
#
#  Args: 
#      ASMpercentUsedThreshold - Percent of the target lun to use for comparison
#                             if override is yes 
#
# Pre-requisites:
#
#  NOTE:  This will check if the total size of the data temp luns of the 
#  target is larger than the total size of the data temp luns of the source.
#  There could still potentially be a failure later if the lun sizes are different
#  and the individual data temp files can't fit into the target lun sizes 
#
##############################################################################

sub preReqASMSize
{
    my ($self, $logStr, $ASMpercentUsedThreshold) = @_;


    #check individual LUNs to see if source used size would fit into the target appropriate LUN.
    $$logStr .= "By deault ASMmpPercentThreshold is equal $ASMpercentUsedThreshold percent\n";

    $$logStr .= "Getting luns and sizes\n";
    my $sqlStatement=<<EOSql;
SELECT s.lun_name,
       TRUNC(s.allocated_size / 1024),
       'target'
  FROM inf_monitor.db_storage s,
       databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{targetSid}\'
   AND s.db_instance = d.instance
UNION
SELECT s.lun_name,
       TRUNC(s.used_size / 1024),
       'source'
  FROM inf_monitor.db_storage s,
       refresh_queue r
 WHERE r.instance = $self->{refreshQueueInst}
   AND s.db_instance = r.source_db_instance
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return undef;
    }
        
    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting lun sizes: $DBI::errstr\n";
        return undef;
    }
        
    $rv = $sth->bind_columns(\my $lun, \my $size, \my $type);
        
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding lun and size: $DBI::errstr\n";
        return undef;
    }
    $$logStr .= "Filling source and target lun size hashes\n";
    $$logStr .= "Target hash contains allocated sizes and source hash contains used sizes\n";
      
    my %sourceLunSizeHash;
    while ($sth->fetch())
    {
        $$logStr .= "Found $type lun $lun with size $size ";
      
        if ($type eq 'target')
        {
            $$logStr .= "allocated\n";
            $self->{lunSizeHash}{$lun} = $size;
        }
        else
        {
            $$logStr .= "used\n";
            $sourceLunSizeHash{$lun} = $size;
        }
    }
    # compare source and target. Each source lun used size should be less
    # than to specified percent of the corresponding target's lun.
    # A source and target assumed to have 3 luns: DATA, FRE and RED.
    # It would check if there a same matching number of LUNs in source and in target.
    # Check if both LUN names exist in target and source.	
        
    my @sortedTargetLuns =  sort keys %{$self->{lunSizeHash}}; 
    my @sortedSourceLuns =  sort keys %sourceLunSizeHash;
         
    if (!@sortedTargetLuns)
    {
        $$logStr .= "Cannot find target lun data in the database\n";
        return undef;
    }
         
    if (!@sortedSourceLuns)
    {
        $$logStr .= "Cannot find source lun data in the database\n";
        return undef;
    }
    #check if they have the same number of keys:
    my $targetLUNnumber = scalar keys %{$self->{lunSizeHash}};
    my $sourceLUNnumber = scalar keys %sourceLunSizeHash;
    if (scalar keys %{$self->{lunSizeHash}} == scalar keys %sourceLunSizeHash) 
    {
        $$logStr .= "Both source and target have the same number of luns.\ There are $targetLUNnumber LUNs in target and $sourceLUNnumber LUNs in source \n";
    } else {
        $$logStr .=  "Source and target don't have the same number of luns.\ There are $targetLUNnumber LUNs in target and $sourceLUNnumber LUNs in source \n";
        return undef;
    }
		 
    my $status = 0;
    # go through sourceLun's in alphabetical order and check if target allocated size is able to accommodate the size of data from corresponding source LUN. 
    # index to compare luns
    for (my $i = 0; $i < @sortedSourceLuns; $i++)
    {
        my $sourceLun = $sortedSourceLuns[$i]; 
        my $targetLun = $sortedTargetLuns[$i];
        if(!$targetLun)
        {
            $$logStr.= "While going through the source and target LUNs found that target ASM LUN is not defined for associated $sourceLun \n";
            return undef;
        }
        elsif($sourceLun eq $targetLun)
        {  
            $$logStr .= "Comparing source: $sourceLun and target: $targetLun\n";
          
            my $ASMpercentOfTarget = ($ASMpercentUsedThreshold * $self->{lunSizeHash}{$targetLun}) / 100; 
            if ($sourceLunSizeHash{$sourceLun} > $ASMpercentOfTarget)
            {
                $$logStr .= "    Source $sourceLun used size ($sourceLunSizeHash{$sourceLun}) is greater than $ASMpercentUsedThreshold percent of target $targetLun allocated size ($ASMpercentOfTarget) - not OK for reuse\n";
                $status = 0;
            }
            else
            {
                $status = 1;
                $$logStr .= "    Source $sourceLun used size ($sourceLunSizeHash{$sourceLun}) is less than $ASMpercentUsedThreshold percents of target $targetLun allocated size ($ASMpercentOfTarget) - OK for reuse\n";
            }
        }
        else
        {
            $$logStr.="Source LUN $sourceLun doesn't match target LUN $targetLun \n ";
            return undef;
        }
    }
    $sth->finish();
         
    my $retVal;
    if ($status == 1)
    {
        $$logStr .= "Target is sufficient for reusing the restore datatemp file\n";
        $retVal = 1;
    }
    elsif ($status == -1)
    {
        $$logStr .= "ERROR: Source lun information does not exist in the database\n";
        $retVal = undef;
    }
    else
    {
        $$logStr .= "Target is not sufficient for reusing the restore datatemp file.  Must create new one\n";
        $retVal = 0 ;
    }

    return $retVal;
}

##############################################################################
##
##  Function: preReqStorage
##      Make sure the ASM target has enough space for conversion of LUN based
##      database.
##
##  Args:
##      LUN2ASMpercentUsedThreshold - Percent of the target lun to use for comparison
##                             if override is yes
##
## Pre-requisites:
##
##  NOTE:  This will check if the total size of the data temp luns of the
##  target is larger than the total size of the data temp luns of the source.
##  There could still potentially be a failure later if the lun sizes are different
##  and the individual data temp files can't fit into the target lun sizes
##
###############################################################################

sub preReqStorage
{
    my ($self, $logStr, $LUN2ASMpercentUsedThreshold) = @_;
    $$logStr .= "Getting source lun size information.\n";
    
    my $status = 0;
    my $sqlStatement=<<EOSql;
SELECT SUM(s.used_size)
  FROM inf_monitor.db_storage s,
       refresh_queue r
 WHERE r.instance = $self->{refreshQueueInst}
   AND s.db_instance = r.source_db_instance
EOSql
    
    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Getting source lun used size total\n";
    my $rv  = $sth->execute();

    if (!defined $rv)
    {
        $$logStr .= "Failed while getting source lun size totals: $DBI::errstr\n";
        return 0;
    }
    
    my ($sourceUsedLunSize) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while source lun size totals, but got more\n";
        return 0;
    }

    if (!defined $sourceUsedLunSize)
    {
        $$logStr .= "Cannot find source lun data in the database\n";
        return 0;
    }
    $$logStr .= "Source used lun size total is $sourceUsedLunSize\n";
    $$logStr .= "Getting target lun allocated size of +DATA\n";

    $sqlStatement=<<EOSql;
SELECT s.allocated_size
  FROM inf_monitor.db_storage s,
       databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{targetSid}\'
   AND s.db_instance = d.instance
   AND s.LUN_NAME = 'DATA'
EOSql

    $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv  = $sth->execute();

    if (!defined $rv)
    {
        $$logStr .= "Failed while getting ASM target lun allocated size of +DATA: $DBI::errstr\n";
        return 0;
    }

    # should only be one
    my $targetAllocDATALunSize = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while ASM target lun allocated size of +DATA, but got more\n";
        return 0;
    }

    if (!defined $targetAllocDATALunSize)
    {
        $$logStr .= "Cannot find target lun data in the database\n";
        return 0;
    }
    
    $$logStr .= "Target lun allocated size of +DATA is $targetAllocDATALunSize\n";
    $$logStr .= "Comparing source sum of luns used size: $sourceUsedLunSize and target allocated +DATA size: $targetAllocDATALunSize\n";
    my $DataLunAdjustTarget = ($LUN2ASMpercentUsedThreshold * $targetAllocDATALunSize) / 100;
    if ($sourceUsedLunSize > $DataLunAdjustTarget)
    {
        $$logStr .= "    Source SUM of  used size ($sourceUsedLunSize) is greater than $LUN2ASMpercentUsedThreshold percents of target +DATA allocated size ($DataLunAdjustTarget) - not OK for reuse\n";
        $status = 0;
    }
    else
    {
        $status = 1;
        $$logStr .= "    Source SUM of  used size ($sourceUsedLunSize) is less than $LUN2ASMpercentUsedThreshold percents of target +DATA allocated size ($DataLunAdjustTarget) - OK for reuse\n"
    }

    $$logStr .= " Checking source archive log destination\n";
    if (!defined $self->{archiveLogDest})
    {
        $self->{archiveLogDest} = getArchiveLogDest($self, $logStr, 'S');
        if (!defined $self->{archiveLogDest})
        {
            #$logStr will already be filled out
            return 0;
        }
    }
    $$logStr .= " Archive Log Destination is $self->{archiveLogDest}\n";
    $$logStr .= "Getting target lun allocated size of +FRA\n";

    $sqlStatement=<<EOSql;
SELECT s.allocated_size
  FROM inf_monitor.db_storage s,
       databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{targetSid}\'
   AND s.db_instance = d.instance
   AND s.LUN_NAME = 'FRA'
EOSql
    
    $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv  = $sth->execute();

    if (!defined $rv)
    {
        $$logStr .= "Failed while getting ASM target lun allocated size of +FRA: $DBI::errstr\n";
        return 0;
    }

    my $targetAllocFRALunSize = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while ASM target lun allocated size of +FRA, but got more\n";
        return 0;
    }

    if (!defined $targetAllocFRALunSize)
    {
        $$logStr .= "Cannot find target lun data in the database\n";
        return 0;
    }

    $$logStr .= "Target lun allocated size of +FRA is $targetAllocFRALunSize\n";
    
    $$logStr .= "Getting source lun size information.\n";

    my $sqlStatement2=<<EOSql;
SELECT s.used_size
  FROM inf_monitor.db_storage s,
       adr_manager.refresh_queue r
 WHERE r.instance = $self->{refreshQueueInst}
   AND s.db_instance = r.source_db_instance
   AND s.lun_name = '$self->{archiveLogDest}'
EOSql

    my $sth2 = $self->{bbDbh}->prepare($sqlStatement2);
    if (!defined $sth2)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Getting source Archive log dest lun ($self->{archiveLogDest}) used size \n";
    my $rv2  = $sth2->execute();

    if (!defined $rv)
    {
        $$logStr .= "Failed while getting source lun size totals: $DBI::errstr\n";
        return 0;
    }
    
    my $sourceArchLogDestLunSize = $sth2->fetchrow_array();
    if ($sth2->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while source lun size totals, but got more\n";
        return 0;
    }
    
    if (!defined $sourceArchLogDestLunSize)
    {
        $$logStr .= "Cannot find source lun data in the database\n";
        return 0;
    }
    
    $$logStr .= "Source used lun size of Archive Log Destination LUN $self->{archiveLogDest} is $sourceArchLogDestLunSize\n";
    $$logStr .= "Comparing source Archive Log Destination Lun $self->{archiveLogDest} size: and target: +FRA\n";
    my $ASMpercentOfFRATarget = ($LUN2ASMpercentUsedThreshold * $targetAllocFRALunSize) / 100;
    if ($sourceArchLogDestLunSize > $targetAllocFRALunSize)
    {
        $$logStr .= "    Source LUN $self->{archiveLogDest} used size ($sourceArchLogDestLunSize) is greater $LUN2ASMpercentUsedThreshold percents of target +FRA lun allocated size ($targetAllocFRALunSize) - not OK for reuse\n";
        $status = 0;
    }
    else
    {
        $status = 1;
        $$logStr .= "    Source LUN $self->{archiveLogDest} used size ($sourceArchLogDestLunSize) is less than $LUN2ASMpercentUsedThreshold percents of target +FRA lun allocated size ($targetAllocFRALunSize) - OK for reuse\n";
    }
    $sth->finish();
    my $retVal;
    if ($status ==1)
    {
        $$logStr .= "Target is sufficient for reusing the restore datatemp files\n";
        $retVal = 1;
    }
    elsif ($status == -1)
    {
        $$logStr .= "ERROR: Source lun information does not exist in the database\n";
        $retVal = undef;
    }
    else
    {
        $$logStr .= "Target is not sufficient for reusing the restore datatemp file.  Must create new one\n";
        $retVal = 0 ;
    }

    return $retVal;
}

##############################################################################
#
#  Function: canReuseRestoreRedoFile
#     Determines if the source restore redo file is able to be reused.
#     If any redo logs are on a local drive or an archive log destination,
#     the redo logs need to be redistributed.
#
#  Args: 
#
#  Returns:
#     Returns 1 if the all source redo logs are on non archive log destination
#     luns.  0 otherwise.
#
# Pre-requisites:
#
##############################################################################

sub canReuseRestoreRedoFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In canReuseRestoreRedoFile\n";

    if (!open (SOURCERESTORE_FH, "< $self->{sourceRestoreRedoFile}"))
    {
        $$logStr .= "Cannot open $self->{sourceRestoreRedoFile}: $!\n";
        return undef;
    }

    if (!defined $self->{sourceArchiveLogDest})
    {
        $self->{sourceArchiveLogDest} = getArchiveLogDest($self, $logStr, 'S');

        if (!defined $self->{sourceArchiveLogDest})
        {
            #$logStr will already be filled out
            return undef;
        }
    }

    my $retVal = 1;
    while (<SOURCERESTORE_FH>)
    {
        if (/^ALTER DATABASE RENAME FILE \'(\/u0.+\/redo.+\.log)\' TO/) 
        {
            $$logStr .= "     Source redo log is on a local disk ($1) - not OK for reuse\n";
            $retVal = 0;
        }
        elsif (/^ALTER DATABASE RENAME FILE \'(\/$self->{sourceArchiveLogDest}.+\/redo.+\.log)\' TO/)
        {
            $$logStr .= "     Source redo log is on archive log destination ($1) - not OK for reuse\n";
            $retVal = 0;
        }
    }

    close SOURCERESTORE_FH;

    if ($retVal)
    {
         $$logStr .= "Source is sufficient for reusing the restore redo file\n";
    }
    else
    {
         $$logStr .= "WARNING: There is a problem with the redo logs in the source database\n";
         $$logStr .= "Source is not sufficient for reusing the restore datatemp file.  Must create new one\n";
    }

    return $retVal;
}

##############################################################################
#
#  Function: canReuseRestoreDTFile
#     Determines if the source and target have compatable mount points
#     The lunSizeHash is also set here.
#
#  Args: mpPercentThreshold - percent of the target lun that cannot be 
#                             exceeded
#
#  Returns:
#     Returns 1 if the target has all lun sizes that are greater than or 
#     equal to the source luns.  0 otherwise.
#
# Pre-requisites:
#
##############################################################################

sub canReuseRestoreDTFile
{
    my ($self, $logStr, $mpPercentThreshold) = @_;

    # put sizes in a hash for reporting purposes and set lunSizeHash in the object
    # for later use

    $$logStr .= "In canReuseRestoreDTFile for $mpPercentThreshold percent\n";

    $$logStr .= "Getting luns and sizes\n";
    my $sqlStatement=<<EOSql;
SELECT s.lun_name,
       TRUNC(s.allocated_size / 1024),
       'target'
  FROM inf_monitor.db_storage s,
       databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{targetSid}\'
   AND s.db_instance = d.instance
UNION
SELECT s.lun_name,
       TRUNC(s.used_size / 1024),
       'source'
  FROM inf_monitor.db_storage s,
       refresh_queue r
 WHERE r.instance = $self->{refreshQueueInst}
   AND s.db_instance = r.source_db_instance
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return undef;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting lun sizes: $DBI::errstr\n";
        return undef;
    }

    $rv = $sth->bind_columns(\my $lun, \my $size, \my $type);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding lun and size: $DBI::errstr\n";
        return undef;
    }

    $$logStr .= "Filling source and target lun size hashes\n";
    $$logStr .= "Target hash contains allocated sizes and source hash contains used sizes\n";

    my %sourceLunSizeHash;
    while ($sth->fetch())
    {
        $$logStr .= "Found $type lun $lun with size $size ";

        if ($type eq 'target')
        {
            $$logStr .= "allocated\n";
            $self->{lunSizeHash}{$lun} = $size;
        }
        else
        {
            $$logStr .= "used\n";
            $sourceLunSizeHash{$lun} = $size;
        }
    }

    # compare source and target.  Each source lun used size should be less 
    # than or equal to the specified percent of the "matching" target's lun.  
    # A source and target lun "match" by being in the same order.
    # For example the following match,
    # srcdev-01 tgtdev-01
    # srcdev-02 tgtdev-02
    # srcdev-03 tgtdev-03
    #
    # Also matching,
    # srcdev-01 tgtdev-05
    # srcdev-02 tgtdev-06    
    # srcdev-03 tgtdev-08 
    # 
    # set matchingLunHash to have the source lun number as the key
    # and the matching target lun as the hash.  Later this will
    # be used whe creating the RestoreDataTemp file

    my $status = -1;
    my @sortedTargetLuns =  sort keys %{$self->{lunSizeHash}}; 
    my @sortedSourceLuns =  sort keys %sourceLunSizeHash;

    if (!@sortedTargetLuns)
    {
        $$logStr .= "Cannot find target lun data in the database\n";
        return undef;
    }

    if (!@sortedSourceLuns)
    {
        $$logStr .= "Cannot find source lun data in the database\n";
        return undef;
    }

    # if there are more target luns than source luns, it is okay
    # to reuse.  if there are more source luns than target luns
    # it is not okay to reuse.

    $$logStr .= "Comparing luns by number order,\n";

    # go through sourceLun's in alphabetical order and use the same 
    # index to compare luns
    for (my $i = 0; $i < @sortedSourceLuns; $i++)
    {
        $status = 1 if ($status == -1);

        if (!defined $sortedTargetLuns[$i])
        {
            my $sourceCount = @sortedSourceLuns;
            $$logStr .= "There are more luns for the source than for the target\n";
            $$logStr .= "There are $sourceCount source luns and $i target luns\n";
            $status = 0;
            last;
        }

        my $sourceLun = $sortedSourceLuns[$i]; 
        my $targetLun = $sortedTargetLuns[$i]; 

        # set the matching hash for replacment later, key is lun number
        $sourceLun =~ /.+-(\d\d)/;
        $self->{matchingLunHash}{$1} = $targetLun;

        $$logStr .= "Comparing source: $sourceLun and target: $targetLun\n";
     
        my $percentOfTarget = ($mpPercentThreshold * $self->{lunSizeHash}{$targetLun}) / 100; 
        if ($sourceLunSizeHash{$sourceLun} > $percentOfTarget)
        {
            $$logStr .= "    Source $sourceLun used size ($sourceLunSizeHash{$sourceLun}) is greater than $mpPercentThreshold percent of target $targetLun allocated size ($percentOfTarget) - not OK for reuse\n";
            $status = 0;
        }
        else
        {
            $$logStr .= "    Source $sourceLun used size ($sourceLunSizeHash{$sourceLun}) is less than or equal to $mpPercentThreshold of target $targetLun allocated size ($percentOfTarget) - OK for reuse\n";

        }
    }
    $sth->finish();

    my $retVal;
    if ($status == 1)
    {
        $$logStr .= "Target is sufficient for reusing the restore datatemp file\n";
        $retVal = 1;
    }
    elsif ($status == -1)
    {
        $$logStr .= "ERROR: Source lun information does not exist in the database\n";
        $retVal = undef;
    }
    else
    {
        $$logStr .= "Target is not sufficient for reusing the restore datatemp file.  Must create new one\n";
        $retVal = 0 ;
    }

    return $retVal;
}

##############################################################################
#
#  Function: rmArchLogLunFromHash
#     Removes the target archive log destination lun from the lunSizeHash
#
#  Args:  None
#
# Pre-requisites:
#
##############################################################################

sub rmArchLogLunFromHash
{
    my ($self, $logStr) = @_;

    if (!defined $self->{archiveLogDest})
    {
        $self->{archiveLogDest} = getArchiveLogDest($self, $logStr, 'T');
        if (!defined $self->{archiveLogDest})
        {
            #$logStr will already be filled out
            return 0;
        }
    }

    delete $self->{lunSizeHash}{$self->{archiveLogDest}};
    return 1;
}


##############################################################################
#
#  Function: isSourceAndTargetHostSame
#     Determines if the source and target are on the same host
#
#  Args:
#
#  Returns:
#      Returns 1 if the source and target are on the same host.
#      0 otherwise.
#
# Pre-requisites:
#
##############################################################################

#TODO...
sub isSourceAndTargetHostSame
{
    my ($self, $logStr) = @_;

    $$logStr .= "About to get whether source is on this host from Big Brother\n";

    # TODO what about DR?  Is new queury okay 
    my $sqlStatement=<<EOSql;
-- orig, but doesn't catch brand new ones w/ no db entry
--SELECT COUNT(DISTINCT mac_instance) 
  --FROM databases 
 --WHERE sid IN (\'$self->{sourceSid}\', \'$self->{targetSid}\') 
   --AND dataguard = 'N'; 

SELECT COUNT(*)
  FROM databases d,
       machines m
 WHERE d.mac_instance = m.instance
   AND m.name = \'$self->{hostName}\'
   AND d.sid = \'$self->{sourceSid}\'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return undef;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting host count: $DBI::errstr\n";
        return undef;
    }

    # should only be one
    my $macCount = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .=  "Expecing 1 row while getting host count, but got more\n";
        return undef;
    }

    # macCount should be 0 or 1 TODO, should there be an error if not?

    $$logStr .= "Getting host count successful\n";
    return $macCount; 
}

sub resetClientDbh
{
    my ($self, $logStr, $dbh) = @_;

    $$logStr .= "In resetClientDbh\n";

    $self->{clientDbh} = $dbh;
}

1;
