package ADRFunctions;


# Provide generic functions

# Funtions:
# getDateTimeStr - Returns current date and time in yyyymmdd_hhmmss form in EST

use strict;
use POSIX qw(:signal_h :errno_h :sys_wait_h);

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use vars qw(@ISA @EXPORT);
use Exporter;
use POSIX qw(WNOHANG);
@ISA = qw(Exporter);
@EXPORT = qw(getDateTimeStr getDateStr getDecryption getFileName checkLogForErrors runSqlAsSysdba runSqlDbi runADRKsh);

##############################################################################
#
#  Function: getDateTimeStr
#      Gets the current date and time in yyyymmdd_hhmmss form in EST
#
#  Args:
#
#  Returns: Date string in the form yyyymmdd_hhmmsss
#
##############################################################################

sub getDateTimeStr
{
    my ($format) = @_;

    my ($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
    $Year += 1900;
    $Month++;
    $Month = $Month < 10 ? "0$Month" : "$Month";
    $Day = $Day < 10 ? "0$Day" : "$Day";
    $Second = $Second < 10 ? "0$Second" : "$Second";
    $Minute = $Minute < 10 ? "0$Minute" : "$Minute";
    $Hour = $Hour < 10 ? "0$Hour" : "$Hour";

    my $retVal = $Year . $Month . $Day . "_" . $Hour . $Minute . $Second;
    if (defined $format && $format eq 'L')
    {
        $retVal = "$Year-$Month-$Day $Hour:$Minute:$Second";
    }

    return $retVal;
}

##############################################################################
#
#  Function: getDateStr
#      Gets the current date in yyyymmdd form in EST
#
#  Args:
#
#  Returns: Date string in the form yyyymmdd
#
##############################################################################

sub getDateStr
{
    my ($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
    $Year += 1900;
    $Month++;
    $Month = $Month < 10 ? "0$Month" : "$Month";
    $Day = $Day < 10 ? "0$Day" : "$Day";

    return $Year . $Month . $Day;
}

##############################################################################
#
#  Function: getDecryption
#      Gets the decrypted password for given string
#
#  Args: password - encrypted password 
#
#  Returns: Decrypted password
#
##############################################################################

sub getDecryption
{
    use ITSCode;

    my ($password) = @_;

    my $dval = "";
    for (my $idx = 33; $idx <= 126; $idx+=7)
    {
        $dval .= chr($idx);
    }

    DecodeData ($dval, $password);

    return $password;
}

##############################################################################
#
#  Function: setFileName
#      Gets a filenames from a directry from a wildcarded string
#
#  Args:
#      fileName - a fully qualified wildcarded file
#
#  Returns:
#      Full filename, undef if error 
#
# Pre-requisites:
#     <fileName> exists and only one file matches it
#
##############################################################################


sub getFileName
{
    my ($logStr, $fileName) = @_;
  
    $$logStr .= "In getFilename for $fileName\n";

    my @files = glob $fileName;

    if (@files > 1)
    {
        $$logStr .= "Found more than one file,\n";
        foreach my $file (@files)
        {
            $$logStr .= "$file\n"; 
        }
 
        return undef;
    }
    elsif (@files == 0)
    {
        $$logStr .= "Found no file,\n";
        return undef;
    }

    return $files[0];
}

##############################################################################
#
#  Function: checkLogForErrors
#      Checks a given log for errors 
#
#  Args:
#      logStr - string that gets written to the database
#      logFileToCheck - the log to chekc
#      type - rman or sql
#
#  Returns:
#      1 if good, 0 for error
#
# Pre-requisites:
#      logFileToCheck exists
#
##############################################################################

sub checkLogForErrors
{
    my ($logStr, $logFileToCheck, $type, $ignoreErrRef) = @_;

    $$logStr .= "In checkLogForErrors for $logFileToCheck and $type\n";

    my $grepRegex = '';

    if ($type eq 'rman')
    {
        $grepRegex = qr/RMAN-\d{5}/;
    }
    elsif ($type eq 'sql')
    {
        $grepRegex = qr/ORA-\d{5}/;
    }
    elsif ($type eq 'nid')
    {
        $grepRegex = qr/NID-\d{5}/;
    }
    else
    {
        $$logStr .= "Incorrect type: $type, expecting rman or sql\n";
        return 0;
    }

    if (!open (LOG_FH, "< $logFileToCheck"))
    {
        $$logStr .= "Cannot open $logFileToCheck: $!\n";
        return 0;
    }

    my $retVal = 1;

    while (my $line = <LOG_FH>)
    {
        if ($line =~ /($grepRegex)/)
        { 
            my $errorStr = $1;
 
            if (defined $ignoreErrRef && exists $ignoreErrRef->{$errorStr})
            {
                $$logStr .= "Ignoring error: $line";
            }
            else
            {
                $$logStr .= "ERROR: Found failure: $line";
                $retVal = 0;
            } 
        }
     }

    close LOG_FH;

    if ($retVal)
    {
        $$logStr .= "SQL successful\n";
    } 
    else
    {
        $$logStr .= "SQL unsuccessful\n";
    }

   return $retVal;

}

##############################################################################
#
#  Function: runSqlAsSysdba
#      run the given sql statement as sysdba
#
#  Args:
#      sqlStatement - sql statement to execute.  Select will not return anything
#
# Pre-requisites:
#
##############################################################################

sub runSqlAsSysdba
{
    my ($logStr, $sqlStatement, $logFile, $ignoreErrRef) = @_;


    # handle cases where there is a password in the sqlstatement
    my $hidePWSqlStatement = $sqlStatement;
    $hidePWSqlStatement =~ s/IDENTIFIED BY .+\;/IDENTIFIED BY \*\*\*\*\*\*\*\*\;/g; 

    $$logStr .= "In runSqlAsSysdba for $hidePWSqlStatement and $logFile\n";

    my $sqlPlus = "$ENV{ORACLE_HOME}/bin/sqlplus";
    my $cmd = "$sqlPlus -s '/ as sysdba'";

    $$logStr .= "About to run sqlplus\n";
    if (!open (SQL_PIPE, "| $cmd >> $logFile"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return 0;
    }

    print SQL_PIPE "$sqlStatement\;";

    close SQL_PIPE;

    if (!checkLogForErrors($logStr, $logFile, 'sql', $ignoreErrRef))
    {
        #$$logStr will be set
        return 0;
    }

    return 1;
}


##############################################################################
#
#  Function: runSql
#      run the given sql statement with given database handle
#
#  Args:
#      sqlStatement - sql statement to execute.  Select will not return anything
#      dbh - database handle
#
# Pre-requisites:
#
##############################################################################

sub runSqlDbi
{
    my ($logStr, $sqlStatement, $dbh, $ignoreErrRef) = @_;

    $$logStr .= "In runSqlDbi for $sqlStatement\n";

    # Can't just feed a file in perl dbi, so open file
    # and execute line by line
    # TODO, use sqlplus instead?

    my @sqlLines;
    my $semiColonStr = "_ADR_" . $$ . "_SEMICOLON_ADR_";

    if ($sqlStatement =~ /^\@(\S+)/)
    {
        my $sqlFile = $1;

        if (!defined $sqlFile)
        {
             $$logStr .= "Need filename for \@ command\n";
             return 0;
        }

        if (!open (SQL_FH, "< $sqlFile"))
        {
            $$logStr .= "Cannot open $sqlFile: $!\n";
            return 0;
        }

        my $tmpStr = "";
        my $openQuote = 0;

        while (my $line = <SQL_FH>)
        {
            # strip out comments and blank lines
            $line =~ s/--.*//;
            next if ($line =~ /^\s*$/);

            # Handle ;'s embedded in quoted strings.
            # Change any ;'s inside a quoted string to
            # $semiColonStr and then change back to ;
            # right before processing.
            # This will not catch cases where the end quote
            # has been missed.  That would be caught while
            # running the sql.

            my @semiColonPos;
            while ($line =~ m/([\'\;])/g)
            {
                if ($1 eq ';' && $openQuote)
                {
                    #found embedded semicolon
                    unshift(@semiColonPos, pos($line));
                }
                elsif ($openQuote)
                {
                    $openQuote = 0;
                }
                elsif ($1 ne ';')
                {
                    $openQuote = 1;
                }

            }

            foreach my $pos (@semiColonPos)
            {
                 $line = substr($line, 0, $pos - 1) . $semiColonStr . substr($line, $pos);
            }

            my $semiColonAtEnd = 0;
            if ($line =~ /\;\s*$/)
            {
                $semiColonAtEnd = 1;
            }

            # handle multiline cases.  look for ;
            my @tmpLines = split ('\;', $line);

            for (my $i = 0; $i < @tmpLines; $i++)
            {
               next if ($tmpLines[$i] =~ /^\s*$/ && $tmpStr eq '');

               $tmpStr .= $tmpLines[$i];

                if (($i == @tmpLines - 1 && $semiColonAtEnd) ||
                    ($i < @tmpLines - 1))
                {
                    push (@sqlLines, $tmpStr);
                    $tmpStr = "";
                }
            }
        }

        close SQL_FH;
    }
    else
    {
        $sqlLines[0] = $sqlStatement;
    }

    my $retVal = 1;

    foreach my $sql (@sqlLines)
    {
        # change any embbedd semicolon strings back to ;
        $sql =~ s/$semiColonStr/\;/g;

        my $rv = $dbh->do($sql);
        if (!defined $rv)
        {
            my $ignore = 0;
            if (defined $ignoreErrRef)
            {
                $DBI::errstr =~ /(ORA-\d+)/;
                my $oraStr = $1;
 
                if (exists $ignoreErrRef->{$oraStr})
                {
                    $ignore = 1;     
                }
            }

            if ($ignore)
            {
                $$logStr .= "Ignoring error: $DBI::errstr\n";
            }
            else
            {
                $$logStr .= "Failed while running sql: $DBI::errstr\n";
                $retVal = 0;
            }
        }
    }

    if ($retVal)
    {
        $$logStr .= "SQL successful\n";
    }
    else
    {
        $$logStr .= "SQL unsuccessful\n";
    }

    return $retVal;
}

################################################################################
##  Function: procREAPER
##      Does waitpid of the process and enables signal handler for child process
##      It is useful for handling hanging processes and for reaping off
##      zombie processes
##  Args:
##  Pre-requisites:
##      should be installed POSIX qw(:signal_h :errno_h :sys_wait_h)
#################################################################################

sub procREAPER
{
    my $pid;
    $pid = waitpid(-1, &WNOHANG);
    if ($pid == -1)
    {
        # no child waiting.  Ignore it.
    }
    elsif (WIFEXITED($?))
    {
        print "Process $pid exited with $?.\n"; 
    }
    else
    {
        print "False alarm on $pid.\n";
    }
    $SIG{CHLD} = \&procREAPER;          # in case of unreliable signals
}

##############################################################################
##
##  Function: RunADRKsh
#       Run ksh script provided in ADR steps list for specific refresh id
#
#   Args:
#       logStr - string that gets written to the database
#
#   Returns:
#      Exit code 1 if good, 0 for error and message details
#
#   Pre-requisites:
#        exists
#
###############################################################################

sub runADRKsh
{
    my ($logStr, $funcLocation, $function, $param1, $param2, $param3, $reportStateInterval, $refreshQueueInst, $RefQueueDefInst, $logObj) = @_;
    my $status=-1;
    $$logStr .= "logStr: In RunADRKsh.\n";
    $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tIn RunADRKsh". "\n");
    my $cmd = "$funcLocation/$function";
    if (!-e $cmd)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tCan't find $cmd\n");
        return 0;
    }
    $cmd .=" $param1" if (defined $param1);
    $cmd .=" $param2" if (defined $param2);
    $cmd .=" $param3" if (defined $param3);
    if (defined $param1)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tFirst parameter is :\n $param1\n");
    }
    if (defined $param2)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tSecond parameter is :\n $param2\n");
    }
    if (defined $param3)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tThird parameter is :\n $param3\n");
    }
    my $logFile = "$funcLocation/$function" . "_refid" . "$refreshQueueInst" . "_" . getDateTimeStr() . ".log";
    $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tFull command line that perl would execute is next:\n $cmd\n\n");

    #install process signal handler before spinning child process
    $SIG{CHLD} = \&procREAPER;
    my $childpid = open (KSH_PROCESS, "$cmd |");
   
    if (!$childpid)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tCannot execute script $function. Step failed with " . "$!" . "\n");
        $$logStr .= "Cannot run script $function. Step failed: $!\n";
        return 0;
    }

    $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tKsh script $function is running. Child process of launched ksh script is $childpid\n");
#    if (!readKshLog($logStr, $logFile, $RefQueueDefInst, $logObj))
#    {
#        return 0;
#    }
    my @stdoutput = <KSH_PROCESS>;
  

    if ($? ==-1)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tFailed to execute ksh script $function : $!\n");
    }
    if ($? >> 8)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tKsh script $function was executed. Status is " . ($? >> 8) . " - Failure\nStandard Output is:\n @stdoutput" . "\n");
        $status = 0;

    }
    elsif ($? & 127)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tKsh script $function has been terminated unexpectadly with  signal " . "$?". "\nStatus is " . ($? >> 8) . "\nStandard Output of $function script is:\n @stdoutput" . "\n");
    }
    else
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tKsh script $function was executed. Status is " . ($? >> 8) . " - Success\nStandard Output is:\n @stdoutput" . "\n");
        $status = 1;

    }
    $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tFor more detail please go to script location $funcLocation and check the logs." . "\n");
    close KSH_PROCESS or $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "\tProcess was already closed: $! $?" . "\n");
    return $status;

}


###############################################################################
##  Function: readKshLog
##      Tails given logfile and updates the database.
##
##  Args: logfile - the ksh script's log
##
## Pre-requisites:
##     should be called by RunADRKsh
##
###############################################################################

sub readKshLog
{
    my ($logStr, $logFile, $RefQueueDefInst, $logObj) = @_;
    my $checklogcmd = "tail -f -n +1 $logFile";
    $$logStr .= "About to run $checklogcmd\n";
    
    my $logpid;
    if (!($logpid = open (KSH_LOG, "$checklogcmd |")))
    {
        $$logStr .= "Cannot execute command $checklogcmd.  Error is $!\n";
        return 0;
    }

    while (<KSH_LOG>)
    {
        $logObj->updateLogDetails($RefQueueDefInst, getDateTimeStr('L') . "$_" . "\n");
    }
}
1;
