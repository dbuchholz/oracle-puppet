package ADR;

# Main ADR module

# Funtions:

use strict;
#use vars qw(@ISA @EXPORT);

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
    unshift (@INC, $ENV{ADR_LOCATION});
}

#use Exporter;
use ADRFunctions;
use ADRFiles;
use ADRDbaUsers;
use ADRDatabase;
use ADRTables;
use ADRPassword;
use ADREngines;
use ADRLog;
use Text::ParseWords;
use ITSCode;
use Sys::Hostname;
use DBI;
use POSIX qw(WNOHANG);

#Functions that require a client dbh:
# ADRDatabase:
#    preReqEnginesDown
#    shutdownDatabase
#    deleteDatabase
#    deleteDatabaseLUN2ASM
#    compileDatabase
#    autoExtend
#    setBlockChangeTracking ('on' only)
#    runSql
#    setFlashback
#    setGlobalName
#    unregisterOldDatabase
#    importSchema
#    setOpenResetLogs
#    setOpenResetLogsUpgrade
# ADRPassword.pm
#    updatePWDatabase
# ADRTables.pm
#    exportSchemas
#    truncateTables
#    exportDataPumpStruct
# ADRDbaUsers
#    createSchemaGrantFile
#    createPreserveUserFile
#    revokeProxies
# ADRFiles.pm
#    preReqInitOraLocalFile
#    preReqInitOraFile
#    createInitOraFile
#    copyInitOraLocalFile


##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      targetSid - target sid to create the RestoreDataTempFiles for
#      sourceSid - source sid used as a base to create RestoreDataTempFiles
#      backupSet - backup set of the source sid
#      backupLocation - location of the source sid backup
#
##############################################################################

sub new
{
#    my ($class, $sourceSid, $targetSid, $isASM, $backupSet, $backupLocation) = @_;
    #my ($class, $refreshQueueInst) = @_;
    my ($class, $refreshQueueInst, $sourceSid, $targetSid, $isASM, $backupLocation, $backupSet) = @_;

    my $self = {
        parentPid => $$,
        sourceSid => $sourceSid,
        targetSid => $targetSid,
        backupSet => $backupSet,
        backupLocation => $backupLocation,
        logObj => undef,
        refreshQueueInst => $refreshQueueInst,
        isASM => $isASM,
        curRefQueueDefInst => undef,
        bbDbh => undef,
        clientDbPW => undef, # use this for ADRTables object
        hostName => undef,
        clientDbh => undef,
        adrTables => undef,
        adrFiles => undef,
        adrPassword => undef,
        adrDbaUsers => undef,
        adrDatabase => undef,
        adrEngines => undef,
        paramHash => {mpPercentThreshold => 90,
                      ASMPercentThreshold => 100,
                      LUN2ASMPercentThreshold =>100,
                      maxReceiveChannels => 12, 
                      sqlScriptsDir => 'sqlScripts', 
                      threadCheckSeconds => 5, 
                      LogDetailLimit => 4000,
                      essCheckSeconds => 20,
                      essCheckMulitplier => 30,
                      reportStateInterval => 1800,
                      dbRecoveryFileDestSize => '100G'}, # defaults
        requiredParams => ['BigBrotherSID', 'BigBrotherUser', 'BigBrotherCode',
                           'RmanCatalogSID', 'RmanCatalogUser'],
        paramsToDecrypt => ['BigBrotherCode'],
        fullTargetPath => undef,
        stepTimeout => 60000
    };

    processParams($self);

    $self->{bbDbh} = connectToBigBrother($self);

    # at this point db logging can begin because of the big 
    # brother connection
    # TODO, put in a text log for any errors that happen before this
    $self->{logObj} = new ADRLog($self->{bbDbh},  $refreshQueueInst, $self->{paramHash}{LogDetailLimit});
    # update main table with a status of running
    $self->{logObj}->updateRefreshQueue('R');

    $self->{hostName} = hostname();
    # strip out domain
    $self->{hostName} =~ s/\..+\..+//;

    my $logStr = "";

    if (!setClientPW($self, \$logStr))
    {
        $self->{logObj}->updateRefreshQueueGeneralFail($logStr);
        return undef;
    }
    if ($self->{isASM})
    {
        $self->{fullTargetPath} = $ENV{ADR_LOCATION};
    }
    else
    {
        $self->{fullTargetPath} = "$ENV{HOME}/$ENV{ADR_LOCATION}";
    }

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: process
#      Read steps from the database and call appropriate functions 
#
#  Args:
#
##############################################################################

sub process
{
    my ($self) = @_;

    # read steps from the BigBrother table

    my $sqlStatement=<<EOSql;
SELECT r.instance,
       s.script_name,
       s.script_location,
       r.param1,
       r.param2,
       r.param3,
       r.step_number,
       r.status
  FROM refresh_queue_def r,
       script_definition s
 WHERE r.refresh_queue_inst = $self->{refreshQueueInst}
   AND s.instance = r.script_instance
   AND r.step_number != 0
   AND r.status != \'S\' 
 ORDER BY step_number
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);
    $sth->execute();

    $sth->bind_columns (\my $refQueueDefInst, \my $function, \my $funcLocation, \my $param1, \my $param2, \my $param3, \my $stepNumber, \my $stepStatus);

    my $finalStatus = 'S';  # success
    
    while ($sth->fetch())
    {
        # the curRefQueueDefInst is need for functions that
        # will update multiple log details
        $self->{curRefQueueDefInst} = $refQueueDefInst;

        my $status = 0;
      
        my $logStr = ""; 

        # do not call the function if this is an overriden step
        if ($stepStatus ne 'O')
        {
            if (!$self->{logObj}->updateStepStartTime($refQueueDefInst))
            {
                $finalStatus = 'F';  # failure
                last;
            }
    
            if (defined $function && defined $funcLocation)
            {
                 if ($funcLocation eq 'internal')
                {
                    if (defined(&{\&{$function}}))
                    {
                        # logStr is the details to put in the database
                        $status = &{\&{$function}}($self, \$logStr, $param1, $param2, $param3); 
                    }
                    else
                    {
                        $logStr = "Internal function $function does not exist.\n"; 
                    }
                }
                else
                {
                    $logStr = "About to execute Function runADRKsh.\n";
                    $status = runADRKsh(\$logStr, $funcLocation, $function, $param1, $param2, $param3, $self->{paramHash}{reportStateInterval}, $self->{refreshQueueInst}, $self->{curRefQueueDefInst}, $self->{logObj});
                    $logStr = "RunADRKsh function was executed.\n";
                }
            }
            else
            {
                $logStr = "function or function location field is null.";
            }

            if (!$status)
            {
                $self->{logObj}->updateStepStatusFail($refQueueDefInst, $logStr);
                # Don't check error here, try to atleast write to the main
                # table
                $finalStatus = 'F';  # failure
                last;
            }


            if (!$self->{logObj}->updateStepStatusSuccess($refQueueDefInst, $logStr))
            {
                # failed writing to the step table
                $finalStatus = 'F';  # failure
                last;
            }
        }
    }
    $sth->finish();

    if (!defined $refQueueDefInst)
    {
        $finalStatus = "M"; # steps are missing
    }

    $self->{logObj}->updateRefreshQueue($finalStatus);

}

##############################################################################
#
#  Function: DESTROY
#      destructor
#
#  Args:
#
##############################################################################

sub DESTROY
{
    my ($self) = @_;

    if ($$ == $self->{parentPid})
    {
        $self->{bbDbh}->disconnect() if (defined $self->{bbDbh});
        $self->{clientDbh}->disconnect() if (defined $self->{clientDbh});
    }
}

##############################################################################
#
#  Function: preReqLunSize
#     make sure the target has enough space on the luns
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqLunSize
{
    my ($self, $logStr, $override) = @_;

    if (!defined $override)
    {
        $$logStr .= "Missing paramter to specify if override is true or false\n";
        return 0;
    }

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqLunSize($logStr, $override, $self->{paramHash}{mpPercentThreshold});

}


sub preReqASMSize
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqASMSize($logStr, $self->{paramHash}{ASMPercentThreshold});

}

sub preReqStorage
{
    my ($self, $logStr) = @_;
    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
        $self->{sourceSid}, $self->{refreshQueueInst},
        $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
        $self->{bbDbh}, $self->{clientDbh});
    }
    return $self->{adrFiles}->preReqStorage($logStr, $self->{paramHash}{LUN2ASMPercentThreshold});
}
##############################################################################
#
#  Function: preReqDatabaseEntries
#     makes sure entries are in the databases table
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqDatabaseEntries
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqDatabaseEntries($logStr);

}

##############################################################################
#
#  Function: preReqEnginesDown
#     makes sure all engines are down 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqEnginesDown
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out 
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqEnginesDown($logStr);

}

##############################################################################
#
#  Function: preReqPaceVersions
#     makes sure pace versions are the same 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqPaceVersions
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqPaceVersions($logStr);

}

##############################################################################
#
#  Function: preReqStarVersions
#     makes sure star versions are the same
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqStarVersions
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqStarVersions($logStr);

}

##############################################################################
#
#  Function: preReqOracleVersions
#     makes sure oracle versions are the same
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqOracleVersions
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqOracleVersions($logStr);

}

##############################################################################
#
#  Function: preReqOraEnv
#      makes sure the oracle enviornment has been set 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqOraEnv
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqOraEnv($logStr);

}

##############################################################################
#
#  Function: preReqOraTab
#     make sure <targetSid> is in the oratab file
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqOraTab
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqOraTab($logStr);

}

##############################################################################
#
#  Function:  preReqASMBkpFile 
#             make sure <backupLocation>/asm_metadata_<backupSet> exists
#
#  Args:
#
# Pre-requisites: 
#     Only for CMP
#
##############################################################################

sub preReqASMBkpFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->preReqASMBkpFile($logStr, $self->{backupLocation}, $self->{backupSet});

}



##############################################################################
#
#  Function: preReqRestoreDTFile
#     make sure <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv
#     and <sourceSid>_<backupSet>_LOG.TXT exist 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqRestoreDTFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqRestoreDTFile($logStr);

}

##############################################################################
#
#  Function: preReqRestoreRedoFile
#      make sure <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqRestoreRedoFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqRestoreRedoFile($logStr);

}

##############################################################################
#
#  Function: preReqControlFile
#     make sure <backupLocation>/Restore*ControlFile_<sourceSid>_<backupSet>.rcv and <backupLocation>/cf_<backupSet>*_full_ctrl exists
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqControlFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqControlFile($logStr);
}

##############################################################################
#
#  Function: stopEngines
#      Stops all engines via the EEM.  Calls a stored procedure to do this
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub stopEngines
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrEngines})
    {
        $self->{adrEngines} = new ADREngines($self->{bbDbh}, 
                            $self->{paramHash}{essCheckSeconds}, 
                            $self->{paramHash}{essCheckMulitplier},
                            $self->{refreshQueueInst});
    }

    return $self->{adrEngines}->stopEngines($logStr);

}

##############################################################################
#
#  Function: startEngines
#      Stops all engines via the EEM.  Calls a stored procedure to do this
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub startEngines
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrEngines})
    {
        $self->{adrEngines} = new ADREngines($self->{bbDbh},
                            $self->{paramHash}{essCheckSeconds},
                            $self->{paramHash}{essCheckMulitplier},
                            $self->{refreshQueueInst});
    }

    return $self->{adrEngines}->startEngines($logStr);

}


##############################################################################
#
#  Function: createRestoreRedoFile
#      Create run_rename_redo.rcv.
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
#
##############################################################################

sub createRestoreRedoFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createRestoreRedoFile($logStr);
}

##############################################################################
##
##  Function: createRestoreRedoFileLUN2ASM
##      Create run_rename_redo.rcv for LUN-to-ASM refresh type on target.
##
##  Args:
##
## Pre-requisites:
##     <backupLocation>/Restore*Redo_<sourceSid>_<backupSet>.rcv exists
##
###############################################################################


sub createRestoreRedoFileLUN2ASM
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createRestoreRedoFileLUN2ASM($logStr);
}


##############################################################################
#
#  Function: runRestoreRedoFile
#      Run run_rename_redo.rcv via sql.
#
#  Args:
#
# Pre-requisites:
#     run_rename_redo.rcv exists
#
##############################################################################

sub runRestoreRedoFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->runRestoreRedoFile($logStr);

}

##############################################################################
##
##  Function: runRestoreRedoFileLUN2ASM
##      Run run_rename_redo.rcv via sql.
##
##  Args:
##
## Pre-requisites:
##     run_rename_redo.rcv exists
##
###############################################################################

sub runRestoreRedoFileLUN2ASM
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->runRestoreRedoFileLUN2ASM($logStr);

}


##############################################################################
#
#  Function: createRestoreDTFile
#      Create RestoreDataTempFiles_<targetSid>.rcv based on mount points and file
#      sizes.  This is used only if the source and target have different mount points
#      and/or sizes
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
#     <sourceSid>_<backupSet>_LOG.TXT exist
#
##############################################################################

sub createRestoreDTFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid}, 
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation},  $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createRestoreDTFile($logStr, $self->{paramHash}{mpPercentThreshold}, $self->{paramHash}{maxReceiveChannels});
}

##############################################################################
##
##  Function: createRestoreDTFileLUN2ASM
##      Create RestoreDataTempFiles_<targetSid>.rcv based on mount points and file
##      sizes.  This is used only if the source and target have different mount points
##      and/or sizes
##
##  Args:
##
## Pre-requisites:
##     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
##     <sourceSid>_<backupSet>_LOG.TXT exist
##
###############################################################################

sub createRestoreDTFileLUN2ASM
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation},  $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createRestoreDTFileLUN2ASM($logStr, $self->{paramHash}{mpPercentThreshold}, $self->{paramHash}{maxReceiveChannels});
}


##############################################################################
#
#  Function: runRestoreDTFile
#      Run RestoreDataTempFiles_<targetSid>.rcv via rman.
#
#  Args:
#
# Pre-requisites:
#     RestoreDataTempFiles_<targetSid>.rcv exists
#
##############################################################################

sub runRestoreDTFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->runRestoreDTFile($logStr, $self->{curRefQueueDefInst}, $self->{logObj});

}

##############################################################################
#
#  Function: createControlFile
#      Create <sourceSid>_specific_cf.rcv
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*ControlFile_<sourceSid>_<backupSet>.rcv exists
#
##############################################################################

sub createControlFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createControlFile($logStr);
}

##############################################################################
#
#  Function: runControlFile
#      Run <sourceSid>_specific_cf.rcv via rman.
#
#  Args:
#
# Pre-requisites:
#     <sourceSid>_specific_cf.rcv exists
#
##############################################################################

sub runControlFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->runControlFile($logStr);
}

##############################################################################
#
#  Function: validateDatafiles
#      checks for bad datafiles
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub validateDatafiles
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->validateDatafiles($logStr);
}

##############################################################################
#
#  Function: preReqInitOraFile
#      make sure <backupLocation>/<backupSet>_init<sourceSid>.ora exists
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqInitOraFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqInitOraFile($logStr, );

}

##############################################################################
#
#  Function: preReqInitOraLocalFile
#      make sure $ENV{ORACLE_HOME}/dbs/init.eagle exists
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub preReqInitOraLocalFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->preReqInitOraLocalFile($logStr, );

}

##############################################################################
#
#  Function: createInitOraFile
#      Create init<targetSid>.ora
#
#  Args:
#
# Pre-requisites:
#     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
#
##############################################################################

sub createInitOraFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createInitOraFile($logStr);
}

##############################################################################
#
#  Function: copyInitOraLocalFile
#      Copy content of local Init Ora File $ENV{ORACLE_HOME}/dbs/init.eagle 
#      to $ENV{ORACLE_HOME}/dbs/init$self->{targetSid}.ora
#
#  Args:
#
# Pre-requisites:
#     $ENV{ORACLE_HOME}/dbs/init.eagle exists
#
##############################################################################

sub copyInitOraLocalFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->copyInitOraLocalFile($logStr);
}

##############################################################################
##
##  Function: createInitOraFileLUN2ASM
##      Create init<targetSid>.ora for LUN-to-ASM refresh type
##
##  Args:
##
## Pre-requisites:
##     <backupLocation>/Restore*DataTempFiles_<sourceSid>_<backupSet>.rcv and
##
###############################################################################

sub createInitOraFileLUN2ASM
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->createInitOraFileLUN2ASM($logStr, $self->{paramHash}{dbRecoveryFileDestSize});
}

##############################################################################
#
#  Function: updateInitOraFileSid
#      Update the sid in init<targetSid>.ora to be the target
#
#  Args:
#
# Pre-requisites:
#     init<targetSid>.ora exists
#
##############################################################################

sub updateInitOraFileSid
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrFiles})
    {
        $self->{adrFiles} = new ADRFiles($self->{targetSid},
                          $self->{sourceSid}, $self->{refreshQueueInst},
                          $self->{backupSet}, $self->{backupLocation}, $self->{isASM},
                          $self->{bbDbh}, $self->{clientDbh});
    }

    return $self->{adrFiles}->updateInitOraFileSid($logStr);
}
##############################################################################
#
#  Function: createSchemaGrantFile
#      Creates ADRSchemaPrivs_yyyyddmm_hhmmss.sql
#
#  Args:
#     schemas - comma separated list of schemas,
#
# Pre-requisites:
#    The schemas exist in the target database
#
##############################################################################

sub createSchemaGrantFile
{
    my ($self, $logStr, $schemas) = @_;

    if (!defined $schemas)
    {
        $$logStr .= "Missing paramter to specify schemas\n";
        return 0;
    }

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);
    }

    if (!defined $self->{adrDbaUsers})
    {
        $self->{adrDbaUsers} = new ADRDbaUsers($self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrDbaUsers}->createSchemaGrantFile($logStr, $schemas);

}

##############################################################################
#
#  Function: runSchemaGrantFile
#      runs PreserveUser_yyyymmdd_hhmmss.sql via SQL
#
#  Args:
#
# Pre-requisites: 
#     ADRSchemaPrivs_yyyymmdd_hhmmss.sql exists
#
##############################################################################

sub runSchemaGrantFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDbaUsers})
    {
        $self->{adrDbaUsers} = new ADRDbaUsers($self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrDbaUsers}->runSchemaGrantFile($logStr);

}

##############################################################################
#
#  Function: createPreserveUserFile
#      Create PreserveUser_yyyymmdd_hhmmss.sql from the target database
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub createPreserveUserFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDbaUsers})
    {
        $self->{adrDbaUsers} = new ADRDbaUsers($self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrDbaUsers}->createPreserveUserFile($logStr);

}

##############################################################################
#
#  Function: runPreserveUserFile
#      runs PreserveUser_yyyymmdd_hhmmss.sql via SQL
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub runPreserveUserFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDbaUsers})
    {
        $self->{adrDbaUsers} = new ADRDbaUsers($self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrDbaUsers}->runPreserveUserFile($logStr);

}

##############################################################################
#
#  Function: revokeProxies
#      revokes proxies for clients 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub revokeProxies
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDbaUsers})
    {
        $self->{adrDbaUsers} = new ADRDbaUsers($self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrDbaUsers}->revokeProxies($logStr);

}

##############################################################################
#
#  Function: exportTables
#      Create ADRTables_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub exportTables
{
    #my ($self, $tables) = @_;
    my ($self, $logStr, $tables) = @_;

    if (!defined $tables)
    {
        $$logStr .= "Missing paramter to specify tables\n";
        return 0;
    }

    if (!defined $self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }
	
    # TODO, pass in id
    return $self->{adrTables}->exportTables($logStr, $tables);

}

##############################################################################
#
#  Function: exportDPTables
#      Create ADRDPTables_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables to be exported using data pump utility
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub exportDPTables
{
    my ($self, $logStr, $tables) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $tables)
    {
        $$logStr .= "Missing paramter to specify tables to be exported by datapump\n";
        return 0;
    }

    if (!defined $self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    # TODO, pass in id 
    return $self->{adrTables}->exportDPTables($logStr, $tables);

}

##############################################################################
#
#  Function: exportSchemas
#      Creates ADRSchemas_yyyymmdd_hhmmss.dmp and
#      ADRSchemaPrivs_yyyymmdd_hhmmss.dmp
#
#  Args:
#     schemas - comma separated list of schemas
#
# Pre-requisites:
#     The schemas exist in the target database
#
##############################################################################

sub exportSchemas
{
    my ($self, $logStr, $schemas) = @_;

    if (!defined $schemas)
    {
        $$logStr .= "Missing paramter to specify schemas\n";
        return 0;
    }

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->exportSchemas($logStr, $schemas);
}


##############################################################################
#
#  Function: truncateTables
#      Truncantes specified tables
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub truncateTables
{
    my ($self, $logStr, $tables) = @_;

    if (!defined $tables)
    {
        $$logStr .= "Missing paramter to specify tables\n";
        return 0;
    }

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined$self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->truncateTables($logStr, $tables);

}

##############################################################################
#
#  Function: truncateSysAudTable
#      Truncantes SYS.AUD$ table
#
#  Args:
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub truncateSysAudTable
{
    my ($self, $logStr) = @_;

    if (!defined$self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->truncateSysAudTable($logStr);
}

##############################################################################
#
#  Function: importTables
#      Imports tables in ADRTables_<targetSid>.dmp
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub importTables
{
    my ($self, $logStr) = @_;

    if (!defined$self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->importTables($logStr);
}

##############################################################################
#
#  Function: importDPTables
#      Imports tables in ADRDPTables_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub importDPTables
{
    my ($self, $logStr) = @_;
    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->importDPTables($logStr);
}

##############################################################################
#
#  Function: importSchemas
#      Imports tables in ADRSchemas_yyyymmdd_hhmmss.dmp and priviledges
#      in ADRSchemaPrivs_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub importSchemas
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);
    }

    if (!defined $self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->importSchemas($logStr);

}

##############################################################################
#
#  Function: exportDataPumpStruct
#      Exports the datapump structure
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub exportDataPumpStruct
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);
    }

    if (!defined$self->{adrTables})
    {
        $self->{adrTables} = new ADRTables($self->{targetSid}, $self->{refreshQueueInst}, $self->{fullTargetPath}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW});
    }

    return $self->{adrTables}->exportDataPumpStruct($logStr);

}

##############################################################################
#
#  Function: createDefaultPWFile
#      Create orapw<sid> file with an "eagle" password
#
#  Args:
#      type - S or T for source or target
#
# Pre-requisites:
#
##############################################################################

sub createDefaultPWFile
{
    my ($self, $logStr, $type) = @_;

    if (!defined $type)
    {
        $$logStr .= "Missing paramter to specify type\n";
        return 0;
    }

    if (!defined $self->{adrPassword})
    {
        $self->{adrPassword} = new ADRPassword($self->{targetSid}, $self->{sourceSid}, $self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrPassword}->createDefaultPWFile($logStr, $type);

}

##############################################################################
#
#  Function: updatePWDatabase
#      Resets the passwords in the target database
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub updatePWDatabase
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrPassword})
    {
        $self->{adrPassword} = new ADRPassword($self->{targetSid}, $self->{sourceSid}, $self->{bbDbh}, $self->{clientDbh}, $self->{refreshQueueInst});
    }

    return $self->{adrPassword}->updatePWDatabase($logStr);

}

##############################################################################
#
#  Function: shutdownDatabase
#     Shutdown the target database with immediate or abort 
#
#  Args:
#      type - I or A for immediate or abort
#
# Pre-requisites:
#
##############################################################################

sub shutdownDatabase
{
    my ($self, $logStr, $type) = @_;

    if (!defined $type)
    {
        $$logStr .= "Missing paramter to specify type\n";
        return 0;
    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    my $status = $self->{adrDatabase}->shutdownDatabase($logStr, $type);

    # the disconnect is in the shutdown database
    $self->{clientDbh} = undef;

    return $status;
}

##############################################################################
#
#  Function: startDatabaseRman
#     Starts up the database via rman
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub startDatabaseRman
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->startDatabaseRman($logStr);

}



##############################################################################
#
#  Function: startDatabaseSql
#     Starts up the database via sql
#
#  Args:
#    type - M, N or B for mount, nomount or blank
#    usePfile: 1 if startup should be using pfile, undefined or 0 if should not be using pfile
#
# Pre-requisites:
#
##############################################################################

sub startDatabaseSql
{
    my ($self, $logStr, $type, $usePfile) = @_;

    if (!defined $type)
    {
        $$logStr .= "Missing paramter to specify type\n";
        return 0;
    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    my $dbStatus = $self->{adrDatabase}->startDatabaseSql($logStr, $type, $usePfile);

    if (!$dbStatus)
    {
        # losgStr has been set already
        return 0;
    }

    return 1;
}

##############################################################################
#
#  Function: deleteDatabase
#     deletes the database
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub deleteDatabase
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    my $status = $self->{adrDatabase}->deleteDatabase($logStr, $self->{paramHash}{threadCheckSeconds});

    # the disconnect is in the shutdown database
    $self->{clientDbh} = undef;
 
    return $status;

}

##############################################################################
##
##  Function: deleteDatabaseLUN2ASM
##     deletes the database for LUN2ASM refresh type
##
##  Args:
##
## Pre-requisites:
##
###############################################################################

sub deleteDatabaseLUN2ASM
{
    my ($self, $logStr, $createPfileFromSpfile) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    my $status = $self->{adrDatabase}->deleteDatabaseLUN2ASM($logStr, $createPfileFromSpfile, $self->{paramHash}{threadCheckSeconds});

    # the disconnect is in the shutdown database
   $self->{clientDbh} = undef;

    return $status;

}

##############################################################################
#
#  Function: compileDatabase
#     compiles the database
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub compileDatabase
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        
        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->compileDatabase($logStr);

}


##############################################################################
#
#  Function: setFlashback
#      sets flashback on or off
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setFlashback
{
    my ($self, $logStr, $action) = @_;

    if (!defined $action)
    {
        $$logStr .= "Missing paramter to specify action\n";
        return 0;
    }

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->setFlashback($logStr, $action);
}

##############################################################################
#
#  Function: setOpenResetLogs
#      open database is reset log mode
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setOpenResetLogs
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->setOpenResetLogs($logStr);
}
##############################################################################
##
##  Function: setOpenResetLogsUpgrade
##  Open and Resetlogs in upgrade mode
##  Args:
##
## Pre-requisites:
##
###############################################################################

sub setOpenResetLogsUpgrade
{
    my ($self, $logStr) = @_;
    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }
    
    return $self->{adrDatabase}->setOpenResetLogsUpgrade($logStr);
}


##############################################################################
#
#  Function: setBlockChangeTracking
#      sets block change tracking on or off 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setBlockChangeTracking
{
    my ($self, $logStr, $action) = @_;

    if (!defined $action)
    {
        $$logStr .= "Missing paramter to specify action\n";
        return 0;
    }

    if ($action =~ /^on$/i && !defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->setBlockChangeTracking($logStr, $action);
}

##############################################################################
#
#  Function: setSupplementalLog
#      sets the supplemental log
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setSupplementalLog
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->setSupplementalLog($logStr);
}

##############################################################################
#
#  Function: createSpFile
#      create the spfile from the pfile 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub createSpFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->createSpFile($logStr);
}

##############################################################################
#
#  Function: setGlobalName
#      sets the global name 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub setGlobalName
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->setGlobalName($logStr);
}

##############################################################################
#
#  Function: changeDatabaseId
#      changes the database id to the target via nid 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub changeDatabaseId
{
    my ($self, $logStr) = @_;

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->changeDatabaseId($logStr);
}

##############################################################################
#
#  Function: unregisterOldDatabase
#      unregisters old database in the rman catalog 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub unregisterOldDatabase
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->unregisterOldDatabase($logStr, $self->{paramHash}{RmanCatalogSID}, $self->{paramHash}{RmanCatalogUser});

}



##############################################################################
#
#  Function: autoExtend
#      sets autoextend for file_name from dba_datafiles based on tablespace size 
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub autoExtend
{
    my ($self, $logStr) = @_;

    if (!defined $self->{clientDbh})
    {
        $$logStr .= "Connecting to the client\n";
        $self->{clientDbh} = connectToClientDB($self, $logStr);
        if (!defined $self->{clientDbh})
        {
            # logStr will be already filled out
            return 0;
        }

        resetClientDbhAll($self, $logStr);

    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    return $self->{adrDatabase}->autoExtend($logStr);
}

##############################################################################
#
#  Function: runSql
#      sets autoextend for file_name from dba_datafiles based on tablespace size
#
#  Args:
#      sqlStatement - sql statement to execute.  Select will not return anything
#
# Pre-requisites:
#
##############################################################################

sub runSql
{
    my ($self, $logStr, $sqlStatement) = @_;

    if (!defined $sqlStatement)
    {
        $$logStr .= "Missing paramter to specify sqlStatement\n";
        return 0;
    }

    if (!defined $self->{adrDatabase})
    {
        $self->{adrDatabase} = new ADRDatabase($self->{targetSid}, $self->{sourceSid}, $self->{isASM}, $self->{bbDbh}, $self->{clientDbh}, $self->{clientDbPW}, $self->{paramHash}{sqlScriptsDir}, $self->{refreshQueueInst});
    }

    # there are some sripts that need to be run as sysdba.  So just
    # do them all as sydba.
    return $self->{adrDatabase}->runSqlSysDba($logStr, $sqlStatement);
}

# END step functions
##############################################################################

##############################################################################
#
#  Function: connectToBigBrother 
#      Connects to a the Big Brother DB 
#
#  Args:
#      None 
#
#  Returns:
#      
#
# Pre-requisites:
#     ~oracle/local/dba/BigBrother exists and tnsnames.ora is setup for 
#     connection to the Big Brother DB
#
# NOTE: Only need to set logStr for errors.  This is not called in a step,
# so it only gets written to the database in case of a failure
#
##############################################################################

sub connectToBigBrother
{
    my ($self) = @_;

    my $serverStr = "dbi:Oracle:$self->{paramHash}{BigBrotherSID}";
    my $dbh = DBI->connect($serverStr, $self->{paramHash}{BigBrotherUser}, 
                           $self->{paramHash}{BigBrotherCode},
                           {PrintError =>1, AutoCommit => 1, InactiveDestroy => 1}) || die "Could not connect: $DBI::errstr\n";

    return $dbh;
}

##############################################################################
#
#  Function: setClientPW 
#      Gets the client passwrod from the big brother
#
#  Args:
#
#  Returns:
#
#
# Pre-requisites:
#
# NOTE: Only need to set logStr for errors.  This is not called in a step,
# so it only gets written to the database in case of a failure
#
##############################################################################

sub setClientPW
{
    my ($self, $logStr) = @_;

    my $dbUser = 'SYSTEM';

    my $sqlStatement=<<EOSql;
SELECT bb_get(nvl(temp_code,code))
  FROM databases d, 
       machines m, 
       user_codes u
 WHERE u.username = \'$dbUser\' 
   AND m.instance = d.mac_instance 
   AND d.instance = u.db_instance 
   AND d.sid = \'$self->{targetSid}\'
   AND m.name = \'$self->{hostName}\'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting client password: $DBI::errstr\n";
        return 0;
    }

    # should only be one
    $self->{clientDbPW} = $sth->fetchrow_array();

    if (!defined $self->{clientDbPW})
    {
        $$logStr .= "ERROR: Cannot find password for $self->{targetSid}\n";
        return 0;
    }

    if ($sth->fetchrow_array())
    {
        $$logStr .= "Expecting 1 row while getting client password but got more\n";
        return 0; 
    }
    $sth->finish();

    return 1;
}


##############################################################################
#
#  Function: connectToClientDB
#      Connects to a the ClientDB
#
#  Args:
#      targetSid - target sid
#      bbDbh - database handle for Big Brother
#
#  Returns:
#
#
# Pre-requisites:
#     ~oracle/local/dba/BigBrother exists and tnsnames.ora is setup for
#     connection to the Big Brother DB
#
##############################################################################

sub connectToClientDB
{
    my ($self, $logStr) = @_;

    $$logStr .= "In connectToClientDB\n";

    $$logStr .= "Connecting to $self->{targetSid}\n";
 
    my $serverStr = "dbi:Oracle:$self->{targetSid}";
    my $dbh = DBI->connect($serverStr, 'SYSTEM', $self->{clientDbPW},
                       {PrintError =>1, AutoCommit => 0});

    if (!defined $dbh)
    {
        $$logStr .= "Could not connect: $DBI::errstr\n";
        return undef;
    }

    $$logStr .= "Connect to client successful\n";
    return $dbh;
}

##############################################################################
#
#  Function: setParamsFromBigBrother
#      Sets the basic paramters from Big Brother
#
#  Args:
#
#  Returns:
#
# Pre-requisites:
#
##############################################################################

sub setParamsFromBigBrotherOld
{
    my ($self) = @_;

    my $sqlStatement=<<EOSql;
SELECT sd.sid source, 
       td.sid target, 
       b.backup_location, 
       b.backup_tag 
  FROM backup_runs b, 
       adr_request a, 
       databases td, 
       databases sd
 WHERE a.source_db_id = sd.instance 
   AND a.target_db_id = td.instance 
   AND b.instance = a.backup_run_id
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);
    $sth->execute();

     # should only be one
    ($self->{sourceSid}, $self->{targetSid}, $self->{backupLocation}, $self->{backupSet}) = $sth->fetchrow_array;

    if ($sth->fetchrow_array)
    {
        die "Expecing 1 row while getting password\n";
    }
    $sth->finish();
     
    return 1;
}

##############################################################################
# MOVED to HAWAII
#  Function: setParamsFromBigBrother
#      Sets the basic paramters from Big Brother
#
#  Args:
#
#  Returns:
#
# Pre-requisites:
#
##############################################################################

sub setParamsFromBigBrother
{
    my ($self, $logStr) = @_;

    # TODO: should target_instance also be in table?  Sachin said should
    # have both... commented out for now
    # get the source and target sid
    my $sqlStatement=<<EOSql;
SELECT sd.sid,
       r.target_sid,
       r.latest_available,
       CASE WHEN r.restore_to_date IS NOT NULL
       THEN TO_CHAR(r.restore_to_date, \'yyyymmdd\')
       ELSE NULL
       END
  FROM refresh_queue r,
       databases sd
 WHERE r.source_db_instance = sd.instance
   AND r.instance = $self->{refreshQueueInst}
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting directory name: $DBI::errstr\n";
        return 0;
    }

     # should only be one
    ($self->{sourceSid}, $self->{targetSid}, my $latestAvail, my $restoreToDate) = $sth->fetchrow_array();

    if ($sth->fetchrow_array())
    {
        $$logStr .= "Expecting 1 row while getting backup information but got more\n";
        return 0
    }
    $sth->finish();
 
    if (($latestAvail eq 'Y' && defined $restoreToDate) ||
        ($latestAvail eq 'N' && !defined $restoreToDate) ||
        ($latestAvail ne 'Y' && $latestAvail ne 'N'))
    {
        
        $$logStr .= "latest_available: $latestAvail and restore_to_date: $restoreToDate in the refresh_queue table are not compatible.\nlatest_available must be Y or N.\nIf it is Y then restore_to_date can not be specified.\nIf it is N then restore_to_date must be specified.";
        return 0;
    }

    my $betweenStr = "";
    if ($latestAvail eq 'N')
    {
        my $startOraStr = "TO_DATE(\'$restoreToDate 00:00:00\', \'yyyymmdd hh24:mi:ss\')";
        my $stopOraStr = "TO_DATE(\'$restoreToDate 23:59:59\', \'yyyymmdd hh24:mi:ss\')";
        $betweenStr = "AND b2.completion_time BETWEEN $startOraStr AND $stopOraStr";

    } 

    # get the backup location and dataset 
    $sqlStatement=<<EOSql;
SELECT backup_location,
       lower(tag)
  FROM rman_backups b
 WHERE completion_time = 
       (SELECT MAX(completion_time)
          FROM rman_backups b2
         WHERE b.name = b2.name
         $betweenStr)
    AND lower(b.name) = lower(\'$self->{sourceSid}\')
EOSql

    $sth = $self->{bbDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting directory name: $DBI::errstr\n";
        return 0;
    }


     # should only be one
    ($self->{backupLocation}, $self->{backupSet}) = $sth->fetchrow_array();

    if (!defined $self->{backupLocation})
    {
        $$logStr .= "ERROR: Can\'t find backup location in rman_backups for $self->{sourceSid}\n"; 
        if ($latestAvail eq 'N')
        {
            $$logStr .= "There are not backups for $restoreToDate\n";
        }
        else
        {
            $$logStr .= "Could not find latest backup\n";
        }
 
        return 0;
    }
    else
    {
        $$logStr .= "Using backup location, $self->{backupLocation}\n";
        $$logStr .= "Using backup tag, $self->{backupSet}\n";
    }

    if ($sth->fetchrow_array())
    {
        $$logStr .= "Expecting 1 row while getting password but got more\n";
        return 0;
    }
    $sth->finish();

    # takeout last slash
    $self->{backupLocation} =~ s/\/$//g;
 
    # TODO hardcode for now, pnedev1 is missing from table 
    #$self->{backupLocation} = "/datadomain/evt/imp2/pnedev1";
    #$self->{backupSet} = "pnedev1_20131204_170303";

    return 1;
}

##############################################################################
#
#  Function: processParams
#      Sets the basic paramters from ADRAppConfig.ini
#
#  Args:
#
#  Returns:
#
#  Pre-requisites: ADRAppConfig.ini exists
#
##############################################################################

sub processParams
{
    my ($self) = @_;
    #print TRACEFH "Entering processParams\n" if ($trace);

    my $iniFile = "$ENV{ADR_LOCATION}/ADRAppConfig.ini";

    die "$iniFile does not exist.  Exiting\n" if (!-e $iniFile);

    open (INIFH, "< $iniFile") || die "Can\'t open $iniFile: $!\n";

    #print TRACEFH "Reading $iniFile\n" if ($trace);

    while (<INIFH>)
    {
        # skip if commented out or if a blank line
        next if /^\s*$/ || /^#/;

        my ($tagName, $tagValue) = parse_line('\s+', 0, $_);
        #print TRACEFH "Setting $tagName to $tagValue\n" if ($trace);
        $self->{paramHash}{$tagName} = $tagValue;
    }

    close INIFH;

    #print TRACEFH "Checking parameters\n" if ($trace);

    my $missingParams = "";
    my $badArg = 0;

    foreach my $param (@{$self->{requiredParams}})
    {
        #print TRACEFH "Checking $param\n" if ($trace);
        if (!exists $self->{paramHash}{$param} || !defined $self->{paramHash}{$param})
        {
            $badArg = 1;
            #print TRACEFH "*** Missing $param\n" if ($trace);
            $missingParams .= "$param ";
        }
     }

    if ($missingParams ne "")
    {
        print "\nERROR:  Missing parameter or value from $iniFile: $missingParams\n";
    }

    if ($self->{paramHash}{mpPercentThreshold} > 99)
    {
        $badArg = 1;
        #print TRACEFH "*** Bad mpPercentThreshold: $self->paramHash{mpPercentThreshold}\n" if ($trace);
        print "\nERROR:  mpPercentThreshold must be less than 100\n";
    }

    # TODO: is this max okay
    if ($self->{paramHash}{maxReceiveChannels} > 50)
    {
        $badArg = 1;
        #print TRACEFH "*** Bad maxReceiveChannels: $self->paramHash{maxReceiveChannels}\n" if ($trace);
        print "\nERROR:  maxReceiveChannels must be less than or equal to 50\n";
    }

    die "\nExiting.\n" if ($badArg);

    # decrypt the passwords
    foreach my $paramToDecrypt (@{$self->{paramsToDecrypt}})
    {
        $self->{paramHash}{$paramToDecrypt} = getDecryption($self->{paramHash}{$paramToDecrypt});
    }

    #print TRACEFH "Leaving processParams\n" if ($trace);
    return 1;

}

sub resetClientDbhAll
{
    my ($self, $logStr) = @_;

    $self->{adrTables}->resetClientDbh($logStr, $self->{clientDbh}) if (defined $self->{adrTables});
    $self->{adrFiles}->resetClientDbh($logStr, $self->{clientDbh}) if (defined $self->{adrFiles});
    $self->{adrPassword}->resetClientDbh($logStr, $self->{clientDbh}) if (defined $self->{adrPassword});
    $self->{adrDbaUsers}->resetClientDbh($logStr, $self->{clientDbh}) if (defined $self->{adrDbaUsers});
    $self->{adrDatabase}->resetClientDbh($logStr, $self->{clientDbh}) if (defined $self->{adrDatabase});

}

1;

