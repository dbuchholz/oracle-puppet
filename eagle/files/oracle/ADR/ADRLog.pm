package ADRLog;


# Provide functions to handle table imports and exports 

# Funtions:
# updateStepStatus - Updates the status in the adr_actual_steps table
# all others should be considered private

use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ITSTrim;

##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      bbDbh - big brother database handle
#      adrRequestId - id of step to update
#
##############################################################################

sub new
{
    my ($class, $bbDbh, $refreshQueueId, $logDetailLimit) = @_;

    my $self = {
        bbDbh => $bbDbh,
        refreshQueueId => $refreshQueueId,
        logDetailLimit => $logDetailLimit,
    };


    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: updateStepStartTime
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateStepStartTime
{
    my ($self, $refQueueDefInst) = @_;

    my $sqlStatement=<<EOSql;
UPDATE refresh_queue_def
   SET start_time = sysdate,
       end_time = NULL,
       status = \'R\'
 WHERE instance = $refQueueDefInst
EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);
    
    my $retVal = 1;
    if (!defined $rv)
    {
        $retVal = 0;
    }

    return $retVal;
}

##############################################################################
#
#  Function: updateRefreshQueueDef
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateRefreshQueueDef
{
    my ($self, $refQueueDefInst, $status, $logStr) = @_;

    my $sqlStatement=<<EOSql;
UPDATE refresh_queue_def
   SET status = \'$status\',
       end_time = sysdate
 WHERE instance = $refQueueDefInst
EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);
    
    if (!defined $rv)
    {
        return 0;
    }

    return updateLogDetails($self, $refQueueDefInst, $logStr);
}


##############################################################################
#
#  Function: updateStatusFail
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateStepStatusFail
{
    my ($self, $refQueueDefInst, $logStr) = @_;

    return updateRefreshQueueDef($self, $refQueueDefInst, 'F', $logStr);
}

##############################################################################
#
#  Function: updateStatusSuccess
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateStepStatusSuccess
{
    my ($self, $refQueueDefInst, $logStr) = @_;

    return updateRefreshQueueDef($self, $refQueueDefInst, 'S', $logStr);
    
}

##############################################################################
#
#  Function: updateRefreshQueueGeneralFail
#      This is called before the iteration through the steps so there
#      is not refresh_queue_def_inst to reference or after the refresh.  
#      This will update step 0 with a failure string and mark the entire 
#      run as failed
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateRefreshQueueGeneralFail
{
    my ($self, $logStr) = @_;

    updateStepZeroDetails($self,$logStr);

    # still try to update the refresh queue even if the step zero fails
    return updateRefreshQueue($self, 'F');
}

##############################################################################
#
#  Function: updateRefreshQueueTargetFail
#      This is called when the launcher determines that perl on the 
#      target crashes.  The last running step needs to be updated with
#      a failure string and mark the entire run as failed.
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateRefreshQueueTargetFail
{
    my ($self, $logStr) = @_;

    # Get the last running step
    my $sqlStatement=<<EOSql;
SELECT instance
  FROM refresh_queue_def
 WHERE refresh_queue_inst = $self->{refreshQueueId}
   AND status = 'R'
   AND step_number != 0
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        return 0;
    }

    # should only be one
    my $refQueueDefInst = $sth->fetchrow_array();

    if (!defined $refQueueDefInst)
    {
        return 0;
    }
    $sth->finish();

    updateStepStatusFail($self, $refQueueDefInst, $logStr); 

    return updateRefreshQueue($self, 'F');
}

##############################################################################
#
#  Function: updateStepZeroDetails
#      Update the log details for step zero in the refresh_queue_status table
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateStepZeroDetails
{
    my ($self, $logStr) = @_;

    formatLogDetails($self, \$logStr);

    my $sqlStatement=<<EOSql;
INSERT INTO refresh_queue_status
SELECT refresh_status_seq.nextval,
       instance,
       \'$logStr\'
  FROM refresh_queue_def
 WHERE refresh_queue_inst = $self->{refreshQueueId}
   AND step_number = 0

EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);

    if (!defined $rv)
    {
        return 0;
    }

    return 1;
}

##############################################################################
#
#  Function: updateRefreshQueue
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateRefreshQueue
{
    my ($self, $status) = @_;

    my $sqlStatement=<<EOSql;
UPDATE refresh_queue
   SET state = \'$status\'
 WHERE instance = $self->{refreshQueueId}
EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);

    if (!defined $rv)
    {
        return 0;
    }

    my $retVal = 1;
    if ($status eq 'F' || $status eq 'S')
    {
         # This is the end, so record endtime for step zero

    $sqlStatement=<<EOSql;
UPDATE refresh_queue_def
   SET end_time = sysdate,
       status = \'$status\'
 WHERE refresh_queue_inst = $self->{refreshQueueId}
   AND step_number = 0
EOSql

        $rv = $self->{bbDbh}->do($sqlStatement);

        if (!defined $rv)
        {
            $retVal = 0;
        }
    }
    elsif ($status eq 'H')
    {
        # This is the start, so record starttime for step zero
    $sqlStatement=<<EOSql;
UPDATE refresh_queue_def
   SET start_time = sysdate,
       end_time = NULL,
       status = 'R'
 WHERE refresh_queue_inst = $self->{refreshQueueId}
   AND step_number = 0
EOSql

        $rv = $self->{bbDbh}->do($sqlStatement);

        if (!defined $rv)
        {
            $retVal = 0;
        }
    }

    return $retVal;

}

##############################################################################
#
#  Function: updateDBRefreshes
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
sub updateDBRefreshes
{

    my ($self, $targetSid, $sourceSid, $hostName) = @_;

    my $sqlStatement=<<EOSql;
INSERT INTO inf_monitor.db_refreshes
SELECT inf_monitor.db_refreshes_seq.nextval,
       \'$targetSid\',
       \'$hostName\',
       (SELECT c.code 
         FROM inf_monitor.clients c, 
              inf_monitor.databases d 
        WHERE LOWER(d.sid) = \'$targetSid\'
          AND d.client = c.instance), 
        \'Standard Refresh\',
        \'$sourceSid\',
        NULL,
        \'Automatic Database Refresh\',
        r.start_time, 
        TRUNC((r.end_time - r.start_time) * 24 * 60, 2) || ' Min'
   FROM refresh_queue_def r
  WHERE r.refresh_queue_inst = $self->{refreshQueueId}
    AND r.step_number = 0
EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);

    if (!defined $rv)
    {
        return 0;
    }

    return 1;
}

##############################################################################
#
#  Function: updateLogDetails
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub updateLogDetails
{
    my ($self, $refQueueDefInst, $logStr) = @_;

    formatLogDetails($self, \$logStr);

    my $sqlStatement=<<EOSql;
INSERT INTO refresh_queue_status (instance, refresh_queue_def_inst, log_detail)
   VALUES (refresh_status_seq.nextval, $refQueueDefInst, \'$logStr\')
EOSql

    my $rv = $self->{bbDbh}->do($sqlStatement);

    my $retVal = 1;
    if (!defined $rv)
    {
        $retVal = 0;
    }

    return $retVal;
}

##############################################################################
#
#  Function: formatLogDetails
#
#  Args: Remove quotes and shorten string if is over the logDetailLimit
#
# Pre-requisites:
#
##############################################################################

sub formatLogDetails
{
    my ($self, $logStr) = @_;

    # remove any quotes
    $$logStr =~ s/\"//g;
    $$logStr =~ s/\'//g;

    if (length($$logStr) > $self->{logDetailLimit})
    {
        # the string is too long
        my $message = "NOTE: The log string is too long.  See log file on target.  The middle of the log message has been removed.\n";
        my $middleStr = "\nPARTIAL DELETE\n";
        
        use integer; 
        my $keepLength = ($self->{logDetailLimit} - length($message) - length($middleStr)) / 2;

        my $beginStr = substr($$logStr, 0, $keepLength);
        my $endStr = substr($$logStr, -$keepLength, $keepLength);

        $$logStr = $message . $beginStr . $middleStr . $endStr;
    }

    return 1;
}

1;
