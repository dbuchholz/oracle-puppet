package ADREngines;


# Provide functions to handle user and grant files

# Funtions:
# stopEngines - Stops all engines via the EEM
# startEngines - Starts all engines via the EEM
# all others should be considered private

use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;

##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      bbDbh - big brother database handle
#      clientDbh - client database handle
#      refreshQueueInst - refresh queue instance 
#
##############################################################################

sub new
{
    my ($class, $bbDbh, $checkSeconds, $checkMultiplier, $refreshQueueInst) = @_;

    my $self = {
        workDir => "$ENV{ADR_LOCATION}/$refreshQueueInst" . "_workDir",
        bbDbh => $bbDbh,
        checkSeconds => $checkSeconds,
        checkMultiplier => $checkMultiplier,
        refreshQueueInst  => $refreshQueueInst
    };

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: stopEngines
#      Stops all engines via the EEM.  Calls a stored procedure to do this
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub stopEngines
{
    my ($self, $logStr) = @_;
 
    $$logStr .= "In stopEngines\n";

    if (!callEngineSP($self, $logStr, 'stop'))
    {
        # logStr will already be filled out
        return 0;
    }

    $$logStr .= "Stopping engines successful";
    return 1;
}

##############################################################################
#
#  Function: startEngines
#      Starts all engines via the EEM.  Calls a stored procedure to do this
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub startEngines
{
    my ($self, $logStr) = @_;

    $$logStr .= "In startEngines\n";

    if (!callEngineSP($self, $logStr, 'start'))
    {
        # logStr will already be filled out
        return 0;
    }

    $$logStr .= "Starting engines successful";
    return 1;
}

##############################################################################
#
#  Function: callEngineSP
#      Calls Engine Start/Stop stored procedure
#
#  Args:
#     action - start or stop
#
# Pre-requisites:
#
##############################################################################

sub callEngineSP
{
    my ($self, $logStr, $action) = @_;

    $$logStr .= "In callEngineSP for $action\n";

    my $storedProc;

    if ($action eq 'start')
    {
        $storedProc = "ssp_manage.adr_start_region";
    }
    elsif ($action eq 'stop')
    {
        $storedProc = "ssp_manage.adr_stop_region";
    }
    else
    {
        $$logStr .= "action must be start or stop\n";
        return 0;
    }

    my $sth = $self->{bbDbh}->prepare("BEGIN $storedProc($self->{refreshQueueInst}, ?); END;");

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $sth->bind_param_inout(1, \my $regionSSInstance, 38);

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while running $storedProc for $self->{refreshQueueInst}: $DBI::errstr\n";
        return 0;
    }
    $sth->finish();

    if ($regionSSInstance == -1)
    {
        $$logStr .= "$storedProc returned an error\n";
        return 0;
    }

    if (!waitAndCheckStatus($self, $logStr, $regionSSInstance))
    {
        $$logStr .= "Could not $action engines\n";
        return 0;
    }

    return 1;
}

##############################################################################
#
#  Function: waitAndCheckStatus
#      Checks the EEM status and waits for the engines to either start and stop.
#      Calls a stored procedure to do this
#
#  Args:
#     instance - instance of the region_start_stop table
#
# Pre-requisites:
#
##############################################################################

sub waitAndCheckStatus
{
    my ($self, $logStr, $instance) = @_;

    $$logStr .= "In waitAndCheckStatus for $instance\n";

    my $sth = $self->{bbDbh}->prepare("BEGIN ssp_manage.adr_get_region_status($instance, ?, ?); END;");

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $sth->bind_param_inout(1, \my $status, 20);
    $sth->bind_param_inout(2, \my $result, 2000);

    for (my $i = 0; $i < $self->{checkMultiplier}; $i++)
    {
        sleep $self->{checkSeconds};
    
        my $rv = $sth->execute();
        if (!defined $rv)
        {
            $$logStr .= "Failed while running ssp_manage.adr_get_region_status: $DBI::errstr\n";
            return 0;
        }

        if ($status eq 'SUCCESS')
        {
             last;
        }
        elsif ($status eq 'FAILURE' || $status eq 'ABANDONED')
        {
            if (!defined $result)
            {
                $result = "Result field is empty";
            }
           
            $$logStr .= "Could not stop engines:  $result\n";
            return 0;
        }
        elsif ($status ne 'PENDING' && $status ne 'RUNNING')
        {
            if (!defined $result)
            {
                $result = "Result field is empty";
            }

            $$logStr .= "Error: $status with result: $result\n";
            return 0;
         
        }
    }

    if (!defined $result)
    {
        $result = "Result field is empty";
    }

    $$logStr .= "Status: $status Result: $result.\n";

    if ($status ne 'SUCCESS')
    {
        return 0;
    }

    return 1;
}

1;

