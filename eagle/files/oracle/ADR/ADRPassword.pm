package ADRPassword;


# Provide functions to create reset Oracle Passwords

# Funtions:
# creatPWFile - Create orapw file
# updatePWDatabase - Update passwords in DB
# all others should be considered private

use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;
use Sys::Hostname;


##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      targetSid - target sid to create the RestoreDataTempFiles for
#      sourceSid - source sid used as a base to create RestoreDataTempFiles
#      bbDbh - big brother database handle
#      clientDbh - client database handle
#
##############################################################################

sub new
{
    my ($class, $targetSid, $sourceSid, $bbDbh, $clientDbh, $refreshQueueInst) = @_;

    my $self = {
        workDir => "$ENV{ADR_LOCATION}/$refreshQueueInst" . "_workDir",
        targetSid => $targetSid,
        sourceSid => $sourceSid,
        bbDbh => $bbDbh,
        clientDbh => $clientDbh,
        refreshQueueInst => $refreshQueueInst
    };

    # TODO - do we want to check for pre-reqs?

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: createDefaultPWFile
#      Create orapw<sid> file with an "eagle" password
#
#  Args:
#      type - S or T for source or target
#
# Pre-requisites:
#
##############################################################################
# TODO, test
sub createDefaultPWFile
{
    my ($self, $logStr, $type) = @_;

    $$logStr .= "In createDefaultPWFile for $type\n";

    my $sid = '';
    if ($type eq 'S')
    {
        $sid = $self->{sourceSid};
    }
    elsif ($type eq 'T')
    {
        $sid = $self->{targetSid};
    }
    else
    {
        $$logStr .= "Incorrect type, $type.  Type must be S (source) or T (target)\n";
        return 0;
    }

    my $orapwd = "$ENV{ORACLE_HOME}/bin/orapwd";

    my $file = "$ENV{ORACLE_HOME}/dbs/orapw$sid";
    $$logStr .= "Password file is $file\n";

    if (-e $file)
    {
        my $saveFile = $file . "_" . getDateTimeStr(); 
        $$logStr .= "Renaming $file to $saveFile\n";
        if (!rename $file, $saveFile)
        {
            $$logStr .= "Rename failed.  Error is $!\n";
            return 0;
        }
    }
    else
    {
        $$logStr .= "No exising $file\n";
    }
    
    my $cmd = "$orapwd file=$file password=eagle entries=20";

    $$logStr .= "About to run $cmd\n";

    # TODO, no idea what output is going to look like
    if (!open (PW_PIPE, "$cmd 2>&1 1>/dev/null |"))
    {
        $$logStr .= "Can't execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <PW_PIPE>;
    foreach (@outp)
    {
        #TODO, check something
    } 

    close PW_PIPE;

    $$logStr .= "Create password file successful\n";
    return 1;
}

##############################################################################
#
#  Function: updatePWDatabase
#      Resets the passwords in the target database  
#
#  Args:
#
# Pre-requisites:
#
##############################################################################
# TODO, test
sub updatePWDatabase
{
    my ($self, $logStr) = @_;

    $$logStr .= "In updatePWDatabase\n";

    my $host = hostname();
    # strip out domain
    $host =~ s/\..+\..+//;

    $$logStr .= "About to get usernames and passwords from Big Brother\n";
    my $sqlStatement=<<EOSql;
SELECT username, 
       bb_get(nvl(temp_code,code)) 
  FROM databases d,
       machines m,
       user_codes u
 WHERE m.instance = d.mac_instance
   AND d.instance = u.db_instance
   AND d.sid = \'$self->{targetSid}\'
   AND m.name = \'$host\'
EOSql

    my $sth = $self->{bbDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting users and and passwords: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $userName, \my $passWord);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding users and passwords: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "About to update passwords in the target\n";
 
    $sqlStatement = "";
    while ($sth->fetch())
    {
        $sqlStatement .= "ALTER USER $userName IDENTIFIED BY \"$passWord\"\;\n";
    }
    $sth->finish();

        my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRUpdatePWSql" . getDateTimeStr() . ".log";

    # ignore if the error is due to a user not existing (ORA-01918)
    my %ignoreErrors = ('ORA-01918' => 1);
 
    if (!runSqlAsSysdba($logStr, $sqlStatement, $logFile, \%ignoreErrors))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Successfully set passwords in the target\n";

    return 1;
}

sub resetClientDbh
{
    my ($self, $logStr, $dbh) = @_;

    $$logStr .= "In resetClientDbh\n";

    $self->{clientDbh} = $dbh;
}

1;

