package ADRTables;


# Provide functions to handle table imports and exports 

# Funtions:
# exportTables - Create ADRTables_yyyymmdd_hhmmss.dmp
# exportDPTables - Create $refreshQueueInst_workDir/$refreshQueueInst_ADRDPTables_yyyyddmm_hhmmss.dmp
# exportSchemas - Create ADRSchemas_yyyymmdd_hhmmss.dmp 
#                 and ADRSchemasGrants_yyyymmdd_hhmmss.dmp
# importTables - Imports tables from  ADRTables_yyyymmdd_hhmmss.dmp
# importDPTables - Import tables from $refreshQueueInst_workDir/$refreshQueueInst_ADRDPTables_yyyyddmm_hhmmss.dmp
# truncateTables - Truncates tables
# exportDataPumpStruct - Exports the datapump structure
# all others should be considered private

use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;
use File::Basename;
use File::Path;

##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      targetSid - target sid 
#      bbDbh - big brother database handle
#      clientDbh - client database handle
#      clientDbPW - system password for the client database
#
##############################################################################

sub new
{
    my ($class, $targetSid, $refreshQueueInst, $fullTargetPath, $bbDbh, $clientDbh, $clientDbPW) = @_;

    my $self = {
        workDir => "$fullTargetPath/$refreshQueueInst" . "_workDir",
        targetSid => $targetSid,
        refreshQueueInst => $refreshQueueInst,
        bbDbh => $bbDbh,
        clientDbh => $clientDbh,
        clientDbPW => $clientDbPW,
        hidePW => "********",
        tableFile => undef,
		tableDPFile => undef,
        schemaFile => undef,
        schemaGrantFile => undef,
        fullTargetPath => $fullTargetPath
    };

    # TODO - do we want to check for pre-reqs?

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: exportTables
#      Create ADRTables_yyyyddmm_hhmmss.dmp
#
#  Args:
#     logStr - referece to log string that will filled while processing
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################
sub exportTables
{
    my ($self, $logStr, $tables) = @_;
   
    $$logStr .= "In exportTables for $tables\n";

    my $dateTimeStr = getDateTimeStr();
    $self->{tableFile} = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRTables_" . $dateTimeStr . ".dmp";   

    $$logStr .= "Export file is $self->{tableFile}\n";
    my $logFile =  $self->{tableFile};
    $logFile =~ s/ADRTables/ADRTables_exp/;
    $logFile =~ s/dmp/log/;

    my $exp = "$ENV{ORACLE_HOME}/bin/exp";

    my $cmd = "$exp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} file=$self->{tableFile} log=$logFile tables=$tables statistics=none";
    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/; 

    $$logStr .= "About to run $hidePWCmd\n";

    # for some reason the log output of this goes to stderr, so use, 2>&1 1>/dev/null
    if (!open (EXP_PIPE, "$cmd 2>&1 1>/dev/null |")) 
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <EXP_PIPE>;
    close EXP_PIPE; 
 
    my $retVal = 1;
    # The last line of the output should say the export is successful
    if ($outp[@outp - 1] !~ /^Export terminated successfully without warnings.$/)

    {
        $$logStr .= "Export failed.  Last line of output is, $outp[@outp - 1]\nSee $logFile on the target machine\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .= "Export succesful\n";
    }
 
    return $retVal;
    
}
##############################################################################
#
#  Function: exportDPTables
#      Create ADRDPTables_yyyyddmm_hhmmss.dmp
#
#  Args:
#     logStr - reference to log string that will filled while processing
#     tables - comma separated list of tables to be exported with DataPump
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub exportDPTables
{
    my ($self, $logStr, $tables) = @_;
	$tables = uc($tables);
	
    $$logStr .= "In exportDPTables for $tables\n";

    my $dateTimeStr = getDateTimeStr();
	
    my $expdpTablesDir = 'expdp_tables_dir';
    my $expdpTablesLogDir = 'expdp_tables_log_dir';
    my $rv;
	
    $$logStr .= "Creating directory $expdpTablesDir in sql\n";
    $rv = $self->{clientDbh}->do("CREATE OR REPLACE directory $expdpTablesDir AS \'$self->{workDir}\'");
    
    if (!defined $rv)
    {
        $$logStr .= "Failed while creating $expdpTablesDir: DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Granting read,write on directory $expdpTablesDir\n";
    $rv = $self->{clientDbh}->do("GRANT read,write ON directory $expdpTablesDir TO public");
    if (!defined $rv)
    {
        $$logStr .= "Failed while granting read,write on $expdpTablesDir: DBI::errstr\n";
        return 0;
    }

    $self->{tableDPFile} = "$self->{refreshQueueInst}_ADRDPTables_" . $dateTimeStr . ".dmp";
    
	my $logFile = "$self->{refreshQueueInst}_ADRDPTables_exp_" . $dateTimeStr . ".log";

    $$logStr .= "Data pump export file is $self->{tableDPFile}\n";

    my $expdp = "$ENV{ORACLE_HOME}/bin/expdp";

    my $cmd = "$expdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} tables=$tables directory=$expdpTablesDir dumpfile=$self->{tableDPFile} logfile=$logFile";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/; 

    $$logStr .= "About to run $hidePWCmd\n";

    # for some reason the log output of this goes to stderr, so use, 2>&1 1>/dev/null
    if (!open (EXP_PIPE, "$cmd 2>&1 1>/dev/null |")) 
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <EXP_PIPE>;
    close EXP_PIPE; 
 
    my $retVal = 1;
    # The last line of the output should say the export is successful
    if ($outp[@outp - 2] !~ /^Job .+ successfully completed at [A-Z][a-z]+ [A-Z][a-z]+ \d\d? \d\d:\d\d:\d\d/)
    {
        $$logStr .= "Export failed.  Last line of output is, $outp[@outp - 1]\nSee $logFile on the target machine\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .= "Data pump export succesful\n";
    }
 
    return $retVal;
    
}

##############################################################################
#
#  Function: exportSchemas
#      Creates ADRSchemas_yyyyddmm_hhmmss.dmp
#
#  Args:
#     schemas - comma separated list of schemas
#
# Pre-requisites:
#     The schemas exist in the target database
#
##############################################################################

sub exportSchemas
{
    my ($self, $logStr, $schemas) = @_;
 
    # convert schemas to uppercase
    $schemas = uc($schemas);

    $$logStr .= "In exportSchemas for $schemas\n";

    my $dateTimeStr = getDateTimeStr();

    my $expSchemaDir = 'exp_schema_dir';

    $$logStr .= "Dropping directory $expSchemaDir in sql\n";
    my $rv = $self->{clientDbh}->do("DROP directory $expSchemaDir");
    if (!defined $rv)
    {
        # If the error string starts with ORA-04043, it just
        # means the schema does not exist which is okay.
        if ($DBI::errstr =~ /^ORA-04043/)
        {
            $$logStr .= "$expSchemaDir does not exist\n";
        }
        else
        {
            $$logStr .= "failed while dropping $expSchemaDir: $DBI::errstr\n";
            return 0;
        }
    }

    $$logStr .= "Creating directory $expSchemaDir in sql\n";
    $rv = $self->{clientDbh}->do("CREATE directory $expSchemaDir AS \'$self->{workDir}\'");
    
    if (!defined $rv)
    {
        $$logStr .= "Failed while creating $expSchemaDir: DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Granting read,write on directory $expSchemaDir\n";
    $rv = $self->{clientDbh}->do("GRANT read,write ON directory $expSchemaDir TO public");
    if (!defined $rv)
    {
        $$logStr .= "Failed while granting read,write on $expSchemaDir: DBI::errstr\n";
        return 0;
    }

    my $schemaFileName = "$self->{refreshQueueInst}_ADRSchemas_" . $dateTimeStr . ".dmp";
    $self->{schemaFile} = "$self->{workDir}/$schemaFileName";

    $$logStr .= "Export file is $self->{schemaFile}\n";
    my $logFile =  $schemaFileName;
    $logFile =~ s/ADRSchemas/ADRSchemas_exp/;
    $logFile =~ s/dmp/log/;

    my $expdp = "$ENV{ORACLE_HOME}/bin/expdp";

    my $cmd = "$expdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} directory=$expSchemaDir dumpfile=$schemaFileName schemas=$schemas logfile=$logFile content=all exclude=statistics,grant";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    if (!open (EXP_PIPE, "$cmd 2>&1 1>/dev/null |"))
    {
        $$logStr .= "Cannot execute $hidePWCmd\nError is $!\n";
        return 0;
    }

    my @outp = <EXP_PIPE>;
    close EXP_PIPE;

    if ($outp[@outp - 2] !~ /^Job .+ successfully completed at \d\d:\d\d:\d\d/)
    {
        $$logStr .= "Export failed.  Last line of output is, $outp[@outp - 2].  See $logFile on the target machine\n";
        return 0;
    }

    $$logStr .= "Export succesful\n";

    return 1;
}

##############################################################################
#
#  Function: importTables
#      Imports tables from  ADRTables_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the dmp file
#
##############################################################################

sub importTables
{
    my ($self, $logStr) = @_;

    $$logStr .= "In importTables\n";

    if (!defined $self->{tableFile})
    {
        # This is a rerun.  Use the latest export file.  This will
        # be the last file on the glob array.  TODO, add queue number

        my $startFileName =  "$self->{workDir}/$self->{refreshQueueInst}_ADRTables_";
        my @files = <$startFileName*.dmp>;
        $self->{tableFile} = $files[@files - 1];
    }

    $$logStr .= "Export file is $self->{tableFile}\n";

    my $impLogFile = $self->{tableFile};
    $impLogFile =~ s/dmp/log/;
    $impLogFile =~ s/ADRTables_/ADRTables_imp_/;

    my $imp = "$ENV{ORACLE_HOME}/bin/imp";

    my $cmd = "$imp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} file=$self->{tableFile} log=$impLogFile full=y ignore=y";
    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    # for some reason the log output of this goes to stderr, so use, 2>&1 1>/dev/null
    if (!open (IMP_PIPE, "$cmd 2>&1 1>/dev/null |"))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <IMP_PIPE>;
    my $retVal = 1;

    # The last line of the output should say the export is successful
    if ($outp[@outp - 1] !~ /^Import terminated successfully without warnings.$/)
    {
        $$logStr .= "Import failed.  Last line of output is, $outp[@outp - 1].  See $impLogFile on the target machine\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .= "Import succesful\n";
    }

    close IMP_PIPE;

    return $retVal;
}

##############################################################################
#
#  Function: importDPTables
#      Imports tables from  ADRDPTables_yyyymmdd_hhmmss.dmp
#
#  Args:
#     tables - comma separated list of tables to be imported by Data Pump utility
#
# Pre-requisites:
#     The tables exist in the dmp file
#
##############################################################################

sub importDPTables
{
    my ($self, $logStr) = @_;

    $$logStr .= "In importDPTables\n";

    if (!defined $self->{tableDPFile})
    {
        # This is a rerun.  Use the latest export file.  This will
        # be the last file on the glob array.  TODO, add queue number

        my $startFileName =  "$self->{workDir}/$self->{refreshQueueInst}_ADRDPTables_";
        my @files = <$startFileName*.dmp>;
		if (!@files)
		{
            $$logStr .= "Cannot find tables export data pump file.  Either exportDPTables has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
        $self->{tableDPFile} = $files[@files - 1];
    }

    $$logStr .= "Export file is $self->{tableDPFile}\n";

    my $impLogFile = $self->{tableDPFile};
    $impLogFile =~ s/dmp/log/;
    $impLogFile =~ s/ADRDPTables_/ADRDPTables_imp_/;
    ##$impLogFile =~ s/($self->{workDir})\//$1\/logs\//;
    #$impLogFile =~ s/.+\///;
    $impLogFile =~ s/$self->{workDir}\///; 
	
    my $impDPTablesDir = 'expdp_tables_dir';
    my $rv;
	
    $$logStr .= "Creating directory $impDPTablesDir in sql\n";
    $rv = $self->{clientDbh}->do("CREATE OR REPLACE directory $impDPTablesDir AS \'$self->{workDir}\'");
    
    if (!defined $rv)
    {
        $$logStr .= "Failed while creating $impDPTablesDir: $DBI::errstr\n";
        return 0;
    }

    my $TableDPFileName = basename($self->{tableDPFile});
    my $impdp = "$ENV{ORACLE_HOME}/bin/impdp";
    
    my $cmd = "$impdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} directory=$impDPTablesDir dumpfile=$TableDPFileName logfile=$impLogFile TABLE_EXISTS_ACTION=APPEND";
    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    # for some reason the log output of this goes to stderr, so use, 2>&1 1>/dev/null
    if (!open (IMP_PIPE, "$cmd 2>&1 1>/dev/null |"))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <IMP_PIPE>;
    my $retVal = 1;

    # The last line of the output should say the export is successful
    #if ($outp[@outp - 1] !~ /^Import terminated successfully without warnings.$/)
    if($outp[@outp - 2] !~ /^Job .+ successfully completed at [A-Z][a-z]+ [A-Z][a-z]+ \d{1,2} \d\d:\d\d:\d\d/)
    {
        $$logStr .= "Import failed.  Last line of output is:\n $outp[@outp - 2].\n  See $impLogFile on the target machine\n";
        $retVal = 0;
    }
    else
    {
        $$logStr .= "Import succesful\n";
    }

    close IMP_PIPE;

    return $retVal;
}

##############################################################################
#
#  Function: importSchemas
#      Imports schemas from  ADRSchemas_yyyymmdd_hhmmss.dmp 
#
#  Args:
#
# Pre-requisites:
#     The schemas exist in the dmp file
#
##############################################################################

sub importSchemas
{
    my ($self, $logStr) = @_;

    # If this is a rerun, and the file is already created
    # the file will have to be looked up.

    if (!defined $self->{schemaFile})
    {
        # This is a rerun.  Use the latest export files.  This will
        # be the last file on the glob array. 

        my $startFileName =  "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRSchemas_";
        my @files = <$startFileName*.dmp>;
        if (!@files)
        {
            $$logStr .= "Cannot find schema export file.  Either exportSchemas has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }

        $self->{schemaFile} = $files[@files - 1];

    }

    $$logStr .= "Export file is $self->{schemaFile}\n";

    # Get schema list from the export log file 
    my $expLogFile = $self->{schemaFile};
    $expLogFile =~ s/dmp/log/;
    $expLogFile =~ s/ADRSchemas_/ADRSchemas_exp_/;

    if (!open (EXPLOG_FH, "< $expLogFile"))
    {
        $$logStr .= "Cannot open $expLogFile: $!\n";
        return 0;
    }

    $$logStr .= "Getting schema list from $expLogFile\n";

    # The one line needed looks like (all one line,
    # Starting "SYS"."SYS_EXPORT_SCHEMA_01":
    # userid="/******** AS SYSDBA" directory=exp_bgim_app_rwro
    # dumpfile=bgim_app_rwro.dmp schemas=BGIM_APP_RW,BGIM_APP_RO
    # logfile=export_bgim_app_rwro.log content=all

    my $schemas = '';
    
    while (<EXPLOG_FH>) 
    {
        if (/^Starting.+schemas=(.+?)\s+/)
        {
            $schemas = $1;
            last;
        }
    }
    close EXPLOG_FH;

    if ($schemas eq '')
    {
        $$logStr .= "Could not find schema list in log file\n";
        return 0;
    }

    $$logStr .= "Found schemas to import, $schemas\n";

    # Handle directory create creation

    my $expSchemaDir = 'exp_schema_dir';
    $$logStr .= "Dropping directory $expSchemaDir in sql\n";
    my $rv = $self->{clientDbh}->do("DROP directory $expSchemaDir");
    if (!defined $rv)
    {
        # If the error string starts with ORA-04043, it just
        # means the schema does not exist which is okay.
        if ($DBI::errstr =~ /^ORA-04043/)
        {
            $$logStr .= "$expSchemaDir does not exist\n";
        }
        else
        {
            $$logStr .= "failed while dropping $expSchemaDir: $DBI::errstr\n";
            return 0;
        }
    }

    $$logStr .= "Creating directory $expSchemaDir in sql\n";
    $rv = $self->{clientDbh}->do("CREATE directory $expSchemaDir AS \'$self->{workDir}\'");

    if (!defined $rv)
    {
        $$logStr .= "Failed while creating $expSchemaDir: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Dropping schema users\n";
    my @schemaArr = split (',', $schemas);

    foreach my $user (@schemaArr)
    {
        $$logStr .= "Dropping $user\n";
        $rv = $self->{clientDbh}->do("DROP user $user CASCADE");
        if (!defined $rv)
        {
            if ($DBI::errstr =~ /^ORA-01918/)
            {
                $$logStr .= "$user does not exist\n";
            }
            else
            {
                $$logStr .= "Failed while dropping user $user: $DBI::errstr\n";
                return 0;
            }
        }
    }

    my $schemaFileName = basename($self->{schemaFile});

    my $fullImpLogFile = $expLogFile;
    $fullImpLogFile =~ s/exp/imp/;

    my $impLogFile = basename($fullImpLogFile);

    my $impdp = "$ENV{ORACLE_HOME}/bin/impdp";

    my $cmd = "$impdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} directory=$expSchemaDir dumpfile=$schemaFileName schemas=$schemas logfile=$impLogFile content=all";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    # for some reason the log output of this goes to stderr, so use, 2>&1 1>/dev/null
    if (!open (IMP_PIPE, "$cmd 2>&1 1>/dev/null |"))
    {
        $$logStr .= "Cannot execute command.  Error is $!\n";
        return 0;
    }

    my @outp = <IMP_PIPE>;
    close IMP_PIPE;

    # The last line of the output will always have errors.  If an objects
    # exist in the database it is flagged as an error.  Need to look for
    # errors and ignore certain ones

    # ignore if the error is due to compliation warnings (ORA-39082)
    my %ignoreErrors = ('ORA-39082' => 1);
    if (!checkLogForErrors($logStr, $fullImpLogFile, 'sql', \%ignoreErrors))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Import successful\n";

    return 1;
}


##############################################################################
#
#  Function: truncateTables
#      Truncates tables
#
#  Args:
#     tables - comma separated list of tables
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub truncateTables
{
    my ($self, $logStr, $tables) = @_;

    $$logStr .= "In truncateTables for $tables\n";

    # DBI Oracle does not allow for executing multiple statements
    # at once, so each truncate needs to be separate.  Also unable
    # to only prepare once since the variable that is changing is the
    # tablename

    my @tables = split (',', $tables);
    my $retVal = 1;

    $$logStr .= "About to truncate tables.\n";
    foreach my $table (@tables)
    {
        my $rv = $self->{clientDbh}->do("TRUNCATE TABLE $table");
        if (!defined $rv)
        {
            # If the error string starts with ORA-00942, it just
            # means the table does not exist which is okay. 
            if ($DBI::errstr =~ /^ORA-00942/)
            {
                $$logStr .= "$table does not exist\n";
            }
            else
            {
                $$logStr .= "failed while truncating $table: $DBI::errstr\n";
                $retVal = 0;
            }
           
        }
    }

    if ($retVal)
    {
        $$logStr .= "Truncate successful\n";
    }

    return $retVal;
}

##############################################################################
#
#  Function: truncateSysAudTable
#      Truncantes SYS.AUD$ table
#
#  Args:
#
# Pre-requisites:
#     The tables exist in the target database
#
##############################################################################

sub truncateSysAudTable
{
    my ($self, $logStr) = @_;

    $$logStr .= "In truncateSysAudTable\n"; 

    my $retVal = 1;

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRTrunSysAudSql_" . getDateTimeStr() . ".log";
    if (!runSqlAsSysdba($logStr, "TRUNCATE TABLE sys.aud\$", $logFile))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Truncate of sys.aud\$ table successful\n";
    return 1;

}

##############################################################################
#
#  Function: exportDataPumpStruct
#      Exports the datapump structure
#
#  Args:
#
# Pre-requisites:
#
##############################################################################

sub exportDataPumpStruct
{

    my ($self, $logStr) = @_;

    $$logStr .= "In exportDataPumpStruct\n";

    $$logStr .= "Getting NLS_LANG\n";

    my $sqlStatement=<<EOSql;
SELECT lang.value || '_' || terr.value || '.' || chrset.value
 FROM v\$nls_parameters lang,
      v\$nls_parameters terr,
      v\$nls_parameters chrset
WHERE lang.parameter='NLS_LANGUAGE' 
  AND terr.parameter='NLS_TERRITORY' 
  AND chrset.parameter='NLS_CHARACTERSET'
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting NLS_LANG: $DBI::errstr\n";
        return 0;
    }

    # should only be one
    $ENV{NLS_LANG} = $sth->fetchrow_array();

    if (!defined $ENV{NLS_LANG})
    {
        $$logStr .= "Could not get NLS_LANG from the database\n";
        return 0;
    }

    if ($sth->fetchrow_array())
    {
        $$logStr .= "Expecing 1 row while getting NLS_LANG, but got more\n";
        return 0;
    }
    $sth->finish();

    my $dpDir = "/datadomain/export/$self->{targetSid}";

    $$logStr .= "export dir is $dpDir\n";

    if (!-d $dpDir)
    {
        $$logStr .= "$dpDir does not exist, creating it\n";

        eval {mkpath($dpDir)};
        if ($@)
        {
            $$logStr .= "Cannot make directory $dpDir: $@\n";
            return 0;
        }
    }

    $$logStr .= "dropping directory in sql\n";

    $rv = $self->{clientDbh}->do("DROP directory DATA_PUMP_DIR");
    if (!defined $rv)
    {
        $$logStr .= "Failed while dropping $dpDir: DBI::errstr\n";
        return 0;
    }

    $$logStr .= "creating directory in sql\n";
    $rv = $self->{clientDbh}->do("CREATE directory DATA_PUMP_DIR AS \'$dpDir\'");
    if (!defined $rv)
    {
        $$logStr .= "Failed while creating $dpDir: DBI::errstr\n";
        return 0;
    }

    my $expdp = "$ENV{ORACLE_HOME}/bin/expdp";
    my $impdp = "$ENV{ORACLE_HOME}/bin/impdp";

    my $dateTimeStr = getDateTimeStr();
    my $fileSize = "30G";
    my $dmpFile = "export_$self->{targetSid}_$dateTimeStr.dmpdp";
    my $sqlFile = "ddl_structure_$self->{targetSid}_$dateTimeStr.log";
    my $expLogFile = "export_datapump_structure_$ENV{ORACLE_SID}_$dateTimeStr.log";
    my $impLogFile = "import_datapump_structure_$ENV{ORACLE_SID}_$dateTimeStr.log";
    my $jobName = "EXPDP_$dateTimeStr";

    my $cmd = "$expdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} dumpfile=$dmpFile filesize=$fileSize logfile=$expLogFile full=Y content=METADATA_ONLY job_name=$jobName";

    my $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    if (!open (EXP_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute $hidePWCmd\nError is $!\n";
        return 0;
    } 
    close EXP_PIPE;    

    # look at the log to determine if successful.  At the time of writng this, 
    # unable to test, so the command line output is unknown.  

    $expLogFile = $dpDir . "/" . $expLogFile; 
    $$logStr .= "Checking $expLogFile\n";
    $cmd = "tail -1 $expLogFile";

    if (!open (T_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute $cmd\nError is $!";
        return 0;
    }

    # will only be one line
    my ($line) = <T_PIPE>;
    close T_PIPE;

    if (!defined $line || $line !~ /^Job .+\.\"$jobName\" successfully completed at \d\d:\d\d:\d\d/)
    {
        $$logStr .= "Export of datapump structure failed.  See $expLogFile\n";
        return 0;
    }
    
    $cmd = "$impdp SYSTEM/$self->{clientDbPW}\@$self->{targetSid} dumpfile=$dmpFile sqlfile=$sqlFile full=Y logfile=$impLogFile";

    $hidePWCmd = $cmd;
    $hidePWCmd =~ s/$self->{clientDbPW}/$self->{hidePW}/;

    $$logStr .= "About to run $hidePWCmd\n";

    if (!open (IMP_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute $hidePWCmd\nError is $!";
        return 0;
    }
    close IMP_PIPE;    

    $$logStr .= "Checking $impLogFile\n";
    $impLogFile = $dpDir . "/" . $impLogFile; 
    $cmd = "tail -1 $impLogFile";

    if (!open (T_PIPE, "$cmd |"))
    {
        $$logStr .= "Cannot execute $cmd\nError is $!";
        return 0;
    }

    # will only be one line
    ($line) = <T_PIPE>;
    close T_PIPE;

    if ($line !~ /^Job .+\..+ successfully completed at \d\d:\d\d:\d\d/)
    {
        $$logStr .= "Import of datapump structure failed.  See $impLogFile\n";
        return 0;
    }

    $$logStr .= "Export of data pump struction successful\n";
    return 1;
}

sub resetClientDbh
{
    my ($self, $logStr, $dbh) = @_;

    $$logStr .= "In resetClientDbh\n";

    $self->{clientDbh} = $dbh;
}

1;
