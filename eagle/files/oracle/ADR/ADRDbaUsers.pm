package ADRDbaUsers;


# Provide functions to handle user and grant files

# Funtions:
# createSchemaGrantFile - Creates ADRSchemaPrivs_yyyyddmm_hhmmss.sql
# runSchemaGrantFile - Runs ADRSchemaPrivs_yyyyddmm_hhmmss.sql
# createPreserveUserFile - Create PreserveUser_<yyyymmmdd_hhmmss>.sql
# runPreserveUserFile - Run PreserveUser_<yyyymmmdd_hhmmss>.sql
# revokeProxies - revokes the proxies
# all others should be considered private

use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
}

use DBI;
use ADRFunctions;

##############################################################################
#
#  Function: new
#      Constructor
#
#  Args:
#      bbDbh - big brother database handle
#      clientDbh - client database handle
#
##############################################################################

sub new
{
    my ($class, $bbDbh, $clientDbh, $refreshQueueInst) = @_;

    my $self = {
        workDir => "$ENV{ADR_LOCATION}/$refreshQueueInst" . "_workDir",
        bbDbh => $bbDbh,
        clientDbh => $clientDbh,
        userFile => undef,
        refreshQueueInst  => $refreshQueueInst
    };

    bless $self, $class;
    return $self;
}

##############################################################################
#
#  Function: createSchemaGrantFile
#      Creates ADRSchemaPrivs_yyyyddmm_hhmmss.sql
#
#  Args:
#     schemas - comma separated list of schemas,
#     schemaGrantSPLimit - the size limit of the data coming back for
#     the get_granted_ddl stored procedure. 
#
# Pre-requisites:
#     The schemas exist in the target database
#
##############################################################################

sub createSchemaGrantFile
{
    my ($self, $logStr, $schemas) = @_;
 
    # convert schemas to uppercase
    $schemas = uc($schemas);

    $$logStr .= "In createSchemaGrantFile for $schemas\n";

    my $dateTimeStr = getDateTimeStr();

    $self->{schemaPrivFile} = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRSchemaPrivs_" . $dateTimeStr . ".sql";

    $$logStr .= "Privilege file is $self->{schemaPrivFile}\n";

    if (!open (PRIV_FH, "> $self->{schemaPrivFile}")) 
    {
        $$logStr .= "Cannot open $self->{schemaPrivFile}: $!\n";
        return 0;
    }

    $$logStr .= "Getting grants\n";

     # no longer using dbms_metadata.get_granted_ddl, instead
     #my $sqlStatement = "SELECT dbms_metadata.get_granted_ddl(\'OBJECT_GRANT\', ?) FROM dual";

    # quotes to schema list
 
    $schemas =~ s/,/\',\'/g;
    $schemas = "\'$schemas\'"; 

    my $sqlStatement=<<EOSql;
SELECT privilege,
       owner,
       table_name,
       grantee,
       DECODE(grantable,\'YES\',\'WITH GRANT OPTION\',\'NO\',\'\')
  FROM dba_tab_privs 
 WHERE owner IN ($schemas)
    OR grantee IN ($schemas)
EOSql

     my $sth = $self->{clientDbh}->prepare($sqlStatement);
     if (!defined $sth)
     {
         $$logStr .= "Failed while preparing: $DBI::errstr\n";
         return 0;
     }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting grants: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $privilege, \my $owner, \my $table, \my $grantee, \my $grantOption);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding tablespaces and sizes: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        my $endLine = "\;\n";
        if (defined $grantOption)
        {
            $endLine = " $grantOption\;\n";
        }

        print PRIV_FH "GRANT $privilege ON $owner.$table TO $grantee" . $endLine; 
    }

    close PRIV_FH;

    $$logStr .= "Create preserve schema grant file successful\n";

    return 1;
}

##############################################################################
#
#  Function: runSchemaGrantFile
#      Runs grants in ADRSchemaPrivs_yyyymmdd_hhmmss.sql
#
#  Args:
#
# Pre-requisites:
#     ADRSchemaPrivs_yyyymmdd_hhmmss.sql exists 
#
##############################################################################

sub runSchemaGrantFile
{
    my ($self, $logStr) = @_;

    # If this is a rerun, and the file is already created
    # the file will have to be looked up.

    if (!defined $self->{schemaPrivFile})
    {
        # This is a rerun.  Use the latest file.  This will
        # be the last file on the glob array. 

        my $startFileName =  "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRSchemaPrivs_";
        my @files = <$startFileName*.sql>;
        if (!@files)
        {
            $$logStr .= "Cannot find schema privilege file.  Either createSchemaGrantFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
        $self->{schemaPrivFile} = $files[@files - 1];
    }

    $$logStr .= "Privilege file is $self->{schemaPrivFile}\n";

    my %ignoreErrors = ('ORA-04063' => 1);
    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRSchemaPrivs_" . getDateTimeStr() . ".log";

    if (!runSqlAsSysdba($logStr, "\@$self->{schemaPrivFile}", $logFile, \%ignoreErrors))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Run of preserve schema grant file successful\n";

    return 1;
}


sub runPreserveUserFile
{
    my ($self, $logStr) = @_;

    if (!defined $self->{userFile})
    {
        # This is a rerun.  Try to find the user file.  Get the latest.

        my $startFileName =  "$self->{workDir}/PreserveUser_";
        my @files = <$startFileName*.sql>;

        if (-e $files[@files - 1])
        {
            $self->{userFile} = $files[@files - 1];
        }
        else
        {
            $$logStr .= "Cannot find $startFileName*.sql.  Either createPreserveUserFile has not ";
            $$logStr .= "been run, or this is a rerun and the file has been deleted.\n";
            return 0;
        }
    }

    $$logStr .= "Preserve User file is $self->{userFile}\n";

    my $logFile = "$self->{workDir}/" . "$self->{refreshQueueInst}_ADRUserSql_" . getDateTimeStr() . ".log";

    # ignore if the error is due to a user not existing (ORA-01918)
    my %ignoreErrors = ('ORA-01918' => 1);

    if (!runSqlAsSysdba($logStr, "\@$self->{userFile}", $logFile, \%ignoreErrors))
    {
        #logStr will already be set
        return 0;
    }

    $$logStr .= "Run of Preserve User file successful\n";
    return 1;

}

sub createPreserveUserFile
{
    my ($self, $logStr) = @_;

    $$logStr .= "In createPreserveUserFile\n";

    $self->{userFile} = "$self->{workDir}/PreserveUser_" . getDateTimeStr() . ".sql";   

    $$logStr .= "File to create is $self->{userFile}\n";

    if (!open (FH, "> $self->{userFile}")) 
    {
        $$logStr .= "Can\'t open $self->{userFile}: $!\n";
        return 0;
    }

    # order by user_id to try to keep same order as orignal so it easily 
    # compared.  This will require joining with the dba_users table with each 
    # query rather than getting a list of users and reusing.  There will be 
    # minimal out of order issues due to the way other columns are ordered.

    my $endSql=<<EOSql;
d.profile = \'EA_PROFILE\'
   AND d.username NOT IN (\'SYS\',\'SYSTEM\')
 ORDER BY d.user_id
EOSql

#
# Drop users
#

    $$logStr .= "Getting usernames with profile EA_PROFILE and are not SYS or SYSTEM\n";
    my $sqlStatement=<<EOSql;
SELECT username
  FROM dba_users d
WHERE $endSql
EOSql

    #keep an array of users for building auditting statements
    my @users;  

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting usernames: $DBI::errstr\n";
        return 0;
    }
   
    $rv = $sth->bind_columns (\my $userName);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding file number, name and status: $DBI::errstr\n";
        return 0;
    }

    $$logStr .= "Found the following usernames,\n";
    while ($sth->fetch())
    {
        $$logStr .= "$userName\n";
        print FH "drop user $userName cascade\;\n";
        push(@users, $userName);
    }

    $sth->finish();
    $$logStr .= "Added \"drop\" to the preserve file\n"; 

#
# Create users
#

$$logStr .= "Executing dbms_metadata.get_ddl in the database\n";

$sqlStatement=<<EOSql;
SELECT dbms_metadata.get_ddl('USER', d.username)
  FROM dba_users d
WHERE $endSql
EOSql

    # temporarily increase the length of the data that can be returned to 210.
    # the default is 80
    my $origLongReadLen = $self->{clientDbh}->{LongReadLen};

    $self->{clientDbh}->{LongReadLen} = 400;

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting status: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns (\my $statement);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding file number, name and status: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        # strip out extra whitespace:
        #   CREATE USER "MPOREDDY" IDENTIFIED BY VALUES '960E7EB1DB797148'
        #      DEFAULT TABLESPACE "USERS"
        #      TEMPORARY TABLESPACE "TEMP"
        #      PROFILE "EA_PROFILE"

        $statement =~ s/\s+CREATE/CREATE/; 
        $statement =~ s/\s+DEFAULT/ DEFAULT/; 
        $statement =~ s/\s+TEMPORARY/ TEMPORARY/; 
        $statement =~ s/\s+PROFILE/ PROFILE/; 
        $statement =~ s/\n//g;
    
        print FH "$statement\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"create\" to the preserve file\n"; 

    $self->{clientDbh}->{LongReadLen} = $origLongReadLen;

#
# Grant system privileges
#

$$logStr .= "Getting system priveleges from the database\n";
$sqlStatement=<<EOSql;
SELECT dsp.privilege,
       dsp.grantee || decode(admin_option,'YES',' with Admin option')
  FROM dba_sys_privs dsp,
       dba_users d
 WHERE dsp.grantee = d.username
   AND $endSql
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting system privileges: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $privilege, \$userName);
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding file privilege and username: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        print FH "grant $privilege to $userName\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"grant privilege\" to the preserve file\n"; 

#
# Grant roles
#

$$logStr .= "Getting role privileges from the database\n";
$sqlStatement=<<EOSql;
SELECT drp.granted_role,
       drp.grantee
  FROM dba_role_privs drp,
       dba_users d
 WHERE drp.grantee = d.username
   AND $endSql
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting role privileges: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $role, \$userName);
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding role and username: $DBI::errstr\n";
        return 0;
    }


    while ($sth->fetch())
    {
        print FH "grant $role to $userName\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"grant role\" to the preserve file\n"; 

#
# Grant privileges
#

$$logStr .= "Getting table privileges from the database\n";
$sqlStatement=<<EOSql;
SELECT dtp.privilege,
       dtp.owner,
       dtp.table_name,
       dtp.grantee
  FROM dba_tab_privs dtp,
       dba_users d
 WHERE dtp.grantee = d.username
   AND $endSql, dtp.privilege
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting table privileges: $DBI::errstr\n";
        return 0;
    }

    $sth->bind_columns(\$privilege, \my $owner, \my $table, \$userName);
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding privilege, owner, table and username: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        print FH "grant $privilege on $owner.$table to $userName\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"grant privilege on table\" to the preserve file\n"; 

#
# Alter quotas
#

$$logStr .= "Getting tablespace quotas from the database\n";
$sqlStatement=<<EOSql;
SELECT dtq.username,
       dtq.tablespace_name
  FROM dba_ts_quotas dtq,
       dba_users d
 WHERE dtq.username = d.username
   AND dtq.max_blocks = -1  
   AND $endSql
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting tablespace quotas: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\$userName, \my $tablespaceName);
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding username and tablespace: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        print FH "alter user $userName quota unlimited on $tablespaceName\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"alter user quota\" to the preserve file\n"; 

#
# Enable auditting
#

    # use array set earlier instead of going back to the db
    foreach my $user (@users)
    {
        print FH "audit all by $user by access\;\n";
        print FH "audit update table, insert table, delete table, execute procedure by $user by access\;\n";
    }
    $$logStr .= "Added \"audit\" to the preserve file for original list of users\n"; 

$$logStr .= "Getting system_privilege_map from the database\n";
$sqlStatement=<<EOSql;
SELECT name
  FROM system_privilege_map
 WHERE (name LIKE 'CREATE%TABLE%'
       OR name LIKE 'CREATE%INDEX%'
       OR name LIKE 'CREATE%CLUSTER%'
       OR name LIKE 'CREATE%SEQUENCE%'
       OR name LIKE 'CREATE%PROCEDURE%'
       OR name LIKE 'CREATE%TRIGGER%'
       OR name LIKE 'CREATE%LIBRARY%'
       OR name LIKE 'ALTER%TABLE%'
       OR name LIKE 'ALTER%INDEX%'
       OR name LIKE 'ALTER%CLUSTER%'
       OR name LIKE 'ALTER%SEQUENCE%'
       OR name LIKE 'ALTER%PROCEDURE%'
       OR name LIKE 'ALTER%TRIGGER%'
       OR name LIKE 'ALTER%LIBRARY%'
       OR name LIKE 'DROP%TABLE%'
       OR name LIKE 'DROP%INDEX%'
       OR name LIKE 'DROP%CLUSTER%'
       OR name LIKE 'DROP%SEQUENCE%'
       OR name LIKE 'DROP%PROCEDURE%'
       OR name LIKE 'DROP%TRIGGER%'
       OR name LIKE 'DROP%LIBRARY%'
       OR name LIKE 'EXECUTE%INDEX%'
       OR name LIKE 'EXECUTE%LIBRARY%')
 ORDER BY name
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting system_privilege_map: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\$privilege);
    if (!defined $rv)
    {
        $$logStr .= "Failed while binding privilege: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        print FH "audit $privilege\;\n";
    }
    $$logStr .= "Added \"audit privilege\" to the preserve file\n"; 
    $sth->finish();

#
# Give access to proxy users
#

$$logStr .= "Getting proxies from the database\n";
$sqlStatement=<<EOSql;
SELECT client,
       proxy
  FROM dba_proxies
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting proxies: $DBI::errstr\n";
        return 0;
    }

    $sth->bind_columns(\my $client, \my $proxy);

    while ($sth->fetch())
    {
        print FH "alter user $client grant connect through $proxy\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"alter user through proxy\" to the preserve file\n"; 

#
# Preserve Service Account Passwords
#

$$logStr .= "Getting service account passwords from the database\n";

$sqlStatement=<<EOSql;
SELECT d.username,
       s.password
  FROM dba_users d,
       sys.user\$ s
 WHERE d.username = s.name
   AND d.profile = \'EA_SERVICE\'
EOSql

    $sth = $self->{clientDbh}->prepare($sqlStatement);
    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting service account passwords: $DBI::errstr\n";
        return 0;
    }

    $sth->bind_columns(\my $username, \my $password);

    while ($sth->fetch())
    {
        print FH "alter user $username identified by values '$password'\;\n";
    }
    $sth->finish();
    $$logStr .= "Added \"alter user\" to the preserve file\n";

    close FH; 

    $$logStr .= "Create perserve user file successful\n";
    return 1;
}

sub revokeProxies
{
    my ($self, $logStr) = @_;

    $$logStr .= "In revokeProxies\n";

    $$logStr .= "Getting clients and proxies\n";

    # get list of proxies to revoke 
    my $sqlStatement=<<EOSql;
SELECT client, 
       proxy
  FROM dba_proxies
EOSql

    my $sth = $self->{clientDbh}->prepare($sqlStatement);

    if (!defined $sth)
    {
        $$logStr .= "Failed while preparing: $DBI::errstr\n";
        return 0;
    }

    my $rv = $sth->execute();
    if (!defined $rv)
    {
        $$logStr .= "Failed while getting clients and proxies: $DBI::errstr\n";
        return 0;
    }

    $rv = $sth->bind_columns(\my $client, \my $proxy);

    if (!defined $rv)
    {
        $$logStr .= "Failed while binding clients and proxies: $DBI::errstr\n";
        return 0;
    }

    while ($sth->fetch())
    {
        $sqlStatement = "ALTER USER $client REVOKE CONNECT THROUGH $proxy"; 

        $rv = $self->{clientDbh}->do($sqlStatement);
        if (!defined $rv)
        {
            $$logStr .= "Failed while revoking proxy: $DBI::errstr\n";
            return 0;
        }

    }

    if (!defined $client)
    {
        $$logStr .= "No proxies to revoke\n";
    }
    else
    {
        $$logStr .= "Revoking proxies successful\n";
    }
    $sth->finish();

    return 1;

}

sub resetClientDbh
{
    my ($self, $logStr, $dbh) = @_;

    $$logStr .= "In resetClientDbh\n";

    $self->{clientDbh} = $dbh;
}

1;
