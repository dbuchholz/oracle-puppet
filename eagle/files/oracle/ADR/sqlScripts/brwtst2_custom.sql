create or replace trigger sys.BRW_LOGON_BGIM_APP_RW before ddl on BGIM_APP_RW.schema
declare
l_proxy varchar2(1000);

begin
SELECT NVL(trim(sys_context('USERENV', 'PROXY_USER')),'x') into l_proxy from dual;

IF trim(l_proxy) = 'x' THEN
--dbms_output.put_line('PROXY USER is:'||l_proxy);
raise_application_error(-20002,'insufficient privileges');
END IF;

IF trim(l_proxy) != 'JBILOTTI' AND trim(l_proxy) != 'RKLINE' AND trim(l_proxy) != 'CHILKER' THEN
--dbms_output.put_line('PROXY USER is:'||l_proxy);
raise_application_error(-20002,'insufficient privileges');
END IF;
end;
/
create or replace trigger sys.BRW_LOGON_BGIM_APP_RO before ddl on BGIM_APP_RO.schema
declare
l_proxy varchar2(1000);

begin
SELECT NVL(trim(sys_context('USERENV', 'PROXY_USER')),'x') into l_proxy from dual;

IF trim(l_proxy) = 'x' THEN
--dbms_output.put_line('PROXY USER is:'||l_proxy);
raise_application_error(-20002,'insufficient privileges');
END IF;

IF trim(l_proxy) != 'JBILOTTI' AND trim(l_proxy) != 'RKLINE' AND trim(l_proxy) != 'CHILKER' THEN
--dbms_output.put_line('PROXY USER is:'||l_proxy);
raise_application_error(-20002,'insufficient privileges');
END IF;
end;
/

ALTER TRIGGER sys.BRW_LOGON_BGIM_APP_RW ENABLE;
ALTER TRIGGER sys.BRW_LOGON_BGIM_APP_RO ENABLE;

grant Debug Connect Session to bgim_app_ro;
grant Debug Connect Session to bgim_app_rw;

alter user bgim_app_ro grant connect through jbilotti;
alter user bgim_app_ro grant connect through rkline;
alter user bgim_app_ro grant connect through chilker;
alter user bgim_app_rw grant connect through jbilotti;
alter user bgim_app_rw grant connect through rkline;
alter user bgim_app_rw grant connect through chilker;

alter user bgim_app_ro revoke connect through JDMOTSNEY;
alter user bgim_app_rw revoke connect through JDMOTSNEY;
alter user bgim_app_ro revoke connect through MBRUNNER;
alter user bgim_app_rw revoke connect through MBRUNNER;

update PACE_MASTERDBO.PACE_USER_ROLE_DETAILS PURD set PURD.ROLE_ID = 198 where PURD.USER_ID in (select PU.INSTANCE from PACE_MASTERDBO.PACE_USERS PU where PU.ACCOUNT_STATE = 'U' and PU.TYPE = 'U' and PU.STARSEC_GROUP_ID = 20 and PU.INSTANCE not in (233, 344)) and PURD.ROLE_ID = 376;

update PACE_MASTERDBO.PACE_USERS PU set PU.STARSEC_GROUP_ID = 26 where PU.ACCOUNT_STATE = 'U' and PU.TYPE = 'U' and PU.STARSEC_GROUP_ID = 20 and PU.INSTANCE not in (233, 344);

commit;
