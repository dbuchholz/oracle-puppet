-- Remove all adhoc jobs from the schedule_def  table
delete from pace_masterdbo.pace_mail_map_info
where user_instance in (select instance from pace_masterdbo.pace_mail_user_info     
			where mail_address not like '%eagleaccess.com%' 
			and mail_address not like '%eagleinvsys.com%');
delete from pace_masterdbo.pace_mail_user_info 
where mail_address not like '%eagleaccess.com%'  
and mail_address not like '%eagleinvsys.com%';