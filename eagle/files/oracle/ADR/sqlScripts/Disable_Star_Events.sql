UPDATE msgcenter_dbo.schedule sch SET sch.active = 0 WHERE sch.active != 0;

UPDATE
  msgcenter_dbo.schedule s
SET
  s.active=1
WHERE
  s.schedule_group_id IN
  (
    SELECT
      schedule_group_id
    FROM
      msgcenter_dbo.schedule_groups sg
    WHERE
      upper(sg.group_name)='DATA RETENTION PURGE RULES'
  );
