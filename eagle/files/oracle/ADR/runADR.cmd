use strict;

BEGIN {
    unshift (@INC, $ENV{EAGLE_ACCESS_PERL_LIBRARY});
    unshift (@INC, $ENV{ADR_LOCATION});
}

use ADR;

if (!defined @ARGV || @ARGV != 6 || $ARGV[0] !~ /\d+/)
{
    # TODO how to handle error?
    print "ERROR: Expecting six parameters to be refresh_queue_id, sourceSid, targetSid, isASM, backupLocation and backupTag\n\n";
    exit 0;
}

my $adr = new ADR(@ARGV);
$adr->process() if (defined $adr);

0;

