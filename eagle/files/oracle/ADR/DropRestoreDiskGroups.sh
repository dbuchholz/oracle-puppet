#!/bin/ksh
# ==================================================================================================
# NAME:     DropRestoreDiskGroups.sh
#
# PURPOSE:  Script to drop ASM Disk Groups and then restore ASM Meta Data from backup
#
# USAGE:    DropRestoreDiskGroups.sh FullyQualifiedAsmMetaDataFile [ASM_Instance]
#
# Maureen Buotte    May-2015         Original Development
# ==================================================================================================


NARG=$#
if [ ${NARG} -eq 2 ]; then
	MetaDataFile=$1
	ADR_Inst=$2
elif [ ${NARG} -eq 1 ]; then
	MetaDataFile=$1
	ADR_Inst='NonADR'
else
	echo "FAIL:  Incorrect number of parameters - must the fully qualified name of the ASM Meta data file to restore and optionally the ADR Instance"
	exit 1
fi

DropSQLFile=/usr/local/scripts/grid/ADR/scripts/dropDiskGroups_$ADR_Inst.sql
. /home/grid/.bash_profile

 set -x
SUCCESS_FILE=/usr/local/scripts/grid/ADR/logs/SUCCESS_${ADR_Inst}.log
FAILURE_FILE=/usr/local/scripts/grid/ADR/logs/FAILURE_${ADR_Inst}.log
TEMPLOG_FILE=/usr/local/scripts/grid/ADR/logs/TempLog_${ADR_Inst}.log
rm $DropSQLFile

echo -e "Step 1:  Create Drop Disk Group File \n" > $TEMPLOG_FILE
STATUS=`$ORACLE_HOME/bin/sqlplus -s '/as sysdba' <<EOF
	SET HEADING OFF FEEDBACK OFF ECHO OFF PAGESIZE 0
	spool $DropSQLFile
	select 'set pages 50000 lines 133 feed on echo on' from dual;
	select 'alter diskgroup ALL dismount force;' from dual;
	select 'drop diskgroup '||name||' force including contents;' from v\\$asm_diskgroup;
	spool off;
	select 'SUCCESS' from dual;
	exit
EOF `

echo -e $STATUS >> $TEMPLOG_FILE
found=$(echo $STATUS | grep -c "SUCCESS")
if [ $found -eq 1 ]; then
	echo -e "\nStep 2:  Run Drop Disk Group File \n" >> $TEMPLOG_FILE
	STATUS=`$ORACLE_HOME/bin/sqlplus -s '/as sysasm' <<EOF
		SET HEADING OFF FEEDBACK OFF ECHO OFF PAGESIZE 0
		@$DropSQLFile
		select 'SUCCESS' from dual;
		exit
EOF `

	echo -e $STATUS >> $TEMPLOG_FILE
	found=$(echo $STATUS | grep -c "SUCCESS")
	if [ $found -eq 1 ]; then
		echo -e "\nStep 3:  Run ascmd md_restore command \n" >> $TEMPLOG_FILE
		STATUS=`asmcmd md_restore $MetaDataFile --full `
		echo $STATUS >> $TEMPLOG_FILE
		problems_asm=$(echo $STATUS | grep -c "ASMCMD-")
		problems_ora=$(echo $STATUS | grep -c "ORA-")
		if [ $problems_asm+$problems_ora -gt 0 ]; then
			echo -e "FAILURE running asmcmd\n\n " > $FAILURE_FILE
			cat $TEMPLOG_FILE >> $FAILURE_FILE
			chmod 664 $FAILURE_FILE
			rm $TEMPLOG_FILE
			exit 1	
		fi
	else
		echo -e "\n\nFAILURE running $DropSQLFile \n\n" > $FAILURE_FILE
		cat $TEMPLOG_FILE >> $FAILURE_FILE
		chmod 664 $FAILURE_FILE
		rm $TEMPLOG_FILE
		exit 1	
	fi

else
	echo -e "\n\nFAILURE Creating $DropSQLFile \n\n" > $FAILURE_FILE
	cat $TEMPLOG_FILE >> $FAILURE_FILE
	chmod 664 $FAILURE_FILE
	rm $TEMPLOG_FILE
        exit 1	
fi

set +x
echo "Successful Completion"
touch $SUCCESS_FILE
cat $TEMPLOG_FILE >> $SUCCESS_FILE
chmod 664 $SUCCESS_FILE
rm $TEMPLOG_FILE
