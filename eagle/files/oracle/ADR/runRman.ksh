#!/bin/ksh

#
# wrapper to execute rman commands
#

#TODO test

if [ $# -lt 2 ]
then
 print "\nUsage:\n\t$0 <command line args> <rman command> <logFile>\n"
 print "\t<command line args> are used when connecting via rman"
 print "\t<rman command> is the command to be run within rman"
 print "\t<logFile> is the log"
 print "\nExamples:\n\t$0 \"targht=/ nocatalog\" @script.rman rman.log\n"
 print "\twould run rman target=/ nocatalog log=rman.log append" 
 print "\tand then execute a script called script.rman\n"
 print "\n\t$0 \"target=/ nocatalog\" \"startup nomount pfile=init.ora\" rman.log\n"
 print "\twould run rman target=/ nocatalog log=rman.log append" 
 print "\tand then execute a startup nomount pfile=init.ora\n"
 exit 1
fi

CMD_ARGS=$1
CMD=$2
LOGFILE=$3

#TODO: what do errors look like in the logfile??
$ORACLE_HOME/bin/rman $CMD_ARGS log=$LOGFILE append<<EOF>>/dev/null
 $CMD
EOF
if [ $? -ne 0 ]
then
 exit 0
fi

cat $LOGFILE

exit 1

