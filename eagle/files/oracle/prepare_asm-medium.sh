#!/bin/bash
# Example Usage: 

ASMCHECK=`/usr/sbin/oracleasm listdisks | grep DATA`
ASM_RETVAL=$?

# Does a volume called DATA already exist? If so, stop.

ASMCHECK=`/usr/sbin/oracleasm listdisks | grep DATA01`
ASM_RETVAL=$?

if [ $ASM_RETVAL -eq 0 ]; then
        echo "This disk has already has already been instantiated by ASM."
        exit 2
elif [ $ASM_RETVAL -eq 1 ]; then
        echo "There is no disk called DATA01, I'll make the DATA, FRA, and REDO disks now."

/usr/sbin/oracleasm configure
/usr/sbin/oracleasm exit; /usr/sbin/oracleasm init
/usr/sbin/oracleasm createdisk DATA01 /dev/sdf1
/usr/sbin/oracleasm createdisk DATA02 /dev/sde1
/usr/sbin/oracleasm createdisk DATA03 /dev/sdg1
/usr/sbin/oracleasm createdisk DATA04 /dev/sdh1
/usr/sbin/oracleasm createdisk DATA05 /dev/sdi1
/usr/sbin/oracleasm createdisk FRA01 /dev/sdk1
/usr/sbin/oracleasm createdisk FRA02 /dev/sdl1
/usr/sbin/oracleasm createdisk FRA03 /dev/sdm1
/usr/sbin/oracleasm createdisk FRA04 /dev/sdn1
/usr/sbin/oracleasm createdisk REDO01 /dev/sdd1
/usr/sbin/oracleasm scandisks

fi
