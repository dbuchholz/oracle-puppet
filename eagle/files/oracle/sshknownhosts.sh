#!/bin/bash

HOSTNAME = `/bin/hostname`

/usr/bin/yum -q -y install openssh-clients 
/usr/bin/ssh-keyscan -t rsa $HOSTNAME > /home/oracle/.ssh/known_hosts 
/bin/chown oracle:oinstall /home/oracle/.ssh/known_hosts 
/bin/chmod 644 /home/oracle/.ssh/known_hosts
