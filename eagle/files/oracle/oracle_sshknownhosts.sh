#!/bin/bash

HOSTNAME=`/bin/hostname`

/usr/bin/yum --quiet -y install openssh-clients > /dev/null 2>&1
/bin/mkdir -p /home/oracle/.ssh
/usr/bin/ssh-keyscan -t rsa $HOSTNAME  > /home/oracle/.ssh/known_hosts 
/bin/chown -R oracle:oinstall /home/oracle/.ssh/
/bin/chmod 644 /home/oracle/.ssh/known_hosts
