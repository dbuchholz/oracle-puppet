#!/usr/bin/ksh

if [ $USER = "oracle" ]; then
          if [ $SHELL = "/bin/ksh" ]; then
                ulimit -n 65536
          else
                ulimit -u 16384 -n 65536
          fi
fi

. ./.aliases
#setsid wtpibc

ORACLE_BASE=/u01/app/oracle
export ORACLE_BASE
ORACLE_HOME=/u01/app/oracle/product/11.2.0.3/db
export ORACLE_HOME
LD_LIBRARY_PATH=$ORACLE_HOME/lib32:$ORACLE_HOME/lib
export LD_LIBRARY_PATH

#export LD_ASSUME_KERNEL=2.4.19
#export LD_ASSUME_KERNEL=2.6.18

export PATH=/usr/kerberos/bin:.:/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:.:/usr/sbin:/u01/app/oracle/product/11.2.0.3/db/bin:/usr/openwin/bin:/u01/app/oracle/utils:/u01/app/oracle/product/11.2.0.3/db/rdbms/admin:/u01/app/oracle/product/11.2.0.3/db/network/admin



set -o vi
if [ -s "$MAIL" ]           # This is at Shell startup.  In normal
then echo "$MAILMSG"        # operation, the Shell checks
fi                          # periodically.
. ./.kshrc
EDITOR=vi
export EDITOR
export COLUMNS=350