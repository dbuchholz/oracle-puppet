#!/bin/bash

HOSTNAME=`/bin/hostname`

/usr/bin/yum --quiet -y install openssh-clients > /dev/null 2>&1
/bin/mkdir /home/grid/.ssh
/usr/bin/ssh-keyscan -t rsa $HOSTNAME  > /home/grid/.ssh/known_hosts 
/bin/chown -R oracle:oinstall /home/grid/.ssh/
/bin/chmod 644 /home/grid/.ssh/known_hosts
