ORACLE_TERM=xterm; export ORACLE_TERM

LD_LIBRARY_PATH=/u01/oracle_11204/app/grid/product/11.2.0/grid/lib
ORACLE_BASE=/u01/oracle_11204/app/grid
ORACLE_HOME=/u01/oracle_11204/app/grid/product/11.2.0/grid
SQLPLUS_HOME=/u01/oracle_11204/app/grid/product/11.2.0/grid
PATH=/bin:/usr/local/bin:/usr/bin:/bin:/u01/oracle_11204/app/grid/product/11.2.0/grid/bin

export ORACLE_SID=+ASM
export ORACLE_UNQNAME=+ASM

export LD_LIBRARY_PATH
export ORACLE_BASE
export ORACLE_HOME
export SQLPLUS_HOME
export PATH

export TEMP=/tmp
export TMPDIR=/tmp
umask 022

. $HOME/.aliases
. $HOME/.kshrc
. $HOME/.exrc
