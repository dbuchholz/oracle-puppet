#!/bin/ksh
#set -x

. /home/oracle/.profile

parse_sid()
{
SID=`hostname | cut -d  "." -f1 | cut -d "-" -f1,2 | sed 's/-//'`
export $SID
echo "Database name: $SID"
}


create_spfile()
{
echo "Create spfile from pfile"
/u01/app/oracle/product/12.1.0.2/db/bin/sqlplus -s / as sysdba <<EOF >>  /tmp/temp.log
      create spfile from pfile;
     exit;
EOF
}

startup_nomount()
{
echo "Starting Database in NO MOUNT"
/u01/app/oracle/product/12.1.0.2/db/bin/sqlplus -s / as sysdba <<EOF >>  /tmp/temp.log
      startup nomount;
     exit;
EOF


}

check_pmon()
{
echo $SID
 pmcount=`ps -ef | grep pmon | grep -v grep | grep $SID |wc -l`
 if [[ $pmcount -gt 0 ]]
 then
    echo "Database $SID Instance started successfully "
 else
    echo "Problem in starting database Instance "
    exit 1
fi
}


create_database()
{
echo "Creating Database"
/u01/app/oracle/product/12.1.0.2/db/bin/sqlplus -s / as sysdba <<EOF >>  /tmp/temp.log
CREATE DATABASE $SID
   USER SYS IDENTIFIED BY Eagle01
   USER SYSTEM IDENTIFIED BY Eagle01
   LOGFILE GROUP 1 ('+REDO','+FRA') SIZE 1G,
           GROUP 2 ('+REDO','+FRA') SIZE 1G,
           GROUP 3 ('+REDO','+FRA') SIZE 1G
   MAXLOGHISTORY 1
   MAXLOGFILES 16
   MAXLOGMEMBERS 3
   MAXDATAFILES 1024
   CHARACTER SET AL32UTF8
   NATIONAL CHARACTER SET AL16UTF16
   EXTENT MANAGEMENT LOCAL
   DATAFILE '+DATA'
     SIZE 2G REUSE AUTOEXTEND ON NEXT 2G MAXSIZE UNLIMITED
   SYSAUX DATAFILE '+DATA'
     SIZE 2G REUSE AUTOEXTEND ON NEXT 2G MAXSIZE UNLIMITED
   BIGFILE DEFAULT TABLESPACE users
      DATAFILE '+DATA'
     SIZE 1G REUSE AUTOEXTEND ON NEXT 1G MAXSIZE UNLIMITED
   BIGFILE DEFAULT TEMPORARY TABLESPACE TEMP
      TEMPFILE '+DATA'
      SIZE 1G REUSE AUTOEXTEND ON NEXT 1G MAXSIZE UNLIMITED
   BIGFILE UNDO TABLESPACE UNDOTBS1
      DATAFILE '+DATA'
      SIZE 1G REUSE AUTOEXTEND ON NEXT 1G MAXSIZE UNLIMITED;
exit;
EOF
}


create_dictobj()
{
echo "Creating Database Dictionary Objects"
/u01/app/oracle/product/12.1.0.2/db/bin/sqlplus -s / as sysdba <<EOF >>  /tmp/temp.log
@?/rdbms/admin/catalog.sql
@?/rdbms/admin/catproc.sql
@?/rdbms/admin/utlrp.sql
exit;
EOF
}


enable_archive()
{
/u01/app/oracle/product/12.1.0.2/db/bin/sqlplus -s / as sysdba <<EOF >>  /tmp/temp.log
alter system set log_archive_dest_1='LOCATION=USE_DB_RECOVERY_FILE_DEST, valid_for=(ALL_LOGFILES,ALL_ROLES) scope=BOTH';
shutdown immediate;
startup mount;
alter database archivelog;
alter database open;
EOF
}


date
parse_sid
create_spfile
startup_nomount
check_pmon

create_database

echo $?

if [ $? -ne 0 ]
then
echo " Creation of Database Failed "
exit 1
else
echo "      Database created Successfully "
fi

create_dictobj
#
#
echo $?

if [ $? -ne 0 ]
then
echo " Creation of Dictionary objects Failed "
exit 1
else
echo "      Dictionary objects created Successfully "
fi
#
enable_archive
date

