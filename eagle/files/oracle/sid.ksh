#!/bin/ksh
# To view / change current Oracle SID and ORALCE_HOME, PATH
#
#set -x
clear
echo ' '
echo '***************************************************************'
printf "      The current ORACLE_SID value is: \x1b[5m$ORACLE_SID\x1b[25m\n"
echo '***************************************************************'
echo ' List of valid instance names along with the ORACLE_HOME value:'
echo ' '
wcntr=0
while read LINE
do
  case $LINE in
  \#*)            ;;      #comment-line in oratab
  *)
   if [ -z $LINE ]; then
      continue
   fi
  #       Proceed only if third field is 'Y'.
  #if [ "`echo $LINE | awk -F: '{print $3}' -`" = "Y" ] ; then
    wcntr=`expr $wcntr + 1`
    OSID=`echo $LINE | awk -F: '{print $1}' -`
    if [ "$ORACLE_SID" = '*' ] ; then
      echo '  *** Unknown Value ***'
    fi
    OHOME=`echo $LINE | awk -F: '{print $2}' -`
    echo ' ' $wcntr ' ' $OSID ' ' $OHOME
  #fi
  esac
done < /etc/oratab
echo ' '
echo '***************************************************************'
echo ' '
#read choice?' Do you want to change ORACLE_SID  [Y/N]? '
#case $choice in 
     #Y|y) continue;;
     #*)   return 0;;
#esac
choice=0
read choice?' Enter Serial# of the SID from above list  : '
if  [ "$choice" = '' ] || [ "$choice" = [A-Z] ] || test $choice -lt 1  || test $choice -gt $wcntr 
then
   echo ''
   echo ''
   echo ' Invalid choice.... Sorry!!!'
   echo ''
   echo ''
   return 1
fi
wcntr=0
while read LINE
do
  case $LINE in
  \#*)            ;;      #comment-line in oratab
  *)
   if [ -z $LINE ]; then
      continue
   fi
  #       Proceed only if third field is 'Y'.
  #if [ "`echo $LINE | awk -F: '{print $3}' -`" = "Y" ] ; then
    wcntr=`expr $wcntr + 1`
  if test $wcntr -eq $choice
     then
ORA_SID=`echo $LINE | awk -F: '{print $1}' -`
ORA_HOME=`echo $LINE | awk -F: '{print $2}' -`

export ORACLE_SID=${ORA_SID}
export ORACLE_HOME=${ORA_HOME}
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export ORACLE_BASE=$HOME
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib

# Reset the original PATH
PATH=/usr/kerberos/bin:.:/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin
export PATH=$PATH:.:/usr/sbin:$ORACLE_HOME/bin:/usr/openwin/bin:$HOME/utils:$ORACLE_HOME/rdbms/admin:$ORACLE_HOME/network/admin:$ORACLE_HOME/OPatch

    fi
  #fi
  esac
done < /etc/oratab
#
echo ' '
echo '********************************************************************************'
echo ' New value of ORACLE_SID is       ' $ORACLE_SID
echo ' New value of ORACLE_HOME is      ' $ORACLE_HOME
echo ' New value of PATH is             ' $PATH
echo ' New value of ORACLE_BASE is      ' $ORACLE_BASE
echo ' New value of ORA_NLS33 is        ' $ORA_NLS33
echo ' New value of LD_LIBRARY_PATH is  ' $LD_LIBRARY_PATH
echo '********************************************************************************'
echo ' '
#

