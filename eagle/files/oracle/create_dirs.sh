#!/bin/bash


if [ $# -ne 2 ]; then
    echo
    echo usage: $0  full_path_to_create user:group
    echo
    echo e.g. $0 /foo/bar/chips/salsa oracle:oinstall
    echo
    exit
fi

IF=$1

echo "mkdir -p $1"

mkdir -p $1
#chown -R $2 $1
chown $2 $1
