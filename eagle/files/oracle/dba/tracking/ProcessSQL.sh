#!/usr/bin/ksh
# ==================================================================================================
# NAME:     ProcessSQL.sh 
#
# AUTHOR:   Maureen Buotte
#
# PURPOSE:  This utility takes a SQL statement and either executes it or validates that the
#			syntax is correct depending on the OPTION parameter
#
# USAGE:    ValidateSQL.sh OPTION SID User PW Statement ResultFile
#
# Frank Davis    24-Sep-2013			Remove tech_support@eagleaccess.com from email
# Maureen Buotte 25-Nov-2008			Original Development
# ==================================================================================================
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications
# -----------------------------------------------------------------------------
function SendNotification {

        # Uncomment for debug
         set -x

        Machine=`uname -a | awk '{print $2}'`
        echo "Error during ${PROGRAM_NAME} \n    Database: ${ORA_SID} \n  Machine: $Machine" > mail.dat
		if [[ x$1 != 'x' ]]; then
			echo "\n$1\n" >> mail.dat
		fi
        cat mail.dat | mail -s "${TYPE} Environment -- ${PROGRAM_NAME} failed for ${ORA_SID}" ${MAILTO}
        rm mail.dat
        return 0

}

# --------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------
funct_chk_parm() {
    # Uncomment next line for debugging
     set -x
    if [ ${NARG} -ne 6 ]; then
        echo "ValidateSQL Fail: Not enough arguments passed ->  ValidateSQL.sh OPTION SID User PW Statement ResultFile"
		SendNotification "Not enough arguments passed -> ValidateSQL.sh OPTION SID User PW Statement ResultFile"
        exit -1
    fi
}



# *****************************
#  MAIN
# *****************************
#
# Uncomment for debug
 set -x

# Set environment variables
export PROGRAM_NAME=`echo  $0 | sed 's/.*\///g'`
IPCheck=`/sbin/ifconfig -a | egrep '10.170.1|10.60.105|10.60.103' | awk '{print $1}'`
if [ x$IPCheck = 'x' ]; then
        export TYPE=PROD
else
        export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com

NARG=$#
funct_chk_parm

export OPTION=$1
export ORA_SID=$2
export USER=$3
export CODE=$4
export STATEMENT=$5
export RESULTFILE=$6


. /usr/bin/setsid.sh $ORA_SID
Status=$?
if [ $Status != 0 ]; then
        echo "Invalid SID -> ${ORA_SID}"
        SendNotification "Invalid SID -> ${ORA_SID}"
        exit -1
fi

echo $OPTION
if [[ ${OPTION} = '1' ]]; then
	Status=`${ORACLE_HOME}/bin/sqlplus -s ${USER}/${CODE}@${ORA_SID} <<EOF
			set heading off
			set feedback off
			set echo off
			spool $RESULTFILE
			explain plan for $STATEMENT
			spool off
	exit
EOF`

else
	Status=`${ORACLE_HOME}/bin/sqlplus -s ${USER}/${CODE}@${ORA_SID} <<EOF
			set feedback on
			set echo on
			set term on
            spool $RESULTFILE
            $STATEMENT
			commit;
            spool off
    exit
EOF`
fi
