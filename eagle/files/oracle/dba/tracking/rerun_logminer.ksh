#!/bin/ksh
# Script name:  rerun_logminer.ksh
# No parameters are passed into this script.  Script will prompt for inputs.
# Use this script to rerun Logminer after running the tracking program.
# This script will prompt for a begin and end time to run Logminer.
# You can run the logminer for subset of times to get the full time in multiple runs of this script.

function password_entry
{
stty -echo
read PW1?"Enter Your Password: "
stty echo
if [[ -z $PW1 ]]
then
 print "\n\nYou did not enter a password.  Aborting here..."
 exit 5
fi
}

function check_sid
{
 sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
 if [[ -z $sid_in_oratab ]]
 then
  print "There is no $ORACLE_SID entry in $ORATAB file"
  exit 6
 fi
}

if [ "$(dirname $0)" = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir/scripts

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

print "The list of Oracle SID are:\n"
ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}'
print "\n"
read ORACLE_SID?"Enter Oracle SID: "
if [[ -z $ORACLE_SID ]]
then
 print "You did not enter an Oracle SID.  Aborting here..."
 exit 1
fi
export ORACLE_SID
check_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
print "\n"
read UN?"Enter Your Database User Name: "
if [[ -z $UN ]]
then
 print "You did not enter your database username.  Aborting here..."
 exit 2
fi
UN=$(print $UN | tr '[a-z]' '[A-Z]')
print "\nNext, you will enter your database password."
print "For security, password will not be reflected to terminal.\n"
password_entry
print "\n"
(( db_version=$(print $ORACLE_HOME | awk -F/ '{print $6}' | awk -F. '{print $1}') ))
if [ $db_version -lt 10 ]
then
 print "This tracking program will not run against and Oracle 9 database."
 print "Please run the tracking using the older script RunUpdateDB.sh"
 print "or take a manual export of the tables effected by the DML changes."
 exit 3
fi
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
connect ${UN}/$PW1
EOF
if [ $? -ne 0 ]
then
 print "Cannot log into $ORACLE_SID Database with $UN Account."
 print "The password may not be correct or the user does not exist."
 exit 4
fi

print "Enter a start time for Logminer."
print "The format is DD-MON-YYYY HH24:MI:SS"
print "For example: 19-AUG-2013 16:45:00"
read start_time?"Enter Start Time: "
read end_time?"Enter End Time: "

now=$(date +'%Y%m%d_%H%M%S')
LC=/datadomain/export/$ORACLE_SID

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set linesize 200
col sql_redo format a55
col sql_undo format a55
ALTER SESSION SET nls_date_format='DD-MON-RRRR hh24:mi:ss';
BEGIN
 DBMS_LOGMNR.START_LOGMNR(STARTTIME => '$start_time', ENDTIME => '$end_time', OPTIONS => DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG + DBMS_LOGMNR.CONTINUOUS_MINE + DBMS_LOGMNR.COMMITTED_DATA_ONLY);
END;
/
set feedback off
delete from audit_undo.sql_redoundo where username='$UN';
commit;
DECLARE
 cursor c1 is SELECT username,sql_undo FROM v\$logmnr_contents where operation in ('INSERT','UPDATE','DELETE','MERGE') and username='$UN' and table_name<>'AUD$';
 user_name VARCHAR2(30);
 sql_undo VARCHAR2(4000);
 x NUMBER :=0;
 commit_count NUMBER := 10000;
BEGIN
 OPEN c1;
 LOOP
 FETCH c1 INTO user_name,sql_undo;
 EXIT WHEN c1%NOTFOUND;
 x := x + 1;
 INSERT INTO audit_undo.sql_redoundo VALUES(user_name,sql_undo);
 IF x > commit_count THEN
  x := 0;
  COMMIT;
 END IF;
END LOOP;
COMMIT;
CLOSE c1;
END;
/
exec dbms_logmnr.end_logmnr;
EOF
# + DBMS_LOGMNR.PRINT_PRETTY_SQL
print "Generating Undo SQL in file: $LC/undo_sql_${UN}_${ORACLE_SID}_${now}.sql"
$ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF> /dev/null
set linesize 4000 pagesize 0 trimspool on echo off feedback off
spool $LC/undo_sql_${UN}_${ORACLE_SID}_${now}.sql
select sql_undo from audit_undo.sql_redoundo where username='$UN';
EOF

exit 0
