set echo on


-- DBA tablespace creation

BEGIN

   DECLARE
    data_file_path varchar2(200);
        tablespace_exists integer;
        SqlStmt varchar2(32767);

   BEGIN

   BEGIN

      select count(1) into tablespace_exists
      from dba_data_files
      where tablespace_name = 'EA_DBA';

      exception when no_data_found then
       tablespace_exists := 0;
    END;

    if tablespace_exists = 1 then

       execute immediate ('DROP TABLESPACE EA_DBA INCLUDING CONTENTS AND DATAFILES');

    end if;

    select substr(file_name,1,instr(file_name,'/', -1))
    into data_file_path
    from dba_data_files
    where tablespace_name = 'SYSTEM';

    SqlStmt := 'CREATE TABLESPACE EA_DBA '||chr(10)
    || 'DATAFILE '|| ''''||data_file_path||'eadba01.dbf'||''''||' SIZE 512M REUSE'||chr(10)
    ||'AUTOEXTEND ON NEXT 104857600 MAXSIZE 5120M'||chr(10)
    ||'LOGGING ONLINE PERMANENT BLOCKSIZE 8192'||chr(10)
    ||'EXTENT MANAGEMENT LOCAL AUTOALLOCATE SEGMENT SPACE MANAGEMENT AUTO';

    execute immediate(SqlStmt);

  END;

END;
/

--- DBA Role creation

BEGIN
    declare RoleName VARCHAR2(100);
BEGIN
  BEGIN
    select role into RoleName
    from dba_roles
    where role = 'EA_DBA_ROLE';
    exception when no_data_found then
        RoleName := '';
  END;

  IF RoleName = 'EA_DBA_ROLE' THEN
    execute immediate('DROP ROLE EA_DBA_ROLE');
  END IF;
END;

END;
/

CREATE ROLE EA_DBA_ROLE;

GRANT DBA, CONNECT, RESOURCE TO EA_DBA_ROLE;

GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;

REVOKE EA_DBA_ROLE FROM SYS;

-- Enable Audit Trail

alter system set audit_trail=db_extended scope=SPFILE ;
audit insert, update, delete on sys.aud$ by access;

-- Creating DBA users

-- Creating MBUOTTE user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'MBUOTTE';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'MBUOTTE' THEN
        execute immediate('DROP USER MBUOTTE CASCADE');
    END IF;
 END;
END;
/

CREATE USER MBUOTTE
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO MBUOTTE;

ALTER USER MBUOTTE QUOTA UNLIMITED ON EA_DBA;

audit all by MBUOTTE by access;
audit update table, insert table, delete table by MBUOTTE by access;

-- Creating SVYAHALKAR user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'SVYAHALKAR';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'SVYAHALKAR' THEN
        execute immediate('DROP USER SVYAHALKAR CASCADE');
    END IF;
 END;
END;
/

CREATE USER SVYAHALKAR
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO SVYAHALKAR;

ALTER USER SVYAHALKAR QUOTA UNLIMITED ON EA_DBA;

audit all by SVYAHALKAR by access;
audit update table, insert table, delete table by SVYAHALKAR by access;

-- Creating JALCORDO user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'JALCORDO';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'JALCORDO' THEN
        execute immediate('DROP USER JALCORDO CASCADE');
    END IF;
 END;
END;
/

CREATE USER JALCORDO
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO JALCORDO;

ALTER USER JALCORDO QUOTA UNLIMITED ON EA_DBA;

audit all by JALCORDO  by access;
audit update table, insert table, delete table by JALCORDO  by access;

-- Creating RMADHAVI user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'RMADHAVI';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'RMADHAVI' THEN
        execute immediate('DROP USER RMADHAVI CASCADE');
    END IF;
 END;
END;
/

CREATE USER RMADHAVI
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO RMADHAVI;

ALTER USER RMADHAVI QUOTA UNLIMITED ON EA_DBA;

audit all by RMADHAVI  by access;
audit update table, insert table, delete table by RMADHAVI  by access;

-- Creating PKRISHNAN user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'PKRISHNAN';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'PKRISHNAN' THEN
        execute immediate('DROP USER PKRISHNAN CASCADE');
    END IF;
 END;
END;
/

CREATE USER PKRISHNAN
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO PKRISHNAN;

ALTER USER PKRISHNAN QUOTA UNLIMITED ON EA_DBA;

audit all by PKRISHNAN  by access;
audit update table, insert table, delete table by PKRISHNAN  by access;

-- Creating ACHOWDHARY user

BEGIN
    declare V_UserName VARCHAR2(100);
 BEGIN
    BEGIN
       select username into V_UserName
       from dba_users
       where username = 'ACHOWDHARY';
       exception when no_data_found then
          V_UserName := '';
    END;

    IF V_UserName = 'ACHOWDHARY' THEN
        execute immediate('DROP USER ACHOWDHARY CASCADE');
    END IF;
 END;
END;
/

CREATE USER ACHOWDHARY
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO ACHOWDHARY;

ALTER USER ACHOWDHARY QUOTA UNLIMITED ON EA_DBA;

audit all by ACHOWDHARY by access;
audit update table, insert table, delete table by ACHOWDHARY by access;

-- CREATING EA_TEAM_INSTALL ROLE

BEGIN
    declare RoleName VARCHAR2(100);
BEGIN
   BEGIN
    select role into RoleName
    from dba_roles
    where role = 'EA_INSTALL_ROLE';
    exception when no_data_found then
        RoleName := '';
   END;

   IF RoleName = 'EA_INSTALL_ROLE' THEN
        execute immediate('DROP ROLE EA_INSTALL_ROLE');
   END IF;
END;

END;
/

CREATE ROLE EA_INSTALL_ROLE;

GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT ALTER SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;

--- Creating PSHERRY user

BEGIN
    declare V_UserName VARCHAR2(100);
  BEGIN
    BEGIN
    select username into V_UserName
    from dba_users
    where username = 'PSHERRY';
    exception when no_data_found then
       V_UserName := '';
    END;
    IF V_UserName = 'PSHERRY' THEN
        execute immediate('DROP USER PSHERRY CASCADE');
    END IF;
  END;
END;
/

CREATE USER PSHERRY
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO PSHERRY;

ALTER USER PSHERRY QUOTA UNLIMITED ON EA_DBA;

GRANT CREATE ANY PROCEDURE TO PSHERRY;
GRANT ALTER ANY PROCEDURE TO PSHERRY;
GRANT CREATE ANY VIEW TO PSHERRY;


CREATE OR REPLACE TRIGGER PSHERRY_EA_tracking
BEFORE CREATE OR ALTER ON PSHERRY.SCHEMA
DECLARE
    validation_error EXCEPTION;
    V_error_msg Varchar2(500);
    count_owner number;
BEGIN
   select count(1)
   into count_owner
   from dual
   where ora_dict_obj_owner in (
    select owner
    from dba_tables
    where owner not in
  (select username from dba_users where temporary_tablespace  = 'TEMP'));

   IF count_owner = 0 THEN
        V_error_msg :='Application Error: Operation not allowed by '||user;
        RAISE validation_error;
   END IF;
   EXCEPTION WHEN validation_error THEN
        RAISE_APPLICATION_ERROR(-20001,V_error_msg);
END;
/

audit all by PSHERRY by access;
audit update table, insert table, delete table by PSHERRY by access;
audit execute procedure by PSHERRY by access;

--- Creating RVERITY user

BEGIN
    declare V_UserName VARCHAR2(100);
  BEGIN
    BEGIN
    select username into V_UserName
    from dba_users
    where username = 'RVERITY';
    exception when no_data_found then
       V_UserName := '';
    END;
    IF V_UserName = 'RVERITY' THEN
        execute immediate('DROP USER RVERITY CASCADE');
    END IF;
  END;
END;
/

CREATE USER RVERITY
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO RVERITY;

ALTER USER RVERITY QUOTA UNLIMITED ON EA_DBA;

GRANT CREATE ANY PROCEDURE TO RVERITY;
GRANT ALTER ANY PROCEDURE TO RVERITY;
GRANT CREATE ANY VIEW TO RVERITY;


CREATE OR REPLACE TRIGGER RVERITY_EA_tracking
BEFORE CREATE OR ALTER ON RVERITY.SCHEMA
DECLARE
    validation_error EXCEPTION;
    V_error_msg Varchar2(500);
    count_owner number;
BEGIN
   select count(1)
   into count_owner
   from dual
   where ora_dict_obj_owner in (
    select owner
    from dba_tables
    where owner not in
  (select username from dba_users where temporary_tablespace  = 'TEMP'));

   IF count_owner = 0 THEN
        V_error_msg :='Application Error: Operation not allowed by '||user;
        RAISE validation_error;
   END IF;
   EXCEPTION WHEN validation_error THEN
        RAISE_APPLICATION_ERROR(-20001,V_error_msg);
END;
/

audit all by RVERITY by access;
audit update table, insert table, delete table by RVERITY by access;
audit execute procedure by RVERITY by access;

--- Creating DBELAMBE user

BEGIN
    declare V_UserName VARCHAR2(100);
  BEGIN
    BEGIN
    select username into V_UserName
    from dba_users
    where username = 'DBELAMBE';
    exception when no_data_found then
       V_UserName := '';
    END;
    IF V_UserName = 'DBELAMBE' THEN
        execute immediate('DROP USER DBELAMBE CASCADE');
    END IF;
  END;
END;
/

CREATE USER DBELAMBE
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO DBELAMBE;

ALTER USER DBELAMBE QUOTA UNLIMITED ON EA_DBA;

GRANT CREATE ANY PROCEDURE TO DBELAMBE;
GRANT ALTER ANY PROCEDURE TO DBELAMBE;
GRANT CREATE ANY VIEW TO DBELAMBE;


CREATE OR REPLACE TRIGGER DBELAMBE_EA_tracking
BEFORE CREATE OR ALTER ON DBELAMBE.SCHEMA
DECLARE
    validation_error EXCEPTION;
    V_error_msg Varchar2(500);
    count_owner number;
BEGIN
   select count(1)
   into count_owner
   from dual
   where ora_dict_obj_owner in (
    select owner
    from dba_tables
    where owner not in
  (select username from dba_users where temporary_tablespace  = 'TEMP'));

   IF count_owner = 0 THEN
        V_error_msg :='Application Error: Operation not allowed by '||user;
        RAISE validation_error;
   END IF;
   EXCEPTION WHEN validation_error THEN
        RAISE_APPLICATION_ERROR(-20001,V_error_msg);
END;
/

audit all by DBELAMBE by access;
audit update table, insert table, delete table by DBELAMBE by access;
audit execute procedure by DBELAMBE by access;

--- Creating GDESISTA user

BEGIN
    declare V_UserName VARCHAR2(100);
  BEGIN
    BEGIN
    select username into V_UserName
    from dba_users
    where username = 'GDESISTA';
    exception when no_data_found then
       V_UserName := '';
    END;
    IF V_UserName = 'GDESISTA' THEN
        execute immediate('DROP USER GDESISTA CASCADE');
    END IF;
  END;
END;
/

CREATE USER GDESISTA
PROFILE DEFAULT
IDENTIFIED BY Chang3m3 PASSWORD EXPIRE
DEFAULT TABLESPACE EA_DBA
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO GDESISTA;

ALTER USER GDESISTA QUOTA UNLIMITED ON EA_DBA;

GRANT CREATE ANY PROCEDURE TO GDESISTA;
GRANT ALTER ANY PROCEDURE TO GDESISTA;
GRANT CREATE ANY VIEW TO GDESISTA;


CREATE OR REPLACE TRIGGER GDESISTA_EA_tracking
BEFORE CREATE OR ALTER ON GDESISTA.SCHEMA
DECLARE
    validation_error EXCEPTION;
    V_error_msg Varchar2(500);
    count_owner number;
BEGIN
   select count(1)
   into count_owner
   from dual
   where ora_dict_obj_owner in (
    select owner
    from dba_tables
    where owner not in
  (select username from dba_users where temporary_tablespace  = 'TEMP'));

   IF count_owner = 0 THEN
        V_error_msg :='Application Error: Operation not allowed by '||user;
        RAISE validation_error;
   END IF;
   EXCEPTION WHEN validation_error THEN
        RAISE_APPLICATION_ERROR(-20001,V_error_msg);
END;
/

audit all by GDESISTA by access;
audit update table, insert table, delete table by GDESISTA by access;
audit execute procedure by GDESISTA by access;

