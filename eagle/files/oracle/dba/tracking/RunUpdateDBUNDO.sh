#!/bin/ksh

#set -x 

##export LD_LIBRARY_PATH=$ORACLE_HOME/lib32

NARG=$#
PROCEDURENAME=$0
if [[ ${NARG} -ne 5  &&  ${NARG} -ne 6 ]]; then
        echo "\n${PROCEDURENAME} requires 5 arguments - Enter parameters or 0 to exit\n"
	read UN?"Enter Your Database User Name: "
	if [[ ${UN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	
	stty -echo
	read PW?"Enter Your Password (this will not be echoed back to the screen): "
	if [[ ${PW} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	stty echo
	echo -n "\n"

	read SID?"Enter Database SID: "
	if [[ ${SID} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read TN?"Enter TrackingNumber: "
	if [[ ${TN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read FN?"Enter update script file name: "
	if [[ ${FN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read LC?"Enter backup location or just hit return to use the default location: "
	if [[ ${LC} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
else 
	UN=$1
	PW=$2
	SID=$3
	TN=$4
	FN=$5
	LC=$6
fi

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set head off
set echo off

SELECT SUPPLEMENTAL_LOG_DATA_MIN FROM V\\$DATABASE;

exit
!`

REDOUNDO=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set head off
set echo off

select count(1) from dba_objects where owner='AUDIT_UNDO';

exit
!`


if [ ${STATUS} != 'YES' ]; then
 clear
 echo ""
 echo ""
 echo "Database ${SID} is not setup for Log mining. Please make sure that SUPPLEMENTAL_LOG is enable..."
 echo "                  Please use RunUpdateDB.sh for this database.                                  "
 echo ""
 echo ""
exit 1
else
 echo "Things looks good so far..."
fi

if [ ${REDOUNDO} != 2 ]; then
  clear
  echo ""
  echo ""
  echo " Enabling Logminer for the first time. Please wait....."
  echo ""
  echo ""
/u01/app/oracle/local/dba/Auditing/ImplementTrackDB.sh ${SID}
fi

OUT=$?
if [ $OUT -eq 0 ];then
  echo " Logminer is now enabled for this database "
  else
echo ""
echo ""
 echo " There was problem enabling Logminer.. Please fix the error.."
echo ""
echo ""
 exit 1
fi

perl -w UpdateDBUNDO.cmd $UN $PW $SID $TN $FN $LC

