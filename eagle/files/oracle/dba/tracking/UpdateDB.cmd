#
=head1  UpdateDB.cmd

=head2  ABSTRACT:
        UpdateDB.cmd will parse a file containing database update/delete statements and create select 
		statements to save the data that will be updated/deleted.  It will then perform the requested
		update(s).

=head2  ARGUMENTS:

=head2  REVISIONS:
	05-Jan-10   Maureen Buotte		Changed to run scripts as individual user
	20-Sep-05   Maureen Buotte		Initial Development

=cut

use strict;
use Text::ParseWords;
use File::Basename;
use DBI;

use lib "$ENV{HOME}/local/dba/perllib";
use ITSError;
use ITSFatal;
use ITSCode;
use ITSTrim;
use IO::Handle;

# =======================================================================================
sub PrintStatement ($) {

#  This subroutine prints a SQL statement 
#
#  Arguments:
#  0: SQL Statement
#
	my $PrintStatement;			# Input String
	my $Strlen;				# Length of string, used for printing
	my $Done;				# Loop variable
	my $Cutoff;				# Cutoff for splitting lines on blanks
	my $Partial;				# Portion of line to print

	$PrintStatement = $_[0];
	$Done = 0;
	while ($Done ne "1") {
		$Strlen = length $PrintStatement;
		if ($Strlen < 70) {
			print "$PrintStatement\n\n";
			$Done = 1;
			next;
		}
		else {
			$Cutoff = index (substr($PrintStatement,69), ' ');
			$Partial = substr ($PrintStatement,0,70+$Cutoff);
			print "$Partial \n";
			$PrintStatement = substr ($PrintStatement,70+$Cutoff);
		}
	}
	return 0;

}
# =======================================================================================
sub GetYesNo ($$) {

	#  This Promts the user for a Y or N response
	#
	#  Arguments:
	#  0: Prompt
	#  1: Response (returned)
	#
	my $Finished;		# Continue until a valid response is entered
	my $Prompt = $_[0];	# Prompt give to the user
	my $UserResp;		# User response

	$Finished = 0;
	while ($Finished ne "1") {
		print ($Prompt);
		$UserResp = <STDIN>;
		chomp($UserResp);
		if ($UserResp ne 'Y' && $UserResp ne 'N') {
			print ("Invalid reply - must enter either 'Y' or 'N' \n");
		}
		else {
			$Finished = 1;
		}
	}
	$_[1] = $UserResp;	
	return 0;

}
# =======================================================================================

# Environment Variables
my $HOME_DIR = $ENV{HOME};									# Home directory
my $WORKING_DIR = $HOME_DIR . "/local/dba/tracking"; 		# Working Directory
my $SCRIPTS_DIR = $WORKING_DIR . "/scripts/";			 	# Directory containing SQL update scripts
my $LOG_DIR = $WORKING_DIR . "/logs/";				  		# Log Directory
my $ProgramName = "UpdateDB.cmd";
my $EXPORT_DIR;

# Local Variables
my $TrackingNum;			# Assigned Tracking number for update
my $UserName;				# User to run the updates as
my $Code;					# Login Credentials
my $ClientSID;				# SID of database to update
my $InputFile;				# File name parameter
my $InputFileName;			# Full location of input File
my $fh_inputfile;			# Input File Handle
my $LogFileName;            # Log File
my $fh_logfile;            # Log File File handle
my $fh_temp2file;            # Log File File handle
my ($Second, $Minute, $Hour, $Day, $Month, $Year);            # Date/Time variables
my $CurDate;				# Current Date
my $CurTime;				# Current Time
my $CurDateTime;			# Current Date/Time string
my $Line;					# Used to read input file
my $ErrLine;				# Used to read error file
my $SQLStatement;			# Individual SQL Statement
my @StatementList;			# Array of SQL Statements contained in a file
my $Statement;				# Full statement
my $SelectStatement;		# Select Statement created from update/delete statement
my $CreateStatement;		# Statement to create temporary table
my $UpdateStatement;		# Update statement to run
my $TempTableName;			# Name of temporary table to create
my $SaveName;				# Used when there are aliases in the table name
my @TempTableList;			# List of all temporary tables
my $TableName;				# Fully Qualified Name of table updating (ex: holdingdbo.position) 
my $Table;					# Non Qualified Name of table updating (ex: position)
my $Done; 					# Loop condition
my $Reply;					# Accepts user input
my $Count;					# Keeps track of number of updates
my $WhereIndx;				# Parsing index
my $Blank;					# Index of Blank in string
my $SubStmt;				# Used to parse UPDATE statements
my $LCount;					# Keep track of number of '(' 
my $RCount;					# Keep track of number of ')' 
my $Status;					# Return Code
my $NumRows;				# Rows effected by SQL update/delete
my $ExpTableList;			# List of temporary tables to be exported
my $Tbl;					# Used to loop through ExpTableList array
my $DelTempTable;			# Statement to delete temporary tables
my $ExportCommand;			# Command to export temporary tables
my $PrintCommand;           # Command to print export with no password display
my $ExportName;				# Name of Export file that will be created
my $ExportLog;				# Name of Export Log file that will be created
my $TestFile;				# Used to check for errors in Export
my $GrepCommand;			# Used to check export log for success
my $SuccessMsg;				# Message indicating export success

my $InComment;              # Used to parse multi line comments
my $InvalidStatements;      # Number of invalid statements
my $LastLine;               # Indicates last line of a SQL Statement
my $PlanFileName;           # Validation File
my $TempFileName;           # Temporary File
my $Temp2FileName;           # Temporary File
my $position;               # Used to strip off inline comments
my $ResultsFileName;        # Results/Status information file
my $VALIDATE;               # Option for ProcessSQL.sh 
my $EXECUTE;                # Option for ProcessSQL.sh 
my $fh_resultsfile;         # Results file handle

# Variables uses with DBI
my $dbh;				# Database Handle
my $sql;				# SQL statement passed to database
my $sth;				# Statement handle
my $Commit="COMMIT";	# Used to commit transaction

# Check Parameters 
if ( (scalar(@ARGV) != 5) &&  (scalar(@ARGV) != 6) ) {
	print ("Invalid Number of Parameters  \n");
	print ("	Must Enter at least 5 Parameters:  Username, Password, SID, Tracking Number, Name of file containing SQL statement(s) \n");
	print ("	There is an optional 6th parameter that, if entered, will be used as export location \n");
	exit
}
($UserName, $Code, $ClientSID,  $TrackingNum, $InputFile) = @ARGV;

if (scalar(@ARGV) != 6) {
	$EXPORT_DIR = "/datadomain/export/$ARGV[2]/";
}
else {
	$EXPORT_DIR = $ARGV[5];
}

$ExportName = $EXPORT_DIR . "TN_" . $TrackingNum . ".dmp";
$ExportLog = $EXPORT_DIR . "TN_" . $TrackingNum . ".log";
if ( !(-e $EXPORT_DIR) ) {
	print ("\n\nExport Directory $EXPORT_DIR does not exist \n");
	exit
}
if (-e $ExportName) {
    print ("\n\nExport File $ExportName already exists \n- make sure the correct tracking number was entered\n\n");
	exit
}

# Get Current Date/Time
($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
$Year += 1900; $Month++;
$Month = $Month < 10 ? "0$Month" : "$Month";
$Day = $Day < 10 ? "0$Day" : "$Day";
$CurDate = $Year.$Month.$Day;
$Hour = $Hour < 10 ? "0$Hour" : "$Hour";
$Minute = $Minute < 10 ? "0$Minute" : "$Minute";
$Second = $Second < 10 ? "0$Second" : "$Second";
$CurTime = $Hour . $Minute . $Second;
$CurDateTime = $CurDate . "_" . $CurTime;
	
$VALIDATE = 1;
$EXECUTE = 2;

# Open Log File
$LogFileName = $LOG_DIR.(substr($ProgramName,0,(index($ProgramName,"."))))."_".$InputFile."_".$CurDateTime.".log";
open(LOGFILE, ">> $LogFileName") || NotifyFatal ("", $ProgramName, "Could not open log file $LogFileName", "team_dba", "DIE");
$fh_logfile = *LOGFILE;
$fh_logfile->autoflush(1);

# Open Results File
$ResultsFileName = $LOG_DIR.$InputFile."_RESULTS_" . $TrackingNum . ".log";
open(RESULTSFILE, "> $ResultsFileName") || NotifyFatal ("", $ProgramName, "Could not open Results file $ResultsFileName", "mbuotte", "DIE");
$fh_resultsfile = *RESULTSFILE;
$fh_resultsfile->autoflush(1);

# Verify Login Credentials
$dbh = DBI->connect( 'dbi:Oracle:'.$ClientSID,
	$UserName,
	$Code,
	{ AutoCommit => 0 }
	) || die "\n\nUnable to connect to database $ClientSID as $UserName - Please fix the credentials and try again:\n\n $DBI::errstr\n\n";

# Open Input File
$InputFileName =  $SCRIPTS_DIR . $InputFile;
open(INPUTFILE, "< $InputFileName") || NotifyFatal ($fh_logfile, $ProgramName, "Could not open Input file $InputFileName", "team_dba", "DIE");
$fh_inputfile = *INPUTFILE;
$fh_inputfile->autoflush(1);

# Files used in query validation
$PlanFileName = $LOG_DIR . "Plan.txt";
$TempFileName = $LOG_DIR . "Temp.txt";
$Temp2FileName = $LOG_DIR . "Temp2.txt";


$SQLStatement = '';
# Loop through file breaking out each SQL Statement
$LastLine = 0;
$InComment = 0;
while ($Line = <INPUTFILE>) {
	TrimString($Line);

	# Determine if this line is the end of /* type comment
	if ($InComment == 1) {
		$position = index ($Line, "*/");
		if ( $position != -1 ) {
			$InComment = 0;
			$Line = substr($Line, $position+2);
		}
		next;
	}

	# Skip Blank Lines
	if ($Line eq "") {
		next;
	}

	# Skip Comment Lines
	if (substr($Line,0,1) eq "#") {
		next;
	}

	# Determine if this is the beginning of /* type comment
	if (substr($Line,0,2) eq "/*") {
		$InComment = 1;
		$position = index ($Line, "*/");
		if ( $position != -1 ) {
			$InComment = 0;
		}
		next;
	}

	# Strip off inline comments
	$position = index ($Line, "--");
	if ( $position != -1 ) {
		$Line = substr ($Line, 0, $position);
	}

	$SQLStatement = $SQLStatement . $Line . " ";

	# See if this is the last line of a statement
	$position = index ($Line, ";");
	if ($position != -1) {
		$LastLine = 1;
	}

	if ( $LastLine == 1)  {
		push (@StatementList, $SQLStatement);
		$LastLine = 0;
		$SQLStatement = '';
	}
}

# This shouldn't happen, means last statement does not contain ";"
TrimString($SQLStatement);
if ($SQLStatement ne "") {
	print "\n\n\n\n\n\nThe Last statement in $InputFileName is invalid.  There is no ';' terminating the statement.  \n\n";
	print "No statements in this file have been executed.  The file should be fixed and then retried \n\n\n\n\n\n";
    exit (0);
}

# Validate each statement -- If any statements are invalid, no statements will be processed
$Count = 0;
$InvalidStatements = 0;
foreach $Statement (@StatementList) {
	$Count++;
	TrimString($Statement);
	if ( (uc (substr($Statement,0,6)) eq "INSERT") || 
		 (uc (substr($Statement,0,6)) eq "UPDATE") || 
		 (uc (substr($Statement,0,6)) eq "DELETE")) {

        $Status = system ("$WORKING_DIR/ProcessSQL.sh $VALIDATE $ClientSID $UserName $Code \"$Statement\" $PlanFileName");
		$Status = system ("grep ORA- $PlanFileName > $TempFileName");
		if ( !(-z $TempFileName) ) {
			$InvalidStatements += 1;
			if ($InvalidStatements == 1) {
				print RESULTSFILE "\n\n\n\n\n\nTHERE ARE INVALID STATEMENT(S) IN $InputFileName \n";
				print RESULTSFILE "NO STATEMENTS FROM THIS FILE WILL BE EXECUTED. \n\n";
				print RESULTSFILE "Below are the invalid statement(s) and the error(s):  \n\n\n\n";
			}
			print RESULTSFILE "STATEMENT NUMBER $Count IS INVALID \n\n";
			system ("sed 's/^[ \t]*explain plan for //g' $PlanFileName >> $Temp2FileName");
			open(TEMP2FILE, "< $Temp2FileName") || NotifyFatal ("", $ProgramName, "Could not open log file $LogFileName", "team_dba", "DIE");
			$fh_temp2file = *TEMP2FILE;
			while ($ErrLine = <TEMP2FILE>) {
				print RESULTSFILE "$ErrLine";
			}
			close (TEMP2FILE);
			unlink($Temp2FileName);
			print RESULTSFILE "\n\n\n\n\n\n";
		}
		system ("rm $TempFileName");
		system ("rm $PlanFileName");

	}
	else  {
		$InvalidStatements += 1;
		if ($InvalidStatements == 1) {
			print RESULTSFILE "\n\n\n\n\n\nTHERE ARE INVALID STATEMENT(S) IN $InputFileName \n";
			print RESULTSFILE "NO STATEMENTS FROM THIS FILE WILL BE EXECUTED. \n\n";
			print RESULTSFILE "Below are the invalid statement(s) and the error(s):  \n\n\n\n";
		}
		print RESULTSFILE "STATEMENT NUMBER $Count IS INVALID.  THE ONLY TYPES OF STATEMENTS ALLOWED ARE iNSERT,";
		print RESULTSFILE "UPDATE AND DELETE.\n\n$Statement\n\n\n\n\n\n";
	}  
}

if ( $InvalidStatements > 0 ) {
	close RESULTSFILE;
	$Status = system ("cat $ResultsFileName");
	exit (0);
}


$TestFile = $EXPORT_DIR . "Export_status.dat";
$SuccessMsg = "Export terminated successfully without warnings";
$GrepCommand = "grep "  . "\"" . $SuccessMsg . "\"  " . $ExportLog . " > " . $TestFile;

$SQLStatement = '';
# Find 1st UPDATE/DELETE statement in File
while ($Line = <INPUTFILE>) {
	TrimString($Line);

	# Skip Blank Lines
	if ($Line eq "") {
		next;
	}

	if ( uc(substr($Line,0,6)) eq "UPDATE" ||  uc(substr($Line,0,6)) eq "DELETE") {
		if ($SQLStatement ne '') {
			push (@StatementList, $SQLStatement);
			my $SQLStatement = '';
		}
		$SQLStatement = $Line;
	}
	else {
		$SQLStatement = $SQLStatement . " " . $Line;
	}
}
if ($SQLStatement ne '') {
	push (@StatementList, $SQLStatement);
}

$Count = 0;
$Status = system ("tput clear");
print "\nThe Statement(s) in the file are: \n";
foreach $Statement (@StatementList) {
	$Count++;
	print "\nSTATEMENT #$Count: \n";
	PrintStatement ($Statement);
}

GetYesNo ("Continue processing this file (Y/N)? ", $Reply);
if ($Reply eq "N") {
	print ("\nProgram Stopped \n\n");
	exit
}
$Status = system ("tput clear");

# Go through file and process each statement
$Count = 0;
foreach $Statement (@StatementList) {
	$Statement =~ s/;//g;
	$UpdateStatement = $Statement;
	$SelectStatement = '';
        $Count++;
	$Status = system ("tput clear");
        print "\nBEGIN PROCESSING STATEMENT #$Count: \n";
        PrintStatement ($Statement);

	if (uc (substr($Statement,0,6)) eq "DELETE") {
		$SelectStatement = "SELECT COUNT (*) " . substr($Statement,7);
		$SubStmt = substr($Statement,7);
		TrimString ($SubStmt);
		$Blank = index($SubStmt, ' ');
		$SubStmt = substr($SubStmt,$Blank+1);
                $Blank = index($SubStmt, ' ');	
		if ($Blank < 0) {
			$TableName = $SubStmt;
		}
		else {
			$TableName = substr($SubStmt,0,index($SubStmt, ' '));
		}
		if (index ($TableName,'.') < 0) {
			print ("\nTable names should be fully qualified (ex: holdingdbo.position)\n");
			$Table = $TableName;
		}
		else {
			$Table = substr ($TableName,index ($TableName,'.')+1);
		}
		print "\nCorresponding SELECT Statement: \n";
		PrintStatement ($SelectStatement);
	}
	elsif  (uc (substr($Statement,0,6)) eq "UPDATE") {
		$Statement = substr($Statement,7);
		TrimString($Statement);
#		$TableName = substr($Statement, 0,index($Statement,' '));
		$TableName = substr($Statement, 0,index(uc($Statement),' SET ' ));
		if (index ($TableName,'.') < 0) {
			print ("\nTable names must be fully qualified (ex: holdingdbo.position)\n");
			exit
		}
		else {
			$Table = substr ($TableName,index ($TableName,'.')+1);
		}
		$SelectStatement = "SELECT COUNT (*) FROM " . $TableName . " ";
		$Done=0;
		$LCount = 0;
		$RCount = 0;
		while ($Done ne "1") {
			$WhereIndx = index (uc($Statement),"WHERE");
			if ($WhereIndx < 0) {
				print "\nCorresponding SELECT Statement: \n";
				PrintStatement ($SelectStatement);
				$Done = 1;
			}
			else {
				$SubStmt = substr ($Statement,0,$WhereIndx);
				$LCount =  ($SubStmt =~ tr /(//);
				$RCount =  ($SubStmt =~ tr /)//);
				if ($LCount eq $RCount) {
					$SelectStatement = $SelectStatement . substr($Statement,$WhereIndx);
					print "\nCorresponding SELECT Statement: \n";
					PrintStatement ($SelectStatement);
					$Done = 1;
				}
				else {
					$Statement = $SubStmt . substr($Statement,$WhereIndx+5);
				}
			}
		}
	}  
	$sql = qq{ $SelectStatement };    # Prepare and execute SELECT
	$sth = $dbh->prepare($sql);
	$sth->execute();

	$sth->bind_columns(undef, \$NumRows);
	print "Number of Rows which will be effected:	";
	while( $sth->fetch() ) {
	    print "$NumRows\n\n";
	}

	GetYesNo ("Process this update (Y/N)? ", $Reply);
	if ($Reply eq "N") {
		$Status = system ("tput clear");
		$sth->finish();                           # Close cursor
		$dbh->disconnect();			  # Disconnect
	}
	else {
		TrimString($Table);
		if (index($Table, "") < 0) {
			$SaveName = $Table;
		}
		else {
			$SaveName = substr ($Table, 0,index($Table, ' '));
		}
			
		$TempTableName = "TN_" . $TrackingNum . "_" . $Count . "_" . $SaveName;
		if (length ($TempTableName) > 29) {
			$TempTableName = substr($TempTableName,0,30);
		}
		$CreateStatement = "CREATE TABLE $TempTableName as ( " . $SelectStatement . ")";
		$CreateStatement =~ s/COUNT \(\*\)/\*/g;
		$Status = system ("tput clear");
		print "Creating Temporary table $TempTableName with:\n";
		PrintStatement ($CreateStatement);
		print "\n\n";
		# Create temp table
		$sql = qq{ $CreateStatement };    
		$sth = $dbh->prepare($sql);
		$sth->execute();
		print "Temporary table Created  \n\n";
		push (@TempTableList, $TempTableName);
		print "=====================================================================================  \n\n";

		# Run Update Statement
		print "Running Update Statement: \n";
		PrintStatement ($UpdateStatement);
		print "\n\n";
		$sql = qq{ $UpdateStatement };    
		$sth = $dbh->prepare($sql);
		$sth->execute();
		print "Update Statement Complete \n\n";
		print "=====================================================================================  \n\n";

		# Commit
		print "Performing Commit\n";
		$sql = qq {$Commit};
		$sth = $dbh->prepare($sql);
		$sth->execute();
		print "Commit Complete \n\n";
		print "=====================================================================================  \n\n";
	}
        $sth->finish();                           # Close cursor
###        $dbh->disconnect();                       # Disconnect
}

$dbh->disconnect();                       # Disconnect

# Export Temporary Tables
print "Exporting Tables:\n";
$ExpTableList='';
if (scalar (@TempTableList) == 0) {
	print "NO UPDATES WERE PERFORMED -- No Temporary Tables need to be exported\n";
}
else {
	foreach $Tbl (@TempTableList) {
		if ($ExpTableList eq '') {
			$ExpTableList = "tables=" . $Tbl;
		}
		else {
			$ExpTableList = $ExpTableList . "," . $Tbl; 
		}
		print "$Tbl \n";
	}
}
print "\n";

print ("ExportTableList: $ExpTableList\n");
if (scalar (@TempTableList) != 0) {
	$ExportCommand = "exp " . $UserName . "/" . $Code . "@" . $ClientSID . " file=" . $ExportName;
	$ExportCommand = $ExportCommand . " log=" . $ExportLog . " rows=y " . $ExpTableList;
	$Status = system ($ExportCommand);
    $PrintCommand = "exp " . $UserName . "/*****" . "@" . $ClientSID . " file=" . $ExportName;
    $PrintCommand = $PrintCommand . " log=" . $ExportLog . " rows=y " . $ExpTableList;
	PrintStatement ($PrintCommand);
	$Status = system ($GrepCommand);
	if (-z $TestFile) {
		print ("\n\nThere may have been problems with the export.\n");
		print ("Check Log File $ExportLog");
		print ("The temp tables will NOT be deleted -- Check with DBA");
		print "=====================================================================================  \n\n";
	}
	else {
		print "Export Complete \n\n";
		print "=====================================================================================  \n\n";

		print "Deleting Temporary Table(s): ";
		$dbh = DBI->connect( 'dbi:Oracle:'.$ClientSID,
					 $UserName,
					 $Code,
					 { RaiseError => 1, AutoCommit => 0 }
				       ) || die "Database connection not made: $DBI::errstr";

		foreach $Tbl (@TempTableList) {
			$DelTempTable = "DROP TABLE " .  $Tbl;
			print ("\n$Tbl");
			$sql = qq{ $DelTempTable };    
			$sth = $dbh->prepare($sql);
			$sth->execute();
		}
		print "Deletes Complete \n\n";
		print "=====================================================================================  \n\n";

		# Close DB Connection
		$sth->finish();               # Close cursor
		$dbh->disconnect();			  # Disconnect
	}
	unlink ($TestFile);
}

close LOGFILE;
close INPUTFILE;

