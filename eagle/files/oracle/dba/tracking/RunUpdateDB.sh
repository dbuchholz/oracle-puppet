#!/bin/ksh

##export LD_LIBRARY_PATH=$ORACLE_HOME/lib32

NARG=$#
PROCEDURENAME=$0
if [[ ${NARG} -ne 5  &&  ${NARG} -ne 6 ]]; then
        echo "\n${PROCEDURENAME} requires 5 arguments - Enter parameters or 0 to exit\n"
	read UN?"Enter Your Database User Name: "
	if [[ ${UN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	
	stty -echo
	read PW?"Enter Your Password (this will not be echoed back to the screen): "
	if [[ ${PW} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	stty echo
	echo -n "\n"

	read SID?"Enter Database SID: "
	if [[ ${SID} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read TN?"Enter TrackingNumber: "
	if [[ ${TN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read FN?"Enter update script file name: "
	if [[ ${FN} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
	read LC?"Enter backup location or just hit return to use the default location: "
	if [[ ${LC} = '0' ]]; then
		echo "\nProcess Terminated\n"
		exit
	fi
else 
	UN=$1
	PW=$2
	SID=$3
	TN=$4
	FN=$5
	LC=$6
fi

perl -w UpdateDB.cmd $UN $PW $SID $TN $FN $LC

