#!/bin/ksh
# Script Name: tracking_with_logminer.ksh
# Usage: tracking_with_logminer.ksh [Oracle SID] [Database Username] [Database Password] [SQL Script Name] <Log Directory>
# <Log Directory> is optional.  It will default to /datadomain/export/$ORACLE_SID
# This script can be run with passing parameters at the command line or run interactively and be prompted for inputs.
# [SQL Script Name] is assumed to be located in a sub-directory where this script is located called scripts.
# The SQL Script needs to be fully qualified with semicolons at the end of each SQL statement.
# This script can run any combination of SQL statements, including mixing DDL and DML.
# IMPORTANT:  Remove all COMMIT Statments from SQL script

function password_entry
{
stty -echo
read PW1?"Enter Your Password: "
stty echo
if [[ -z $PW1 ]]
then
 print "\n\nYou did not enter a password.  Aborting here..."
 exit 17
fi
}

function check_sid
{
 sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
 if [[ -z $sid_in_oratab ]]
 then
  print "There is no $ORACLE_SID entry in $ORATAB file"
  exit 18
 fi
}

## MAIN ##
rows_effected_to_abort=2000000

clear
print "\n\nThe SQL that runs through this tracking program needs to be formatted as follows:"
print "1.  REMOVE ALL COMMIT STATEMENTS FROM SQL SCRIPT."
print "2.  All objects need to be fully qualified including the owner of the object."
print "3.  All SQL statements must end with a semicolon(;).\n\n"
read press_continue?"Press Enter to continue... or CTRL/C to exit "
print "\n\n"

if [ "$(dirname $0)" = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir/scripts

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH

if [ $# -lt 4 ]
then
 print "The list of Oracle SID are:\n"
 ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}'
 print "\n"
 read ORACLE_SID?"Enter Oracle SID: "
 if [[ -z $ORACLE_SID ]]
 then
  print "You did not enter an Oracle SID.  Aborting here..."
  exit 1
 fi
 export ORACLE_SID
 check_sid
 print "\n"
 read UN?"Enter Your Database User Name: "
 if [[ -z $UN ]]
 then
  print "You did not enter your database username.  Aborting here..."
  exit 2
 fi
 print "\nNext, you will enter your database password."
 print "For security, password will not be reflected to terminal.\n"
 password_entry
 print "\n"
. /usr/local/bin/oraenv > /dev/null
(( db_version=$(print $ORACLE_HOME | awk -F/ '{print $7}' | awk -F. '{print $1}') ))
if [ $db_version -lt 10 ]
then
 print "This tracking program will not run against and Oracle 9 database."
 print "Please run the tracking using the older script RunUpdateDB.sh"
 print "or take a manual export of the tables effected by the DML changes."
 exit 5
fi
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
connect ${UN}/$PW1
EOF
if [ $? -ne 0 ]
then
 print "Cannot log into $ORACLE_SID Database with $UN Account."
 print "The password may not be correct or the user does not exist."
 exit 3
fi
 clear
 print "\nThe 10 most recently created files in the scripts directory sorted by most recent at bottom:\n"
 ls -ltr $log_dir | tail
 print "\n"
 read FN?"Enter Script Name in scripts Directory with SQL commands to process: "
 print "\n"
 if [[ -z $FN ]]
 then
  print "You did not enter the name of the script to process. Aborting here..."
  exit 4
 fi
 read LC?"Enter backup location or just hit return to use the default location: "
 print "\n"
else 
 ORACLE_SID=$1
 UN=$2
 PW1=$3
 FN=$4
 LC=$5
 check_sid
 export ORACLE_SID
 . /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
connect ${UN}/$PW1
EOF
 if [ $? -ne 0 ]
 then
  print "Cannot log into $ORACLE_SID Database with $UN Account."
  print "The password may not be correct or the user does not exist."
  exit 6
 fi
fi

UN=$(print $UN | tr '[a-z]' '[A-Z]')
FN=$(print $FN | awk -F/ '{print $NF}')
if [[ ! -s $log_dir/$FN ]]
then
 print "File $log_dir/$FN does not exist."
 print "Please create that file, or choose a file that does exist."
 exit 7
fi
bn_FN=$(print $FN | awk -F. '{print $1}')

if [[ -z $LC ]]
then
 LC=/datadomain/export/$ORACLE_SID
 mkdir -p $LC
else
 if [[ ! -d $LC ]]
 then
  mkdir -p $LC > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
   print "Cannot make directory for log files: $LC"
   print "Please run program again and specified a valid directory path."
   exit 8
  fi
 fi
fi

clear
print "\nThe 10 most recently created files in the scripts directory sorted by most recent at bottom:"
ls -ltr $log_dir | tail
most_recent_file=$(ls -ltr $log_dir | tail -1 | awk '{print $NF}' | awk -F/ '{print $NF}')
print "\nThe most recent file created in scripts Directory is: $most_recent_file"
if [ "$most_recent_file" != "$FN" ]
then
 print "\nYou chose a file to process that was not the most recent file created in the scripts Directory.\n"
 print "You chose this file: $FN"
 print "Most recent file is: $most_recent_file"
 print "\nDid you choose the correct file?\n"
fi
print "\nHere is the top 10 lines of the SQL Script you chose: ${FN}\n"
head $log_dir/$FN
print "\n"
read the_answer?"Do you want to continue processing this SQL script? Y or N  Default is N: "
if [[ -z $the_answer ]]
then
 print "\nYou chose to not process SQL script.  Exiting here..."
 exit 9
fi
the_answer=$(print $the_answer | tr '[a-z]' '[A-Z]')
if [ $the_answer != 'Y' ]
then
 print "\nYou chose to not process SQL script.  Exiting here..."
 exit 10
fi

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 0 echo off
SELECT SUPPLEMENTAL_LOG_DATA_MIN FROM V\\$DATABASE;
EOF`
STATUS=$(print $STATUS | sed 's/\n//g')

REDOUNDO=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 0 echo off
select count(1) from dba_objects where owner='AUDIT_UNDO' and object_name='SQL_REDOUNDO';
EOF`
REDOUNDO=$(print $REDOUNDO | sed 's/\n//g')

REDOUNDO_WIDTH=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 0 echo off
select count(1) from dba_tab_columns where table_name='SQL_REDOUNDO';
EOF`
REDOUNDO_WIDTH=$(print $REDOUNDO_WIDTH | sed 's/\n//g')

if [ $STATUS != 'YES' ]
then
print "Adding supplemental log data.  If this hangs for more than five minutes, abort out of the script"
print "with CTRL-C.  The database requires adding supplemental data at time where there are no transactions."
print "To run this manually, run the following commands in the database:\n"
print "alter database add supplemental log data;"
print "alter database add supplemental log data(primary key) columns;"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
alter database add supplemental log data;
alter database add supplemental log data(primary key) columns;
EOF
fi

STATUS2=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 0 echo off
SELECT SUPPLEMENTAL_LOG_DATA_MIN FROM V\\$DATABASE;
EOF`
STATUS2=$(print $STATUS2 | sed 's/\n//g')
if [ $STATUS2 != 'YES' ]
then
 clear
 print "\n\nDatabase $ORACLE_SID is not setup for Log Miner. Please make sure that SUPPLEMENTAL_LOG is enable..."
 exit 11
fi

USER_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<EOF
set echo off feedback off heading off
select username from dba_users where username='AUDIT_UNDO';
EOF`
USER_NAME=$(print $USER_NAME|tr -d " ")
if [[ -z $USER_NAME ]]
then
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
create user audit_undo identified by audit_eagle2011 default tablespace USERS temporary tablespace temp profile ea_service;
alter user audit_undo quota unlimited on USERS;
grant create session,connect,resource to audit_undo;
EOF
fi

if [[ $REDOUNDO -ne 1 ]] || [[ $REDOUNDO_WIDTH -ne 2 ]]
then
 clear
 print "\n\n Enabling Log Miner for the first time. Please wait.....\n\n"
 mkdir -p /u01/app/oracle/audit_${ORACLE_SID}
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
drop table audit_undo.sql_redoundo;
WHENEVER SQLERROR EXIT FAILURE
create table audit_undo.sql_redoundo(username varchar2(30),sql_undo varchar2(4000)) tablespace USERS;
execute dbms_logmnr_d.build('dictionary.ora','/u01/app/oracle/audit_${ORACLE_SID}',options => dbms_logmnr_d.store_in_flat_file);
EOF
 if [ $? -eq 0 ]
 then
  print "\nLog Miner is now enabled for this database\n"
 else
  print "\n\n There was problem enabling Log Miner.. Please fix the error..\n\n"
  exit 13
 fi
fi

now=$(date +'%Y%m%d_%H%M%S')
read want_to_verify?"Do you want to verify row counts? [Y|N] Default is Y: "
if [[ -z $want_to_verify ]]
then
 want_to_verify=Y
fi
want_to_verify=$(print $want_to_verify | tr '[a-z]' '[A-Z]')

if [[ ! $want_to_verify = "Y" ]] && [[ ! $want_to_verify = "N" ]]
then
 print "If entry is made, must enter Y or N at prompt to verify row counts.  No entry is interpreted as wanting to verify."
 exit 14
fi

if [ $want_to_verify = 'Y' ]
then
print "set echo on" > $LC/hold_${FN}.txt
cat $LC/hold_${FN}.txt $log_dir/$FN > $log_dir/hold2_${FN}.txt
mv $log_dir/hold2_${FN}.txt $log_dir/$FN
rm -f $LC/hold_${FN}.txt
$ORACLE_HOME/bin/sqlplus ${UN}/${PW1}<<EOF>/dev/null
set trimspool on define off sqlblanklines on
spool $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt
@$log_dir/$FN
rollback;
EOF
((row_count=$(wc -l $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt | awk '{print $1}')))
if [ $row_count -lt 120 ]
then
 print "\n***** Row Count Verification Begin ********************************"
 cat $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt
 print "***** Row Count Verification End ********************************\n"
else
 print "To verify row counts see: $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt\n"
fi

#rows_created=$(grep ' row' $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt | grep 'created\.' | awk '{sum+=$1} END {printf "%9d\n",sum}')
#rows_updated=$(grep ' row' $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt | grep 'updated\.' | awk '{sum+=$1} END {printf "%9d\n",sum}')
#rows_deleted=$(grep ' row' $LC/rows_effected_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt | grep 'deleted\.' | awk '{sum+=$1} END {printf "%9d\n",sum}')
#total_rows_effected=$(print $rows_created + $rows_updated + $rows_deleted | bc)
#if [ $total_rows_effected -gt $rows_effected_to_abort ]
#then
# print "Total rows affected is greater than $rows_effected_to_abort rows."
# print "The total rows affected is: $total_rows_effected"
# print "The logminer process is not reliable after ${rows_effected_to_abort} rows affected."
# print "If there are multiple statements, break up the statements to be less than $rows_effected_to_abort rows affected and run through tracking."
# print "If there is one statement that is greater than $rows_effected_to_abort rows affected, determine if statement can be broken up into"
# print "less than $rows_effected_to_abort row pieces.  This can sometimes be done with the rownum function.  When a date is in the where clause, choosing an appropriate"
# print "subset of the date range can also sometimes be used."
# print "Aborting here."
# exit 1
#fi

read continue?"Do you want to continue with running this statement? Y or N: "
print "\n"
continue=$(print $continue | tr '[a-z]' '[A-Z]')
if [[ -z $continue ]]
then
 print "Must enter Y or N at prompt to continue running statement."
 exit 15
fi
if [[ ! $continue = "Y" ]] && [[ ! $continue = "N" ]]
then
 print "If entry is made, must enter Y or N to prompt to verify row counts.  No entry is interpreted as wanting to verify."
 exit 16
fi
else
continue=Y
fi
if [ $continue = Y ]
then
 print "Running ${bn_FN}.txt"
 start_sys_time=$(date "+%d-%b-%Y %H:%M:%S" | tr '[a-z]' '[A-Z]')
$ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF
set trimspool on define off sqlblanklines on
spool $LC/run_log_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt
@$log_dir/$FN
EOF
print "\nCreating Undo Records with Logminer...\n"
 end_sys_time=$(date "+%d-%b-%Y %H:%M:%S" | tr '[a-z]' '[A-Z]')
	$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
	set linesize 200
	col sql_redo format a55
	col sql_undo for a55
	ALTER SESSION SET nls_date_format='DD-MON-RRRR hh24:mi:ss';
	BEGIN
	 DBMS_LOGMNR.START_LOGMNR(STARTTIME => '${start_sys_time}', ENDTIME => '${end_sys_time}', OPTIONS => DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG + DBMS_LOGMNR.CONTINUOUS_MINE + DBMS_LOGMNR.COMMITTED_DATA_ONLY);
	END;
	/
	set feedback off
	delete from audit_undo.sql_redoundo where username='$UN';
	commit;
	-- insert into audit_undo.sql_redoundo (SELECT username,sql_undo FROM v\$logmnr_contents where operation in ('INSERT','UPDATE','DELETE','MERGE') and username='$UN' and table_name<>'AUD$');
	DECLARE
	 cursor c1 is SELECT username,sql_undo FROM v\$logmnr_contents where operation in ('INSERT','UPDATE','DELETE','MERGE') and username='$UN' and table_name<>'AUD$';
	 user_name VARCHAR2(30);
	 sql_undo VARCHAR2(4000);
	 x NUMBER :=0;
	 commit_count NUMBER := 10000;
	BEGIN
	 OPEN c1;
	 LOOP
  FETCH c1 INTO user_name,sql_undo;
  EXIT WHEN c1%NOTFOUND;
  x := x + 1;
  INSERT INTO audit_undo.sql_redoundo VALUES(user_name,sql_undo);
  IF x > commit_count THEN
   x := 0;
   COMMIT;
  END IF;
 END LOOP;
 COMMIT;
 CLOSE c1;
END;
/
exec dbms_logmnr.end_logmnr;
EOF
# + DBMS_LOGMNR.PRINT_PRETTY_SQL
print "Generating Undo SQL in file: $LC/undo_sql_${bn_FN}_${UN}_${ORACLE_SID}_${now}.sql"
$ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF> /dev/null
set linesize 4000 pagesize 0 trimspool on echo off feedback off
spool $LC/undo_sql_${bn_FN}_${UN}_${ORACLE_SID}_${now}.sql
select sql_undo from audit_undo.sql_redoundo where username='$UN';
EOF
fi

client_code=$(print $ORACLE_SID | cut -c1-3)
if [ $client_code = dac ]
then
$ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF
set linesize 200 trimspool on
col WHEN format a24
col owner format a20
col obj_name format a25
col SQLTEXT format a80
spool $LC/audit_trail_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt
select to_char(timestamp,'MM/DD/YYYY HH24:MI:SS') WHEN,owner,obj_name,substr(sql_text,1,80) SQLTEXT
from sys.dba_audit_trail where sql_text not like 'SELECT%' and owner<>'SYS'
and timestamp >= sysdate-1/24 and username in (select username from dba_users where profile='EA_PROFILE') order by timestamp;
spool off
EOF
mailrecipients=${UN}@eagleinvsys.com
subject="Audit Report for ${ORACLE_SID}. Best viewed with WordPad....."
uuencode $LC/audit_trail_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt audit_trail_${bn_FN}_${UN}_${ORACLE_SID}_${now}.txt | mailx -s "$subject" $mailrecipients
fi

exit 0


