#!/bin/ksh
#########################################################################################
# Author:	Nirmal S Arri																#
# Date:		10/12/2010																	#
# Usage: 	MultipleUpdates.sh <ScriptName.sql> <SID>									#
# Purpose:	This script is used to run multiple updates, in a single file for a single  #
#           table.																		#
#########################################################################################	
#set -x
ARG=$#
	if [ ${ARG} -ne 2 ]; then
        clear
		echo "Not enough parameter..."
        echo ""
		echo ""
		echo "Usage: MultipleUpdates.sh <ScriptName> <SID>"
		exit 1
	fi

UPDATEFILE=${HOME}/local/dba/tracking/scripts/$1

#if [ -s ${UPDATEFILE} ]; then
   #echo "Clearing Duplicate Records..."
#STATUS=`cat ${UPDATEFILE} |uniq > ${UPDATEFILE}`
  #else
   #echo "File does not exist or it is an empty file..."
#fi

STATUS=`ps -fu oracle |grep -v grep| grep ora_pmon_$2`
if [ $? != 0 ]; then
     echo -e "Database ${ORACLE_SID} is not running. Please try with valid SID..."
     exit 1
fi

ORA_SID=$2
export ORACLE_SID=${ORA_SID}


read TRACKING?"Enter Tracking #:> "

read USERNAME?"Enter your DBA username:> "

stty -echo
read PASSWORD?"Enter your DBA password:> "
stty echo
echo

if [ ${TRACKING} == "" ]; then
  echo "Please provide Tracking number.."
  exit 1
fi


$ORACLE_HOME/bin/sqlplus -L -s ${USERNAME}/${PASSWORD} <<!
set head off
set echo off
set feedback off
select '' from dual;
exit
!

ret_code=$?
 
if [ "$ret_code" -gt 0 ]; then
  echo "Invalid username and/or password"
  exit 1
fi

STATUS=`$ORACLE_HOME/bin/sqlplus -L -s ${USERNAME}/${PASSWORD} <<!
set head off
set echo off
set feedback off
select granted_role from dba_role_privs where grantee=${USERNAME} and granted_role='DBA';
exit
!`

if [[ x$STATUS = 'x' ]]; then
 echo "Sorry you do not have enough privileges to run this script...."
 exit 1
fi


INSERTFILE="${HOME}/local/dba/tracking/scripts/in${TRACKING}."`echo ${UPDATEFILE} |awk -F. '{print $NF}'`

touch tmp.txt
TEMPFILE=tmp.txt
awk ' { if(NR == 1) {print $0 }}' ${UPDATEFILE} > tmp.txt


TABNAME=`cut -f2 -d" " tmp.txt`
SCHEMA=`echo ${TABNAME} |awk -F. '{print $1}'`
TMPNAME=`echo ${TABNAME} |awk -F. '{print $NF}'`"_${TRACKING}_tmp"
LENGTH=`echo ${#TMPNAME}`
if [[ ${LENGTH} > 15 ]]; then
TNAME=`echo ${TMPNAME:0:10}`"_${TRACKING}_tmp"
TMPNAME=${SCHEMA}"."${TNAME}
fi

sed -e 's/^Update.*where//ig' -e "s/^/insert into ${TMPNAME} select * from ${TABNAME} where  /g" ${UPDATEFILE} > ${INSERTFILE}

rm tmp.txt

LOGFILE="${HOME}/local/dba/tracking/scripts/in${TRACKING}.log"
STATUS=`$ORACLE_HOME/bin/sqlplus -s ${USERNAME}/${PASSWORD} <<!
WHENEVER SQLERROR EXIT FAILURE
spool ${LOGFILE}
create table  ${TMPNAME} as select * from  ${TABNAME} where 1=2;
sta ${INSERTFILE}
commit;
sta ${UPDATEFILE}
commit;
spool off
exit
!`

status=$?
if [[ status -ne -0 ]]; then
  echo "Update failed....."
   exit 1
fi

#echo ${STATUS}

clear
echo ""
echo ""
echo "Created Temporary file..."
echo "Update completed...."
echo "Starting Export..."

BACKUP_DIR="/u01/app/oracle/backup/$ORA_SID"

if  [ ! -d  ${BACKUP_DIR} ]; then mkdir -p ${BACKUP_DIR}; fi


exp ${USERNAME}/${PASSWORD} file=${BACKUP_DIR}/${TMPNAME}.dmp rows=y tables=${TMPNAME} log=${BACKUP_DIR}/${TMPNAME}.log

status=$?
if [[ status -ne -0 ]]; then
      echo "Export failed....."
                return 1
        else
STATUS=`$ORACLE_HOME/bin/sqlplus -s ${USERNAME}/${PASSWORD} <<!
drop table ${TMPNAME};
exit
!`
fi

echo ""
echo "Dropped temporary table....."
echo ""
exit 0
