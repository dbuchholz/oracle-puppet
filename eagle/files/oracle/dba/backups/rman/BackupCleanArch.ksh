#!/bin/ksh
#################################################################################
# Purpose:  This script looks for the Archive Log Destination                  ##
#           and checks if the LUN is over 75% full. If true it then            ##
#           runs RMANArcLogBkp_All_Clean.sh to backup archive logs for that    ##
#           database and deletes the arc file to make space.                   ##
#                                                                              ##
# Author: Nirmal S Arri                                                        ##
# Date  : 04/19/2012                                                           ##
#                                                                              ##
#   05/29/2015 Frank Davis   Made compatible with ASM databases                ##
#                                                                              ##
#################################################################################

function RunCleanArch
{
 if [ $ASM_CLIENT = N ]
 then
  ## Get Archive log Destination LUN #
  ARCH_DIR=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
  set echo off feedback off heading off
  col value for a70
  select value from v\\$parameter where name='log_archive_dest_1';
EOF
`
  ARCH_DIR=$(print $ARCH_DIR | tr -d " ")
  ARCH_DIR=$(print $ARCH_DIR | awk -F= '{print $NF}')

  ## Get percentage usage for the Archive log Destination LUN
  ARCH_LUN=$(print $ARCH_DIR | awk -F/ '{print "/"$2}')
  ARCH_LUN_PCT=$($DF | grep "${ARCH_LUN}\$" | awk '{print $5}' | sed 's/%//g')
 
  ## If primary database mount point percentage utilized is over 75%, Backup archive logs and delete to free up disk space.
  if [ $database_type = db ]
  then
   if [ $ARCH_LUN_PCT -gt 75 ]
   then
    nohup /u01/app/oracle/local/dba/backups/rman/RMANArcLogBkp_All_Clean.sh $ORACLE_SID > /dev/null 2>&1 &
    print "Ran RMANArcLogBkp_All_Clean.sh for Primary SID: ${ORACLE_SID}.  Threshold is 75%.  Reached: ${ARCH_LUN_PCT}%  Date: $RUN_DATE" | tee -a $LOG_FILE
   else
    print "No Need to run Archive Log Purge, Usage is only ${ARCH_LUN_PCT}% for Primary SID: ${ORACLE_SID}.  Threshold is 75%."
   fi
  fi
  ## If standby database mount point percentage utilized is over 70%, Backup archive logs and delete to free up disk space.
  if [ $database_type = dr ]
  then
   if [ $ARCH_LUN_PCT -gt 70 ]
   then
    max_seq=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
    set heading off feedback off
    WHENEVER SQLERROR EXIT FAILURE
    select max(sequence#) from v\\$archived_log;
EOF
`
    if [ $? -ne 0 ]
    then
     print "There was an error on reaching the DR database to get the most recent archive log applied to $ORACLE_SID"
     print "Aborting script"
     exit 1
    fi
    max_seq=$(print $max_seq|tr -d " ")
    arch_dest=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
    WHENEVER SQLERROR EXIT FAILURE
    set heading off feedback off
    select value from v\\$parameter where name='log_archive_dest_1';
EOF
`
    if [ $? -ne 0 ]
    then
     print "There was an error on reaching the DR database to get the log archive destination for $ORACLE_SID"
     print "Aborting script"
     exit 2
    fi
    arch_dest=$(print $arch_dest|tr -d " " | awk -F= '{print $NF}')
    third_to_newest_arch=$(ls -l $arch_dest | tail -3 | head -1 | awk '{print $NF}')
    if [[ -n $third_to_newest_arch ]]
    then
     find  ${arch_dest} -type f -name "*.arc" ! -newer ${arch_dest}/$third_to_newest_arch -exec rm -f {} \;
    fi
    print "Ran Archive Log Purge for DR SID: ${ORACLE_SID}.  Threshold is 70%.  Reached: ${ARCH_LUN_PCT}%  Date: $RUN_DATE" | tee -a $LOG_FILE
   else
    print "No Need to run Archive Log Purge, Usage is only ${ARCH_LUN_PCT}% for DR SID: ${ORACLE_SID}.  Threshold is 70%."
   fi
  fi
 else
  if [ $database_type = db ]
  then
   # code for ASM FRA cleanup
   disk_group_used_threshold=50
   export ORACLE_SID=+ASM
   export ORAENV_ASK=NO
   export PATH=/usr/local/bin:$PATH
   . /usr/local/bin/oraenv > /dev/null
   $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
   WHENEVER SQLERROR EXIT FAILURE
   set pages 999 feedback off
   col capacity format 999999999 heading "Capacity(%)"
   col name format a15 heading "Disk Group"
   col "Total MB" format 999999999
   col "Used MB" format 999999999
   spool $WORKING_DIR/$$_diskgroup_info.txt
   select name,total_mb "Total MB",cold_used_mb "Used MB",cold_used_mb/total_mb*100 capacity
   from v\$asm_diskgroup where total_mb>0 and name='FRA';
EOF
   if [ $? -ne 0 ]
   then
    print "\nThere was an error querying the ASM Database."
    print "Exiting here.\n"
    exit 6
   fi
   FRA_PCT=$(grep '^FRA' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $NF}')
   rm -f $WORKING_DIR/$$_diskgroup_info.txt
   if [[ -n $FRA_PCT ]]
   then
    if [ $FRA_PCT -ge $disk_group_used_threshold ]
    then
     ORACLE_SID=$INPUT_SID
     $WORKING_DIR/RMANArcLogBkp_All_Clean.sh $ORACLE_SID
    fi
   fi
  fi
 fi
}
# --------------------------------------------
# funct_check_asm_client
# --------------------------------------------
funct_check_asm_client()
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}
#########################################################
###                  MAIN                             ###
#########################################################
clear
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 DF='df -Ph'
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 DF='df -lk'
fi
if [[ -z $DF ]]
then
 print "Unrecognized operating system.  Must be Linux or SunOS."
 print "Operating system is: $ostype"
 exit 1
fi

DB_LIST=$(ps -ef | grep ora_smon_ | grep -v grep | awk -F_ '{print $NF}')
for INPUT_SID in $DB_LIST
do
 RUN_DATE=$(date +"%m-%d-%Y %r")
 export ORACLE_SID=$INPUT_SID
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH
 export TERM=xterm
 . /usr/local/bin/oraenv > /dev/null

 funct_check_asm_client
 ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
 if [[ -n $ASM_CLIENT ]]
 then
  if [ $ASM_CLIENT = Y ]
  then
   ASM_CLIENT=Y
  fi
 else
  ASM_CLIENT=N
 fi
 

 if [ $ASM_CLIENT = Y ]
 then
  export WORKING_DIR=/usr/local/scripts/dba/backups/rman
  export LOG_FILE=/home/oracle/CRONLOG/RMANArcLogBkp_All_Clean_${ORACLE_SID}.log
 else
   export WORKING_DIR="/u01/app/oracle/local/dba/backups/rman"
   export LOG_FILE=/u01/app/oracle/CRONLOG/RMANArcLogBkp_All_Clean_${ORACLE_SID}.log
 fi

 export BACKUP_STATUS=$WORKING_DIR/${ORACLE_SID}_backup_status.txt

 # Check if CRONLOG directory exists
 if [ $ASM_CLIENT = Y ]
 then
  if [[ ! -d /home/oracle/CRONLOG ]]
  then
   mkdir -p /home/oracle/CRONLOG
  fi
 else
  if [[ ! -d $HOME/CRONLOG ]]
  then
   mkdir -p $HOME/CRONLOG
  fi
 fi

 #database_type will be "db" or "dr"
 dr_test_1=0
 dr_test_2=0
 fal_client=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 set pagesize 0 trimspool on feedback off echo off
 select value from v\\$parameter where name='fal_client';
EOF
`
 look_for_sb=$(print $fal_client|tr -d " ")
 if [[ -n $look_for_sb ]]
 then
  lc_sb=$(print $look_for_sb | tr '[A-Z]' '[a-z]')
  print "$lc_sb" | egrep '^sb_|\_dr$' > /dev/null
  if [ $? -eq 0 ]
  then
   dr_test_1=1
  fi
 fi
 is_mounted=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 set pagesize 0 trimspool on feedback off echo off
 select open_mode from v\\$database;
EOF
`
 is_mounted=$(print $is_mounted|tr -d " ")
 if [[ -n $is_mounted ]]
 then
  if [ "$is_mounted" = "MOUNTED" ]
  then
   dr_test_2=1
  fi
 fi
 if [ $dr_test_1 -eq 1 ] && [ $dr_test_2 -eq 1 ]
 then
  database_type=dr
 else
  database_type=db
 fi
# RunCleanArch $ASM_CLIENT $database_type
 RunCleanArch
done

exit 0

