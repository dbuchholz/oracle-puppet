#!/bin/ksh
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS != 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# MAIN
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
log_dir=$PAR_HOME/backups/rman
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f $PARFILE ]]
then
 STATUS=$(cat $PARFILE | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "INFRASTRUCTURE_DATABASE:")
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g')
 fi
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "${PROGRAM_NAME}:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

INIFILE=$HOME/local/dba/backups/rman/RMANParam.ini
if [[ -a $INIFILE ]]
then
 export BACKUP_BASE=$(grep $ORACLE_SID $INIFILE | awk '{print $2}')
 if [ x$BACKUP_BASE = 'x' ]
 then
  export BACKUP_BASE=$(grep all $INIFILE | awk '{print $2}')
  if [ x$BACKUP_BASE = 'x' ]
  then
   print "$PROGRAM_NAME Failed for ${ORACLE_SID}: Backup Base Location not defined in ${INIFILE}"
   export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID - Backup Base Location not defined in ${INIFILE}"
   exit 5
  fi
 fi

 CATALOG_ID=$(grep CATALOG_ID $INIFILE | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 if [ x$CATALOG_ID = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_ID not defined in $INIFILE"
  export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID - CATALOG_ID not defined in $INIFILE"
   exit 6
 fi

 CATALOG_PASSWD=$(grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [ x$CATALOG_PASSWD = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_PASSWD not defined in $INIFILE"
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID - CATALOG_PASSWD not defined in $INIFILE"
  exit 7
 fi
 export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [ x$CATALOG_DB = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_DB not defined in $INIFILE"
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID - CATALOG_DB not defined in $INIFILE"
  exit 8
 fi
else
 print "$PROGRAM_NAME FAILED: $ORACLE_SID - $INIFILE file does not exist"
 export IMPACT=2
 export URGENCY=3
 SendNotification "$ORACLE_SID - $INIFILE file does not exist"
 exit 9
fi

backup_dir=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off 
select backup_location from inf_monitor.backup_runs
where db_instance=(select instance from inf_monitor.databases where sid='$ORACLE_SID' and dataguard='N')
and end_time > sysdate-7 and rownum<2;
EOF
`

backup_tag=$(ls -ltr $backup_dir/*LOG.TXT | tail -1 | awk '{print $NF}' | awk -F/ '{print $NF}' | awk -F_ '{print $2"_"$3}')

ls -1 $backup_dir/*${backup_tag}* | egrep -v '\.TXT$|\.rcv$|\.sql|\.ctl|_orapw|\.ora$' > $log_dir/x_$$_x.rcv
sed -e "s/^/catalog backuppiece '/g" -e "s/$/';/g" $log_dir/x_$$_x.rcv > $log_dir/y_$$_y.rcv
mv $log_dir/y_$$_y.rcv $log_dir/x_$$_x.rcv
$ORACLE_HOME/bin/rman target / catalog $CATALOG_ID/${CATALOG_PASSWD}@${CATALOG_DB}<<EOF
@$log_dir/x_$$_x.rcv
EOF
rm -f $log_dir/x_$$_x.rcv
exit 0