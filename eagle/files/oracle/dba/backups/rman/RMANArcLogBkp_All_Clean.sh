#!/bin/ksh
# Script Name: RMANArcLogBkp_All_Clean.sh
# This script takes no parameters.
#
# Frank Davis  6/5/2013  Added full path to mount command as it is not picked up in Solaris environments.
# Frank Davis  5/2/2013  Added TYPE Variable definition to distinguish between Prod and non-Prod
# Frank Davis  3/1/2013  Changed the way that DataDomain mount is detected to make compatible with Solaris
# Frank Davis  6/24/2011 Change IPCheck Variable to updated list of VLANS
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications
# -----------------------------------------------------------------------------
function SendNotification
{
 print "$PROGRAM_NAME \n     Machine: $BOX" > $WORKING_DIR/mail.dat
 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail.dat
  rm -f $ERROR_FILE
 fi

 if [[ -n $2 ]]
 then
  if [[ $2 = 'FATAL' ]]
  then
   print "*** This is a FATAL ERROR - $PROGRAM_NAME aborted at this point *** " >> $WORKING_DIR/mail.dat
  fi
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1
}

# --------------------------------------------------------------
# funct_db_online_verify(): Verify that database is online               
# --------------------------------------------------------------
function funct_db_online_verify
{
 STATUS=$(ps -fu oracle | grep -v grep | grep ora_smon_$ORACLE_SID)
 if [[ -z $STATUS ]]
 then
  print "Database $ORACLE_SID is not running.  Can not perform RMAN Backup"
  SendNotification "Database $ORACLE_SID is not running. $PROGRAM_NAME can not run"
  exit 1
 fi
}

# --------------------------------------------------------------------------------------------------------------
# funct_chk_bkup_dir(): Make sure Datadomain is mounted and create backup directory if it doesn't  exist
# --------------------------------------------------------------------------------------------------------------
function funct_chk_bkup_dir
{
 $MOUNT | grep '\/datadomain ' > /dev/null
 if [ $? -ne 0 ]
 then
  SendNotification "$PROGRAM_NAME can not run because datadomain is not mounted"
  exit 1
 fi

 if [[ -n $BACKUP_SET ]]
 then
  mkdir -p $BACKUP_SET
  if [ $? -ne 0 ]
  then
   SendNotification "$PROGRAM_NAME failed for $ORACLE_SID because program could not create directory $BACKUP_SET"
   exit 1
  fi
 fi
}

# -------------------------------------------------------------------
# funct_catalog_check(): Verify ability to connect to RMAN Catalog
# -------------------------------------------------------------------
function funct_catalog_check
{
 export NO_CATALOG=0
 CATALOG_CONNECT=`$ORACLE_HOME/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 CATALOG_CONNECT=$(print $CATALOG_CONNECT | tr -d " ")
 if [ $CATALOG_CONNECT -ne 1 ]
 then
  export NO_CATALOG=1
  SendNotification "Unable to connect to RMAN catalog using $CATALOG_ID/xxxxxxxx@$CATALOG_DB however rman Archivelog backup is continuing" "WARN"
 fi
}

#---------------------------------------------------------------------------------------------------
# funct_archivelog_rman_backup -- To Backup Archive Log between specified Sequence Numbers
#----------------------------------------------------------------------------------------------------
function funct_archivelog_rman_backup
{
 # Create the script to run for the RMAN backup
 if [ $NO_CATALOG -eq 1 ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "	host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "	CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 
 typeset -i i=1
 while [ ${i} -le $NUM_CHANNELS ]
 do
  print "	ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT	
  i=${i}+1;
 done

 print "	BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT 
 print "	FORMAT '$BACKUP_SET/al_${BACKUP_TAG}_%U_archive'" >> $BACKUP_SCRIPT 
 print "	ARCHIVELOG ALL NOT BACKED UP DELETE ALL INPUT ;" >> $BACKUP_SCRIPT 
 print "	host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "	RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT	
  j=${j}+1;
 done

 print "}" >> $BACKUP_SCRIPT 
 print "quit" >> $BACKUP_SCRIPT 

 nohup $ORACLE_HOME/bin/rman >>$BACKUPLOGFILE 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF

process_errors;

}  
# ----------------------------------------------------------------------------------------
# process_errors():   PROCESS errors that occured in backup and in the validitation process
# ----------------------------------------------------------------------------------------
function process_errors
{
 TMP_ERR_FILE=$WORKING_DIR/captureerr_$BACKUP_TAG
 rm -f $TMP_ERR_FILE
 egrep -i 'failure|ORA-|corruption|RMAN-' $BACKUPLOGFILE | grep -v 'RMAN-08137' > $TMP_ERR_FILE
 if [[ -s $TMP_ERR_FILE ]]
 then
  error_msg=$(cat $TMP_ERR_FILE)
  SendNotification "$PROGRAM_NAME Failed for $ORACLE_SID -> See $BACKUPLOGFILE for errors::  $error_msg" 
  print  "$PROGRAM_NAME Failed for $ORACLE_SID -> See $BACKUPLOGFILE for errors::  $error_msg " 
  exit 1
 fi
 rm -f $TMP_ERR_FILE
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
 export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
 set heading off feedback off
 select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

#-----------------------------------
#         MAIN
#-----------------------------------
if [ $# -ne 1 ]
then
 print "$0 requires one argument - Enter Oracle SID"
 read ORACLE_SID?"Enter SID: "
 if [[ -z $ORACLE_SID ]]
 then
  print "\nProcess Terminated\n"
  exit 1
 fi
else
 ORACLE_SID=$1
fi	

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 MOUNT=/bin/mount
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 MOUNT=/etc/mount
fi

export MAILTO=team_dba@eagleaccess.com

export ORACLE_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
export TERM=xterm
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

# Set environment variables
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/backups/rman
else
 export WORKING_DIR="$HOME/local/dba/backups/rman"
fi
export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

HOSTNAME=$(hostname)

if [ $ASM_CLIENT = Y ]
then
 LOG_FILE=/home/oracle/CRONLOG/RMANArcLogBkp_All_Clean_${ORACLE_SID}.log
else
 LOG_FILE=$HOME/CRONLOG/RMANArcLogBkp_All_Clean_${ORACLE_SID}.log
fi

RUN_DATE=$(date +"%d-%m-%Y_%r")

((process_count=$(ps -ef | grep "ksh .*RMANArcLogBkp_All_Clean\.sh $ORACLE_SID" | grep -v grep | wc -l)))
if [ $process_count -gt 1 ]
then
 print "There is already an instance of RMANArcLogBkp_All_Clean.sh running for $ORACLE_SID" >> $LOG_FILE
 print "This attempt will be aborted." >> $LOG_FILE
 exit 1
fi

ps -ef | grep "RMANBackup\.sh $ORACLE_SID" | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 exit 2
fi

print "Ran RMANArcLogBkp_All_Clean.sh for SID: $ORACLE_SID Date: $RUN_DATE" >> $LOG_FILE

INIFILE=$WORKING_DIR/RMANParam.ini
if [[ -f $INIFILE ]]
then
 export BACKUP_BASE=$(grep $ORACLE_SID $INIFILE | awk '{print $2}')
 if [[ -z $BACKUP_BASE ]]
 then
  export BACKUP_BASE=$(grep all $INIFILE | awk '{print $2}')
  if [[ -z $BACKUP_BASE ]]
  then
   print "$PROGRAM_NAME Failed for ${ORACLE_SID}: Backup Base Location  not defined in $INIFILE"
   SendNotification "$ORACLE_SID - Backup Base Location not defined in $INIFILE"
   exit 1
  fi
 fi
 CATALOG_ID=$(grep CATALOG_ID $INIFILE | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_ID ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_ID not defined in $INIFILE"
  SendNotification "${ORACLE_SID} - CATALOG_ID not defined in $INIFILE"
  exit 1
 fi

 CATALOG_PASSWD=$(grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_PASSWD ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_PASSWD not defined in $INIFILE"
  SendNotification "${ORACLE_SID} - CATALOG_PASSWD not defined in $INIFILE"
  exit 1
 fi
 export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_DB ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_DB not defined in $INIFILE"
  SendNotification "$ORACLE_SID - CATALOG_DB not defined in $INIFILE"
  exit 1
 fi

else
 print "$PROGRAM_NAME FAILED: $ORACLE_SID - $INIFILE file does not exist"
 SendNotification "$ORACLE_SID - $INIFILE file does not exist"
 exit 1
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.60\.99' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export BACKUP_SET=$(ls $BACKUP_BASE/evt/prod? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 export TYPE=PROD
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.105\.|10\.60\.103\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export BACKUP_SET=$(ls $BACKUP_BASE/evt/imp? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 export TYPE=IMP
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.70\.4.\.|10\.70\.3.\.|10\.70\.7.\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export BACKUP_SET=$(ls $BACKUP_BASE/pgh/prod? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 export TYPE=PROD
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.70\.14.\.|10\.70\.13.\.|10\.70\.17.\.|10\.70\.173\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export BACKUP_SET=$(ls $BACKUP_BASE/pgh/imp? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 export TYPE=IMP
fi
export BACKUP_MOUNT=$(print $BACKUP_BASE | awk -F/ '{print $2}')

BACKUP_SET="/$BACKUP_MOUNT/$BACKUP_SET/$ORACLE_SID"

export BACKUP_TAG=${ORACLE_SID}_$(date +'%Y%m%d_%H%M%S')
export TRGT_DB="/"
export BACKUP_SCRIPT=$WORKING_DIR/${ORACLE_SID}_ArchiveLog_rman_backup_script.sql
export BACKUPLOGFILE=$BACKUP_SET/${BACKUP_TAG}_ARCHIVE_log.TXT

if [[ -f $PARFILE ]]
then
 STATUS=$(sed '/^#/d' $PARFILE > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
 fi
fi
rm -f $NO_COMMENT_PARFILE
export PATH
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

export NUM_CHANNELS=2

funct_db_online_verify

funct_chk_bkup_dir

funct_catalog_check

funct_archivelog_rman_backup

# Get SCN
SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
set heading off feedback off
select max(NEXT_CHANGE#) from V\\$LOG_HISTORY;
EOF
`
SCN=$(print $SCN|tr -d " ")
export SCN
print "Until SCN Number $SCN" >> $BACKUPLOGFILE

print "\n ------------------------------------------------------------------------------\n "
print "  Please Check Log File Located in  $BACKUPLOGFILE for more details"
print "\n ------------------------------------------------------------------------------\n "
exit 0

