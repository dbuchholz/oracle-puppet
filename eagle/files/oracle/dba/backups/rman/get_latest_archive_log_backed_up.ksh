#!/bin/ksh
ORACLE_SID=$1
. /usr/bin/setsid.sh $ORACLE_SID > /dev/null
$ORACLE_HOME/bin/rman target / catalog rman/rman@eabprd1<<EOF>/dev/null
spool log to list.txt;
LIST BACKUP OF ARCHIVELOG FROM TIME 'SYSDATE-1';
spool log off;
EOF
grep '^  1    [0-9]' list.txt | sort -nk2 | tail -1 | awk '{print $2}'
exit 0
