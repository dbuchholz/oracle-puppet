#!/bin/ksh
print "This works with Oracle 10g and newer.  It will not work with Oracle 9i and lower."
read ORACLE_SID?"Enter name of Oracle SID: "
sid_in_oratab=$(grep -v "^#" /etc/oratab | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "Cannot find Instance $ORACLE_SID in /etc/oratab"
 exit 1
fi
read the_path?"Enter the full path of the files to catalog to ${ORACLE_SID}: "
print "\n\n\n\tORACLE_SID = $ORACLE_SID"
print "\tPath of files to catalog in RMAN for ${ORACLE_SID}: $the_path\n"
read wait_for_prompt?"Is the above information correct?.  Press CTRL-C if not correct to abort program.  Press Enter if correct."
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/rman target / catalog rman/rman@eabprd1<<EOF
catalog start with '$the_path';
EOF
if [ $? -ne 0 ]
then
 print "$ORACLE_SID : Error on: catalog start with $the_path"
 exit 1
fi
exit 0
