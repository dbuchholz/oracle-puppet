#!/bin/ksh
# ******************************************************************************************** 
# NAME:		RMAN_DR_Clean.sh
# 
# AUTHOR:   Poornima Krishnan               
#
# PURPOSE: 	This utility will do a crosscheck on DR and clean the content on catalog of those archive log's not found
#
# USAGE:	RMAN_DR_Clean.sh SID 
#
# INPUT PARAMETERS:
#		SID	Oracle SID of database to backup
#
# Revisions
# ----------
#
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications 
# -----------------------------------------------------------------------------
function SendNotification {

	# Uncomment for debug
	 set -x

	print "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat
	if [[ x$1 != 'x' ]]; then
		print "\n$1\n" >> mail.dat
	fi

	if [[ -a ${ERROR_FILE} ]]; then
		cat $ERROR_FILE >> mail.dat
		rm ${ERROR_FILE}
	fi

	if [[ $2 = 'FATAL' ]]; then
		print "*** This is a FATAL ERROR - ${PROGRAM_NAME} aborted at this point *** " >> mail.dat
	fi

	cat mail.dat | /bin/mail -s "PROBLEM WITH ${TYPE} Environment -- ${PROGRAM_NAME} on ${BOX} for ${ORACLE_SID}" ${MAILTO}
	rm mail.dat

	# Update the mail counter
	MAIL_COUNT=${MAIL_COUNT}+1

	return 0
}

# --------------------------------------------------------------
# funct_db_online_verify(): Verify that database is online               
# --------------------------------------------------------------
funct_db_online_verify(){
 # Uncomment next line for debugging
 set -x

 ps -ef | grep ora_pmon_$ORACLE_SID | grep -v grep > /dev/null
 if [ $? -ne 0 ]
 then
  print "Database $ORACLE_SID is not running.  Cannot perform RMAN Backup"
  SendNotification "Database $ORACLE_SID is not running. $PROGRAM_NAME cannot run "
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 15
 fi
} 

# -------------------------------------------------------------------
# funct_catalog_check(): Verify ability to connect to RMAN Catalog
# -------------------------------------------------------------------
funct_catalog_check(){

	# Uncomment next line for debugging
	 set -x

	export NO_CATALOG=0
	CATALOG_CONNECT=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
		set heading off
		set feedback off
		select 1 from dual;
        exit
EOF`
    CATALOG_CONNECT=$(print $CATALOG_CONNECT|tr -d " ")
	if [ ${CATALOG_CONNECT} != '1' ]; then
		export NO_CATALOG=1
		HOLD_MAILTO=$MAILTO
		export MAILTO=team_dba@eagleaccess.com
		SendNotification "Unable to connect to RMAN catalog using $CATALOG_ID/xxxxxxxx@$CATALOG_DB. $PROGRAM_NAME cannot run without connecting to the catalog "
		export PROCESS_STATUS=FAILURE
		funct_final_audit_update
		exit 15
	fi
}

# --------------------------------------------
# funct_rman_DR_clean():   cross check and delete the archive logs from catalog of those not present on disk
# --------------------------------------------
funct_rman_DR_clean() {
    # Uncomment next line for debugging
      set -x

	# Create the script to run for the RMAN backup
	if [ ${NO_CATALOG} = '1' ]; then
 	   print "connect target $TRGT_DB" > $BACKUP_SCRIPT
	else
    	print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
	    print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
	fi
	print "   CROSSCHECK ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
	print "   DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;" >> $BACKUP_SCRIPT

	print "quit" >> $BACKUP_SCRIPT

nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
    @${BACKUP_SCRIPT}
HOF

}
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging
     set -x

	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
		PERFORM_CRON_STATUS=0
	fi
}


# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {
     set -x   
 	DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off
        set feedback off
        select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
        exit
EOF`
    DB_INSTANCE=`echo -e $DB_INSTANCE |tr -d " "`
    export DB_INSTANCE
    if [ "x$DB_INSTANCE" == "x" ]; then
        PERFORM_CRON_STATUS=0
        return 1
    fi

    Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off
        set feedback off
        select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
        start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
        exit
EOF`
    Sequence_Number=`echo $Sequence_Number|tr -d " "`
    if [[ x$Sequence_Number = 'x' ]]; then
        Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set serveroutput on size 1000000
        set heading off
        set feedback off
        declare i number;
        begin
        insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
        commit;
        dbms_output.put_line (i);
        end;
/
        exit
EOF`
    else
        STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off
        set feedback off
        update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
        commit;
EOF`

    fi
    export Sequence_Number
    return 0

}

# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

    # Uncomment for debug
     set -x

    # Update DBA Auditing Database with end time and backup size
    STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off
        set feedback off
        update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
        commit;

        update INSTANCE_CRON_JOB_RUNS set run_time= (select
        trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
        trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
        trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
        from instance_cron_job_runs where instance=${Sequence_Number})
        where instance=${Sequence_Number};
        commit;
/
        exit
EOF`

    echo  "$PROGRAM_NAME Completed `date +\"%c\"`with a status of ${PROCESS_STATUS} "
}

############################################################
#                       MAIN                               
############################################################
	ORATAB='/etc/oratab'
	HOSTNAME=$(hostname)
	# Uncomment next line for debugging
	set -x

	export ORACLE_SID=$1
	export MAIL_COUNT=0
	grep "^${ORACLE_SID}:" $ORATAB > /dev/null
	if [ $? -ne 0 ]
	then
	 print "\nThe first parameter entered into script is not a valid Oracle SID in $ORATAB."
	 print "Choose a valid Oracle SID from $ORATAB.\n"
	 SendNotification "Not a valid Oracle SID -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
	 exit 2
	fi
	export ORAENV_ASK=NO
	export PATH=/usr/local/bin:$PATH
	. /usr/local/bin/oraenv
	export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
	export LD_LIBRARY_PATH=$ORACLE_HOME/lib
	export ORACLE_BASE=$HOME
	export PROCESS_STATUS=SUCCESS

	export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
	export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F "." '{print $1}')
	export BOX=$(print $(hostname) | awk -F "." '{print $1}')
	export DB_INSTANCE 

	export WORKING_DIR="$ORACLE_BASE/local/dba/backups/rman"
	export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt
	export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

	export Sequence_Number=0
	export PERFORM_CRON_STATUS=0
	export PAR_HOME=$HOME/local/dba
	export PARFILE=$PAR_HOME/BigBrother.ini
	export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
	
	BACKUPLOGFILE="$WORKING_DIR/${ORACLE_SID}_DR_Clean_Log.txt"

	# Check if CRONLOG directory exists
	if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
		mkdir -p ${ORACLE_BASE}/CRONLOG
	fi

	INIFILE=$HOME/local/dba/backups/rman/RMANParam.ini
	print $INIFILE
	if [[ -a $INIFILE ]]; then

		CATALOG_ID=$(grep CATALOG_ID ${INIFILE} | awk -F "=" '{print $2}')
		export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
		if [[ x$CATALOG_ID = 'x' ]]; then
			print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_ID not defined in $INIFILE"
			SendNotification "${ORACLE_SID} - CATALOG_ID not defined in $INIFILE"
			exit 6
		fi

		CATALOG_PASSWD=$( grep CATALOG_PASSWD $INIFILE | awk -F "=" '{print $2}')
		CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
		if [[ x$CATALOG_PASSWD = 'x' ]]; then
			print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_PASSWD not defined in $INIFILE"
			SendNotification "$ORACLE_SID - CATALOG_PASSWD not defined in $INIFILE"
			exit 7
		fi
		export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

		CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F "=" '{print $2}')
		export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
		if [[ x$CATALOG_DB = 'x' ]]; then
			print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_DB not defined in $INIFILE"
			SendNotification "$ORACLE_SID - CATALOG_DB not defined in $INIFILE"
			exit 8
		fi
	else
		print "$PROGRAM_NAME FAILED: $ORACLE_SID - $INIFILE file does not exist"
		SendNotification "$ORACLE_SID - $INIFILE file does not exist"
		exit 9
	fi

	 export MAILTO=team_dba@eagleaccess.com

	export TRGT_DB="/"
	export BACKUP_SCRIPT=$HOME/local/dba/backups/rman/${ORACLE_SID}_rman_dr_crosscheck_script.sql

	if [[ -a ${PARFILE} ]]; then
		STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
		LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
		if [[ -n $LINE ]]; then
			INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
			export PERFORM_CRON_STATUS=1
			export CRON_SID=$(print $INFO | awk '{print $1}')
			export CRON_USER=$(print $INFO | awk '{print $2}')
			CRON_CONNECT=$(print $INFO | awk '{print $3}')
			export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
			# Make sure you can get to the infrastructure database
			funct_check_inf_database
			print $PERFORM_CRON_STATUS
		fi
		LINE=$(cat $NO_COMMENT_PARFILE | grep  "DONT_RUN_ANYTHING:")
		if [[ -n $LINE ]]; then
			DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
		fi
		LINE=$(cat $NO_COMMENT_PARFILE | grep  "${PROGRAM_NAME}:")
		if [[ -n $LINE ]]; then
			DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
		fi
	fi
	rm -f ${NO_COMMENT_PARFILE}

	# Make sure this database is not in list of SIDs that nothing should be run against
	EXCEPTION=$(awk -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
	if [ $EXCEPTION -ne 0 ]
	then
	 SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} no jobs should run for this SID "
	 exit 14
	fi

	# Make sure this database is not in list of SIDs that this job should not be run for
	EXCEPTION=$(awk -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
	if [[ "${EXCEPTION}" != "0" ]]; then
		SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} because it is explicitly excluded in CronStatus.ini file"
		exit 15
	fi
	
	# Add entry into DBA Admin database
	if [ $PERFORM_CRON_STATUS -eq 1 ]
	then
	 funct_initial_audit_update
	 print $DB_INSTANCE
	 export DB_INSTANCE
	fi

	funct_db_online_verify

	funct_catalog_check

    funct_rman_DR_clean

	funct_final_audit_update


######## END MAIN  #########################
exit 0
