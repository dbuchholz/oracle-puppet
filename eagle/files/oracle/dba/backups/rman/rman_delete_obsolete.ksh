#!/bin/ksh
export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" /etc/oratab | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "Cannot find Instance $ORACLE_SID in /etc/oratab"
 exit 1
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/rman target / catalog rman/rman@eabprd1<<EOF
crosscheck backup;
delete noprompt obsolete;
EOF
if [ $? -ne 0 ]
then
 print "$ORACLE_SID :  Error on crosscheck backup or delete obsolete"
 exit 1
fi
exit 0
