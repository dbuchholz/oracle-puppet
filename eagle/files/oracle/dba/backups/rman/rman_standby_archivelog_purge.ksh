#!/bin/ksh
# Script name: rman_standby_archivelog_purge.ksh
# Usage: rman_standby_archivelog_purge.ksh <Oracle SID>
# Bug 3771019: DELETE OBSOLETE FAILS WHEN HAVING STANDBY & PRIMARY ARCHIVELOGS IN REC. CATALOG
#
if [ $# -ne 1 ]
then
 print "One parameter must be entered into this script."
 print "The parameter is the Oracle SID."
 exit 1
fi
export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" /etc/oratab | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThe Oracle SID you entered does not exist on this host: $(hostname)\n"
 exit 2
fi

print "Start of $ORACLE_SID Archive Log Purge at: $(date)"

export PATH=/usr/local/bin:$PATH
export ORAENV_ASK=NO
. /usr/local/bin/oraenv
export TNS_ADMIN=$ORACLE_HOME/network/admin

PAR_HOME=$HOME/local/dba
INIFILE=$PAR_HOME/backups/rman/RMANParam.ini
if [[ -a $INIFILE ]]
then
 CATALOG_ID=$(grep CATALOG_ID $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd ${CATALOG_PASSWD})
 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
fi

rman_catalog_connect_string="$CATALOG_ID/${CATALOG_PASSWD}@$CATALOG_DB"

$ORACLE_HOME/bin/rman<<EOF
connect target /
connect catalog $rman_catalog_connect_string
delete noprompt force archivelog until time 'sysdate-15';
EOF
if [ $? -ne 0 ]
then
 print "$(hostname) ${ORACLE_SID}: There was an error in RMAN Archive Log Purge."
fi

print "End of $ORACLE_SID Archive Log Purge at $(date)"

exit 0
