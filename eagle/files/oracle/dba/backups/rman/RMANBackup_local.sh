#!/bin/ksh
# ******************************************************************************************** 
# NAME:		RMANBackup_local.sh
# 
# AUTHOR: 	Maureen Buotte                 
#
# PURPOSE: 	This utility will perform a RMAN backup of the specified database.  This script has been modified to
#           backup to a location of: /OraRmanBackup/$ORACLE_SID
#           To change the top level directory, /OraRmanBackup, search for OraRmanBackup in this script and make the change
#           to this line only: export BACKUP_BASE=/OraRmanBackup
#
# USAGE:	RMANBackup_local.sh [Oracle SID] [INCREMENTAL] [LEVEL]
#
# INPUT PARAMETERS:
#		Oracle SID is Oracle SID of database to backup
#
# Revisions
# ----------
# Frank Davis           04-Oct-2013     Change backup location to: /OraRmanBackup/$ORACLE_SID
# Frank Davis           02-Oct-2013     Correct bug in script that would cause exit if script was running for first time
# Frank Davis           01-Oct-2013     Prevent the script from running if it is already being run the same SID
# Frank Davis           30-Sep-2013     Remove team_dba@eagleaccess.com from email
# Frank Davis           24-Sep-2013     Remove tech_support@eagleaccess.com from email
# Frank Davis           09-Sep-2013     Changed SendNotification Function to correct error 
# Frank Davis           12-Aug-2013     Fixed MAILTO Address List to include where Service Now was missing
# Frank Davis           19-Jun-2013     Removed unnecessary code that removes orphaned files in IMP
# Frank Davis           17-Jun-2013     Made script compatible with Solaris and Linux
# Frank Davis           13-Jun-2013     Made script compatible with Service Now
# Frank Davis           25-Jul-2012     Changed orphan backup removal from 15 days to 21 days due to incremental backup
# Frank Davis           25-Jul-2012     Changed manual purging of backup files from 14/31 days to 21/38 days due to incremental backup
# Frank Davis           16-Apr-2012     Updated IPCheck Variable to remove Newton DR VLAN and remove 10.70.13X VLAN
# Frank Davis           08-Feb-2012     Removed notification that database was not registered in RMAN catalog but was successfully registered.
# Frank Davis           26-Jan-2012     Added CHECK LOGICAL after BACKUP Command.  Now checks for logical corruptions during backup.
# Frank Davis           01-Dec-2011     Removed 10.62.5. VLAN from Production Pittsburgh IPCheck.  This was stated in error.
# Frank Davis           29-Nov-2011     Added 10.62.5. VLAN to Production Pittsburgh IPCheck.
# Frank Davis           11-Oct-2011     Replaced DELETE NOPROMPT OBSOLETE with DELETE NOPROMPT FORCE OBSOLETE.  For Error: RMAN-06207
# Frank Davis           13-Sep-2011     RMAN Standard Output changed from "input archive log thread=" in Oracle 10g to "input archived log thread=" in Oracle 11g.  Made script compatible with both.
# Frank Davis           30-Jun-2011     Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis           24-Jun-2011     Updated IPCheck Variable to included current list of IP Address VLANs
# Frank Davis           10-Jun-2011     Moved the RESTORE_INFO Log file name creation to after the funct_check_registered function so if the BACKUP_TYPE changes due to not registered
#                                       and a Level 0 backup is done instead of a Level 1, the RESTORE_INFO file name reflects the correct level of backup.
# Frank Davis           31-May-2011     Removed DELETE ALL INPUT and added back 1.5 days delete of archive logs.  This is due to creating standby from backup cannot find archive logs.
# Frank Davis           17-May-2011     Added 10.70.32. VLAN to Production Pittsburgh IPCheck.
# Frank Davis           13-May-2011     Removed deleting of archive log files older than 1.5 days as  makes this unnecessary.
# Frank Davis           12-May-2011     Made an exception for RMAN-08137 Warning.  This is not reported and will not effect SUCCESS Status of backup. 
# Frank Davis           12-May-2011     Removed exit in process_errors.  This prevents the rest of the backup from completing important tasks.
# Frank Davis           11-May-2011     Changed sqlplus output format for online redo logs to prevent wrapping of line in middle of command.
# Frank Davis           11-May-2011     Added NOT BACKED UP 1 TIMES DELETE ALL INPUT to prevent RMAN error and to not back up archive logs multiple times.
# Frank Davis           11-May-2011     Fixed logic that determines if Level 0 backup needs to be run when there is a failure of the previous backup.
# Frank Davis           11-Feb-2011     Changed to restore by log sequence number rather than SCN.  Log sequence number is determined to
#                                       be the most recent archive log backed up in the current backup set. When using SCN as the script was using
#                                       the SCN was from after the archive logs in the current backup set.  This caused the restore from the
#                                       monthly or yearly folder to fail when using that SCN.
# Frank Davis           10-Feb-2011     Change *RESTORE_INFO.LOG to *RESTORE_INFO*.LOG to allow for incremental backup log file pattern.
# Frank Davis           07-Feb-2011     Changed Prod Backups to go to /datadomain/evt/prod1-prod5
# Frank Davis           20-Jan-2011     Changed IMP Backups to go to /datadomain/evt/imp1-imp5
# Frank Davis           16-Nov-2010     Added Incremental Backups
# Frank Davis           05-Nov-2010     Added Pittsburgh Backups
# Maureen Buotte        27-Sep-2010     Fixed orapwd to be orapw
# Ritesh Bose           12-Apr-2010     Modified to include Cummulative Incremental backup in IMP/ Test regions
# Nishigandha Sathe     02-Mar-2010     Modified to add new primary key machines(instance)
# Maureen Buotte        20-Jul-2009     Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte        09-Sep-2009     Change to ensure PROD TEXT files are kept for 31 days
# Maureen Buotte        09-Sep-2009     Add purging of text files and orphaned TEST backups (due to restores)
# Maureen Buotte        02-Sep-2009     Changed retention periods to be 14 days for NON PROD and 31 days for PROD
# Maureen Buotte        10-Aug-2009     Changed Restore Scripts to use SCN at end of backup not SCN from before backup
# Maureen Buotte        06 Aug-2009     Added "CONFIGURE CONTROLFILE AUTOBACKUP OFF" to restore scripts
# Maureen Buotte        16-Jun-2009     Create restore scripts (different versions for 9i and 10g)
# Maureen Buotte        15-Jun-2009     Changed to just use 2 Channels for lowest impact to start, turned on controlfile autobackup
# Maureen Buotte        01-Jun-2009     Changed to create 1 channel per CPU
# Maureen Buotte        25-May-2009     Made RMAN script dynamic and created 1 channel per datafile
# Maureen Buotte        20-May-2009     Added validation of catalog and making sure database is registered
# Maureen Buotte        14-Apr-2009     Added reporting to Database of Databases
# 
# *********************************************************************************************
#alter table inf_monitor.backup_runs add (backup_type char(1) CHECK (backup_type in('F','0','1')));
#update inf_monitor.backup_runs set backup_type='F';
#
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications 
# -----------------------------------------------------------------------------
function SendNotification 
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat
 if [ "x$1" != x ]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail.dat
  rm $ERROR_FILE
 fi

 if [ $2 = FATAL ]
 then
  print "*** This is a FATAL ERROR - ${PROGRAM_NAME} aborted at this point *** " >> $WORKING_DIR/mail.dat
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1

 return 0
}

# --------------------------------------------------------------
# funct_db_online_verify(): Verify that database is online               
# --------------------------------------------------------------
funct_db_online_verify()
{
 ps -ef | grep ora_pmon_$ORACLE_SID | grep -v grep > /dev/null
 if [ $? -ne 0 ]
 then
  print "Database $ORACLE_SID is not running.  Cannot perform RMAN Backup"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Database $ORACLE_SID is not running. $PROGRAM_NAME cannot run "
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 15
 fi
} 

# --------------------------------------------------------------------------------------------------------------
# funct_chk_bkup_dir(): Make sure Datadomain is mounted and create backup directory if it doesn't  exist
# --------------------------------------------------------------------------------------------------------------
funct_chk_bkup_dir()
{
 DD_MOUNTED=$($MOUNT | grep $BACKUP_MOUNT)
 if [[ -z "$DD_MOUNTED" ]]
 then
  export MAILTO='eaglecsi@eagleaccess.com'
  export IMPACT=2
  export URGENCY=3
  SendNotification "$PROGRAM_NAME can not run because datadomain is not mounted"
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 16
 fi

 mkdir -p $BACKUP_LOC
 if [ $? -ne 0 ]
 then
  export IMPACT=2
  export URGENCY=3
  SendNotification "$PROGRAM_NAME failed for $ORACLE_SID because program could not create directory $BACKUP_LOC"
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 17
 fi
}

# ------------------------------------------
# funct_logmode_check(): Check DB log mode               
# ------------------------------------------
funct_logmode_check()
{
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select log_mode from v\\$database;
 quit
EOF`

 if [ ! $STATUS = "ARCHIVELOG" ]
 then
  print "$PROGRAM_NAME required database to be in ARCHIVELOG mode -- $ORACLE_SID can not be backed up"  >>$BACKUPLOGFILE
  export IMPACT=2
  export URGENCY=3
  SendNotification "Database is not in ARCHIVELOG mode -- $ORACLE_SID can not be backed up"  
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 18
 fi
}

# -------------------------------------------------------------------
# funct_catalog_check(): Verify ability to connect to RMAN Catalog
# -------------------------------------------------------------------
funct_catalog_check()
{
 export NO_CATALOG=0
 CATALOG_CONNECT=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
 set heading off feedback off
 select 1 from dual;
 exit
EOF`
 CATALOG_CONNECT=$(print $CATALOG_CONNECT|tr -d " ")
 if [ $CATALOG_CONNECT != '1' ]
 then
  export NO_CATALOG=1
  if [ $BACKUP_TYPE='INCREMENTAL' ]
  then
   export BACKUP_TYPE=0
  fi
  export IMPACT=2
  export URGENCY=3
  print "Unable to connect to RMAN catalog using $CATALOG_ID/xxxxxxxx@$CATALOG_DB however rman backup is continuing"
  export PROCESS_STATUS=WARNING
 fi
}

# -------------------------------------------------------------------------------------------------------------
# funct_check_registered(): Verify the database is registered in the RMAN catalog and, if not, register it
# -------------------------------------------------------------------------------------------------------------
funct_check_registered()
{
 DBID=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select dbid from v\\$database;
 exit
EOF`
 DBID=$(print $DBID|tr -d " ")

 REGISTERED=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
 set heading off feedback off
 select count(*) from rc_database where dbid=${DBID} and upper(name)=upper('${ORACLE_SID}');
 exit
EOF`
 REGISTERED=$(print $REGISTERED|tr -d " ")

 if [ $REGISTERED != '1' ]
 then
  STATUS=`${ORACLE_HOME}/bin/rman <<EOF
  connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB
  connect target $TRGT_DB
  register database;
  $RETENTION_PERIOD
  configure controlfile autobackup ON;
  configure controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl';
  quit
EOF`
  REGISTERED=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
  set heading off feedback off
  select count(*) from rc_database where dbid=${DBID};
  exit
EOF`
  if [ $BACKUP_TYPE='INCREMENTAL' ]
  then
   export BACKUP_TYPE=0
  fi
  REGISTERED=$(print $REGISTERED|tr -d " ")
  if [ $REGISTERED != '1' ]
  then
   export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID was not registered in the RMAN catalog $CATALOG_DB and an attempt to register the database failed.  The RMAN backup will continue but this needs to be resolved" "WARN"
   export PROCESS_STATUS=WARNING
  else
   export PROCESS_STATUS=SUCCESS
  fi
 fi
}
# --------------------------------------------
# funct_rman_backup():   Backup database in its entirety
# --------------------------------------------
funct_rman_backup()
{
 # Touch a file to be used later to determine which files are part of this backup set
 touch $START_FILE

 # Create the script to run for the RMAN backup
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   CROSSCHECK ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT

 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done

 print "   BACKUP $CHECK_LOGICAL TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_full_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1 ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   sql \"alter system SWITCH LOGFILE\";" >> $BACKUP_SCRIPT
 
 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   FORMAT '$BACKUP_LOC/al_${BACKUP_TAG}_%U_full_archive'" >> $BACKUP_SCRIPT
 print "   ARCHIVELOG ALL NOT BACKED UP 1 TIMES ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "  FORMAT '$BACKUP_LOC/cf_${BACKUP_TAG}_%U_full_ctrl'" >> $BACKUP_SCRIPT
 print "   CURRENT CONTROLFILE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   report schema;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done

 print "   CROSSCHECK BACKUP;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT FORCE OBSOLETE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT ARCHIVELOG ALL COMPLETED BEFORE 'SYSDATE-1.5';" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>$BACKUPLOGFILE 2>&1 <<HOF
 @$BACKUP_SCRIPT
HOF

 process_errors;

}
# --------------------------------------------
# funct_rman_level_0_backup():   Backup Level 0 Incremental Backup
# --------------------------------------------
funct_rman_level_0_backup()
{
 # Touch a file to be used later to determine which files are part of this backup set
 touch $START_FILE

 # Create the script to run for the RMAN backup
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   CROSSCHECK ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT

 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done

 print "   BACKUP $CHECK_LOGICAL INCREMENTAL LEVEL 0 TAG \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_level_0_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   sql \"alter system SWITCH LOGFILE\";" >> $BACKUP_SCRIPT

 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   FORMAT '$BACKUP_LOC/al_${BACKUP_TAG}_%U_level_0_archive'" >> $BACKUP_SCRIPT
 print "   ARCHIVELOG ALL NOT BACKED UP 1 TIMES ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "  FORMAT '$BACKUP_LOC/cf_${BACKUP_TAG}_%U_full_ctrl'" >> $BACKUP_SCRIPT
 print "   CURRENT CONTROLFILE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   report schema;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done

 print "   CROSSCHECK BACKUP;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT FORCE OBSOLETE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT ARCHIVELOG ALL COMPLETED BEFORE 'SYSDATE-1.5';" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF

 process_errors;

}

# --------------------------------------------
# funct_rman_level_1_backup():   Backup Level 1 Incremental Backup
# --------------------------------------------
funct_rman_level_1_backup()
{
 # Touch a file to be used later to determine which files are part of this backup set
 touch ${START_FILE}

 # Create the script to run for the RMAN backup
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   CROSSCHECK ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT

 typeset -i i=1
 while [ $i -le $NUM_CHANNELS ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done

 print "   BACKUP $CHECK_LOGICAL INCREMENTAL LEVEL 1 CUMULATIVE TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_level_1_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   sql \"alter system SWITCH LOGFILE\";" >> $BACKUP_SCRIPT

 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   FORMAT '$BACKUP_LOC/al_${BACKUP_TAG}_%U_level_1_archive'" >> $BACKUP_SCRIPT
 print "   ARCHIVELOG ALL NOT BACKED UP 1 TIMES ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "  FORMAT '$BACKUP_LOC/cf_${BACKUP_TAG}_%U_full_ctrl'" >> $BACKUP_SCRIPT
 print "   CURRENT CONTROLFILE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   report schema;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ $j -le $NUM_CHANNELS ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done

 print "   CROSSCHECK BACKUP;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT FORCE OBSOLETE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 print "   DELETE NOPROMPT ARCHIVELOG ALL COMPLETED BEFORE 'SYSDATE-1.5';" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>$BACKUPLOGFILE 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF

 process_errors;

}
# ------------------------------------------------------
# funct_control_backup():  Backup control file to trace
# -------------------------------------------------------
funct_control_backup()
{
 CONTROLFILE_NAME=$BACKUP_LOC/${BACKUP_TAG}_CONTROL_FILE.sql;
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 alter database backup controlfile to trace as '${CONTROLFILE_NAME}';
 exit
EOF`
}

# ---------------------------------------------------------------
# funct_create_restore_log(): Create log with backup information 
# ---------------------------------------------------------------
funct_create_restore_log()
{
 # Get the list of datafiles
 print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
 print  "DATAFILES: " >> ${RESTOREFILE}
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a ${RESTOREFILE}
 set heading off feedback off pagesize 500
 select 'DATAFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M'
 from V\$DATAFILE;
 quit
EOF

 # Get the number of datafiles
 NUM_DATA_FILES=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF 
 set heading off feedback off
 select count(*) from V\\$DATAFILE;
 quit
EOF`
 export NUM_DATA_FILES

 # Get the list of Temporary Files
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 print  "Temporary Files: " >> $RESTOREFILE
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a $RESTOREFILE
 set heading off feedback off
 select 'TEMPFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M'
 from V\$TEMPFILE;
 quit
GOF

 # Get the list of redo logs
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 print  "REDO LOGS: " >> $RESTOREFILE
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTOREFILE
 set heading off feedback off pagesize 500
 select 'LOGFILE | '||a.group#||' | '||member||' | '||a.bytes/1024/1024||'M' from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
 quit
BOF


 # Get SCN 
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select max(next_change#) from V\\$LOG_HISTORY;
 exit
EOF`
 SCN=$(print $SCN|tr -d " ")
 export SCN

 print "DBID:  ${DBID}" | tee -a $RESTOREFILE
 print "SCN before backup for information purpose only [max(next_change#)]:  ${SCN}" | tee -a $RESTOREFILE
 print "This SCN is not required to restore.  The SCN after the backup will be in the restore scripts." | tee -a $RESTOREFILE
 print "BACKUP_TAG:  ${BACKUP_TAG}" | tee -a $RESTOREFILE
 print "BACKUPDIR:  $BACKUP_LOC" | tee -a $RESTOREFILE
}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_9i(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_9i()
{
 BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

 # Create the controlfile restore script
 print "set dbid $DBID " >> $RESTORE_CONTROLFILE_9i
 print " " >> $RESTORE_CONTROLFILE_9i
 print "run {  " >> $RESTORE_CONTROLFILE_9i
 print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl'; " >> $RESTORE_CONTROLFILE_9i
 print "	restore controlfile from autobackup maxdays 7; " >> $RESTORE_CONTROLFILE_9i
 print "	alter database mount; " >> $RESTORE_CONTROLFILE_9i
 print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> $RESTORE_CONTROLFILE_9i
 print "}  " >> $RESTORE_CONTROLFILE_9i

 # Create the Data files restore script
 print "run {  " >> $RESTORE_DATA_FILES_9i
 print "	allocate channel 'RCHL1' device type disk;  " >> $RESTORE_DATA_FILES_9i
 print "	allocate channel 'RCHL2' device type disk;  " >> $RESTORE_DATA_FILES_9i

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a $RESTORE_DATA_FILES_9i
 set heading off feedback off pagesize 500
 select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$DATAFILE;
 quit
EOF

 print "" >> $RESTORE_DATA_FILES_9i
 print "$restore_by_log_sequence" >> $RESTORE_DATA_FILES_9i
 print "restore database;  " >> $RESTORE_DATA_FILES_9i
 print "switch datafile all;  " >> $RESTORE_DATA_FILES_9i
 print "recover database;  " >> $RESTORE_DATA_FILES_9i
 print "}  " >> $RESTORE_DATA_FILES_9i

 # Create the TEMP files restore script
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF | tee -a $RESTORE_TEMP_FILES_9i
 set heading off feedback off
 select 'Alter tablespace  ' || t.name || ' add TEMPFILE ''' || (replace (p.name,'$ORACLE_SID','xxxxxx')) || ''' size ' || bytes/1024/1024 || 'M;'
 from V\$TEMPFILE p, v\$TABLESPACE t where p.ts#=t.ts#;
 quit
GOF

 # Create the Redo files script file
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTORE_REDO_9i
 set heading off feedback off pagesize 500 trimspool on linesize 200
 select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
 quit
BOF

}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_10g(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_10g()
{
 BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

 # Create the controlfile restore script
 print "set dbid $DBID " >> $RESTORE_CONTROLFILE_10g
 print " " >> $RESTORE_CONTROLFILE_10g
 print "run {  " >> $RESTORE_CONTROLFILE_10g
 print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl';  "  >> $RESTORE_CONTROLFILE_10g
 print "	restore controlfile from autobackup maxdays 7; " >> $RESTORE_CONTROLFILE_10g
 print "	alter database mount; " >> $RESTORE_CONTROLFILE_10g
 print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> $RESTORE_CONTROLFILE_10g
 print "}  " >> $RESTORE_CONTROLFILE_10g

 # Create the Data/Temp files restore script
 print "run {  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "	allocate channel 'RCHL1' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "	allocate channel 'RCHL2' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off pagesize 500
 select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$DATAFILE;
 quit
EOF

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off
 select 'SET NEWNAME FOR TEMPFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$TEMPFILE;
 quit
GOF

 print "" >> $RESTORE_DATA_TEMP_FILES_10g
 print "$restore_by_log_sequence" >> $RESTORE_DATA_TEMP_FILES_10g
 print "restore database;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "switch datafile all;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "switch tempfile all;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "recover database;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "}  " >> $RESTORE_DATA_TEMP_FILES_10g

 # Create the Redo files script file
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTORE_REDO_10g
 set heading off feedback off pagesize 500 trimspool on linesize 200
 select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
 quit
BOF

}

# ---------------------------------------------------
# funct_init_backup():  Backup initxxxxxx.ora file
# ---------------------------------------------------
funct_init_backup()
{
 #Copy current init<SID>.ora file to backup directory
 print  " Copying current init${ORACLE_SID}.ora file from spfile"  >> $BACKUPLOGFILE

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $BACKUPLOGFILE
 set heading off feedback off
 create pfile='${BACKUP_LOC}/${BACKUP_TAG}_init${ORACLE_SID}.ora' from SPFILE;
 quit
BOF
 if [ $? -ne 0 ]
 then
  print "Error Copying the init.ora file to the backup location as init${ORACLE_SID}_${BACKUP_TAG}.ora" >> $BACKUPLOGFILE
 fi

 cp ${ORACLE_HOME}/dbs/orapw${ORACLE_SID} ${BACKUP_LOC}/${BACKUP_TAG}_orapw${ORACLE_SID}
}

# ----------------------------------------------------------------------------------------
# process_errors():   PROCESS errors that occured in backup and in the validitation process
# ----------------------------------------------------------------------------------------
process_errors()
{
 TMP_ERR_FILE=$HOME/local/dba/backups/rman/captureerr_$BACKUP_TAG
 rm -rf $TMP_ERR_FILE
 cat $BACKUPLOGFILE | egrep -i 'failure|ORA-|corruption|rman-|RMAN-'|grep -v RMAN-08137|tee $TMP_ERR_FILE
 if [ $? -eq 0 ]
 then
  if [[ -s $TMP_ERR_FILE ]]
  then
   error_msg=$(cat $TMP_ERR_FILE)
   if [ $TYPE = PROD ]
   then
    export IMPACT=1
    export URGENCY=1
   else
    export IMPACT=2
    export URGENCY=3
   fi
   SendNotification "${PROGRAM_NAME} Failed for ${ORACLE_SID} -> See ${BACKUPLOGFILE} for errors::  $error_msg" 
   print  "$PROGRAM_NAME Failed for $ORACLE_SID -> See $BACKUPLOGFILE for errors::  $error_msg " 
   export PROCESS_STATUS=FAILURE
  fi
 fi
	
 rm -rf $TMP_ERR_FILE
}
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS != 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
 exit
EOF`
 DB_INSTANCE=$(print $DB_INSTANCE |tr -d " ")
 export DB_INSTANCE
 if [ "x$DB_INSTANCE" == "x" ]
 then
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID on $BOX does not exist in $CRON_SID -> $PROGRAM_NAME will run for $ORACLE_SID but $CRON_SID will not be updated" 
  PERFORM_CRON_STATUS=0
  return 1	
 fi

 DB_DBID=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select dbid from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
 exit
EOF`
 DB_DBID=$(print $DB_DBID |tr -d " ")
 export DB_DBID

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from BACKUP_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
 exit
EOF`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [ x$Sequence_Number = 'x' ]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000
  set heading off feedback off
  declare i number;
  begin
   insert into BACKUP_RUNS values (backup_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','${BACKUP_LOC}','${DB_DBID}','${BACKUP_TAG}','','0','${BACKUP_TYPE}') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
  exit
EOF`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update BACKUP_RUNS set start_time=sysdate, dbid='${DB_DBID}', backup_tag='${BACKUP_TAG}' where instance=${Sequence_Number};
  commit;
EOF`

 fi
 export Sequence_Number
 return 0
}

# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update 
# ---------------------------------------------------------------------------------------------
funct_final_audit_update ()
{
 # Update DBA Auditing Database with end time and backup size
 if [ $PERFORM_CRON_STATUS = 1 ]
 then
  typeset -i BACKUP_SIZE=$(find $BACKUP_LOC -newer $START_FILE -type f -exec du -sk {} \; | awk  '{print $1}' |  awk '{sum+=$1} END {print sum}')
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update BACKUP_RUNS set end_time=sysdate,backup_size=${BACKUP_SIZE}/1024,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1 where instance=${Sequence_Number};
  commit;
  update BACKUP_RUNS set run_time= (select
  trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
  trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
  trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
  from backup_runs where instance=${Sequence_Number}),backup_type='$BACKUP_TYPE'
  where instance=${Sequence_Number};
  commit;
  exit
EOF`
 fi

 rm -f $START_FILE
 print  "${ORACLE_SID}, RMAN Backup Completed $(date +\"%c\") with a status of $PROCESS_STATUS " |tee -a $BACKUPLOGFILE

 if [ $BACKUP_TYPE -eq 0 ]
 then
  print "$PROCESS_STATUS" > $LEVEL_0_BACKUP
 fi
}
############################################################
#                       MAIN                               
############################################################
HOSTNAME=$(hostname)
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 AWK=awk
 MOUNT=/bin/mount
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 AWK=nawk
 MOUNT=/etc/mount
fi
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
export WORKING_DIR="$HOME/local/dba/backups/rman"
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export ORACLE_SID=$1

ps -ef | grep 'RMANBackup_local\.sh' | grep -w $ORACLE_SID | grep '\/bin\/ksh' | wc -l > $WORKING_DIR/running_count_${ORACLE_SID}_backups.txt
backups_running=$(cat $WORKING_DIR/running_count_${ORACLE_SID}_backups.txt)
rm -f $WORKING_DIR/running_count_${ORACLE_SID}_backups.txt
if [ $backups_running -gt 1 ]
then
 exit 1
fi

IPCheck=$(cat /etc/hosts | grep $HOSTNAME | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [ "x$IPCheck" != "x" ]
then
 export TYPE=PROD
else
 export TYPE=IMP
fi
export MAILTO='eaglecsi@eagleaccess.com'
if [ $# -lt 1 ] || [ $# -gt 3 ]
then
 print "\n$0 Failed: Incorrect number of arguments -> $0 ORACLE_SID [INCREMENTAL] [LEVEL]"
 print "The second parameter INCREMENTAL is optional.  If not specified, backup is FULL.\n"
 print "If specified, the only valid value for the second parameter is INCREMENTAL.\n"
 print "The third parameter LEVEL of incremental backup is optional. If specified, must be 0 or 1\n"
 print "If third parameter LEVEL is not specified and second parameter INCREMENTAL is specified,"
 print "then the INCREMENTAL backup will be a Level 0 backup.\n"
 export IMPACT=2
 export URGENCY=3
 SendNotification "Incorrect number of arguments -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
 exit 1
fi

grep "^${ORACLE_SID}:" $ORATAB > /dev/null
if [ $? -ne 0 ]
then
 print "\nThe first parameter entered into script is not a valid Oracle SID in $ORATAB."
 print "Choose a valid Oracle SID from $ORATAB.\n"
 export IMPACT=2
 export URGENCY=3
 SendNotification "Not a valid Oracle SID -> $PROGRAM_NAME ORACLE_SID [INCREMENTAL] [LEVEL]"
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export LEVEL_0_BACKUP=$HOME/local/dba/backups/rman/${ORACLE_SID}_level_0_status.txt

BACKUP_TYPE=F
BACKUP_FORMAT=FULL
if [ $# -gt 1 ]
then
 BACKUP_TYPE=0
 BACKUP_FORMAT=$(print $2 | tr '[a-z]' '[A-Z]')
 if [ $BACKUP_FORMAT != INCREMENTAL ]
 then
  print "\nA second parameter was entered but only one value for the second parameter is allowed."
  print "The only allowed value for the second parameter is:  INCREMENTAL\n"
  print "Omit the second parameter to perform a FULL backup or enter INCREMENTAL for the second parameter to perform an incremental backup.\n"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Not a valid second parameter -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
  exit 3
 fi
fi

if [ $# -eq 3 ]
then
 BACKUP_TYPE=$3
 if [ $BACKUP_TYPE != 0 ] && [ $BACKUP_TYPE != 1 ]
 then
  print "Level of Incremental backup must be 0 or 1"
  print "For example:  $0 INCREMENTAL 0"
  print "For example:  $0 INCREMENTAL 1"
  print "Or just omit the third parameter and it will default to 0"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Not a valid third parameter LEVEL -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
  exit 4
 fi
fi

export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export DB_INSTANCE 

export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt
export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

export PROCESS_STATUS=SUCCESS
export Sequence_Number=0
export BACKUP_SIZE=0
export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

mkdir -p $HOME/CRONLOG

export BACKUP_BASE=/OraRmanBackup
CATALOG_ID=rman
CATALOG_PASSWD=473H
export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)
CATALOG_DB=eabprd1

export MAILTO='eaglecsi@eagleaccess.com'
export BACKUP_MOUNT=$(print $BACKUP_BASE | awk -F\/ '{print $NF}')
export BACKUP_LOC=$BACKUP_BASE/$ORACLE_SID

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.60\.99' | awk '{print $1}')
if [ "x$IPCheck" != "x" ]
then
 export TYPE=PROD
 #export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 31 DAYS;'
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.105\.|10\.60\.103\.' | awk '{print $1}')
if [ "x$IPCheck" != "x" ]
then
 export TYPE=IMP
 #export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;'
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.70\.4.\.|10\.70\.3.\.' | awk '{print $1}')
if [ "x$IPCheck" != "x" ]
then
 export TYPE=PROD
 #export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 31 DAYS;'
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | grep '10\.70\.14.\.' | awk '{print $1}')
if [ "x$IPCheck" != "x" ]
then
 export TYPE=IMP
 #export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;'
fi

the_status=$(find $BACKUP_LOC -mtime -2 -name "*_LOG.TXT" | tail -1 | xargs grep 'RMAN Backup Completed ' | awk '{print $NF}')
if [[ -n $the_status ]]
then
 if [ $the_status != SUCCESS ]
 then
  BACKUP_TYPE=0
 fi
else
 BACKUP_TYPE=0
fi

funct_chk_bkup_dir

export BACKUP_TAG=${ORACLE_SID}_$(date +'%Y%m%d_%H%M%S')
export TRGT_DB="/"
export BACKUP_SCRIPT=$HOME/local/dba/backups/rman/${ORACLE_SID}_rman_backup_script.sql
export ORPHANED_BACKUPS=$HOME/local/dba/backups/rman/${ORACLE_SID}_Orphaned_Backups.txt
export ORPHANED_BACKUP_DATES=$HOME/local/dba/backups/rman/${ORACLE_SID}_Orphaned_Backup_Dates.txt
export ORPHANED_FILE_DELETES=$HOME/local/dba/backups/rman/${ORACLE_SID}_Orphaned_File_Deletes.txt
export TEXT_FILE_DELETES=$HOME/local/dba/backups/rman/${ORACLE_SID}_TEXT_FILE_Deletes.txt

BACKUPLOGFILE="$BACKUP_LOC/${BACKUP_TAG}_LOG.TXT"

# 10g Restore script files
RESTORE_CONTROLFILE_10g="$BACKUP_LOC/Restore10gControlFile_${BACKUP_TAG}.rcv"
RESTORE_DATA_TEMP_FILES_10g="$BACKUP_LOC/Restore10gDataTempFiles_${BACKUP_TAG}.rcv"
RESTORE_REDO_10g="$BACKUP_LOC/Restore10gRedo_${BACKUP_TAG}.rcv"

# 9i Restore script files
RESTORE_CONTROLFILE_9i="$BACKUP_LOC/Restore9iControlFile_${BACKUP_TAG}.rcv"
RESTORE_DATA_FILES_9i="$BACKUP_LOC/Restore9iDataFiles_${BACKUP_TAG}.rcv"
RESTORE_TEMP_FILES_9i="$BACKUP_LOC/Restore9iTempFiles_${BACKUP_TAG}.rcv"
RESTORE_REDO_9i="${BACKUP_LOC}/Restore9iRedo_${BACKUP_TAG}.rcv"

if [[ -a $PARFILE ]]
then
 STATUS=$(cat $PARFILE | sed -e '/^#/d'  > $NO_COMMENT_PARFILE)
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "INFRASTRUCTURE_DATABASE:")
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "DONT_RUN_ANYTHING:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "${PROGRAM_NAME}:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

# Make sure this database is not in list of SIDs that nothing should be run against
EXCEPTION=$($AWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [ $EXCEPTION -ne 0 ]
then
 export IMPACT=2
 export URGENCY=3
 SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} no jobs should run for this SID "
 exit 14
fi

# Make sure this database is not in list of SIDs that this job should not be run for
EXCEPTION=$($AWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [ $EXCEPTION != 0 ]
then
 export IMPACT=2
 export URGENCY=3
 SendNotification "$PROGRAM_NAME will not run for $ORACLE_SID because it is explicitly excluded in CronStatus.ini file"
 exit 15
fi

ORAVER=$(print $ORACLE_HOME | grep 9.2)
if [ x$ORAVER = 'x' ]
then
 export Version_10g=1
 export Version_9i=0
 export CHECK_LOGICAL='CHECK LOGICAL'
else
 export Version_9i=1
 export Version_10g=0
 export CHECK_LOGICAL=''
fi

export START_FILE=$BACKUP_LOC/START_TIME.txt

print " Starting RMAN Backup of ....  $ORACLE_SID on $(date +\"%c\")" 

# Add entry into DBA Admin database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 funct_initial_audit_update
 export DB_INSTANCE
fi

funct_db_online_verify
funct_logmode_check

funct_catalog_check
if [ $NO_CATALOG -eq 0 ]
then
 funct_check_registered
fi

if [ $BACKUP_FORMAT = FULL ]
then
 RESTOREFILE="$BACKUP_LOC/${BACKUP_TAG}_RESTORE_INFO.TXT"
else
 RESTOREFILE="$BACKUP_LOC/${BACKUP_TAG}_RESTORE_INFO_level_${BACKUP_TYPE}.TXT"
fi

funct_create_restore_log

print "${ORACLE_SID}, RMAN Backup Started on $(date +\"%c\")" |tee -a $BACKUPLOGFILE
export NUM_CHANNELS=2

if  [ $BACKUP_FORMAT = FULL ]
then
 funct_rman_backup
else
 if [ $BACKUP_TYPE -eq 0 ]
 then
  funct_rman_level_0_backup
 else
  funct_rman_level_1_backup
 fi
fi

# for PROD databases, create a standby control file
if  [ $TYPE = PROD ]
then
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 alter database create standby controlfile as  '${BACKUP_LOC}/standbycontrol_${BACKUP_TAG}.ctl';
 quit
EOF`
fi

newest_archive_log_in_backup=$(grep 'input archive.* log thread=[0-9] sequence=' $BACKUPLOGFILE | awk '{print $5}' | awk -F= '{print $NF}' | sort -n | tail -1)
export restore_by_log_sequence="set until sequence $newest_archive_log_in_backup thread 1;"

# Get the SCN after the backup
SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
set heading off feedback off
select max(next_change#) from V\\$LOG_HISTORY;
exit 16
EOF`
SCN=$(print $SCN|tr -d " ")
print "SCN retrieval after backup: $SCN" >> $BACKUPLOGFILE

if [ $Version_10g = 1 ]
then
 funct_create_restore_scripts_10g
else
 funct_create_restore_scripts_9i
fi

funct_init_backup
funct_control_backup
funct_final_audit_update

# Purge orphaned TEST backups
if [ $TYPE = IMP ]
then
 rm -f $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*_init*.ora" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*_init*.ora" -mtime +21 -exec rm {} \;
 find $BACKUP_LOC -name "*RESTORE_INFO*.TXT" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*RESTORE_INFO*.TXT" -mtime +21 -exec rm {} \;
 find $BACKUP_LOC -name "*LOG.TXT" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*LOG.TXT" -mtime +21 -exec rm {} \;
 find $BACKUP_LOC -name "*CONTROL_FILE.sql" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*CONTROL_FILE.sql" -mtime +21 -exec rm {} \;
 find $BACKUP_LOC -name "*Restore*.rcv" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*Restore*.rcv" -mtime +21 -exec rm {} \;
 find $BACKUP_LOC -name "*_orapw*" -mtime +21 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC -name "*_orapw*" -mtime +21 -exec rm {} \;
fi

# Purge orphaned PROD Text files
if  [ $TYPE = PROD ]
then
 rm -f $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*_init*.ora" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*_init*.ora" -mtime +38 -exec rm {} \;
 find $BACKUP_LOC  -name "*RESTORE_INFO*.TXT" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*RESTORE_INFO*.TXT" -mtime +38 -exec rm {} \;
 find $BACKUP_LOC  -name "*LOG.TXT" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*LOG.TXT" -mtime +38 -exec rm {} \;
 find $BACKUP_LOC  -name "*CONTROL_FILE.sql" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*CONTROL_FILE.sql" -mtime +38 -exec rm {} \;
 find $BACKUP_LOC  -name "*Restore*.rcv" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*Restore*.rcv" -mtime +38 -exec rm {} \;
 find $BACKUP_LOC  -name "*_orapw*" -mtime +38 >> $TEXT_FILE_DELETES
 find $BACKUP_LOC  -name "*_orapw*" -mtime +38 -exec rm {} \;
fi

######## END MAIN  #########################
exit 0
