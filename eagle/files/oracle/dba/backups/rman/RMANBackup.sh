#!/bin/ksh -x
# ******************************************************************************************** 
# NAME:		RMANBackup.sh
# 
# AUTHOR: 	Maureen Buotte                 
#
# PURPOSE: 	This utility will perform a RMAN backup of the specified database
#
# USAGE:	RMANBackup.sh SID [INCREMENTAL] [LEVEL]
#
# INPUT PARAMETERS:
#		SID	Oracle SID of database to backup
#
# Revisions
# ----------
# Nirmal Arri           11-Aug-2015     Changed NUM_CHANNELS=8 and added the IMPACT and URGENCY. Line #1569 and #1620 
# Frank Davis           16-May-2015     Made compatible with ASM Managed Databases
# Poornima Krishnan &    
# David Amico           06-May-2015     Backing up ASM Metadata, Change permissions on directory to GRID user. RestoreDataTemp file format change for ASM database
# Frank Davis           22-Oct-2014     Do not do force a Level 0 backup if database cannot reach RMAN catalog(NOCATALOG).
# Frank Davis           08-Sep-2014     Divided RMAN run block into sections.
# Frank Davis           08-Sep-2014     Do not force a full backup if the previous two days backup does not show success.
# Frank Davis           05-Sep-2014     Added code to prevent script from running if it is currently running.
# Frank Davis           05-Sep-2014     Replaced "find" command for determining the backup directory with an "ls" command.
# Frank Davis           04-Sep-2014     Fixed RUN_TIME value of DB_REFRESHES Table.
# Frank Davis           03-Sep-2014     Corrected IF Statements that used x${Variable}=x to [[ -z $Variable ]]
# Frank Davis           02-Oct-2013     Correct bug in script that would cause exit if script was running for first time
# Frank Davis           01-Oct-2013     Prevent the script from running if it is already being run the same SID
# Frank Davis           30-Sep-2013     Remove team_dba@eagleaccess.com from email
# Frank Davis           24-Sep-2013     Remove tech_support@eagleaccess.com from email
# Frank Davis           09-Sep-2013     Changed SendNotification Function to correct error 
# Frank Davis           12-Aug-2013     Fixed MAILTO Address List to include where Service Now was missing
# Frank Davis           19-Jun-2013     Removed unnecessary code that removes orphaned files in IMP
# Frank Davis           17-Jun-2013     Made script compatible with Solaris and Linux
# Frank Davis           13-Jun-2013     Made script compatible with Service Now
# Frank Davis           25-Jul-2012     Changed orphan backup removal from 15 days to 21 days due to incremental backup
# Frank Davis           25-Jul-2012     Changed manual purging of backup files from 14/31 days to 21/38 days due to incremental backup
# Frank Davis           16-Apr-2012     Updated IPCheck Variable to remove Newton DR VLAN and remove 10.70.13X VLAN
# Frank Davis           08-Feb-2012     Removed notification that database was not registered in RMAN catalog but was successfully registered.
# Frank Davis           26-Jan-2012     Added CHECK LOGICAL after BACKUP Command.  Now checks for logical corruptions during backup.
# Frank Davis           01-Dec-2011     Removed 10.62.5. VLAN from Production Pittsburgh IPCheck.  This was stated in error.
# Frank Davis           29-Nov-2011     Added 10.62.5. VLAN to Production Pittsburgh IPCheck.
# Frank Davis           11-Oct-2011     Replaced DELETE NOPROMPT OBSOLETE with DELETE NOPROMPT FORCE OBSOLETE.  For Error: RMAN-06207
# Frank Davis           13-Sep-2011     RMAN Standard Output changed from "input archive log thread=" in Oracle 10g to "input archived log thread=" in Oracle 11g.  Made script compatible with both.
# Frank Davis           30-Jun-2011     Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis           24-Jun-2011     Updated IPCheck Variable to included current list of IP Address VLANs
# Frank Davis           10-Jun-2011     Moved the RESTORE_INFO Log file name creation to after the funct_check_registered function so if the BACKUP_TYPE changes due to not registered
#                                       and a Level 0 backup is done instead of a Level 1, the RESTORE_INFO file name reflects the correct level of backup.
# Frank Davis           31-May-2011     Removed DELETE ALL INPUT and added back 1.5 days delete of archive logs.  This is due to creating standby from backup cannot find archive logs.
# Frank Davis           17-May-2011     Added 10.70.32. VLAN to Production Pittsburgh IPCheck.
# Frank Davis           13-May-2011     Removed deleting of archive log files older than 1.5 days as  makes this unnecessary.
# Frank Davis           12-May-2011     Made an exception for RMAN-08137 Warning.  This is not reported and will not effect SUCCESS Status of backup. 
# Frank Davis           12-May-2011     Removed exit in process_errors.  This prevents the rest of the backup from completing important tasks.
# Frank Davis           11-May-2011     Changed sqlplus output format for online redo logs to prevent wrapping of line in middle of command.
# Frank Davis           11-May-2011     Added NOT BACKED UP 1 TIMES DELETE ALL INPUT to prevent RMAN error and to not back up archive logs multiple times.
# Frank Davis           11-May-2011     Fixed logic that determines if Level 0 backup needs to be run when there is a failure of the previous backup.
# Frank Davis           11-Feb-2011     Changed to restore by log sequence number rather than SCN.  Log sequence number is determined to
#                                       be the most recent archive log backed up in the current backup set. When using SCN as the script was using
#                                       the SCN was from after the archive logs in the current backup set.  This caused the restore from the
#                                       monthly or yearly folder to fail when using that SCN.
# Frank Davis           10-Feb-2011     Change *RESTORE_INFO.LOG to *RESTORE_INFO*.LOG to allow for incremental backup log file pattern.
# Frank Davis           07-Feb-2011     Changed Prod Backups to go to /datadomain/evt/prod1-prod5
# Frank Davis           20-Jan-2011     Changed IMP Backups to go to /datadomain/evt/imp1-imp5
# Frank Davis           16-Nov-2010     Added Incremental Backups
# Frank Davis           05-Nov-2010     Added Pittsburgh Backups
# Maureen Buotte        27-Sep-2010     Fixed orapwd to be orapw
# Ritesh Bose           12-Apr-2010     Modified to include Cummulative Incremental backup in IMP/ Test regions
# Nishigandha Sathe     02-Mar-2010     Modified to add new primary key machines(instance)
# Maureen Buotte        20-Jul-2009     Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte        09-Sep-2009     Change to ensure PROD TEXT files are kept for 31 days
# Maureen Buotte        09-Sep-2009     Add purging of text files and orphaned TEST backups (due to restores)
# Maureen Buotte        02-Sep-2009     Changed retention periods to be 14 days for NON PROD and 31 days for PROD
# Maureen Buotte        10-Aug-2009     Changed Restore Scripts to use SCN at end of backup not SCN from before backup
# Maureen Buotte        06 Aug-2009     Added "CONFIGURE CONTROLFILE AUTOBACKUP OFF" to restore scripts
# Maureen Buotte        16-Jun-2009     Create restore scripts (different versions for 9i and 10g)
# Maureen Buotte        15-Jun-2009     Changed to just use 2 Channels for lowest impact to start, turned on controlfile autobackup
# Maureen Buotte        01-Jun-2009     Changed to create 1 channel per CPU
# Maureen Buotte        25-May-2009     Made RMAN script dynamic and created 1 channel per datafile
# Maureen Buotte        20-May-2009     Added validation of catalog and making sure database is registered
# Maureen Buotte        14-Apr-2009     Added reporting to Database of Databases
# 
# *********************************************************************************************
#alter table inf_monitor.backup_runs add (backup_type char(1) CHECK (backup_type in('F','0','1')));
#update inf_monitor.backup_runs set backup_type='F';
#
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications 
# -----------------------------------------------------------------------------
function SendNotification 
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat
 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1

 return 0
}

# --------------------------------------------------------------
# funct_db_online_verify(): Verify that database is not shut down
# --------------------------------------------------------------
funct_db_online_verify()
{
 ps -ef | grep ora_pmon_$ORACLE_SID | grep -v grep > /dev/null
 if [ $? -ne 0 ]
 then
  print "Database $ORACLE_SID is not running.  Cannot perform RMAN Backup"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Database $ORACLE_SID is not running. $PROGRAM_NAME cannot run "
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 17
 fi
} 

# --------------------------------------------------------------------------------------------------------------
# funct_chk_bkup_dir(): Make sure Datadomain is mounted and create backup directory if it doesn't  exist
# --------------------------------------------------------------------------------------------------------------
funct_chk_bkup_dir()
{
 $MOUNT | grep $BACKUP_MOUNT > /dev/null
 if [ $? -ne 0 ]
 then
  export MAILTO='eaglecsi@eagleaccess.com'
  export IMPACT=2
  export URGENCY=3
  SendNotification "$PROGRAM_NAME can not run because datadomain is not mounted"
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 18
 fi

 mkdir -p $BACKUP_LOC
 chmod g+w $BACKUP_LOC

 if [ $? -ne 0 ]
 then
  export IMPACT=2
  export URGENCY=3
  SendNotification "$PROGRAM_NAME failed for $ORACLE_SID because program could not create directory $BACKUP_LOC"
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 19
 fi
}

# ------------------------------------------
# funct_logmode_check(): Check DB log mode               
# ------------------------------------------
funct_logmode_check()
{
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select log_mode from v\\$database;
EOF
`

 if [ ! $STATUS = "ARCHIVELOG" ]
 then
  print "$PROGRAM_NAME required database to be in ARCHIVELOG mode -- $ORACLE_SID can not be backed up"  >>$BACKUPLOGFILE
  export IMPACT=2
  export URGENCY=3
  SendNotification "Database is not in ARCHIVELOG mode -- $ORACLE_SID can not be backed up"
  export PROCESS_STATUS=FAILURE
  funct_final_audit_update
  exit 20
 fi
}

# -------------------------------------------------------------------
# funct_catalog_check(): Verify ability to connect to RMAN Catalog
# -------------------------------------------------------------------
funct_catalog_check()
{
 export NO_CATALOG=0
 CATALOG_CONNECT=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 CATALOG_CONNECT=$(print $CATALOG_CONNECT|tr -d " ")
 if [ $CATALOG_CONNECT != '1' ]
 then
  export NO_CATALOG=1
  export IMPACT=2
  export URGENCY=3
  print "Unable to connect to RMAN catalog using $CATALOG_ID/xxx@$CATALOG_DB however rman backup is continuing"
  SendNotification "Unable to connect to RMAN catalog using $CATALOG_ID/xxx@$CATALOG_DB for $ORACLE_SID however rman backup is continuing\n$CATALOG_CONNECT"
  export PROCESS_STATUS=WARNING
 fi
}

# -------------------------------------------------------------------------------------------------------------
# funct_check_registered(): Verify the database is registered in the RMAN catalog and, if not, register it
# -------------------------------------------------------------------------------------------------------------
funct_check_registered()
{
 DBID=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select dbid from v\\$database;
EOF
`
 DBID=$(print $DBID|tr -d " ")

 REGISTERED=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
 set heading off feedback off
 select count(*) from rc_database where dbid=${DBID} and upper(name)=upper('${ORACLE_SID}');
EOF
`
 REGISTERED=$(print $REGISTERED|tr -d " ")

 if [ $REGISTERED != '1' ]
 then
  STATUS=`${ORACLE_HOME}/bin/rman <<EOF
  connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB
  connect target $TRGT_DB
  register database;
  $RETENTION_PERIOD
  configure controlfile autobackup ON;
  configure controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl';
EOF
`
  REGISTERED=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<EOF
  set heading off feedback off
  select count(*) from rc_database where dbid=${DBID};
EOF
`
  if [ $BACKUP_TYPE='INCREMENTAL' ]
  then
   export BACKUP_TYPE=0
  fi
  REGISTERED=$(print $REGISTERED|tr -d " ")
  if [ $REGISTERED != '1' ]
  then
   export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID was not registered in the RMAN catalog $CATALOG_DB and an attempt to register the database failed.  The RMAN backup will continue but this needs to be resolved"
   export PROCESS_STATUS=WARNING
  else
   export PROCESS_STATUS=SUCCESS
  fi
 fi
}

# --------------------------------------------
# funct_crosscheck_archive_logs()
# --------------------------------------------
funct_crosscheck_archive_logs()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   CROSSCHECK ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_delete_expired_archive_logs()
# --------------------------------------------
funct_delete_expired_archive_logs()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_check_asm_client
# --------------------------------------------
funct_check_asm_client()
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

# --------------------------------------------
# funct_backup_asm_metadata
# --------------------------------------------
funct_backup_asm_metadata()
{
 #ssh -q grid@$BOX $WORKING_DIR/asm_metadata_backup.ksh $BACKUP_LOC/asm_metadata_$BACKUP_TAG >>${BACKUPLOGFILE}
 sudo -iu grid asmcmd md_backup $BACKUP_LOC/asm_metadata_$BACKUP_TAG >>${BACKUPLOGFILE}
 if [ $? -ne 0 ]
 then
  return 1
 fi
}

# --------------------------------------------
# funct_backup_full():   Backup Full non-incremental
# --------------------------------------------
funct_backup_full()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done

 print "   BACKUP $CHECK_LOGICAL TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_full_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1 ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_backup_level_0():   Backup Level 0 Incremental Backup
# --------------------------------------------
funct_backup_level_0()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done

 print "   BACKUP $CHECK_LOGICAL INCREMENTAL LEVEL 0 TAG \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_level_0_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_backup_level_1():   Backup Level 1 Incremental Backup
# --------------------------------------------
funct_backup_level_1()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done
 print "   BACKUP $CHECK_LOGICAL INCREMENTAL LEVEL 1 CUMULATIVE TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_level_1_dbf' " >> $BACKUP_SCRIPT
 print "   DATABASE FILESPERSET 1;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_switch_logfile():   Switch a logfile in the database
# --------------------------------------------
funct_switch_logfile()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   sql \"ALTER SYSTEM SWITCH LOGFILE\";" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_backup_archivelogs(): Backup archivelogs that have not been backed up yet.
# --------------------------------------------
funct_backup_archivelogs()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done
 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "   FORMAT '$BACKUP_LOC/al_${BACKUP_TAG}_%U_level_${BACKUP_TYPE}_archive'" >> $BACKUP_SCRIPT
 print "   ARCHIVELOG ALL NOT BACKED UP 1 TIMES ;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_backup_controlfile():
# --------------------------------------------
funct_backup_controlfile()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
 print "  FORMAT '$BACKUP_LOC/cf_${BACKUP_TAG}_%U_full_ctrl'" >> $BACKUP_SCRIPT
 print "   CURRENT CONTROLFILE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_report_schema():
# --------------------------------------------
funct_report_schema()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   REPORT SCHEMA;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_crosscheck_backup_9i():
# --------------------------------------------
funct_crosscheck_backup_9i()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "   ALLOCATE CHANNEL FOR MAINTENANCE TYPE DISK;" >> $BACKUP_SCRIPT
 print "   CROSSCHECK BACKUP;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT
 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_crosscheck_backup():
# --------------------------------------------
funct_crosscheck_backup()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done
 print "   CROSSCHECK BACKUP;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_delete_obsolete():
# --------------------------------------------
funct_delete_obsolete()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   DELETE NOPROMPT FORCE OBSOLETE;" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_delete_archivelogs():
# --------------------------------------------
funct_delete_archivelogs()
{
 if [ $NO_CATALOG = '1' ]
 then
  print "connect target $TRGT_DB" > $BACKUP_SCRIPT
 else
  print "connect catalog $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB" > $BACKUP_SCRIPT
  print "connect target $TRGT_DB" >> $BACKUP_SCRIPT
 fi

 print "run {" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT
 typeset -i i=1
 while [ ${i} -le ${NUM_CHANNELS} ]
 do
  print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
  i=${i}+1;
 done
 print "   DELETE NOPROMPT ARCHIVELOG ALL COMPLETED BEFORE 'SYSDATE-1.5';" >> $BACKUP_SCRIPT
 print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
 typeset -i j=1
 while [ ${j} -le ${NUM_CHANNELS} ]
 do
  print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
  j=${j}+1;
 done
 print "}" >> $BACKUP_SCRIPT
 print "quit" >> $BACKUP_SCRIPT

 nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF
process_errors
}

# --------------------------------------------
# funct_pre_backup():   Pre backup steps common to all backup types
# --------------------------------------------
funct_pre_backup()
{
 # Touch a file to be used later to determine which files are part of this backup set
 touch $START_FILE

 funct_crosscheck_archive_logs
 funct_delete_expired_archive_logs
}

# --------------------------------------------
# funct_post_backup():   Post backup steps common to all backup types
# --------------------------------------------
funct_post_backup()
{
 funct_switch_logfile
 ((delete_archive_logs = 0))
 funct_backup_archivelogs
 if [ $error_flag -eq 1 ]
 then
  ((delete_archive_logs = 1))
 fi
 funct_backup_controlfile
 funct_report_schema
 if [ "$BACKUP_TYPE" != 1 ]
 then
  if [ "$Version_10g" -eq 1 ]
  then
   funct_crosscheck_backup
  else
   funct_crosscheck_backup_9i
  fi
  funct_delete_obsolete
 fi
 if [ $delete_archive_logs -eq 0 ]
 then
  funct_delete_archivelogs
 fi
}

# --------------------------------------------
# funct_rman_backup():   Backup database in its entirety non-incremental
# --------------------------------------------
funct_rman_backup()
{
 funct_pre_backup
 funct_backup_full
 funct_post_backup
}

# --------------------------------------------
# funct_rman_level_0_backup():   Backup Level 0 Incremental Backup
# --------------------------------------------
funct_rman_level_0_backup()
{
 funct_pre_backup
 funct_backup_level_0
 funct_post_backup
}

# --------------------------------------------
# funct_rman_level_1_backup():   Backup Level 1 Incremental Backup
# --------------------------------------------
funct_rman_level_1_backup()
{
 funct_pre_backup
 funct_backup_level_1
 funct_post_backup
}

# ------------------------------------------------------
# funct_control_backup():  Backup control file to trace
# -------------------------------------------------------
funct_control_backup()
{
 CONTROLFILE_NAME=$BACKUP_LOC/${BACKUP_TAG}_CONTROL_FILE.sql;
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 alter database backup controlfile to trace as '${CONTROLFILE_NAME}';
EOF
`
}

# ---------------------------------------------------------------
# funct_create_restore_log(): Create log with backup information 
# ---------------------------------------------------------------
funct_create_restore_log()
{
 # Get the list of datafiles
 print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
 print  "DATAFILES: " >> ${RESTOREFILE}
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a ${RESTOREFILE}
 set heading off feedback off pagesize 500
 select 'DATAFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M'
 from V\$DATAFILE;
EOF

 # Get the number of datafiles
 NUM_DATA_FILES=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF 
 set heading off feedback off
 select count(*) from V\\$DATAFILE;
EOF
`
 export NUM_DATA_FILES

 # Get the list of Temporary Files
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 print  "Temporary Files: " >> $RESTOREFILE
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a $RESTOREFILE
 set heading off feedback off
 select 'TEMPFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M'
 from V\$TEMPFILE;
GOF

 # Get the list of redo logs
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 print  "REDO LOGS: " >> $RESTOREFILE
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTOREFILE
 set heading off feedback off pagesize 500
 select 'LOGFILE | '||a.group#||' | '||member||' | '||a.bytes/1024/1024||'M' from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
BOF


 # Get SCN 
 print  "\n ------------------------------------------------------------------------------\n " >> $RESTOREFILE
 SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select max(next_change#) from V\\$LOG_HISTORY;
EOF
`
 SCN=$(print $SCN|tr -d " ")
 export SCN

 print "DBID:  ${DBID}" | tee -a $RESTOREFILE
 print "SCN before backup for information purpose only [max(next_change#)]:  ${SCN}" | tee -a $RESTOREFILE
 print "This SCN is not required to restore.  The SCN after the backup will be in the restore scripts." | tee -a $RESTOREFILE
 print "BACKUP_TAG:  ${BACKUP_TAG}" | tee -a $RESTOREFILE
 print "BACKUPDIR:  $BACKUP_LOC" | tee -a $RESTOREFILE
}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_9i(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_9i()
{
 BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

 # Create the controlfile restore script
 print "set dbid $DBID " >> $RESTORE_CONTROLFILE_9i
 print " " >> $RESTORE_CONTROLFILE_9i
 print "run {  " >> $RESTORE_CONTROLFILE_9i
 print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl'; " >> $RESTORE_CONTROLFILE_9i
 print "	restore controlfile from autobackup maxdays 7; " >> $RESTORE_CONTROLFILE_9i
 print "	alter database mount; " >> $RESTORE_CONTROLFILE_9i
 print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> $RESTORE_CONTROLFILE_9i
 print "}  " >> $RESTORE_CONTROLFILE_9i

 # Create the Data files restore script
 print "run {  " >> $RESTORE_DATA_FILES_9i
 print "	allocate channel 'RCHL1' device type disk;  " >> $RESTORE_DATA_FILES_9i
 print "	allocate channel 'RCHL2' device type disk;  " >> $RESTORE_DATA_FILES_9i

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a $RESTORE_DATA_FILES_9i
 set heading off feedback off pagesize 500
 select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$DATAFILE;
EOF

 print "" >> $RESTORE_DATA_FILES_9i
 print "$restore_by_log_sequence" >> $RESTORE_DATA_FILES_9i
 print "restore database;  " >> $RESTORE_DATA_FILES_9i
 print "switch datafile all;  " >> $RESTORE_DATA_FILES_9i
 print "recover database;  " >> $RESTORE_DATA_FILES_9i
 print "}  " >> $RESTORE_DATA_FILES_9i

 # Create the TEMP files restore script
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF | tee -a $RESTORE_TEMP_FILES_9i
 set heading off feedback off
 select 'Alter tablespace  ' || t.name || ' add TEMPFILE ''' || (replace (p.name,'$ORACLE_SID','xxxxxx')) || ''' size ' || bytes/1024/1024 || 'M;'
 from V\$TEMPFILE p, v\$TABLESPACE t where p.ts#=t.ts#;
GOF

 # Create the Redo files script file
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTORE_REDO_9i
 set heading off feedback off pagesize 500 trimspool on linesize 200
 select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
BOF

}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_10g(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_10g()
{
 BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

 # Create the controlfile restore script
 print "set dbid $DBID " >> $RESTORE_CONTROLFILE_10g
 print " " >> $RESTORE_CONTROLFILE_10g
 print "run {  " >> $RESTORE_CONTROLFILE_10g
 print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl';  "  >> $RESTORE_CONTROLFILE_10g
 print "	restore controlfile from autobackup maxdays 7; " >> $RESTORE_CONTROLFILE_10g
 print "	alter database mount; " >> $RESTORE_CONTROLFILE_10g
 print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> $RESTORE_CONTROLFILE_10g
 print "}  " >> $RESTORE_CONTROLFILE_10g

 # Create the Data/Temp files restore script
 print "run {  " >> $RESTORE_DATA_TEMP_FILES_10g

 if [[ $ASM_CLIENT = Y ]]
 then

 print "        allocate channel 'RCHL1' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL2' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL3' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL4' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL5' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL6' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL7' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL8' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off pagesize 500
 select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || substr(name,0,instr(name,'/')-1)||''';'  
 from V\$DATAFILE;       
EOF

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off
 select 'SET NEWNAME FOR TEMPFILE ' ||file#|| ' TO ''' ||substr(name,0,instr(name,'/')-1)||''';'  
 from V\$TEMPFILE;
GOF

 else

 print "        allocate channel 'RCHL1' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "        allocate channel 'RCHL2' device type disk;  " >> $RESTORE_DATA_TEMP_FILES_10g

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off pagesize 500
 select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$DATAFILE;
EOF

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a $RESTORE_DATA_TEMP_FILES_10g
 set heading off feedback off
 select 'SET NEWNAME FOR TEMPFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$TEMPFILE;
GOF

 fi

 print "" >> $RESTORE_DATA_TEMP_FILES_10g
 print "$restore_by_log_sequence" >> $RESTORE_DATA_TEMP_FILES_10g
 print "restore database;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "switch datafile all;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "switch tempfile all;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "sql 'alter database disable block change tracking';  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "recover database;  " >> $RESTORE_DATA_TEMP_FILES_10g
 print "}  " >> $RESTORE_DATA_TEMP_FILES_10g

 # Create the Redo files script file
 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $RESTORE_REDO_10g
 set heading off feedback off pagesize 500 trimspool on linesize 200
 select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
 from V\$LOGFILE b , V\$LOG a
 where a.GROUP#=b.GROUP#;
BOF

}

# ---------------------------------------------------
# funct_init_backup():  Backup initxxxxxx.ora file
# ---------------------------------------------------
funct_init_backup()
{
 #Copy current init<SID>.ora file to backup directory
 print  " Copying current init${ORACLE_SID}.ora file from spfile"  >> $BACKUPLOGFILE

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a $BACKUPLOGFILE
 set heading off feedback off
 create pfile='${BACKUP_LOC}/${BACKUP_TAG}_init${ORACLE_SID}.ora' from SPFILE;
BOF
 if [ $? -ne 0 ]
 then
  print "Error Copying the init.ora file to the backup location as init${ORACLE_SID}_${BACKUP_TAG}.ora" >> $BACKUPLOGFILE
 fi

 cp ${ORACLE_HOME}/dbs/orapw${ORACLE_SID} ${BACKUP_LOC}/${BACKUP_TAG}_orapw${ORACLE_SID}
}

# ----------------------------------------------------------------------------------------
# process_errors():   PROCESS errors that occurred in backup and in the validation process
# ----------------------------------------------------------------------------------------
process_errors()
{
 export error_flag=0
 TMP_ERR_FILE=$WORKING_DIR/captureerr_$BACKUP_TAG
 rm -rf $TMP_ERR_FILE
 cat $BACKUPLOGFILE | egrep -i 'failure|ORA-|corruption|rman-|RMAN-'|grep -v RMAN-08137|tee $TMP_ERR_FILE
 if [ $? -eq 0 ]
 then
  if [[ -s $TMP_ERR_FILE ]]
  then
   export error_flag=1
   error_msg=$(cat $TMP_ERR_FILE)
   if [ $TYPE = PROD ]
   then
    export IMPACT=1
    export URGENCY=1
   else
    export IMPACT=2
    export URGENCY=3
   fi
   SendNotification "${PROGRAM_NAME} Failed for ${ORACLE_SID} -> See ${BACKUPLOGFILE} for errors::  $error_msg"
   print  "$PROGRAM_NAME Failed for $ORACLE_SID -> See $BACKUPLOGFILE for errors::  $error_msg " 
   export PROCESS_STATUS=FAILURE
  fi
 fi
	
 rm -rf $TMP_ERR_FILE
}
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS != 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
EOF
`
 DB_INSTANCE=$(print $DB_INSTANCE |tr -d " ")
 export DB_INSTANCE
 if [[ -z $DB_INSTANCE ]]
 then
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID on $BOX does not exist in $CRON_SID -> $PROGRAM_NAME will run for $ORACLE_SID but $CRON_SID will not be updated"
  PERFORM_CRON_STATUS=0
  return 1	
 fi

 DB_DBID=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select dbid from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
EOF
`
 DB_DBID=$(print $DB_DBID |tr -d " ")
 export DB_DBID

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from BACKUP_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ -z $Sequence_Number ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000
  set heading off feedback off
  declare i number;
  begin
   insert into BACKUP_RUNS values (backup_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','${BACKUP_LOC}','${DB_DBID}','${BACKUP_TAG}','','0','${BACKUP_TYPE}') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update BACKUP_RUNS set start_time=sysdate, dbid='${DB_DBID}', backup_tag='${BACKUP_TAG}' where instance=${Sequence_Number};
  commit;
EOF
`

 fi
 export Sequence_Number
 return 0
}

# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update 
# ---------------------------------------------------------------------------------------------
funct_final_audit_update ()
{
bsize_date=`date +%m-%d-%Y`
 # Update DBA Auditing Database with end time and backup size
 if [ $PERFORM_CRON_STATUS = 1 ]
 then
  #typeset -i BACKUP_SIZE=$(find $BACKUP_LOC -newer $START_FILE -type f -exec du -sk {} \; | awk  '{print $1}' |  awk '{sum+=$1} END {print sum}')

if [ $NO_CATALOG = '1' ]
 then
typeset -i BACKUP_SIZE=-1
else
typeset -i BACKUP_SIZE=`${ORACLE_HOME}/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB <<!
set heading off feedback off

select sum(b_size) from
 (select (sum(r.blocks*r.block_size)/1024/1024) b_size from rman.rc_backup_datafile r, rman.rc_rman_backup_job_details j
where upper(r.db_name)=upper('${ORACLE_SID}')
and to_char(j.START_TIME, 'mm-dd-yyyy')='${bsize_date}' and to_char(j.end_TIME, 'mm-dd-yyyy') = to_char(r.COMPLETION_TIME, 'mm-dd-yyyy')
and r.db_key=j.db_key and j.INPUT_TYPE='DB INCR'
 union
  select (sum(r.blocks*r.block_size)/1024/1024) b_size from rman.rc_archived_log r, rman.rc_rman_backup_job_details j
where upper(r.db_name)=upper('${ORACLE_SID}')
and to_char(j.START_TIME, 'mm-dd-yyyy')='${bsize_date}' and to_char(j.end_TIME, 'mm-dd-yyyy') = to_char(r.COMPLETION_TIME, 'mm-dd-yyyy')
and r.db_key=j.db_key and j.INPUT_TYPE='ARCHIVELOG'
 union
 select (sum(r.blocks*r.block_size)/1024/1024) b_size from rman.rc_backup_controlfile r, rman.rc_rman_backup_job_details j
where upper(r.db_name)=upper('${ORACLE_SID}')
and to_char(j.START_TIME, 'mm-dd-yyyy')='${bsize_date}' and to_char(j.end_TIME, 'mm-dd-yyyy') = to_char(r.COMPLETION_TIME, 'mm-dd-yyyy')
and r.db_key=j.db_key and j.INPUT_TYPE='CONTROLFILE'
union
select (sum(r.blocks*r.block_size)/1024/1024) b_size from rman.rc_backup_redolog r, rman.rc_rman_backup_job_details j
where upper(r.db_name)=upper('${ORACLE_SID}')
and to_char(j.START_TIME, 'mm-dd-yyyy')='${bsize_date}' and to_char(j.end_TIME, 'mm-dd-yyyy') = to_char(r.COMPLETION_TIME, 'mm-dd-yyyy')
and r.db_key=j.db_key and j.INPUT_TYPE='CONTROLFILE'
 );

!`
fi


  ${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  set heading off feedback off
  update BACKUP_RUNS set
  end_time=sysdate,backup_size=${BACKUP_SIZE},status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1 where instance=${Sequence_Number};
EOF
  the_run_time=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  select 
  to_char(trunc((end_time-start_time)*24),'FM00')||
  ':'||
  to_char(trunc(60*substr(((end_time-start_time)*24),instr((to_char(end_time-start_time)*24),'.'),5)),'FM00')||
  ':'||
  case when to_char(to_number(substr(to_char((end_time-start_time)*24*60),instr(to_char((end_time-start_time)*24*60),'.'))*60),'FM00') = '###' THEN '00'
  else
  to_char(to_number(substr(to_char((end_time-start_time)*24*60),instr(to_char((end_time-start_time)*24*60),'.'))*60),'FM00')
  end
  from backup_runs where instance=${Sequence_Number};
EOF
`
  the_run_time=$(print $the_run_time |tr -d " ")

  ${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  update BACKUP_RUNS set run_time='${the_run_time}',backup_type='$BACKUP_TYPE' where instance=${Sequence_Number};
EOF
 fi

 rm -f $START_FILE
 print  "${ORACLE_SID}, RMAN Backup Completed $(date +\"%c\") with a status of $PROCESS_STATUS " |tee -a $BACKUPLOGFILE
 print "$PROCESS_STATUS" > $BACKUP_STATUS
}
############################################################
#                       MAIN                               
############################################################
HOSTNAME=$(hostname)
ostype=$(uname)

if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 AWK=awk
 MOUNT=/bin/mount
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 AWK=nawk
 MOUNT=/etc/mount
fi
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export ORACLE_SID=$1

#IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.|10\.70\.7.\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
else
 export TYPE=IMP
fi
export MAILTO='eaglecsi@eagleaccess.com'
if [ $# -lt 1 ] || [ $# -gt 3 ]
then
 print "\n$0 Failed: Incorrect number of arguments -> $0 ORACLE_SID [INCREMENTAL] [LEVEL]"
 print "The second parameter INCREMENTAL is optional.  If not specified, backup is FULL.\n"
 print "If specified, the only valid value for the second parameter is INCREMENTAL.\n"
 print "The third parameter LEVEL of incremental backup is optional. If specified, must be 0 or 1\n"
 print "If third parameter LEVEL is not specified and second parameter INCREMENTAL is specified,"
 print "then the INCREMENTAL backup will be a Level 0 backup.\n"
 export IMPACT=2
 export URGENCY=3
 SendNotification "Incorrect number of arguments -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
 exit 1
fi

grep "^${ORACLE_SID}:" $ORATAB > /dev/null
if [ $? -ne 0 ]
then
 print "\nThe first parameter entered into script is not a valid Oracle SID in $ORATAB."
 print "Choose a valid Oracle SID from $ORATAB.\n"
 export IMPACT=2
 export URGENCY=3
 SendNotification "Not a valid Oracle SID -> $PROGRAM_NAME ORACLE_SID [INCREMENTAL] [LEVEL]"
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/backups/rman
else
 export WORKING_DIR="/u01/app/oracle/local/dba/backups/rman"
fi

export BACKUP_STATUS=$WORKING_DIR/${ORACLE_SID}_backup_status.txt

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

this_script=$(basename $0)
pid_for_this_script=$$
parent_pid=$(ps -ef | grep "$this_script" |grep '\/bin\/ksh' | grep "$ORACLE_SID" | grep -w "$pid_for_this_script" | grep -v grep | awk '{print $3}')
(( process_count=$(ps -ef | grep "$this_script" | grep '\/bin\/ksh' | grep "$ORACLE_SID" | grep -wv "$parent_pid" | grep -v grep |grep -v 'ps -ef' | wc -l) ))
if [ $process_count -gt 0 ]
then
 print "Backup is already running.  Aborting this backup attempt."
 exit 3
fi

BACKUP_TYPE=F
BACKUP_FORMAT=FULL
if [ $# -gt 1 ]
then
 BACKUP_TYPE=0
 BACKUP_FORMAT=$(print $2 | tr '[a-z]' '[A-Z]')
 if [ $BACKUP_FORMAT != INCREMENTAL ]
 then
  print "\nA second parameter was entered but only one value for the second parameter is allowed."
  print "The only allowed value for the second parameter is:  INCREMENTAL\n"
  print "Omit the second parameter to perform a FULL backup or enter INCREMENTAL for the second parameter to perform an incremental backup.\n"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Not a valid second parameter -> ${PROGRAM_NAME} ORACLE_SID [INCREMENTAL] [LEVEL]"
  exit 4
 fi
fi

if [ $# -eq 3 ]
then
 BACKUP_TYPE=$3
 if [ $BACKUP_TYPE != 0 ] && [ $BACKUP_TYPE != 1 ]
 then
  print "Level of Incremental backup must be 0 or 1"
  print "For example:  $0 INCREMENTAL 0"
  print "For example:  $0 INCREMENTAL 1"
  print "Or just omit the third parameter and it will default to 0"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Not a valid third parameter -> $PROGRAM_NAME ORACLE_SID [INCREMENTAL] [LEVEL]"
  exit 5
 fi
fi

export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export DB_INSTANCE 

export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt

export PROCESS_STATUS=SUCCESS
export Sequence_Number=0
export BACKUP_SIZE=0
export PERFORM_CRON_STATUS=0

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

INIFILE=$WORKING_DIR/RMANParam.ini
if [[ -f $INIFILE ]]
then
 export BACKUP_BASE=$(grep $ORACLE_SID $INIFILE | awk '{print $2}')
 if [[ -z $BACKUP_BASE ]]
 then
  export BACKUP_BASE=$(grep all $INIFILE | awk '{print $2}')
  if [[ -z $BACKUP_BASE ]]
  then
   print "$PROGRAM_NAME Failed for ${ORACLE_SID}: Backup Base Location not defined in ${INIFILE}"
   export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID - Backup Base Location not defined in ${INIFILE}"
   exit 6
  fi
 fi

 CATALOG_ID=$(grep CATALOG_ID $INIFILE | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_ID ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_ID not defined in $INIFILE"
  export IMPACT=2
   export URGENCY=3
   SendNotification "$ORACLE_SID - CATALOG_ID not defined in $INIFILE"
   exit 7
 fi

 CATALOG_PASSWD=$(grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_PASSWD ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_PASSWD not defined in $INIFILE"
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID - CATALOG_PASSWD not defined in $INIFILE"
  exit 8
 fi
 export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [[ -z $CATALOG_DB ]]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_DB not defined in $INIFILE"
  export IMPACT=2
  export URGENCY=3
  SendNotification "$ORACLE_SID - CATALOG_DB not defined in $INIFILE"
  exit 9
 fi
else
 print "$PROGRAM_NAME FAILED: $ORACLE_SID - $INIFILE file does not exist"
 export IMPACT=2
 export URGENCY=3
 SendNotification "$ORACLE_SID - $INIFILE file does not exist"
 exit 10
fi

export BACKUP_MOUNT=$(print $BACKUP_BASE | awk -F/ '{print $2}')

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.60\.99' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export MAILTO='eaglecsi@eagleaccess.com'
 export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 31 DAYS;'
 export BACKUP_SET=$(ls $BACKUP_BASE/evt/prod? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 if [[ -z $BACKUP_SET ]]
 then
  prod_most_free=$(du -sk $BACKUP_BASE/evt/prod? | sort -nk1 | head -1 | awk '{print $2}' | awk -F\/ '{print $NF}')
  BACKUP_LOC=$BACKUP_BASE/evt/$prod_most_free/$ORACLE_SID
  mkdir -p $BACKUP_LOC
  chmod g+w $BACKUP_LOC

  if [[ ! -d $BACKUP_LOC ]]
  then
   print "Cannot create directory $BACKUP_LOC"
   print "Will abort here as there is no place to put the backup files."
   exit 11
  fi
 else
  export BACKUP_LOC=$BACKUP_BASE/$BACKUP_SET/$ORACLE_SID
 fi
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.105\.|10\.60\.103\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=IMP
 export MAILTO='eaglecsi@eagleaccess.com'
 export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;'
 export BACKUP_SET=$(ls $BACKUP_BASE/evt/imp? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 if [[ -z $BACKUP_SET ]]
 then
  imp_most_free=$(du -sk $BACKUP_BASE/evt/imp? | sort -nk1 | head -1 | awk '{print $2}' | awk -F\/ '{print $NF}')
  BACKUP_LOC=$BACKUP_BASE/evt/$imp_most_free/$ORACLE_SID
  mkdir -p $BACKUP_LOC
  chmod g+w $BACKUP_LOC

  if [[ ! -d $BACKUP_LOC ]]
  then
   print "Cannot create directory $BACKUP_LOC"
   print "Will abort here as there is no place to put the backup files."
   exit 12
  fi
  else
   export BACKUP_LOC=$BACKUP_BASE/$BACKUP_SET/$ORACLE_SID
  fi
 fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.70\.4.\.|10\.70\.3.\.|10\.70\.7.\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export MAILTO='eaglecsi@eagleaccess.com'
 export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 31 DAYS;'
 export BACKUP_SET=$(ls $BACKUP_BASE/pgh/prod? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')
 if [[ -z $BACKUP_SET ]]
 then
  prod_most_free=$(du -sk $BACKUP_BASE/pgh/prod? | sort -nk1 | head -1 | awk '{print $2}' | awk -F\/ '{print $NF}')
  BACKUP_LOC=$BACKUP_BASE/pgh/$prod_most_free/$ORACLE_SID
  mkdir -p $BACKUP_LOC
  chmod g+w $BACKUP_LOC

  if [[ ! -d $BACKUP_LOC ]]
  then
   print "Cannot create directory $BACKUP_LOC"
   print "Will abort here as there is no place to put the backup files."
   exit 13
  fi
 else
  export BACKUP_LOC=$BACKUP_BASE/$BACKUP_SET/$ORACLE_SID
 fi
fi

IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.70\.14.\.|10\.70\.173\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=IMP
 export MAILTO='eaglecsi@eagleaccess.com'
 export RETENTION_PERIOD='CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 14 DAYS;'
 export BACKUP_SET=$(ls $BACKUP_BASE/pgh/imp? | sed '1!G;h;$!d' | sed "1,/$ORACLE_SID/d" | sed '/^$/,$d' | grep '\/datadomain\/' | sed "s/:$/\/$ORACLE_SID/g" | awk -F\/ '{print $3"/"$4}')

 if [[ -z $BACKUP_SET ]]
 then
  imp_most_free=$(du -sk $BACKUP_BASE/pgh/imp? | sort -nk1 | head -1 | awk '{print $2}' | awk -F\/ '{print $NF}')
  BACKUP_LOC=$BACKUP_BASE/pgh/$imp_most_free/$ORACLE_SID
  mkdir -p $BACKUP_LOC
  chmod g+w $BACKUP_LOC

  if [[ ! -d $BACKUP_LOC ]]
  then
   print "Cannot create directory $BACKUP_LOC"
   print "Will abort here as there is no place to put the backup files."
   exit 14
  fi
 else
  export BACKUP_LOC=$BACKUP_BASE/$BACKUP_SET/$ORACLE_SID
 fi
fi

funct_chk_bkup_dir

export BACKUP_TAG=${ORACLE_SID}_$(date +'%Y%m%d_%H%M%S')
export TRGT_DB="/"
export BACKUP_SCRIPT=$WORKING_DIR/${ORACLE_SID}_rman_backup_script.sql
export TEXT_FILE_DELETES=$WORKING_DIR/${ORACLE_SID}_TEXT_FILE_Deletes.txt

BACKUPLOGFILE="$BACKUP_LOC/${BACKUP_TAG}_LOG.TXT"

# 10g Restore script files
RESTORE_CONTROLFILE_10g="$BACKUP_LOC/Restore10gControlFile_${BACKUP_TAG}.rcv"
RESTORE_DATA_TEMP_FILES_10g="$BACKUP_LOC/Restore10gDataTempFiles_${BACKUP_TAG}.rcv"
RESTORE_REDO_10g="$BACKUP_LOC/Restore10gRedo_${BACKUP_TAG}.rcv"

# 9i Restore script files
RESTORE_CONTROLFILE_9i="$BACKUP_LOC/Restore9iControlFile_${BACKUP_TAG}.rcv"
RESTORE_DATA_FILES_9i="$BACKUP_LOC/Restore9iDataFiles_${BACKUP_TAG}.rcv"
RESTORE_TEMP_FILES_9i="$BACKUP_LOC/Restore9iTempFiles_${BACKUP_TAG}.rcv"
RESTORE_REDO_9i="${BACKUP_LOC}/Restore9iRedo_${BACKUP_TAG}.rcv"


if [[ -f $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE > $NO_COMMENT_PARFILE)
 LINE=$(grep  "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "DONT_RUN_ANYTHING:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "${PROGRAM_NAME}:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

# Make sure this database is not in list of SIDs that nothing should be run against in BigBrother.ini
EXCEPTION=$($AWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [ $EXCEPTION -ne 0 ]
then
 exit 15
fi

# Make sure this database is not in list of SIDs that this job should not be run for in BigBrother.ini
EXCEPTION=$($AWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [ $EXCEPTION != 0 ]
then
 exit 16
fi

ORAVER=$(print $ORACLE_HOME | grep 9.2)
if [[ -z $ORAVER ]]
then
 export Version_10g=1
 export Version_9i=0
 export CHECK_LOGICAL='CHECK LOGICAL'
else
 export Version_9i=1
 export Version_10g=0
 export CHECK_LOGICAL=''
fi

export START_FILE=$BACKUP_LOC/START_TIME.txt

print " Starting RMAN Backup of ....  $ORACLE_SID on $(date +\"%c\")" 

# Add entry into DBA Admin database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 funct_initial_audit_update
 export DB_INSTANCE
fi

funct_db_online_verify
funct_logmode_check

funct_catalog_check
if [ $NO_CATALOG -eq 0 ]
then
 funct_check_registered
fi

if [ $BACKUP_FORMAT = FULL ]
then
 RESTOREFILE="$BACKUP_LOC/${BACKUP_TAG}_RESTORE_INFO.TXT"
else
 RESTOREFILE="$BACKUP_LOC/${BACKUP_TAG}_RESTORE_INFO_level_${BACKUP_TYPE}.TXT"
fi

funct_create_restore_log

print "${ORACLE_SID}, RMAN Backup Started on $(date +\"%c\")" |tee -a $BACKUPLOGFILE
export NUM_CHANNELS=8

if  [ $BACKUP_FORMAT = FULL ]
then
 funct_rman_backup
else
 if [ $BACKUP_TYPE -eq 0 ]
 then
  funct_rman_level_0_backup
 else
  funct_rman_level_1_backup
 fi
fi

# for PROD databases, create a standby control file
if  [ $TYPE = PROD ]
then
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 alter database create standby controlfile as  '${BACKUP_LOC}/standbycontrol_${BACKUP_TAG}.ctl';
EOF
`
fi

newest_archive_log_in_backup=$(grep 'input archive.* log thread=[0-9] sequence=' $BACKUPLOGFILE | awk '{print $5}' | awk -F= '{print $NF}' | sort -n | tail -1)
export restore_by_log_sequence="set until sequence $newest_archive_log_in_backup thread 1;"

# Get the SCN after the backup
SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
set heading off feedback off
select max(next_change#) from V\\$LOG_HISTORY;
EOF
`
SCN=$(print $SCN|tr -d " ")
print "SCN retrieval after backup: $SCN" >> $BACKUPLOGFILE

if [ $Version_10g = 1 ]
then
 funct_create_restore_scripts_10g
else
 funct_create_restore_scripts_9i
fi

funct_init_backup
funct_control_backup

if [ $ASM_CLIENT = Y ]
then 
 funct_backup_asm_metadata
 if [ $? -ne 0 ]
 then
  print "There was an error when attempting to back up the +ASM Metadata."
  export IMPACT=2
  export URGENCY=3
  SendNotification "Database $ORACLE_SID cannot back up the ASM Metadata. "
 fi
fi 

funct_final_audit_update

# Purge orphaned TEST backups
#if [ $TYPE = IMP ]
#then
# rm -f $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*_init*.ora" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*_init*.ora" -mtime +21 -exec rm {} \;
# find $BACKUP_LOC -name "*RESTORE_INFO*.TXT" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*RESTORE_INFO*.TXT" -mtime +21 -exec rm {} \;
# find $BACKUP_LOC -name "*LOG.TXT" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*LOG.TXT" -mtime +21 -exec rm {} \;
# find $BACKUP_LOC -name "*CONTROL_FILE.sql" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*CONTROL_FILE.sql" -mtime +21 -exec rm {} \;
# find $BACKUP_LOC -name "*Restore*.rcv" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*Restore*.rcv" -mtime +21 -exec rm {} \;
# find $BACKUP_LOC -name "*_orapw*" -mtime +21 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC -name "*_orapw*" -mtime +21 -exec rm {} \;
#fi

# Purge orphaned PROD Text files
#if  [ $TYPE = PROD ]
#then
# rm -f $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*_init*.ora" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*_init*.ora" -mtime +38 -exec rm {} \;
# find $BACKUP_LOC  -name "*RESTORE_INFO*.TXT" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*RESTORE_INFO*.TXT" -mtime +38 -exec rm {} \;
# find $BACKUP_LOC  -name "*LOG.TXT" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*LOG.TXT" -mtime +38 -exec rm {} \;
# find $BACKUP_LOC  -name "*CONTROL_FILE.sql" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*CONTROL_FILE.sql" -mtime +38 -exec rm {} \;
# find $BACKUP_LOC  -name "*Restore*.rcv" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*Restore*.rcv" -mtime +38 -exec rm {} \;
# find $BACKUP_LOC  -name "*_orapw*" -mtime +38 >> $TEXT_FILE_DELETES
# find $BACKUP_LOC  -name "*_orapw*" -mtime +38 -exec rm {} \;
#fi

######## END MAIN  #########################
exit 0


