#!/bin/ksh
# ******************************************************************************************** 
# NAME:		External_RMANBackup.sh
# USAGE:	External_RMANBackup.sh <Oracle SID> <Full Path to Backup Location>
#
# --------------------------------------------------------------
# funct_db_online_verify(): Verify that database is online               
# --------------------------------------------------------------
funct_db_online_verify(){

 ps -ef | grep ora_pmon_$ORACLE_SID | grep -v grep > /dev/null
 if [ $? -ne 0 ]
 then
  print "Database $ORACLE_SID is not running.  Cannot perform RMAN Backup"
  exit 15
 fi
} 

# ------------------------------------------
# funct_logmode_check(): Check DB log mode               
# ------------------------------------------
funct_logmode_check(){

	STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select log_mode from v\\$database;
		quit
EOF`

	if [ ! $STATUS = "ARCHIVELOG" ]; then
		print "${PROGRAM_NAME} required database to be in ARCHIVELOG mode -- $ORACLE_SID can not be backed up"  >>${BACKUPLOGFILE} 
		exit 18
	fi
}

# --------------------------------------------
# funct_rman_backup():   Backup database in its entirety
# --------------------------------------------
funct_rman_backup() {

# Touch a file to be used later to determine which files are part of this backup set
touch ${START_FILE}

# Create the script to run for the RMAN backup
print "connect target $TRGT_DB" > $BACKUP_SCRIPT

print "run {" >> $BACKUP_SCRIPT
print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT
print "   CONFIGURE CHANNEL DEVICE TYPE DISK MAXOPENFILES 1;" >> $BACKUP_SCRIPT

typeset -i i=1
while [ ${i} -le ${NUM_CHANNELS} ]
do
 print "   ALLOCATE CHANNEL d${i} DEVICE TYPE DISK;" >> $BACKUP_SCRIPT
 i=${i}+1;
done

print "   BACKUP CHECK LOGICAL TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
print "   format '$BACKUP_LOC/df_${BACKUP_TAG}_%U_full_dbf' " >> $BACKUP_SCRIPT
print "   DATABASE FILESPERSET 1 ;" >> $BACKUP_SCRIPT
print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

print "   sql \"alter system SWITCH LOGFILE\";" >> $BACKUP_SCRIPT

print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
print "   FORMAT '$BACKUP_LOC/al_${BACKUP_TAG}_%U_full_archive'" >> $BACKUP_SCRIPT
print "   ARCHIVELOG ALL;" >> $BACKUP_SCRIPT
print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

print "   BACKUP TAG  \"$BACKUP_TAG\" " >> $BACKUP_SCRIPT
print "  FORMAT '$BACKUP_LOC/cf_${BACKUP_TAG}_%U_full_ctrl'" >> $BACKUP_SCRIPT
print "   CURRENT CONTROLFILE;" >> $BACKUP_SCRIPT
print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

print "   report schema;" >> $BACKUP_SCRIPT
print "   host \"date +'%b %d %Y %T'>>${BACKUPLOGFILE}\" ;" >> $BACKUP_SCRIPT

typeset -i j=1
while [ ${j} -le ${NUM_CHANNELS} ]
do
    print "   RELEASE CHANNEL d${j};" >> $BACKUP_SCRIPT
    j=${j}+1;
done

print "}" >> $BACKUP_SCRIPT
print "quit" >> $BACKUP_SCRIPT

nohup $ORACLE_HOME/bin/rman >>${BACKUPLOGFILE} 2>&1 <<HOF
 @${BACKUP_SCRIPT}
HOF

process_errors;

}
# ------------------------------------------------------
# funct_control_backup():  Backup control file to trace
# -------------------------------------------------------
funct_control_backup(){

	CONTROLFILE_NAME=${BACKUP_LOC}/${BACKUP_TAG}_CONTROL_FILE.sql;
    STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
        alter database backup controlfile to trace as '${CONTROLFILE_NAME}';
        exit
EOF`
}

# ---------------------------------------------------------------
# funct_create_restore_log(): Create log with backup information 
# ---------------------------------------------------------------
funct_create_restore_log() {

	# Get the list of datafiles
	print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
	print  "DATAFILES: " >> ${RESTOREFILE}
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a ${RESTOREFILE}
		set heading off feedback off pagesize 500
		select 'DATAFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M'
		from V\$DATAFILE;
	quit
EOF

	# Get the number of datafiles
	NUM_DATA_FILES=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF 
		set heading off feedback off
		select count(*) from V\\$DATAFILE;
	quit
EOF`
	export NUM_DATA_FILES

	# Get the list of Temporary Files
	print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
	print  "Temporary Files: " >> ${RESTOREFILE}
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a ${RESTOREFILE}
		set heading off feedback off
		select 'TEMPFILE |'||file#||' | '||name||' | '||BYTES/1024/1024||'M' from V\$TEMPFILE;
		quit
GOF

	# Get the list of redo logs
	print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
	print  "REDO LOGS: " >> ${RESTOREFILE}
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a ${RESTOREFILE}
		set heading off feedback off pagesize 500
     	select 'LOGFILE | '||a.group#||' | '||member||' | '||a.bytes/1024/1024||'M' from V\$LOGFILE b , V\$LOG a
		where a.GROUP#=b.GROUP#;
	quit
BOF


	# Get SCN 
	print  "\n ------------------------------------------------------------------------------\n " >> ${RESTOREFILE}
	SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off feedback off
		select max(next_change#) from V\\$LOG_HISTORY;
		exit
EOF`
	SCN=$(print $SCN|tr -d " ")
	export SCN

print "DBID:  ${DBID}" | tee -a  ${RESTOREFILE}
print "SCN before backup for information purpose only [max(next_change#)]:  ${SCN}" | tee -a  ${RESTOREFILE}
print "This SCN is not required to restore.  The SCN after the backup will be in the restore scripts." | tee -a  ${RESTOREFILE}
print "BACKUP_TAG:  ${BACKUP_TAG}" | tee -a  ${RESTOREFILE}
print "BACKUPDIR:  ${BACKUP_LOC}" | tee -a  ${RESTOREFILE}
}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_9i(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_9i() {

	BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

	# Create the controlfile restore script
	print "set dbid ${DBID} " >> ${RESTORE_CONTROLFILE_9i}
	print " " >> ${RESTORE_CONTROLFILE_9i}
	print "run {  " >> ${RESTORE_CONTROLFILE_9i}
	print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl'; " >> ${RESTORE_CONTROLFILE_9i}
	print "	restore controlfile from autobackup maxdays 7; " >> ${RESTORE_CONTROLFILE_9i}
	print "	alter database mount; " >> ${RESTORE_CONTROLFILE_9i}
	print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> ${RESTORE_CONTROLFILE_9i}
	print "}  " >> ${RESTORE_CONTROLFILE_9i}

	# Create the Data files restore script
	print "run {  " >> ${RESTORE_DATA_FILES_9i}
	print "	allocate channel 'RCHL1' device type disk;  " >> ${RESTORE_DATA_FILES_9i}
	print "	allocate channel 'RCHL2' device type disk;  " >> ${RESTORE_DATA_FILES_9i}

    ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a ${RESTORE_DATA_FILES_9i}
        set heading off feedback off pagesize 500
		select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';' from V\$DATAFILE;
    quit
EOF

	print "" >> ${RESTORE_DATA_FILES_9i}
    print "$restore_by_log_sequence" >> ${RESTORE_DATA_FILES_9i}
	print "restore database;  " >> ${RESTORE_DATA_FILES_9i}
	print "switch datafile all;  " >> ${RESTORE_DATA_FILES_9i}
	print "recover database;  " >> ${RESTORE_DATA_FILES_9i}
	print "}  " >> ${RESTORE_DATA_FILES_9i}

	# Create the TEMP files restore script
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a ${RESTORE_TEMP_FILES_9i}
		set heading off feedback off
		select 'Alter tablespace  ' || t.name || ' add TEMPFILE ''' || (replace (p.name,'$ORACLE_SID','xxxxxx')) || ''' size ' || bytes/1024/1024 || 'M;'
		from V\$TEMPFILE p, v\$TABLESPACE t where p.ts#=t.ts#;
		quit
GOF


	# Create the Redo files script file
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a ${RESTORE_REDO_9i}
		set heading off feedback off pagesize 500 trimspool on linesize 200
		select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
		from V\$LOGFILE b , V\$LOG a
		where a.GROUP#=b.GROUP#;
	quit
BOF

}

# ----------------------------------------------------------------------------------------
# funct_create_restore_scripts_10g(): Create scripts to be used to restore a 10g database
# -----------------------------------------------------------------------------------------
funct_create_restore_scripts_10g() {

	BACKED_UP_CONTROLFILE=$(ls $BACKUP_LOC/cf_${BACKUP_TAG}_*_full_ctrl)

	# Create the controlfile restore script
	print "set dbid ${DBID} " >> ${RESTORE_CONTROLFILE_10g}
	print " " >> ${RESTORE_CONTROLFILE_10g}
	print "run {  " >> ${RESTORE_CONTROLFILE_10g}
	print "	set controlfile autobackup format for device type disk to '${BACKUP_LOC}/cf_%F_full_ctrl';  "  >> ${RESTORE_CONTROLFILE_10g}
	print "	restore controlfile from autobackup maxdays 7; " >> ${RESTORE_CONTROLFILE_10g}
	print "	alter database mount; " >> ${RESTORE_CONTROLFILE_10g}
	print "	 CONFIGURE CONTROLFILE AUTOBACKUP OFF; " >> ${RESTORE_CONTROLFILE_10g}
	print "}  " >> ${RESTORE_CONTROLFILE_10g}

	# Create the Data/Temp files restore script
	print "run {  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "	allocate channel 'RCHL1' device type disk;  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "	allocate channel 'RCHL2' device type disk;  " >> ${RESTORE_DATA_TEMP_FILES_10g}

    ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF | tee -a ${RESTORE_DATA_TEMP_FILES_10g}
        set heading off feedback off pagesize 500
		select 'SET NEWNAME FOR DATAFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';' from V\$DATAFILE;
    quit
EOF

	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<GOF |tee -a ${RESTORE_DATA_TEMP_FILES_10g}
		set heading off feedback off
		select 'SET NEWNAME FOR TEMPFILE ' ||file#|| ' TO ''' || (replace (name,'$ORACLE_SID','xxxxxx')) || ''';' from V\$TEMPFILE;
		quit
GOF

	print "" >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "$restore_by_log_sequence" >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "restore database;  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "switch datafile all;  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "switch tempfile all;  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "recover database;  " >> ${RESTORE_DATA_TEMP_FILES_10g}
	print "}  " >> ${RESTORE_DATA_TEMP_FILES_10g}

	# Create the Redo files script file
	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a ${RESTORE_REDO_10g}
		set heading off feedback off pagesize 500 trimspool on linesize 200
		select 'ALTER DATABASE RENAME FILE ''' || member || ''' TO ''' || (replace (member,'$ORACLE_SID','xxxxxx')) || ''';'
		from V\$LOGFILE b , V\$LOG a
		where a.GROUP#=b.GROUP#;
	quit
BOF

}

# ---------------------------------------------------
# funct_init_backup():  Backup initxxxxxx.ora file
# ---------------------------------------------------
funct_init_backup(){

	#Copy current init<SID>.ora file to backup directory
	print  " Copying current init${ORACLE_SID}.ora file from spfile"  >> ${BACKUPLOGFILE}

	${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<BOF | tee -a ${BACKUPLOGFILE}
	set heading off feedback off
     	create pfile='${BACKUP_LOC}/${BACKUP_TAG}_init${ORACLE_SID}.ora' from SPFILE;
	quit
BOF
	if [ $? -ne 0 ]
    then
     print "Error Copying the init.ora file to the backup location as init${ORACLE_SID}_${BACKUP_TAG}.ora" >> ${BACKUPLOGFILE}
	fi

	cp ${ORACLE_HOME}/dbs/orapw${ORACLE_SID} ${BACKUP_LOC}/${BACKUP_TAG}_orapw${ORACLE_SID}

}

# ----------------------------------------------------------------------------------------
# process_errors():   PROCESS errors that occured in backup and in the validitation process
# ----------------------------------------------------------------------------------------
process_errors() {

    TMP_ERR_FILE=$WORKING_DIR/captureerr_${BACKUP_TAG}
    rm -rf ${TMP_ERR_FILE}
    cat ${BACKUPLOGFILE}| egrep -i 'failure|ORA-|corruption|rman-|RMAN-'|grep -v RMAN-08137|tee ${TMP_ERR_FILE}

	if [ $? -eq 0 ]
	then
        if [ -s ${TMP_ERR_FILE} ]
		then
            error_msg=$(cat ${TMP_ERR_FILE})
			print  "${PROGRAM_NAME} Failed for ${ORACLE_SID} -> See ${BACKUPLOGFILE} for errors::  $error_msg " 
		fi
	fi
	
	rm -rf ${TMP_ERR_FILE}
	
}
funct_get_dbid() {

    DBID=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
        set heading off
        set feedback off
        select dbid from v\\$database;
        exit
EOF`
    DBID=$(print $DBID|tr -d " ")
}
############################################################
#                       MAIN                               
############################################################
    ostype=$(uname)
    if [ $ostype = Linux ]
    then
     ORATAB=/etc/oratab
    fi
    if [ $ostype = SunOS ]
    then
     ORATAB=/var/opt/oracle/oratab
    fi
	HOSTNAME=$(hostname)

	if [ $# -ne 2 ]
	then
	 print "\n$0 Failed: Incorrect number of arguments -> $0 <ORACLE_SID> <Full Path to Backup Location>"
	 exit 1
	fi

	export ORACLE_SID=$1
	grep "^${ORACLE_SID}:" $ORATAB > /dev/null
	if [ $? -ne 0 ]
	then
	 print "\nThe first parameter entered into script is not a valid Oracle SID in $ORATAB."
	 print "Choose a valid Oracle SID from $ORATAB.\n"
	 exit 2
	fi

	export BACKUP_LOC=$2
    if [[ ! -d $BACKUP_LOC ]]
    then
     print "Backup directory specified by second parameter into this script does not exist"
     print "Directory does not exist: $BACKUP_LOC"
     exit 2
    fi

	export ORAENV_ASK=NO
	export PATH=/usr/local/bin:$PATH
	. /usr/local/bin/oraenv
	export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
	export LD_LIBRARY_PATH=$ORACLE_HOME/lib
	export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
	export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')

    if [ "$(dirname $0)" = "." ]
    then
     WORKING_DIR=$(pwd)
    else
     WORKING_DIR=$(dirname $0)
    fi

	export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt
	export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

	export TRGT_DB="/"
	export BACKUP_SCRIPT=$WORKING_DIR/${ORACLE_SID}_rman_backup_script.sql

	export BACKUP_TAG=${ORACLE_SID}_$(date +'%Y%m%d_%H%M%S')
	BACKUPLOGFILE="$BACKUP_LOC/${BACKUP_TAG}_LOG.TXT"

	# 10g Restore script files
	RESTORE_CONTROLFILE_10g="$BACKUP_LOC/Restore10gControlFile_${BACKUP_TAG}.rcv"
	RESTORE_DATA_TEMP_FILES_10g="$BACKUP_LOC/Restore10gDataTempFiles_${BACKUP_TAG}.rcv"
	RESTORE_REDO_10g="$BACKUP_LOC/Restore10gRedo_${BACKUP_TAG}.rcv"

	# 9i Restore script files
	RESTORE_CONTROLFILE_9i="$BACKUP_LOC/Restore9iControlFile_${BACKUP_TAG}.rcv"
	RESTORE_DATA_FILES_9i="$BACKUP_LOC/Restore9iDataFiles_${BACKUP_TAG}.rcv"
	RESTORE_TEMP_FILES_9i="$BACKUP_LOC/Restore9iTempFiles_${BACKUP_TAG}.rcv"
	RESTORE_REDO_9i="${BACKUP_LOC}/Restore9iRedo_${BACKUP_TAG}.rcv"

	ORAVER=$(print $ORACLE_HOME | grep 9.2)
	if [[ x$ORAVER = 'x' ]]; then
		export Version_10g=1
		export Version_9i=0
	else
		export Version_9i=1
		export Version_10g=0
	fi

	export START_FILE=$BACKUP_LOC/START_TIME.txt

	print " Starting RMAN Backup of ....  $ORACLE_SID on $(date +\"%c\")" 

	funct_db_online_verify
	funct_logmode_check
    funct_get_dbid

	RESTOREFILE="$BACKUP_LOC/${BACKUP_TAG}_RESTORE_INFO.TXT"

	funct_create_restore_log

	print "${ORACLE_SID}, RMAN Backup Started on $(date +\"%c\")" |tee -a $BACKUPLOGFILE
	export NUM_CHANNELS=2

	funct_rman_backup

	STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
	alter database create standby controlfile as  '${BACKUP_LOC}/standbycontrol_${BACKUP_TAG}.ctl';
	quit
EOF`

    newest_archive_log_in_backup=$(grep 'input archive.* log thread=[0-9] sequence=' $BACKUPLOGFILE | awk '{print $5}' | awk -F= '{print $NF}' | sort -n | tail -1)
    export restore_by_log_sequence="set until sequence $newest_archive_log_in_backup thread 1;"

    # Get the SCN after the backup
    SCN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
        set heading off
        set feedback off
        select max(next_change#) from V\\$LOG_HISTORY;
        exit 16
EOF`
    SCN=$(print $SCN|tr -d " ")
    print "SCN retrieval after backup: $SCN" >> $BACKUPLOGFILE

	if [ $Version_10g = 1 ]
	then
	 funct_create_restore_scripts_10g
	else
	 funct_create_restore_scripts_9i
	fi

	funct_init_backup
	funct_control_backup

######## END MAIN  #########################
exit 0
