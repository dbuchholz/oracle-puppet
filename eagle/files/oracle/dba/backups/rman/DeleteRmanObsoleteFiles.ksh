#!/bin/ksh
# *************************************************************************************************************************
# NAME:         DeleteRmanObsoleteFiles.ksh
#
# AUTHOR:       Nirmal S Arri
#
# PURPOSE:      This utility will delete all the obsolete .ora/.rvc/.TXT/orapw/.sql files created during RMANBackup.sh run 
#               which are over 38 days old for PROD and 21 days for IMP.
#
# USAGE:        DeleteRmanObsoleteFiles.ksh
#
# INPUT PARAMETERS: Not Required
#
# Revisions
# ----------
# 
# *************************************************************************************************************************
#set -x

####### Main ###############

HOSTNAME=$(hostname)
IPCheck=$(cat /etc/hosts | grep $HOSTNAME | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
else
 export TYPE=IMP
fi

export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
 fi
fi
rm -f $NO_COMMENT_PARFILE
# echo $CRON_SID 
# echo $CODE


SID_LIST=`ps -ef |grep pmon|grep -v grep| awk -F_ '{print $3}'`

for line in $SID_LIST
do

export LIST_TEXT_FILE_DELETES=$HOME/local/dba/backups/rman/${line}_LIST_FILE_Deletes.txt
export LIST_FILES_DELETED=$HOME/local/dba/backups/rman/${line}_FILE_Deletes.txt


if [[ $TYPE = PROD ]]
then
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<!
set heading off
set feedback off
set pagesize 0
set linesize 200
spool ${LIST_TEXT_FILE_DELETES}
select start_time||' '||backup_location||' '||substr(backup_tag,9,15) from inf_monitor.backup_runs  where start_time between (sysdate-66) and (sysdate-38) and db_instance=(select instance from inf_monitor.databases where sid = '${line}' and dataguard='N') order by start_time;

spool off
!
else
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<!
set heading off
set feedback off
set pagesize 0
set linesize 200
spool ${LIST_TEXT_FILE_DELETES}
select start_time||' '||backup_location||' '||substr(backup_tag,9,15) from inf_monitor.backup_runs  where start_time between (sysdate-42) and (sysdate-21) and db_instance=(select instance from inf_monitor.databases where sid = '${line}' and dataguard='N') order by start_time;

spool off
!
fi

while read file_dir
do
IFS=' ' read v_date db_dir date_tag <<< ${file_dir}

#echo ${v_date}
#echo ${db_dir}
#echo ${date_tag}
rm ${LIST_FILES_DELETED}
ls -ltr ${db_dir}/*${date_tag}*.ora >> ${LIST_FILES_DELETED}
rm ${db_dir}/*${date_tag}*.ora
ls -ltr ${db_dir}/*${date_tag}*.TXT >> ${LIST_FILES_DELETED}
rm ${db_dir}/*${date_tag}*.TXT
ls -ltr ${db_dir}/*${date_tag}*.sql >> ${LIST_FILES_DELETED}
rm ${db_dir}/*${date_tag}*.sql
ls -ltr ${db_dir}/*${date_tag}*.rcv >> ${LIST_FILES_DELETED}
rm ${db_dir}/*${date_tag}*.rcv
ls -ltr ${db_dir}/*${date_tag}_orapw* >> ${LIST_FILES_DELETED}
rm ${db_dir}/*${date_tag}_orapw*

done <${LIST_TEXT_FILE_DELETES}

done 

rm ${LIST_TEXT_FILE_DELETES}
rm ${LIST_FILES_DELETED}

exit 0

