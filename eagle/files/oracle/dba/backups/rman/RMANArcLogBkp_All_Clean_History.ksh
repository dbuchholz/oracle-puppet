#!/bin/ksh
MAILTO=team_dba@eagleaccess.com
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ $(dirname $0) = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$log_dir/machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where lower(os_type) != 'windows' order by name;
EOF

rm -f $log_dir/past_week_arch_clean.txt
while read name ip_address
do
 print $name
 ssh -nq $ip_address find /u01/app/oracle/CRONLOG -type f -name RMANArcLogBkp_All_Clean_*.log -mtime -7 -exec cat \{\} \\\; | grep 'Ran RMANArcLogBkp_All_Clean' | tee -a $log_dir/past_week_arch_clean.txt
done<$log_dir/machine_info.txt

sed G $log_dir/past_week_arch_clean.txt > $log_dir/previous_week_arch_clean.txt

if [[ -s $log_dir/past_week_arch_clean.txt ]]
then
mailx -s "Archive Log Cleanup For Past Week" $MAILTO<<EOF
Over the past seven days, the following archive log mount points exceeded 75% capacity
and was backed up to DataDomain and the files removed to clear up space.
Based on the frequency of cleanup events for a given database,
evaluate the need to expand the archive log filesystem.

$(cat $log_dir/previous_week_arch_clean.txt)

EOF
fi
rm -f $log_dir/past_week_arch_clean.txt

exit 0
