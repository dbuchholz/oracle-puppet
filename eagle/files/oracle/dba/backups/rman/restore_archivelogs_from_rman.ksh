#!/bin/ksh
# Script name: restore_archivelogs_from_rman.ksh
# Usage: restore_archivelogs_from_rman.ksh
ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}'
print
read ORACLE_SID?"Enter the Oracle SID of the database from the list above that you will be restoring archive logs: "
export ORACLE_SID
export PAR_HOME=$HOME/local/dba
INIFILE=$HOME/local/dba/backups/rman/RMANParam.ini
if [[ -a $INIFILE ]]
then
 export BACKUP_BASE=$(grep $ORACLE_SID $INIFILE | awk '{print $2}')
 if [ x$BACKUP_BASE = 'x' ]
 then
  export BACKUP_BASE=$(grep all $INIFILE | awk '{print $2}')
  if [ x$BACKUP_BASE = 'x' ]
  then
   print "$PROGRAM_NAME Failed for ${ORACLE_SID}: Backup Base Location not defined in ${INIFILE}"
   exit 5
  fi
 fi

 CATALOG_ID=$(grep CATALOG_ID $INIFILE | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 if [ x$CATALOG_ID = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_ID not defined in $INIFILE"
  exit 6
 fi

 CATALOG_PASSWD=$(grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [ x$CATALOG_PASSWD = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_PASSWD not defined in $INIFILE"
  exit 7
 fi
 export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [ x$CATALOG_DB = 'x' ]
 then
  print "$PROGRAM_NAME FAILED -> ${ORACLE_SID}:  CATALOG_DB not defined in $INIFILE"
  exit 8
 fi
else
 print "$PROGRAM_NAME FAILED: $ORACLE_SID - $INIFILE file does not exist"
fi
read from_sequence?"Enter the beginning archive log sequence number you want to restore from rman: "
read to_sequence?"Enter the ending archive log sequence number you want to restore from rman: "
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/rman target / catalog $CATALOG_ID/${CATALOG_PASSWD}@$CATALOG_DB<<EOF
RESTORE ARCHIVELOG FROM SEQUENCE $from_sequence UNTIL SEQUENCE $to_sequence;
EOF
print "Archive log restore complete"
exit 0
