#!/usr/bin/ksh
# ******************************************************************* 
# NAME:		hotbackup                                 
# 
# AUTHOR: 	Maureen Buotte                                    
#
# PURPOSE: 	This utility will perform a hot backup of the specified database
#
# USAGE:		hotbackup SID 
#
# INPUT PARAMETERS:
#		SID	Oracle SID of database to backup
#
# Revisions
# ----------
# Frank Davis       24-Sep-2013 Remove tech_support@eagleaccess.com from email
# Frank Davis       24-Jun-2011 Modified IPCheck Variable to include updated list of VLANs
# Maureen Buotte	2-Feb-2009	Changed df -k size check to allow for larger filesystem names
# Maureen Buotte	17-Jan-2009	Changed to allow for more than 1 SID Exception, change echo to echo -e
# Maureen Buotte	5-Aug-2008	Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte	1-Jun-2008	Added reporting to DBA database
# Maureen Buotte	29-Aug-2005	Cleaned up and made generic between 8i and 9i
# Maureen Buotte	02-Jun-2005	Added second log switch 
# Nirmal Arri 		27-MAY-2005 	Added function funct_chk_disk()
#					to check the available disk space,
#					before the backup begins.
# Nirmal Singh Arri	13-Jun-2005	Replaced <<! and !! with EOF in funct_chk_disk()
# ********************************************************************
#
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications 
# -----------------------------------------------------------------------------
function SendNotification {

        # Uncomment for debug
        # set -x

        Machine=`uname -a | awk '{print $2}'`
        echo -e "Error during hot backup \n	Database: ${ORACLE_SID} \n	Machine: $Machine" > mail.dat
	if [[ x$1 != 'x' ]]; then
		echo -e "\n$1\n" >> mail.dat
	fi
        cat mail.dat | mail -s "${TYPE} Environment -- ${PROGRAM_NAME} failed for ${ORACLE_SID}" ${MAILTO}
        rm mail.dat

        # Update mail sent indicator in DBA Admin Database record
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		if [[ ${Sequence_Number} -ne 0 ]]; then
			# Update mail sent indicator in DBA Admin Database record
			STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			update CRON_JOB_RUNS set mail_sent=mail_sent+1 where instance=${Sequence_Number};
			commit;
			exit
EOF`
		fi
	fi


        return 0

}
# --------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------
funct_chk_parm() {
	# Uncomment next line for debugging
	# set -x
	if [ ${NARG} -ne 1 ]; then
		echo -e "HOTBACKUP_FAIL: Not enough arguments passed -> hotbackup.sh SID"
                SendNotification "Not enough arguments passed -> hotbackup.sh SID"
		exit 1
	fi
} 

# ---------------------------------------------------
# funct_get_vars(): Get environment variables
# ---------------------------------------------------
funct_get_vars(){

	# Uncomment next line for debugging
	# set -x

	init_file=$ORA_HOME/dbs/init$ORACLE_SID.ora
	if [[ ! -f $init_file ]]; then
		echo -e "HOTBACKUP_FAIL: init$ORACLE_SID.ora does not exist in ORACLE_HOME/dbs" 
                SendNotification "init$ORACLE_SID.ora does not exist in ORACLE_HOME/dbs"
		exit 1
	fi

	log_arch_dest1=`sed  /#/d  $init_file|grep -i  log_archive_dest_1|awk -F "="  '{print $3}'`
	log_arch_dest=`echo -e $log_arch_dest1|tr -d "'"|tr -d '"'`
	if [[ x$log_arch_dest = 'x' ]]; then
		echo -e "HOTBACKUP_FAIL: log_archive_dest not defined " 
                SendNotification "log_archive_dest not defined "
		exit 1
	fi


	udump_dest=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select value from v\\$parameter
		where name='user_dump_dest';
		exit
EOF`

	udump_dest=`echo -e $udump_dest|tr -d " "`

	if [[ x$udump_dest = 'x' ]]; then
		echo -e "HOTBACKUP_FAIL: user_dump_dest not defined " 
                SendNotification "user_dump_dest not defined " 
		exit 1
	fi
} 

# ------------------------------------------------------------------------
# funct_chk_bkup_dir(): Create backup directories if the don't exist
# ------------------------------------------------------------------------
funct_chk_bkup_dir() {
	# Uncomment next line for debugging
	# set -x
                
	RESTOREFILE_DIR="${BACKUPDIR}/restorefile_dir"
	BACKUPLOG_DIR="${BACKUPDIR}/backuplog_dir"
	DATAFILE_DIR="${BACKUPDIR}/datafile_dir"
	CONTROLFILE_DIR="${BACKUPDIR}/controlfile_dir"
	REDOLOG_DIR="${BACKUPDIR}/redolog_dir"
	ARCLOG_DIR="${BACKUPDIR}/arclog_dir"
	INITFILE_DIR="${BACKUPDIR}/initfile_dir"

	BACKUPLOGFILE="${BACKUPLOG_DIR}/backup_log_${ORACLE_SID}"
	RESTOREFILE="${RESTOREFILE_DIR}/restorefile_${ORACLE_SID}"
	LOGFILE="${LOGDIR}/${ORACLE_SID}.log"

	if  [ ! -d  ${RESTOREFILE_DIR} ]; then mkdir -p ${RESTOREFILE_DIR}; fi
	if  [ ! -d  ${BACKUPLOG_DIR} ]; then  mkdir -p  ${BACKUPLOG_DIR}; fi
	if  [ ! -d  ${DATAFILE_DIR} ]; then  mkdir -p  ${DATAFILE_DIR}; fi
	if  [ ! -d  ${CONTROLFILE_DIR} ]; then  mkdir -p  ${CONTROLFILE_DIR}; fi
	if  [ ! -d  ${REDOLOG_DIR} ]; then  mkdir -p  ${REDOLOG_DIR}; fi
	if  [ ! -d  ${ARCLOG_DIR} ]; then  mkdir -p  ${ARCLOG_DIR}; fi
	if  [ ! -d  ${INITFILE_DIR} ]; then  mkdir -p  ${INITFILE_DIR}; fi
	if  [ ! -d  ${LOGDIR} ]; then  mkdir -p  ${LOGDIR}; fi

	# Remove old backup
	rm -f ${RESTOREFILE_DIR}/*
	rm -f ${BACKUPLOG_DIR}/*
	rm -f ${DATAFILE_DIR}/*
	rm -f ${CONTROLFILE_DIR}/*
	rm -f ${REDOLOG_DIR}/*
	rm -f ${ARCLOG_DIR}/*
	rm -f ${INITFILE_DIR}/*

	echo -e  "--${PROGRAM_NAME}: hotbackup of ${ORACLE_SID} began on `date +\"%c\"`" > ${BACKUPLOGFILE}
}  

# --------------------------------------------------------------
# funct_verify(): Verify that database is online               
# --------------------------------------------------------------
funct_verify(){

	# Uncomment next line for debugging
	# set -x

	STATUS=`ps -fu oracle |grep -v grep| grep ora_pmon_${ORACLE_SID}`
	if [ $? != 0 ]; then
		echo -e "Database ${ORACLE_SID} is not running.  Can't perform hotbackup "
		SendNotification "Database ${ORACLE_SID} is not running. Can't perform hotbackup "
		exit 1
	fi
} 

# ------------------------------------------
# funct_chk_dblogmode(): Check DB log mode               
# ------------------------------------------
funct_chk_dblogmode(){

	# Uncomment next line for debugging
	# set -x

	STATUS=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select log_mode from v\\$database;
		exit
EOF`

	if [ $STATUS = "NOARCHIVELOG" ]; then
		echo -e  "`date`" |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
		echo -e "HOTBACKUP_FAIL: $ORACLE_SID is in NOARCHIVELOG mode. Can't perform hotbackup "  |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
                SendNotification "$ORACLE_SID is in NOARCHIVELOG mode. Can't perform hotbackup "
		exit 1
	fi
}


# --------------------------------------------------
# funct_chk_disk(): Check the backup disk space available
# --------------------------------------------------
funct_chk_disk() {

	# Uncomment next line for debugging
	# set -x

	typeset -i DISK_GB=`df -P | grep ${BACKUP_MOUNT}'\b' | awk '{print int($(NF-2)/1024/1024)}'`
	echo -e "Available Disk space = ${DISK_GB} GB"

	typeset -i DFILE_GB=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<EOF
		set echo off head off feedback off pages 0
		select ceil((sum(bytes/1024)/1024/1024)) from v\\$datafile;
EOF`
	echo -e "Total Datafile size to copy = ${DFILE_GB} GB" 

	typeset -i ARCH_GB=`ls -l ${log_arch_dest} | awk '{sum+=int($5/1024/1024/1024)} END {print sum}'`
	echo -e "Total Logfile size to copy = ${ARCH_GB} GB" 

        typeset -i TOTAL_COPY=${ARCH_GB}+${DFILE_GB}
	echo -e "Total Size of Backup = ${TOTAL_COPY} GB"

	if [[ ${TOTAL_COPY} -ge ${DISK_GB} ]]; then
		echo -e "HOTBACK_FAIL: ${ORACLE_SID}, Not enough diskspace on ${BACKUPDIR}"
		SendNotification "Not enough space available on ${BACKUPDIR}\n   Required:  ${TOTAL_COPY} GB\n  Available:  ${DISK_GB} GB"
		exit 1
	fi
}

# --------------------------------------------
# funct_hot_backup():   Backup datafiles
# --------------------------------------------
funct_hot_backup(){

	# Uncomment next line for debugging
	# set -x

	# Get the list of  tablespaces
	echo -e  "Building tablespace list " >> ${BACKUPLOGFILE}
	tablespace_list=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off 
		set feedback off
		set pagesize 500
		select distinct tablespace_name from dba_data_files
		order by tablespace_name;
		exit
EOF`

	echo -e "##### DATE:" `date` > ${RESTOREFILE}
	echo -e "###### Data Files( Please restore only corrupted files) " >> ${RESTOREFILE}

	for tblspace in `echo -e $tablespace_list`
	do
		# Get the datafiles for the current tablespace
		datafile_list=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
			set heading off   
			set feedback off
			set pagesize 500
			select file_name from dba_data_files
			where tablespace_name = '${tblspace}';
			exit
EOF`

		echo -e  " Beginning back up of tablespace ${tblspace}..."  >> ${BACKUPLOGFILE}
		`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
			set heading off   
			set feedback off
			alter tablespace ${tblspace} begin backup;
			exit
EOF`
		  
		# Copy datafiles of current tablespace
		for datafile in `echo -e $datafile_list`
		do
			echo -e  "Copying datafile ${datafile}..." >> ${BACKUPLOGFILE}
			# The next command prepares restore file
			echo -e cp -p ${DATAFILE_DIR}/`echo -e $datafile|awk -F"/" '{print $NF}'` $datafile >> ${RESTOREFILE}
			cp -p ${datafile} ${DATAFILE_DIR}
			if [ $? != 0 ];  then
				echo -e  "`date`" |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
				echo -e  "HOTBACKUP_FAIL:  Failed to copy file to backup location "| tee -a ${BACKUPLOGFILE}  >> ${LOGFILE}
				SendNotification "Failed to copy file ${datafile} to backup location ${DATAFILE_DIR}"

				# Ending the tablespace backup before exiting
				`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF 
					set heading off  
					set feedback off 
					alter tablespace ${tblspace} end backup;
					exit
EOF`

				exit 1
			fi
		done

		`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF 
			set heading off   
			set feedback off
			alter tablespace ${tblspace} end backup;
			exit
EOF`

		echo -e  " Ending back up of  tablespace ${tblspace}.." >> ${BACKUPLOGFILE}
	done
} 

# --------------------------------------------------
# funct_temp_backup():  Prepare SQL for temp files
# --------------------------------------------------
funct_temp_backup(){

	# Uncomment next line for debugging
	# set -x

	echo -e "############# Recreate the following Temporary Files" >> ${RESTOREFILE}
	`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF  >> ${RESTOREFILE}
		set heading off   
		set feedback off
		select 'alter tablespace '||tablespace_name||' add tempfile '||''''||file_name||''''||' reuse'||';'
		from dba_temp_files;
		exit
EOF`
}

# -----------------------------------------------
# funct_control_backup():  Backup control file
# -----------------------------------------------
funct_control_backup(){

	# Uncomment next line for debugging
	# set -x

	echo -e  " Begin backup of controlfile and trace to trace file"  >> ${BACKUPLOGFILE}
	`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		alter database backup controlfile to '${CONTROLFILE_DIR}/backup_control.ctl';
		alter database backup controlfile to trace;
		exit
EOF`

	# Backup trace of control file
	CONTROL=`ls -t ${udump_dest}/*.trc |head -1`
	if [ ! -z "$CONTROL" ];  then
		grep 'CONTROL' ${CONTROL} 1> /dev/null
		if test $? -eq 0; then
			cp ${CONTROL} ${CONTROLFILE_DIR}/backup_control.sql
		fi
	fi  

	# Prepare restore file for control file
	echo -e "###### Control File   " >> ${RESTOREFILE}
	echo -e "# Restoring control file not advised unless required..."  >> ${RESTOREFILE}
	echo -e  " End of backup of control file"  >> ${BACKUPLOGFILE}
}

# ------------------------------------------------------
# funct_archivelog_backup():  Backup archivelog files
# ------------------------------------------------------
funct_archivelog_backup(){

	# Uncomment next line for debugging
	# set -x

	echo -e  "Begin backup of archived redo logs" >> ${BACKUPLOGFILE}
	#Switch logs to flush current redo log to archived redo before back up
	`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off 
		set feedback off
		alter system switch logfile;
		exit
EOF`

	sleep 15

	#Switch logs to flush current redo log to archived redo before back up
	`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off 
		set feedback off
		alter system archive log stop;   
		exit
EOF`

	# This gets the redo sequence number that is being archived
	# and remove this from the list of files to be backed up
	ARCSEQ=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off 
		set feedback off
		select min(sequence#) from v\\$log
		where archived='NO';
		exit
EOF`

	#Get current list of archived redo log files
	ARCLOG_FILES=`ls ${log_arch_dest}/*|grep -v $ARCSEQ`

	`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off  
		set feedback off
		alter system archive log start;   
		exit
EOF`

	#Prepare restore file for arc log files 
	echo -e "##### Archive Log Files" >> ${RESTOREFILE}
	echo -e "Archive log process" >> ${RESTOREFILE}
	for arc_file in `echo -e $ARCLOG_FILES`
	do
		echo -e cp -p ${ARCLOG_DIR}/`echo -e $arc_file|awk -F"/" '{print $NF}'` $arc_file >> ${RESTOREFILE}
	done

	#Copy arc log files to backup location
	# And remove the archived redo logs from the log_archive_dest if copy is successful
	cp -p ${ARCLOG_FILES} ${ARCLOG_DIR}
	if [ $? = 0 ]; then
		echo -e "Archive Log Copy Successful" >> ${LOGFILE}
		rm ${ARCLOG_FILES}
	else
		echo -e  "`date`" |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
		echo -e  "HOTBACKUP_FAIL:   Failed to copy Archive log files" |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
		SendNotification "Failed to copy Archive log files:  ${ARCLOG_FILES} \n to ${ARCLOG_DIR}"
		exit 1
	fi

	echo -e  "End backup of archived redo logs"  >> ${BACKUPLOGFILE}
}

# ---------------------------------------------------
# funct_init_backup():  Backup initxxxxxx.ora file
# ---------------------------------------------------
funct_init_backup(){

	# Uncomment next line for debugging
	# set -x

	#Copy current init<SID>.ora file to backup directory
	echo -e  " Copying current init${ORACLE_SID}.ora file"  >> ${BACKUPLOGFILE}
	cp -p ${init_file} ${INITFILE_DIR}/init${ORACLE_SID}.ora
	if [ $? != 0 ]; then
		echo -e "Failed to copy init.ora file to backup location "
		exit 1
	fi

	# Prepare restore file for init.ora
	echo -e "############# Parameter Files" >> ${RESTOREFILE}
	echo -e cp -p ${INITFILE_DIR}/init${ORACLE_SID}.ora  ${init_file} >> ${RESTOREFILE}
}

# ---------------------------------------------------------
#  funct_final_arclog_backup()  For DR Site need final log
# ---------------------------------------------------------
funct_final_arclog_backup(){

        # Uncomment next line for debugging
        # set -x

        echo -e  "Begin backup of final archived redo log" >> ${BACKUPLOGFILE}
        #Switch logs to flush current redo log to archived redo before back up
        `${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
                set heading off
                set feedback off
                alter system switch logfile;
                exit
EOF`

	sleep 15

        #Get list of archived redo log files
	echo -e "Final Archive log process" >> ${RESTOREFILE}
        ARCLOG_FILES=`ls ${log_arch_dest}/*`
        for arc_file in `echo -e $ARCLOG_FILES`
        do
                echo -e cp -p ${ARCLOG_DIR}/`echo -e $arc_file|awk -F"/" '{print $NF}'` $arc_file >> ${RESTOREFILE}
        done

        # Put copy into the ARCLOG_DIR
        cp -p ${ARCLOG_FILES} ${ARCLOG_DIR}

        echo -e  "End backup of final archived redo log"  >> ${BACKUPLOGFILE}
}

############################################################
#                       MAIN                               
############################################################

# Uncomment next line for debugging
# set -x

NARG=$#
ORA_SID=$1
funct_chk_parm

export BACKUP_HOME=$HOME/local/dba/backups/hot
export INIFILE=$BACKUP_HOME/hotbackup.ini

# Set environment variables
export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export Sequence_Number=0
export BACKUP_SIZE=0
export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/CronStatus.ini

IPCheck=`cat /etc/hosts | grep $(hostname) | awk '{print $1}' | egrep '10\.60\.5\.|10\.60\.3\.|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
        export TYPE=PROD
else
        export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com

if [[ -a ${INIFILE} ]]; then
	export BACKUP_BASE=`cat ${INIFILE} | grep ${ORA_SID} | awk '{print $2}'`
	if [[ x$BACKUP_BASE = 'x' ]]; then
		export BACKUP_BASE=`cat ${INIFILE} | grep all | awk '{print $2}'`
		if [[ x$BACKUP_BASE = 'x' ]]; then
			echo -e "HOTBACKUP_FAIL: ${ORACLE_SID} - Backup location not defined in hotbackup.ini"
			SendNotification "HOTBACKUP_FAIL: ${ORACLE_SID} - Backup location not defined in hotbackup.ini"
		fi
	fi
else
	echo -e "HOTBACKUP_FAIL: ${ORACLE_SID} - hotbackup.ini file does not exist"
	SendNotification "HOTBACKUP_FAIL: ${ORACLE_SID} - hotbackup.ini file does not exist"
fi

BACKUPDIR=${BACKUP_BASE}/${ORA_SID}
BACKUPMOUNT=`echo -e $BACKUPDIR | awk -F/ '{print $2}' `
export BACKUP_MOUNT="/"${BACKUPMOUNT}

LOGDIR="${BACKUPDIR}"

if [[ -a ${PARFILE} ]]; then
        LINE=`head -1 ${PARFILE}`
        if [[ -n $LINE ]]; then
                export PERFORM_CRON_STATUS=1
                export CRON_SID=`echo -e $LINE | awk '{print $1}' `
                export CRON_USER=`echo -e $LINE | awk '{print $2}' `
                CRON_CONNECT=`echo -e $LINE | awk '{print $3}' `
                export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
                export BOX_PREFIX=`echo -e $LINE | awk '{print $4}' `
                export SID_EXCEPTION=`echo -e $LINE | awk '{print $5}' `
        fi
fi

. /usr/bin/setsid.sh $ORA_SID
Status=$?
if [ $Status != 0 ]; then
        echo -e "Invalid SID -> ${ORA_SID}"
        SendNotification "Invalid SID -> ${ORA_SID}"
        exit 1
fi

# See if SID running for is in exception list
EXCEPTION=`awk -v a="$SID_EXCEPTION" -v b="$ORA_SID" 'BEGIN{print index(a,b)}' `

# Make sure this is not a Dataguard Instance
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	ORA_SID_PREFIX=`echo -e ${ORA_SID} | awk '{print  substr ($1,1,3)}'`
	if [ "${ORA_SID_PREFIX}" != "${BOX_PREFIX}" ] && [ "${EXCEPTION}" == "0" ]; then
		echo -e "Hotbackup.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance"
		SendNotification "Hotbackup.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance"
		exit 1
	fi
fi

export ORACLE_SID
export ORACLE_HOME
export PATH
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export ORACLE_BASE=$HOME

echo -e " Starting hotbackup of ....  ${ORACLE_SID} on `date +\"%c\"`" 

# Add Start time entry to DBA Auditing Database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from CRON_JOB_RUNS where script_name='${PROGRAM_NAME}' and
		run_date > to_char(sysdate,'DD-MON-YYYY') and lower(sid)='${ORA_SID}';
	exit
EOF`

	Sequence_Number=`echo -e $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set serveroutput on size 1000000
			set heading off
			set feedback off
			declare i number;
			begin
			insert into CRON_JOB_RUNS values (cron_job_runs_seq.nextval,'${PROGRAM_NAME}','${BOX}','${ORA_SID}', sysdate, sysdate, '','',0,'','${BACKUP_BASE}','','0') returning instance into i;
			commit;
			dbms_output.put_line (i);
			end;
/
		exit
EOF`
		else
			STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			update CRON_JOB_RUNS set run_date=sysdate, start_time=sysdate where instance=${Sequence_Number};
			commit;
/
EOF`
		fi

fi

export Sequence_Number

funct_get_vars
funct_chk_bkup_dir
funct_verify
funct_chk_dblogmode
funct_chk_disk
funct_hot_backup
funct_temp_backup
funct_control_backup
funct_archivelog_backup
funct_init_backup
funct_final_arclog_backup

if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	cd ${BACKUPDIR}
	typeset -i BACKUP_SIZE=`du -sk | awk '{print int($1/1024)}'`
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update CRON_JOB_RUNS set end_time=sysdate,backup_size=${BACKUP_SIZE},status='SUCCESS',number_of_runs=number_of_runs+1 where instance=${Sequence_Number};
		commit;
			
		update CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':' ||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09')) 
		from cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit; 
		exit    
EOF`
fi

echo -e  "${ORACLE_SID}, hotbackup Completed successfully on `date +\"%c\"`" |tee -a ${BACKUPLOGFILE} >> ${LOGFILE}
echo -e  "${ORACLE_SID}, hotbackup Completed successfully on `date +\"%c\"`" 
######## END MAIN  #########################

