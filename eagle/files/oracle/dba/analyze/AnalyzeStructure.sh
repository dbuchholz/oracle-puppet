#!/usr/bin/ksh
# ******************************************************************* 
# NAME:		AnalyzeStructure.sh
# 
# AUTHOR: 	Maureen Buotte
#
# PURPOSE: 	This utility will Analyze and Validate the Physical Structure of Tables and it's associate 
#		Indexes for any corruptions.
#
# USAGE:	AnalyzeStructure.sh SID 
#
# INPUT PARAMETERS:
#		SID	Oracle SID of database to Analyze
#
# Revisions
# Frank Davis  09/24/2013  Remove tech_support@eagleaccess.com was from email
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis      24-Jun-2011  Modified IPCheck Variable to include updated list of VLANs
# Maureen Buotte    5-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    1-Jun-2008  Added reporting to DBA database
#
# ********************************************************************
#
# -----------------------------------------------------------------------------
# Function SendNotification
#       This function sends mail notifications 
# -----------------------------------------------------------------------------
function SendNotification {

        # Uncomment for debug
        # set -x

        Machine=`uname -a | awk '{print $2}'`

        cat $ErrorFile | /bin/mail -s "${TYPE} Environment -- ${PROGRAM_NAME} failed for ${ORACLE_SID} on ${Machine}" ${MAILTO}
        rm $ErrorFile

        # Update mail sent indicator in DBA Admin Database record
        if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
                if [[ ${Sequence_Number} -ne 0 ]]; then
                        # Update mail sent indicator in DBA Admin Database record
                        STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
                        set heading off
                        set feedback off
                        update CRON_JOB_RUNS set mail_sent=mail_sent+1 where instance=${Sequence_Number};
                        commit;
                        exit
EOF`
                fi
        fi

        return 0

}

# --------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------
funct_chk_parm() {
	# Uncomment next line for debugging
	# set -x
	if [ ${NARG} -ne 1 ]; then
		echo "ANALYZE_STRUCTURE_FAILED: Due to not enough arguments passed -> AnalyzeStructure.sh SID"
                SendNotification "Not enough arguments passed -> AnalyzeStructure.sh SID"
		exit 1
	fi
} 

# --------------------------------------------------------------
# funct_verify(): Verify that database is online               
# --------------------------------------------------------------
funct_verify(){

	# Uncomment next line for debugging
	# set -x

	STATUS=`ps -fu ${ORA_OWNER} |grep -v grep| grep ora_pmon_${ORACLE_SID}`
	if [ $? != 0 ]; then
		echo "Database ${ORACLE_SID} is not running.  Can't perform AnalyzeStructure "
		 SendNotification "Database ${ORACLE_SID} is not running. Can't perform AnalyzeStructure"
		exit 1
	fi
} 

# --------------------------------------------
# funct_analyze_struct():   Analyze Tables
# --------------------------------------------
funct_analyze_struct(){

        # Uncomment next line for debugging
        # set -x
        Status=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
                set heading off
                set feedback off
                set linesize 300
                set pages 0
                spool $SCRIPTFILE
                select 'dbv file=$DATAFILE_DIR/'||substr(a.file_name,instr(a.file_name,'/',-1,1)+1)||
                ' LOGFILE=$LOGDIR/$ORA_SID'||'_analyze_'||substr(a.file_name,instr(a.file_name,'/',-1,1)+1)||'.log'||
                ' BLOCKSIZE='||b.block_size
                from dba_data_files a,dba_tablespaces b
                where a.tablespace_name = b.tablespace_name;
                spool off
                exit
EOF`
          chmod +x $SCRIPTFILE
          $SCRIPTFILE
         
          if test -f ${LOGFILE}
          then
           rm $LOGFILE
          fi

          cat $LOGDIR/$ORA_SID_analyze_*.dbf.log >> $LOGFILE
          rm $LOGDIR/$ORA_SID_analyze_*.dbf.log
}

############################################################
#                       MAIN                               
############################################################

# Uncomment next line for debugging
#set -x

export LOGDIR=$HOME/local/dba/analyze/logs
NumericDate=`date +%Y%m%d`
export CORRUPTION_ERROR=0

# Set environment variables
export PROGRAM_NAME=`echo  $0 | sed 's/.*\///g'`
export BOX=`echo $(hostname) | awk -F "." '{print $1}'`
export Sequence_Number=0
export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/CronStatus.ini
if [[ -a ${PARFILE} ]]; then
        LINE=`head -1 ${PARFILE}`
        if [[ -n $LINE ]]; then
                export PERFORM_CRON_STATUS=1
                export CRON_SID=`echo $LINE | awk '{print $1}' `
                export CRON_USER=`echo $LINE | awk '{print $2}' `
                CRON_CONNECT=`echo $LINE | awk '{print $3}' `
                export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
                export BOX_PREFIX=`echo $LINE | awk '{print $4}' `
                export SID_EXCEPTION=`echo $LINE | awk '{print $5}' `
        fi
fi

if [ x$SID_EXCEPTION = 'x' ]; then
        export SID_EXCEPTION=NONE
fi

IPCheck=`cat /etc/hosts | grep $(hostname) | awk '{print $1}' | egrep '10\.60\.5\.|10\.60\.3\.|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
        export TYPE=PROD
else
        export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com

STATUS=`echo $HOME | grep oracle9`
if [ x$STATUS = 'x' ]; then
        export ORA_OWNER=oracle
else
        export ORA_OWNER=oracle9
fi

export ORATAB="/etc/oratab"
NARG=$#
if [ ${NARG} -eq 1 ]; then
        # Running for a single database
        ORA_SID=$1
        . /usr/bin/setsid.sh $ORA_SID
        Status=$?
        if [ $Status != 0 ]; then
                echo "Invalid SID -> ${ORA_SID}"
                 SendNotification "Invalid SID -> ${ORA_SID}"
                exit 1
        fi

	export ErrorFile=${HOME}/local/dba/analyze/logs/ErrorFile_${ORA_SID}

    #See if SID running for is in exception list
    EXCEPTION=`awk -v a="$SID_EXCEPTION" -v b="$ORA_SID" 'BEGIN{print index(a,b)}' `
     
    #Make sure this is not a Dataguard Instance

    if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
           ORA_SID_PREFIX=`echo -e ${ORA_SID} | awk '{print  substr ($1,1,3)}'`
           if [ "${ORA_SID_PREFIX}" != "${BOX_PREFIX}" ] && [ "${EXCEPTION}" == "0" ]; then
              echo "This procedure can not be run against this database - may be a dataguard instance" > ${ErrorFile}
              echo "This procedure can not be run against this database - may be a dataguard instance"
                SendNotification "AnalyzeStructure.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance"
                exit 1
          fi
     fi

        export ORACLE_SID
        export ORACLE_HOME
        export PATH
        export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
        export LD_LIBRARY_PATH=$ORACLE_HOME/lib

    export BACKUPDIR=`grep -i $ORA_SID $HOME/local/dba/backups/hot/hotbackup.ini|awk '{print $2}'`
    if [ x$BACKUPDIR = 'x' ]; then
        export BACKUPDIR=`grep -i 'all' $HOME/local/dba/backups/hot/hotbackup.ini|awk '{print $2}'`
    fi

    export DATAFILE_DIR=$BACKUPDIR/$ORA_SID/datafile_dir 
	export SCRIPTFILE=$LOGDIR/Analyze_${ORA_SID}_${NumericDate}.sh
	# export RESULTSFILE=$LOGDIR/Results_${ORA_SID}_${NumericDate}.txt
    export LOGFILE=$LOGDIR/Analyze_${ORA_SID}_${NumericDate}.log
   
	echo " Starting Analyze Structure of ....  ${ORACLE_SID} on `date +\"%c\"`" 

	# Add Start time entry to DBA Auditing Database
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			select instance from CRON_JOB_RUNS where script_name='${PROGRAM_NAME}' and
			run_date > to_char(sysdate,'DD-MON-YYYY') and lower(sid)='${ORA_SID}';
		exit
EOF`

		Sequence_Number=`echo $Sequence_Number|tr -d " "`
		if [[ x$Sequence_Number = 'x' ]]; then
			typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
				set serveroutput on size 1000000
				set heading off
				set feedback off
				declare i number;
				begin
				insert into CRON_JOB_RUNS values (cron_job_runs_seq.nextval,'${PROGRAM_NAME}','${BOX}','${ORA_SID}', sysdate, sysdate, '','',0,'','','','0') returning instance into i;
				commit;
				dbms_output.put_line (i);
				end;
/
			exit
EOF`
			else
				STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
				set heading off
				set feedback off
				update CRON_JOB_RUNS set run_date=sysdate, start_time=sysdate where instance=${Sequence_Number};
				commit;
/
EOF`
			fi

	fi

	export Sequence_Number

        funct_verify
        Status=$?
        if [[ $Status != 0 ]]; then
                exit 0
        fi
        funct_analyze_struct

	# Check for Errors
    Errors=0
    if test -f ${LOGFILE}
    then
        if test `grep "DBV-" ${LOGFILE} | wc -l` -ne 0
        then
            Errors=1
            grep "ORA-" ${LOGFILE} >> $ErrorFile
        fi

    fi

    # Send errors to DBA
    if test $Errors -eq 1
    then
        SendNotification "Errors found in AnalyzeStructure log for ${ORACLE_SID}\n"
    fi

    cat ${LOGFILE} | while read LINE
    do
        case `grep "Total Pages Failing   (Seg)" $LINE |awk '{print $6}'` in
          "0") CORRUPTION_ERROR=0;;
           *) CORRUPTION_ERROR=1
              exit 1;;
         esac
    done

	# Send errors to DBA
	if [[ ${CORRUPTION_ERROR} = 1 ]]; then
		SendNotification "Errors found in AnalyzeStructure log for ${ORACLE_SID}, look at log file ${LOGFILE}\n"
	fi

	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			update CRON_JOB_RUNS set end_time=sysdate,status='SUCCESS',number_of_runs=number_of_runs+1 where instance=${Sequence_Number};
			commit;

			update CRON_JOB_RUNS set run_time= (select
			trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
			trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':' ||
			trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
			from cron_job_runs where instance=${Sequence_Number})
			where instance=${Sequence_Number};
			commit;
			exit
EOF`
	fi

	# Purge old files
	KEEPDEPTH=60
	OLDFILES=`ls -t ${LOGDIR}/Analyze_${ORACLE_SID}* 2> /dev/null | tail +${KEEPDEPTH}`
	if [ "$OLDFILES" != "" ] ; then
		rm ${OLDFILES}
		echo "Removing old log file(s): \n$OLDFILES";
	fi
	KEEPDEPTH=60
	OLDFILES=`ls -t ${LOGDIR}/Results_${ORACLE_SID}* 2> /dev/null | tail +${KEEPDEPTH}`
	if [ "$OLDFILES" != "" ] ; then
		rm ${OLDFILES}
		echo "Removing old log file(s): \n$OLDFILES";
	fi
	echo " Finished Analyze Structure of ....  ${ORACLE_SID} on `date +\"%c\"`" 
else
        # Run for all databases in oratab
        cat $ORATAB | while read LINE
        do
                case $LINE in
                        \#*)    ;;      # comment-line in oratab

                        \**)            ;;      # ignore home setting

                        *)      ORA_SID=`echo $LINE | awk -F: '{print $1}' `
                                if [[ x$ORA_SID = 'x' ]]; then
                                        continue
                                fi

                                . /usr/bin/setsid.sh $ORA_SID
                                Status=$?
                                if [ $Status != 0 ]; then
                                        echo "Invalid SID -> ${ORA_SID}"
                                         SendNotification "Invalid SID -> ${ORA_SID}"
                                        exit 1
                                fi

				export ErrorFile=${HOME}/local/dba/analyze//logs/ErrorFile_${ORA_SID}

				# Make sure this is not a Dataguard Instance
				if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
					ORA_SID_PREFIX=`echo ${ORA_SID} | awk '{print  substr ($1,1,3)}'`
					if [ "${ORA_SID_PREFIX}" != "${BOX_PREFIX}" ] && [ "${ORA_SID}" != "${SID_EXCEPTION}" ]; then
						echo "AnalyzeStructure.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance" > ${ErrorFile}
						echo "AnalyzeStructure.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance" 
						 SendNotification 
						continue
					fi
				fi

                                export ORACLE_SID
                                export ORACLE_HOME
                                export PATH
                                export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
                                export LD_LIBRARY_PATH=$ORACLE_HOME/lib

                                export BACKUPDIR=`grep -i $ORA_SID $HOME/local/dba/backups/hot/hotbackup.ini|awk '{print $2}'`
                                if [ x$BACKUPDIR = 'x' ]; then
                                        export BACKUPDIR=`grep -i 'all' $HOME/local/dba/backups/hot/hotbackup.ini|awk '{print $2}'`
                                fi
                                echo "\n\n Starting  AnalyzeStructure ....  ${ORA_SID} on `date +\"%c\"`"

				# Add Start time entry to DBA Auditing Database
				if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
					typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
						set heading off
						set feedback off
						select instance from CRON_JOB_RUNS where script_name='${PROGRAM_NAME}' and
						run_date > to_char(sysdate,'DD-MON-YYYY') and lower(sid)='${ORA_SID}';
					exit
EOF`

					Sequence_Number=`echo $Sequence_Number|tr -d " "`
					if [[ x$Sequence_Number = 'x' ]]; then
						typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
							set serveroutput on size 1000000
							set heading off
							set feedback off
							declare i number;
							begin
							insert into CRON_JOB_RUNS values (cron_job_runs_seq.nextval,'${PROGRAM_NAME}','${BOX}','${ORA_SID}', sysdate, sysdate, '','',0,'','','','0') returning instance into i;
							commit;
							dbms_output.put_line (i);
							end;
/
						exit
EOF`
						else
							STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
							set heading off
							set feedback off
							update CRON_JOB_RUNS set run_date=sysdate, start_time=sysdate where instance=${Sequence_Number};
							commit;
/
EOF`
						fi

				fi

				export Sequence_Number

                export DATAFILE_DIR=$BACKUPDIR/$ORACLE_SID/datafile_dir
                export SCRIPTFILE=$LOGDIR/Analyze_${ORACLE_SID}_${NumericDate}.sh
                # export RESULTSFILE=$LOGDIR/Results_${ORACLE_SID}_${NumericDate}.txt
                export LOGFILE=$LOGDIR/Analyze_${ORACLE_SID}_${NumericDate}.log

                funct_verify
				Status=$?
				if [[ $Status != 0 ]]; then
					exit 0
				fi

				funct_analyze_struct
                # Check for Errors

                Errors=0
                if test -f ${LOGFILE}
                then
                   if test `grep "DBV-" ${LOGFILE} | wc -l` -ne 0
                   then
                      Errors=1
                      grep "ORA-" ${LOGFILE} >> $ErrorFile
                   fi
                fi

                # Send errors to DBA
                if test $Errors -eq 1
                then
                     SendNotification "Errors found in AnalyzeStructure log for ${ORACLE_SID}\n"
                fi

			    cat ${LOGFILE} | while read LINE
    				do
        				case `grep "Total Pages Failing   (Seg)" $LINE |awk '{print $6}'` in
         				 "0") CORRUPTION_ERROR=0;;
          			 *) CORRUPTION_ERROR=1
             			 exit 1;;
         				esac
    				done

    			# Send errors to DBA
   				if [[ ${CORRUPTION_ERROR} = 1 ]]; then
        				SendNotification "Errors found in AnalyzeStructure log for ${ORACLE_SID}, look at log file ${LOGFILE}\n"
    			fi


				echo " Finished Analyze Structure of ....  ${ORACLE_SID} on `date +\"%c\"`" 

				if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
					STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
						set heading off
						set feedback off
						update CRON_JOB_RUNS set end_time=sysdate,status='SUCCESS',number_of_runs=number_of_runs+1 where instance=${Sequence_Number};
						commit;

						update CRON_JOB_RUNS set run_time= (select
						trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
						trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':' ||
						trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
						from cron_job_runs where instance=${Sequence_Number})
						where instance=${Sequence_Number};
						commit;
						exit
EOF`
				fi

				# Purge old files
				KEEPDEPTH=60
				OLDFILES=`ls -t ${LOGDIR}/Analyze_${ORACLE_SID}* 2> /dev/null | tail +${KEEPDEPTH}`
				if [ "$OLDFILES" != "" ] ; then
					rm ${OLDFILES}
					echo "Removing old log file(s): \n$OLDFILES";
				fi
				KEEPDEPTH=60
				OLDFILES=`ls -t ${LOGDIR}/Results_${ORACLE_SID}* 2> /dev/null | tail +${KEEPDEPTH}`
				if [ "$OLDFILES" != "" ] ; then
					rm ${OLDFILES}
					echo "Removing old log file(s): \n$OLDFILES";
				fi
				;;
		esac
	done
fi




echo  "AnalyseStructure Completed successfully on `date +\"%c\"`" 

######## END MAIN  #########################

