#!/bin/ksh
# ==================================================================================================
# NAME:         VerifyDataguard.sh
#
# AUTHOR:       Maureen Buotte
#
# PURPOSE:      This utility will check all dataguard instances on a box and make sure a log has 
#		been applied within the last hour.  This will give us advanced notice if there are
#		problems with a dataguard instance.
#
# USAGE:        VerifyDataguard.sh
#
# Changes:
# Frank Davis  05/16/2015  Added compatibility with ASM Managed Databases
# Frank Davis  09/30/2013  Removed team_dba.eagleaccess.com from email
# Frank Davis  09/24/2013  Removed tech_support.eagleaccess.com from email
# Frank Davis  06/14/2013  Change mail to mailx to make compatible with Solaris. This script is now compatible with Solaris and Linux.
# Frank Davis  06/13/2013  Made script compatible with Service Now
# Frank Davis  05/08/2013  Added additional output to alert to tell how many minutes DR is behind Prod
# Frank Davis  12/10/2011  Change purge of archive logs from 3 days to 2 days.
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Nishigandha Sathe     01-Mar-2010 	Modified to use new primary key machines(instance)
# Maureen Buotte    	30-Sep-2009 	Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte	09-Jan-2009	Fixed logic for purging logs
# Maureen Buotte	09-Jan-2009 	Put mim special code in
# Pankaj Gupta		08-Jan-2009 	Added purge of logs on DR box
# Maureen Buotte	05-Aug-2008 	Added serveroutput setting because glogin.sql is not consistent on all database installs
# Sachin Vyahalkar	20-Jul-2008 	Changed archive log apply check to make every 2 hours instead of 1 hour
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat
 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail.dat
  rm $ERROR_FILE
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH ${TYPE} Environment -- ${PROGRAM_NAME} on ${BOX} for ${ORACLE_SID}" ${MAILTO}
 rm $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where name = '${BOX}');
EOF
`
 DB_INSTANCE=$(print $DB_INSTANCE |tr -d " ")
 export DB_INSTANCE
 if [[ -z $DB_INSTANCE ]]
 then
  export IMPACT=2
  export URGENCY=3
  SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
  PERFORM_CRON_STATUS=0
  return 1	
 fi

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ -z $Sequence_Number ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000
  set heading off feedback off
  declare i number;
  begin
   insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off 
 update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;

 update INSTANCE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from instance_cron_job_runs where instance=${Sequence_Number}) where instance=${Sequence_Number};
 commit;
/
EOF
`
 print  "$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS"
}

# --------------------------------------------------------------------------------------
# funct_verify_mounted (): Verify that status of database is MOUNTED
# ---------------------------------------------------------------------------------------
function funct_verify_mounted
{
 typeset -i MOUNTED=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select case when trim(upper(status))='MOUNTED' then 1 else 0 end from v\\$instance;
EOF
`
 MOUNTED=$(print $MOUNTED|tr -d " ")
 if [ $MOUNTED -eq 1 ]
 then
  return 1
 fi
}


# ------------------------------------------------------------------------
# funct_check_last_log(): Determine when the last archive log was applied
# ------------------------------------------------------------------------
function funct_check_last_log
{
 typeset -i LastApply=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 alter session set nls_date_format = 'DD-MON-YYYY HH24:MI:SS';
 select case when t1.next_time is null or t1.next_time < (sysdate - (1/12)) then 0
 else 1 
 end 
 from 
 (select max(next_time) next_time from v\\$archived_log where applied='YES') t1;
EOF
`
 if [ $LastApply -eq 0 ]
 then	
  minutes_since_log_applied=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF
  set heading off feedback off
  select round((sysdate-max(NEXT_TIME))*86400/60) min_since_last_log_applied from v\\$archived_log where applied='YES';
EOF
`
 minutes_since_log_applied=$(print $minutes_since_log_applied |tr -d " ")
 print "$ORACLE_SID DR is $minutes_since_log_applied Minutes behind $ORACLE_SID Production\n" >> $ERROR_FILE
 $WORKING_DIR/check_dr_log_applied.ksh $ORACLE_SID >> $ERROR_FILE
 SendNotification "Last log applied for dataguard instance ${ORA_SID} was more than an hour ago -- This needs to be resolved as soon as possible"
 else
  FullLastFile=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
  set heading off feedback off
  select name from v\\$archived_log where next_time = (select max(next_time) from v\\$archived_log);
EOF
`
  Extension=$(print $FullLastFile | awk -F. '{print $NF}')
  LastFile=$(print $FullLastFile | awk -F/ '{print $NF}')
  FileStart=$(print $LastFile | awk -F_ '{print $1}')
  Location=$(print $FullLastFile | sed 's/'$LastFile//g)
  #find $Location$FileStart*.${Extension} -mtime +1 -exec rm {} \;
sudo -iu grid asmcmd find $Location$FileStart*.${Extension} -mtime +1 -exec rm {} \;
 fi
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
#function funct_check_asm_client
#{
#export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
#set heading off feedback off
#select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
#EOF
#`
#}


function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(instance_name)='+ASM';
EOF
`
}


# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
 set -x

export PROGRAM_NAME=$(print  $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export DB_INSTANCE 

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 AWK=awk
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 AWK=nawk
fi

export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/dataguard
else
 export WORKING_DIR=/u01/app/oracle/local/dba/dataguard
fi

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt
export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

# Create file with all databases currently running on this box
ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}' > $DATABASES_FILE

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
  print $PERFORM_CRON_STATUS
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g')
 fi
 LINE=$(grep "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

export TYPE=PROD
export IMPACT=1
export URGENCY=1
export MAILTO='eaglecsi@eagleaccess.com'

NumericDate=$(date +%Y%m%d)

# Loop through the list of Oracle instances to run against
cat $DATABASES_FILE | while read LINE
do
 Sequence_Number=0
 ORA_SID=$LINE
 if [[ -z $ORA_SID ]]
 then
  continue
 fi

 export MAIL_COUNT=0

 ErrorFile=$WORKING_DIR/ErrorFile_$ORA_SID
 rm -f $ErrorFile

 export ORACLE_SID=$ORA_SID
 export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
 . /usr/local/bin/oraenv > /dev/null
 if [ $? -ne 0 ]
 then
  print "Invalid SID -> $ORA_SID"
  export IMPACT=2
  export URGENCY=3
  SendNotification "Invalid SID -> ${ORA_SID}"
  continue
 fi

 export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
 export LD_LIBRARY_PATH=$ORACLE_HOME/lib

 # Make sure this database is not in list of SIDs that nothing should be run against
 EXCEPTION=$($AWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID" 'BEGIN{print index(a,b)}')
 if [ "$EXCEPTION" -ne 0 ]
 then
  continue
 fi  

 # Make sure this database is not in list of SIDs that this job should not be run for 
 EXCEPTION=$($AWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID" 'BEGIN{print index(a,b)}')
 if [ "$EXCEPTION" -ne 0 ]
 then
  continue
 fi  

 # Make sure database is in MOUNTED state if not, skip to the next database
 funct_verify_mounted
 if [ $? -eq 0 ]
 then
  continue
 fi

 # If the infprd2 database is available make sure this is a DR instance and not just a database in a MOUNT state during a refresh
 if [ $PERFORM_CRON_STATUS -eq 1 ]
 then
  TRUE_DATAGUARD=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  select count(*) from databases where sid='${ORACLE_SID}' and dataguard='Y' and mac_instance = (select instance from machines where name='${BOX}');
EOF
`
  TRUE_DATAGUARD=$(print $TRUE_DATAGUARD|tr -d " ")
  if [ $TRUE_DATAGUARD -ne 1 ]
  then
   continue
  fi
 fi

# Add entry into DBA Admin database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 funct_initial_audit_update
 if [ $? -ne 0 ]
 then
  PERFORM_CRON_STATUS=0
 fi
 print $DB_INSTANCE
 export DB_INSTANCE
fi

# Make sure history directory exists
if [[ ! -d $WORKING_DIR/history ]]
then
 mkdir -p ${WORKING_DIR}/history
fi 
			
funct_check_last_log

# Update Infrastructure database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 if [ $MAIL_COUNT -gt 0 ]
 then
  PROCESS_STATUS='WARNING'
 else
  PROCESS_STATUS='SUCCESS'
 fi
 funct_final_audit_update
fi

done

rm -f $DATABASES_FILE $ERROR_FILE

print "\nFinished Checking all Databases \n"
exit 0
