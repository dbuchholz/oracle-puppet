#!/bin/ksh

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of the standby database."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi

ORACLE_SID=$1
db_version=$(grep $ORACLE_SID $ORATAB | awk -F/ '{print $6}' | awk -F. '{print $1}')
if [ $db_version -lt 11 ]
then
 alert_log=/u01/app/oracle/admin/$ORACLE_SID/bdump/alert*
else
 alert_log=/u01/app/oracle/diag/rdbms/$ORACLE_SID/$ORACLE_SID/trace/alert*
fi

ls -1t $alert_log > $log_dir/alert_file_list.txt
while read file_name
do
 grep 'Media Recovery Log ' $file_name > /dev/null
 if [ $? -eq 0 ]
 then
  break
 fi
done<$log_dir/alert_file_list.txt

sed '1!G;h;$!d' $file_name > $log_dir/basdfg.log
awk  '/Media Recovery Log /{print $0;getline;print $0}' $log_dir/basdfg.log | head -2 > $log_dir/casdfg.log
archive_log=$(head -1 $log_dir/casdfg.log)
apply_timestamp=$(tail -1 $log_dir/casdfg.log)
rm -f $log_dir/basdfg.log $log_dir/casdfg.log
second_of_minute=$(print $apply_timestamp | awk '{print $4}' | awk -F: '{print $NF}')
minute_of_hour=$(print $apply_timestamp | awk '{print $4}' | awk -F: '{print $2}')
hour_of_day=$(print $apply_timestamp | awk '{print $4}' | awk -F: '{print $1}')
day_of_month=$(print $apply_timestamp | awk '{print $3}')
month_of_year=$(print $apply_timestamp | awk '{print $2}')
case $month_of_year in
Jan) the_month=0;;
Feb) the_month=1;;
Mar) the_month=2;;
Apr) the_month=3;;
May) the_month=4;;
Jun) the_month=5;;
Jul) the_month=6;;
Aug) the_month=7;;
Sep) the_month=8;;
Oct) the_month=9;;
Nov) the_month=10;;
Dec) the_month=11;;
esac
the_year=$(print $apply_timestamp | awk '{print $NF}')
how_long_ago=$($script_dir/check_dr_log_applied.pl $second_of_minute $minute_of_hour $hour_of_day $day_of_month $the_month $the_year)
time_since_log_applied=$(print $how_long_ago | sed 's/ //g')
print "Last archive log applied to DR was $time_since_log_applied minutes ago."
exit 0
