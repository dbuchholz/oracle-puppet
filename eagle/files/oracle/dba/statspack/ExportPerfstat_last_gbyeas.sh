#!/usr/bin/ksh
# ==================================================================================================
# NAME:     ExportPerfstat.sh
#
# AUTHOR:   Maureen Buotte
#
# PURPOSE:  This utility exports the data from the perfstat user and then purges the perfstat  
#           user.  This will keep the database statistics data from growing too large.  The  
#           exported data can be imported to a test area if historical information is required.
#
# USAGE:    ExportPerfstat.sh  ORACLE_SID
#
# Maureen Buotte    17-Jan-2009 Allow for more than 1 SID Exception, change echo -e -e to echo -e -e, fix tail + with tail -n
# Maureen Buotte    5-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    1-Jun-2008  Added reporting to DBA database
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

        # Uncomment for debug
        # set -x

        Machine=`uname -a | awk '{print $2}'`
        # Add any additional information to mail message
        if [[ x$1 != 'x' ]]; then
                echo -e "\n\n$1\n" >> mail.dat
        fi

        cat mail.dat | mail -s "${TYPE} Environment -- ${PROGRAM_NAME} failure on $Machine " ${MAILTO}
        rm mail.dat

        # Update mail sent indicator in DBA Admin Database record
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		if [[ ${Sequence_Number} -ne 0 ]]; then
			# Update mail sent indicator in DBA Admin Database record
			STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			update CRON_JOB_RUNS set mail_sent=mail_sent+1 where instance=${Sequence_Number};
			commit;
			exit
EOF`
		fi
	fi

        return 0
}
#
# --------------------------------------------------
# funct_check_parm(): Check for input parameters
# --------------------------------------------------
funct_check_parm() {
        # Uncomment next line for debugging
        # set -x

	if [ ${NARG} -ne 1 ]; then
		echo -e "ExportPerfstat.sh FAIL: Incorrect number of arguments passed - Must pass SID"
		exit 1
	fi
}

# --------------------------------------------------------------
# funct_check_installation(): Make sure statspack is installed
# --------------------------------------------------------------
funct_check_installation() {
	# Uncomment next line for debugging
	# set -x

	typeset -i PerfstatUser=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select count(*) from dba_users where username='PERFSTAT';
		exit
EOF`

	if [[ $PerfstatUser -eq 0 ]]; then
		echo -e "Oracle SID $ORACLE_SID does not have statspack installed. Can't analyze statistics"
		SendNotification "Oracle SID $ORACLE_SID does not have statspack installed. Can't analyze statistics"
		exit 1
	fi
}

# ----------------------------------------------
# funct_export_perfstat(): Export PERFSTAT data
#	- use $ORACLE_HOME/rdbms/admin/spuexp.par
# ----------------------------------------------
funct_export_perfstat() {
	# Uncomment next line for debugging
	# set -x

	echo -e " Exporting PERFSTAT ....  ${ORACLE_SID} on `date +\"%c\"`"
	exp perfstat/perfstat@${ORACLE_SID} file=${ExportFile}.dmp log=${ExportFile}.log \
		compress=y grants=y indexes=y rows=y constraints=y owner=PERFSTAT consistent=y
        if [ $? != 0 ]; then
                echo -e "Unable to perform export of PERFSTAT for $ORACLE_SID. Will not truncate data "
                SendNotification "Unable to perform export of PERFSTAT for $ORACLE_SID. Will not truncate data "
                exit 1
        fi

	compress -vf ${ExportFile}.dmp
}

# --------------------------------------------------
# funct_truncate_perfstat(): Truncate PERFSTAT data
# --------------------------------------------------
funct_truncate_perfstat() {
	# Uncomment next line for debugging
	# set -x

	echo -e " Truncating PERFSTAT ....  ${ORACLE_SID} on `date +\"%c\"`"
	Status=`${ORACLE_HOME}/bin/sqlplus -s perfstat/perfstat@${ORACLE_SID} <<EOF
		set heading off
		set feedback off
		@${ORACLE_HOME}/rdbms/admin/sptrunc.sql

		exit
EOF`
}

# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
# set -x

NARG=$#
funct_check_parm

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export Sequence_Number=0
export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/CronStatus.ini
if [[ -a ${PARFILE} ]]; then
        LINE=`head -1 ${PARFILE}`
        if [[ -n $LINE ]]; then
                export PERFORM_CRON_STATUS=1
                export CRON_SID=`echo -e $LINE | awk '{print $1}' `
                export CRON_USER=`echo -e $LINE | awk '{print $2}' `
                CRON_CONNECT=`echo -e $LINE | awk '{print $3}' `
                export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
                export BOX_PREFIX=`echo -e $LINE | awk '{print $4}' `
                export SID_EXCEPTION=`echo -e $LINE | awk '{print $5}' `
        fi
fi

if [ x$SID_EXCEPTION = 'x' ]; then
        export SID_EXCEPTION=NONE
fi


IPCheck=`/sbin/ifconfig -a | egrep '10.170.1|10.60.105|10.60.103' | awk '{print $1}'`
if [ x$IPCheck = 'x' ]; then
        export TYPE=PROD
        export MAILTO=tech_support@eagleaccess.com
else
        export TYPE=TEST
        export MAILTO=team_dba@eagleaccess.com
fi

ORATAB=/etc/oratab
ORA_SID=$1
. /usr/bin/setsid.sh $ORA_SID
Status=$?
if [ $Status != 0 ]; then
        echo -e "Invalid SID -> ${ORA_SID}"
        SendNotification "Invalid SID -> ${ORA_SID}"
        exit 1
fi

# See if SID running for is in exception list
EXCEPTION=`awk -v a="$SID_EXCEPTION" -v b="$ORA_SID" 'BEGIN{print index(a,b)}' `

# Make sure this is not a Dataguard Instance
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	ORA_SID_PREFIX=`echo -e ${ORA_SID} | awk '{print  substr ($1,1,3)}'`
	if [ "${ORA_SID_PREFIX}" != "${BOX_PREFIX}" ] && [ "${EXCEPTION}" == "0" ]; then
		echo -e "ExportPerfstat.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance"
		SendNotification "ExportPerfstat.sh will not run for ${ORA_SID} -- ${ORA_SID} may be a dataguard instance"
		exit 1
	fi
fi

# Add entry into DBA Admin database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from CRON_JOB_RUNS where script_name='${PROGRAM_NAME}' and
		run_date > to_char(sysdate,'DD-MON-YYYY') and lower(sid)='${ORA_SID}';
	exit
EOF`

	Sequence_Number=`echo -e $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		typeset Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
            set serveroutput on size 1000000
			set heading off
			set feedback off
			declare i number;
			begin
			insert into CRON_JOB_RUNS values (cron_job_runs_seq.nextval,'${PROGRAM_NAME}','${BOX}','${ORA_SID}', sysdate, sysdate, '','',0,'','','','0') returning instance into i;
			commit;
			dbms_output.put_line (i);
			end;
/
		exit
EOF`
		else
			STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			update CRON_JOB_RUNS set run_date=sysdate, start_time=sysdate where instance=${Sequence_Number};
		commit;
/
EOF`
	fi
	export Sequence_Number
fi


export WorkingDir=${ORACLE_BASE}/local/dba/statspack/exports

CurrentDate=$(date +%Y%m%d)
export ExportFile=${WorkingDir}/Export_${ORACLE_SID}_${CurrentDate}

echo -e " Starting perfstat export/purge ....  ${ORACLE_SID} on `date +\"%c\"`"

funct_check_installation
funct_export_perfstat
funct_truncate_perfstat

# Purge files older than 30 days
OLDFILES=`find ${WorkingDir} -mtime +30`
if [ "$OLDFILES" != "" ] ; then
    rm ${OLDFILES}
    echo -e "Removing old export file(s): \n$OLDFILES";
fi

# Update DBA Admin datbase record with status
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update CRON_JOB_RUNS set end_time=sysdate, status='SUCCESS',number_of_runs=number_of_runs+1
		where instance=${Sequence_Number};
		commit;

		update CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':' ||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09')) 
		from cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
		exit
EOF`
fi


echo -e " Finished perfstat export/purge ....  ${ORACLE_SID} on `date +\"%c\"`"

######## END MAIN  #########################
