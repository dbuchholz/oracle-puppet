#!/usr/bin/ksh
# ==================================================================================================
# NAME:     ExportPerfstat.sh
#
# AUTHOR:   Maureen Buotte
#
# PURPOSE:  This utility exports the data from the perfstat user and then purges the perfstat  
#           user.  This will keep the database statistics data from growing too large.  The  
#           exported data can be imported to a test area if historical information is required.
#
# USAGE:    ExportPerfstat.sh {Full path of location to put export dump file}
#
# Frank Davis  09/24/2013  Removed tech_support@eagleaccess.com from email
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis       24-Jun-2011  Modified IPCheck Variable to include updated list of VLANs
# Frank Davis       19-Jan-2011  Changed backup location from /u01 to /backups
# Frank Davis       19-Jan-2011  Changed compress to gzip as compress does not exist on Linux
# Frank Davis       19-Jan-2011  When removing old files add the "-type f" to prevent error on attempted removal of directories
# Frank Davis       20-Jan-2011  Added a parameter to pass into script for export file dump location
# Nishigandha Sathe 02-Mar-2010  Modified to use new primary key machines(instance)
# Maureen Buotte    17-Jan-2009  Allow for more than 1 SID Exception, change echo -e -e to echo -e -e, fix tail + with tail -n
# Maureen Buotte    05-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    01-Jun-2008  Added reporting to DBA database
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

	# Uncomment for debug
	 set -x

	echo -e "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat
	if [[ x$1 != 'x' ]]; then
		echo -e "\n$1\n" >> mail.dat
	fi

	if [[ -a ${ERROR_FILE} ]]; then
		cat $ERROR_FILE >> mail.dat
		rm ${ERROR_FILE}
	fi

	if [[ $2 = 'FATAL' ]]; then
		echo "*** This is a FATAL ERROR - ${PROGRAM_NAME} aborted at this point *** " >> mail.dat
	fi

	cat mail.dat | /bin/mail -s "PROBLEM WITH ${TYPE} Environment -- ${PROGRAM_NAME} on ${BOX} for ${ORACLE_SID}" ${MAILTO}
	rm mail.dat

	# Update the mail counter
	MAIL_COUNT=${MAIL_COUNT}+1

	return 0
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging
     set -x

	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=`echo -e $PERFORM_CRON_STATUS|tr -d " "`
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
		PERFORM_CRON_STATUS=0
	fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {

	# Uncomment for debug
	 set -x

	DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
		exit
EOF`
	DB_INSTANCE=`echo -e $DB_INSTANCE |tr -d " "`
	export DB_INSTANCE
	if [ "x$DB_INSTANCE" == "x" ]; then
		SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
        PERFORM_CRON_STATUS=0
		return 1	
	fi

	Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
		start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
		exit
EOF`
	Sequence_Number=`echo $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set serveroutput on size 1000000
		set heading off
		set feedback off
		declare i number;
		begin
		insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
		commit;
		dbms_output.put_line (i);
		end;
/
		exit
EOF`
	else
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
		commit;
EOF`

	fi
	export Sequence_Number
	return 0

}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

	# Uncomment for debug
	 set -x

    # Update DBA Auditing Database with end time and backup size
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off 
		set feedback off 
		update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
		commit;

		update INSTANCE_CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
		from instance_cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
/
		exit
EOF`

    echo  "$PROGRAM_NAME Completed `date +\"%c\"`with a status of ${PROCESS_STATUS} "
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open {

	# Uncomment next line for debugging
	 set -x

    typeset -i OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
		exit
EOF`

    OPEN=`echo -e $OPEN|tr -d " "`

	if [[ ${OPEN} == 1 ]]; then
		return 1
	else
		return 0
	fi
}

# --------------------------------------------------------------
# funct_check_installation(): Make sure statspack is installed
# --------------------------------------------------------------
funct_check_installation() {
	# Uncomment next line for debugging
	# set -x

	typeset -i PerfstatUser=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select count(*) from dba_users where username='PERFSTAT';
		exit
EOF`

	if [[ $PerfstatUser -eq 0 ]]; then
		echo -e "Oracle SID $ORACLE_SID does not have statspack installed. Can't analyze statistics"
		SendNotification "Oracle SID $ORACLE_SID does not have statspack installed. Can't analyze statistics"
		exit 1
	fi
}

# ----------------------------------------------
# funct_export_perfstat(): Export PERFSTAT data
#	- use $ORACLE_HOME/rdbms/admin/spuexp.par
# ----------------------------------------------
funct_export_perfstat() {
	# Uncomment next line for debugging
	# set -x

	echo -e " Exporting PERFSTAT ....  ${ORACLE_SID} on `date +\"%c\"`"
	exp perfstat/perfstat@${ORACLE_SID} file=${ExportFile}.dmp log=${ExportFile}.log \
		compress=y grants=y indexes=y rows=y constraints=y owner=PERFSTAT consistent=y
        if [ $? != 0 ]; then
                echo -e "Unable to perform export of PERFSTAT for $ORACLE_SID. Will not truncate data "
                SendNotification "Unable to perform export of PERFSTAT for $ORACLE_SID. Will not truncate data "
                exit 1
        fi

	gzip -f ${ExportFile}.dmp
}

# --------------------------------------------------
# funct_truncate_perfstat(): Truncate PERFSTAT data
# --------------------------------------------------
funct_truncate_perfstat() {
	# Uncomment next line for debugging
	# set -x

	echo -e " Truncating PERFSTAT ....  ${ORACLE_SID} on `date +\"%c\"`"
	Status=`${ORACLE_HOME}/bin/sqlplus -s perfstat/perfstat@${ORACLE_SID} <<EOF
		set heading off
		set feedback off
		@${ORACLE_HOME}/rdbms/admin/sptrunc.sql

		exit
EOF`
}

# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
# set -x

HOSTNAME=`hostname`
IPCheck=`cat /etc/hosts | grep ${HOSTNAME} | awk '{print $1}' | egrep '10\.60\.5\.|10\.60\.3\.|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
    export TYPE=PROD
else
    export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com

WORKING_DIR=$1
if [ $# -ne 1 ]
then
 print "Must pass the location of the where to put the export dump file."
 print "Use full path for location."
 print "$0 {Full path of location to place export dump file}"
 SendNotification "$HOSTNAME Invalid parameter: $0 {Full path of location to place export dump file}"
 exit 1
fi

# Make sure exports directory exists
if [[ ! -d $WORKING_DIR ]]; then
 mkdir -p $WORKING_DIR > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "Cannot create Directory: $WORKING_DIR"
  SendNotification "$HOSTNAME Invalid parameter: $0 Cannot create the directory to place the export dump file: $WORKING_DIR"
  exit 2
 fi
fi 

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export PROGRAM_NAME_FIRST=`echo ${PROGRAM_NAME} | awk -F "." '{print $1}'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export DB_INSTANCE 

export ORACLE_BASE=$HOME
export ORATAB_LOC="/etc"
export DATABASES_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_databases.txt
export ERROR_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_error.txt

# Check if CRONLOG directory exists
if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
    mkdir -p ${ORACLE_BASE}/CRONLOG
fi


# Get environment variables set up - need to do this differently for RHEL 5
dummy_sid=`cat ${ORATAB_LOC}/oratab | grep ${ORACLE_BASE} | grep -v "^#" | grep -v "^*" | awk -F: '{print $1}' | tail -1`
. /usr/bin/setsid.sh $dummy_sid

# Create file with all databases currently running on this box
ps -ef | grep ora_smon_ | grep -v grep | awk -F_ '{print $NF}' > $DATABASES_FILE

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a ${PARFILE} ]]; then
	STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
	LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
	if [[ -n $LINE ]]; then
		INFO=`echo $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g' `
		export PERFORM_CRON_STATUS=1
		export CRON_SID=`echo -e $INFO | awk '{print $1}' `
		export CRON_USER=`echo -e $INFO | awk '{print $2}' `
		CRON_CONNECT=`echo -e $INFO | awk '{print $3}' `
		export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
		# Make sure you can get to the infrastructure database
		funct_check_inf_database
		echo $PERFORM_CRON_STATUS
	fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_ANYTHING=`echo $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
	fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "${PROGRAM_NAME}:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_THIS_JOB=`echo $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
    fi
fi
rm -f ${NO_COMMENT_PARFILE}


NumericDate=`date +%Y%m%d`

# Loop through the list of Oracle instances to run against
cat ${DATABASES_FILE} | while read LINE
do

    Sequence_Number=0
    ORA_SID=$LINE
    if [ x$ORA_SID = 'x' ]; then
        continue
    fi

    export MAIL_COUNT=0

    ErrorFile=${WORKING_DIR}/ErrorFile_${ORA_SID}
    rm -f ${ErrorFile}

    . /usr/bin/setsid.sh $ORA_SID
    if [ $? -ne 0 ]; then
	 print "Invalid SID -> ${ORA_SID}"
	 SendNotification "Invalid SID -> ${ORA_SID}"
	 continue
    fi

	export ORACLE_SID
	export ORACLE_HOME
	export PATH
	export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
	export LD_LIBRARY_PATH=$ORACLE_HOME/lib

    # Make sure this database is not in list of SIDs that nothing should be run against
    EXCEPTION=`awk -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
    if [[ "${EXCEPTION}" != "0" ]]; then
        continue
    fi

    # Make sure this database is not in list of SIDs that this job should not be run for
    EXCEPTION=`awk -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
    if [[ "${EXCEPTION}" != "0" ]]; then
        continue
    fi

    # Make sure database is 9i, statspack isn't used with 10g and above
    ORAVER=$(print $ORACLE_HOME | grep 9\.2)
    if [[ x$ORAVER = 'x' ]]; then
        continue
    fi

	# Make sure database is OPEN if not, skip to the next database
	funct_verify_open
	if [ $? -eq  0 ]; then
		continue
	fi

	# Add entry into DBA Admin database
    if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
        funct_initial_audit_update
		if [ $? -ne 0 ]; then
			PERFORM_CRON_STATUS=0
		fi
		echo $DB_INSTANCE
		export DB_INSTANCE
    fi


	CurrentDate=$(date +%Y%m%d)
	export ExportFile=$WORKING_DIR/Export_${ORACLE_SID}_${CurrentDate}

	echo -e " Starting perfstat export/purge ....  ${ORACLE_SID} on `date +\"%c\"`"

	funct_check_installation
	funct_export_perfstat
	funct_truncate_perfstat

	# Update Infrastructure database
    if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		if [[ ${MAIL_COUNT} > 0 ]]; then
			PROCESS_STATUS='WARNING'
		else
			PROCESS_STATUS='SUCCESS'
		fi
        funct_final_audit_update
    fi

	# Purge files older than 30 days
    OLDFILES=$(find $WORKING_DIR -type f \( -name "Export_*.dmp.gz" -o -name "Export_*.log" \) -mtime +30)
	if [ "$OLDFILES" != "" ] ; then
		rm ${OLDFILES}
		echo -e "Removing old export file(s): \n$OLDFILES";
	fi

	echo -e " Finished perfstat export/purge ....  ${ORACLE_SID} on `date +\"%c\"`"

done

rm -f ${DATABASES_FILE}
rm -f ${ERROR_FILE}

echo -e "\n${PROGRAM_NAME} Finished Checking all Databases \n"


######## END MAIN  #########################
exit 0
