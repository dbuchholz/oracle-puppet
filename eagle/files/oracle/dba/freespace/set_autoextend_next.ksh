#!/bin/ksh
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
export log_dir=$script_dir/logs

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of the database you want to check tablespace free space."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi

export ORACLE_SID=$1

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set lines 200 feedback off trimspool on pages 0
col sum(bytes) format 999999999
spool $script_dir/tablespace_sizes_${ORACLE_SID}.txt
select tablespace_name,sum(bytes)/1024/1024 from dba_data_files group by tablespace_name;
EOF
sed '/^$/d' $script_dir/tablespace_sizes_${ORACLE_SID}.txt > $script_dir/tlbsizes_${ORACLE_SID}.txt
mv $script_dir/tlbsizes_${ORACLE_SID}.txt $script_dir/tablespace_sizes_${ORACLE_SID}.txt

print "set echo on" > $script_dir/set_autoextend_and_increment_${ORACLE_SID}.txt
while read tablespace_name size
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$script_dir/set_autoextend_and_increment_${ORACLE_SID}.txt
set lines 200 feedback off trimspool on pages 0
SELECT 
case
when $size < 32768 then 'ALTER DATABASE DATAFILE '''||file_name||''' AUTOEXTEND ON NEXT 512M MAXSIZE UNLIMITED;' 
when $size between 32768 and 65534 then 'ALTER DATABASE DATAFILE '''||file_name||''' AUTOEXTEND ON NEXT 1024M MAXSIZE UNLIMITED;' 
when $size between 65535 and 131068 then 'ALTER DATABASE DATAFILE '''||file_name||''' AUTOEXTEND ON NEXT 1536M MAXSIZE UNLIMITED;' 
when $size > 131068 then 'ALTER DATABASE DATAFILE '''||file_name||''' AUTOEXTEND ON NEXT 2048M MAXSIZE UNLIMITED;' 
end
FROM DBA_DATA_FILES 
WHERE tablespace_name='$tablespace_name';
EOF
done<$script_dir/tablespace_sizes_${ORACLE_SID}.txt

$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
set echo on
@$script_dir/set_autoextend_and_increment_${ORACLE_SID}.txt
EOF

rm $script_dir/tablespace_sizes_${ORACLE_SID}.txt $script_dir/set_autoextend_and_increment_${ORACLE_SID}.txt
exit 0
