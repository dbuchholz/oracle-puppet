#!/bin/ksh
# ==================================================================================================
# NAME:         check_free_space.ksh
#
# AUTHOR:       Frank Davis
#
# PURPOSE:      This script will check the free space in all the tablespaces for all instances on this server.
#
# USAGE:        check_free_space.ksh
#
# Nirmal Arri  07/30/2015  Disable the alert that check is the spare DISK is avalable.
# Frank Davis  05/16/2015  Made compatible with ASM Managed Databases
# Frank Davis  05/27/2014  Changed mail to mailx so it is compatible with both Linux and Solaris.
# Frank Davis  09/30/2013  Removed team_dba.eagleaccess.com from email list
# Frank Davis  09/24/2013  Removed tech_support.eagleaccess.com from email list
# Frank Davis  06/13/2013  Made script compatible with Service Now and compatible with Linux and Solaris.
# Frank Davis  04/20/2012  Incorporated Tablespace Sizes to Big Brother tablespaces Table
# Frank Davis  02/21/2012  Initial Development
# Frank Davis  03/27/2012  Initial Implementation
# ==================================================================================================
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat

 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail.dat
  rm -f $ERROR_FILE
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off feedback off
        select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [[ $PERFORM_CRON_STATUS != 1 ]]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
 set pagesize 0 echo off trimspool on
 WHENEVER SQLERROR EXIT FAILURE
 spool $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt
 select instance from databases where sid='$ORACLE_SID' and mac_instance = (select instance from machines where lower(name)='$BOX');
EOF
 grep 'no rows selected' $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt > /dev/null
 if [ $? -eq 0 ]
 then
  rm -f $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt
  IMPACT=2
  URGENCY=3
  SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated"
  PERFORM_CRON_STATUS=0
  return 1
 fi
 DB_INSTANCE=$(sed -e '/^$/d' -e 's/ //g' $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt)
 rm -f $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ x$Sequence_Number = 'x' ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000 heading off feedback off
  declare i number;
  begin
   insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;

 update INSTANCE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from instance_cron_job_runs where instance=${Sequence_Number})
 where instance=${Sequence_Number};
 commit;
/
EOF
`
 print "\n$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS for $ORACLE_SID"
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open {

 typeset -i OPEN=`$ORACLE_HOME/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
EOF
`

OPEN=`print $OPEN|tr -d " "`

if [ $OPEN = 1 ]
then
 return 0
else
 return 1
fi
}

# ------------------------------------------------------------
# funct_get_freespace(): Get the freespace from the database
# ------------------------------------------------------------
function funct_get_freespace
{
 #set -x

 status=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set pagesize 999 linesize 100 feedback off
 column tablespace format A15
 spool ${FreespaceFile}
 select t.tablespace,  t.totalspace as " Totalspace(MB)",
 round((t.totalspace-fs.freespace),2) as "Used Space(MB)",
 fs.freespace as "Freespace(MB)",
 round(((t.totalspace-fs.freespace)/t.totalspace)*100,2) as "% Used",
 round((fs.freespace/t.totalspace)*100,2) as "% Free"
 from
 (select round(sum(d.bytes)/(1024*1024)) as totalspace, d.tablespace_name tablespace
 from dba_data_files d
 group by d.tablespace_name) t,
 (select round(sum(f.bytes)/(1024*1024)) as freespace, f.tablespace_name tablespace
 from dba_free_space f
 group by f.tablespace_name) fs
 where t.tablespace=fs.tablespace
 order by t.tablespace;
 spool off
 set feedback off pagesize 999 linesize 132 heading off
 spool ${InsertFile}
 select 'Insert into tablespaces values (' || ${DB_INSTANCE} || ','''|| sysdate || ''',''' || t.tablespace || ''',' || t.totalspace || ',' || round((t.totalspace-fs.freespace),2) || ');'
 from
 (select round(sum(d.bytes)/(1024*1024)) as totalspace, d.tablespace_name tablespace
 from dba_data_files d
 group by d.tablespace_name) t,
 (select round(sum(f.bytes)/(1024*1024)) as freespace, f.tablespace_name tablespace
 from dba_free_space f
 group by f.tablespace_name) fs
 where t.tablespace=fs.tablespace
 order by t.tablespace;
 spool off
EOF
`

 # Insert the space info into the DBA database
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 @$InsertFile
 commit;
 insert into tablespaces select db_instance, report_date, 'TOTAL', sum(Space_Allocated_M), sum(Space_Used_m)
 from tablespaces where db_instance=${DB_INSTANCE} and report_date = to_char(sysdate,'DD-MON-YYYY')
 group by db_instance,report_date;
 commit;
EOF
}

### MAIN #####
ostype=$(uname)
if [ $ostype = Linux ]
then
 export ORATAB_LOC=/etc
 ORATAB=$ORATAB_LOC/oratab
 export DF_COMMAND="df -Pk"
 NAWK=awk
fi
if [ $ostype = SunOS ]
then
 export ORATAB_LOC=/var/opt/oracle
 ORATAB=$ORATAB_LOC/oratab
 export DF_COMMAND="df -lk"
 NAWK=nawk
fi

export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/freespace
else
 export WORKING_DIR="$HOME/local/dba/freespace"
fi

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

candidate_sids=$(ps -ef | grep "ora_smon_" | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
if [[ -z $candidate_sids ]]
then
 exit 0
fi

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE

export PERFORM_CRON_STATUS=0
if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep  "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(grep  "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

IPCheck=$(grep $BOX /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=1
 export URGENCY=2
else
 export TYPE=TEST
 export IMPACT=1
 export URGENCY=3
fi

## MAILTO Settings....
#export MAILTO='narri@eagleinvsys.com'
export MAILTO='eaglecsi@eagleaccess.com'

# Add entry into DBA Admin database
 if [ $PERFORM_CRON_STATUS = 1 ]
 then
  funct_initial_audit_update
  if [ $? -ne 0 ]
  then
   PERFORM_CRON_STATUS=0
  fi
  export DB_INSTANCE
 fi

ERROR_FILE=$WORKING_DIR/check_free_space_error_file.txt
rm -f $ERROR_FILE
if [ $ASM_CLIENT = Y ]
then
 #----------------------------------------------------------------
 # This section is for ASM
 # Adjust thresholds here:
  asm_tablespace_percent_threshold=70
  disk_group_used_threshold=75
  disk_group_available_threshold=500  # Value in Gigabytes
 #----------------------------------------------------------------

 asm_instance=$(ps -ef | grep asm_smon_+ASM |grep -v grep|awk '{print $NF}'|awk -F_ '{print $NF}')
 export ORACLE_SID=$asm_instance
 export ORAENV_ASK=NO
 . /usr/local/bin/oraenv > /dev/null

 HOLD_ERROR_FILE=$WORKING_DIR/asm_error_file2.txt
 TEMP_ERROR_FILE=$WORKING_DIR/asm_error_file3.txt
 rm -f $HOLD_ERROR_FILE $TEMP_ERROR_FILE

 $ORACLE_HOME/bin/sqlplus / as sysdba<<EOF>$WORKING_DIR/$$_sqlplus_banner.txt
 WHENEVER SQLERROR EXIT FAILURE
EOF
 if [ $? -ne 0 ]
 then
  print "\nCould not reach the ASM Database."
  print "Exiting here.\n"
  exit 5
 fi
 ora_version=$(grep '^SQL\*Plus: ' $WORKING_DIR/$$_sqlplus_banner.txt | awk '{print $3}' | awk -F. '{print $1}')

 $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
 WHENEVER SQLERROR EXIT FAILURE
 set pages 999 feedback off
 col capacity format 999999999 heading "Capacity(%)"
 col name format a15 heading "Disk Group"
 col "Total MB" format 999999999
 col "Used MB" format 999999999
 spool $WORKING_DIR/$$_diskgroup_info.txt
 select name,total_mb "Total MB",cold_used_mb "Used MB",cold_used_mb/total_mb*100 capacity
 from v\$asm_diskgroup
 where total_mb>0
 order by capacity desc;
EOF
 if [ $? -ne 0 ]
 then
  print "\nCould not reach the ASM Database."
  print "Exiting here.\n"
  exit 6
 fi
 DATA_NAME=$(grep '^DATA' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $1}')
 DATA_PCT=$(grep '^DATA' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $NF}')
 FRA_NAME=$(grep '^FRA' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $1}')
 FRA_PCT=$(grep '^FRA' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $NF}')
 RED_NAME=$(grep '^RED' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $1}')
 RED_PCT=$(grep '^RED' $WORKING_DIR/$$_diskgroup_info.txt | awk '{print $NF}')
 rm -f $ERROR_FILE
 hit_capacity=0
 if [ $DATA_PCT -ge $disk_group_used_threshold ]
 then
  print "\n$DATA_NAME Diskgroup has reached $DATA_PCT Percent Capacity.\n" >> $ERROR_FILE
  hit_capacity=1
 fi
 if [ $FRA_PCT -ge $disk_group_used_threshold ]
 then
  print "\n$FRA_NAME Diskgroup has reached $FRA_PCT Percent Capacity.\n" >> $ERROR_FILE
  hit_capacity=1
 fi
 if [ $RED_PCT -ge $disk_group_used_threshold ]
 then
  print "\n$RED_NAME Diskgroup has reached $RED_PCT Percent Capacity.\n" >> $ERROR_FILE
  hit_capacity=1
 fi

 # For Oracle 11g and newer it uses os_mb, for Oracle 10g and older it uses total_mb
 if [ $ora_version -ge 11 ]
 then
  $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
  WHENEVER SQLERROR EXIT FAILURE
  set pages 999 feedback off
  col label format a15
  col header_status format a15
  col path format a15
  col "Disk(GB)" format 9999
  set feedback on
  spool $WORKING_DIR/$$_rows_found.txt
  select rownum "Line Number",label,header_status,path,os_mb/1000 "Disk(GB)" from v\$asm_disk where header_status in  ('PROVISIONED','FORMER') order by rownum;
EOF
  if [ $? -ne 0 ]
  then
   print "\nThere was an error when querying ASM Database."
   print "Exiting here.\n"
   exit 7
  fi
 else
  $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
  WHENEVER SQLERROR EXIT FAILURE
  set pages 999 feedback off
  col label format a15
  col header_status format a15
  col path format a15
  col "Disk(GB)" format 9999
  set feedback on
  spool $WORKING_DIR/$$_rows_found.txt
  select rownum "Line Number",label,header_status,path,total_mb/1000 "Disk(GB)" from v\$asm_disk where header_status in  ('PROVISIONED','FORMER') order by rownum;
EOF
  if [ $? -ne 0 ]
  then
   print "\nThere was an error when querying ASM Database."
   print "Exiting here.\n"
   exit 7
  fi
 fi

# This is where we check DISK..
#
# spare_disks=0
# grep '^no rows selected' $WORKING_DIR/$$_rows_found.txt > /dev/null
# if [ $? -eq 0 ]
# then
#  print "There are no free disks available on this host for expanding database storage." >> $ERROR_FILE
#  (( available=0 ))
#  spare_disks=1
# else
#  ((available=$(sed -e '1,3d' $WORKING_DIR/$$_rows_found.txt | head -n -3 | awk '{sum+=$NF} END {printf "%6d\n",sum}')))
# fi

# rm -f $HOLD_ERROR_FILE
# if [[ -f $ERROR_FILE ]]
# then
#  if [ $hit_capacity -eq 1 ]
#  then
#   print "$(hostname): One or more diskgroups have reached the threshold of $disk_group_used_threshold percent capacity.\n" >> $HOLD_ERROR_FILE
#   print "The diskgroup(s) that have reached the threshold:" >> $HOLD_ERROR_FILE
#   cat $HOLD_ERROR_FILE $ERROR_FILE > $TEMP_ERROR_FILE
#   mv $TEMP_ERROR_FILE $ERROR_FILE
#  fi
#  cat $WORKING_DIR/$$_diskgroup_info.txt >> $ERROR_FILE
#  grep 'There are no free disks available on this host for expanding database storage' $ERROR_FILE > /dev/null
#  if [ $? -ne 0 ]
#  then
#   print "\nThe devices shown here with PROVISIONED and FORMER can be used to expand storage." >> $ERROR_FILE
#   cat $WORKING_DIR/$$_rows_found.txt >> $ERROR_FILE
#  else
#   print "\nThere must be at least ${disk_group_available_threshold}GB of storage available to expand database.\n\n" >> $ERROR_FILE
#  fi
# fi
#
# if [ $spare_disks -eq 0 ]
# then
#  if [ $available -lt $disk_group_available_threshold ]
#  then
#   print "\nThere is less than ${disk_group_available_threshold}GB available to expand database on $(hostname)" >> $ERROR_FILE
#   print "There is ${available}GB available for expansion.  Please allocate to at least ${disk_group_available_threshold}GB\n\n" >> $ERROR_FILE
#  fi
# fi

 export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
 . /usr/local/bin/oraenv > /dev/null

 InsertFile=$WORKING_DIR/insert_in_bb_asm_tbsp.txt
 $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
 set feedback off pagesize 999 linesize 132 heading off
 spool $InsertFile
 select 'Insert into tablespaces values (' || '$DB_INSTANCE' || ','''|| sysdate || ''',''' || t.tablespace || ''',' || t.totalspace || ',' || round((t.totalspace-fs.freespace),2) || ');'
 from
 (select round(sum(d.bytes)/(1024*1024)) as totalspace, d.tablespace_name tablespace
 from dba_data_files d
 group by d.tablespace_name) t,
 (select round(sum(f.bytes)/(1024*1024)) as freespace, f.tablespace_name tablespace
 from dba_free_space f
 group by f.tablespace_name) fs
 where t.tablespace=fs.tablespace
 order by t.tablespace;
EOF

 # Insert the space info into the DBA database
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
 @$InsertFile
 insert into tablespaces select db_instance, report_date, 'TOTAL', sum(Space_Allocated_M), sum(Space_Used_m)
 from tablespaces where db_instance=${DB_INSTANCE} and report_date = to_char(sysdate,'DD-MON-YYYY')
 group by db_instance,report_date;
 commit;
EOF
 rm -f $WORKING_DIR/$$_rows_found.txt $WORKING_DIR/$$_diskgroup_info.txt $WORKING_DIR/$$_sqlplus_banner.txt
 rm -f $HOLD_ERROR_FILE $TEMP_ERROR_FILE $InsertFile

 $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
 set lines 200 trimspool on pagesize 2000 feedback off
 col "USED(MB)" format 99999999
 col "ALLOCATED(MB)" format 99999999
 col "PCT USED" format 999999
 spool $WORKING_DIR/free_space_in_tablespace.txt
 select tablespace_name,sum(maxbytes)/1024/1024 "ALLOCATED(MB)",sum(bytes)/1024/1024 "USED(MB)",100-(sum(maxbytes)-sum(bytes))/sum(maxbytes)*100 "PCT USED"
 from dba_data_files group by tablespace_name;
 spool off
 set feedback on
 col file_name format a60
 col autoextensible format a15
 spool $WORKING_DIR/files_that_are_autoextend.txt
 select file_name,autoextensible
 from dba_data_files
 where autoextensible='NO'
 union
 select file_name,autoextensible
 from dba_temp_files
 where autoextensible='NO';
 spool off
EOF
 sed '/^$/d' $WORKING_DIR/free_space_in_tablespace.txt > $WORKING_DIR/output_without_blank_lines.txt
 (( lines=$(wc -l $WORKING_DIR/output_without_blank_lines.txt | awk '{print $1}') ))
 (( remainder=$(print "scale=3; $lines - 2" | bc) ))
 tail -$remainder $WORKING_DIR/output_without_blank_lines.txt  > $WORKING_DIR/only_just_the_files_check.txt
 ((exception_found_in_tbsp=0))
 while read tablespace allocated used pct_used
 do
  if [ $pct_used -ge $asm_tablespace_percent_threshold ]
  then
   print "${BOX}: $ORACLE_SID Database $tablespace Tablespace has reached $pct_used Percent Used" >> $ERROR_FILE
   ((exception_found_in_tbsp=1))
  fi 
 done<$WORKING_DIR/only_just_the_files_check.txt
 if [ $exception_found_in_tbsp -eq 1 ]
 then
  print "\n\t\t\t\tThe threshold for Percent Used is ${asm_tablespace_percent_threshold}.\n" >> $ERROR_FILE
  print "The corrective action is to add another datafile to tablespace." >> $ERROR_FILE
  print "Set AUTOEXTEND ON and MAXSIZE UNLIMITED on the datafile or tempfile.\n" >> $ERROR_FILE
  cat $WORKING_DIR/free_space_in_tablespace.txt >> $ERROR_FILE
 fi 

 grep 'no rows selected' $WORKING_DIR/files_that_are_autoextend.txt > /dev/null
 if [ $? -ne 0 ]
 then
  print "\n${BOX}: $ORACLE_SID Database: Datafile(s) found with autoextend off.  Autoextend must be enabled for all datafiles and tempfiles." >> $ERROR_FILE
  cat $WORKING_DIR/files_that_are_autoextend.txt >> $ERROR_FILE
 fi
 rm -f $WORKING_DIR/output_without_blank_lines.txt $WORKING_DIR/only_just_the_files_check.txt
 rm -f $WORKING_DIR/files_that_are_autoextend.txt $WORKING_DIR/free_space_in_tablespace.txt

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE
  SendNotification "$(hostname): $ORACLE_SID database growth warning. ASM Storage"
 fi

 # Update Infrastructure database
 if [ $PERFORM_CRON_STATUS -eq 1 ]
 then
  if [ $MAIL_COUNT -gt 0 ]
  then
   PROCESS_STATUS='WARNING'
  else
   PROCESS_STATUS='SUCCESS'
  fi
  funct_final_audit_update
 fi

else
 #--------------------------------------------------
 # This section is for non-ASM
 # Adjust threshold here:
 export minimum_expansions=10
 #--------------------------------------------------

 export log_dir=$WORKING_DIR/logs
 export today=$(date "+%Y%m%d")

 # Loop through the list of Oracle instances to run against
 for a_sid in $candidate_sids
 do
  Sequence_Number=0
  export MAIL_COUNT=0
  export ORACLE_SID=$a_sid
  export ORAENV_ASK=NO
  export PATH=/usr/local/bin:$PATH
  . /usr/local/bin/oraenv > /dev/null
  unset ORA_NLS33

  # Make sure this database is not in list of SIDs that nothing should be run against
  EXCEPTION=$($NAWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID" 'BEGIN{print index(a,b)}')
  if [[ "$EXCEPTION" != "0" ]]
  then
   continue
  fi  

  # Make sure this database is not in list of SIDs that this job should not be run for 
  EXCEPTION=$($NAWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID" 'BEGIN{print index(a,b)}')
  if [[ "$EXCEPTION" != "0" ]]
  then
   continue
  fi  

  # Make sure database is OPEN if not, skip to the next database
  funct_verify_open
  if [[ $? -ne 0 ]]
  then
   continue
  fi

  # Add entry into DBA Admin database
  if [ $PERFORM_CRON_STATUS = 1 ]
  then
   funct_initial_audit_update
   if [ $? -ne 0 ]
   then
    PERFORM_CRON_STATUS=0
   fi
   export DB_INSTANCE
  fi
 
  export ORAENV_ASK=NO
  export PATH=/usr/local/bin:$PATH
  . /usr/local/bin/oraenv > /dev/null
  $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > $log_dir/db_block_size_${ORACLE_SID}.txt
  set pagesize 0 linesize 180 feedback off trimspool on echo off
  select tablespace_name,block_size from dba_tablespaces order by tablespace_name;
EOF
  $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > $log_dir/part1_of_this_thing.txt
  set pagesize 9000 linesize 180 feedback off tab off
  break on tablespace_name on MXNX skip 1
  col tablespace_name format a30
  col file_name format a70
  col MXNX format 99999
  col FILEMB format 999999
  col MXBYTES format 9999999
  col INCRMT format 999999
  col CANGROW format 9999999
  select file_name,
       a.bytes/1024/1024 FILEMB,
       maxbytes/1024/1024 MXBYTES,
       increment_by INCRMT,
       NVL(CEIL(max(NVL(next_extent,0)*(1+(NVL(pct_increase,0)/100))/1024/1024)),1) MXNX,
       a.tablespace_name,
       (maxbytes-a.bytes-increment_by)/1024/1024 CANGROW
  from dba_data_files a, dba_segments b
  where a.tablespace_name = b.tablespace_name(+)
  and a.tablespace_name not like 'UNDOTBS%'
  group by file_name, a.bytes, a.tablespace_name, maxbytes, increment_by, (maxbytes-a.bytes-increment_by)
  order by a.tablespace_name,increment_by desc,file_name;
EOF

  head -3 $log_dir/part1_of_this_thing.txt > $log_dir/hold_header_${ORACLE_SID}.txt
  sed '1,3d' $log_dir/part1_of_this_thing.txt > $log_dir/just_the_facts_maam_${ORACLE_SID}.txt
  rm -f $log_dir/test_df_${ORACLE_SID}.txt
  while read a_line
  do
   (( no_records=$(print $a_line | awk '{print NF}') ))
   file_name=$(print $a_line | awk '{print $1}')
   file_mb=$(print $a_line | awk '{print $2}')
   maxbytes=$(print $a_line | awk '{print $3}')
   increment=$(print $a_line | awk '{print $4}')
   if [ $no_records -eq  7 ]
   then
    maxnext=$(print $a_line | awk '{print $5}')
    cangrow=$(print $a_line | awk '{print $7}')
    tablespace_name=$(print $a_line | awk '{print $6}')
    tablespace_name_no_dollar=$(print $tablespace_name | sed 's/\$//g')
    (( block_size=$(grep -w $tablespace_name_no_dollar $log_dir/db_block_size_${ORACLE_SID}.txt | awk '{print $2}') ))
    (( size_of_increment=$increment * $block_size / 1024 / 1024 ))
    printf  "%-70s%8d%9d%8d%7d%s%-31s%8d\n" $file_name $file_mb $maxbytes $size_of_increment $maxnext " " $tablespace_name $cangrow >> $log_dir/test_df_${ORACLE_SID}.txt
   fi
   if [ $no_records -eq  0 ]
   then
    print "" >> $log_dir/test_df_${ORACLE_SID}.txt
    continue
   fi
   if [ $no_records -eq  5 ]
   then
    cangrow=$(print $a_line | awk '{print $5}')
    (( block_size=$(grep -w $tablespace_name_no_dollar $log_dir/db_block_size_${ORACLE_SID}.txt | awk '{print $2}') ))
    (( size_of_increment=$increment * $block_size / 1024 / 1024 ))
    printf  "%-70s%8d%9d%8d%7d%s%39d\n" $file_name $file_mb $maxbytes $size_of_increment $maxnext " " $cangrow >> $log_dir/test_df_${ORACLE_SID}.txt
   fi
  done<$log_dir/just_the_facts_maam_${ORACLE_SID}.txt
  cat $log_dir/hold_header_${ORACLE_SID}.txt $log_dir/test_df_${ORACLE_SID}.txt > $log_dir/result_df_${ORACLE_SID}.txt

  mv $log_dir/result_df_${ORACLE_SID}.txt $log_dir/part1_of_this_thing.txt
  rm -f $log_dir/result_df_${ORACLE_SID}.txt $log_dir/test_df_${ORACLE_SID}.txt $log_dir/just_the_facts_maam_${ORACLE_SID}.txt $log_dir/hold_header_${ORACLE_SID}.txt $log_dir/db_block_size_${ORACLE_SID}.txt

  if [[ ! -d $WORKING_DIR/logs/df_growth ]]
  then
   mkdir -p $WORKING_DIR/logs/df_growth
  fi
  $WORKING_DIR/df_growth.pl
  sed '1,2d' $WORKING_DIR/logs/df_growth/expanding_datafile_report_${ORACLE_SID}_${today}.txt > $log_dir/expanding_datafile_report_${ORACLE_SID}_${today}.txt
  $WORKING_DIR/df_growth_exception_report.pl
  if [ $? -ne 0 ]
  then
   cat $log_dir/exceptions_${ORACLE_SID}_${today}.txt > $ERROR_FILE
   print "\n\n\nSee most recent expanding_datafile_report_${ORACLE_SID}_*.txt for full report." >> $ERROR_FILE
   print "There is $minimum_expansions datafile expansions or fewer remaining for" >> $ERROR_FILE
   print "the following tablespaces before the next expansion will cause the tablespace" >> $ERROR_FILE
   print "to run out of space." >> $ERROR_FILE
   print "\nSee the number(left justified) below the tablespace's datafile(s)." >> $ERROR_FILE
   print "This is the number of expansions of INCRMT(increment_by) before above tablespace is full." >> $ERROR_FILE
   print "\nConsider expanding tablespace by adding a new datafile or extending existing datafiles." >> $ERROR_FILE
   SendNotification "$(hostname): $ORACLE_SID database growth warning.  Filesystem Storage"
  fi
  rm -f $log_dir/exceptions_${ORACLE_SID}_${today}.txt

  # Make sure history directory exists
  if [[ ! -d ${WORKING_DIR}/history ]]
  then
   mkdir -p ${WORKING_DIR}/history
  fi 

  # Get freespace for the current database
  FreespaceFile=${WORKING_DIR}/history/${ORACLE_SID}_Freespace_${NumericDate}.dat
  InsertFile=$WORKING_DIR/history/${ORACLE_SID}_Insert_${NumericDate}.dat
  funct_get_freespace
  rm -f $InsertFile

  # Update Infrastructure database
  if [ $PERFORM_CRON_STATUS = 1 ]
  then
   if [ $MAIL_COUNT -gt 0 ]
   then
    PROCESS_STATUS='WARNING'
   else
    PROCESS_STATUS='SUCCESS'
   fi
  funct_final_audit_update
  fi

 done
 rm -f $ERROR_FILE

 # Run the reset of AUTOEXTEND NEXT on the first Monday of the month
 (( date_of_month=$(date "+%d") ))
 day_of_month=$(date "+%A")
 if [[ $date_of_month -le 7 ]] && [[ $day_of_month = 'Monday' ]]
 then
  $WORKING_DIR/set_autoextend_next_all.ksh
 fi
fi

exit 0

