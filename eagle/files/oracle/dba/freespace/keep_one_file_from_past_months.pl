#!/usr/bin/perl
# Script name:  keep_one_file_from_past_months.pl
$system_day=`date "+%d"`;
chomp $system_day;
if ( $system_day != '28' )
{
 exit 0;
}
$log_dir="/u01/app/oracle/local/dba/freespace/logs/df_growth";
$system_month=`date "+%b"`;
$system_os=`uname`;
if ( $system_os =~ /SunOS/ )
{
 $ORATAB="/var/opt/oracle/oratab";
}
else
{
 $ORATAB="/etc/oratab";
}
chomp $ORATAB;
chomp $system_month;
@sids_in_oratab=`grep -v "^#" $ORATAB | awk -F: '{print \$1}' |grep -v '^\$'`;
chomp $sids_in_oratab;
$inputfile=$log_dir . "/directory_listing.txt";
foreach $sid (@sids_in_oratab)
{
 chomp $sid;
 system "ls -rlt $log_dir/*${sid}* > $log_dir/directory_listing.txt";
 $previous_month=" ";
 open(INPUT,"$inputfile") || die "Could not open file $indexfile : $!\n";
 while (<INPUT>)
 {
  ($x1,$x2,$x3,$x4,$x5,$current_month,$x7,$time_or_year,$file_name)=split;
  if (( $current_month =~ /$system_month/ ) && ( $time_or_year =~ /:/ ))
  {
   last;
  }
  if ( $current_month =~ /$previous_month/ )
  {
   system "rm $file_name";
  }
  $previous_month=$current_month;
 }
 close (INPUT);
 unlink $log_dir . "/directory_listing.txt";
}
exit 0;
