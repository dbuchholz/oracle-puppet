#!/usr/bin/perl
$minimum_expansions=$ENV{'minimum_expansions'};
$today=$ENV{'today'};
$sid=$ENV{'ORACLE_SID'};
$log_dir=$ENV{'log_dir'};
$inputfile=$log_dir . "/expanding_datafile_report_" . $sid . "_" . $today . ".txt";
$paragraphfile=$log_dir . "/paragraph_" . $sid . "_" . $today . ".txt";
$tempparagraphfile=$log_dir . "/tempparagraph_" . $sid . "_" . $today . ".txt";
$exceptionsfile=$log_dir . "/exceptions_" . $sid . "_" . $today . ".txt";
open(INPUT,"$inputfile") || die "Could not open file $inputfile : $!\n";
open(OUTPUT,">$paragraphfile") || die "Could not open file $paragraphfile : $!\n";
open(OUTPUT2,">$exceptionsfile") || die "Could not open file $exceptionsfile : $!\n";
LINE: while (<INPUT>)
{
 chomp;
 if (($_ =~ /^FILE_NAME.*$/) || ($_ =~ /^\-\-\-\-.*$/))
 {
  print OUTPUT2 "$_\n";
  next LINE;
 }
 if ( $_ =~ /^$/ )
 {
  close (OUTPUT);
  system "cp $paragraphfile $tempparagraphfile";
  open(NEWINPUT,"$tempparagraphfile") || die "Could not open file $paragraphfile : $!\n";
  open(OUTPUT,">$paragraphfile") || die "Could not open file $paragraphfile : $!\n";
  if ( $previous_record <= $minimum_expansions )
  {
   while (<NEWINPUT>)
   {
    print OUTPUT2 "$_";
   }
   close (NEWINPUT);
   next LINE;
  }
 }
 print OUTPUT "$_\n";
 $previous_record=$_;
 chomp $previous_record;
}
close (INPUT);
close (OUTPUT);
close (OUTPUT2);
unlink "$tempparagraphfile";
unlink "$paragraphfile";
unlink "$inputfile";
$lines_in_report=`wc -l $exceptionsfile | awk '{print \$1}'`;
if ( $lines_in_report > 3 )
{
 exit 1;
}
exit 0;
