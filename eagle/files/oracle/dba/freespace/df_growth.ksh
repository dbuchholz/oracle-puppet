#!/bin/ksh
# Script name:  df_growth.ksh
# Usage:  df_growth.ksh <Oracle SID>
mailrecipients=team_dba@eagleaccess.com
export minimum_expansions=10
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
export log_dir=$script_dir/logs
  
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of the database you want to check tablespace free space."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi 

export ORACLE_SID=$1

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export today=$(date "+%Y%m%d")
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > $log_dir/db_block_size_${ORACLE_SID}.txt
set pagesize 0 linesize 180 feedback off trimspool on echo off
select tablespace_name,block_size from dba_tablespaces order by tablespace_name;
EOF
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > $log_dir/part1_of_this_thing.txt
set pagesize 9000 linesize 180 feedback off tab off
break on tablespace_name on MXNX skip 1
col tablespace_name format a30
col file_name format a70
col MXNX format 99999
col FILEMB format 999999
col MXBYTES format 9999999
col INCRMT format 999999
col CANGROW format 9999999
select file_name,
       a.bytes/1024/1024 FILEMB,
       maxbytes/1024/1024 MXBYTES,
       increment_by INCRMT,
       NVL(CEIL(max(NVL(next_extent,0)*(1+(NVL(pct_increase,0)/100))/1024/1024)),1) MXNX,
       a.tablespace_name,
       (maxbytes-a.bytes-increment_by)/1024/1024 CANGROW
from dba_data_files a, dba_segments b
where a.tablespace_name = b.tablespace_name(+)
and a.tablespace_name not in ('RBS','TEMP','TEMP2','AUTO_UNDO','UNDOTBS','UNDOTBS1')
group by file_name, a.bytes, a.tablespace_name, maxbytes, increment_by, (maxbytes-a.bytes-increment_by)
order by a.tablespace_name,increment_by desc,file_name;
EOF

head -3 $log_dir/part1_of_this_thing.txt > $log_dir/hold_header_${ORACLE_SID}.txt
sed '1,3d' $log_dir/part1_of_this_thing.txt > $log_dir/just_the_facts_maam_${ORACLE_SID}.txt
rm -f $log_dir/test_df_${ORACLE_SID}.txt
while read a_line
do
 (( no_records=$(print $a_line | awk '{print NF}') ))
 file_name=$(print $a_line | awk '{print $1}')
 file_mb=$(print $a_line | awk '{print $2}')
 maxbytes=$(print $a_line | awk '{print $3}')
 increment=$(print $a_line | awk '{print $4}')
 if [ $no_records -eq  7 ]
 then
  maxnext=$(print $a_line | awk '{print $5}')
  cangrow=$(print $a_line | awk '{print $7}')
  tablespace_name=$(print $a_line | awk '{print $6}')
  (( block_size=$(grep -w $tablespace_name $log_dir/db_block_size_${ORACLE_SID}.txt | awk '{print $2}') ))
  (( size_of_increment=$increment * $block_size / 1024 / 1024 ))
  printf  "%-70s%8d%9d%8d%7d%s%-31s%8d\n" $file_name $file_mb $maxbytes $size_of_increment $maxnext " " $tablespace_name $cangrow >> $log_dir/test_df_${ORACLE_SID}.txt
 fi
 if [ $no_records -eq  0 ]
 then
  print "" >> $log_dir/test_df_${ORACLE_SID}.txt
  continue
 fi
 if [ $no_records -eq  5 ]
 then
  cangrow=$(print $a_line | awk '{print $5}')
  (( block_size=$(grep -w $tablespace_name $log_dir/db_block_size_${ORACLE_SID}.txt | awk '{print $2}') ))
  (( size_of_increment=$increment * $block_size / 1024 / 1024 ))
  printf  "%-70s%8d%9d%8d%7d%s%39d\n" $file_name $file_mb $maxbytes $size_of_increment $maxnext " " $cangrow >> $log_dir/test_df_${ORACLE_SID}.txt
 fi
done<$log_dir/just_the_facts_maam_${ORACLE_SID}.txt
cat $log_dir/hold_header_${ORACLE_SID}.txt $log_dir/test_df_${ORACLE_SID}.txt > $log_dir/result_df_${ORACLE_SID}.txt

mv $log_dir/result_df_${ORACLE_SID}.txt $log_dir/part1_of_this_thing.txt
rm -f $log_dir/result_df_${ORACLE_SID}.txt $log_dir/test_df_${ORACLE_SID}.txt $log_dir/just_the_facts_maam_${ORACLE_SID}.txt $log_dir/hold_header_${ORACLE_SID}.txt $log_dir/db_block_size_${ORACLE_SID}.txt

if [[ ! -d $script_dir/logs/df_growth ]]
then
 mkdir -p $script_dir/logs/df_growth
fi
$script_dir/df_growth.pl
sed '1,2d' $script_dir/logs/df_growth/expanding_datafile_report_${ORACLE_SID}_${today}.txt > $log_dir/expanding_datafile_report_${ORACLE_SID}_${today}.txt
$script_dir/df_growth_exception_report.pl
if [ $? -ne 0 ]
then
 mailx -s "$(hostname): $ORACLE_SID database growth warning" $mailrecipients<<EOF
$(cat $log_dir/exceptions_${ORACLE_SID}_${today}.txt)


See most recent expanding_datafile_report_${ORACLE_SID}_*.txt for full report.
There is $minimum_expansions datafile expansions or fewer remaining for
the following tablespaces before the next expansion will cause the tablespace
to run out of space.

See the number(left justified) below the tablespace's datafile(s).
This is the number of expansions of INCRMT(increment_by) before above tablespace is full.

Consider expanding tablespace by adding a new datafile or extending existing datafiles.
EOF
fi
rm -f $log_dir/exceptions_${ORACLE_SID}_${today}.txt
exit 0
