#!/bin/ksh

# Are any background processes there
ps -ef | grep "ora_smon_" | awk '{print $NF}' > /dev/null
candidate_sids=$(ps -ef | grep "ora_smon_" | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
if [[ -z $candidate_sids ]]
then
 exit 0
fi

for a_sid in $candidate_sids
do
export ORACLE_SID=$a_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
DB_STATUS=$($ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF | tr -s ' ' | sed -e 's/ /-/g' -e 's/ //g' -e 's/	//g'
     WHENEVER SQLERROR EXIT FAILURE
     set pages 0 trim on trims on lines 150
     SELECT open_mode,status,c.* FROM v\$database, v\$instance, dual c ;
EOF
)
if [[ $DB_STATUS = "READ-WRITE-OPEN-X" ]]
then
 /u01/app/oracle/local/dba/freespace/df_growth.ksh $a_sid
fi
done
exit 0
