#!/bin/ksh
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [[ $PERFORM_CRON_STATUS != 1 ]]
 then
  PERFORM_CRON_STATUS=0
 fi
}

### MAIN #####

export ORACLE_SID=$(ps -ef | grep asm_smon_+ASM | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
if [[ -z $ORACLE_SID ]]
then
 exit 0
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
. /usr/local/bin/oraenv > /dev/null

export BOX=$(print $(hostname) | awk -F. '{print $1}')
TODAY=$(date "+%m-%d-%Y")

WORKING_DIR=/usr/local/scripts/dba/freespace

MAILTO=fdavis@eagleinvsys.com

#----------------------------------------------------------------
# This section is for ASM
# Adjust threshold here:
 disk_group_available_threshold=500  # Value in Gigabytes
#----------------------------------------------------------------

$ORACLE_HOME/bin/sqlplus / as sysdba<<EOF>$WORKING_DIR/$$_sqlplus_banner.txt
WHENEVER SQLERROR EXIT FAILURE
EOF
if [ $? -ne 0 ]
then
 print "\nCould not reach the ASM Database."
 print "Exiting here.\n"
 exit 5
fi
ora_version=$(grep '^SQL\*Plus: ' $WORKING_DIR/$$_sqlplus_banner.txt | awk '{print $3}' | awk -F. '{print $1}')
rm -f $WORKING_DIR/$$_sqlplus_banner.txt

# For Oracle 11g and newer it uses os_mb, for Oracle 10g and older it uses total_mb
if [ $ora_version -ge 11 ]
then
 $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
 WHENEVER SQLERROR EXIT FAILURE
 set pages 999 feedback off
 col label format a15
 col header_status format a15
 col path format a15
 col "Disk(GB)" format 9999
 set feedback on
 spool $WORKING_DIR/asm_disk_rows_found.txt
 select rownum "Line Number",label,header_status,path,os_mb/1000 "Disk(GB)" from v\$asm_disk where header_status in  ('PROVISIONED','FORMER') order by rownum;
EOF
 if [ $? -ne 0 ]
 then
  print "\nThere was an error when querying ASM Database."
  print "Exiting here.\n"
  exit 7
 fi
else
 $ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
 WHENEVER SQLERROR EXIT FAILURE
 set pages 999 feedback off
 col label format a15
 col header_status format a15
 col path format a15
 col "Disk(GB)" format 9999
 set feedback on
 spool $WORKING_DIR/asm_disk_rows_found.txt
 select rownum "Line Number",label,header_status,path,total_mb/1000 "Disk(GB)" from v\$asm_disk where header_status in  ('PROVISIONED','FORMER') order by rownum;
EOF
 if [ $? -ne 0 ]
 then
  print "\nThere was an error when querying ASM Database."
  print "Exiting here.\n"
  exit 7
 fi
fi
print "\t\t\t\t\tASM Disk Report"
print "\t\t\t\t\t$TODAY\n"
print "The threshold is ${disk_group_available_threshold}GB Available\n"
print "Server Name        Available(GB)"
grep 'no rows selected' $WORKING_DIR/asm_disk_rows_found.txt > /dev/null
if [ $? -eq 0 ]
then
 print "$BOX     No ASM Devices available."
else
 ((available=$(sed -e '1,3d' $WORKING_DIR/asm_disk_rows_found.txt | head -n -3 | awk '{sum+=$NF} END {printf "%6d\n",sum}')))
fi
if [[ -n $available ]]
then
 if [ $available -lt $disk_group_available_threshold ]
 then
  printf "%-20s%8d\n" $BOX $available
 fi
fi
exit 0
