#!/usr/bin/perl
# Script name:  df_growth.pl
$today=`date "+%Y%m%d"`;
chomp $today;
$sid=$ENV{'ORACLE_SID'};
$df_command=$ENV{'DF_COMMAND'};
$log_dir=$ENV{'log_dir'};
$history_dir=$log_dir . "/df_growth";
$indexfile=$log_dir . "/part1_of_this_thing.txt";
$outputfile=$history_dir . "/expanding_datafile_report_" . $sid . "_" . $today . ".txt" ;
open(INPUT,"$indexfile") || die "Could not open file $indexfile : $!\n";
open(OUTPUT,">$outputfile") || die "Could not open file $outputfile : $!\n";
LINE: while (<INPUT>)
{
 chomp;
 if (( $_ =~ /^$/ ) || ( $_ =~ /^FILE/ ) || ( $_ =~ /------/ ))
 {
  if ( $_ =~ /^FILE/ )
  {
   print OUTPUT "$_" . "  In FS" . "\n";
   next LINE;
  }
  if ( $_ =~ /^------/ )
  {
   print OUTPUT "$_" . "  -----" . "\n";
   next LINE;
  }
  print OUTPUT "$_\n";
  next LINE;
 }
 $mount_point_end=index($_,"\/oradata");
 $filesystems=substr($_,0,$mount_point_end);
 $sizeofit=`$df_command $filesystems |tail -1|awk '{printf "%5d\\n",\$4/1000}'`;
 print OUTPUT "$_" . " " . $sizeofit;
}
close (INPUT);
close (OUTPUT);
unlink "$log_dir" . "/part1_of_this_thing.txt";
$tempfile=$log_dir . "/tempafile.out";
open(INPUT,"$outputfile");
open(OUTPUT,">$tempfile");
LINE: while (<INPUT>)
{
 chomp;
 if (($_ =~ /^FILE_NAME.*$/) || ($_ =~ /^\-\-\-\-.*$/))
 {
  print OUTPUT "$_\n";
  next LINE;
 }
 $blank_lines=0;
 if ( $_ =~ /^$/ )
 {
  $blank_lines=1;
  $everything=$previous_record . $expansions;
  print OUTPUT "$everything\n";
  $expansions=0;
  $previous_record=$_;
 }
  print OUTPUT "$_\n";
  if ( $blank_lines == 0 )
  {
   @record=split;
   $increment=@record[3];
   $max_next=@record[4];
    $cangrow=@record[6];
    $infs=@record[7];
   if ( @record[5] =~ /[A-Z]/ )    # a line with tablespace name and not a blank line
   {
    $cangrow=@record[6];
    $infs=@record[7];
   }
   else
   {
    $cangrow=@record[5];
    $infs=@record[6];
   }
   if ( $max_next > $increment )
   {
    $hold_larger=$max_next;
   }
   else
   {
    $hold_larger=$increment;
   }
   if ( $cangrow > 0 )
   {
    if ( $infs > $cangrow )
    {
     if ( $hold_larger > 0 )
     {
      $expansions=$expansions + int ($cangrow/$hold_larger);
     }
     else
     {
      $expansions=0;
     }
    }
    else
    {
     if ( $hold_larger > 0 )
     {
      $expansions=$expansions + int ($infs/$hold_larger);
     }
     else
     {
      $expansions=0;
     }
    }
   }
  }
}
close (INPUT);
close (OUTPUT);
rename $tempfile,$outputfile;
exit 0;
