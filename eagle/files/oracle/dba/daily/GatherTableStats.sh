#!/bin/ksh
# ******************************************************************* 
# NAME:		GatherTableStats.sh	
# 
# AUTHOR: 	Maureen Buotte                                    
#
# PURPOSE: 	This utility will gather statistics for a database 
#
# USAGE:	GatherTableStats SID
#
# INPUT PARAMETERS:
#			SID		Oracle SID of database to gather statistics for
# REVISIONS:
# Frank Davis  05/16/2015  Added compatibility with ASM Managed Databases
# Frank Davis  09/30/2013  Remove team_dba@eagleaccess.com from email
# Frank Davis  09/23/2013  Remove tech_support@eagleaccess.com from email
# Frank Davis  06/13/2013  Made script compatible Service Now.
# Frank Davis  04/29/2013  Made script compatible with Linux and Solaris
# Frank Davis  04/26/2013  Added compatibility with Oracle partitioned tables
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis      24-Jun-2011  Modified IPCheck Variable to include updated list of VLANs
# Nishigandha Sathe     02-Mar-2010  Modofied to use new primary key machines(instance)
# Maureen Buotte   20-Jul-2009  Modified to run on Hawaii using the new infprd1 database
#  
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
 print "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat
 if [[ -n $1 ]]
 then
  print "\n$1\n" >> mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> mail.dat
  rm $ERROR_FILE
 fi

 cat mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
EOF
`
 DB_INSTANCE=$(print $DB_INSTANCE | tr -d " ")
 export DB_INSTANCE
 if [[ -z $DB_INSTANCE ]]
 then
  SendNotification "$ORACLE_SID on $BOX does not exist in $CRON_SID -> $PROGRAM_NAME will run for $ORACLE_SID but $CRON_SID will not be updated" 
  PERFORM_CRON_STATUS=0
  return 1	
 fi

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ -z $Sequence_Number ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000
  set heading off feedback off
  declare i number;
  begin
   insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;

 update INSTANCE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from instance_cron_job_runs where instance=${Sequence_Number})
 where instance=${Sequence_Number};
 commit;
/
EOF
`
 print  "$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS"
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open
{
 typeset -i OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off
 select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
EOF
`

 OPEN=$(print $OPEN|tr -d " ")

 if [ $OPEN -eq 1 ]
 then
  return 1
 fi
}

# --------------------------------------------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------------------------------------------
funct_chk_parm()
{
 if [ $NARG -ne 1 ]
 then
  print "$PROGRAM_NAME Failed: Incorrect number of arguments -> $PROGRAM_NAME SID "
  SendNotification "Incorrect number of arguments -> $PROGRAM_NAME SID  "
  exit 1
 fi
}


# --------------------------------------------------------------------------------------
# funct_set_monitoring_on:  Turn monitoring on for all tables
# --------------------------------------------------------------------------------------
funct_set_monitoring_on()
{
 RunDate=$(date +%Y%m%d)
 SetMonitoringOnFile="$WORKING_DIR/Scripts/SetMonitoringOn_${INPUT_SID}_${NumericDate}.sql"

 ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
 set heading off feedback off pagesize 300 linesize 200
 spool $SetMonitoringOnFile
 select 'alter table '||owner||'.'||table_name||' monitoring;'
 from sys.dba_tables
 where monitoring = 'NO'
 and temporary != 'Y'
 and owner not in ('SYS','SYSTEM')
 and nvl(duration,'X') not in ('SYS$SESSION','SYS$TRANSACTION')
 and iot_type is null
 and (owner,table_name) not in
 (select owner,table_name from dba_external_tables);
EOF

 # Run script to update tables for Monitoring
 if [ $ASM_CLIENT = Y ]
 then
  LogFile=/home/oracle/CRONLOG/SetMonitoringOnFile_${INPUT_SID}.log
 else
  LogFile=$HOME/CRONLOG/SetMonitoringOnFile_${INPUT_SID}.log
 fi

 if [[ -f $SetMonitoringOnFile ]]
 then
  $ORACLE_HOME/bin/sqlplus -s '/as sysdba' <<EOF
  spool $LogFile
  @$SetMonitoringOnFile
EOF
 fi

} 

# --------------------------------------------------------------------------------------
# funct_gather_stats:  Gather table statistics 
# --------------------------------------------------------------------------------------
funct_gather_stats()
{
 GatherTableStatsFile="${WORKING_DIR}/Scripts/GatherTableStats_${INPUT_SID}_${NumericDate}.sql"
 DOW=$(date +%w)
 if [ $DOW != 6 ]
 then
  ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
  set pages 0 lines 999 trimsp on feed off heading off
  spool $GatherTableStatsFile
  SELECT 'exec dbms_stats.gather_table_stats(ownname => '''||dt.owner||''',tabname => '''||dt.table_name||''',estimate_percent => 20,degree => 8,cascade => true);'
  FROM DBA_TABLES dt WHERE dt.owner NOT IN ('SYSTEM','SYS','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and dt.logging='YES' and (dt.num_rows <= 5000000 or dt.num_rows is null);
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||partition_name||''''||',estimate_percent => 20,degree => 8,cascade => true,granularity => '||''''||'PARTITION'||''''||');' from dba_tab_partitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and (num_rows <= 5000000 or num_rows is null);
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||subpartition_name||''''||',estimate_percent => 20,degree => 8,cascade => true,granularity => '||''''||'SUBPARTITION'||''''||');' from dba_tab_subpartitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and (num_rows <= 5000000 or num_rows is null);
EOF
 else
  ${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
  set pages 0 lines 999 trimsp on feed off heading off
  spool ${GatherTableStatsFile}
  SELECT 'exec dbms_stats.gather_table_stats(ownname => '''||dt.owner||''',tabname => '''||dt.table_name||''',estimate_percent => 20,degree => 8,cascade => true);'
  FROM DBA_TABLES dt WHERE dt.owner NOT IN ('SYSTEM','SYS','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and dt.logging='YES' and ((dt.num_rows > 5000000 and dt.num_rows <= 10000000) or dt.num_rows is null);
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||partition_name||''''||',estimate_percent => 20,degree => 8,cascade => true,granularity => '||''''||'PARTITION'||''''||');' from dba_tab_partitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and ((num_rows > 5000000 and num_rows <= 10000000) or num_rows is null);
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||subpartition_name||''''||',estimate_percent => 20,degree => 8,cascade => true,granularity => '||''''||'SUBPARTITION'||''''||');' from dba_tab_subpartitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and ((num_rows > 5000000 and num_rows <= 10000000) or num_rows is null);

  SELECT 'exec dbms_stats.gather_table_stats(ownname => '''||dt.owner||''',tabname => '''||dt.table_name||''',estimate_percent => 5,degree => 8,cascade => true);'
  FROM DBA_TABLES dt WHERE dt.owner NOT IN ('SYSTEM','SYS','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and dt.logging='YES' and dt.num_rows > 10000000;
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||partition_name||''''||',estimate_percent => 5,degree => 8,cascade => true,granularity => '||''''||'PARTITION'||''''||');' from dba_tab_partitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and num_rows > 10000000;
  select 'exec dbms_stats.gather_table_stats(ownname => '||''''||table_owner||''''||',tabname => '||''''||table_name||''''||',partname => '||''''||subpartition_name||''''||',estimate_percent => 5,degree => 8,cascade => true,granularity => '||''''||'SUBPARTITION'||''''||');' from dba_tab_subpartitions where table_owner not in ('SYS','SYSTEM','OUTLN','DIP','TSMSYS','DBSNMP','WMSYS','EXFSYS','ANONYMOUS','XDB','ORDPLUGINS','SI_INFORMTN_SCHEMA','MDSYS','ORDSYS','SYSMAN','MGMT_VIEW','PERFSTAT','CTXSYS','MDSYS','TRACKING','PACE_READ','QUEST','TRACESVR','MKPROXY') and  partition_name > 'P_PART_201301' and logging='YES' and num_rows > 10000000;
EOF
 fi

 # Run script to gather schema statistics 
 if [ $ASM_CLIENT = Y ]
 then
  LogFile=/home/oracle/CRONLOG/GatherTableStatsFile_${INPUT_SID}.log
 else
  LogFile=$HOME/CRONLOG/GatherTableStatsFile_${INPUT_SID}.log
 fi

 if [[ -f $GatherTableStatsFile ]]
 then
  $ORACLE_HOME/bin/sqlplus -s '/as sysdba' <<EOF
  spool $LogFile
  @$GatherTableStatsFile
EOF
 fi
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

############################################################
#                       MAIN                               
############################################################
# Uncomment next line for debugging
NARG=$#
INPUT_SID=$1

export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export DB_INSTANCE 

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 AWK=awk
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 AWK=nawk
fi

# Get environment variables set up - need to do this differently for RHEL 5
dummy_sid=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORACLE_SID=$dummy_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/daily
else
 export WORKING_DIR=$HOME/local/dba/daily
fi

export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

HOSTNAME=$(hostname)
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=2
 export URGENCY=3
else
 export TYPE=TEST
 export IMPACT=2
 export URGENCY=3
fi
export MAILTO='eaglecsi@eagleaccess.com'

funct_chk_parm

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi


export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f ${PARFILE} ]]
then
 STATUS=$(sed '/^#/d' $PARFILE  > ${NO_COMMENT_PARFILE})
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(grep "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

export NumericDate=$(date +%Y%m%d)

export MAIL_COUNT=0

ErrorFile=$WORKING_DIR/ErrorFile_$INPUT_SID
rm -f $ErrorFile

export ORACLE_SID=$INPUT_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "Invalid SID -> ${INPUT_SID}"
 SendNotification "Invalid SID -> ${INPUT_SID_SID}"
 exit 1
fi

export ORACLE_SID
export ORACLE_HOME
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

# Make sure this database is not in list of SIDs that nothing should be run against
EXCEPTION=$($AWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [[ "${EXCEPTION}" != "0" ]]
then
 SendNotification "$PROGRAM_NAME will not run for $ORACLE_SID no jobs should run for this SID "
 continue
fi

# Make sure this database is not in list of SIDs that this job should not be run for
EXCEPTION=$($AWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
if [[ "${EXCEPTION}" != "0" ]]
then
 SendNotification "$PROGRAM_NAME will not run for $ORACLE_SID because it is explicitly excluded in BigBrother.ini file"
 continue
fi

# Make sure database is OPEN if not, skip to the next database
funct_verify_open
if [ $? -eq 0 ]
then
 print "$INPUT_SID is not OPEN -> $PROGRAM_NAME will not run"
 SendNotification "$INPUT_SID is not OPEN -> $PROGRAM_NAME will not run"
 exit 1
fi

# Add entry into DBA Admin database
if [[ $PERFORM_CRON_STATUS = 1 ]]
then
 funct_initial_audit_update
 if [ $? -ne 0 ]
 then
  PERFORM_CRON_STATUS=0
 fi
 export DB_INSTANCE
fi

ORAVER=$(print $ORACLE_HOME | grep 9.2)
if [[ -z $ORAVER ]]
then
 export Version_9i=0
else
 funct_set_monitoring_on
fi

funct_gather_stats

# Update Infrastructure database
if [[ $PERFORM_CRON_STATUS = 1 ]]
then
 if [ $MAIL_COUNT -gt 0 ]
 then
  PROCESS_STATUS='WARNING'
 else
  PROCESS_STATUS='SUCCESS'
 fi
 funct_final_audit_update
fi

print "${INPUT_SID}, $PROGRAM_NAME Completed successfully on $(date +\"%c\")"

######## END MAIN  #########################
exit 0
