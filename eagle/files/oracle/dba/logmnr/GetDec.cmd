#!/usr/bin/perl -w
#

use strict;
use lib "$ENV{HOME}/local/dba/perllib";
use ITSCode;

my $UserResponse;

if (scalar(@ARGV) != 1) {
	print "Enter value: ";
	$UserResponse = <>;
}
else {
	($UserResponse) = @ARGV;
}
chomp $UserResponse;

# calc dval
my $dval = "";
for (my $idx = 33; $idx <= 126; $idx+=7) {
	$dval .= chr($idx);
}

DecodeData ($dval, $UserResponse);

print "$UserResponse";

1;
