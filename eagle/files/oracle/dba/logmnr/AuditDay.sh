#/usr/bin/ksh
# ==================================================================================================
# NAME:         AuditDay.sh
#
# AUTHOR:       Maureen Buotte
#
# PURPOSE:      This utility uses the archived log files from the hot backup and pulls audit data required
#
# USAGE:        AuditDay.sh
#
# Frank Davis       24-Sep-2013  Remove tech_support@eagleaccess.com from email
# Nishigandha Sathe 02-Mar-2010  Modified to use new primary key machines(instance)
# Maureen Buotte    30-Sep-2009  Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte    24-Jun-2009  Changed to work with RMAN
# Maureen Buotte    23-Sep-2008  Added reporting to DBA database
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

	# Uncomment for debug
	 set -x

	echo -e "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat
	if [[ x$1 != 'x' ]]; then
		echo -e "\n$1\n" >> mail.dat
	fi

	if [[ -a ${ERROR_FILE} ]]; then
		cat $ERROR_FILE >> mail.dat
		rm ${ERROR_FILE}
	fi

	if [[ $2 = 'FATAL' ]]; then
		echo "*** This is a FATAL ERROR - ${PROGRAM_NAME} aborted at this point *** " >> mail.dat
	fi

	cat mail.dat | /bin/mail -s "PROBLEM WITH ${TYPE} Environment -- ${PROGRAM_NAME} on ${BOX} for ${ORACLE_SID}" ${MAILTO}
	rm mail.dat

	# Update the mail counter
	MAIL_COUNT=${MAIL_COUNT}+1

	return 0
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging
     set -x

	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=`echo -e $PERFORM_CRON_STATUS|tr -d " "`
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
		PERFORM_CRON_STATUS=0
	fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {

	# Uncomment for debug
	 set -x

	DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
		exit
EOF`
	DB_INSTANCE=`echo -e $DB_INSTANCE |tr -d " "`
	export DB_INSTANCE
	if [ "x$DB_INSTANCE" == "x" ]; then
		SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
        PERFORM_CRON_STATUS=0
		return 1	
	fi

	Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
		start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
		exit
EOF`
	Sequence_Number=`echo $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set serveroutput on size 1000000
		set heading off
		set feedback off
		declare i number;
		begin
		insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
		commit;
		dbms_output.put_line (i);
		end;
/
		exit
EOF`
	else
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
		commit;
EOF`

	fi
	export Sequence_Number
	return 0

}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

	# Uncomment for debug
	 set -x

    # Update DBA Auditing Database with end time and backup size
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off 
		set feedback off 
		update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
		commit;

		update INSTANCE_CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
		from instance_cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
/
		exit
EOF`

    echo  "$PROGRAM_NAME Completed `date +\"%c\"`with a status of ${PROCESS_STATUS} "
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open {

	# Uncomment next line for debugging
	 set -x

    typeset -i OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
		exit
EOF`

    OPEN=`echo -e $OPEN|tr -d " "`

	if [[ ${OPEN} == 1 ]]; then
		return 1
	else
		return 0
	fi
}


# --------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------
funct_chk_parm() {
        # Uncomment next line for debugging
        # set -x
        if [ ${NARG} -ne 1 ]; then
                echo "$0 FAILED: Not enough arguments passed -> SID"
                SendNotification "Not enough arguments passed -> SID"
                exit 1
        fi
}



############################################################
#                       MAIN
############################################################

# Uncomment next line for debugging
 set -x

NARG=$#
INPUT_SID=$1
funct_chk_parm

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export PROGRAM_NAME_FIRST=`echo ${PROGRAM_NAME} | awk -F "." '{print $1}'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export DB_INSTANCE 

export ORACLE_BASE=$HOME
export ORATAB_LOC="/etc"
export WORKING_DIR="${ORACLE_BASE}/local/dba/logmnr"
export ERROR_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_error.txt

# Check if CRONLOG directory exists
if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
    mkdir -p ${ORACLE_BASE}/CRONLOG
fi

# Get environment variables set up - need to do this differently for RHEL 5
dummy_sid=`cat ${ORATAB_LOC}/oratab | grep ${ORACLE_BASE} | grep -v "^#" | grep -v "^*" | awk -F: '{print $1}' | tail -1`
. /usr/bin/setsid.sh ${dummy_sid}

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a ${PARFILE} ]]; then
	STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
	if [[ -n $LINE ]]; then
		INFO=`echo $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g' `
		export PERFORM_CRON_STATUS=1
		export CRON_SID=`echo -e $INFO | awk '{print $1}' `
		export CRON_USER=`echo -e $INFO | awk '{print $2}' `
		CRON_CONNECT=`echo -e $INFO | awk '{print $3}' `
		export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
        # Make sure you can get to the infrastructure database
        funct_check_inf_database
        echo $PERFORM_CRON_STATUS
	fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_ANYTHING=`echo $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
	fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "${PROGRAM_NAME}:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_THIS_JOB=`echo $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
    fi
fi
rm -f ${NO_COMMENT_PARFILE}

HOSTNAME=`hostname`
IPCheck=`cat /etc/hosts | grep ${HOSTNAME} | awk '{print $1}' | egrep '10.60.5|10.60.3|10.60.99' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
    export TYPE=PROD
else
    export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com

# Make sure this database is not in list of SIDs that nothing should be run against
EXCEPTION=`awk -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
	SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} no jobs should run for this SID "
	continue
fi

# Make sure this database is not in list of SIDs that this job should not be run for
EXCEPTION=`awk -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
	SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} because it is explicitly excluded in CronStatus.ini file"
	continue
fi

# Set up for input SID
. /usr/bin/setsid.sh ${INPUT_SID}
Status=$?
if [ $Status != 0 ]; then
	echo -e "Invalid SID -> ${INPUT_SID}"
	SendNotification "Invalid SID -> ${INPUT_SID}"
	exit
fi

export ORACLE_SID
export ORACLE_HOME
export PATH
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib



# Make sure database is OPEN if not, skip to the next database
funct_verify_open
Status=$?
if [[ $Status == 0 ]]; then
	continue
fi

# Add entry into DBA Admin database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	funct_initial_audit_update
	Status=$?
	if [[ $Status != 0 ]]; then
		PERFORM_CRON_STATUS=0
	fi
	echo ${DB_INSTANCE}
	export DB_INSTANCE
fi

# Make sure scripts directory exists
if [[ ! -d ${WORKING_DIR}/scripts ]]; then
	mkdir -p ${WORKING_DIR}/scripts
fi 

# Make sure parameter file exists and get parameters from .ini file
export INIFILE=$HOME/local/dba/logmnr/AuditDay.ini
if [ ![ -a ${INIFILE} ]]; then
	echo "${PROGRAM_NAME} failed: Parameter file ${INIFILE} doesn't exist"
	SendNotification "Parameter file ${INIFILE} doesn't exist"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi

INILINE=`cat ${INIFILE} |grep ${ORACLE_SID}`
if [ "x$INILINE" == "x" ]; then
	echo "AUDIT_DAY_FAIL: ${ORACLE_SID}, Dictionary not found in INI file"
	SendNotification "${PROGRAM_NAME}:  ${ORACLE_SID}, there is no line in the ${INIFILE} for this SID"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi

ADMIN_USER=`echo $INILINE | awk '{print $2}'`
if [ "x$ADMIN_USER" == "x" ]; then
	echo "AUDIT_DAY_FAIL: ${ORACLE_SID}, User not found in parameter file"
	SendNotification "${ORACLE_SID}, User not found in parameter file"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi

ADMIN_PASS=`echo $INILINE | awk '{print $3}'`
if [ "x$ADMIN_PASS" == "x" ]; then
	echo "AUDIT_DAY_FAIL: ${ORACLE_SID}, Encrypted password not found in INI file"
	SendNotification "${PROGRAM_NAME}: ${ORACLE_SID}, Encrypted password not found in INI file"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi
ADMIN_CODE=`/u01/app/oracle/local/dba/logmnr/GetDec.cmd ${ADMIN_PASS}`
if [ "x$ADMIN_CODE" == "x" ]; then
	echo "AUDIT_DAY_FAIL: ${ORACLE_SID}, Encrypted password not found in INI file"
	SendNotification "${PROGRAM_NAME}: ${ORACLE_SID}, Encrypted password not found in INI file"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi

DICTIONARY=`echo $INILINE | awk '{print $4}'`
if [ "x$DICTIONARY" == "x" ]; then
	echo "AUDIT_DAY_FAIL: ${ORACLE_SID}, Dictionary not found in INI file"
	SendNotification "${PROGRAM_NAME}:  ${ORACLE_SID}, Dictionary not found in INI file"
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
	exit 0
fi

# Test the connection defined in the .ini file
TEST_CONNECTION=`${ORACLE_HOME}/bin/sqlplus -s  ${ADMIN_USER}/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
	select 1 from dual;
	exit
EOF`
TEST_CONNECTION=`echo -e $TEST_CONNECTION|tr -d " "`
if [[ ${TEST_CONNECTION} != 1 ]]; then
	echo "${PROGRAM_NAME}: Login failed for user ${ADMIN_USER} in database ${INPUT_SID}"
	SendNotification "${PROGRAM_NAME}:  Login failed for user ${ADMIN_USER} in database ${INPUT_SID}" 
	if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
		PROCESS_STATUS='FAILURE'
		funct_final_audit_update
	fi
exit 0
fi

DDL_TABLE="AUDIT_DDL"
DML_TABLE="AUDIT_DML"

# Check if run date is the 2nd day of any month.  If it is, the tables need to be archived
# and the monthly table cleared
DOM=`date +%e`
if [ $DOM == 2 ]; then
	MON=`date +%b`
	typeset -i YEAR=`date +%Y`
	case $MON in
		"Jan")	LAST_MON="DEC";
			YEAR=$YEAR-1;
		;;
		"Feb")	LAST_MON="JAN";
		;;
		"Mar")	LAST_MON="FEB";
		;;
		"Apr")	LAST_MON="MAR";
		;;
		"May")	LAST_MON="APR";
		;;
		"Jun")	LAST_MON="MAY";
		;;
		"Jul")	LAST_MON="JUN";
		;;
		"Aug")	LAST_MON="JUL";
		;;
		"Sep")	LAST_MON="AUG";
		;;
		"Oct")	LAST_MON="SEP";
		;;
		"Nov")	LAST_MON="OCT";
		;;
		"Dec")	LAST_MON="NOV";
		;;
	esac

	HISTORY_DDL_TABLE=${DDL_TABLE}_${LAST_MON}_${YEAR}
	HISTORY_DML_TABLE=${DML_TABLE}_${LAST_MON}_${YEAR}

	`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
        create table ${HISTORY_DDL_TABLE} as (select * from ${DDL_TABLE});
        create table ${HISTORY_DML_TABLE} as (select * from ${DML_TABLE});
EOF`
	typeset -i DML_Count=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
	select count(*) from ${DML_TABLE};
	exit
EOF`
	typeset -i DDL_Count=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
	select count(*) from ${DDL_TABLE};
	exit
EOF`
	typeset -i DML_Count_History=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
	select count(*) from ${HISTORY_DML_TABLE};
	exit
EOF`
	typeset -i DDL_Count_History=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
	select count(*) from ${HISTORY_DDL_TABLE};
	exit
EOF`
	if [[ ${DML_Count} -ne ${DML_Count_History} ]]; then
		SendNotification "Archiving DML Audit Data Failed"
		exit 0
	fi
	if [[ ${DDL_Count} -ne ${DDL_Count_History} ]]; then
		SendNotification "Archiving DDL Audit Data Failed"
		exit 0
	fi

	`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
	set heading off
	set feedback off
        truncate table ${DDL_TABLE};
        truncate table ${DML_TABLE}; 
	exit
EOF`

fi

# Prepare the logminer parameters and set up to use all logs from the last backup location
NumericDate=`date +%Y%m%d`
LOGMINER_SETUP_FILE_LOC=$HOME/local/dba/logmnr/scripts
LOGMINER_SETUP_FILE=$LOGMINER_SETUP_FILE_LOC/Logminer_${ORACLE_SID}_${NumericDate}.sql
TEMP_FILE=$LOGMINER_SETUP_FILE_LOC/tempfile.txt
ARCLOG_FILE=$LOGMINER_SETUP_FILE_LOC/arclogfile.txt

status=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
	set feedback off
	set heading off
	set termout off
	set pagesize 0
	set linesize 132
	spool ${ARCLOG_FILE}
        select 'exec DBMS_LOGMNR.add_logfile (options => DBMS_LOGMNR.addfile,logfilename =>  ''' || name || ''');'
        from v\\$archived_log
        where first_time >= trunc(sysdate-1,'DD') and first_time < trunc(sysdate,'DD') and
        upper(name) not like '%SB_%';
	spool off
	exit
EOF`

QUOTE=\'
echo '' > ${TEMP_FILE}
echo "exec dbms_logmnr.start_logmnr(dictfilename =>${QUOTE}${DICTIONARY}${QUOTE})" >> ${TEMP_FILE}
echo "exec dbms_logmnr.start_logmnr(OPTIONS =>  DBMS_LOGMNR.DDL_DICT_TRACKING)" >> ${TEMP_FILE}

cat ${ARCLOG_FILE} ${TEMP_FILE} > ${LOGMINER_SETUP_FILE}
rm ${TEMP_FILE}
rm ${ARCLOG_FILE}

# Hold beginning counts so mail can be sent indicating how many records are added per day
typeset -i DDL_COUNT_BEFORE=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
        set heading off
        set feedback off
	select count(*) from ${DDL_TABLE};
	exit
EOF`
typeset -i DML_COUNT_BEFORE=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
        set heading off
        set feedback off
	select count(*) from ${DML_TABLE};
	exit
EOF`

# starting insert into Audit tables
echo  " Beginning audit table inserts "
`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF  
	set heading off   
	-- set feedback off
	alter session set nls_date_format='DD-MON-YYYY HH24:MI:SSSSS';

	@$LOGMINER_SETUP_FILE

	truncate table audit_temp;
			
	insert into audit_temp (id,  scn,  timestamp ,LOG_ID,SEG_OWNER,SESSION_INFO, OPERATION,SQL_REDO,SQL_UNDO)
	Select  audit_temp_seq.nextval, scn,  timestamp,LOG_ID,SEG_OWNER,SESSION_INFO,OPERATION,SQL_REDO,SQL_UNDO
        from v\\$logmnr_contents
        where seg_owner not in ('ANONYMOUS', 'CTXSYS', 'DBSNMP', 'MDSYS', 'ORDPLUGINS', 'ORDSYS','UNKNOWN',
                        'OUTLN', 'OVERNIGHT', 'PERFSTAT', 'RMAN', 'EA_ADMIN', 'SITESEE', 'SYS', 'SYSTEM', 'WMSYS', 'XDB')
		and operation not in ('INTERNAL','UNSUPPORTED');         

	execute get_audit_data;
  exit
EOF`

echo " Finished loading Audit Records "

# Check ending counts
typeset -i DDL_COUNT_AFTER=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
        set heading off
        set feedback off
	select count(*) from ${DDL_TABLE};
EOF`
typeset -i DML_COUNT_AFTER=`${ORACLE_HOME}/bin/sqlplus -s  ea_admin/${ADMIN_CODE} <<EOF
        set heading off
        set feedback off
	select count(*) from ${DML_TABLE};
EOF`


if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	if [[ ${MAIL_COUNT} > 0 ]]; then
		PROCESS_STATUS='WARNING'
	else
		PROCESS_STATUS='SUCCESS'
	fi
	funct_final_audit_update
fi


  
typeset -i NEW_DDL=${DDL_COUNT_AFTER}-${DDL_COUNT_BEFORE}
typeset -i NEW_DML=${DML_COUNT_AFTER}-${DML_COUNT_BEFORE}
echo " $NEW_DDL DDL records generated " > mail.dat
echo " $NEW_DML DML records generated" >> mail.dat
cat mail.dat | mail -s "${PROGRAM_NAME} ran for ${ORACLE_SID} for ${NumericDate}" team_dba@eagleaccess.com
rm mail.dat

# Purge Old Scripts
OLDFILES=`find ${LOGMINER_SETUP_FILE_LOC} -mtime +30`
if [ "$OLDFILES" != "" ] ; then
    rm ${OLDFILES}
    echo "Removing old script file(s): \n$OLDFILES";
fi

