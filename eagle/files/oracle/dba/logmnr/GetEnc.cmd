#!/usr/bin/perl -w
#
=head1  Maint.cmd

=head2  ABSTRACT:
		Encode/decode passwords	

=head2  ARGUMENTS:

=head2  REVISIONS:
		MAB 18-Aug-03	xxxxx-x  MAB Initial development

=cut


use strict;
use lib "$ENV{HOME}/local/dba/perllib";
use ITSCode;

my $UserResponse;

if (scalar(@ARGV) != 1) {
	print "Enter value: ";
	$UserResponse = <>;
}
else {
	($UserResponse) = @ARGV;
}
chomp $UserResponse;

# calc dval
my $dval = "";
for (my $idx = 33; $idx <= 126; $idx+=7) {
	$dval .= chr($idx);
}

EncodeData ($dval, $UserResponse);

print "\nEncoded value is:$UserResponse:\n";

1;
