#!/usr/bin/ksh
#set -x
check_sid()
{
 sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{echo $1}')

 if [[ -z $sid_in_oratab ]]
 then
  echo "There is no $ORACLE_SID entry in $ORATAB file"
  exit 1
 else
  echo " Valid SID passed..."
 fi
}

upgrade_db()
{
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<!
spool $upgradelog 
set echo on
@?/rdbms/admin/catupgrd.sql;
spool off
exit
!
}
#

start_db()
{
echo "inside startdb "
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<!
startup force;

exit
!
}

fn_post_upgrade_db()
{
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<!

spool /tmp/${ORACLE_SID}_catuppst.log
@?/rdbms/admin/catuppst.sql
spool off

spool /tmp/${ORACLE_SID}_utlrp.log
@?/rdbms/admin/utlrp.sql
spool off

spool /tmp/${ORACLE_SID}_utlu112s.log
@?/rdbms/admin/utlu112s.sql
spool off

!
}


## MAIN ##
#set -x
export ORATAB=/etc/oratab
#export ORACLE_SID=`ps -ef |grep pmon|grep -v grep| grep -v +ASM|awk -F_ '{echo $3}'`
export ORACLE_SID=`cat /etc/oratab|egrep ':N|:Y'|grep -v \*|grep -v +ASM|grep -v '^\#'|cut -f1 -d':'`
. /usr/bin/setsid.sh $ORACLE_SID

#export ORACLE_SID=$1

upgradelog=/tmp/${ORACLE_SID}_upgrade.log
# if [[ $1 = "" ]]
#  then
#  echo "Please provide the ORACLE_SID as a parameter"
#  exit 1
# fi

#check_sid

#
upgrade_db

if [ $? -ne 0 ]
 then
  echo "Problem upgrading the database... Exiting here.."
  exit 1
else
  echo "Upgrade is good...  "
fi
#
#
start_db
if [ $? -ne 0 ]
 then
  echo "Problem Starting the database... Exiting here.."
  exit 2
  else
  echo "  Database started successfully !!!!"
fi
#
fn_post_upgrade_db
if [ $? -ne 0 ]
 then
  echo "Problem Running Post Upgrade Scripts... Exiting here.."
exit 3
fi





STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<!
col comp_name for a45
set linesize 200
set feed off
set echo off
set head off
spool /tmp/db_registry.txt
select comp_name,version,status from dba_registry where status='INVALID';
spool off
!`

STATUS=`grep "INVALID" /tmp/db_registry.txt`

echo $STATUS

if [[ $STATUS = "" ]]
then
echo "All is well.. All is well.."
exit 0
else
echo "Not good.."
exit 1
fi

exit 0

