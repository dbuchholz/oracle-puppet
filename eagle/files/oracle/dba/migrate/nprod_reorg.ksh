#!/bin/ksh
#
# Changes made 
#********************************************************************************************
# 20160210 - Added logic to avoid duplication in the remap_schema vairable for USERS tablespace
# 20160210 - Added logic to  take care if a schema has objects in two different tablespaces
# 20160212 - Added directory structures for logs and SQLs
# 20160212 - Modified -d switch.
# 20160223 - Added function to check for default tablespace is System
# 20160223 - Added function to convert empty tablespaces to BFT as this will not get captured in main logic
# 20160226 - Added function to address many to one scenario (n:1)
# 20160303 - Added function to address  users with just quota on the tablespaces (no objects)
# 20160307 - Added function to maually give dbms_lock grant to Application schema (fn_grantDlockManual)
# 20160309 - Added function to collect schema stats (fn_GatherStats)
# 20160321 - Added function to delete dumpfile after successful run (fn_remDumpFiles)
# 20160408 - added code to append 'reorg' after /dbimport/oracle
# 20160811 - Modified fn_compareObjCnt function to remove "$" in the schema owner comparison (tr -d)
# 20160811 - Added function fn_grantUsersBFT to give grant to users who have objects to USERS_BFT
# 20160811 - Added function fn_checkSystemTBS to check the Systems objects are self contained.
# 20160811 - excluded UNDOTBS1 from fn_RenameDFiles function.
#
#
#
#
#
#
fn_message()
{
echo "$(date +%Y%m%d-%T) ${1}" | tee -a ${_reorgLog}
}

fn_masterlog()
{
  echo "${1}" | tee -a ${_mstrReorgLog}
}

fn_segment_count()
{
if [[ $eflg == 'Y' || ($pflg == 'Y' && ${1} == 'Target') ]]
then
echo " " | tee -a ${_reorgLog}
echo "------------------------------------------------------------------" | tee -a ${_reorgLog}
printf "%-20s\t%-30s\t%-30s\n" "Owner" "Tablespace Name" "Count " | tee -a ${_reorgLog}
echo "------------------------------------------------------------------" | tee -a ${_reorgLog}
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
select  distinct 'Y '||owner, default_tablespace, count(a.object_name)
    from dba_objects a, dba_users b
         where default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
         and a.owner = b.username
         having count(a.object_name) > 0
      group by owner, default_tablespace
order by 1;
EOF
)|grep -i 'Y '| while read junk2 OWNM TBSNM TBSCNT
do
        if [ ! -z ${OWNM} ]
        then
        printf "%-20s\t%-30s\t%-30s\n" "${OWNM}" "$TBSNM" "$TBSCNT" | tee -a ${_reorgLog}
        echo "${1},${OWNM},$TBSNM,$TBSCNT" >> $segmentCntLog
        fi
done
    fn_masterlog "INFO: Successfully queried the database object count at ${1}"
else
   if [[ ! -f ${segmentCntLog}_org ]]
   then
       echo " before move"
       mv $segmentCntLog ${segmentCntLog}_org
       echo "after move"
   fi
       echo " before cat"
       cat ${segmentCntLog}_org | grep -i ${1} > $segmentCntLog
       echo "after cat"
       fn_masterlog "INFO: Successfully moved the ${1} entries from Original file "
fi
}

fn_getInvalidObj()
{
if [[ $eflg == 'Y' || ($pflg == 'Y' && ${1} == 'Target') ]]
#if [[ $eflg == 'Y' ]]
then
   invalidobj=`(sqlplus -s / as sysdba  <<EOF
   set heading off
   set feedback off
   select 'Y '||count(1) from dba_objects where status <> 'VALID';
EOF
)|grep -i 'Y '| awk '{print $2}'`
   if [ ! -z ${invalidobj} ]
   then
    fn_message "${1}:Total Invalid Objects -> ${invalidobj}"
    echo "${1},Invalidcnt,${invalidobj}" >> $segmentCntLog
    fn_masterlog "INFO: Successfully queried Invalid object count at ${1}"
   fi
fi
}


fn_check_ts()
{
if [[ $eflg == 'Y' ]]
then
fn_message " Dynamically creating dropuser.sql, ${_DropOldTbspSql}, ${_crBFTbspSql} Scripts"
rm ${_crBFTbspSql}
rm ${_dropUserSql}
rm ${_DropOldTbspSql}
rm ${_renTbspSql}
rm ${_grantDLckSql}
i=0
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off


select distinct 'Y '||OWNER||':'|| default_tablespace||':'|| rtrim(nvl(substr(owner,'0', instr(owner, 'DBO')-1),owner),'_')
                   from dba_objects a, dba_users b
                        where 
-- account_status = 'OPEN'
                        default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
                        and a.owner = b.username
                        having count(a.object_name) > 0
                     group by owner, default_tablespace
union
select distinct 'Y '||OWNER||':'|| TABLESPACE_NAME||':'|| rtrim(nvl(substr(owner,'0', instr(owner, 'DBO')-1),owner),'_')
                       from dba_segments a, dba_users b
                      where 
-- account_status = 'OPEN'
                      a.owner = b.username
                      and tablespace_name not in ('SYSTEM','SYSAUX','UNDOTBS1')
                     group by owner, tablespace_name
 union
 select distinct 'Y '||a.username||':'|| TABLESPACE_NAME||':'|| rtrim(nvl(substr(a.username,'0', instr(a.username, 'DBO')-1),a.username),'_')
 from dba_users a, dba_ts_quotas b
 where account_status = 'OPEN'
                         and default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
                         and a.username = b.username
                         and bytes > 0
minus
select distinct 'Y '||b.OWNER||':'|| a.TABLESPACE_NAME||':'|| rtrim(nvl(substr(b.owner,'0', instr(b.owner, 'DBO')-1),b.owner),'_') from
    (
     select tablespace_name, count (distinct owner) count from dba_segments
    where tablespace_name not in  ('SYSTEM','SYSAUX','USERS') group by tablespace_name
    having count(distinct owner) > 1) a,
    dba_segments b
   where a.tablespace_name = b.tablespace_name;
EOF
)|grep -i 'Y '| while read junk2 TS_NAME
do
userexist='N'
        if [ ! -z ${TS_NAME} ]
        then
              owner=`echo $TS_NAME | cut -d ":" -f1`
              tsname1=`echo $TS_NAME | cut -d ":" -f2`
              tsname2=`echo $TS_NAME | cut -d ":" -f3`
echo " "
echo " "
#echo $owner:$tsname1:$tsname2
echo " "
              if [[ $i -eq 0 ]]
              then
                    fn_check_def_TS "${tsname1}"
                    if [[ ${DefUsrTS} == 'Y' ]]
                    then
                        fn_check_segments "${tsname1}" "${owner}"
                        if [[ ${_SegCnt} -gt 0 ]]
                        then
                           echo "Default Users tablespace : segments found  -> ${_SegCnt} i=$i"
                           echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
                           echo "alter user ${owner} quota unlimited on ${tsname1}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
#                          fn_create_common "alter user ${owner} quota unlimited on ${tsname1}_BFT;"
                           own_name=$owner
                           remapts=${tsname1}:${tsname1}_BFT
                            i=1
                        else
                           echo " Default USERS tablespace and No segments found.. leaving it alone."
                        fi
                    else
#                          echo "Schema tablespace is not USERS. i=0"
                           own_name=$owner
                           remapts=${tsname1}:${tsname2}_BFT
                           echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
                           echo " grant execute on sys.dbms_lock to $owner;" | tee -a >> ${_grantDLckSql}
                           fn_check_create_tbs "${tsname2}" "${tsname1}" "${owner}"

                            i=1
                    fi
              else
                 echo $own_name | grep -wq $owner
                 status=$?
                 if [[ $status -eq 0 ]]
                 then
                    userexist='Y'
#----------
                        # routine to check if the tablespace is default tablespace of DB
                        fn_check_def_TS "${tsname1}"
                        if [[ ${DefUsrTS} == 'Y' ]]
                        then
                          fn_message " ${tsname1}  Tablespace is the default tablespace for this database.  Cannot be dropped"
                          own_name=${own_name},${owner}
#---------------
                          echo "Checking for duplicate in the REMAP string - user exist in OWN_NAME"
                           echo ${tsname1}
                           echo "${tsname1}"_BFT
                           remap1="${tsname1}":"${tsname1}"_BFT
                           echo $remap1
                           echo $remapts
                           echo $remapts | grep -wq "$remap1"
                           tsstatus=$?
                           if [[ ${tsstatus} -eq 0 ]]
                           then
                                echo " $remapts"
#                                sleep 5
                                echo " Found the string pair... Not adding "
                            else
                                echo "USER EXISTS in OWN_NAME Pair not found in remapts string"
                                 remapts=$remapts,${tsname1}:${tsname1}_BFT
                            fi
#---------------
                        else
                          echo "User exists and does not belong to USERS tablespace.. adding drop TS and adding TBS to remaps string"
                          echo " Drop tablespace ${tsname1} including contents and datafiles;" | tee -a >> ${_DropOldTbspSql}
                          remapts=$remapts,${tsname1}:${tsname2}_BFT
                        fi
                        # END Check #
# END Check what happens if a user has two tablespaces -- future
                 else
                    echo "USER not in OWN_NAME"
                    fn_check_def_TS "${tsname1}"
#                       echo " default TB -> ${DefUsrTS}"
                    if [[ ${DefUsrTS} == 'Y' ]]
                    then
                        echo "default TBS Users"
                        fn_check_segments "${tsname1}" "${owner}"
                        if [[ ${_SegCnt} -gt 0 ]]
                        then
                           echo "Default Users tablespace : segments found  -> ${_SegCnt}"
                           own_name=${own_name},${owner}
                           echo ${own_name}
                           echo "Checking for duplicate in the REMAP string No2."
                           remap1="${tsname1}":"${tsname1}"_BFT
#                           echo $remap1
#                           echo $remapts
#                           echo $remapts | grep -wq "${tsname1}":"${tsname1}"_BFT
                           echo $remapts | grep -wq "$remap1"
                           tsstatus=$?
                           if [[ ${tsstatus} -eq 0 ]]
                           then
#                                echo " $remapts"
                                echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
echo "alter user ${owner} quota unlimited on ${tsname1}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
                           else
                                echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
                                own_name=${own_name},${owner}
                                remapts=$remapts,${tsname1}:${tsname1}_BFT
                           fi
                        else
                           echo " Default USERS tablespace and No segments found.. leaving it alone."
                        fi
                    else
#                          echo "No Default users "
                           own_name=${own_name},${owner}
                           echo "Checking for duplicate in the REMAP string in Eagle tablespace"
			   remap1="${tsname1}":"${tsname1}"_BFT
#                           echo $remap1
#			   echo $remapts
			    echo $remapts | grep -wq "$remap1"
			   tsstatus=$?
                           if [[ ${tsstatus} -eq 0 ]]
                           then
#                                echo " $remapts"
                                echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
echo "alter user ${owner} quota unlimited on ${tsname1}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
                           else
                                echo " Drop user $owner cascade;" | tee -a >> ${_dropUserSql}
                                echo " grant execute on sys.dbms_lock to $owner;" | tee -a >> ${_grantDLckSql}
                                remapts=$remapts,${tsname1}:${tsname2}_BFT
                                fn_check_create_tbs "${tsname2}" "${tsname1}" "${owner}"
                           fi
                    fi

                 fi
            fi
        fi
done
fn_check_ts2
fn_check_ts3
        echo $own_name > ${_expSchema}
        echo $remapts > ${_remapTbs}
echo " "
echo " "
echo " "
echo " "
echo $own_name
echo $remapts

fi
#exit 0
        fn_grantUsersBFT
        fn_create_expdp 
}





fn_check_ts2()
{
fn_message " ---------------------------------------------------------- "
fn_message " Many to one scenario (ie. many Schema having objects in one tablespace)"
fn_message " ---------------------------------------------------------- "

(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
select distinct 'Y '||b.OWNER||':'|| a.TABLESPACE_NAME||':'|| rtrim(nvl(substr(b.owner,'0', instr(b.owner, 'DBO')-1),b.owner),'_') from
       (
        select tablespace_name, count (distinct owner) count from dba_segments
       where tablespace_name not in  ('SYSTEM','SYSAUX','USERS') group by tablespace_name
       having count(distinct owner) > 1) a,
       dba_segments b
      where a.tablespace_name = b.tablespace_name;
EOF
)|grep -i 'Y '| while read junk2 TS_NAME9
do
if [ ! -z ${TS_NAME9} ]
        then
              owner10=`echo $TS_NAME9 | cut -d ":" -f1`
              tsname11=`echo $TS_NAME9 | cut -d ":" -f2`
              tsname12=`echo $TS_NAME9 | cut -d ":" -f3`
#echo " $owner10, $tsname11"
              echo $own_name | grep -wq $owner10
              status2=$?
                       if [[ $status2 -ne 0 ]] # user Not exist in the string OWN_NAME2
                       then
                           echo " Drop user $owner10 cascade;" | tee -a >> ${_dropUserSql}
                           echo " grant execute on sys.dbms_lock to $owner10;" | tee -a >> ${_grantDLckSql}
                           own_name=${own_name},${owner10}
echo "alter user ${owner10} quota unlimited on ${tsname11}_BFT;" | tee -a >> ${_grantTbsQuotaSql}

#                            echo "Checking for duplicate in the REMAP string"
                             remap1="${tsname11}":"${tsname11}"_BFT
                             echo $remapts | grep -wq "$remap1"
                             tsstatus2=$?
                                    if [[ ${tsstatus2} -eq 0 ]]
                                    then
#                                       echo " $remapts"
                                        echo " Found the string pair in the remap... Not adding "
echo "alter user ${owner10} quota unlimited on ${tsname11}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
                                    else
                                        remapts=$remapts,${tsname11}:${tsname11}_BFT
                                       fn_check_create_tbs "${tsname11}" "${tsname11}" "${tsname11}"
                                    fi
                        else  # User Exists

#                              echo "Checking for duplicate in the REMAP string"
                              remap1="${tsname11}":"${tsname11}"_BFT
                              echo $remapts | grep -wq "$remap1"
                              tsstatus2=$?
                                       if [[ ${tsstatus2} -eq 0 ]]
                                       then
                                           echo " Found the string pair in the remap... Not adding "
echo "alter user ${owner10} quota unlimited on ${tsname11}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
                                       else
                                            remapts=$remapts,${tsname11}:${tsname11}_BFT
                                            fn_check_create_tbs "${tsname11}" "${tsname11}" "${tsname11}"
                                       fi
                        fi
else

echo "TS_NAME9 is empty"
fi

done

own_name=`echo $own_name | sed 's/^,//g'`
remapts=`echo $remapts | sed 's/^,//g'`
echo ""
echo ""
echo ""
echo ""
echo "After 2 query$own_name"
echo ""
echo "after 2 query ${remapts}"
echo ""
echo ""
echo ""
echo ""
}


fn_check_ts3()
{

fn_message " ---------------------------------------------------------- "
fn_message " Only quota  scenario "
fn_message " ---------------------------------------------------------- "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off

   with  t1 as
(
select c.owner, c.tablespace, c.owner as new_tablespace from
(
select OWNER as owner, default_tablespace as tablespace
                     from dba_objects a, dba_users b
                            where
                            default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
                            and a.owner = b.username
                            having count(a.object_name) > 0
                         group by owner, default_tablespace
    union
    select distinct OWNER as owner, TABLESPACE_NAME as tablespace
                          from dba_segments a, dba_users b
                         where
                            a.owner = b.username
                         and tablespace_name not in ('SYSTEM','SYSAUX','UNDOTBS1')
                        group by owner, tablespace_name
    union
    select distinct a.username as owner , TABLESPACE_NAME as tablespace
    from dba_users a, dba_ts_quotas b
    where account_status = 'OPEN'
                            and default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
                            and a.username = b.username
                            and bytes > 0
    minus
(select distinct b.OWNER, a.TABLESPACE_NAME from
       (
        select tablespace_name, count (distinct owner) count from dba_segments
       where tablespace_name not in  ('SYSTEM','SYSAUX','USERS') group by tablespace_name
       having count(distinct owner) > 1) a,
       dba_segments b
      where a.tablespace_name = b.tablespace_name
)
   ) c
   ) ,
   t2 as
      ( select distinct a.username as owner , TABLESPACE_NAME as tablespace
from dba_users a, dba_ts_quotas b
where account_status = 'OPEN'
and default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
and a.username = b.username
and b.tablespace_name <> 'USERS'
and bytes = 0
)
select 'Y '||t2.owner||':'|| nvl(t1.tablespace,t2.tablespace)||':'|| nvl(rtrim(nvl(substr(t1.owner,'0', instr(t1.owner, 'DBO')-1),t1.owner),'_'),t2.tablespace) as new_tablespace
   from t2 left join t1 on (t1.tablespace = t2.tablespace )
;
EOF
)|grep -i 'Y '| while read junk2 TS_NAME10
do
if [ ! -z ${TS_NAME10} ]
        then
              owner20=`echo $TS_NAME10 | cut -d ":" -f1`
              tsname21=`echo $TS_NAME10 | cut -d ":" -f2`
              tsname22=`echo $TS_NAME10 | cut -d ":" -f3`
#echo " $owner20, $tsname21 $tsname22"
              echo $own_name | grep -wq $owner20
              status2=$?
                       if [[ $status2 -ne 0 ]] # user Not exist in the string OWN_NAME2
                       then
                           echo " Drop user $owner20 cascade;" | tee -a >> ${_dropUserSql}
#                           echo " grant execute on sys.dbms_lock to $owner10;" | tee -a >> ${_grantDLckSql}
                           own_name=${own_name},${owner20}
echo "alter user ${owner20} quota unlimited on ${tsname22}_BFT;" | tee -a >> ${_grantTbsQuotaSql}

#                            echo "Checking for duplicate in the REMAP string"
                             remap1="${tsname21}":"${tsname22}"_BFT
                             echo $remapts | grep -wq "$remap1"
                             tsstatus3=$?
                                    if [[ ${tsstatus3} -eq 0 ]]
                                    then
#                                       echo " $remapts"
                                        echo " Found the string pair... Not adding "
                                    else
                                        remapts=$remapts,${tsname21}:${tsname22}_BFT
                                        fn_check_create_tbs "${tsname21}" "${tsname22}" "${tsname22}"
                                    fi
                        else  # User Exists

#                              echo "Checking for duplicate in the REMAP string"
                              remap1="${tsname21}":"${tsname22}"_BFT
                              echo $remapts | grep -wq "$remap1"
                              tsstatus3=$?
                                       if [[ ${tsstatus3} -eq 0 ]]
                                       then
                                           echo " Found the string pair... Not adding "
echo "alter user ${owner20} quota unlimited on ${tsname22}_BFT;" | tee -a >> ${_grantTbsQuotaSql}
                                       else
                                            remapts=$remapts,${tsname21}:${tsname22}_BFT
                                          fn_check_create_tbs "${tsname21}" "${tsname22}" "${tsname22}"
                                       fi
                        fi
else

echo "TS_NAME10 is empty"
fi

done

own_name=`echo $own_name | sed 's/^,//g'`
remapts=`echo $remapts | sed 's/^,//g'`
echo ""
echo ""
echo ""
echo ""
echo " 3->$own_name"
echo ""
echo "3 -> ${remapts}"
echo ""
echo ""
echo ""
echo ""
}
#----------------------------------------------------------------------
#
#                 Generate script for Export 
#
#----------------------------------------------------------------------

fn_create_expdp()
{
if [[ $eflg == 'Y' ]]
then

  _CheckDir=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select 'Y '||directory_name from dba_directories where upper(directory_name) = 'CMP_MIGRATION';
EOF
)|grep -i 'Y '| awk '{print $2}'`
	if [ ! -z ${_CheckDir} ]
  	then
     	   fn_message "Found ${_CheckDir} Directory in the Database " 
           fn_lclsql_common "drop directory cmp_migration;"
        fi 
     fn_message " CMP_MIGRATION - Directory Not Found" 
     fn_message "Creating Directory CMP_MIGRATION ........"
     fn_create_common "create directory cmp_migration as '$exportDir';"
     fn_masterlog "INFO: Datapump Directory Created  Successfully."
     fn_message " Preparing to export data for the applicaiton schema listed below:"
     fn_masterlog "INFO: Preparing to export data for applicaition schemas"
     fn_message "${1}"
     fn_message " "
#
     dumpfiles=$(ls $exportDir/cmpexport_*.dmp 2> /dev/null | wc -l)
     if [ "$dumpfiles" != "0" ]
     then
        fn_message "  Previous export file exist..  Clearing "
        rm $exportDir/cmpexport_*.dmp
        fn_masterlog "INFO: Successfully removed previous Export dumfiles."
     else
        fn_message " Export file does not exist.  Good to proceed with export "
        fn_masterlog "INFO: Starting Export....."
     fi

     if [[ -f ${_expSchema} ]]
     then
        schemalst=`cat ${_expSchema}`
     else
        fn_message " Cannot find ${_expSchema}.  Exiting...."
        exit 1
     fi
expdp \' / as sysdba \' directory=CMP_MIGRATION parallel=$parallel dumpfile=cmpexport_%U.dmp logfile=cmpexport.log schemas=${schemalst} exclude=statistics  >> ${_reorgLog} 2>&1

#
     fn_masterlog "INFO: Checking for Export Errors..."
     fn_check_expimp_errors "export"
     if [[ $exp_status == "Y" ]]
     then
         fn_message "  Export successfully Completed ... No errors. " 
         fn_masterlog "INFO: Export successfully Completed ... No errors."
         touch ${_expSuccess}
#exit 0
     else
            fn_message "Export has errors ....  Please check the log and rerun the process ..Exiting"
            fn_masterlog "ERROR: Export has errors..  Exiting..."
         exit 1
     fi
fi
#
# END of EXPORT PROCESS
#
if [[ $iflg == 'Y' ]]
then
         fn_masterlog "INFO: Checking for BFTs and create if needed......"
         fn_createBFTs
         fn_message "Checking for Datapump directory"
         fn_ExportDirCreate
         fn_message " Dropping users.. "
         fn_drop_users
### --> c/20160127 - drop tablespace to release space before import
#           if [[ $BFT == 'Y' ]]
           if [[ -f ${_remBFTFlg} ]]
	    then
	        fn_message " Dropping old tablespaces "
	        fn_drop_oldTS
           fi

           if [[ -f ${_remapTbs} ]]
	   then
	      remaplst=`cat ${_remapTbs}`
	   else
	      fn_message " Cannot find ${_remapTbs}.  Exiting...."
	      exit 1
	   fi

         fn_message "Proceeding to import "
         fn_masterlog "INFO: Starting Import....."
         fn_message " Importing data .........."

         echo "impdp \' / as sysdba \' directory=CMP_MIGRATION dumpfile=cmpexport_%U.dmp logfile=cmpimport.log  remap_tablespace=${remaplst} table_exists_action=replace parallel=$parallel"
impdp \' / as sysdba \' directory=CMP_MIGRATION dumpfile=cmpexport_%U.dmp logfile=cmpimport.log  remap_tablespace=${remaplst} table_exists_action=replace parallel=$parallel   >> ${_reorgLog} 2>&1

         fn_masterlog "INFO: Import Completed....."
         fn_masterlog "INFO: Checking for Import Errors..."
         fn_check_expimp_errors "import"
         if [[ $imp_status == "Y" ]]
         then
            touch ${_impSuccess}
            fn_masterlog "INFO: Imported Completed with known errors [ORA-39082] caused by inter schema grants"
            fn_masterlog "      This will be fixed once we run utlrp.sql later in the re-org process"
#
            fn_message " Compiling Invalid objects after Import....."
            fn_grantDlockManual
            fn_grantDlock
            fn_masterlog "INFO: Successfully granted dbms_lock to Application schemas"
            fn_invalid_objects
            fn_masterlog "INFO: Successfully compiled invalid objects"
	    # copy the import and export logs to the local directory
	     fn_message "Copying datapump logs to local directory"
	     fn_fileMgmt "$exportDir/cmpexport.log" "copy" "${_cmpExpLog}"
	     fn_fileMgmt "$exportDir/cmpimport.log" "copy" "${_cmpImpLog}"
	     fn_masterlog "INFO: Successfully copied Data Pump logs to local directory.."
#exit 0
         else
             # copy the import and export logs to the local directory
	     fn_message "Copying datapump logs to local directory"
	     fn_fileMgmt "$exportDir/cmpexport.log" "copy" "${_cmpImpLog}"
	     fn_fileMgmt "$exportDir/cmpimport.log" "copy" "${_cmpImpLog}"
	     fn_masterlog "INFO: Successfully copied Data Pump logs to local directory.."
            fn_message "  Errors found during the import.  Please check "
            fn_masterlog "ERROR: Import has errors..  Exiting..."
            exit 1
         fi
fi
     
}
#------------------------------------

fn_fileMgmt()
{
  if [ -f "${1}" ]
  then
     if [[ "${2}" == "remove" ]]
     then
         fn_message " Removing ${1}"
         rm ${1}
     elif [[ "${2}" == "copy" ]]
     then
         fn_message " Copying file ${1}" 
         cp ${1} ${3}
     fi
  fi
}



fn_create_common()
{
     sqlplus -s / as sysdba <<EOF >> $_reorgLog
     set serveroutput on
     set echo on
     set time on
     set timing on
     spool ${_createddlLog} append
     ${1}
     spool off
     set echo off
     set time off
     set timing off
     exit
EOF
}

#------------------------------------
fn_check_create_tbs()
{
 BFT=N
 BFTTS=${1}_BFT

_CheckTbs=`(sqlplus -s / as sysdba <<EOF
        select 'Y '||tablespace_name from dba_tablespaces where upper(tablespace_name) = '${BFTTS}';
EOF
)|grep -i 'Y '| awk '{print $2}'`
if [ ! -z ${_CheckTbs} ]
  then
     fn_message "Found BIGFILE Tablespace ${BFTTS} in the Database "
     fn_masterlog "INFO: Bigfile Tablespace ${BFTTS}.  Moving on.."
else
     if [[ ! -f ${_remBFTFlg} ]]
     then
        touch ${_remBFTFlg}
     fi
     BFT='Y'
     fn_message " Cannot find BigFile tablespace ${BFTTS}"
     echo " Drop tablespace ${2} including contents and datafiles;" | tee -a >> ${_DropOldTbspSql}
     echo " alter tablespace ${BFTTS} rename to ${3}_TS;" | tee -a >> ${_renTbspSql}
     echo "create bigfile tablespace" ${BFTTS}" datafile '+DATA' size 2G autoextend on extent management local autoallocate;" | tee -a >> ${_crBFTbspSql}
     echo "alter user ${3} quota unlimited on  ${BFTTS};" | tee -a >> ${_grantTbsQuotaSql}
     fn_message "create bigfile tablespace ${BFTTS} datafile '+DATA' size 2G autoextend on extent management local autoallocate;"

     fn_create_common "create bigfile tablespace ${BFTTS} datafile '+DATA' size 2G autoextend on extent management local autoallocate;"
#     fn_create_common "alter user ${3} quota unlimited on  ${BFTTS};"
     fn_masterlog "INFO: Successfully created ${BFTTS} Bigfile Tablespace"
  fi

}

fn_check_def_TS()
{
DefUsrTS='N'
_CheckDefTS=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select 'Y '||property_name from database_properties where upper(property_value) = '${1}' and property_name = 'DEFAULT_PERMANENT_TABLESPACE';
EOF
)|grep -i 'Y '| awk '{print $2}'`
  if [ ! -z ${_CheckDefTS} ]
  then
     echo " ${1} Tablespace is the default tablespace for this database"
     DefUsrTS='Y'
  else
     DefUsrTS='N'
  fi
}

fn_check_segments()
{
_SegCnt='0'
#echo "select 'Y '||count(segment_name) from dba_segments  where tablespace_name = '${1}' and owner='${2}' group by owner;"
_SegCnt=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select 'Y '||count(segment_name) from dba_segments  where tablespace_name = '${1}' and owner='${2}' group by owner;
EOF
)|grep -i 'Y '| awk '{print $2}'`
#echo "segcount is -> ${_SegCnt}"
}


fn_invalid_objects()
{
rm ${_compInvdObjSql}
	sqlplus -s / as sysdba <<EOF >> $_reorgLog
	set heading off
	set feedback off
	spool ${_compInvdObjSql}
	select 'alter ' || decode(object_type, 'PACKAGE BODY', 'PACKAGE', object_type) ||
	' ' || owner || '.' ||
	object_name || '  compile ' ||
	decode(object_type, 'PACKAGE BODY', 'BODY','')  ||';'
	AS  RECOMPILE
	from ALL_objects
	where status = 'INVALID'
	and object_type in
	('PROCEDURE','FUNCTION','PACKAGE','PACKAGE BODY','TRIGGER','VIEW')
	ORDER BY 1;
	spool off
	set echo on
        spool ${_compInvldObjLog}
        @ ${_compInvdObjSql}
        set echo off
	set feedback on
	set heading on
        spool off
EOF
}


fn_drop_users()
{
 if [[ -f ${_dropUserSql} ]]
then
    for dusr in `cat ${_dropUserSql} | tr -s " " | cut -d " " -f4`
    do
      fn_message " Dropping user ${dusr}"
      dropUserCnt=`(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select count(1) from dba_users where username = '$dusr';
EOF
        )`
        if [[ ${dropUserCnt} -gt 0 ]]
        then
        fn_message  " Drop user $dusr cascade;"
        fn_lclsql_common "Drop user $dusr cascade;"
        else
        fn_message " Cannot find user $dusr.  Moving on.."
        fi
    done
    fn_masterlog "INFO: Successfully Dropped all Application Schemas."
fi
}

fn_drop_oldTS()
{
 if [[ -f ${_DropOldTbspSql} ]]
then
    for otbs in `cat ${_DropOldTbspSql} | tr -s " " | cut -d " " -f4`
    do
      fn_message " Dropping tablespace ${otbs}"
      dropOtbsCnt=`(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select count(1) from dba_tablespaces where tablespace_name = '$otbs';
EOF
        )`
        if [[ ${dropOtbsCnt} -gt 0 ]]
        then
        fn_message  " Drop tablespace $otbs including contents and datafiles;"
        fn_lclsql_common "Drop tablespace $otbs including contents and datafiles;"
        else
        fn_message  " Cannot find tablespace $otbs.  Moving on.."
        fi
    done
    fn_masterlog "INFO: Successfully Dropped all the old tablespaces."
else
    fn_message "Cannot find ${_DropOldTbspSql}..  Exiting. "
    fn_masterlog "ERROR: Cannot find ${_DropOldTbspSql}..  Exiting. "
    exit 1
fi
}

fn_createBFTs()
{
 if [[ -f ${_crBFTbspSql} ]]
then
    for cbfts in ` cat ${_crBFTbspSql} | grep create | tr -s " " | cut -d " " -f4`
    do
      cBFTCnt=`(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select count(1) from dba_tablespaces where tablespace_name = '$cbfts';
EOF
        )`
        if [[ ${cBFTCnt} -eq 0 ]]
        then
        fn_message  " create bigfile tablespace $cbfts datafile '+DATA' size 2G autoextend on extent management local autoallocate;"
        fn_lclsql_common "create bigfile tablespace $cbfts datafile '+DATA' size 2G autoextend on extent management local autoallocate;"
        else
        fn_message  " Bigfile tablespace $cbfts exists.  Moving on.."
        fi
    done
    fn_masterlog "INFO: Successfully Dropped all the old tablespaces."
else
    fn_message "Cannot find ${_crBFTbspSql}..  Exiting. "
    fn_masterlog "ERROR: Cannot find ${_crBFTbspSql}..  Exiting. "
    exit 1
fi
}


fn_rename_tblspc()
{
 rm rename_tblspc.log
 sqlplus -s / as sysdba <<EOF >> $_reorgLog
        set heading off
        set feedback off
        set echo on
        spool rename_tblspc.log
        @ ${_renTbspSql}
        spool off
        set heading on
        set feedback on
        set echo off
EOF
}

fn_rename_TS()
{
 if [[ -f ${_renTbspSql} ]]
then
while IFS= read -r line
do
  fn_message "${line}"
 fn_lclsql_common "${line}" 
# fn_masterlog "INFO: ${line} Successful"
done < ${_renTbspSql}
fi
}


fn_grantDlock()
{
 if [[ -f ${_grantDLckSql} ]]
then
while IFS= read -r dline
do
  fn_message "${dline}"
 fn_lclsql_common "${dline}"
# fn_masterlog "INFO: ${dline} Successful"
done < ${_grantDLckSql}
else
 fn_message "WARNING: Cannot find ${_grantDLckSql}.  There will be some invalid objects after the re-org process"
 fn_masterlog "WARNING: Cannot find ${_grantDLckSql}.  There will be some invalid objects after the re-org process"
 fn_masterlog "To Fix, grant dbms_lock to application schema and run utlrp.sql"
fi
}


fn_autoExtendTempFiles()
{
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select 'Y '|| file_id from dba_temp_files where AUTOEXTENSIBLE ='NO';
EOF
)|grep -i 'Y '| while read junk2 TEMPFILEID
do
   if [ ! -z ${TEMPFILEID} ]
   then
      fn_message "Alter database tempfile ${TEMPFILEID} autoextend on;"
      fn_lclsql_common "Alter database tempfile ${TEMPFILEID} autoextend on;"
   fi
done
}

fn_AcctUnLock()
{
rm ${_acctLock}
rm ${_acctUnlock}
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select  distinct 'Y '||owner
       from dba_objects a, dba_users b
            where default_tablespace not in ('SYSTEM','SYSAUX','UNDOTBS1')
            and a.owner = b.username
            and account_status <> 'OPEN'
            having count(a.object_name) > 0
         group by owner, default_tablespace;
EOF
)|grep -i 'Y '| while read junk2 UNLOCK_USER
do
    if [ ! -z ${UNLOCK_USER} ]
        then
           echo " alter user ${UNLOCK_USER} account unlock;" | tee -a ${_acctUnlock}
           fn_lclsql_common "alter user ${UNLOCK_USER} account unlock;"
           echo " alter user ${UNLOCK_USER} account lock;" >> ${_acctLock}
    fi
done
}


fn_AcctLock()
{
 if [[ -f ${_acctLock} ]]
then
while IFS= read -r line
do
  fn_message "${line}"
 fn_lclsql_common "${line}"
# fn_masterlog "INFO: ${line} Successful"
done < ${_acctLock}
fi
}


fn_ExportDirCreate()
{
  _CheckDir=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select count(1) from dba_directories where upper(directory_name) = 'CMP_MIGRATION';
EOF
)`
if [[ ${_CheckDir} -eq 0 ]]
  then
     fn_message "Cannot find CMP_MIGRATION directory in the Database "
     fn_lclsql_common "create directory cmp_migration as '$exportDir';"
     fn_masterlog "INFO: Datapump Directory Created  Successfully."
else
     fn_lclsql_common "drop directory cmp_migration;"
     fn_lclsql_common "create directory cmp_migration as '$exportDir';"
fi
}

fn_ExportDirRemove()
{
  _CheckDir=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select count(1) from dba_directories where upper(directory_name) = 'CMP_MIGRATION';
EOF
)`
if [[ ${_CheckDir} -gt 0 ]]
  then
#     fn_message "Found ${_CheckDir} Directory in the Database "
     fn_message "Found CMP_MIGRATION directory in the Database "
     echo "drop directory cmp_migration;"
     fn_lclsql_common " drop directory cmp_migration;"
     fn_masterlog "INFO: Successfully dropped Data Pump Directory "
fi
}

fn_lclsql_common()
{
     sqlplus -s / as sysdba <<EOF
     set serveroutput on
     set echo on
     set time on
     set timing on
     spool ${_lclLog} append
     ${1}
     spool off
     set echo off
     set time off
     set timing off
     exit
EOF
}

fn_nonapp_TS()
{
fn_message " Migrating Non Applicaiton  tablespaces to Bigfile "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '|| tablespace_name from dba_tablespaces where tablespace_name not in
        (select tablespace_name from dba_segments)
        and contents <> 'TEMPORARY'
        and tablespace_name not in (select property_value from database_properties where property_name = 'DEFAULT_PERMANENT_TABLESPACE')
        and bigfile <> 'YES';
EOF
)|grep -i 'Y '| while read junk2 NONAPP_TS_NAME
do
   fn_message "create bigfile tablespace ${NONAPP_TS_NAME}_TS datafile '+DATA' size 1G autoextend on extent management local autoallocate;"
   fn_lclsql_common "create bigfile tablespace ${NONAPP_TS_NAME}_TS datafile '+DATA' size 1G autoextend on extent management local autoallocate;"

   fn_user_nonapp_ts "$NONAPP_TS_NAME"

   fn_message " drop tablespace ${NONAPP_TS_NAME} including contents and datafiles;"
   fn_lclsql_common "drop tablespace ${NONAPP_TS_NAME} including contents and datafiles;"
done

}

fn_user_nonapp_ts()
{
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||USERNAME from dba_ts_quotas where TABLESPACE_NAME =  '${1}';
EOF
)|grep -i 'Y '| while read junk2 NONAPP_TS_USER
do
    if [ ! -z ${NONAPP_TS_USER} ]
        then
           fn_message " alter user ${NONAPP_TS_USER} quota unlimited on ${1}_TS;"
           echo " alter user ${NONAPP_TS_USER} quota unlimited on ${1}_TS;" | tee -a >> ${_grantTbsQuotaSql}
#           fn_lclsql_common "alter user ${NONAPP_TS_USER} quota unlimited on ${1}_TS;"
    fi
done
}


fn_Temp_TS()
{
fn_message " Migrating Non-Default temporary  tablespace (Application specific) to Bigfile "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '|| tablespace_name from dba_tablespaces
        where contents = 'TEMPORARY' and bigfile <> 'YES'
        and tablespace_name not in
        ( select property_value from database_properties where property_name = 'DEFAULT_TEMP_TABLESPACE');
EOF
)|grep -i 'Y '| while read junk2 TEMP_TS_NAME
do
   fn_message "create bigfile temporary tablespace ${TEMP_TS_NAME}_TS tempfile '+DATA' size 10G autoextend on next 10G;"
   fn_lclsql_common "create bigfile temporary tablespace ${TEMP_TS_NAME}_TS tempfile '+DATA' size 10G autoextend on next 10G;"

   fn_user_temp_ts "$TEMP_TS_NAME"

   fn_message " drop tablespace ${TEMP_TS_NAME} including contents and datafiles;"
   fn_lclsql_common "drop tablespace ${TEMP_TS_NAME} including contents and datafiles;"
done
   fn_masterlog "INFO: Successfully Migrated Non-Default temporary tablespace (Application specific)  to Bigfile."
}

fn_user_temp_ts()
{
# Called by fn_Temp_TS
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '|| username from dba_users where temporary_tablespace = '${1}';
EOF
)|grep -i 'Y '| while read junk2 TEMP_TS_USER
do
    if [ ! -z ${TEMP_TS_USER} ]
        then
           fn_message " alter user ${TEMP_TS_USER} temporary tablespace ${1}_TS;"
           fn_lclsql_common "alter user ${TEMP_TS_USER} temporary tablespace ${1}_TS;"
    fi
done
}


fn_Def_Temp_TS()
{
fn_message " Migrating Default temporary  tablespace (TEMP) to Bigfile "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||  property_value from database_properties where property_name = 'DEFAULT_TEMP_TABLESPACE';
EOF
)|grep -i 'Y '| while read junk2 DEF_TEMP_TS_NAME
do
   fn_message "create bigfile temporary tablespace ${DEF_TEMP_TS_NAME}_TS tempfile '+DATA' size 10G autoextend on next 10G;"
   fn_lclsql_common "create bigfile temporary tablespace ${DEF_TEMP_TS_NAME}_TS tempfile '+DATA' size 10G autoextend on next 10G;"

   fn_message "alter database default temporary tablespace ${DEF_TEMP_TS_NAME}_TS;"
   fn_lclsql_common "alter database default temporary tablespace ${DEF_TEMP_TS_NAME}_TS;"

   fn_message "drop tablespace $DEF_TEMP_TS_NAME including contents and datafiles;"
   fn_lclsql_common "drop tablespace ${DEF_TEMP_TS_NAME} including contents and datafiles;"

   fn_message "alter tablespace ${DEF_TEMP_TS_NAME}_TS rename to ${DEF_TEMP_TS_NAME};"
   fn_lclsql_common "alter tablespace ${DEF_TEMP_TS_NAME}_TS rename to ${DEF_TEMP_TS_NAME};"


done
   fn_masterlog "INFO: Successfully Migrated default temporary tablesapce to Bigfile."
}


fn_Def_User_TS()
{
fn_message " Migrating Default User tablespaces to Bigfile "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||  property_value from database_properties where property_name = 'DEFAULT_PERMANENT_TABLESPACE';
EOF
)|grep -i 'Y '| while read junk2 DEF_USER_TS_NAME
do
#   fn_message "create bigfile tablespace ${DEF_USER_TS_NAME}_TS datafile '+DATA' size 1G autoextend on next 10G;"
#   fn_lclsql_common "create bigfile tablespace ${DEF_USER_TS_NAME}_TS datafile '+DATA' size 1G autoextend on next 10G;"

   fn_message "alter database default tablespace ${DEF_USER_TS_NAME}_BFT;"
   fn_lclsql_common "alter database default tablespace ${DEF_USER_TS_NAME}_BFT;"

   fn_message "drop tablespace $DEF_USER_TS_NAME including contents and datafiles;"
   fn_lclsql_common "drop tablespace $DEF_USER_TS_NAME including contents and datafiles;"

   fn_message "alter tablespace ${DEF_USER_TS_NAME}_BFT rename to ${DEF_USER_TS_NAME};"
   fn_lclsql_common "alter tablespace ${DEF_USER_TS_NAME}_BFT rename to ${DEF_USER_TS_NAME};"
done
   fn_masterlog "INFO: Successfully Migrated default Permanent tablespace (USERS) to Bigfile."

}

fn_Create_Users_BFT()
{
 fn_message " Creating Big File USERS Tablespace "
 (sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||  property_value from database_properties where property_name = 'DEFAULT_PERMANENT_TABLESPACE';
EOF
)|grep -i 'Y '| while read junk2 DEF_USER_TS_NAME
do
   usrbfcnt=`(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select count(1) from dba_tablespaces where tablespace_name = '${DEF_USER_TS_NAME}_BFT' and bigfile='YES';
EOF
        )`
        if [[ ${usrbfcnt} -gt 0 ]]
        then
        echo  " Big File USER tablespace exists..  Moving on."
        else
            fn_message "create bigfile tablespace ${DEF_USER_TS_NAME}_BFT datafile '+DATA' size 1G autoextend on next 10G;"
            fn_lclsql_common "create bigfile tablespace ${DEF_USER_TS_NAME}_BFT datafile '+DATA' size 1G autoextend on next 10G;"
            fn_masterlog "INFO: Successfully Created USERS Bigfile tablespace."
        fi
done
}


fn_Undo_TS()
{
fn_message " Migrating UNDO tablespaces to Bigfile "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||  tablespace_name from dba_tablespaces where contents = 'UNDO';
EOF
)|grep -i 'Y '| while read junk2 UNDO_TS_NAME
do
   fn_message "create bigfile undo tablespace ${UNDO_TS_NAME}_TS datafile '+DATA' size 10G autoextend on next 1G;"
   fn_lclsql_common "create bigfile undo tablespace ${UNDO_TS_NAME}_TS datafile '+DATA' size 10G autoextend on next 1G;"

   fn_message "alter tablespace $UNDO_TS_NAME rename to ${UNDO_TS_NAME}_old;"
   fn_lclsql_common "alter tablespace $UNDO_TS_NAME rename to ${UNDO_TS_NAME}_old;"

   fn_message "alter tablespace ${UNDO_TS_NAME}_TS rename to ${UNDO_TS_NAME};"
   fn_lclsql_common "alter tablespace ${UNDO_TS_NAME}_TS rename to ${UNDO_TS_NAME};"


   fn_message "alter system set undo_tablespace=$UNDO_TS_NAME scope=both;"
   fn_lclsql_common "alter system set undo_tablespace=$UNDO_TS_NAME scope=both;"

   fn_message "drop tablespace ${UNDO_TS_NAME}_old including contents and datafiles;"
   fn_lclsql_common "drop tablespace ${UNDO_TS_NAME}_old including contents and datafiles;"

done
   fn_masterlog "INFO: Successfully Migrated UNDO tablespace ${UNDO_TS_NAME} to Bigfile."

}

fn_bounceDB()
{
  fn_message "Bouncing Database "
  fn_lclsql_common "shutdown immediate;" >> $_reorgLog
  fn_lclsql_common "Startup mount;" >> $_reorgLog
  fn_lclsql_common "Alter database open;" >> $_reorgLog
  fn_masterlog "INFO: Successfully  bounced the database"
}

fn_setlogmodeDB()
{
#  fn_message "${1} Log Mode"
  fn_lclsql_common "shutdown immediate;" >> $_reorgLog
  fn_lclsql_common "Startup mount;" >> $_reorgLog
  if [[ ${1} == 'DISABLE' ]]
  then
     fn_lclsql_common "Alter database noarchivelog;" >> $_reorgLog
  elif [[ ${1} == 'ENABLE' ]]
    then
    fn_lclsql_common "Alter database archivelog;" >> $_reorgLog
  fi
  fn_lclsql_common "Alter database open;" >> $_reorgLog
  fn_masterlog "INFO: Database Log Mode - ${1} Successful"
}


fn_utlrp()
{
sqlplus -s / as sysdba <<EOF
     set serveroutput on
     set echo on
     set time on
     set timing on
     spool ${_lclLog} append
     @ $ORACLE_HOME/rdbms/admin/utlrp.sql
EOF
}



fn_getDBname()
{
_DBname=`(sqlplus -s / as sysdba  <<EOF
    set heading off
    set feedback off
    select count(1)||','|| lower(name) from v\\$database group by name;
EOF
)`
       cnt=`echo $_DBname | cut -d "," -f1`
#       dbname=`echo $_DBname | cut -d "," -f2`
       if [[ $cnt == '1' ]]
       then
           fn_message "Successfully queried the database"
           dbname=`echo $_DBname | cut -d "," -f2`
           fn_masterlog "INFO: Successfully queried the database.  The database name is ${dbname}"
       else
       fn_message " Critical: Cannot query Database..  Check if the database is down or the environment is set "
           fn_masterlog "ERROR:  Cannot query Database..  Check whether the database is up and the DB environment is set."
       exit 1
       fi
}


fn_grantDlockManual()
{
 for gl in ALLYDBO ARCH_CTRLCTR CASHDBO CTRLCTR DATAEXCHDBO DATAMARTDBO EAGLEKB EAGLEMART EAGLEMGR EAGLE_TEMP ESTAR HOLDINGDBO ISTAR LEDGERDBO MSGCENTER_DBO PACE_MASTERDBO PERFORMDBO PRICING RULESDBO SCRUBDBO SECURITYDBO TRADESDBO
do
  echo " grant execute on sys.dbms_lock to ${gl};" | tee -a >> ${_grantDLckSql}
done
}

fn_check_expimp_errors ()
{
 if [[ ${1} == 'export' ]]
 then
    expchkerr=`cat $exportDir/cmpexport.log | grep -i "successfully completed at" | grep -v grep | wc -l`
     if [[ $expchkerr -ge 1 ]]
     then
#         fn_message "  Export successfully Completed ... No errors. "
         exp_status='Y'
     else
         exp_status='N'
     fi
 elif [[ ${1} == 'import' ]]
 then
    impchkerr=`cat $exportDir/cmpimport.log | grep -v "ORA-39082" | grep -i "ora-"| grep -v grep | wc -l`
    if [[ $impchkerr -ge 1 ]]
     then
         fn_message "  Errors found during the import.  Please check "
         imp_status='N'
    else
         fn_message " Import successful "
         imp_status='Y'
    fi
  fi
}



fn_RenameDFiles()
{
  if [ -f ${_rmanRenTbs} ]; then
     rm ${_rmanRenTbs}
  fi
echo " "
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select distinct 'Y '||file_id||':'||tablespace_name
        from dba_data_files
        where
        tablespace_name not in ('SYSTEM','SYSAUX','UNDOTBS1')
        and lower(file_name) like '%_bft.%' order by 1;
EOF
)|grep -i 'Y '| while read junk2 tsdata
do
#echo $tsdata
        if [ ! -z $tsdata ]
        then
         fid=`echo $tsdata | cut -d ":" -f1`
         tbs=`echo $tsdata | cut -d ":" -f2`
       echo " # tablespace $tbs " | tee -a >> ${_rmanRenTbs}
       echo " sql \" alter tablespace $tbs offline\";" | tee -a >> ${_rmanRenTbs}
       echo "backup as copy datafile ${fid} format '+DATA';" | tee -a >> ${_rmanRenTbs}
       echo "switch datafile ${fid} to copy;" | tee -a >> ${_rmanRenTbs}
       echo " sql \" alter tablespace $tbs online\";" | tee -a >> ${_rmanRenTbs}
       echo "delete noprompt copy of datafile ${fid};" | tee -a >> ${_rmanRenTbs}
       echo " " | tee -a >> ${_rmanRenTbs}
        fi
done
  if [ -f ${_rmanRenTbs} ]; then
      rman target / cmdfile=${_rmanRenTbs} log=${_rmanRenTbspsLog}
  else
     fn_message " Cannot find ${_rmanRenTbs} file... Datafile Rename failed...."
     fn_masterlog "WARNING: Cannot find ${_rmanRenTbs} file... Datafile Rename did not happen...."
  fi
  fn_masterlog "INFO: Successfully renamed Datafiles"
}




fn_pace_masterdbo_grant()
{
fn_message "Granting specific system privileges to PACEMASTER_DBO"
fn_lclsql_common "grant select on v_\$database to PACE_MASTERDBO;"
fn_lclsql_common "grant select on DBA_HIST_SNAPSHOT to pace_masterdbo;"
fn_lclsql_common "grant select on v_\$instance to pace_masterdbo;"
fn_lclsql_common "grant execute on dbms_workload_repository to pace_masterdbo;"
fn_masterlog "INFO: Successfully Granted specific system privileges to PACEMASTER_DBO"
}

fn_compareObjCnt()
{
if [[ -f "$segmentCntLog" ]]
then
objmismatch='N'
for i in `cat $segmentCntLog | grep -iw source | grep -ivw Invalidcnt`
do
  schemaOwner1=`echo ${i}|cut -d"," -f2`
  schemaOwner=`echo $schemaOwner1 | tr -d "$"`
  schemaObjcnt=`echo ${i}|cut -d "," -f4`
  targetOwn=`cat $segmentCntLog | grep -iw target | cut -d "," -f2,4|tr -d "$" |grep -iw $schemaOwner `
     if [ ! -z ${targetOwn} ]
     then
        targetObjcnt=`echo $targetOwn | cut -d "," -f2`
        if [[ $schemaObjcnt -ne $targetObjcnt ]]
        then
           fn_message " ERROR: Object mismatch.  $schemaOwner:Source->$schemaObjcnt:Target->$targetObjcnt"
           objmismatch='Y'
        fi
     else
        fn_message " CRITICAL: Cannot find Schema $schemaOwner in the target. Please investigate...... "
        exit 1
     fi
done
     if [[ ${objmismatch} == 'Y' ]]
     then
        fn_message "WARNING:  Object Mismatch between Source and Target.  Please Investigate "
     else
        fn_message " INFO :All objects Matched "
        objectflg=Y
     fi
else
   fn_message "Error: Cannot find **$segmentCntLog**  to compare Objects between Source and Target."
fi
   fn_masterlog "INFO: Successfully compared Schema objects "
}

fn_compareInvldObj()
{
if [[ -f "$segmentCntLog" ]]
then
 for i in `cat $segmentCntLog | grep -iw source | grep -iw Invalidcnt`
 do
  sourInvldCnt=`echo ${i} | cut -d "," -f3`
  TarInvldCnt=`cat $segmentCntLog | grep -iw target | grep -iw Invalidcnt |  cut -d "," -f3`
  if [ ! -z $TarInvldCnt ]
  then
     if [[ $sourInvldCnt  -lt $TarInvldCnt ]]
     then
        fn_message " WARNING: Invalid Objects detected...  Please investigate "
        invalidflg='N'
     else
        fn_message " INFO: Invalid count Matched "
        invalidflg='Y'
     fi
  else
     echo " Cannot find Target invalid count in the log File - $segmentCntLog"
     invalidflg='N'
  fi

 done
else
   fn_message "Error: Cannot find **$segmentCntLog**  to compare Invalid objects between Source and Target."
fi
   fn_masterlog "INFO: Successfully compared Invalid objects "
}

fn_checkBFTbs()
{
bftbscnt=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select count(1) from dba_tablespaces where tablespace_name not in ('SYSTEM','SYSAUX') and bigfile <> 'YES';
EOF
)`
if [[ ${bftbscnt} -gt 0 ]]
then
  fn_message " Error:  There are ${bftbscnt} Tablespaces that were not converted to BigFile"
      BFTcntStatus='N'
else
   fn_message "All the tablesapces are converted to Bigfile "
      BFTcntStatus='Y'
fi
   fn_masterlog "INFO: Successfully queried Tablespaces for BIGFILE='YES'."
}

fn_chkNonBFTs()
{
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select 'Y '|| tablespace_name from dba_tablespaces where tablespace_name not in ('SYSTEM','SYSAUX') and contents='PERMANENT' and bigfile <> 'YES'
        minus
        select tablespace_name from dba_segments where tablespace_name not in ('SYSTEM','SYSAUX') group by tablespace_name having count(1) < 1;
EOF
)|grep -i 'Y '| while read junk2 NONBFTS
do
echo "eee ${NONBFTS}"
   if [ ! -z ${NONBFTS} ]
   then
      fn_message "Drop tablespace ${NONBFTS} including contents and datafiles;"
      fn_lclsql_common "Drop tablespace ${NONBFTS} including contents and datafiles;"
      fn_message "create bigfile tablespace ${NONBFTS} datafile '+DATA' size 2G autoextend on extent management local autoallocate;"
      fn_lclsql_common "create bigfile tablespace ${NONBFTS} datafile '+DATA' size 2G autoextend on extent management local autoallocate;"
   else
      fn_message " There are no smallfile tablespaces."
   fi
done
}


fn_chkDefPerTbs()
{
_CheckDPT=`(sqlplus -s / as sysdba  <<EOF
set heading off
set feedback off
select count(1) from database_properties where PROPERTY_NAME = 'DEFAULT_PERMANENT_TABLESPACE' and PROPERTY_VALUE <> 'USERS';
EOF
)`
   if [[ ${_CheckDPT} -eq 1 ]]
   then
      fn_message "  The default permanent tablespace for this database is not USERS.  Resetting to USERS";
      fn_message " alter database default tablespace USERS;"
      fn_lclsql_common "alter database default tablespace USERS;"
   else
      fn_message " The default permanent tablespace for this database is USERS..  Moving on.."
   fi
}

fn_GatherStats()
{
  fn_message "Gathering Schema Statistics "
  fn_masterlog "INFO: Gathering Schema Statistics."
  if [ -f ${_GatherStats} ]; then
     rm ${_GatherStats}
  fi
echo "spool ${_schemsStatsLog}" >> ${_GatherStats}
echo " alter session set sort_area_size = 1000000000;" >> ${_GatherStats}
echo " select to_char(sysdate,'MM/DD/YYYY HH24:MI:SS') from dual;" >>  ${_GatherStats}
echo " exec dbms_stats.set_global_prefs('CONCURRENT','TRUE');" >>  ${_GatherStats}
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
select username from dba_users where username not in
( 'ANONYMOUS','CTXSYS','DBSNMP','EXFSYS','LBACSYS','MDSYS','MGMT_VIEW','OLAP_SYS','OWBSYS','ORDPLUGINS','ORDSYS','OUTLN'
,'SI_INFORMTN_SCHEMA','SYS','SYSMAN','SYSTEM','TSMSYS','WK_TEST'
,'WKSYS','WKPROXY','WMSYS','XDB','APEX_PUBLIC_USER','DIP'
,'FLOWS_30000','FLOWS_FILES','MDDATA','ORACLE_OCM','SPATIAL_CSW_ADMIN_USR'
,'SPATIAL_WFS_ADMIN_USR','XS$NULL','APPQOSSYS');
EOF
)| while read GSTATS
do
#echo $tsdata
        if [ ! -z $GSTATS ]
        then
    echo "exec dbms_stats.gather_schema_stats(ownname => '${GSTATS}' ,estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size 1',cascade => TRUE, granularity => 'ALL', no_invalidate => FALSE, degree => dbms_stats.auto_degree);" | tee -a >> ${_GatherStats}
        fi
done
   echo " exec dbms_stats.set_global_prefs('CONCURRENT','FALSE');" >>  ${_GatherStats}
   echo " select to_char(sysdate,'MM/DD/YYYY HH24:MI:SS') from dual;" >> ${_GatherStats}

  if [ -f ${_GatherStats} ]; then
  sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        @ ${_GatherStats}
        exit
EOF
  else
     echo " Cannot find ${_GatherStats} file... Stats not collected ...."
     fn_message " Cannot find ${_GatherStats} file... Stats not collected ...."
     fn_masterlog "WARNING: Cannot find ${_GatherStats} file... Stats not collected ...."
  fi
  fn_masterlog "INFO: Schema Stats collected successfully."
}


fn_remDumpFiles()
{
dumpfiles1=$(ls $exportDir/cmpexport_*.dmp 2> /dev/null | wc -l)
     if [ "$dumpfiles1" != "0" ]
     then
        fn_message "  Removing Export Dumpfiles. "
        rm $exportDir/cmpexport_*.dmp
        fn_masterlog "INFO: Successfully removed export dumpfiles."
        mv $exportDir/cmpexport.log .
        mv $exportDir/cmpimport.log .
     fi
}

fn_grantUsersBFT ()
{
(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select 'Y '|| owner from dba_segments a, dba_users b
 where a.owner=b.username
 and b.default_tablespace='USERS'
 having count(segment_name) > 1
 group by owner, tablespace_name;
EOF
)|grep -i 'Y '| while read junk2 USERTBSUSERS
do
   if [ ! -z ${USERTBSUSERS} ]
   then
      fn_message "alter user ${USERTBSUSERS} quota unlimited on users_bft;"
      echo "alter user ${USERTBSUSERS} quota unlimited on users_bft;" | tee -a >> ${_grantTbsQuotaSql}
     fn_lclsql_common "alter user ${USERTBSUSERS} quota unlimited on users_bft;"
   fi
done
}

fn_checkSystemTBS()
{
systemtTbsCnt=`(sqlplus -s / as sysdba  <<EOF
        set heading off
        set feedback off
        select count(1) from dba_segments where owner='SYSTEM' and tablespace_name not in ('SYSTEM','SYSAUX');
EOF
        )`
        if [[ ${systemtTbsCnt} -gt 0 ]]
        then
        fn_message  "Systems Objects present in NON-SYSTEM TableSpace.  CAnnot continue.."
        exit 1
        else
        fn_message " Systems Objects are self Contained.  Continuing"
        fi
}


fn_runstatus()
{
    echo " " | tee -a ${_reorgLog}
    echo "****************************************************************" | tee -a ${_reorgLog}
    echo "*          R E O R G   S T A T U S                             *" | tee -a ${_reorgLog}
    echo "****************************************************************" | tee -a ${_reorgLog}
    echo "*                                                              *" | tee -a ${_reorgLog}
 if [[ $objectflg == 'Y' ]]
 then
    echo "* INFO:  Object Count matched between Source and Target        *" | tee -a ${_reorgLog}
 else
    echo "* ERROR: Object count dosen't match between Source and Target  *" | tee -a ${_reorgLog}
    echo "*        Please check the log file for the details             *" | tee -a ${_reorgLog}
fi
 if [[ $invalidflg == 'Y' ]]
 then
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* INFO:  Invalid Count matched between Source and Target       *" | tee -a ${_reorgLog}
 else
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* ERROR: Invalid object mismatch between Source and Target     *" | tee -a ${_reorgLog}
    echo "*        Please check the log file for the details             *" | tee -a ${_reorgLog}
fi
 if [[ $BFTcntStatus == 'Y' ]]
 then
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* INFO:  Tablespaces are converted to BigFile..                *" | tee -a ${_reorgLog}
 else
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* Error: Some Tablespaces are not Converted to BigFile         *" | tee -a ${_reorgLog}
    echo "*        Please query the database and investigate..           *" | tee -a ${_reorgLog}
fi
 if test "$objectflg" = "Y" && test "$invalidflg" = "Y" && test "$BFTcntStatus" = "Y"
 then
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* ReOrg completed successfully                                 *" | tee -a ${_reorgLog}
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "****************************************************************" | tee -a ${_reorgLog}
    fn_masterlog "INFO: ReOrg completed successfully "
    fn_remDumpFiles
    touch ${_reorgSuccessFlg}
    exit 0
 else
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "* ReOrg Process has Errors.. Please check the logfile          *" | tee -a ${_reorgLog}
    echo "*                                                              *" | tee -a ${_reorgLog}
    echo "****************************************************************" | tee -a ${_reorgLog}
    fn_message "INFO: ReOrg completed with Errors "
    exit 2
 fi
}

#----------------------------------------------------------------------
#
#                       MAIN PROCESS
#
#----------------------------------------------------------------------
_reorgLog=dbreorg.log
_mstrReorgLog=masterdbreorg.log
fn_checkSystemTBS
fn_getDBname


mkdir -p migrate/${dbname}/log
mkdir -p migrate/${dbname}/sql
mkdir -p migrate/${dbname}/misc
mv dbreorg.log migrate/${dbname}/log
mv masterdbreorg.log migrate/${dbname}/log

#Logfiles

_reorgLog=migrate/${dbname}/log/dbreorg.log
_mstrReorgLog=migrate/${dbname}/log/masterdbreorg.log
_createddlLog=migrate/${dbname}/log/create_ddl.log
_compInvldObjLog=migrate/${dbname}/log/compile_invalid_objects.log
_cmpExpLog=migrate/${dbname}/log/cmpexport.log
_cmpImpLog=migrate/${dbname}/log/cmpimport.log
_rmanRenTbspsLog=migrate/${dbname}/log/rename_tablespace.log
_lclLog=migrate/${dbname}/log/lcl_sql.log
#_SegCntLog=migrate/${dbname}/log/segment_count.log
segmentCntLog=migrate/${dbname}/log/segment_count.log
_schemsStatsLog=migrate/${dbname}/log/schema_stats.log

#Sql Files

_crBFTbspSql=migrate/${dbname}/sql/cr_tablespace.sql
_dropUserSql=migrate/${dbname}/sql/drop_user.sql
_grantTbsQuotaSql=migrate/${dbname}/sql/grantTbsQuota.sql
_DropOldTbspSql=migrate/${dbname}/sql/drop_oldtblspc.sql
_renTbspSql=migrate/${dbname}/sql/rename_tblspc.sql
_grantDLckSql=migrate/${dbname}/sql/grant_dlock.sql
_compInvdObjSql=migrate/${dbname}/sql/compile_invalid_objects.sql
_acctUnlock=migrate/${dbname}/sql/accountunlock.sql
_acctLock=migrate/${dbname}/sql/accountlock.sql
_GatherStats=migrate/${dbname}/sql/schema_stats.sql

#Misc Files

_remapTbs=migrate/${dbname}/misc/remaptbs.lst
_expSchema=migrate/${dbname}/misc/expschema.lst
_expSuccess=migrate/${dbname}/misc/export.success
_impSuccess=migrate/${dbname}/misc/import.success
_rmanRenTbs=migrate/${dbname}/misc/rename_tablespace.rcv
_remBFTFlg=migrate/${dbname}/misc/removeBFT
_reorgSuccessFlg=migrate/${dbname}/misc/reorg.success

#*************Variables End****************************
while (($#>0))
do
 case $1 in
  -d) exportDir=$2
      fn_message "Directory Name - ${exportDir}"
      shift 2
      ;;
  -r) rflag=$2
      echo "run flag is set to $rflag"
      break ;;
  -?) fn_message "Usage : `basename "$0"` -d [Directory Name]     Directory used for ReOrg process"
      exit 1
      ;;
   *) if [[ -z ${exportDir} ]]
      then
          fn_message " Usage : `basename "$0"` -d <export directory name>"
      fi
      exit 1
      ;;
  esac
done
if [[ -z ${exportDir} ]]
then
  fn_message "INFO: No directory was passed as parameter for DataDump Process.  Using the default.[/dbimport/oracle/reorg] "
  mkdir -p /dbimport/oracle/reorg
  exportDir=/dbimport/oracle/reorg
fi


# Parsing run flag

if [[ -z $rflag || "$rflag" == "exp" ]]
then
   fn_fileMgmt "${_expSuccess}" "remove"
   eflg='Y'
   iflg='Y'
   pflg='Y'
   fn_message " runflag is either NULL or set to -> $rflag.  Status of flags: expflg -> $eflg; impflg -> $iflg and PostDPump -> $pflg"
#   exit
elif [[ $rflag == 'imp' ]]
then
     if [[ -f ${_expSuccess} ]]
     then
        fn_fileMgmt "${_impSuccess}" "remove"
        eflg='N'
        iflg='Y'
        pflg='Y'
        fn_message " runflag is $rflag -> exp is $eflg; imp is $iflg and Post Import is $pflg"
#        exit
     else
        fn_message "Cannot restart import as Export was not performed."
        exit 0
     fi
elif [[  $rflag == 'pdp' ]]
then
   if [[ -f ${_impSuccess} ]]
   then
   	eflg='N'
        iflg='N'
        pflg='Y'
        fn_message " runflag is $rflag -> exp is $eflg; imp is $iflg and Post Import is $pflg"
#        exit
    else
        fn_message "Cannot restart Post data pump tasks as Import was not performed."
        exit 0
    fi
else
   fn_message " runflag is $rflag -> exp is $eflg; imp is $iflg and Post Import is $pflg"
   fn_message "Invalid run flag option.  Usage: -r exp|imp|pdp"
   exit 1
fi

 
fn_fileMgmt "dbreorg.log" "remove"
fn_fileMgmt "masterdbreorg.log" "remove"



if [ -f ${_reorgSuccessFlg} ]
then
   fn_message "Previous run was successful..  Nothing needs to be done.  Exiting.."
   fn_masterlog "INFO:Previous run was successful..  Nothing needs to be done.  Exiting.."
   exit 0
fi




fn_fileMgmt "${_createddlLog}" "remove"

fn_message " Datapump directory is -> $exportDir"
parallel=$((`nproc`*2))
#
#
fn_message "Re_org process starts"
if [[ $eflg == 'Y' ]]
then
   if [ -w "${exportDir}" ]
   then
      fn_message "Directory exists and  Writable"
      mkdir -p ${exportDir}/${dbname}
      fn_masterlog "INFO: Successfully created Oracle Dump directory."
   else
      fn_message "Directory either does not Exists nor  Writable.. Exiting"
      fn_masterlog "ERROR: Invalid Oracle Dump directory or Directory is now Writable." 
      exit 1
      fi
fi
      exportDir=${exportDir}/${dbname}



if [[ $eflg == 'Y' ]]
then
fn_fileMgmt "${segmentCntLog}" "remove"
fn_fileMgmt "${_remBFTFlg}" "remove"
#fn_AcctUnLock
fn_autoExtendTempFiles
fn_chkDefPerTbs
fi
#
#
        
	fn_segment_count "Source"
	fn_getInvalidObj "Source"
#
#
if [[ $eflg == 'Y' ]]
then
	fn_setlogmodeDB "DISABLE"
	fn_Create_Users_BFT
#fn_grantUsersBFT
fi
	fn_check_ts

if [[ $pflg == 'Y' ]]
then
  if [ -f ${_impSuccess} ]
  then
    if [[ -f ${_remBFTFlg} ]]
    then
   	 fn_message " Renaming BFT tablespaces "
         fn_rename_TS
         fn_masterlog "INFO: Successfully Renamed Tablespaces to standard naming convention "
    fi
	fn_bounceDB
	fn_pace_masterdbo_grant
	fn_utlrp
	fn_masterlog "INFO: Successfully Recompiled database objects using UTLRP."
	fn_Temp_TS
	#fn_nonapp_TS
	fn_Def_Temp_TS
	fn_Def_User_TS
	fn_Undo_TS
	fn_setlogmodeDB "ENABLE"
	fn_RenameDFiles
	fn_ExportDirRemove
	fn_segment_count "Target"
	fn_getInvalidObj "Target"
	fn_compareObjCnt
	fn_compareInvldObj
        fn_chkNonBFTs
	fn_checkBFTbs
#        fn_AcctLock
        fn_GatherStats
	fn_runstatus
  fi
fi
