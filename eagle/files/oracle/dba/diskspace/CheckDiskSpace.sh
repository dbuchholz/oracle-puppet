#!/bin/ksh
# ==================================================================================================
# NAME:		CheckDiskSpace.sh                                 
# 
# AUTHOR:	Maureen Buotte                                    
#
# PURPOSE:	This utility monitors Disk Space and sends mail when any mount point usage is
#		over 85%.
#
# USAGE:	CheckDiskSpace.sh
#
# Frank Davis  05/28/2015  Made script compatible with ASM
# Frank Davis  10/30/2014  Added /var /boot /dev/shm /tmp / to the exclusion list of mount point not to report.
# Frank Davis  09/30/2013  Remove team_dba@eagleaccess.com from email
# Frank Davis  09/24/2013  Remove tech_support@eagleaccess.com from email
# Frank Davis  06/25/2013  Exclude reporting of /datadomain,/datadomain2 and /mnt reaching threshold on all servers.
# Frank Davis  06/24/2013  Only report /datadomain reaching threshold on hawaii and ppld0001
# Frank Davis  06/14/2013  Made script compatible with Linux and Solaris.
# Frank Davis  06/13/2013  Made script compatible with Service Now.
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis  06/24/2011  Modified IPCheck Variable to include updated list of VLANs
# Nishigandha Sathe 02-Mar-2010  Modified to use new primary key machines(instance)
# Maureen Buotte   	20-Jul-2009  Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte   	05-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte   	01-Jun-2008  Added reporting to DBA database
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat
 print "\n$1\n" >> $WORKING_DIR/mail.dat
 cat $ERROR_FILE >> $WORKING_DIR/mail.dat
 rm $ERROR_FILE
 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX " $MAILTO
 rm $WORKING_DIR/mail.dat
 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
	PERFORM_CRON_STATUS=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
	set heading off feedback off
    select 1 from dual;
EOF
`
    PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
    if [ $PERFORM_CRON_STATUS -ne 1 ]
    then
     PERFORM_CRON_STATUS=0
	fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
        ## ---- Fetching the mac_instance for the BoxName from machines table ------
            Mac_Instance=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
                set heading off feedback off
                select instance from MACHINES where name='${BOX}';
EOF
`
            Mac_Instance=$(print $Mac_Instance|tr -d " ")
        ## ---------------------------------------------------------------------------


	Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off feedback off
		select instance from MACHINE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
		start_time > to_char(sysdate,'DD-MON-YYYY') and mac_instance = ${Mac_Instance};
EOF
`
	Sequence_Number=$(print $Sequence_Number|tr -d " ")
	if [ x$Sequence_Number = 'x' ]
    then
		Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set serveroutput on size 1000000
		set heading off feedback off
		declare i number;
		begin
		insert into MACHINE_CRON_JOB_RUNS values (machine_cron_job_runs_seq.nextval, '${PROGRAM_NAME}', sysdate, '','',0,'','0',${Mac_Instance}) returning instance into i;
		commit;
		dbms_output.put_line (i);
		end;
/
EOF
`
	else
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off feedback off
		update MACHINE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
		commit;
EOF
`

	fi
	export Sequence_Number
	return 0

}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
    # Update DBA Auditing Database with end time and backup size
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off feedback off 
		update MACHINE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
		commit;

		update MACHINE_CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
		from machine_cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
/
EOF
`

    print  "$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS "
}
# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
#function funct_check_asm_client
#{
#export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
#set heading off feedback off
#select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
#EOF
#`
#}

function funct_check_asm_client
{
ASM_CLIENT=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
export ASM_CLIENT
}

# *****************************
#  MAIN 
# *****************************
export PROGRAM_NAME=$(print  $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 export DF_COMMAND="df -Pk"
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 export DF_COMMAND="df -k"
fi

######
export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
####

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/diskspace
else
 export WORKING_DIR="$HOME/local/dba/diskspace"
fi
export ERROR_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_error.txt

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 export DF_COMMAND="df -Pk"
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 export DF_COMMAND="df -k"
fi

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

# Get environment variables set up - need to do this differently for RHEL 5
export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number
export PERFORM_CRON_STATUS=0

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(grep  "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=`print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
 fi
fi
rm -f $NO_COMMENT_PARFILE

HOSTNAME=$(hostname)
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
IPCheck=$(cat /etc/hosts | grep $HOSTNAME | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=1
 export URGENCY=1
else
 export TYPE=TEST
 export IMPACT=2
 export URGENCY=3
fi
export MAILTO='eaglecsi@eagleaccess.com'
#export MAILTO='sn_alert_dev@eagleinvsys.com,sn_alert_test@eagleinvsys.com'

# Add entry into DBA Admin database
if [[ $PERFORM_CRON_STATUS -eq 1 ]]
then
 funct_initial_audit_update
 if [ $? -ne 0 ]
 then
  PERFORM_CRON_STATUS=0
 fi
fi

export Sequence_Number

rm -f $ERROR_FILE
# Get the Disk Usage
# Check only local filesystems (doesn't include datadomain)
$DF_COMMAND | egrep -v '/datadomain|/datadomain2|/mnt|/var|/boot|/dev/shm|/tmp| /$'  > /tmp/tmp_cds.dat
cat /tmp/tmp_cds.dat | while read LINE
do
 case $LINE in
 "Filesystem"*)  ;;      # Header Line
             *) typeset -i PercentUsed=$(print $LINE | awk '{print $5}' | awk -F% '{print $1}')
				MountedOn=$(print $LINE | awk '{print $6}')
                if [[ $PercentUsed -ge 85 ]]
                then
                 print "$MountedOn			${PercentUsed}% " >> $ERROR_FILE
                fi ;;
        esac
done

if [[ -f $ERROR_FILE ]]
then
 SendNotification "The following disk space issues need to be resolved on ${BOX}: \n"
fi

# Update Infrastructure database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 if [ $MAIL_COUNT -gt 0 ]
 then
  PROCESS_STATUS='WARNING'
 else
  PROCESS_STATUS='SUCCESS'
 fi
 funct_final_audit_update
fi

rm -f /tmp/tmp_cds.dat
exit 0
