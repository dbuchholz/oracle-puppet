package ITSCalc;

=head1  ITSCalc.pm

=head2  ABSTRACT:
		Provide standard data calculation routines - current routines are:
		ComputeCheckDigit - Computes a check digit for a SEDOL or CUSIP
		IncrementChar  - Increments a character field through alphanumeric characters

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 16-NOV-04 13354-0  RCR Changed invalid digit check
		RAM 13-JAN-04 13354-0  JGF Added ComputeCheckDigit
		RAM 08-SEP-03 13354-0  JGF Initial development

=cut

use strict;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ComputeCheckDigit IncrementChar);

my $char_order;          # array of characters in order of increment
my $check_cusip;         # array of characters for computing cusip check digit
my $check_sedol;         # array of characters for computing sedol check digit
my @sedol_weight;        # weight for each SEDOL digit

INIT {
	$char_order  = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	$check_cusip = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ*@#";
	$check_sedol = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	@sedol_weight = (1, 3, 1, 7, 3, 9);           # weighting factor for each digit of SEDOL
}


sub ComputeCheckDigit ($) {

=head1  sub ComputeCheckDigit ($)

=head2  ABSTRACT:
		# this routine computes a check digit for a 6 character SEDOL or 8 character CUSIP
		# and appends it to the input argument
		# if the length of the argument is not 6 or 8, the argument is unchanged
		# adapted from itsgn_compute_checkdgt by DTJ

=head2  ARGUMENT:
		Security ID to have the check digit added

=cut

	my $digit;                # digit for computation
	my $factor;               # factor to apply to this digit
	my $idxchr;               # index into characters of security ID
	my $lenid;                # length of security ID
	my $number;               # number after multiplication by factor
	my $ones;                 # ones digit of result
	my $secid;                # input security ID
	my $sum;                  # running sum for calculation
	my $tens;                 # tens digit of result

	$secid = $_[0];           # get input security ID
	$lenid = length($secid);  # get length of input

	if    ($lenid == 6) {                 # SEDOL computation

		#     Assign each digit a numeric value (Note: only numbers and consonants are used in the Sedol):
		#     Digit          Value
		#      0              0
		#      1              1
		#     ...            ...
		#      9              9
		#      B             11
		#      C             12
		#     ...            ...
		#      Z             35
		#
		#     (1) Multiply first digit's value by 1
		#     (2)          second              by 3
		#     (3)          third               by 1
		#     (4)          fourth              by 7
		#     (5)          fifth               by 3
		#     (6)          sixth               by 9
		#     (7) Sum the above
		#     (8) Take the mod of the sum with 10
		#     (9) Check digit is 10 less the mod, or zero if 10

		$sum = 0;

		for $idxchr (0 .. 5) {
			$digit = index($check_sedol, substr($secid, $idxchr, 1));
			if ($digit < 0) { return 0; }                             # return out if digit not recognizable
			$sum = $sum + $digit * $sedol_weight[$idxchr];
		}

		$digit = 10 - ($sum % 10);
		if ($digit == 10) { $digit = 0; }
		$_[0] .= $digit;                        # append check digit to argument

	}

	elsif ($lenid == 8) {                 # CUSIP computation

		# ... The method:
		#     (1) Assign a value to the first character in the Cusip, where
		#         numeric characters get their number values, A is 10, B is 11...
		#     (2) Multiply this value by the appropriate value in the series
		#         1,2,1,2,1,2,1,2 (i.e., by "1" for this first character)
		#     (3) Take the resulting number and sum its digits (the highest
		#         resulting number will be 70, or Z*2)  (or 76 for PPN type)
		#     (4) Do the same for the other characters in the Cusip, adding on
		#         to the value of the sum of digits in step (3)
		#     (5) Take the grand total's rightmost digit, and subtract it from
		#         10.  The result is the check digit (or zero if 10).

		$factor = 1;
		$sum = 0;

		for $idxchr (0 .. 7) {
			$digit = index($check_cusip, substr($secid, $idxchr, 1));
			if ($digit < 0) { return 0; }                             # return out if digit not recognizable
			$number = $digit * $factor;
			$ones = $number % 10;
			$tens = ($number / 10) % 10;
			$sum = $sum + $tens + $ones;
			$factor = 3 - $factor;                   # series runs 1,2,1,2,1,2,1,2
		}

		$digit = 10 - ($sum % 10);
		if ($digit == 10) { $digit = 0; }
		$_[0] .= $digit;                        # append check digit to argument

	}

	1;
}

sub IncrementChar ($) {

=head1  sub IncrementChar ($)

=head2  ABSTRACT:
		# this routine increments a character string through alphanumeric characters
		# sequence is 0-9, A-Z, a-z

=head2  ARGUMENT:
		String to be incremented

=cut

	my $char;        # character being examined
	my $found;       # index of found character
	my $idxstr;      # index into string
	my $lenstr;      # length of string
	my $string;      # string to process

	$string = $_[0];
	if ($string eq "") { 
		$_[0] = "?";
		return 0;       # return 0 if input string was null
	}
	$lenstr = length($string) - 1;       # zero based length

	# loop backwards on the string - usually only last character needs to be changed
	for $idxstr (reverse(0 .. $lenstr)) {
		$char = substr($string, $idxstr, 1);

		# check for last character in sequence (z)
		if ($char eq "z") {
			# set to "0" and go on to do next character in string
			substr($string, $idxstr, 1) = "0";
			# check if we just did first character.  If so, must prepend with a "1"
			if ($idxstr == 0) {
				substr($string, 0, 0) = "1";
			}
		}
		else {
			$found = index($char_order, $char);
			if ($found >= 0) {
				$char = substr($char_order, $found + 1, 1);
			}
			else {
				# not found - return "~"
				$char = "~";
			}
			substr($string, $idxstr, 1) = $char;
			last;
		}
	}
	$_[0] = $string;
	1;                # return true - character was incremented
}
1;
