package ITSFatal;

=head1  ITSFatal.pm

=head2  ABSTRACT:
        Provide standard fatal failure routines - current routines are:
		CreateMailto    - creates a hash entry, To, from a notify list
        GetTraceback    - gets a traceback of subroutines from this point up
        NotifyCondition - writes out messages and mails a notify message to a list
        NotifyFatal     - writes out messages and mails a notify message to a list
                          optionally, can notify the ENS
        NotifyMessage   - writes out messages and mails a notify message to a list
        NotifyReport    - mails a report file notify message to a list
		SetMailOverride - sets override lists for mail conditions

=head2  REVISIONS:
        FBD 24-SEP-13 13354-0  FBD Changed tech_support.eagleaccess.com to eaglecsi@eagleaccess.com for Service Now
        JML 07-SEP-11 13354-0  JML Modified mail servers for Post Move Environment.
        RCR 01-NOV-06 13354-0  RCR Added Everett mail server - 10.60. => 10.60.7.242
        RAM 24-FEB-06 13354-0  RCR Corrected Newton mail server
        RAM 29-DEC-05 13354-0  RCR Changed 192 mail server from .117 to .20
        RAM 24-JUN-05 13354-0  RCR Added directed mail capability
        RAM 14-JUN-05 13354-0  RCR Corrected strays
        RAM 23-MAY-05 13354-0  RCR Changed Watertown SMTP Server
        RAM 15-MAR-05 13354-0  RCR Changed to only notify ENS for certain domains
        RAM 13-Jan-05 13354-0  RCR Changed administrator mail address to environment name
        RAM 11-Jan-05 13354-0  RCR Fixed code to not print environment if NOE present in options
        RAM 05-Jan-05 13354-0  RCR Fixed ENS code to work with Eagle Mail server
        RAM 04-OCT-04 13354-0  RCR Changed to use new IP addresses and names
        RAM 28-MAY-04 13354-0  RCR Upgraded to mail to multiple servers based on IP address - added tech_support to ENS call
        RAM 28-MAY-04 13354-0  RCR Changed itsww to eagleaccess
		RAM 23-APR-04 13354-0  RCR Corrected expansion of notify list
		RAM 22-APR-04 13354-0  RCR Corrected code structure and logging problems
		RAM 20-APR-04 13354-0  RCR Added NotifyMessage
		RAM 16-MAR-04 13354-0  JGF Added NotifyReport
        RAM 15-OCT-03 13354-0  JGF Added environment line back in
        RAM 26-NOV-02 13354-0  JGF Initial development

=cut

use strict;
use Carp;
use Cwd;
use ITSCheck;
use ITSSystem;
use ITSTrim;
use Mail::Sendmail;
use Text::ParseWords;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CreateMailto GetTraceback NotifyCondition NotifyFatal NotifyMessage NotifyReport SetMailOverride);

my $ens_address;                      # ens mail address
my @ens_eligibles;                    # ens eligible domains
my $flg_ens;                          # notify ens?

# define server list of IP criteria, addresses, notify names, and overrides
my $numover;           # last index into overrides
my $server;            # ip address of mail server

my @overrides;         # files and names to be notified for override conditions

my %mail;              # mail hash for use with sendmail
my %mailip;            # ip addresses for mail server criteria
my %notify_names;      # names to be notified for each list

INIT {
	# set up mail addresses to be used
	$ens_address='eaglecsi@eagleaccess.com';            # ens mail address

	%notify_names = ();      # names to be notified for each list
	$notify_names{ "VAULT"  } = [ 'Team_Vault@eagleaccess.com' ];

	@overrides = ();         # files and names to be notified for override conditions
	$numover = -1;           # last index into overrides

        # define server list of IP criteria and addresses
        $server = "10.70.13.244";                        # default mail server ip address
        %mailip = ( "10.70." => "10.70.13.244",         # mail server criteria and destination
                    "10.60." => "10.60.7.44");


	# define ENS eligible domains
	@ens_eligibles = ("10.60.3","10.60.5","10.60.52","10.60.99",
                          "10.70.40","10.70.41","10.70.42","10.70.43","10.70.44","10.70.45","10.70.46","10.70.47",
                          "10.70.32","10.70.33","10.70.34","10.70.35");


	my $keyip;                    # key of mail ip address hash
	my $ipaddr;
	my $hostname;
	my $fullhost;

	# get current account information
	GetHostInfo($ipaddr, $hostname, $fullhost);

	# set up server IP address for mail - see declaration at top for default value
	foreach $keyip (keys(%mailip)) {
		if (substr($ipaddr, 0, length($keyip)) eq $keyip) {
			$server = $mailip{ $keyip };
			last;
		}
	}

	# check domain and define flg_ens - true if current domain is in ens_eligibles list
	$flg_ens = 0;
	foreach $keyip (@ens_eligibles) {
		if (substr($ipaddr, 0, length($keyip)) eq $keyip) {
			$flg_ens = 1;
			last;
		}
	}
}

sub CreateMailto($) {

=head1  sub CreateMailto($)

=head2  ABSTRACT:
                Creates a To entry in the %mail hash from a notify list

=head2  ARGUMENTS:
                0: notify list - entries separated by commas

=cut

	my $id_name;                # destination id to be notified
	my $sub_name;               # sub name of list
	$mail{To} = ''; 

	my $notify_list = $_[0];
	# notify_list not in hash, create new list from it
	my @new_list = split(",", $notify_list);
	foreach $id_name (@new_list) {
		if ($id_name ne "") {                                         # name must not be null
			if (!exists($notify_names{ $id_name })) {
				TrimString($id_name);                                                   # get rid of whitespace in the list	
				if (index($id_name, '@') < 0) { $id_name .= '@eagleaccess.com'; }       # if no @, assume it's one of ours
				if (index($mail{To}, $id_name) < 0) {                                   # only add if not already there
					$mail{To} .= " <" . $id_name . ">,";
				}
			}
			else {
				# if notify list exists, send mail to each member of the list
				foreach $sub_name ( @{$notify_names{ $id_name }} ) {
					if (index($mail{To}, $sub_name) < 0) {                               # only add if not already there
						$mail{To} .= " <" . $sub_name . ">,";
					}
				}
			}
		}
	}
	$mail{To} =~ s/,$//g;                 # get rid of last comma

	1;

}

sub GetTraceback(\@) {

=head1  sub GetTraceback

=head2  ABSTRACT:
                Gets, in array of array form, traceback information from the current call point

=head2  ARGUMENTS:
                0: returned array of array

=cut

	my $idxsbr;                 # index of routines used for caller
	my $reftraces = $_[0];      # argument is a reference to the array

	@$reftraces = ();
	for $idxsbr (1 .. 20) {                               # loop starts at 1 so we don't trace this routine
		if (!defined(caller($idxsbr))) {last;}               # end loop on first return of undefined value from caller
		push @$reftraces, [ (caller($idxsbr))[0,1,2,3] ];    # returns package, source, line, subroutine name
	}
	1;

}

sub NotifyCondition (*$$$$$) {

=head1  sub NotifyCondition (*$$$$$)

=head2  ABSTRACT:
                Notifies a list of a condition

=head2  ARGUMENTS:
                0: filehandle of LOGFILE
                1: program name
				2: condition
				3: filetype
                4: message
                5: notify list name

=cut

	my $env_name;               # environment name
	my $fullhost;               # full name of current host
	my $hostname;               # name of current host
	my $idxfld;                 # index into fields
	my $idxover;                # index into overrides
	my $ipaddr;                 # IP address to distribute to
	my $login;                  # login name of this account
	my $source;                 # name of source file containing subroutine

	my $fh_logfile   = $_[0];   # file handle of log file
	my $program_name = $_[1];   # name of program
	my $condition    = $_[2];   # condition for notification
	my $filetype     = $_[3];   # filetype for notification
	my $message      = $_[4];   # message from program
	my $notify_list  = $_[5];   # name of list to be notified

	my @fields;                 # fields in override table

	%mail = ();                 # initialize mail hash

	# get account login
	$login = getlogin() || "N/A";

	# get current account information
	GetHostInfo($ipaddr, $hostname, $fullhost);

	# set mail server
	$mail{Smtp} = $server;

	# find environment - use it for mail reply name
	$source = cwd;
	CheckDirectory($source, $env_name);
	$mail{From} = 'Eagle Access <' . $env_name . '@eagleaccess.com>';

	# check for overrides matching condition and file type
	if ($numover >= 0) {
		for $idxover (0 .. $numover) {

			@fields = @{ $overrides[$idxover] };
			if ($fields[0] eq $condition) {
				if (($fields[1] eq "*") || ($fields[1] eq $filetype)) {
					# found overrides, add to notify list
					for $idxfld (2 .. $#fields) {
						$notify_list .= "," . $fields[$idxfld];
					}
				}
			}
		}
	}

	if ($notify_list ne "") {

		# print messages to screen and logfile
		print                                      "\n$condition CONDITION: $message\n\n";
		if ($fh_logfile ne "") { print $fh_logfile "\n$condition CONDITION: $message\n\n"; }

		print                                      "ENVIRONMENT: $env_name\n\n";
		if ($fh_logfile ne "") { print $fh_logfile "ENVIRONMENT: $env_name\n\n"; }

		CreateMailto($notify_list);           # create To from input notify list

		$mail{Subject} = "$condition CONDITION: - Program: $program_name\n";
		$mail{Message} = "\n$condition CONDITION: Filetype: $filetype  $message\n\n";
		$mail{Message} .= "From: $login\@$ipaddr <$hostname>\n";
		$mail{Message} .= "\nENVIRONMENT: $env_name\n\n";

		if (sendmail %mail) {
			print                                      "Notification sent to $mail{To}\n";
			if ($fh_logfile ne "") { print $fh_logfile "Notification sent to $mail{To}\n"; }
		}
		else {
			print                                      "\n!Error sending mail:\n$Mail::Sendmail::error\n";
			if ($fh_logfile ne "") { print $fh_logfile "\n!Error sending mail:\n$Mail::Sendmail::error\n"; }
		}
	}

	1;
}

sub NotifyFatal (*$$$$) {

=head1  sub NotifyFatal (*$$$$)

=head2  ABSTRACT:
                prints a fatal error to the screen, a LOGFILE and sends notification mail

=head2  ARGUMENTS:
                0: filehandle of LOGFILE
                1: program name
                2: message
                3: notify list name
                4: Options flag - one of: ""  - do nothing
                                                                  DIE - die after notification
                                                                  ENS - notify ens
                                                                  NOT - no traceback

=cut

	my $env_name;               # environment name
	my $fullhost;               # full name of current host
	my $hostname;               # name of current host
	my $idxsbr;                 # index of routines used for caller
	my $ipaddr;                 # IP address to distribute to
	my $line;                   # line number called from
	my $login;                  # login name of this account
	my $package;                # package name
	my $severity;               # error severity, FATAL or SEVERE
	my $source;                 # name of source file containing subroutine
	my $subr;                   # subroutine name

	my $fh_logfile   = $_[0];   # file handle of log file
	my $program_name = $_[1];   # name of the failing program
	my $message      = $_[2];   # message from program
	my $notify_list  = $_[3];   # name of list to be notified
	my $options      = $_[4];   # options

	my @traces;                 # traceback values returned from caller

	%mail = ();                 # initialize mail hash

	# set error severity
	if ($options =~ /DIE/) {
			$severity = "FATAL";
	}
	else {
			$severity = "SEVERE";
	}

	# print message to screen and logfile
	print                                      "\n$severity ERROR: $message\n\n";
	if ($fh_logfile ne "") { print $fh_logfile "\n$severity ERROR: $message\n\n"; }

	# get account login
	$login = getlogin() || "N/A";

	# get current account information
	GetHostInfo($ipaddr, $hostname, $fullhost);

	# set mail server
	$mail{Smtp} = $server;

	# find environment - use it for mail reply name and print it
	$source = cwd;
	CheckDirectory($source, $env_name);
	$mail{From} = 'Eagle Access <' . $env_name . '@eagleaccess.com>';
	if ($options !~ /NOE/) {
		print                                      "ENVIRONMENT: $env_name\n\n";
		if ($fh_logfile ne "") { print $fh_logfile "ENVIRONMENT: $env_name\n\n"; }
	}

	# if not turned off, print traceback
	if ($options !~ /NOT/) {
		GetTraceback(@traces);
		print                                      "\nTraceback follows:\n";
		if ($fh_logfile ne "") { print $fh_logfile "\nTraceback follows:\n"; }
		for $idxsbr (0 .. $#traces) {
				($package, $source, $line, $subr) = @{ $traces[$idxsbr] };
				print                                      "$subr  called at $source line $line\n";
				if ($fh_logfile ne "") { print $fh_logfile "$subr  called at $source line $line\n"; }
		}
	}

	if ($notify_list ne "") {
		CreateMailto($notify_list);           # create To from input notify list

		$mail{Subject} = "$severity ERROR - Program: $program_name - FAILED\n";
		$mail{Message} = "\n$severity ERROR: $message\n\n";
		$mail{Message} .= "From: $login\@$ipaddr <$hostname>\n";
		$mail{Message} .= "To: $notify_list Notification List\n";
		if ($options !~ /NOE/) {
			$mail{Message} .= "\nENVIRONMENT: $env_name\n\n";
		}

		# if not turned off, get and print traceback
		if ($options !~ /NOT/) {
				$mail{Message} .= "\nTraceback follows:\n";
				for $idxsbr (0 .. $#traces) {
						($package, $source, $line, $subr) = @{ $traces[$idxsbr] };
					$mail{Message} .= "$subr  called at $source line $line\n";
			}
		}
		if (sendmail %mail) {
			print                                      "Notification sent to $mail{To}\n";
			if ($fh_logfile ne "") { print $fh_logfile "Notification sent to $mail{To}\n"; }
		}
		else {
			print                                      "\n!Error sending mail:\n$Mail::Sendmail::error\n";
			if ($fh_logfile ne "") { print $fh_logfile "\n!Error sending mail:\n$Mail::Sendmail::error\n"; }
		}
	}

	# notify ENS if ENS is in options and ens flag is true and not in vaultdev
	if ($options =~ /ENS/ && $flg_ens && $env_name ne "vaultdev") {
 
		# send mail to ENS
		%mail = ();
		$mail{Smtp} = $server;
		$mail{From} = 'Eagle Access <' . $env_name . '@eagleaccess.com>';
		$mail{To} = $ens_address;
		$mail{Subject} = "$severity ERROR - Program: $program_name - FAILED\n";
		$mail{Message} = "\n$severity ERROR: $message\n\n";
		$mail{Message} .= "From: $login\@$ipaddr <$hostname>\n";
		$mail{Message} .= "To: $ens_address Notification List\n";

		if ($options !~ /NOE/) {
			$mail{Message} .= "\nENVIRONMENT: $env_name\n\n";
		}

		# if not turned off, get and print traceback
		if ($options !~ /NOT/) {
				$mail{Message} .= "\nTraceback follows:\n";
				for $idxsbr (0 .. $#traces) {
						($package, $source, $line, $subr) = @{ $traces[$idxsbr] };
						$mail{Message} .= "$subr  called at $source line $line\n";
				}
		}

		if (sendmail %mail) {
			print                                      "Notification sent to $mail{To}\n";
			if ($fh_logfile ne "") { print $fh_logfile "Notification sent to $mail{To}\n"; }
		}
		else {
			print                                      "\n!Error sending mail:\n$Mail::Sendmail::error\n";
			if ($fh_logfile ne "") { print $fh_logfile "\n!Error sending mail:\n$Mail::Sendmail::error\n"; }
		}
	}

	# call NotifyCondition to notify extra addresses of FAILURE
	NotifyCondition($fh_logfile, $program_name, 'FAILURE', '*', $message, '');

	# now die to kill the program if DIE is in options
	if ($options =~ /DIE/) {
			die;
	}
	1;
}

sub NotifyMessage (*$$$) {

=head1  sub NotifyMessage (*$$$)

=head2  ABSTRACT:
                sends a message as notification

=head2  ARGUMENTS:
                0: filehandle of LOGFILE
                1: message subject
                2: message text
                3: notify list name

=cut

	%mail = ();                 # initialize mail hash

	my $fh_logfile   = $_[0];   # file handle of log file

	$mail{Smtp} = $server;
	$mail{From} = 'Eagle Access <EagleAccess@eagleaccess.com>';
	$mail{Subject} = "$_[1]\n";    # message subject
	$mail{Message} = "$_[2]\n";    # message text

	CreateMailto($_[3]);           # create To from input notify list

	# all set up - send the mail using the mail hash
	if (sendmail %mail) {
		print                                      "Notification sent to $mail{To}\n";
		if ($fh_logfile ne "") { print $fh_logfile "Notification sent to $mail{To}\n"; }
	}
	else {
		print                                      "\n!Error sending mail:\n$Mail::Sendmail::error\n";
		if ($fh_logfile ne "") { print $fh_logfile "\n!Error sending mail:\n$Mail::Sendmail::error\n"; }
	}

	1;
}

sub NotifyReport (*$$$) {

=head1  sub NotifyReport (*$$$)

=head2  ABSTRACT:
                sends a report as notification

=head2  ARGUMENTS:
                0: filehandle of LOGFILE
                1: program name
                2: report file name
                3: notify list name

=cut

	my $env_name;               # environment name
	my $fullhost;               # full name of current host
	my $hostname;               # name of current host
	my $ipaddr;                 # IP address to distribute to
	my $line;                   # line of report
	my $login;                  # login name of this account
	my $source;                 # name of source file containing subroutine

	my $fh_logfile   = $_[0];   # file handle of log file
	my $program_name = $_[1];   # name of the failing program
	my $rep_name     = $_[2];   # report file name
	my $notify_list  = $_[3];   # name of list to be notified

	%mail = ();                 # initialize mail hash
	$mail{Smtp} = $server;
	$mail{From} = 'Eagle Access <EagleAccess@eagleaccess.com>';

	# get account login
	$login = getlogin() || "N/A";

	# get current account information
	GetHostInfo($ipaddr, $hostname, $fullhost);

	# find environment and print it
	$source = cwd;
	CheckDirectory($source, $env_name);
	if ($fh_logfile ne "") { print $fh_logfile "ENVIRONMENT: $env_name\n\n"; }

	CreateMailto($notify_list);           # create To from input notify list

	$mail{Subject} = "Program: $program_name - Report\n";
	if (open(REPFILE, "<$rep_name")) {
		while ($line = <REPFILE>) {
			$mail{Message} .= "$line";
		}
		close(REPFILE);
	}

	if (sendmail %mail) {
		print                                      "Notification sent to $mail{To}\n";
		if ($fh_logfile ne "") { print $fh_logfile "Notification sent to $mail{To}\n"; }
	}
	else {
		print                                      "\n!Error sending mail:\n$Mail::Sendmail::error\n";
		if ($fh_logfile ne "") { print $fh_logfile "\n!Error sending mail:\n$Mail::Sendmail::error\n"; }
	}

	1;
}

sub SetMailOverride($$) {

=head1  sub SetMailOverride($$)

=head2  ABSTRACT:
                Reads a tables and creates entries in the %overrides hash

=head2  ARGUMENTS:
                0: directory of tables
                1: loader or environment name

=cut

	my $line;              # line from table
	my @fields;            # fields on line

	# set up table name
	my $DIR_TAB = $_[0];
	if (substr($DIR_TAB, -1) ne "/") { $DIR_TAB .= "/"; }
	my $table_name = $DIR_TAB . $_[1] . "_mailoverride.tab";

	# read table and store override parameters
	if (open(TABFILE, "< $table_name")) {
		while ($line = <TABFILE>) {
			chomp($line);
			TrimLine($line);
			if ($line eq "") { next; }      # skip if comment line
			@fields = &parse_line('\s+', 0, $line);
			if ($#fields < 2) { next; }     # skip if not enough fields on line
			$numover++;
			push( @{ $overrides[$numover] }, @fields);
		}
		close(TABFILE);
	}

	1;

}

1;
