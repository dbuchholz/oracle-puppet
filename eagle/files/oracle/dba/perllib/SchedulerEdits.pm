package SchedulerEdits;

=head1  SchedulerEdits.pm

=head2  ABSTRACT:
            Provide custom edits for files processed throught the scheduler
            EdaciCDB   - Edits for client ACI CDB files
            EdaciCLS   - Edits for client ACI CLS files
            EdaciPRF   - Edits for client ACI PRF files
            EdaciTREP  - Makes sure that "TREP" is found on the header record
            Edbrw1     - Makes sure the first word in the header is "SERE"
            Edeim1     - Send 1st confirm and confirm received between 12:00 and 1:00 pm to client
            Edeofnlf   - Removes from a file any records that contain any case form of "END OF FILE" or "END_OF_FILE"
            Eddmfcon   - Edits for client DMF SMF constituent files
            Eddmfcrd   - Edits for client DMF files from CRD
            Eddmffdw   - Edits for client DMF files from FDW
            Eddmficn   - Edits for client DMF index constituent files
            EdUnzip    - Unzips a file
            Edrmtrail  - Removes the last line from a file
            EdDecrypt  - Decrypt PGP encrypted Files
            EdEncrypt  - Encrypt File with PGP
            EdRmwhite  - Removes unnecessary white space in a comma delimited file
            Edaci1     - Combine Edbrw1 and EdRmwhite to form edit for Addenda
            Edxls2pd   - Create a pipe delimited file from an xls file
            Edbssq     - Backslashes occurrences of single quotes
			Edtdtran   - Edits for client TDS transaction file
			Edwspreu   - Edit for WSP Reuters files
            SirsUpdate - Edit to create new Sirs Update file
=head2  ARGUMENTS:
            Arguments to all edits are:
            0: Local Filename (input)
            1: Error Message (output)
            2: Subject Message (output)
            3: Original Filename (input)
            4: Send Message Flag (input as 1 - change to 0 for no message on error)

=head2  REVISIONS:
      RCR 16-JAN-09 xxxxx-x  RCR Add Edwspreu
      JML 27-OCT-08 xxxxx-x  JML Correct single quote issue in SirsUpdate
      RCR 29-SEP-08 xxxxx-x  RCR Added Eddmfcon, Eddmficn
      JML 25-SEP-08 xxxxx-x  JML Added client specific logic to EdEncrypt
      JML 10-MAR-08 xxxxx-x  JML Added client specific logic to EdEncrypt
      JML 05-FEB-08 xxxxx-x  JML Added SirsUpdate
      JML 18-DEC-07 xxxxx-x  JML Added EdaciPRF
      JML 01-NOV-07 xxxxx-x  JML Added Client specific logic for EdEncrypt
      JML 21-AUG-07 xxxxx-x  JML Added quoting to filename for EdEncrypt
      RCR 17-AUG-07 xxxxx-x  RCR Added Edtdtran
      JML 03-JUL-07 xxxxx-x  JML Added Client specific logic for EdEncrypt
      JML 25-MAY-07 xxxxx-x  JML Add code to prevent dual extentions in EdDecrypt
      JML 10-APR-07 xxxxx-x  JML Added Client specific logic for EdEncrypt
      JML 22-MAR-07 xxxxx-x  JML Added a new client to EdEncrypt
      RCR 19-MAR-07 xxxxx-x  RCR Added Edbssq
      RCR 21-FEB-07 xxxxx-x  RCR Revised Eddmffdw for better trailer checking
      RCR 13-DEC-06 xxxxx-x  RCR Changed header line examined in EdaciCLS from line 5 to line 4
      MAB 09-NOV-06 xxxxx-x  MAB Add Edxls2pd
      JML 24-OCT-06 xxxxx-x  JML Add Client screening to EdEncrypt
      RCR 28-SEP-06 xxxxx-x  RCR Added EdaciCDB
      RCR 25-SEP-06 xxxxx-x  RCR Removed temporary write of cmd to log file in EdUnzip
      RCR 20-SEP-06 xxxxx-x  RCR Added Edeofnlf (Edeofnl for a full file)
      MAB 14-Sep-06 xxxxx-x  MAB Add EdRmwhite subroutine
      MAB 07-Sep-06 xxxxx-x  MAB Add EdEncrypt subroutine
      JML 04-AUG-06 xxxxx-x  JML Added EdaciCLS
      JML 27-JUL-06 xxxxx-x  JML Added EdaciTREP
      RCR 28-JUN-06 xxxxx-x  RCR Remove new unzip file before unzipping
      MAB 31-MAY-06 xxxxx-x  MAB Added Edrmtrail
      RCR 26-APR-06 xxxxx-x  RCR Added EdUnzip
      RCR 02-FEB-06 xxxxx-x  RCR Added Eddmfcrd and Eddmffdw
      MAB 13-SEP-05 xxxxx-x  MAB Add mail to indicate if 1st EIM Confirm file not found
      MAB 17-JUN-05 xxxxx-x  MAB Add new parameter to indicate if mail should be sent for Edit
      MAB 17-JUN-05 xxxxx-x  MAB Add Edit for Essex DTC Confirm files
      RCR 14-JUN-05 xxxxx-x  RCR Corrected strays
      MAB 24 MAY-05 xxxxx-x  MAB Add EdDecrypt subroutine
      MAB 09-FEB-05 xxxxx-x  MAB Add New parameter to hold original file name
      MAB 09-FEB-05 xxxxx-x  MAB Initial development

=cut

use strict;
use File::Copy;
use ITSCheck;
use ITSDate;
use ITSTrim;
use ITSCode;
use ITSSystem;
use EagleDate;
use Text::ParseWords;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(EdaciCDB EdaciCLS EdaciPRF EdaciTREP Edbrw1 Edbrw2 Edbrw3 Edpit1 EdDecrypt EdEncrypt Edeim1 
             Eddmfcon Eddmfcrd Eddmffdw Eddmficn 
			EdUnzip Edrmtrail EdRmwhite Edaci1 Edeofnlf Edxls2pd Edbssq Edtdtran Edwspreu SirsUpdate);

my $FirstDTC;       # Indicates if first DTC file has been sent to client
my $SecondDTC;      # Indicates if second DTC file has been sent to client

INIT {
      # Initialize to not sent
      $FirstDTC = 0;
      $SecondDTC = 0;
}

sub EdaciCLS ($$$$$$) {

    my $FileName = $_[0];
    my $OriginalFileName = $_[3];
   # SCALARS
    my $ipaddr;
    my $hostname;
    my $fullhost;
my $inp_file_name;         # name of feed input file
my $out_file_name;         # name of feed file after processing

my $flag_cls = "N";
my $pipe;
my $lcount_cls;
my @clsline;
my $line4;
my $header_date;
my $index;
my $pacer_rec_count;
my $disp_rec_count;
my $date_field;
my $field5;
my $field3;
my $field2;
my $string;
my @split_infile;

$inp_file_name = $FileName;
#$out_file_name = "Post_" . $inp_file_name;
$out_file_name = $inp_file_name;
$out_file_name =~ s/\.[T,t][X,x][T,t]/\.post/g;

@split_infile = split(/\./,$inp_file_name); # split the filename based on "." extension

if( uc($split_infile[0]) =~ /CLSMAIN/ )
{
    $field2 = "MAIN";
}
if( uc($split_infile[0]) =~ /CLSALT1/ )
{
    $field2 = "ALT1";
}

if( uc($split_infile[0]) =~ /CLSALT2/ )
{
    $field2 = "ALT2";
}

    # get current account information
    GetHostInfo($ipaddr, $hostname, $fullhost);

#  try to open the input file - if we can't, die here
if (!open(INFILE, "< $inp_file_name") ) {
    $_[1] = "Cannot open input file on $hostname\n";
    $_[2] = "Error opening $inp_file_name \n";
    return 0;
}
#  try to open the output file - if we can't, die here
if (!open(OUTFILE, "> $out_file_name") ) {
    $_[1] = "Cannot open output file on $hostname\n";
    $_[2] = "Error opening $out_file_name \n";
    return 0;
}
# loop to read a record from the file

      $pipe = "|";

      $lcount_cls = @clsline = <INFILE>;
      $line4 = $clsline[3];
      $line4 =~ s/ //g;

      $header_date = reverse(substr(reverse($line4),0,12));

      for( $index = 0; $index < $lcount_cls; $index++ )
      {
            if( uc($clsline[$index]) =~ /PACER> REPOCLAS/ )
            {
                  $pacer_rec_count = $index;
                  $flag_cls = "Y";
            }
            if( uc($clsline[$index]) =~ /DISPLAY COMPLETED/ )
            {
                  $disp_rec_count = $index;
            }
      }

      if(  $flag_cls ne "Y")
      {
          $_[1] = "PACER> REPOCLAS not found in header -> no processing will occur on $hostname\n";
          $_[2] = "Error with HEADER record in $OriginalFileName \n";
          return 0;
      }

      for( $a = ($pacer_rec_count + 22); $a < $disp_rec_count -1; $a++ )
      {
            if(length($clsline[$a]) >= 87)
            {
                  $date_field = substr($clsline[$a],81,6);
                  $date_field =~ s/ //g;
                  if($date_field ne "")
                  {
                        $field5 = date_validation(substr($clsline[$a],81,6));
                  }
                  else
                  {
                        $field5 = date_validation2($header_date);
                  }

                  $field3 = ( sprintf("%09d",substr($clsline[$a],61,9)));
                  if( $field3 eq "000000000" )
                  {
                        $field3 = "NA"
                  }
                  $string = $pipe.substr($clsline[$a],48,12).$pipe.$field2.$pipe.$field3.$pipe.substr($clsline[$a],72,8).$pipe.$field5.$pipe;
                  $string =~ s/\s+\|/\|/g;
                  $string =~ s/\|\s+/\|/g;
                  $string = substr($string,1,(length($string) - 1));
                  chop($string);
              if ( substr($clsline[$a],8,16) ne "NAME NOT ON FILE" )
                {
                        print OUTFILE $string."\n";
              }
                  $field3 = "";
                  $field5 = "";
            }
      }

   close INFILE;
   close OUTFILE;
   $_[0] = $out_file_name;
   return 1;
}

sub EdaciTREP ($$$$$$) {

    my $FileName = $_[0];
    my $OriginalFileName = $_[3];
    my $indx;
    my $ipaddr;
    my $hostname;
    my $fullhost;

    # get current account information
    GetHostInfo($ipaddr, $hostname, $fullhost);

    open(FEEDFILE, "$FileName");
    my $HeaderLine = <FEEDFILE>;
    close (FEEDFILE);

    $indx = index ($HeaderLine, "TREP");
    if ($indx >= 0) {
        return 1;
    }
    else {
        $_[1] = "TREP not found in header -> no processing will occur on $hostname\n";
        $_[2] = "Error with HEADER record in $OriginalFileName \n";
        return 0;
    }
    1;
}

sub Edbrw1 ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];

      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      close (FEEDFILE);
      TrimString ($HeaderLine);
      if (substr($HeaderLine, 0, 4) eq "SERE") {
            return 1;
      }
    else {
        $_[1] = "SERE not found in header -> no processing will occur\n";
        $_[2] = "Error with HEADER record in $OriginalFileName \n";
        return 0;
    }

      1;
}


sub Edbrw2 ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];
    my $indx;

      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      close (FEEDFILE);
      $indx = index ($HeaderLine, "!");
      if ($indx >= 0) {
            substr($HeaderLine, 0, $indx+1) = "";
      }
      TrimString ($HeaderLine);
      if (substr($HeaderLine, 0, 4) eq "TREP") {
            return 1;
      }
      else {
            $_[1] = "TREP not found in header -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            return 0;
      }

      1;
}

sub Edbrw3 ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];
    my $indx;

      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      $HeaderLine = <FEEDFILE>;
      close (FEEDFILE);
      $indx = index ($HeaderLine, "PACER > VALU");
      if ($indx >= 0) {
            return 1;
      }
      else {
            $_[1] = "PACER>VALU not found in 2nd header Line -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            return 0;
      }

      1;
}


sub Edpit1 ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];
      my $pos1;
      my $pos2;
      $pos1 = $pos2 = -1;
      while ( ($pos1 = index ($FileName, "/", $pos1)) > -1 ) {
            $pos2 = $pos1;
            $pos1++;
      }
    my $TempFileName = substr ($FileName, 0, $pos2+1) . "TEMP";
      my $TrailerRec;
      my $Trailer;
      my $TrailerCount;
      my $Count;
      my $Status;
      my $cmd;
      my $Discard;

      # Allow for the possibility that there is a null/blank last line
      $Status = system ("tail -2 $FileName > $TempFileName");
    open (TRAILERFILE, "$TempFileName");
    $TrailerRec = <TRAILERFILE>;
      ($Trailer, $TrailerCount) = split(/,/, $TrailerRec);
      $Trailer =~ s/\"//g;
      TrimString($Trailer);
      if ($Trailer ne "TRAILER-") {
            $TrailerRec = <TRAILERFILE>;
            ($Trailer, $TrailerCount) = split(/,/, $TrailerRec);
            $Trailer =~ s/\"//g;
            TrimString($Trailer);
            if ($Trailer ne "TRAILER-") {
                  $_[1] = "TRAILER record not found, file will not be loaded\n";
                  $_[2] = "Error with TRAILER record in $OriginalFileName \n";
                  return 0;
            }
            $Discard = 2;
      }
      else {
            $Discard = 3;
      }
      close (TRAILERFILE);

      $cmd = "cat $FileName | wc -l | awk \'{print " . '$1' . "}\'" . " > $TempFileName ";
      $Status = system ($cmd);
      open(COUNTFILE, "$TempFileName");
      $Count = <COUNTFILE>;
      $Count = $Count - $Discard;   # Account for header and trailer records
      close (COUNTFILE);
    unlink ($TempFileName);

      if ($Count != $TrailerCount) {
            $_[1] = "TRAILER record count $TrailerCount Doesn't match number of records in file -> $Count.   File will not be loaded. \n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

      1;

}


sub EdDecrypt ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];
      my $cmd;
      my $NewFileName;
      my $num;
      my $loc;
      my $Status;
      my $Quote = '"';
##  my $Code = "The 1 Ring to Rule them ALL!";
      my $Code = "u27YriDcpq2/2J\"MLMolhm}8at|Y";
      my $QCode;

      my $dval = "";
      for (my $idx = 33; $idx <= 126; $idx+=7) {
            $dval .= chr($idx);
      }
      DecodeData ($dval, $Code);

      $QCode = $Quote . $Code . $Quote;

    $NewFileName = $FileName;
      $NewFileName =~ s/\.[P,p][G,g][P,p]/\.dat/g;
##    $NewFileName =~ s/\.PGP/\.dat/g;


#     This code is designed to try to prevent dual extenstions on a file
#     for example .dat.dat

      for ($num = 0; $NewFileName =~ m/\./g; $num++){}
      if ($num == 2) {
         $loc = rindex($NewFileName, ".");
         substr($NewFileName,$loc) ="";
      }

      $cmd = "pgp --decrypt $FileName --passphrase $QCode --output $NewFileName --overwrite remove ";
      $Status = system ($cmd);

      $_[0] = $NewFileName;
      return 1;

      if (-e $NewFileName) {
            $_[0] = $NewFileName;
            return 1;
      }
      else {
            $_[1] = "Unable to decrypt $OriginalFileName Check log files. \n";
            $_[2] = "PGP Decryption Failed \n";
            return 0;
      }
    1;
}

sub EdEncrypt ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];
      my $Client = $_[5];
      my $cmd;
      my $EncryptCmd;
      my $NewFileName;
      my $QFileName;
      my $Status;
      my $Quote = '"';
      my $Key;
      my $OKey;

      if ($Client eq "HSB" || $Client eq "HSBTst" || $Client eq "HSBDr") {
         $Key = "IxPartners";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".asc";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t -a $QFileName -r $OKey ";
      }
      elsif ($Client eq "HSBC" || $Client eq "HSBCTst" || $Client eq "HSBCDev" || $Client eq "HSBCDr") {
         $Key = "HSBCXFR.060925 6am EDT  <connectenterprise\@us.hsbc.com>";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".asc";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t -a $QFileName -r $OKey ";
      }
      elsif ($Client eq "MFCDEV" || $Client eq "MFCTEST" || $Client eq "MFCSTAGE" || $Client eq "MFCTEST2" || $Client eq "MFCPrp" || $Client eq "MFCDev2") {
         $Key = "JHTestKey";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".pgp";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t $QFileName -r $OKey ";
      }
      elsif ($Client eq "MFC" || $Client eq "MFCDr") {
         $Key = "JohnHancockKey";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".pgp";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t $QFileName -r $OKey ";
      }
       elsif ($Client eq "TBC2BBH" ) {
         $Key = "BBHSECURE <security\@bbh.com>";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".pgp";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t $QFileName -r $OKey ";
      }
       elsif ($Client eq "CCM" || $Client eq "CCMTest" ) {
         $Key = "Principal_Financial_DH";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".pgp";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t $QFileName -r $OKey ";
      }
     else {
         $Key = "Mellon Financial Corp. FXRUNIT Mainframe PGP 0 <fxrunit\@mellon.com>";
         $OKey = $Quote . $Key . $Quote;
         $NewFileName = $FileName . ".asc";
         $QFileName = $Quote . $FileName . $Quote;
         $EncryptCmd = "pgp -e -t -a $QFileName -r $OKey ";
      }

      # If a file with the same name as the new encrypted file exists, delete it
	  if (-e $NewFileName) {
	  	$cmd = "rm $NewFileName";
	  	$Status = system ($cmd);
	  }

      $Status = system ($EncryptCmd);

      if (-e $NewFileName) {
            $_[0] = $NewFileName;
            return 1;
      }
      else {
            $_[1] = "Unable to encrypt $OriginalFileName Check log files. \n";
            $_[2] = "PGP Encryption Failed \n";
            return 0;
      }
    1;
}

sub Edeim1 ($$$$$$) {

    my $OriginalFileName = $_[3];
    my $FileNumber;
    my $indx;
    my $FixedFileName;        # Needed because for some reason can't get rid of single quote in message center file name
    my ($Second, $Minute, $Hour, $Day, $Month, $Year);          # Date/Time variables

      # Indicate mail message shouldn't be sent on error
    $_[4] = 0;

    if ($FirstDTC == 0) {
        $FixedFileName= substr($OriginalFileName,1);
        $indx = index ($FixedFileName, ".FIXED");
        $FileNumber = substr ($FixedFileName, $indx-2, 2);
        if ($FileNumber eq "01"){
            $FirstDTC = 1;
            return 1;
        }
        else {
                  ($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
                  if ($Hour >= 7) {
                        $_[1] = "The first confirm file for today has not been received, therefore the client has not ";
                        $_[1] = $_[1] . "received the required file.  Client needs to be notified and the file must be generated. \n";
                        $_[2] = "1st DTC Confirm File not available  \n";
                        # Indicate mail message should be sent
                        $_[4] = 1;
                        return 0;
                  }

            $_[1] = "DTC Confirm file detected but not sent to Essex.  This is because the file $OriginalFileName \n";
            $_[1] = $_[1] . "isn't the first confirm file of the day -- indicated by *01.FIXED in the file name \n";
            $_[2] = "DTC Confirm File $OriginalFileName not sent to Essex  \n";
            return 0;
        }
    }
    elsif ($SecondDTC == 0) {
        ($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
        if ( ($Hour >= 12) && ($Hour <= 13) ) {
            $SecondDTC = 1;
            return 1;
        }
        else {
            $_[1] = "DTC Confirm file detected but not sent to Essex.  This is because the file $OriginalFileName \n";
            $_[1] = $_[1] . " wasn't received between 12:00 and 1:00 \n";
            $_[2] = "DTC Confirm File $OriginalFileName not sent to Essex  \n";
            return 0;
        }

    }
    else {
        $_[1] = "DTC Confirm file detected but not sent to Essex.  This is because both confirm files for \n";
        $_[1] = $_[1] . " the day have already been sent to the client \n";
        $_[2] = "DTC Confirm File $OriginalFileName not sent to Essex  \n";
        return 0;
    }

      1;
}

sub Eddmfcon ($$$$$$) {

      my $FileName = $_[0];
      my $FileNameNew = $FileName . "_new";
      my $line;
      my $OriginalFileName = $_[3];

      # open files
	#  try to open the files - if we can't, die here
	if (!open(FEEDFILE, "< $FileName") ) {
		$_[1] = "Cannot open input file on $FileName\n";
		$_[2] = "Error opening $OriginalFileName \n";
		return 0;
	}
	#  try to open the output file - if we can't, die here
	if (!open(NEWFILE, "> $FileNameNew") ) {
		$_[1] = "Cannot open output file $FileNameNew\n";
		$_[2] = "Error opening $OriginalFileName \n";
		return 0;
	}

	# start discarding lines while checking for header
	my $flg_header = 0;
	while ($line = <FEEDFILE>) {
		if (index($line, '<COMMENT>') >= 0) {
			if ( (index($line, 'BEGIN') >= 0) || (index($line, 'START OF') >= 0) ) {
				if (index($line, 'CONSTITUENT') >= 0) {
					$flg_header = 1;
					last;
				}
			}
		}
	}
	if ($flg_header == 0) {
		$_[1] = "header record not found -> no processing will occur\n";
		$_[2] = "Error with HEADER in $OriginalFileName \n";
		close (FEEDFILE);
		close (NEWFILE);
		return 0;
	}

      # check for trailer; writing lines until found, discarding lines when found
	my $flg_trailer = 0;
	while ($line = <FEEDFILE>) {
		if (index($line, '<COMMENT>') >= 0) {
			if (index($line, 'END') >= 0) {
				if (index($line, 'CONSTITUENT') >= 0) {
					$flg_trailer = 1;
					last;               # skip out here to discard trailer and any lines after it
				}
			}
		}
		# if not a trailer, change commas to pipes and print the line to NEWFILE
		$line =~ s/,/\|/g;
		print NEWFILE "$line";
      }
      close (FEEDFILE);
      close (NEWFILE);

      if (!$flg_trailer) {
            $_[1] = "TRAILER record not found -> no processing will occur\n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

	# must rename new file name back to filename to complete process
	rename($FileNameNew, $FileName);

	1;
}

sub Eddmfcrd ($$$$$$) {

      my $FileName = $_[0];
      my $line;
      my $OriginalFileName = $_[3];

      # open file and test header record
      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      if (substr($HeaderLine, 0, 6) ne "HEADER") {
            $_[1] = "HEADER not found in header record -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            close (FEEDFILE);
            return 0;
      }

      # check for trailer
      my $flg_trailer = 0;
      while ($line = <FEEDFILE>) {
            if (substr($line, 0, 7) eq "TRAILER") { $flg_trailer = 1; }
      }
      close (FEEDFILE);

      if (!$flg_trailer) {
            $_[1] = "TRAILER record not found -> no processing will occur\n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

      1;
}

sub Eddmffdw ($$$$$$) {

      my $FileName = $_[0];
      my $line;
      my $OriginalFileName = $_[3];

      # open file and test header record
      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      if (substr($HeaderLine, 0, 2) ne "HH") {
            $_[1] = "HH not found in header record -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            close (FEEDFILE);
            return 0;
      }

      # check for trailer
      my $flg_trailer = 0;
      while ($line = <FEEDFILE>) {
			if (substr($line, 0, 2) eq "TT") { 
				$line = <FEEDFILE>;
				close (FEEDFILE);
				if (!defined($line)) {
					return 1;             # good return - header and trailer found in right places
				}
				else {
					$_[1] = "Records found after TRAILER record -> no processing will occur\n";
					$_[2] = "Error with TRAILER record in $OriginalFileName \n";
					return 0;
				}
			}
      }
      close (FEEDFILE);

      if (!$flg_trailer) {
            $_[1] = "TRAILER record not found -> no processing will occur\n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

      1;
}

sub Eddmficn ($$$$$$) {

	my $FileName = $_[0];
	my $FileNameNew = $FileName . "_new";
	my $line;
	my $OriginalFileName = $_[3];

	# set filetype according to name
	my $filetype = "D";
	if (index($OriginalFileName, "DS_INDEX_M") >= 0) { $filetype = "M"; }

	#  try to open the files - if we can't, die here
	if (!open(FEEDFILE, "< $FileName") ) {
		$_[1] = "Cannot open input file on $FileName\n";
		$_[2] = "Error opening $OriginalFileName \n";
		return 0;
	}
	#  try to open the output file - if we can't, die here
	if (!open(NEWFILE, "> $FileNameNew") ) {
		$_[1] = "Cannot open output file $FileNameNew\n";
		$_[2] = "Error opening $OriginalFileName \n";
		return 0;
	}

	# read first line and get date to add to file
	$line = <FEEDFILE>;
	my ($type, $date) = parse_line(',', 0, $line);
	chomp($date);
	$date = "20" . substr($date, 6, 2) . substr($date, 3, 2) . substr($date, 0, 2);

	# start discarding lines while checking for header
	my $flg_header = 0;
	while ($line = <FEEDFILE>) {
		if (index($line, '<COMMENT>') >= 0) {
			if ( (index($line, 'BEGIN') >= 0) || (index($line, 'START OF') >= 0) ) {
				if (index($line, 'CONSTITUENT') >= 0) {
					$flg_header = 1;
					last;
				}
			}
		}
	}
	if ($flg_header == 0) {
		$_[1] = "header record not found -> no processing will occur\n";
		$_[2] = "Error with HEADER in $OriginalFileName \n";
		close (FEEDFILE);
		close (NEWFILE);
		return 0;
	}

      # check for trailer; writing lines until found, discarding lines when found
	my $flg_trailer = 0;
	while ($line = <FEEDFILE>) {
		if (index($line, '<COMMENT>') >= 0) {
			if (index($line, 'END') >= 0) {
				if (index($line, 'CONSTITUENT') >= 0) {
					$flg_trailer = 1;
					last;               # skip out here to discard trailer and any lines after it
				}
			}
		}
		# if not a trailer, reformat and print the line to NEWFILE
		chomp($line);
		my @fields = split(",", $line);
		my $field20 = $date . $fields[1];
		$line =~ s/,/\|/g;
		$line = join("\|", $line, $filetype, $date, $field20);
		print NEWFILE "$line\n";
      }
      close (FEEDFILE);
      close (NEWFILE);

      if (!$flg_trailer) {
            $_[1] = "TRAILER record not found -> no processing will occur\n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

	# must rename new file name back to filename to complete process
	rename($FileNameNew, $FileName);

	1;
}

sub EdUnzip ($$$$$$) {

    my $FileName = $_[0];
    my $OriginalFileName = $_[3];
    my $cmd;
    my $NewDirName;
    my $NewFileName;
    my $Status;

    $NewFileName = $FileName;
    $NewFileName =~ s/\.[Z,z][I,i][P,p]//g;         # get rid of .zip for new file name
      $NewDirName = substr($NewFileName, 0, rindex($NewFileName, "/"));

      # first, remove new file if it exists
    if (-e $NewFileName) {
            $cmd = "rm $NewFileName";
            $Status = system ($cmd);
    }

    $cmd = "unzip -d $NewDirName -o $FileName";
    $Status = system ($cmd);

    if (-e $NewFileName) {
        $_[0] = $NewFileName;
        return 1;
    }
    else {
        $_[1] = "Unable to unzip $OriginalFileName Check log files. \n";
        $_[2] = "Unzip Failed \n";
        return 0;
    }
    1;
}


sub Edrmtrail ($$$$$$) {

      my $FileName = $_[0];
      my $Done;
      my $Status;
      my $TrailerRec;
    my $pos1;
    my $pos2;
    $pos1 = $pos2 = -1;
    while ( ($pos1 = index ($FileName, "/", $pos1)) > -1 ) {
        $pos2 = $pos1;
        $pos1++;
    }
    my $TempFileName = substr ($FileName, 0, $pos2+1) . "TEMP";

      # Find the last, non blank line, and delete it
      $Done = 0;
      while ($Done == 0) {
            $Status = system ("tail -1 $FileName > $TempFileName");
            open (TRAILERFILE, "$TempFileName");
            $TrailerRec = <TRAILERFILE>;
            chomp ($TrailerRec);
            if ($TrailerRec ne '') {
                  $Done = 1;
            }
			close(TRAILERFILE);
            $Status = system ("cat $FileName | sed '\$d' > $TempFileName");
            $Status = system ("mv $TempFileName $FileName");
      }
1;
}

sub Edrmwhite ($$$$$$) {

      my $FileName = $_[0];
      my $Status;
    my $pos1;
    my $pos2;
    $pos1 = $pos2 = -1;
    while ( ($pos1 = index ($FileName, "/", $pos1)) > -1 ) {
        $pos2 = $pos1;
        $pos1++;
    }
    my $TempFileName = substr ($FileName, 0, $pos2+1) . "TEMP";

      # Replacle all "   ," with ","
      $Status = system ("cat $FileName | sed 's/ *,/,/g' > $TempFileName");
      $Status = system ("mv $TempFileName $FileName");

      # Replacle all ",    " with ","
      $Status = system ("cat $FileName | sed 's/, */,/g' > $TempFileName");
      $Status = system ("mv $TempFileName $FileName");

1;
}


sub Edaci1 ($$$$$$) {

      my $FileName = $_[0];
      my $OriginalFileName = $_[3];

      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      close (FEEDFILE);
      TrimString ($HeaderLine);
      if (substr($HeaderLine, 0, 4) eq "SERE") {
            my $Status;
            my $pos1;
            my $pos2;
            $pos1 = $pos2 = -1;
            while ( ($pos1 = index ($FileName, "/", $pos1)) > -1 ) {
                  $pos2 = $pos1;
                  $pos1++;
            }
            my $TempFileName = substr ($FileName, 0, $pos2+1) . "TEMP";

            # Replacle all "   ," with ","
            $Status = system ("cat $FileName | sed 's/ *,/,/g' > $TempFileName");
            $Status = system ("mv $TempFileName $FileName");

            # Replacle all ",    " with ","
            $Status = system ("cat $FileName | sed 's/, */,/g' > $TempFileName");
            $Status = system ("mv $TempFileName $FileName");

            return 1;
      }
      else {
            $_[1] = "SERE not found in header -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            return 0;
      }



      1;
}

sub Edeofnlf ($$$$$$) {

	# Edeofnlf   - Removes from a file any records that contain any case form of "END OF FILE" or "END_OF_FILE"
	my $FileName = $_[0];
	my $FileNameTemp = $_[0] . "temp";
	my $line;
	my $Status;

	# open input and output files and process
	if (open(INPFILE, "<$FileName")) {
		if (open(OUTFILE, ">$FileNameTemp")) {
			while ($line = <INPFILE>) {
				# skip if line contains END OF FILE
				if ( ($line =~ m/END OF FILE/i) || ($line =~ m/END_OF_FILE/i) ) {
					next;
				}
				# otherwise write out line to new file
				print OUTFILE "$line";
			}
			close(OUTFILE);
			$Status = system("mv $FileNameTemp $FileName");
		}
		close(INPFILE);
	}
	1;
}

sub EdaciCDB ($$$$$$) {

	# EdaciCDB   - Checks for header and trailer in aciCDB files
	my $FileName = $_[0];
	my $FileNameTemp = $_[0] . "temp";
	my $header;
	my $line;
	my $linelast = "";
	my $Status;

	# open input and output files and process
	if (!open(INPFILE, "<$FileName")) {
		$_[1] = "Could not open input file\n";
		$_[2] = "Error processing $FileName\n";
		return 0;
	}
	else {
		if (!open(OUTFILE, ">$FileNameTemp")) {
			$_[1] = "Could not open temporary file\n";
			$_[2] = "Error processing $FileNameTemp\n";
			return 0;
		}
		else {

			# read first record and check header
			$header = <INPFILE>;
			if (!defined($header)) {
				$_[1] = "First line of file does not exist\n";
				$_[2] = "Error processing $FileName\n";
				return 0;
			}
			if (substr($header, 0, 1) ne "!") {
				$_[1] = "First line of file does not start with !\n";
				$_[2] = "Error processing $FileName\n";
				return 0;
			}
			$linelast = $header;

			# process all records printing last line and saving new line to last
			while ($line = <INPFILE>) {
				print OUTFILE "$linelast";
				$linelast = $line;
			}

			# at end of file - is last line a trailer?
			if (substr($linelast, 0, 1) ne "!" || $linelast eq $header) {
				$_[1] = "Last line of file is not a trailer line\n";
				$_[2] = "Error processing $FileName\n";
				return 0;
			}

			close(OUTFILE);
			$Status = system("mv $FileNameTemp $FileName");
		}
		close(INPFILE);
	}
	1;
}
1;

sub EdaciPRF ($$$$$$) {

    # EdaciPRF   - Checks for header and trailer in aciPRF files
    my $FileName = $_[0];
    my $FileNameTemp = $_[0] . "temp";
    my $folder;
    my $header;
    my $letter; 
    my $line;
    my $linelast = "";
    my $location;
    my $ActualFile;
    my $CleanLine;
    my @date_start;
    my $date_string;
    my $Status;
    my $PRFFileName;

    @date_start = (localtime)[0..5];   # get 6 value localtime array to compute elapsed time and write statistics
    $date_string = sprintf("%04d%02d%02d_%02d%02d%02d", $date_start[5] + 1900, $date_start[4] + 1, $date_start[3],$date_start[2], $date_start[1], $date_start[0]);

    $location = index($FileName, "D_");
    if ($location < 0) {
       $location = index($FileName, "M_");
    }

    if ($location < 0) {
       $_[1] = "File name does not match requirements\n";
       $_[2] = "Error processing $FileName\n";
       return 0;
    }
    $letter = substr($FileName, $location, 1);
    $ActualFile = substr($FileName, $location);
    $folder = substr($FileName, 0, $location); 


    # open input and output files and process
    if (!open(INPFILE, "<$FileName")) {
        $_[1] = "Could not open input file\n";
        $_[2] = "Error processing $FileName\n";
        return 0;
    }
    else {
        if (!open(OUTFILE, ">$FileNameTemp")) {
            $_[1] = "Could not open temporary file\n";
            $_[2] = "Error processing $FileNameTemp\n";
            return 0;
        }
        else {

            # read first record and check header
            $header = <INPFILE>;
            if (!defined($header)) {
                $_[1] = "First line of file does not exist\n";
                $_[2] = "Error processing $FileName\n";
                return 0;
            }
            if (substr($header, 0, 1) ne "!") {
                $_[1] = "First line of file does not start with !\n";
                $_[2] = "Error processing $FileName\n";
                return 0;
            }
            $linelast = $header;

            # process all records printing last line and saving new line to last
            while ($line = <INPFILE>) {
                $CleanLine = $linelast; 
                chomp($CleanLine);
                if(substr($CleanLine, 0, 1) ne "!") {
                   print OUTFILE "$CleanLine|$letter\n";
                }
                $linelast = $line;
            }

            # at end of file - is last line a trailer?
            if (substr($linelast, 0, 1) ne "!" || $linelast eq $header) {
               $_[1] = "Last line of file is not a trailer line\n";
                $_[2] = "Error processing $FileName\n";
                return 0;
            }

            close(OUTFILE);

            $PRFFileName = $folder . "aciinfxprf_" . $date_string . "_ACIINDXPRF.dat";
            $Status = system("mv $FileNameTemp $PRFFileName");
            $_[0] = $PRFFileName; 
        }
        close(INPFILE);
    }
    1;
}
1;

sub Edxls2pd ($$$$$$) {

	my $FileName = $_[0];
	my $Options = $_[5];
	my $NewFileName;
	my $cmd;
	my $Status;
	my $ProgramName = "$ENV{HOME}/sitesee/xls2csv.pl";

	$NewFileName = $FileName;
	$NewFileName =~ s/\.[X,x][L,l][S,s]/.csv/g;         # Change xls to csv for new file name

    $cmd = "perl -w $ProgramName -x \'$FileName\' -c \'$NewFileName\' " . $Options;
    $Status = system ($cmd);

	# check for existence of output file and return error if not there
	if (!(-e $NewFileName)) {
		$_[1] = "Output file not created by xls2csv.pl - check input spreadsheet\n";
		$_[2] = "Can not find output file: $NewFileName\n";
		return 0;
	}
	
	$_[0] = $NewFileName;
	1;
}

sub Edbssq ($$$$$$) {

	# Edbssq   - Backslash single quotes
	my $FileName = $_[0];
	my $FileNameTemp = $_[0] . "temp";
	my $line;
	my $Status;

	# open input and output files and process
	if (open(INPFILE, "<$FileName")) {
		if (open(OUTFILE, ">$FileNameTemp")) {
			while ($line = <INPFILE>) {
				# backslash single quotes
				$line =~ s/'/\\'/g;
				print OUTFILE "$line";
			}
			close(OUTFILE);
			$Status = system("mv $FileNameTemp $FileName");
		}
		close(INPFILE);
	}
	1;
}

sub Edtdtran ($$$$$$) {

      my $FileName = $_[0];
      my $line;
      my $OriginalFileName = $_[3];

      # open file and test header record
      open(FEEDFILE, "$FileName");
      my $HeaderLine = <FEEDFILE>;
      if (substr($HeaderLine, 0, 10) ne "HEADER****") {
            $_[1] = "HEADER**** not found in header record -> no processing will occur\n";
            $_[2] = "Error with HEADER record in $OriginalFileName \n";
            close (FEEDFILE);
            return 0;
      }
		my $NewFileName = $FileName . "_new";
      open(NEWFILE, ">$NewFileName") || return 0;

      # check for trailer
      my $num_data = 0;
      my $flg_trailer = 0;
      while ($line = <FEEDFILE>) {
			if (substr($line, 0, 11) eq "TRAILER****") { 
				# check count of data records against trailer record count
				chomp($line);
				my $loccol = index($line, ":");
				my $num_trail = substr($line, $loccol + 1);
				if (!CheckNumber($num_trail)) {
					$_[1] = "Number in trailer: $num_trail is not a number -> no processing will occur\n";
					$_[2] = "Error with TRAILER record in $OriginalFileName \n";
					return 0;

				}
				if ($num_data != $num_trail) {
					$_[1] = "Number in trailer: $num_trail  Data records in file: $num_data -> no processing will occur\n";
					$_[2] = "Error with TRAILER record in $OriginalFileName \n";
					return 0;
				}

				# check if trailer is last record and return
				$line = <FEEDFILE>;
				close (FEEDFILE);
				  close (NEWFILE);
				if (!defined($line)) {
					unlink($FileName);
					rename($NewFileName, $FileName);
					return 1;             # good return - header and trailer found in right places
				}
				else {
					$_[1] = "Records found after TRAILER record -> no processing will occur\n";
					$_[2] = "Error with TRAILER record in $OriginalFileName \n";
					return 0;
				}
			}
			else {
				# must be a data record - count it - and write it to new file
				$num_data++;
				print NEWFILE "$line"; 
			}
      }
      close (FEEDFILE);
      close (NEWFILE);

      if (!$flg_trailer) {
            $_[1] = "TRAILER record not found -> no processing will occur\n";
            $_[2] = "Error with TRAILER record in $OriginalFileName \n";
            return 0;
      }

      1;
}
sub SirsUpdate ($$$$$$) {
	#
	# General variables
	my $Body;                                      # Body of email message
	my $Counter;
	my $Data;
	my @DataPair;
	my @DataPairFinal;
	my $ENSInd;                                    # Indicates if ENS should be called on a failure
	my $ExistingRecords;
	my $fh_newfile;                                # Log File File handle
	my $Header;
	my $Identifier;
	my $indx;
	my $indx2;
	my $lenval;
	my $NewFileName;                               # Log File
	my $NumSecurities;
	my $numval;
	my $numvalue;
	my $NewRecord;
	my $out_file_name;
    my $Secid;
	my $ucSecid;
	my $SecidOld;
	my $SecidType;
	my $ucSecidType;
	my $SecidTypeOld;
	my @Securities;
	my $Sender;                                    # Sender of email message
	my $SirsupdateFile;
    my @split_infile;
	my $Status;                                    # Temporary Status Indicator
	my $Subject;                                   # Subject line of email message
	my $tag;
	my $uctag;
	my $tag_val;
	my $Trailer;
	my $value;
	#
	# Database variables - set when parameter table is read
	my $DatabaseUser = "";
	my $DatabaseCode = "";
	my $DatabaseSID = "";
	my $dval;
	#
	# Variables used with DBI
	my $dbh;                                # Database Handle
	my $sql;                                # SQL statement passed to database
	my $sth;                                # Statement handle
	my $InsertStatement;                    # Select Statement created from update/delete statement
	my $SelectStatement;                    # Update statement to run
	my $RetStatus;
	#
	# Variables for handling .ini file
	my $SirsIniFile;
	my $TagName;
	my $TagValue;
	my $Key;
	my $Value;
	my %ParamHash = (
		DatabaseSID => '',
		DatabaseUser  => '',
		DatabaseCode  => '',
		SendENS	=> ''
	);
	#
		my $ipaddr;
		my $hostname;
		my $fullhost;
	#   get current account information
		GetHostInfo($ipaddr, $hostname, $fullhost);
	#
	# Variables for Universe Table
	#
	my $ID;
	my $SECID;
	my $SECIDTYPE;
	my $TAG;
	my $VALUE;
	#
	# Open New File
	#
	$NewFileName = $_[0] . "_temp";
	open(NEWFILE, ">> $NewFileName");
	$fh_newfile = *NEWFILE;
	#
	# Open the .ini file and get parameters
	#
	$SirsIniFile ="SirsUpdate.ini";
	$Status = open(INI, $SirsIniFile);
	if (!$Status) {
		$_[1] = "Cannot open ini file on $hostname\n";
		$_[2] = "Error opening $SirsIniFile \n";
		return 0;
	}
	while (<INI>) {
		next if /^(\s)*$/;      # skip blank lines
		chomp($_);      # get rid of \n
		($TagName, $TagValue) = parse_line('\s+', 0, $_);                    # parse line into tag and value
		if (substr($TagName, -1) eq ":") { substr($TagName, -1) = ""; }      # remove trailing colon, if present
		TrimString($TagName);
		TrimString ($TagValue);
		if (exists ($ParamHash{$TagName}) ) {
			$ParamHash{$TagName} =  $TagValue;
		}
	}
	close(INI);
	#
	# Make sure all parameters were present
	#
	while ( ($Key, $Value) = each(%ParamHash) ) {
		if ($Value eq '') {
			$_[1] = "Error in ini file on $hostname\n";
			$_[2] = "$SirsIniFile does not contain a value for $Key \n";
			return 0;
		}
	}
	#
	$DatabaseCode = $ParamHash{DatabaseCode};
	$DatabaseUser = $ParamHash{DatabaseUser};
	$DatabaseSID  = $ParamHash{DatabaseSID};
	#
	#    Decode password
	#
	$dval = "";
	for (my $idx = 33; $idx <= 126; $idx+=7) {
		$dval .= chr($idx);
	}
	DecodeData ($dval, $DatabaseCode);
	#
	# Connect to the database
	#
	$dbh = DBI->connect( 'dbi:Oracle:'.$DatabaseSID,
							$DatabaseUser,
							$DatabaseCode,
							 { PrintError => 1, AutoCommit => 0 }
							   ) ;
	if (!$dbh) {
			$_[1] = "SirsUpdate Error on $hostname\n";
			$_[2] = "Unable to Connect to $DatabaseSID: \n";
			return 0;
	}

	$SirsupdateFile = $_[0];
	#
	# Read records from update File
	$Status = open(UPDATEFILE, $SirsupdateFile);
	if (!$Status) {
		$_[1] = "Error in SirsUpdate on $hostname\n";
		$_[2] = "Could not open update file $SirsupdateFile \n";
		return 0;
	}
	#
	#
	$NumSecurities=0;
	while (<UPDATEFILE>) {
		next if /^(\s)*$/;      # skip blank lines
		chomp($_);      # get rid of \n

		if (substr($_, 0, 13) eq "RECORD=HEADER") {           # Skip header and trailer but save for output file
		   $Header = $_;
		   next;
		}

		if (substr($_, 0, 14) eq "RECORD=TRAILER") {           # Skip header and trailer but save for output file
		   $Trailer = $_;
		   next;
		}
        @DataPair = parse_line(',', 0, $_);
		my $num_fields=@DataPair;
		$indx=0;
		$indx2=0;
		@DataPairFinal = ();
		while ($indx < $num_fields) {
		   $tag_val = $DataPair[$indx];
		   ($tag, $value) = split('=', $tag_val);
		   if (defined($tag) && defined($value)) {
			  if (substr ($value, 0, 1) eq '(') {
				 # value is list surrounded by parens - must parse out list
                 if ($value eq '(') {
                    $value = '(""';
                 }
				 $numvalue = 1;
				 $lenval = length($value);
				 while (substr ($value, $lenval-1, 1) ne ')') {
					$numvalue++;
                    if ($DataPair[$indx+1] eq "") {
                       $DataPair[$indx+1] = '""';
                    }
                    if ($DataPair[$indx+1] eq ')') {
                       $DataPair[$indx+1] = '"")';
                    }
					$value = $value . "," .$DataPair[$indx+1];
					$lenval = length($value);
					$indx++;
				 }
                  if (substr ($value, 0, 2) eq '()') {
                     $value = '("")';
                  }
				  $DataPairFinal[$indx2] = $tag . "=" . $value;
				  $indx++;
				  $indx2++;
			  }
			  else {
#                 value is normal value
                  if ($value eq "") {
                      $value = '""';
                  }
                  $DataPairFinal[$indx2] = $tag . "=" . $value;
#                 $DataPairFinal[$indx2] = $DataPair[$indx];
                  $indx++;
                  $indx2++;
			  }
		   }
		}
		$Secid="";
		$SecidType="";
		for $Counter (0 .. (@DataPairFinal - 1))  {
		   ($tag, $value) = split('=', $DataPairFinal[$Counter]);
		   if($Counter == 0) {
			  if($tag eq 'CUSIP'){
				$Secid = $value;
				$SecidType = $tag;
				$Securities[$NumSecurities]=$DataPairFinal[$Counter];
				$NumSecurities++;
				next;
			  }
			  if($tag eq 'SEDOL'){
				$Secid = $value;
				$SecidType = $tag;
				$Securities[$NumSecurities]=$DataPairFinal[$Counter];
				$NumSecurities++;
				next;
			  }
		   }
	#
	#      Set up Insert Statement - if record exists, use update
	#
		  $ucSecid = uc($Secid);
		  $ucSecidType = uc($SecidType);
		  $uctag = uc($tag);
	#
		  $SelectStatement = "SELECT count(*) from sirs_rec where ((upper(secid) = '$ucSecid') and ";
		  $SelectStatement = $SelectStatement . "(upper(secidtype) = '$ucSecidType') and ";
		  $SelectStatement = $SelectStatement . "(upper(tag) = '$uctag')) ";
	#
	#
	#     Prepare the SQL Statement
	#
		  $sql = qq{ $SelectStatement };    # Prepare and execute SELECT
		  $sth = $dbh->prepare($sql);
		  if (!$sth) {
			 $_[1] = "Error in SirsUpdate on $hostname\n";
			 $_[2] = "Unable to prepare sql statement $SelectStatement \n";
			 $sth->finish();
			 return 0;
		  }
	#
	#     Execute the SQL Statement
	#
		  $sth->execute();
	#
		  if (!$sth) {
			 $_[1] = "Error in SirsUpdate on $hostname\n";
			 $_[2] = "Unable to execute sql statement $SelectStatement \n";
			 $sth->finish();
			 return 0;
		  }
	#
	#     If record already exists, update instead of insert
	#
		  $ExistingRecords = $sth->fetchrow_array();
		  $sth->finish();
#
#         Added another single quote to any found in value
          $value =~ s/'/''/g;
	#
		  if ($ExistingRecords == 0) {
	#
			 $InsertStatement = "INSERT INTO sirs_rec (id,secid,secidtype,tag,value) VALUES (";
			 $InsertStatement = $InsertStatement . "seq_sirsuniv.nextval, '" . $Secid . "', '" . $SecidType . "', '" . $tag . "', '" . $value . "')";
		  }
		  else {
			 $InsertStatement = "UPDATE sirs_rec set value = '$value' where ((upper(secid) = '$ucSecid') and ";
			 $InsertStatement = $InsertStatement . "(upper(secidtype) = '$ucSecidType') and ";
			 $InsertStatement = $InsertStatement . "(upper(tag) = '$uctag')) ";
		  }
	#
	#     Prepare the SQL Statement
	#
		  $sql = qq{ $InsertStatement };    # Prepare and execute INSERT
		  $sth = $dbh->prepare($sql);
		  if (!$sth) {
			 $_[1] = "Error in SirsUpdate on $hostname\n";
			 $_[2] = "Unable to prepare sql statement $InsertStatement \n";
			 $sth->finish();
			 return 0;
		  }
	#
	#     Execute the SQL Statement
	#
		  $sth->execute();
		  if (!$sth) {
			 $_[1] = "Error in SirsUpdate on $hostname\n";
			 $_[2] = "Unable to execute sql statement $InsertStatement \n";
			 $sth->finish();
			 return 0;
		  }
	   } 
	}
	close(UPDATEFILE);
	#
	print NEWFILE $Header. "\n";
	for $Counter (0 .. $NumSecurities-1)  {

	#
	#  Set up Select Statement 
	#
	   ($SecidType, $Secid) = split('=', $Securities[$Counter]);
	   $ucSecid = uc($Secid);
	   $ucSecidType = uc($SecidType);
	#
	   $SelectStatement = "SELECT * from sirs_rec where ((upper(secid) = '$ucSecid') and ";
	   $SelectStatement = $SelectStatement . "(upper(secidtype) = '$ucSecidType')) ";
	#
	#
	#  Prepare the SQL Statement
	#
	   $sql = qq{ $SelectStatement };    # Prepare and execute SELECT
	   $sth = $dbh->prepare($sql);
	   if (!$sth) {
		  $_[1] = "Error in SirsUpdate on $hostname\n";
		  $_[2] = "Unable to prepare sql statement $SelectStatement \n";
		  $sth->finish();
		  return 0;
	   }
	#
	#  Execute the SQL Statement
	#
	   $sth->execute();
	#
	   if (!$sth) {
		  $_[1] = "Error in SirsUpdate on $hostname\n";
		  $_[2] = "Unable to prepare sql statement $SelectStatement \n";
		  $sth->finish();
		  return 0;
	   }
	#
	#
		$sth->bind_columns (undef,\$ID, \$SECID, \$SECIDTYPE, \$TAG, \$VALUE);
	#
	#
		$sth->fetch();
		$NewRecord=$SECIDTYPE . "=" . $SECID . "," . $TAG . "=" . $VALUE;

		while( $sth->fetch() ) {
		   $NewRecord=$NewRecord . "," . $TAG . "=" . $VALUE;
		}
		print NEWFILE $NewRecord . "\n";
		$sth->finish();
	}
		print NEWFILE $Trailer. "\n";
	#
	$dbh->disconnect();
	close NEWFILE;
	#

    $out_file_name = $SirsupdateFile;
    @split_infile = split(/SRITSEIM/,$out_file_name); # split the filename based on "." extension
    $SirsupdateFile = $split_infile[0] . "sritsful.dat";
    rename ($NewFileName, $SirsupdateFile);
    $_[0] = $SirsupdateFile;
	#
	1;
	#
}

sub Edwspreu ($$$$$$) {

	my $FileName = $_[0];

	my $date;
	my $datenext;
	my $time;
	my $dts_file;
	my $dts_min;
	my $dts_max;
	my $locdts;
	my $loctype;
	my @date_start;
	
    @date_start = (localtime)[0..5];   # get current date and time
    $date = sprintf("%04d%02d%02d", $date_start[5] + 1900, $date_start[4] + 1, $date_start[3]);
    $time = sprintf("%02d%02d%02d", $date_start[2], $date_start[1], $date_start[0]);

    $loctype = index($FileName, "Intraday");
	if ($loctype >= 0) {
		$locdts = $loctype + 8;
		$dts_file = substr($FileName, $locdts, 14);

		$dts_min = $date . "080000";
		$dts_max = $date . "090000";
		
		if (($dts_file < $dts_min) || ($dts_file > $dts_max)) {
			$_[1] = "Current date/time is: $date/$time\n\n";
			$_[1] = $_[1] . "For file: $FileName  File DTS was: $dts_file\n";
            $_[1] = $_[1] . "This is outside expected range of: $dts_min - $dts_max\n";
            $_[2] = "WSP Reuters File has incorrect timestamp\n";
			return 0;     # error out the file
		}
	}

    $loctype = index($FileName, "EOD");
	if ($loctype >= 0) {
		$locdts = $loctype + 3;
		$dts_file = substr($FileName, $locdts, 14);

		$dts_min = $date . "234000";
		CalcDate8X2T7($date, 1, $datenext); 
		$dts_max = $datenext . "004000";
		
		if (($dts_file < $dts_min) || ($dts_file > $dts_max)) {
			$_[1] = "Current date/time is: $date/$time\n\n";
			$_[1] = $_[1] . "For file: $FileName  File DTS was: $dts_file\n";
            $_[1] = $_[1] . "This is outside expected range of: $dts_min - $dts_max\n";
            $_[2] = "WSP Reuters File has incorrect timestamp\n";
			return 0;     # error out the file
		}
	}

    1;
}
