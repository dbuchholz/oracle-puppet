package ITSDate;

=head1  ITSDate.pm

=head2  ABSTRACT:
		Provide standard date routines - current routines are:
		CalcDate8T2X7         - Calculates the difference between two dates
		CalcDate8X2T7         - Calculates a new date from a date and an offset 
		CalcDateEOM           - Calculates the end of month date from a date
		CalcDateT2X7          - Calculates the difference between two dates
		CalcDateX2T7          - Calculates a new date from a date and an offset 
		CheckDate             - Checks an 8 character date field
		CheckDate7ZZ          - Checks a  7 character date field which may have last 2 characters of zero
		CheckDateDDMMMYY      - Checks a  7 character date field of the form ddmmmyy
		CheckDateDDMMMYYYY    - Checks a  9 character date field of the form ddmmmyyyy
		CheckDateMMMHYY       - Checks a  6 character date field of the form mmm-yy
		CheckDateSDMYYYY      - Checks a 10 character date field of the form dd/mm/yyyy
		CheckDateSMDY         - Checks an 8 character date field of the form mm/dd/yy
		CheckDateSMDYYYY      - Checks a 10 character date field of the form mm/dd/yyyy
		CheckDateUS           - Checks a character date field of the form Month [d]d, yyyy
		CheckDateUSAny        - Checks a character date field of the form Mon[th] [d]d,[ ]yyyy
		CheckDateVSMDYYYY     - Checks a character date field of the form [m]m/[d]d/yyyy
		CheckDateYHMHD        - Checks a character date field of the form yyyy-mm-dd
		CheckDateYMMDD        - Checks a  5 character date field of the form ymmdd
		CheckDateYYMMDD       - Checks a  6 character date field of the form yymmdd
		CheckDateZZZZ         - Checks an 8 character date field which may have last 4 characters of zero
		CheckLeap             - Checks a year to see if it is a leap year
		Convert_date8_mmddyys - Converts yyyymmdd to mm/dd/yy format (s for slash)
		Convert_ddd_doc       - Converts yyyy ddd to doc
		Convert_ddd_mm_dd     - Converts yyyy ddd to yyyy mm dd
		Convert_doc_ddd       - Converts doc to yyyy ddd
		Convert_mm_dd_ddd     - Converts yyyy mm dd to yyyy ddd
		GetDate               - Gets the character system date
		GetTime               - Gets the character system time 
		GetDateTime           - Gets the character system date and time
		GetDateTimeLog        - Gets the character system date and time for log file format
		GetFileAge            - Gets the age of a file in days

=head2  REVISIONS:
		RAM 14-JUN-05 xxxxx-x  RCR Corrected strays
		RAM 23-MAY-05 xxxxx-x  RCR Added CheckDateYHMHD
		RAM 27-OCT-04 xxxxx-x  RCR Added GetFileAge
        RAM 10-MAY-04 13354-0  RCR Added CalcDateEOM
		RAM 02-APR-04 13354-0  RCR Added GetDateTimeLog
		RAM 20-FEB-04 13354-0  RCR Combined date checking to streamline code
		RAM 18-FEB-04 13354-0  RCR Moved CheckDate* routines here from ITSCheck
		RAM 18-AUG-02 13354-0  JGF Initial development

=cut

use strict;
use integer;

use English;
use ITSTrim;
use Time::Local;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CalcDate8T2X7 CalcDate8X2T7 CalcDateEOM CalcDateT2X7 CalcDateX2T7 
            CheckDate CheckDate7ZZ CheckDateDDMMMYY  CheckDateDDMMMYYYY
			CheckDateMMMHYY CheckDateSDMYYYY CheckDateSMDY CheckDateSMDYYYY CheckDateUS CheckDateUSAny
			CheckDateVSMDYYYY CheckDateYHMHD CheckDateYMMDD CheckDateYYMMDD CheckDateZZZZ 
		 	CheckLeap Convert_date8_mmddyys 
		 	Convert_ddd_doc Convert_ddd_mm_dd Convert_doc_ddd Convert_mm_dd_ddd
		 	GetDate GetTime GetDateTime GetDateTimeLog GetFileAge); 

# declare constants for use in routines
my @monthlong;           # long month names
my @monthnm;             # month names
my @numdays;             # number of days in each month
my @DOW_NAME;            # names of days of the week (3 character)
my @MON_NAME;            # names of months of the year (3 character)
my @LAST_DAYS;           # last day of the month in non-leap and leap years
my @OFFSETS;             # Julian day number of the last day of the previous month in non-leap and leap years
my $OSTYPE;              # operating system type
my $THISYEAR;            # current year

INIT {

	# Initializes constants used by date routines
	@monthlong = ("nul","JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE",
						"JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER");

	@monthnm = ("nul","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
	@numdays = ( "00", "31", "29", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31");

	@DOW_NAME = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");

	@MON_NAME = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

#                     JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC
    @LAST_DAYS = ( [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],           # non-leap year
                   [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] );         #     leap year

#                     JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC
    @OFFSETS   = ( [0,  0, 31, 59, 90,120,151,181,212,243,273,304,334],           # non-leap year
                   [0,  0, 31, 60, 91,121,152,182,213,244,274,305,335] );         #     leap year

	$THISYEAR = (localtime)[5] + 1900;                                            # get current year to determine decade

	if ($OSNAME =~ m/Win32/ || $OSNAME =~ m/win32/) { 
	   $OSTYPE = "Win"; 
   }
   else {
	   $OSTYPE = "Unix";
   }

1;
}

sub CalcDate8T2X7($$$) {

	# ABSTRACT:
	# Calculates a difference between two dates

	# ARGUMENTS:
	# 0: Input Date1 (yyyymmdd)
	# 1: Input Date2 (yyyymmdd)
	# 2: Offset

	my $offset;    # calculated offset

	# try to calculate the date using CalcDateT2X7
	if (length($_[0]) == 8 && length($_[1]) == 8) {
		if (CalcDateT2X7(substr($_[0], 0, 4), substr($_[0], 4, 2), substr($_[0], 6, 2),
						 substr($_[1], 0, 4), substr($_[1], 4, 2), substr($_[1], 6, 2), $offset)) {
			$_[2] = $offset;
			return 1;
		}
	}

	# if we get here, calculation failed - return 0
	$_[2] = 0;
	return 0;

	1;
}

sub CalcDate8X2T7($$$) {

	# ABSTRACT:
	# Calculates a date given a start date and an offset

	# ARGUMENTS:
	# 0: Input Date (yyyymmdd)
	# 1: Offset
	# 2: Output Date (yyyymmdd)

	my $day;
	my $month;
	my $year;

	# try to calculate the date using CalcDateX2T7
	if (length($_[0]) == 8) {
		if (CalcDateX2T7(substr($_[0], 0, 4), substr($_[0], 4, 2), substr($_[0], 6, 2), $_[1], $year, $month, $day)) {
			  $_[2] = sprintf("%04d%02d%02d", $year, $month, $day);
			return 1;
		}
	}

	# if we get here, calculation failed - return 0
	$_[2] = 0;
	return 0;

	1;
}

sub CalcDateEOM($) {

	# ABSTRACT:
	# Converts a date to the end of month date

	# ARGUMENTS:
	# 0: input 8 digit date - output end of month date

	my $date = $_[0];
	if (!CheckDate($date)) { return 0; }                   # check for valid date
	my $leap = CheckLeap(substr($date, 0, 4));             # 1 if leap year, 0 if not
	my $month = substr($date, 4, 2);
	my $day = $LAST_DAYS[$leap][$month];                   # get last day in month
	$_[0] = substr($date, 0, 6) . sprintf("%02d", $day);   # construct end of month day

	1;
}

sub CalcDateT2X7 ($$$$$$$) {

	# ABSTRACT:
	# Calculates a difference between two dates

	# ARGUMENTS:
	# 0: Input Year
	# 1: Input Month
	# 2: Input Day
	# 3: Input Year2
	# 4: Input Month2
	# 5: Input Day2
	# 6: Offset number of days (may be + or -)

	my $ddd;
	my $docone;
	my $doctwo;

	# convert first date into day of calendar
	if (Convert_mm_dd_ddd($_[0], $_[1], $_[2], $ddd)) {
		if (Convert_ddd_doc($_[0], $ddd, $docone)) {
			# convert second date into day of calendar
			if (Convert_mm_dd_ddd($_[3], $_[4], $_[5], $ddd)) {
				if (Convert_ddd_doc($_[3], $ddd, $doctwo)) {
					# compute the difference and return
					$_[6] = $doctwo - $docone;
					return 1;
				}
			}
		}
	}

	# if we get here, something failed along the way - return zero
	$_[6] = 0;
	return 0;

	1;
}


sub CalcDateX2T7 ($$$$$$$) {

	# ABSTRACT:
	# Calculates a date given a start date and an offset number of days

	# ARGUMENTS:
	# 0: Input Year
	# 1: Input Month
	# 2: Input Day
	# 3: Offset number of days (may be + or -)
	# 4: Output Year
	# 5: Output Month
	# 6: Output Day

	my $ddd;
	my $doc;
	my $year;
	my $month;
	my $day;

	if (Convert_mm_dd_ddd($_[0], $_[1], $_[2], $ddd)) {
		if (Convert_ddd_doc($_[0], $ddd, $doc)) {
			$doc += $_[3];
			if (Convert_doc_ddd($doc, $year, $ddd)) {
				if (Convert_ddd_mm_dd($year, $ddd, $month, $day)) {
					$_[4] = $year;
					$_[5] = $month;
					$_[6] = $day;
					return 1;
				}
			}
		}
	}

	# if we get here, something failed along the way - return zeros
	$_[4] = 0;
	$_[5] = 0;
	$_[6] = 0;
	return 0;

	1;
}

sub CheckDate ($) {

	# takes an argument that is a string date (8 character - may have leading or trailing whitespace)
	# returns an 8 character string date
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 8 ) { return 0; }

	# if bad character found, return 0
	elsif ($out =~ m/[^0-9]/) { return 0; }

	else {      # have a number in the string - process it
		my $year  = substr($out, 0, 4);
		my $month = substr($out, 4, 2);
		my $day   = substr($out, 6, 2);

		# check year month and day for valid date combination
		# official ITS calendar range: 1901-2399
		# date is in $out - leave alone unless invalid.  If invalid, return 0.
		#
		if ($year lt "1901" || $year gt "2399") { return 0; } 
		else {
			if ($month lt "01" || $month gt "12") { return 0; }
			else {
				if ($day lt "01" || $day gt $numdays[$month]) { return 0; }
				else {
					if ($month eq "02" && $day eq "29") {
						# check for leap year day in a non-leap year
						if ( ( ($year % 4) != 0) ||
							$year eq "2100" || $year eq "2200" || $year eq "2300") { return 0; }
					}
				}
			}
		}
	}

	$_[0] = $out;     # return $out back into argument in case blanks were trimmed off, or set to null
	1;                # return true - valid date is being returned
}

sub CheckDate7ZZ ($) {

	# takes an argument that is a string date (7 character - may have leading or trailing whitespace)
	# argument format is cyymmdd (c is 0 for 19, 1 for 20, etc.)
	# returns an 8 character string date
	# if the date is a valid year but has a day of zero, sets the date to "" and returns 1
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 7 ) { return 0; }

	# if bad character found, return 0
	if ($out =~ m/[^0-9]/) { return 0; }

	if (substr($out, 5, 2) eq "00") {
		$_[0] = "";         # date is of form cyymm00 - null it out and return ok
		return 1;
	}

	$out += 19000000;       # adjust century per the format given
	if (!CheckDate($out)) { return 0; }

	$_[0] = $out;           # return result to argument
	1;                      # return true - valid date is being returned
}

sub CheckDateDDMMMYY ($) {

	# takes an argument that is a string date (7 character - of form ddmmmyy)
	# returns an 8 character string date
	# windows the year at 80
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1

	my $date;
	my $month;
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 7 ) { return 0; }

	my $day  = substr($out, 0, 2);
	my $mmm  = uc(substr($out, 2, 3));
	my $year = substr($out, 5, 2);

	# check month for validity
	for ($month = 1; $month <= 12; $month++) {
		if ($mmm eq $monthnm[$month]) { last; }
	}
	if ($month >  12)        { return 0; }
	$month = sprintf("%02d", $month);

	# window the year at 80
	if ($year  =~ m/[^0-9]/) { return 0; }
	if ($year >= 80) {
		substr($year, 0, 0) = "19";
	}
	else {
		substr($year, 0, 0) = "20";
	}

	$date = $year . $month . $day;                    # construct 8 digit date
	if (!CheckDate($date)) { return 0; }              # check for valid date
	$_[0] = $date;                                    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateDDMMMYYYY ($) {

	# takes an argument that is a string date (9 character - of form ddmmmyyyy)
	# returns an 8 character string date
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1

	my $date;
	my $month;
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 9 ) { return 0; }

	my $day  = substr($out, 0, 2);
	my $mmm  = uc(substr($out, 2, 3));
	my $year = substr($out, 5, 4);

	# check month for validity
	for ($month = 1; $month <= 12; $month++) {
		if ($mmm eq $monthnm[$month]) { last; }
	}
	if ($month >  12)        { return 0; }
	$month = sprintf("%02d", $month);

	# check year
	if ($year  =~ m/[^0-9]/) { return 0; }

	$date = $year . $month . $day;                    # construct 8 digit date
	if (!CheckDate($date)) { return 0; }              # check for valid date
	$_[0] = $date;                                    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateMMMHYY ($) {

	# takes an argument that is a string date (6 character - of form mmm-yy)
	# returns an 8 character string date, which is the last day of the month
	# windows the year at 80
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $day;
	my $month;
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 6 ) { return 0; }

	my $mmm   = uc(substr($out, 0, 3));
	my $hyph  = substr($out, 3, 1);
	my $year  = substr($out, 4, 2);
	# if bad date found, return 0
	for ($month = 1; $month <= 12; $month++) {
		if ($mmm eq $monthnm[$month]) { last; }
	}
	if ($month >  12)        { return 0; }
	if ($hyph  ne "-")       { return 0; }
	if ($year  =~ m/[^0-9]/) { return 0; }

	$month = sprintf("%02d", $month);
	# window the year at 80
	if ($year >= 80) {
		substr($year, 0, 0) = "19";
	}
	else {
		substr($year, 0, 0) = "20";
	}

	# set day to last day of month
	$day = $numdays[$month];
	if ($month eq "02" && !CheckLeap($year)) {
		$day = 28;
	}

	$_[0] = $year . $month . $day;     # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateSDMYYYY ($) {

	# takes an argument that is a string date (10 character - of form dd/mm/yyyy)
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 10 ) { return 0; }

	my $date = join("", substr($out, 6, 4), substr($out, 3, 2), substr($out, 0, 2));
	if (!CheckDate($date)) { return 0; }

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateSMDY ($) {

	# takes an argument that is a string date (8 character - of form mm/dd/yy)
	# returns an 8 character string date
	# assumes year is prefixed by 20 (FTI convention)
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 8 ) { return 0; }

	my $date = join("", "20", substr($out, 6, 2), substr($out, 0, 2), substr($out, 3, 2));
	if (!CheckDate($date)) { return 0; }              # check for valid date

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateSMDYYYY ($) {

	# takes an argument that is a string date (10 character - of form mm/dd/yyyy)
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 10 ) { return 0; }

	my $date = join("", substr($out, 6, 4), substr($out, 0, 2), substr($out, 3, 2));
	if (!CheckDate($date)) { return 0; }

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateUS ($) {

	my $day;
	my $monstr;
	my $month;
	my $year;
	my @tokens;

	# takes an argument that is a string date (of form Month [d]d[,] yyyy)
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	TrimString($out);
	
	# split string and check - if string has wrong number of tokens, return 0
	@tokens = split(/\s+/, $out);
	if ($#tokens != 2) { return 0; }
	($monstr, $day, $year) = @tokens;

	# check month for validity
	for ($month = 1; $month <= 12; $month++) {
		if (uc($monstr) eq $monthlong[$month]) { last; }
	}
	if ($month > 12)        { return 0; }

	# get rid of trailing comma on day, if present
	if (substr($day, -1) eq ",") { substr($day, -1) = ""; }
	
	# reconstruct 8 digit date and check it for validity
	my $date = sprintf("%04s%02s%02s", $year, $month, $day);
	if (!CheckDate($date)) { return 0; }

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateUSAny ($) {

	my $day;
	my $monstr;
	my $month;
	my $year;
	my @tokens;

	# takes an argument that is a string date (of form Mon[th] [d]d[,][ ]yyyy)
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	TrimString($out);
	
	# split string and check - if string has wrong number of tokens, return 0
	@tokens = split(/\s+/, $out);
	if ($#tokens == 1) { 
		# check if no space between day and year
		($tokens[1], $tokens[2]) = split(/,/, $tokens[1])
	}


	if ($#tokens != 2) { return 0; }
	($monstr, $day, $year) = @tokens;

	# check month for validity - only compare to length of month in input string
	for ($month = 1; $month <= 12; $month++) {
		if (uc($monstr) eq substr($monthlong[$month], 0, length($monstr))) { last; }
	}
	if ($month >  12)        { return 0; }

	# get rid of trailing comma on day, if present
	if (substr($day, -1) eq ",") { substr($day, -1) = ""; }
	
	# reconstruct 8 digit date and check it for validity
	my $date = sprintf("%04s%02s%02s", $year, $month, $day);
	if (!CheckDate($date)) { return 0; }

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateVSMDYYYY ($) {

	# takes an argument that is a string date (of form [m]m/[d]d/yyyy)
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null
	TrimString($out);
	
	# if string has bad length, return 0
	if (length($out) < 8 || length($out) > 10) { return 0; }

	# break date out into components, check for validity and get rid of whitespace
	my ($mon, $day, $year) = split("/", $out);
	if (!defined($mon) || !defined($day) || !defined($year)) { return 0; }     # return out if split failed
	TrimString($mon);
	TrimString($day);

	# reconstruct 8 digit date and check it for validity
	my $date = sprintf("%04s%02s%02s", $year, $mon, $day);
	if (!CheckDate($date)) { return 0; }

	$_[0] = $date;    # return back 8 digit date
	1;                # return true - valid date is being returned
}

sub CheckDateYHMHD ($) {

	# takes an argument that is a string date (10 character - yyyy-mm-dd - may have leading or trailing whitespace)
	# returns an 8 character string date
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 10 ) { return 0; }

	# construct 8 character date, check for validity and return
	$out = substr($out, 0, 4) . substr($out, 5, 2) . substr($out, 8, 2);
	if (!CheckDate($out)) { return 0; }
	$_[0] = $out;
	1;
}

sub CheckDateYMMDD ($) {

	# takes an argument that is a string date (5 character - may have leading or trailing whitespace)
	# returns an 8 character string date
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 5 ) { return 0; }

	# if bad character found, return 0
	elsif ($out =~ m/[^0-9]/) { return 0; }

	else {      # have a number in the string - process it
		my $year  = substr($out, 0, 1);          # single digit year from date
		my $month = substr($out, 1, 2);
		my $day   = substr($out, 3, 2);

		# determine four digit year from the single digit by using current year to decide decade
		my $decade = $THISYEAR / 10;
		my $curryr = $THISYEAR - ($decade * 10);   # single digit current year
		my $diff = $curryr - $year;
		if ($diff >=  5) { $decade++; }          # date is in next decade - ex: curryr 9 and year 0
		if ($diff <= -5) { $decade--; }          # date is in last decade - ex: curryr 0 and year 9
		$year = ($decade * 10) + $year;
		substr($out, 0, 0) = $decade;            # prepend decade to $out

		# check year month and day for valid date combination
		# date is in $out - leave alone unless invalid.  If invalid, return 0.
		#
		if ($year lt "1995" || $year gt "2105") { return 0; } 
		else {
			if ($month lt "01" || $month gt "12") { return 0; }
			else {
				if ($day lt "01" || $day gt $numdays[$month]) { return 0; }
				else {
					if ($month eq "02" && $day eq "29") {
						# check for leap year day in a non-leap year
						if ( ($year % 4) != 0) { return 0; }
					}
				}
			}
		}
	}

	$_[0] = $out;     # return $out back into argument in case blanks were trimmed off, or set to null
	1;                # return true - valid date is being returned
}

sub CheckDateYYMMDD ($) {

	# takes an argument that is a string date (6 character - may have leading or trailing whitespace)
	# returns an 8 character string date
	# returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	if (length($out) != 6 ) { return 0; }

	# if bad character found, return 0
	elsif ($out =~ m/[^0-9]/) { return 0; }

	else {      # have a number in the string - process it
		my $year  = substr($out, 0, 2);
		my $month = substr($out, 2, 2);
		my $day   = substr($out, 4, 2);
		$year += 2000;      # don't even ask
		substr($out, 0, 0) = "20";

		# check year month and day for valid date combination
		# date is in $out - leave alone unless invalid.  If invalid, return 0.
		#
		if ($year lt "2000" || $year gt "2099") { return 0; } 
		else {
			if ($month lt "01" || $month gt "12") { return 0; }
			else {
				if ($day lt "01" || $day gt $numdays[$month]) { return 0; }
				else {
					if ($month eq "02" && $day eq "29") {
						# check for leap year day in a non-leap year
						if ( ($year % 4) != 0) { return 0; }
					}
				}
			}
		}
	}

	$_[0] = $out;     # return $out back into argument in case blanks were trimmed off, or set to null
	1;                # return true - valid date is being returned
}

sub CheckDateZZZZ ($) {

	# takes an argument that is a string date (8 character - may have leading or trailing whitespace)
	# returns an 8 character string date
	# if the date is a valid year or all blanks but has a month and day of zero, sets the date to "" and returns 1
	# otherwise, returns 0 if an error was encountered or the date is invalid - otherwise returns 1
	my $out = $_[0];
	if ($out eq "its_null") { return 1; }           # pass through its_null

	# if string has bad length, return 0
	if (length($out) != 8 ) { return 0; }

	if (substr($out, 4, 4) eq "0000") {
		$_[0] = "";            # date is of form "xxxx0000" - null it out and return ok
		return 1;              # return true - valid date is being returned
	}
	
	TrimString($out);
	if (!CheckDate($out)) { return 0; }         # check for bad date

	$_[0] = $out;     # return $out back into argument in case blanks were trimmed off
	1;                # return true - valid date is being returned
}


sub CheckLeap ($) {

	# ABSTRACT:
	# Given a year, returns 1 if it is a leap year, 0 if it is not or if the argument is invalid

	# ARGUMENTS:
	# 1: Year (yyyy)

	my $leap;     # work variable to hold true or false
	my $year;     # work variable to hold the year

	# get argument in and check to see that it is a valid year
	$year = $_[0];
	if ($year =~ m/\D/) { return 0; }
	if ($year < 1901)   { return 0; }

	$leap = 0;
	if ($year % 4 == 0) {
		$leap = 1;
		if ($year % 100 == 0) {
			$leap = 0;
			if ($year % 400 == 0) {
				$leap = 1;
			}
		}
	}

	return $leap;
	1;
}

sub Convert_date8_mmddyys ($) {

	# ABSTRACT:
	# Converts an 8 digit date into mm/dd/yy format (s for slash)

	# ARGUMENTS:
	# 8 digit date (yyyymmdd)

	my $newdate;
	my $date = $_[0];

	if (!CheckDate($date)) {
		# date is bad
		$newdate = "        ";
	}
	else {
		$newdate = join("/", substr($date, 4, 2), substr($date, 6, 2), substr($date, 2, 2));
	}
	return $newdate;

	1;
}

sub Convert_ddd_mm_dd ($$$$) {

	# ABSTRACT:
	# Converts a year and ddd number of days to mm and dd

	# ARGUMENTS:
	# 0: input year (yyyy, 1901-2399)
	# 1: input Julian number of days (ddd, 1-366)
	# 2: output month (mm, 1-12)
	# 3: output days (dd, 1-31)

	my $month;

	my $leap = CheckLeap($_[0]);             # 1 if leap year, 0 if not
	my $ddd = $_[1];
	if ($ddd <= 0 || $ddd > (365 + $leap) ) {
		# input ddd is bad - return 0 for two output arguments
		$_[2] = 0;
		$_[3] = 0;
		return 0;
	}
	else {
		# find what month we're in and calculate the days
		for ($month = 12; $month >= 1; $month--) {
			if ($ddd > $OFFSETS[$leap][$month]) {
				$_[2] = $month;
				$_[3] = $ddd - $OFFSETS[$leap][$month];
				return 1;
			}
		}
	}
	1;
}

sub Convert_mm_dd_ddd ($$$$) {

	# ABSTRACT:
	# Converts a year, month, and days to ddd number of days

	# ARGUMENTS:
	# 0: input year (yyyy, 1901-2399)
	# 1: input month (mm, 1-12)
	# 2: input days (dd, 1-31)
	# 3: Julian number of days (ddd, 1-366)

	my $leap = CheckLeap($_[0]);             # 1 if leap year, 0 if not
	my $month = $_[1];
	my $days  = $_[2];
	if ($month < 1 || $month > 12 ||
		$days  < 1 || $days  > $LAST_DAYS[$leap][$month]) {
		$_[3] = 0;
		return 0;
	}
	else {
		$_[3] = $OFFSETS[$leap][$month] + $days;
		return 1;
	}
	1;
}

sub Convert_ddd_doc ($$$) {

	# ABSTRACT:
	# Converts a year, ddd to doc

	# ARGUMENTS:
	# 0: input year (yyyy, 1901-2399)
	# 1: Julian number of days (ddd, 1-366)
	# 2: day of calendar day number (1-182256)

	my $doc;
	my $leap;

	my $year = $_[0];
	my $ddd  = $_[1];

	# check arguments for validity
	$leap = CheckLeap($year);
	if ($year < 1901 || $year > 2399 ||
		$ddd < 0 || $ddd > (365 + $leap)) {
		$_[2] = 0;
		return 0;
	}
	
	# compute doc
	$doc = (($year - 1901) * 1461) / 4 + $ddd;
	if ($year >= 2101) {
		$doc = $doc - (($year - 2001) / 100);
	}
	$_[2] = $doc;
	1;
}

sub Convert_doc_ddd ($$$) {

	# ABSTRACT:
	# Converts a doc to year, ddd

	# ARGUMENTS:
	# 0: input day of calendar day number (1-182256)
	# 1: output year (yyyy, 1901-2399)
	# 2: output number of days (ddd, 1-366)

	my $ddd;
	my $year;

	my $daysincent = 25 * 1461;                      # number of days in a full century
	my $begin2001 = $daysincent;                     # begin day number in 2001
	my $begin2101 = $begin2001 + $daysincent - 1;    # begin day number in 2101

	my $doc = $_[0];

	# check argument for validity
	if ($doc < 1 || $doc > 182256) {
		$_[1] = 0;
		$_[2] = 0;
		return 0;
	}

	# bump up doc by 1 for conversion algorithm if past missing leap days
	if ($doc >= $begin2101) {
		$doc = $doc + (($doc - $begin2001) / ($daysincent - 1))
	}
	
	# compute year and days
	$year = (($doc - ($doc / 1461) + 364) / 365) + 1900;
	$ddd = $doc - ((($year - 1901) * 1461) / 4);
	$_[1] = $year;
	$_[2] = $ddd;
	1;
}

sub GetDate () {

	# ABSTRACT:
	# Gets the system date in character form

	my $result;           # resulting character date string

	my ($dow, $mon, $day, $year) = (localtime)[6,4,3,5];
	$year += 1900;
	if ($OSTYPE eq "Unix") {
		$result = join(" ", $DOW_NAME[$dow], $MON_NAME[$mon], sprintf("%02d", $day), $year);
	}
	else {           # Windows format date
		$result = $DOW_NAME[$dow] . " " . join("/", sprintf("%02d", ++$mon), sprintf("%02d", $day), $year);
	}
	return $result;
}

sub GetTime () {

	# ABSTRACT:
	# Gets the system time in character form

	my $result = sprintf("%02d:%02d:%02d", (localtime)[2,1,0]);
	return $result;
}
1;

sub GetDateTime () {

	# ABSTRACT:
	# Gets the system date and time in character form

	my $result;           # resulting character date/time string

	my $date = GetDate;       # date string
	my $time = GetTime;       # time string
	if ($OSTYPE eq "Unix") {
		$result = join(" ", substr($date, 0, 10), $time, substr($date, 11, 4));
	}
	else {
		$result = join(" ", $date, $time);
	}
	return $result;
}

sub GetDateTimeLog () {

	# ABSTRACT:
	# Gets the system date and time in character form for writing to log files

	my ($sec, $min, $hour, $day, $mon, $year) = (localtime)[0..5];
	my $result = sprintf("%02d/%02d/%4d %02d:%02d:%02d", ++$mon, $day, $year+1900, $hour, $min, $sec);
	return $result;
}

sub GetFileAge ($$) {

	# ABSTRACT:
	# Gets the age of a file in days

	my $filename = $_[0];

	my $mtime = (stat($filename))[9];
	if (!defined($mtime)) { 
		$_[1] = 0;                       # set default age to 0 if file not there or error from stat
		return 0;
	}
	my $fage  = (time() - $mtime) / (3600 * 24);
	$_[1] = $fage;
	1;
}

1;
