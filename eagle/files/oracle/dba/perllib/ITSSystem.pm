package ITSSystem;

=head1  ITSSystem.pm

=head2  ABSTRACT:
		Provide standard system interface routines - current routines are:
		GetHostInfo - Gets the host IP address, its name, and its fully qualified name

=head2  REVISIONS:
		RAM 04-NOV-02 13354-0  JGF Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(GetHostInfo);

sub GetHostInfo ($$$) {

=head1 sub GetHostInfo ($$$)

=head2 ABSTRACT:
       Gets the host IP address, its name, and its fully qualified name

=head2 ARGUMENTS:
       0: IP address expressed as a string
       1: host name
       2: fully qualified host name

=cut

	use strict;
	use Socket;
	use Sys::Hostname;

	my $idxbyte;
	my @ipnum;

	# set arguments to blanks in case something goes wrong
	@$_ = (" ", " ", " ");

	# get regular host name
	my $hostname = hostname();
	$_[1] = $hostname;

	# get IP address
	my $address = gethostbyname($hostname)         || return 0;
	for $idxbyte (0 .. 3) {
		$ipnum[$idxbyte] = ord(substr($address, $idxbyte, 1));
	}
	$_[0] = join("\.", @ipnum);

	# get fully qualified host name
	$_[2] = gethostbyaddr($address, AF_INET) || return 0;

	1;
}
