package ITSOrder;

=head1  ITSOrder.pm

=head2  ABSTRACT:
		Reorder a vault temporary output file, inserting null fields
		OrderOutputFile       - Reorders an output file and inserts null fields

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 15-JUL-03 13354-0  JGF Initial development

=cut

use strict;
use File::Copy;
use ITSCheck;
use ITSError;
use MasterTable;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(OrderOutputFile);

sub OrderOutputFile($$$$$$$) {

=head1 sub OrderOutputFile($$$$$$$) 

=head2 ABSTRACT:
	   Reorders an output file and inserts null fields

=head2 ARGUMENTS:
	   0: Filehandle of log file
	   1: Tables directory
	   2: Loader name
	   3: Loader type
	   4: Key of file (SMF, PRI...)
	   5: Name of output file
	   6: Name of ordered file

=cut

# SCALARS
my $fh_logfile;            # filehandle of logfile
my $flg_oneonone;          # is correspondence one on one?
my $idxfld;                # index into fields on record
my $idxord;                # index into orders
my $inp_file_name;         # name of input file
my $inpdlm;                # input file delimiter (escaped so it will work with split)
my $input;                 # number of input field or null
my $key;                   # file key designation
my $line;                  # input line from the feed file
my $name;                  # name of item
my $num_error;             # number of errors for this call
my $num_fields;            # number of fields for this record
my $num_outs = 0;          # records in output file
my $num_recs = 0;          # records in input file
my $outdlm;                # output file delimiter
my $order_table_name;      # name of order table
my $out_file_name;         # name of output file
my $output;                # output file number
my $outrec;                # constructed output record
my $status;                # the status of the file opens

# ARRAYS
my @fields;                # values of fields
my @orders;                # order correspondence array
my @output;                # output array
my @output_default;        # default output array

$fh_logfile = $_[0];
$num_error = 0;
$outdlm = GetOutdlm();
$inpdlm = "\\" . $outdlm;  # escape character so it will always work with split

# open order table and read in values
$idxfld =  0;
$key = $_[4];
$order_table_name = $_[1] . $_[3] . "_" . $_[4] . ".cot";
$status = open(TABLE, "< $order_table_name");
if (!$status) {
	print $fh_logfile "Ordering terminated - could not open order table:\n$order_table_name\n";
	print             "Ordering terminated - could not open order table:\n$order_table_name\n";
	return 0; 
} 

while ($line = <TABLE>) {
	chomp($line);  
	($name, $input, $output) = split(/\s+/, $line);
	$idxfld++;
	 
	if (!defined($input) || !defined($output)) { 
		TokenErrorLog($fh_logfile, $idxfld, $line, "Table error for $key: not enough tokens", $num_error); 
	} 
	if (!CheckInteger($output)) { 
		TokenErrorLog($fh_logfile, $idxfld, $line, "Table error for $key: output not a number", $num_error); 
	}
	if ($output != $idxfld) { 
		TokenErrorLog($fh_logfile, $idxfld, $line, "Table error for $key: sequential integrity failure", $num_error); 
	} 
	$output--;                  # decrement to zero based number for arrays
	push(@output_default, "");
	if ($input ne "null") {
		if (!CheckInteger($input)) { 
			TokenErrorLog($fh_logfile, $idxfld, $line, "Table error for $key: input not a number", $num_error); 
		} 
		$input--;               # decrement to zero based number for arrays
		$orders[$input] = $output;
	}
}
close(TABLE);

if ($num_error > 0) {       # don't try to process if there were table errors
	print $fh_logfile "Ordering terminated due to error%s in table\n", $num_error == 1 ? "" : "s";
	print             "Ordering terminated due to error%s in table\n", $num_error == 1 ? "" : "s";
	return 0; 
}

$num_fields = $#output_default > $#orders ? $#output_default : $#orders;

# determine if files have full one to one correspondence
$flg_oneonone = 1;
for $idxord (0 .. $num_fields) {
	if (!defined($orders[$idxord])) { 
		$flg_oneonone = 0; 
		last;
	}
	elsif ($orders[$idxord] != $idxord) { 
		$flg_oneonone = 0;
		last;
	}
}

# set up file names
$inp_file_name = $_[5];
$out_file_name = $_[6];

if ($flg_oneonone) {
	# files have one on one correspondence - just copy old to new
	$status = copy("$inp_file_name", "$out_file_name");
	if (!$status) {
		print $fh_logfile "Ordering terminated - could not copy input file to output file:\n$inp_file_name\n$out_file_name\n";
		print             "Ordering terminated - could not copy input file to output file:\n$inp_file_name\n$out_file_name\n";
	}
	else {
		print $fh_logfile "Ordering process copied file for key: $key\n";
		print             "Ordering process copied file for key: $key\n";
	}
}
else {
	# correspondence is not one on one - must reorder file
		
	# open input and output files
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { 
		print $fh_logfile "Ordering terminated - could not open input file:\n$inp_file_name\n";
		print             "Ordering terminated - could not open input file:\n$inp_file_name\n";
		return 0; 
	} 

	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { 
		print $fh_logfile "Ordering terminated - could not open input file:\n$out_file_name\n";
		print             "Ordering terminated - could not open input file:\n$out_file_name\n";
		return 0; 
	} 

	# loop reading a record
	GETRECORD: while ($line = <INFILE>) {
		chomp($line); 
		$num_recs++;

		# process data records
		# split the record into fields
		@fields = split(/$inpdlm/, $line, -1);

		# set up output array using default fields and ordered fields
		@output = @output_default;
		for $idxfld (0 .. $num_fields) {
			if (defined($orders[$idxfld])) {
				$output[$orders[$idxfld]] = $fields[$idxfld];
			}
		}

		$outrec = join("$outdlm", @output);
		print OUTFILE "$outrec\n";
		$num_outs++;
	}

	print $fh_logfile "Ordering process output $num_outs records for key: $key\n";
	print             "Ordering process output $num_outs records for key: $key\n";

	# close files
	close INFILE;
	close OUTFILE;
}

1;
}
