package EagleDate;

=head1  EagleDate.pm

=head2  ABSTRACT:
		Date Validation routines from Eagle
        date_validation
        date_validation2

=head2  REVISIONS:
		JML 19-SEP-02 13354-0  JGF Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(date_validation date_validation2);

###############################################################################################
#           Start Sub Routines date_validation(yymmdd)
###############################################################################################
sub date_validation
{
      my $outdate;
      my($indate) = @_;
      if ( substr($indate,0,2) >= "60" && substr($indate,0,2) <= "99" )
            {
                  $outdate = "19".$indate;
            }
      else
            {
                  $outdate = "20".$indate;
            }
      return $outdate;
}
1;
###############################################################################################
#           End Sub Routines date_validation
###############################################################################################

###############################################################################################
#           Start Sub Routines date_validation2 (converts (dd-mon-yyyy) to (yyyymmdd))
###############################################################################################
sub date_validation2
{
      my @currtime1;
      my $outdate;
      my($indate) = @_;
      chop($indate);
      @currtime1 = split(/\-/,$indate);

      if ($currtime1[1] eq "JAN") {$currtime1[1] = "01"}
      if ($currtime1[1] eq "FEB") {$currtime1[1] = "02"}
      if ($currtime1[1] eq "MAR") {$currtime1[1] = "03"}
      if ($currtime1[1] eq "APR") {$currtime1[1] = "04"}
      if ($currtime1[1] eq "MAY") {$currtime1[1] = "05"}
      if ($currtime1[1] eq "JUN") {$currtime1[1] = "06"}
      if ($currtime1[1] eq "JUL") {$currtime1[1] = "07"}
      if ($currtime1[1] eq "AUG") {$currtime1[1] = "08"}
      if ($currtime1[1] eq "SEP") {$currtime1[1] = "09"}
      if ($currtime1[1] eq "OCT") {$currtime1[1] = "10"}
      if ($currtime1[1] eq "NOV") {$currtime1[1] = "11"}
      if ($currtime1[1] eq "DEC") {$currtime1[1] = "12"}
      $currtime1[2] =~ s/ //g;
      $currtime1[1] =~ s/ //g;
      $currtime1[0] =~ s/ //g;

      $outdate = $currtime1[2].$currtime1[1].$currtime1[0];
      return $outdate;
}

1;
###############################################################################################
#           End Sub Routines date_validation2
###############################################################################################
