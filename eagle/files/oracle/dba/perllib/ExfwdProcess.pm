package ExfwdProcess;

=head1  ExfwdProcess.pm

=head2  ABSTRACT:
		Get forwards definitions and enable processing of Exshare values - current routines are:
		ExpandExfwd           - Expands a file containing a list of forwards cross rates into 
								another file containing the full matrix of cross rates

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 24-MAY-04 13354-0  JGF Initial development

=cut

use strict;
use ITSCheck;
use ITSTrim;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ExpandExfwd);

sub ExpandExfwd($$$$) {

=head1 sub ExpandExfwd($$$$) 

=head2 ABSTRACT:
	   Expands a list of forwards cross rates in a file to a fully populated
	   matrix of all possible cross rates

=head2 ARGUMENTS:
	   0: Name of the input forwards cross rates file
	   1: Name of the output cross rates matrix
	   2: Number of headers in the output file
	   3: Number of records in the output file

=cut

# SCALARS
my $base;                  # code for intermediate base currency
my $currfrom;              # from currency
my $currto;                # to currency
my $effective_date;        # the effective date for the file
my $exrate;                # the calculated cross exchange rate
my $format;                # variable to construct the output format for the rate
my $idxfld;                # index into fields on record
my $idxfrom = -1;          # index of from currency
my $idxnew;                # index of new field
my $idxto   = -1;          # index of to currency
my $idxrate = -1;          # index of exrate
my $idxdate = -1;          # index of date
my $idxsou  = -1;          # index of source code 
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $logval;                # logarithm of the rate to guage its magnitude
my $maxfields;             # max number of fields on record
my $maxnew;                # max number of new fields on record
my $numdp;                 # number of decimal places in output format
my $num_outs	 = 0;      # records in output file
my $num_recs	 = 0;      # records in input file
my $out_file_name;         # name of output file
my $outrec;                # constructed output record
my $source_code;           # the source code for the file
my $status;                # the status of the file opens
my $value;                 # temporary value
my $valuefrom;             # temporary value
my $valueto;               # temporary value

# ARRAYS
my @outarray;              # output array
my @fields;                # values of fields
my @names;                 # names of fields

# HASHES
my %currlist;              # list of all currency codes
my %matrix;                # matrix of all cross rates

$_[3] = 0;                        # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
$status = open(INFILE, "< $inp_file_name");
if (!$status) { return 0; } 

$out_file_name = $_[1];
$status = open(OUTFILE, "> $out_file_name");
if (!$status) { return 0; } 
$num_outs++;

# loop reading a record
GETRECORD: while ($line = <INFILE>) {
	chomp($line);                           # get rid of \n
	$num_recs++;

	# split the record into fields
	if ($num_recs == 1) {
		print OUTFILE "$line\n";                # output header record
		# for first record, get the names and find "from_curr" and "to_curr"
		@names  = split(/\|/, $line, -1);
		TrimString($names[0]);
		for $idxfld (0 .. $#names) {
			if ($names[$idxfld] eq "from_curr")      { $idxfrom = $idxfld }
			if ($names[$idxfld] eq "to_curr")        { $idxto   = $idxfld }
			if ($names[$idxfld] eq "effective_date") { $idxdate = $idxfld }
			if ($names[$idxfld] eq "source_code")    { $idxsou  = $idxfld }
		}
		# if any of the fields are not found, the expansion can not be done
		if ($idxfrom < 0 || $idxto < 0)  { return 0; }
		if ($idxdate < 0 || $idxsou < 0) { return 0; }
	}
	else {
		# process data records
		# get individual fields
		@fields = split(/\|/, $line, -1);
		$currfrom = $fields[$idxfrom];
		$currto   = $fields[$idxto];

		# save date and source code from second record
		if ($num_recs == 2) {
			$effective_date = $fields[$idxdate];
			$source_code    = $fields[$idxsou];
			$maxfields = $#fields;
			$maxnew = $maxfields - 7;
		}

		GETFIELD: for $idxrate (7 .. $maxfields) {
			$exrate = $fields[$idxrate];
			$idxnew = $idxrate - 7;
			if ($exrate eq "") { next GETFIELD; }

			# skip field if rate not a number or le 0
			if (!CheckNumber($exrate)) { next GETFIELD; }
			if ($exrate <= 0.0)        { next GETFIELD; }

			# add both currencies to currency list if they don't exist
			if (!exists($currlist{ $currfrom })) { $currlist{ $currfrom } = 1; }
			if (!exists($currlist{ $currto   })) { $currlist{ $currto   } = 1; }

			# add rate to matrix
			$matrix{ $currfrom } { $currto } [$idxnew] = $exrate;
		}
	}
}

# now at end of file - generate the full matrix, writing out as we do so

foreach $currfrom (sort keys %currlist) {

	GETCURRTO: foreach $currto (sort keys %currlist) {

		GETNEWFIELD: for $idxnew  (0 .. $maxnew) {

			$value = "";
			$exrate = "";
			if    ($currfrom eq $currto) {
				# if currencies are the same, rate is 1.0
				$exrate = 1.0;
			}
			elsif (defined($matrix{ $currfrom } { $currto } [$idxnew])) {
				# rate exists - do nothing
				next GETNEWFIELD;
			}
			else {
				if (defined($matrix{ $currto } { $currfrom } [$idxnew])) {
					# inverse exists
					$value = 1.0 / $matrix{ $currto } { $currfrom } [$idxnew];
				}
				else {
					# try to calculate the rate using an intermediate base currency if the cross rates exist
					foreach $base ("USD","GBP","EUR") {
						if (defined($matrix{ $currfrom } { $base } [$idxnew]) && defined($matrix{ $currto } { $base } [$idxnew])) { 
							# can calculate through a base currency
							$valuefrom = $matrix{ $currfrom } { $base } [$idxnew];
							$valueto   = $matrix{ $currto   } { $base } [$idxnew];
							$value = $valuefrom / $valueto;
							last;
						}
						if (defined($matrix{ $base } { $currfrom } [$idxnew]) && defined($matrix{ $base } { $currto } [$idxnew])) { 
							# can calculate through a base currency
							$valuefrom = $matrix{ $base } { $currfrom } [$idxnew];
							$valueto   = $matrix{ $base } { $currto } [$idxnew];
							$value = $valueto / $valuefrom;
							last;
						}
					}
				}

				# format calculated exchange rate according to its magnitude
				if ($value ne "") {
					$numdp = 6;
					if ($value < 1) {
						$logval = log($value) / log(10);
						$numdp = 7 - int($logval);
					}
					$format = "%." . $numdp . "f";
					$exrate = sprintf($format, $value);
				}
			}
			if ($exrate ne "") {
				# check the number before writing out (this will squish out unwanted trailing zeros also)
				if (CheckNumber($exrate)) {
					# new rate is ok - store it
					$matrix{ $currfrom } { $currto } [$idxnew] = $exrate;
				}
			}

		}

		# set undefined elements of the array to null before writing
		# use outarray so we don't mess with the undefined contents of matrix
		@outarray = ();
		for $idxnew (0 .. $maxnew) {
			if (defined($matrix{ $currfrom } { $currto } [$idxnew]) ) { 
				push (@outarray, $matrix{ $currfrom } { $currto } [$idxnew]);
			}
			else {
				push (@outarray, "");
			}
	   }

		# write out the new record, adding CASH to the front of the ISO codes
		$outrec = join("|", "CASH$currfrom", $source_code, $effective_date, 
					   "CASH", "CASH$currfrom", "CASH", "CASH$currto", 
					   join("|", @outarray) );
		print OUTFILE "$outrec\n";
		$num_outs++;
	}
}

$_[3] = $num_outs;                 # set number of output records

# close files
close INFILE;
close OUTFILE;

1;
}
