package MarkerTable;

=head1  MarkerTable.pm

=head2  ABSTRACT:
		Package used to process loader markers

=head2  ROUTINES:
		CreateMarkerHash($table_file)
			Creates the hash MarkerDefinitionHash containing the information from the Marker field definition file.
			$table_file     The name of the file containing the Marker field definitions

		ProcessMarker($marker_name, $marker_value, $data_value)
			Processes a data value according to the defintion of the marker value for this marker
			$marker_name	The name of the marker
			$marker_value	The value of the marker
			$data_value		The data value to be processed

=head2  REVISIONS:
		RCR 02-MAY-07 13354-0  RCR Changed parse_line call to check for continuous whitespace
		RCR 23-APR-07 13354-0  RCR Changed single marker value processing to not use range as default
		RAM 30-AUG-05 13354-0  RCR Changed quotewords to parse_line
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 29-AUG-04 13354-0  RCR Added special *ALLOTHERS marker value
		RAM 03-AUG-04 13354-0  RCR Added rounding of certain values after a CALC operation
		RAM 13-JAN-04 13354-0  JGF Added CHECKDIGIT operation
		RAM 06-OCT-03 13354-0  JGF Added STRVAL operation
		RAM 10-JUN-02 13354-0  JGF Initial Development

=cut

use ITSCalc;
use ITSCheck;
use ITSTrim;
use Text::ParseWords;
use TranslateTable;

use strict;

use vars qw(@ISA @EXPORT %MarkerDefinitionHash);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CreateMarkerHash ProcessMarker %MarkerDefinitionHash);

sub CreateMarkerHash($) {

=head1 sub CreateMarkerHash($)

=head2 ABSTRACT:
       Creates the hash MarkerDefinitionHash containing the information from the Marker field definition file.

=head2 ARGUMENTS:
       0: $table_file     The name of the file containing the Marker field definitions

=cut

	my $CurrentMarkerName = "";
	my $InMarkerName;

	my $item;
	my $lenexpr;        # length of MarkerExpr
	my $locdash;        # location of dash in MarkerExpr

	my $MarkerBeg;
	my $MarkerEnd;
	my $MarkerExpr;
	my $MarkerVal;
	my $rMarkerValueHash = {};

	my @fields;
	my @InstructionSyntax = ();
	my @ListofInstructions = ();

	%MarkerDefinitionHash = ();       # clear marker hash in case there's no table

	# Get file name from first parameter
	my $table_name = $_[0];

	# Open file or return if file can't be opened
	if (!open(Marker_Table, "< $table_name")) {
		return 0;
	}

	# Read & Process all records in the Marker Table
	while (my $InputLine = <Marker_Table>) {
		chomp ($InputLine);
		TrimString($InputLine);

		# Check if record is start of new Marker definition
		if ((substr $InputLine, 0,1) eq "&") {
			($InMarkerName = $InputLine) =~ s/ //g;
			$InMarkerName =~ s/&//g;
			if ($CurrentMarkerName ne "") {
				$MarkerDefinitionHash{$CurrentMarkerName} = $rMarkerValueHash;
				$rMarkerValueHash = {};
			}
			$CurrentMarkerName = $InMarkerName;
		}

		# If not, process marker value->definition record combination
		else {
			@fields = parse_line('\s+', 0, $InputLine);

			$MarkerExpr = shift @fields;     # shift off Marker Expression
			# default is that marker expression is a constant
			# check for marker range and set up values
			$lenexpr = length($MarkerExpr);
			$locdash = index($MarkerExpr, "--"); 
			if ($locdash >= 1 && $locdash <= ($lenexpr - 2)) {
				# marker expression is range
				$MarkerBeg = substr($MarkerExpr, 0, $locdash);
				$MarkerEnd = substr($MarkerExpr, $locdash + 2);
			}
			else {
				# marker expression is single value
				$MarkerBeg = $MarkerExpr;
				$MarkerEnd = $MarkerExpr;
			}

			# Parse Marker Value definition 
			foreach $item (@fields) {
				if ($item ne ";") { 
					push @InstructionSyntax, $item;
				}

				# ';' indicates new instruction for given Marker Value
				else {
					push @ListofInstructions, [@InstructionSyntax];
					@InstructionSyntax = ();
				}
			}
            push @ListofInstructions, [@InstructionSyntax];
			@InstructionSyntax = ();

			if ($MarkerBeg eq $MarkerEnd) {
				# single marker
				push (@{$$rMarkerValueHash{$MarkerBeg}}, @ListofInstructions);
			}
			else {
				# loop through all values of the marker, associating it with this ListofInstructions
				for $MarkerVal ($MarkerBeg .. $MarkerEnd) {
					push (@{$$rMarkerValueHash{$MarkerVal}}, @ListofInstructions);
				}
			}
			@ListofInstructions = ();
		}
	}

	# Push final Marker Definition
	$MarkerDefinitionHash{$CurrentMarkerName} = $rMarkerValueHash;
	close (Marker_Table);
	1;
}


sub ProcessMarker ($$$) {

=head1 sub ProcessMarker ($$$)

=head2 ABSTRACT:
       Processes a data value according to the defintion of the marker value for this marker

=head2 ARGUMENTS:
       1: Marker Name
       2: Marker Value
       3: Data Value - may be modified depending on marker

=head2 NOTES:
       Uses MarkerDefinitionHash created in CreateMarkerHash

=cut

	my $address;                # address of start of string value
	my $lenstr;                 # length of string value
	my $lenval;                 # length of data value
	my $operand;                # CALC operand value
	my $operation;              # operation of this action
	my $operator;               # CALC operator 
 
	# put arguments into recognizable scalars
	my ($marker_name, $marker_value, $data_value) = @_;

	# check to see if this marker value exists for this marker in the hash
	if (!exists($MarkerDefinitionHash{ $marker_name } { $marker_value })) {
		$marker_value = "*ALLOTHERS";                                # if it doesn't exist, try this value
	}

	# check to see if this marker value exists for this marker in the hash
	if (exists($MarkerDefinitionHash{ $marker_name } { $marker_value })) {

		# marker name and value exist - get base reference to hash for this marker and value 
		my $ArrayRef = $MarkerDefinitionHash{ $marker_name } { $marker_value };

		# loop through all operations for this value
		foreach my $action (@$ArrayRef) {

			# get field and associated values out of hash and process them
			$operation = $$action[0];

			if    ($operation eq "CALC") {
				# calculation on operator, operand
				if ($data_value ne "") {
					$operator = $$action[1];
					$operand  = $$action[2];
					if    ($operator eq "+") { $data_value += $operand; }
					elsif ($operator eq "-") { $data_value -= $operand; }
					elsif ($operator eq "*") { $data_value *= $operand; }
					elsif ($operator eq "/") { $data_value /= $operand; }
					else                     { return 0; }              # operator is not valid

					# check after calculation to see if it resulted in value very close to but not quite even
					if (length($data_value) >= 10) {
						if (index($data_value, ".") >= 0) {
							if ((substr($data_value, -7, 7) eq "0000001") || (substr($data_value, -7, 7) eq "9999999")) {  
								$data_value = sprintf("%12.10f",$data_value);
								CheckNumber($data_value);                          # trim trailing zeros off of number
							}
						}
					}
				}
			}
			elsif ($operation eq "CHECKDIGIT") {
				ComputeCheckDigit($data_value);
			}
			elsif ($operation eq "CONSTANT") {
				# set to constant
				$data_value = $$action[1];
			}
			elsif ($operation eq "INVERT") {
				# invert value
				if ($data_value ne "") {
					$data_value = 1.0 / $data_value;
				}
			}
			elsif ($operation eq "LC") {
				# lowercase field
				$data_value = lc($data_value);
			}
			elsif ($operation eq "NOTHING") {
				# do nothing
			}
			elsif ($operation eq "NULL") {
				# set to null
				$data_value = "";
			}
			elsif ($operation eq "STRTRIM") {
				TrimString($data_value);
			}
			elsif ($operation eq "STRVAL") {
				# string value on address, length
				if ($data_value ne "") {
					$lenval = length($data_value);
					$address = $$action[1];
					if ($address > $lenval) { 
						$data_value = "";                      # address is beyond end of string
					}
					else {
						$address--;                                               # convert to zero based address
						$lenstr  = $$action[2];
						if ($lenstr > 0 && $lenstr <= $lenval) {
							$data_value = substr($data_value, $address, $lenstr);
						}
						else {
							$data_value = substr($data_value, $address);           # return to end of string
						}
					}
				}
			}
			elsif (substr($operation, 0, 2) eq "TR") {
				# process translation
				ProcessTranslate($operation,$data_value);
			}
			elsif ($operation eq "UC") {
				# uppercase field
				$data_value = uc($data_value);
			}
			else {      # field type is not valid
				return 0;
			}
		}
	}
	else {     # marker name or value does not exist in hash
		return 0;
	}
	# return the current data value to the third argument
	$_[2] = $data_value;
	1;
}
