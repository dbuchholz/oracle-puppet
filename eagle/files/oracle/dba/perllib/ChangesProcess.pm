package ChangesProcess;

=head1  ChangesProcess.pm

=head2  ABSTRACT:
		Process changes in IDs - current routines are:
		CompressChanges        - Compresses a file containing a list of ID changes eliminating 
							 	changes to the same ID and duplicate change records

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 31-DEC-04 13354-0  RCR Initial development

=cut

use strict;
use ITSCheck;
use ITSTrim;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CompressChanges);

sub CompressChanges($$$$) {

=head1 sub CompressChanges($$$$) 

=head2 ABSTRACT:
		Compresses a file containing a list of ID changes eliminating 
		changes to the same ID and duplicate change records

=head2 ARGUMENTS:
	   0: Name of the input IDs changes file
	   1: Name of the output IDs changes file
	   2: Number of headers in the output file
	   3: Number of records in the output file

=cut

# SCALARS
my $idfrom;                # ID from
my $idto;                  # ID to
my $idxfrom = 3;           # index of from ID
my $idxto   = 4;           # index of to ID
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $num_outs = 0;          # records in output file
my $num_recs = 0;          # records in input file
my $out_file_name;         # name of output file
my $outrec;                # constructed output record
my $status;                # the status of the file opens

# ARRAYS
my @fields;                # values of fields

# HASHES
my %records;               # list of all unique records

$_[3] = 0;                 # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
$status = open(INFILE, "< $inp_file_name");
if (!$status) { return 0; } 

$out_file_name = $_[1];
$status = open(OUTFILE, "> $out_file_name");
if (!$status) { return 0; } 

# reoutput header
$line = <INFILE>;
print OUTFILE "$line";
$num_outs++;

# loop reading a record
GETRECORD: while ($line = <INFILE>) {
	chomp($line);                           # get rid of \n
	$num_recs++;

	# process data records
	# get individual fields
	@fields = split(/\|/, $line, -1);

	$idfrom = $fields[$idxfrom];
	$idto   = $fields[$idxto];

	if ($idfrom eq $idto) { next GETRECORD; }        # skip if from ID equals to ID

	if (exists($records{ $idfrom })) {
		if ($line eq $records{ $idfrom }) { next GETRECORD; }     # new record matches stored record - skip it
		print "ERROR: change from: $idfrom record mismatch - new record: $line\n";
	}
	else {
		# idfrom is new - save record
		$records{ $idfrom } = $line;
	}

}

# now at end of file - write out the records in ID order to the new file
foreach $idfrom (sort keys %records) {
	$outrec = $records{ $idfrom };
	print OUTFILE "$outrec\n";
	$num_outs++;
}

$_[3] = $num_outs;                 # set number of output records

# close files
close INFILE;
close OUTFILE;

1;
}
