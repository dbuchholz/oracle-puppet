package AppendProcess;

=head1  AppendProcess.pm

=head2  ABSTRACT:
		Provide file append service - current routines are:
		AppendFile           - Appends a file to another file

=head2  REVISIONS:
		RCR 15-JAN-08 13354-0  RCR Added CommaDlm
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 16-NOV-04 13354-0  RCR Changed prototype and comments to reflect 4 arguments
		RAM 09-AUG-04 13354-0  RCR Removed sort of resulting file
		RAM 20-JUN-04 13354-0  JGF Initial development

=cut

use strict;

use File::Copy;
use ITSCheck;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(AppendFile CommaDlm);

sub AppendFile($$$$) {

=head1 sub AppendFile($$$$) 

=head2 ABSTRACT:
	   Appends a file to another file

=head2 ARGUMENTS:
	   0: Name of the file to append 
	   1: Name of the file to be appended to
	   2: Number of output headers
	   3: Number of records in the output file

=cut

	# SCALARS
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $num_outs	 = 0;      # records in output file
	my $num_recs	 = 0;      # records in input file
	my $out_hdr;               # number of headers in output file
	my $out_file_name;         # name of output file
	my $status;                # the status of the file opens

	$out_hdr = $_[2];
	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, ">> $out_file_name");           # open output file with append
	if (!$status) { return 0; } 

	# get rid of the header record if necessary
	while ($num_recs < $out_hdr) {
		$line = <INFILE>;
		$num_recs++;
	}

	# loop reading a record and writing it to the RTG file
	while ($line = <INFILE>) {
		$num_recs++;
		print OUTFILE "$line";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}

sub CommaDlm($$$$) {

=head1 sub CommaDlm($$$$) 

=head2 ABSTRACT:
	   Changes VAULT standard dlm of pipe to comma

=head2 ARGUMENTS:
	   0: Name of the file to append 
	   1: Name of the file to be appended to
	   2: Number of output headers
	   3: Number of records in the output file

=cut

	# SCALARS
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $num_outs	 = 0;      # records in output file
	my $num_recs	 = 0;      # records in input file
	my $out_file_name;         # name of output file
	my $status;                # the status of the file opens

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name"); 
	if (!$status) { return 0; } 

	# loop reading a record and writing it to the output file
	while ($line = <INFILE>) {
		$num_recs++;
        $line =~ s/\|/,/g;
		print OUTFILE "$line";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}
