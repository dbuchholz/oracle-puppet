package ExrateProcessRaw;

=head1  ExrateProcessRaw.pm

=head2  ABSTRACT:
		Get currency definitions and enable processing of Exshare values - current routines are:
		ExpandExrateRaw       - Expands a file containing a list of currency cross rates into 
								another file containing the full matrix of cross rates

=head2  REVISIONS:
		RCR 08-NOV-07 13354-0  RCR Initial development - offshoot of ExrateProcess

=cut

use strict;
use ITSCheck;
use ITSTrim;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ExpandExrateRaw);

sub ExpandExrateRaw($$$$) {

=head1 sub ExpandExrateRaw($$$$) 

=head2 ABSTRACT:
	   Expands a list of cross rates in a file to a fully populated
	   matrix of all possible cross rates

=head2 ARGUMENTS:
	   0: Name of the input currencies cross rates file
	   1: Name of the output cross rates matrix
	   2: Number of headers in the output file
	   3: Number of records in the output file

=cut

# SCALARS
my $base;                  # code for intermediate base currency
my $currfrom;              # from currency
my $currto;                # to currency
my $effective_date;        # the effective date for the file
my $exrate;                # the calculated cross exchange rate
my $idxfld;                # index into fields on record
my $idxfrom = -1;          # index of from currency
my $idxto   = -1;          # index of to currency
my $idxrate = -1;          # index of exrate
my $idxdate = -1;          # index of date
my $idxsou  = -1;          # index of source code 
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $num_outs	 = 0;      # records in output file
my $num_recs	 = 0;      # records in input file
my $out_file_name;         # name of output file
my $outrec;                # constructed output record
my $source_code;           # the source code for the file
my $status;                # the status of the file opens
my $value;                 # temporary value
my $valuefrom;             # temporary value
my $valueto;               # temporary value

# ARRAYS
my @fields;                # values of fields
my @names;                 # names of fields

# HASHES
my %currlist;              # list of all currency codes
my %matrix;                # matrix of all cross rates

$_[3] = 0;                        # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
$status = open(INFILE, "< $inp_file_name");
if (!$status) { return 0; } 

$out_file_name = $_[1];
$status = open(OUTFILE, "> $out_file_name");
if (!$status) { return 0; } 
print OUTFILE "            security_id|source_code|effective_date|from_type|from_curr|to_type|to_curr|exrate\n";
$num_outs++;

# loop reading a record
GETRECORD: while ($line = <INFILE>) {
	chomp($line);                           # get rid of \n
	$num_recs++;

	# split the record into fields
	if ($num_recs == 1) {
		# for first record, get the names and find "from_curr" and "to_curr"
		@names  = split(/\|/, $line, -1);
		TrimString($names[0]);
		for $idxfld (0 .. $#names) {
			if ($names[$idxfld] eq "from_curr")      { $idxfrom = $idxfld }
			if ($names[$idxfld] eq "to_curr")        { $idxto   = $idxfld }
			if ($names[$idxfld] eq "exrate")         { $idxrate = $idxfld }
			if ($names[$idxfld] eq "effective_date") { $idxdate = $idxfld }
			if ($names[$idxfld] eq "source_code")    { $idxsou  = $idxfld }
		}
		# if any of the fields are not found, the expansion can not be done
		if ($idxfrom < 0 || $idxto < 0)                  { return 0; }
		if ($idxrate < 0 || $idxdate < 0 || $idxsou < 0) { return 0; }
	}
	else {
		# process data records
		# get individual fields
		@fields = split(/\|/, $line, -1);
		$currfrom = $fields[$idxfrom];
		$currto   = $fields[$idxto];
		$exrate   = $fields[$idxrate];

		# skip record if rate not a number or le 0
		if (!CheckNumber($exrate)) { next GETRECORD; }
		if ($exrate <= 0.0)        { next GETRECORD; }

		# save date and source code from second record
		if ($num_recs == 2) {
			$effective_date = $fields[$idxdate];
			$source_code    = $fields[$idxsou];
		}

		# add both currencies to currency list if they don't exist
		if (!exists($currlist{ $currfrom })) { $currlist{ $currfrom } = 1; }
		if (!exists($currlist{ $currto   })) { $currlist{ $currto   } = 1; }

		# add rate to matrix
		$matrix{ $currfrom } { $currto } = $exrate;

		# calculate inverse here, as we only have rates relative to one base currency coming in
		if (!exists($matrix{ $currto } { $currfrom })) {
			# inverse does not exist
			$value = 1.0 / $matrix{ $currfrom } { $currto };
			$matrix{ $currto } { $currfrom } = $value;
		}
	}
}

# now at end of file - generate the full matrix, writing out as we do so

foreach $currfrom (sort keys %currlist) {

	GETCURRTO: foreach $currto (sort keys %currlist) {

		$value = "";
		$exrate = "";
		if    ($currfrom eq $currto) {
			# if currencies are the same, rate is 1.0
			$exrate = 1.0;
		}
		elsif (exists($matrix{ $currfrom } { $currto })) {
			# rate exists
			$exrate = $matrix{ $currfrom } { $currto };
		}
		else {
			if (exists($matrix{ $currto } { $currfrom })) {
				# inverse exists
				$exrate = 1.0 / $matrix{ $currto } { $currfrom };
			}
			else {
				# try to calculate the rate using an intermediate base currency if the cross rates exist
				foreach $base ("USD","GBP","EUR") {
					if (exists($matrix{ $currfrom } { $base }) && exists($matrix{ $currto } { $base })) { 
						# can calculate through a base currency
						$valuefrom = $matrix{ $currfrom } { $base };
						$valueto   = $matrix{ $currto   } { $base };
						$exrate = $valuefrom / $valueto;
						last;
					}
					if (exists($matrix{ $base } { $currfrom }) && exists($matrix{ $base } { $currto })) { 
						# can calculate through a base currency
						$valuefrom = $matrix{ $base } { $currfrom };
						$valueto   = $matrix{ $base } { $currto };
						$exrate = $valueto / $valuefrom;
						last;
					}
				}
			}

		}
		if ($exrate eq "") {
			# no common cross existed - can't calculate the rate
			next GETCURRTO;
		}

		# check the number before writing out (this will squish out unwanted trailing zeros also)
		if (CheckNumbany($exrate)) {
			# write out the new record, adding CASH to the front of the ISO codes
			$outrec = join("|", "CASH$currfrom", $source_code, $effective_date, 
						   "CASH", "CASH$currfrom", "CASH", "CASH$currto", $exrate);
			print OUTFILE "$outrec\n";
			$num_outs++;
		}
	}
}

$_[3] = $num_outs;                 # set number of output records

# close files
close INFILE;
close OUTFILE;

1;
}
