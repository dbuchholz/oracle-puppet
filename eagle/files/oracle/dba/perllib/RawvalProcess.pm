package RawvalProcess;

=head1  RawvalProcess.pm

=head2  ABSTRACT:
		Setup for and get raw values from a record - current routines are:
		GetRawval         - gets a raw data value from the record
		InitRawval        - initializes a record for GetRawval
		GetRawvalMode     - gets the mode of the record
		SetRawvalMode     - sets the mode of the record
		ParseDouble       - Parses an input line using an input delimiter and double quotes delimiter

=head2  REVISIONS:
		RCR 24-OCT-07 13354-0  RCR Added new modes of cdfd and pdfd
		RCR 21-MAR-07 13354-0  RCR Added ParseDouble
		RAM 01-FEB-06 13354-0  RCR Added new modes of tdf and tdfq
		RAM 26-JAN-06 13354-0  RCR Changed InitRawval to return 0 if no fields after split
		RAM 30-AUG-05 13354-0  RCR Changed quotewords to parse_line
		RAM 24-FEB-05 13354-0  RCR Use input length of field for delimited files to deliver the rawval
		RAM 14-FEB-05 13354-0  RCR Added return of length of field for delimited files
		RAM 13-JAN-04 13354-0  RCR Added colon delimited files
		RAM 06-AUG-04 13354-0  RCR Added check for undefined field in "df" type processing - cleaned up comments
		RAM 26-FEB-04 13354-0  JGF Added extra check for tag and value definition
		RAM 21-OCT-03 13354-0  JGF Add handling of Tag/Value pairs
		RAM 02-APR-03 13354-0  JGF Initial development

=cut

use strict;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(GetRawval InitRawval GetRawvalMode SetRawvalMode ParseDouble);

# variables that must be retained across the routines
my $lenrec;               # length of record
my $mode;                 # the mode set for all calls
my $record;               # the data record

my @fields;               # array of data fields
my %fields_tag_value;	  # Hash of Tag/Value pairs

sub GetRawval ($$) {

=head1  sub GetRawval ($$)

=head2  ABSTRACT:
        gets a raw data value from a previously initialized record
		returns null if the value requested is not on the record
        
=head2  ARGUMENTS:
        0: address or field number of data
        1: length of data or 0 for delimited files

		returns: raw data value

=cut

	use strict;

	my $address = $_[0];
	my $length  = $_[1];
	my $value;                 # temporary storage for data value

	if    ($mode eq "add") {
		# mode is by address
		if ($address < 0) { return ""; }                    # bad address - return null
		if ($length  < 1) { return ""; }                    # bad length  - return null
		if ($address > $lenrec) { return ""; }              # start address beyond end of record - return null
		if (($address + $length - 1) > $lenrec) {
			$value = substr($record, $address);             # end of field is beyond end of record - return to end of record
		}
		else {
			$value = substr($record, $address, $length);    # normal field
		}
		return $value;
	}
	elsif (substr($mode, 1, 2) eq "df") {
		if ($address < 0)                { return ""; }              # bad address - return null
		if ($address > $#fields)         { return ""; }              # beyond end of fields - return null
		if (!defined($fields[$address])) { return ""; }              # necessary because parse_line sometimes returns an undef
		if ($length == 0) {
			$value = $fields[$address];
		}
		else {
			$value = substr($fields[$address], 0, $length);
		}
		$_[1] = length($value);
		return $value;
	}
	elsif (substr($mode, 1, 2) eq "tv") {
		if (!exists($fields_tag_value{$address}) ) { return ""; }    # tag not found - return null
		if ($length == 0) {
			$value = $fields_tag_value{$address};
		}
		else {
			$value = substr($fields_tag_value{$address}, 0, $length);
		}
		$_[1] = length($value);
		return $value;
	}
	else {
		# mode is unknown - return null
		return "";
	}
	1;
}

sub InitRawval ($) {

=head1  sub InitRawval ($)

=head2  ABSTRACT:
		Initializes record for GetRawval calls
        
=head2  ARGUMENTS:
		0: record from feed

=cut

	use strict;
	use Text::ParseWords;

	my $tag_val;				# Tag/Value pair
	my $tag;					# Tag
	my $value;					# Value
	my $tagbase;				# Base Tage name for multi value tags
	my $numvalue;				# Number of values for a multi value tag
	my $lenval;					# Length of value
	my $indx;					# Index counter
	
	@fields=();                 # reinitialize to detect failures
	$record = $_[0];
	$lenrec = length($record);
	if (substr($mode, 1, 2) eq "df") {
		if    ($mode eq "cdf") {
			@fields = split(/,/,  $record, -1);       # split comma delimited file
		}
		elsif ($mode eq "cdfd") {
			@fields = ParseDouble(',', 0, $record);   # split comma delimited file with double quotes
		}
		elsif ($mode eq "cdfq") {
			@fields = parse_line(',', 0, $record);    # split comma delimited file with embedded quotes
		}
		elsif ($mode eq "ndf") {
			@fields = split(/:/,  $record, -1);       # split colon delimited file
		}
		elsif ($mode eq "ndfq") {
			@fields = parse_line(':', 0, $record);    # split colon delimited file with embedded quotes
		}
		elsif ($mode eq "pdf") {
			@fields = split(/\|/, $record, -1);       # split pipe delimited file
		}
		elsif ($mode eq "pdfd") {
			@fields = ParseDouble('\|', 0, $record);  # split pipe delimited file with double quotes
		}
		elsif ($mode eq "pdfq") {
			@fields = parse_line('\|', 0, $record);   # split pipe delimited file with embedded quotes
		}
		elsif ($mode eq "tdf") {
			@fields = split(/~/,  $record, -1);       # split tilde delimited file
		}
		elsif ($mode eq "tdfq") {
			@fields = parse_line('~', 0, $record);    # split tilde delimited file with embedded quotes
		}
	}
	elsif ($mode eq "ctvq") {
		%fields_tag_value = ();                  # must reinitialize hash to nothing before processing
		@fields = parse_line(',', 0, $record);   # split comma delimited file with embedded quotes
		my $num_fields=@fields;
		$indx=0;
		while ($indx < $num_fields) {
			$tag_val = $fields[$indx];
			($tag, $value) = split('=', $tag_val);
			$indx++;
			if (defined($tag) && defined($value)) {
				if (substr ($value, 0, 1) eq '(') {
					# value is list surrounded by parens - must parse out list
					$value = substr ($value, 1);
					$tagbase = $tag;
					$numvalue = 1;
					$lenval = length($value);
					while (substr ($value, $lenval-1, 1) ne ')') {
						$tag = $tagbase . $numvalue;
						$fields_tag_value{$tag} = $value;
						$numvalue++;
						$value = $fields[$indx];
						$lenval = length($value);
						$indx++;
					}
					$tag = $tagbase . $numvalue;
					$fields_tag_value{$tag} = substr ($value, 0, length($value)-1);
				}
				else {
					# value is normal value
					$fields_tag_value{$tag} = $value;
				}
			}
		}
	}
	elsif ($mode eq "ntvq") {
		%fields_tag_value = ();                  # must reinitialize hash to nothing before processing
		@fields = parse_line(':', 0, $record);   # split colon delimited file with embedded quotes

		# process fields in pairs - first field is tag, second is value
		for ($indx = 0; $indx <= $#fields; $indx = $indx + 2) {
			$fields_tag_value{$fields[$indx]} = $fields[$indx+1];
		}
	}
	if ($mode ne "add" && $#fields < 0) { return 0; }              # return error if no fields could be found
	1;
}

sub GetRawvalMode ($) {

=head1  sub GetRawvalMode ($)

=head2  ABSTRACT:
		Gets the record mode
        
=head2  ARGUMENTS:
		0: Mode type - returned - "add", "cdf", "cdfq", "ndf", "ndfq", "pdf", "pdfq", "tdf", "tdfq", "ctvq", or "ntvq" 

=cut

	$_[0] = $mode;
	1;
}

sub SetRawvalMode ($) {

=head1  sub SetRawvalMode ($)

=head2  ABSTRACT:
		Sets the mode for subsequent InitRawval and GetRawval calls
        
=head2  ARGUMENTS:
		0: Mode type - must be: "add", "cdf", "cdfd", "cdfq", "ndf", "ndfq", "pdf", "pdfd", "pdfq", "tdf", "tdfq", "ctvq", "ntvq" 
			- defaults to "add" if bad value specified

=cut

	$mode = $_[0];
	if ($mode ne "add" && $mode ne "cdf"  && $mode ne "cdfd" && $mode ne "cdfq" && $mode ne "ndf"  && $mode ne "ndfq" && 
		$mode ne "pdf" && $mode ne "pdfd" && $mode ne "pdfq" && $mode ne "tdf"  && $mode ne "tdfq" &&
		$mode ne "ctvq" && $mode ne "ntvq") {
		$mode = "add";
	}
	1;
}

sub ParseDouble ($$$) {

=head1  sub ParseDouble ($$$)

=head2  ABSTRACT:
		Parses an input line using an input delimiter and double quotes delimiter
		Initializes record for GetRawval calls
        
=head2  ARGUMENTS:
		0: delimiter to user for parsing
        1: keep flag - true to keep quotes in returned tokens
        2: input line

=cut

	use strict;

	my $delim;
	my $flg_inquo = 0;       # set flag to not be in quoted string
	my $keep;
	my $line;
	my $quostring;           # quoted string value containing several tokens
	my $token;               # token from split line

	my @outtoks = ();        # initialize to nothing in case of failure
	my @rawtoks = ();        # initialize to nothing in case of failure

	($delim, $keep, $line) = @_;

	@rawtoks = split(/$delim/, $line);

	foreach $token (@rawtoks) {
		if (!$flg_inquo) {
			if (substr($token, 0, 1) eq '"') {
				# start quoted string token
				$quostring = $token;
				$flg_inquo = 1;
			}
			else {
				# normal token - push onto array
				push(@outtoks, $token);
			}
		}
		else {    # in quoted string
			# put delimiter back in to quoted string between tokens
			$quostring .= $delim . $token;

			if (substr($token, -1) eq '"') {
				# end of quoted string - check keep and push to output array
				if (!$keep) {
					# keep is off - remove quotes at beginning and end
					substr($quostring, 0, 1) = "";
					substr($quostring, -1)   = "";
				}
				push(@outtoks, $quostring);
				$flg_inquo = 0;
			}
		}
	}

	# finished - return output tokens
	return @outtoks;
}

1;
