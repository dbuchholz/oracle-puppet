package CustomEdit;

=head1  CustomEdit.pm

=head2  ABSTRACT:
            Provide standard edit routines - Current edits are:
            Edhdrval    - Performs custom edit for header record field in file and retains value for subsequent records
            Edfxfirst   - Performs custom edit for first FX field in file and retains value for subsequent records
            EdFMCID     - Performs custom edits on the FMC ID field
            EdModtrn    - Creates Modtrannum field from Trannum Field
            Edmeldate   - Checks an end date and a posted date for equality
            Edmelid     - Checks and creates a Mellon ID
            EdBlknum    - Create Block number field from Trannum Field
            Edpdate     - Returns day before current system date
            Edmbegsl    - Returns the month begin day for a date in mm/dd/yyyy format
            Edmbegdt    - Returns the month begin day for a date in yyyymmdd format
            Ednetgr     - Returns the gross dividend amount if available, otherwise returns the net
            Edhgvtrd    - Checks the SUB TXN CODE and TXN CODE for record suppression
			Edsetid     - Sets the initial security ID field in the record to the contents of security_id and then resorts the file
			Edvadsfm    - Interprets a value in VADS format
			Edbambat    - Interprets and joins several fields to form a batch_id
			Edffobat    - Interprets and joins several fields to form a batch_id

=head2  REVISIONS:
            RCR 20-AUG-08 13354-0  RCR Added Edffobat
            RCR 16-JUL-08 13354-0  RCR Added Edhdrval
            RCR 13-JUN-08 13354-0  RCR Added Edbambat
            RCR 25-APR-07 13354-0  RCR Added Edvadsfm
            RCR 24-APR-07 13354-0  RCR Added Edsetid
            RCR 03-JAN-07 13354-0  RCR Added Edhgvtrd
            RCR 06-DEC-06 13354-0  RCR Added Ednetgr
            VRC 09-AUG-06 13354-0  VRC Added Edmbegdt
            RCR 09-AUG-06 13354-0  RCR Added Edmbegsl
            RCR 25-NOV-05 13354-0  KDA Added Edpdate
            RAM 14-JUN-05 13354-0  MAB Corrected strays
            AKG 21-MAR-05 13354-0  MAB Added Edmeldate
            AKG 15-MAR-05 13354-0  MAB Added Edmelid
            MAB 22-FEB-05 13354-0  MAB Added EdRmcurr
            RAM 17-FEB-05 13354-0  MAB Added EdModtrn, EdBlknum
            RAM 14-FEB-05 13354-0  MAB Added Edfxfirst
            RAM 09-FEB-05 13354-0  MAB Initial development

=cut

use strict;
use ITSCalc;
use ITSCheck;
use ITSDate;
use ITSTrim;
use TranslateTable;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edhdrval Edfxfirst EdFMCID EdModtrn Edmeldate Edmelid EdBlknum EdRmcurr Edpdate Edmbegsl Edmbegdt Ednetgr 
			Edhgvtrd Edsetid Edvadsfm Edbambat Edffobat);

my $retarg;        # retain argument for fxfirst
my $rethdr;        # retain argument for Edhdrval

BEGIN {
      $retarg = "";
      $rethdr = "";
}

sub Edhdrval ($) {

      # takes an argument.  On first record, retains it and returns it.  On subsequent records, returns the same thing.
      if ($rethdr eq "") {
            $rethdr = $_[0];
            TrimString($rethdr);
      }
      $_[0] = $rethdr;
      1;
}

sub Edfxfirst ($) {

      # takes an argument.  On first record, returns CASHargument.  On subsequent records, returns the same thing.
      if ($retarg eq "") {
            $retarg = $_[0];
            TrimString($retarg);
            $retarg = "CASH" . $retarg;
      }
      $_[0] = $retarg;
      1;
}

sub EdFMCID ($) {

      # takes an argument that is a string and checks to see if it is a CUSIP or SEDOL and performs
      # custom edits on it based on that and the codes that follow it

      my $secid = $_[0];
      TrimString($secid);

      my $lenid = length($secid);
      if ($lenid == 10) {
            my $lasttwo = substr($secid, 8, 2);
            if ($lasttwo eq "WO" || $lasttwo eq "FO" ||
                $lasttwo eq "UJ" || $lasttwo eq "WW") {
                  $secid = substr($secid, 0, 8);
                  ComputeCheckDigit($secid);
            }
            elsif (substr($secid, 6, 4) eq "   X") {
                  $secid = substr($secid, 0, 6);
                  ComputeCheckDigit($secid);
            }
      }
      $_[0] = $secid;

      1;
}


sub EdModtrn ($) {

      # takes an argument that is a string and returns a portion of it as MODTRAN field

      my $field = $_[0];
      TrimString($field);
    my $index;

    if (substr ($field,0,2) eq "TC" ) {
            $field = substr($field,2);
      }

      $index = index ($field, ":");
      if ($index ne "-1") {
            $_[0] = substr ($field, 0, $index);
      }
      else {
            $_[0] = $field;
      }

      1;
}

sub Edmeldate ($) {

      # takes an argument that contains fields 2 and 22, checking them for equality

      my ($date2, $date22) = split(",", $_[0]);
      TrimString($date2);
      TrimString($date22);

      $_[0] = "";           # set output argument to null in case there are any failures

      # check dates for validity
      if (!CheckDate($date2 )) { return 0; }
      if (!CheckDate($date22)) { return 0; }

      # check for equality
      if ($date2 != $date22) { return 0; }

      # everything ok - return date in argument
      $_[0] = $date22;
      1;
}

sub Edmelid ($) {

      # takes an argument that contains fields 4, 16, 31, 52 and returns a Mellon ID

      my ($secid, $category, $pay_curr, $local_curr) = split(",", $_[0]);
      TrimString($secid);
      if ($secid eq "") {
            if ($category eq "000") {
                  $secid = "CASH" . $pay_curr;
            }
            else {
                  $secid = "CASH" . $local_curr;
            }
      }

      $_[0] = $secid;
      1;
}

sub EdBlknum ($) {

      # takes an argument that is a string and returns a portion of it as BLOCKNUM field

      my $field = $_[0];
      TrimString($field);
    my $index;

    if (substr ($field,0,2) eq "TC" ) {
            $field = substr($field,2);
      }

      $index = index ($field, ":");
      if ($index ne "-1") {
            $_[0] = substr ($field, $index+1);
      }
      else {
            $_[0] = "";
      }

      1;
}

sub EdRmcurr ($) {

      # Takes an argument and removes the last 2 characters if they represent a currency code.
      #  Ex:  1.234     returns 1.234
      #       1.234US   returns 1.234

      my $field  = $_[0];
      TrimString($field);

      my $last = length($field);

      if (substr($field, -2) =~ /^[A-Za-z]+$/) {
            $_[0] =  substr($field, 0, $last - 2);
      }
      else
      {
            $_[0] = $field;
      }

      1;
}

sub Edpdate ($) {

      # Takes an argument and returns the date one day before the current system date

      my ($day, $month, $year) = (localtime)[3 .. 5];
      my $current = sprintf("%04d%02d%02d", $year + 1900, $month + 1, $day);
      CalcDate8X2T7($current, -1, $_[0]);

      1;
}

sub Edmbegsl ($) {

      # Takes an argument in mm/dd/yyyy format and returns the month begin date

      my ($month, undef, $year) = split("/", $_[0]);
      $_[0] = $year . $month . "01";

      1;
}

sub Edmbegdt ($) {

      # Takes an argument in yyyymmdd format and returns the month begin date
      my $field  = $_[0];
      TrimString($field);

      my ($month)= substr($field,4,2);
      my ($year) = substr($field,0,4);
      $_[0] = $year . $month . "01";

      1;
}

sub Ednetgr ($) {

	# takes an argument that contains a net and gross field
	# Ednetgr     - Returns the gross dividend amount if available, otherwise returns the net

	my $amount;
	my ($net, $gross) = split(",", $_[0]);

	if ($gross eq "99900000000") {
		# gross not available, return net
		$amount = $net;
	}
	else {
		# gross available, return gross
		$amount = $gross;
	}

	# if amount is the EXSHARE code for nothing, set to null
	if ($amount eq "99900000000") { $amount = ""; }

	# return value
	$_[0] = $amount;
	1;
}

sub Edhgvtrd ($) {

	# takes an argument that contains a sub code and code and 
	# returns null on certain combinations so the record will be suppressed

	my ($subcode, $code) = split(",", $_[0]);

	# default is to return the subcode
	$_[0] = $subcode;

	if    ($subcode eq "IBC" || $subcode eq "ISC" || $subcode eq "SPC") {
		$_[0] = "";
	}
	elsif ($subcode eq "FCC" && $code eq "FX ") {
		$_[0] = "";
	}
	elsif ($subcode eq "IB " || $subcode eq "IS ") {
		$_[0] = "";
	}

	1;
}



sub Edsetid($$$$) {

=head1 sub Edsetid($$$$) 

=head2 ABSTRACT:
	   Sets the initial security ID field in the record to the contents of security_id
       Contents will then be automatically resorted by vaultgp

=head2 ARGUMENTS:
	   0: Name of the input file
	   1: Name of the output file
	   2: Number of headers in the output file
	   3: Number of records in the output file

=cut

	# SCALARS
	my $idxfld;                # index into fields on record
	my $idxsec  = -1;          # index of security ID
	my $idxtyp  = -1;          # index of id_type
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $num_outs	 = 0;      # records in output file
	my $num_recs	 = 0;      # records in input file
	my $out_file_name;         # name of output file
	my $outrec;                # constructed output record
	my $status;                # the status of the file opens

	# ARRAYS
	my @fields;                # values of fields
	my @names;                 # names of fields

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and writing it out
	GETRECORD: while ($line = <INFILE>) {
		chomp($line);                           # get rid of \n
		$num_recs++;

		# split the record into fields
		if ($num_recs == 1) {
			print OUTFILE "$line\n";                       # write out header
			# for first record, find the security_id
			@names  = split(/\|/, $line, -1);
			TrimString($names[0]);
			for $idxfld (0 .. $#names) {
				if ($names[$idxfld] eq "security_id")    { $idxsec = $idxfld; }
				if ($names[$idxfld] eq "id_type")        { $idxtyp = $idxfld; }
			}
			# if fields are not found, the expansion can not be done
			if ($idxsec < 0 || $idxtyp < 0) { return 0; }
		}
		else {
			# process data records
			# get individual fields
			@fields = split(/\|/, $line, -1);
			$fields[0] = $fields[$idxsec];      # replace initial security ID with updated security ID
			$fields[$idxtyp] = length($fields[0]) >= 8 ? "CUSIP" : "SEDOL";

			# write out the new record with updated ID
			$outrec = join("|", @fields);
			print OUTFILE "$outrec\n";
			$num_outs++;
		}
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}

sub Edvadsfm ($) {

	# takes an argument that contains a VADS format code and value and returns the adjusted value

	my ($formcd, $value) = split(",", $_[0]);
	$_[0] = "";          # set initial return value to null so we can return out on errors

	# check length and format code for validity
	my $length = length($value);
	if ($length < 3 || $length > 15) { return 0; }

	if ($formcd eq "Z") { return 0; }
	my $numdp = index('0123456789ABCDEF', $formcd);
	if ($numdp < 0) { return 0; }

	# have value and number of decimal places - construct number and validate

	my $adjval = substr($value, 0, $length - $numdp) . "." . substr($value, $length - $numdp, $numdp);
	my $status = CheckNumber($adjval);
	if (!$status) { $adjval = ""; }

	$_[0] = $adjval;
	1;
}

sub Edbambat ($) {

	# takes an argument that contains a batch date and an entity ID and forms a batch ID
	my ($batch_date, $ent_id) = split(",", $_[0]);
	$_[0] = "";          # set initial return value to null so we can return out on errors

	CalcDateEOM($batch_date);
	ProcessTranslate("TRBAMENT", $ent_id);
    $_[0] = $batch_date . $ent_id . "eglPrfPc-BAM Model";
	1;
}

sub Edffobat ($) {

	# takes an argument that contains a batch date and an entity ID and forms a batch ID
	my ($batch_date, $ent_id) = split(",", $_[0]);
	$_[0] = "";          # set initial return value to null so we can return out on errors

	$batch_date = substr($batch_date, 0, 4) . "-" . substr($batch_date, 4, 2) . "-" . substr($batch_date, 6, 2);
	ProcessTranslate("TRFFOENT", $ent_id);
    $_[0] = $batch_date . $ent_id . "gpsPrfPc-Total-Investment-Security Model";
	1;
}

1;
