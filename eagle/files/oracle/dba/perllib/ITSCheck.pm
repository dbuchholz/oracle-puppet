package ITSCheck;

=head1  ITSCheck.pm

=head2  ABSTRACT:
		Provide standard data check routines - current routines are:
		CheckChar        - Checks a character field
		CheckCharMin     - Checks a character field, filling it to a minimum length
		CheckDirectory   - Checks a directory string and returns the environment name
		CheckFileGroup   - Checks a file name and determines the group it is in
		CheckInteger     - Checks an integer field
		CheckNumbany     - Checks a number field of any format
		CheckNumber      - Checks a number field
		CheckOpnumber    - Checks an overpunched number field
		CheckTable       - Checks a table to see if it was compiled after modification
		CheckTime        - Checks a time field

=head2  REVISIONS:
		RCR 16-FEB-08 13354-0  RCR Changed to further enhance processing of exponentials in CheckNumbany
		RCR 09-NOV-07 13354-0  RCR Changed to correct processing of exponentials in CheckNumbany
		RAM 26-FEB-06 13354-0  RCR Changed CheckDirectory to check for /app
		RAM 08-DEC-05 13354-0  RCR Changed CheckNumbany to allow commas in the number
		RAM 06-JUL-04 13354-0  RCR Changed CheckDirectory to check for /apps
		RAM 02-JUN-04 13354-0  RCR Changed CheckDirectory to check for /export/home(n) as well as /export/home
		RAM 11-MAY-04 13354-0  RCR Added CheckNumbany
		RAM 01-APR-04 13354-0  RCR Added CheckTable
		RAM 20-JAN-03 13354-0  RAM Added pass through of "its_null"
		RAM 10-MAY-02 13354-0  JGF Initial development

=cut

use strict;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CheckChar CheckCharMin CheckDirectory CheckFileGroup CheckInteger CheckNumbany CheckNumber CheckOpnumber 
			 CheckTable CheckTime);

my $digits;          # digits string
my $zeroes;          # zeroes string

INIT {
	$digits = "0123456789";
	$zeroes = "0000000000000000000000000000000000000000000000";
}

sub CheckChar ($) {
	# this routine is designed to check a character string if more stringent checks are developed

	TrimString($_[0]);
	1;                # always return true - no errors possible
}

sub CheckCharMin ($$) {
	# this routine checks a character string and fills it to a minimum length with blanks
	my ($string, $minimum) = @_;
	my $lenstr = length($string);
	if ($lenstr lt $minimum) {
		substr($_[0], $lenstr) = substr("                    ", 0, $minimum - $lenstr);
	}
	1;                # always return true - no errors possible
}

sub CheckDirectory ($$) {

	# takes an argument that is a string containing a directory specification    
	# parses it and returns the environment name, defined as:
	# 1: the contents of ENV variable CLIENT, or
	# 2: the subdirectory after "/eaglesystems" or "/export/home" or "/apps" or "/app"

	# if the directory is valid, 1 is returned - otherwise, 0 is returned

	my $client;             # client name
	my $idxdir;             # index into subdirectory array
	my $string;             # input string
	my @subdir;             # parsed subdirectory array

	$_[1] = "unknown";         # set up to return "unknown" as the default environment
	
	# get CLIENT ENV variable value if available
	if (exists($ENV{ CLIENT })) {
	   $client = $ENV{ CLIENT };
	}
	else {
		$client = "";
	}

	if ($client eq "") {    # client not found, get from directory
		$string = $_[0];
		TrimString($string);

		# if string has no length, just return 0
		if ($string eq "") { return 0; } 
		@subdir = split("/", $string);

		for ($idxdir = 0; $idxdir <= $#subdir; $idxdir++) {
			if ($subdir[$idxdir] eq "eaglesystems") { last; }
		}

		if ($idxdir > $#subdir) {                  # eaglesystems not found - check for /export/home
			for ($idxdir = 0; $idxdir <= $#subdir; $idxdir++) {
				if ($subdir[$idxdir] eq "export") { 
					if ($idxdir < $#subdir && substr($subdir[$idxdir + 1], 0, 4) eq "home") { 
						$idxdir++;
						last;
					}
				}
			}
		}

		if ($idxdir > $#subdir) {                  # eaglesystems not found - check for /apps
			for ($idxdir = 0; $idxdir <= $#subdir; $idxdir++) {
				if ($subdir[$idxdir] eq "apps") { last; }
			}
		}

		if ($idxdir > $#subdir) {                  # eaglesystems not found - check for /app
			for ($idxdir = 0; $idxdir <= $#subdir; $idxdir++) {
				if ($subdir[$idxdir] eq "app") { last; }
			}
		}

		if ($idxdir >= $#subdir) { return 0; }     # could not find either one or found at end - return 0 and "unknown"
		$client = $subdir[$idxdir + 1];

	}
	$_[1] = $client;
	1;                   # return true - valid subdir is being returned
}

sub CheckFileGroup ($$) {

	# takes an argument that is a string containing a file specification    
	# parses it and returns the group designation, defined as:
	# 1: INP for input if no group present, or
	# 2: the three character file group (SMF, PRI, etc.)
	# 3: rejected

	# assumes filename is of form: loader_date[_time][_group].dat

	# if the filename and group are valid, 1 is returned - otherwise, 0 is returned

	my $filename;           # filename to be parsed
	my $locdot;             # location of dot in name
	my $locsl;              # location of slash in name

	my @names;              # array of names from filename

	$filename = $_[0];
	$_[1] = "";             # set group to null in case something goes wrong along the way

	# remove leading directory designation from name
	$locsl = index($filename, "/");
	if ($locsl >= 0) {	
		substr($filename, 0, $locsl + 1) = "";
	}

	# remove suffix from name
	$locdot = rindex($filename, ".");
	if ($locdot >= 0) {
		substr($filename, $locdot) = "";
	}

	# split filename on underscores
	@names = split("_", $filename);
	if ($#names < 1) { return 0; }      # not enough entries in name
	# remove first two entries
	shift(@names);
	shift(@names);
	# if any left, is the first a time? - if so, remove it
	if ($#names >= 0) {
		if (CheckTime($names[0])) {
			shift(@names);
		}
	}

	# now, if any left?  return accordingly
	if    ($#names > 0) { return 0; }              # too many 
	elsif ($#names < 0) { $_[1] = "INP"; }         # nothing left - must be input file
	else                { $_[1] = $names[0]; }     # one left - must be group

	1;
}

sub CheckInteger ($) {

	# takes an argument that is a string containing an integer
	# returns the argument that is the number with leading spaces and zeros trimmed off
	# if the number is valid, 1 is returned - otherwise, 0 is returned

	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has no length, just return it - otherwise, process it
	if ($out ne "") { 
		# remove + sign if first character
		$out =~ s/^\+//;
		
		# remove - sign if first character and set flag (true if - present)
		my $flg_neg = ($out =~ s/^\-//);
	
		# if bad character found, return ""
		if ($out =~ m/[^0-9]/) { return 0; }

		else {      # have a number in the string - process it
			# trim leading zeros
			$out =~ s/^0+//;

			# done with number - check for special cases
			# nothing left, return "0"
			if ($out eq "") {
				$out = "0";
			}

			# if we had negative sign - restore it
			elsif ($flg_neg) {
				$out = join("", "-", $out);
			}
		}
	}

	$_[0] = $out;
	1;                # return true - valid number is being returned
}

sub CheckNumbany ($) {

	# takes an argument that is a string containing a number of any format
	# returns the argument that is the number with leading and trailing spaces and zeros trimmed off
	# if the number is valid, 1 is returned - otherwise, 0 is returned

	my $exponent = "";           # exponent of scientific notation number
	my $endnum;                  # end of remaining number

	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has no length, just return it - otherwise, process it
	if ($out ne "") { 
		# remove + sign if first character
		$out =~ s/^\+//;
		
		# remove - sign if first character and set flag (true if - present)
		my $flg_neg = ($out =~ s/^\-//);

		$endnum = length($out) - 1;             # get remaining length for further testing
		# check for negative number specified by parenthesis
		if ($endnum >= 3) {
			if ((substr($out, 0, 1) eq "(") && (substr($out, $endnum, 1) eq ")")) {
				$flg_neg = 1;
				$out = substr($out, 1, $endnum - 1);
			}
		}

		$endnum = length($out) - 1;             # get remaining length for further testing
		# check for exponential number
		if ($endnum >= 4) {
			if (uc(substr($out, $endnum - 3, 1)) eq "E") {
				$exponent = substr($out, $endnum - 2, 3);
				if (!CheckNumber($exponent)) { return 0; }     # make sure exponent is valid and normalize it
				if ($exponent == 0) { $exponent = ""; }        # if exponent is 0, don't need to do anything, so null it out
				$out = substr($out, 0, $endnum - 3);
			}
		}

		# eliminate any commas
		$out =~ s/,//g;
	
		# if bad character found, return ""
		if ($out =~ m/[^0-9\.]/) { return 0; }

		else {      # have a number in the string - process it
			# trim leading zeros
			$out =~ s/^0+//;

			# if decimal point found, trim trailing zeros 
			if ($out =~ m/\./) {
				$out =~ s/0+$//;
			}

			# done with number - check for special cases
			# nothing or only decimal point left, return "0"
			if ($out eq "" || $out eq ".") {
				$out = "0";
			}

			# if exponential, apply exponent
			if ($exponent ne "") {
				# check for location of decimal point and adjust
				my $locdp = index($out, ".");
				if ($locdp < 0) { 
					$locdp = length($out);
				}
				else {
					substr($out, $locdp, 1) = "";        # get rid of dp
				}
				$locdp = $locdp + $exponent;         # position of new dp

				if ($locdp < 0) {
					substr($out, 0, 0) = substr($zeroes, 0, - $locdp);   # fill with zeros
					$locdp = 0;
				}
				elsif ($locdp > length($out)) {
					$out .= substr($zeroes, 0, $locdp - length($out));      # fill with zeros

				}

				substr($out, $locdp, 0) = ".";        # put dp back in
				$out =~ s/ /0/g;                      # must substitute 0 for blank in case dp has moved beyond number
			}

			# if we had negative sign - restore it
			if ($flg_neg) {
				substr($out, 0, 0) = "-";
			}
		}
	}

	$_[0] = $out;
	1;                # return true - valid number is being returned
}

sub CheckNumber ($) {

	# takes an argument that is a string containing a number    
	# returns the argument that is the number with leading and trailing spaces and zeros trimmed off
	# if the number is valid, 1 is returned - otherwise, 0 is returned

	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has no length, just return it - otherwise, process it
	if ($out ne "") { 
		# remove + sign if first character
		$out =~ s/^\+//;
		
		# remove - sign if first character and set flag (true if - present)
		my $flg_neg = ($out =~ s/^\-//);
	
		# if bad character found, return ""
		if ($out =~ m/[^0-9\.]/) { return 0; }

		else {      # have a number in the string - process it
			# trim leading zeros
			$out =~ s/^0+//;

			# if decimal point found, trim trailing zeros 
			if ($out =~ m/\./) {
				$out =~ s/0+$//;
			}

			# done with number - check for special cases
			# nothing or only decimal point left, return "0"
			if ($out eq "" || $out eq ".") {
				$out = "0";
			}

			# if we had negative sign - restore it
			elsif ($flg_neg) {
				substr($out, 0, 0) = "-";
			}
		}
	}

	$_[0] = $out;
	1;                # return true - valid number is being returned
}

sub CheckOpnumber ($) {

	# takes an argument that is a string containing an overpunched number    
	# returns the argument that is the number with leading and trailing spaces and zeros trimmed off
	# if the number is valid, 1 is returned - otherwise, 0 is returned

	my $flg_neg;                   # is number negative?
	my $idxlast;
	my $lastch;
	my $locval;                    # location of value in check overpunch string

	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has no length, just return it - otherwise, process it
	if ($out ne "") { 
		$idxlast = length($out) -1;
		$lastch  = substr($out, $idxlast, 1);

		# check last character for + sign
		$locval = index('{ABCDEFGHI', $lastch);
		if ($locval >= 0 ) { 
			substr($out, $idxlast, 1) = substr($digits, $locval, 1);
		}

		# check last character for - sign
		$flg_neg = 0;
		$locval = index('}JKLMNOPQR', $lastch);
		if ($locval >= 0 ) { 
			substr($out, $idxlast, 1) = substr($digits, $locval, 1);
			$flg_neg = 1;
		}
		
		# if bad character found, return ""
		if ($out =~ m/[^0-9\.]/) { return 0; }

		else {      # have a number in the string - process it
			# trim leading zeros
			$out =~ s/^0+//;

			# if decimal point found, trim trailing zeros 
			if ($out =~ m/\./) {
				$out =~ s/0+$//;
			}

			# done with number - check for special cases
			# nothing or only decimal point left, return "0"
			if ($out eq "" || $out eq ".") {
				$out = "0";
			}

			# if we had negative sign - restore it
			elsif ($flg_neg) {
				substr($out, 0, 0) = "-";
			}
		}
	}

	$_[0] = $out;
	1;                # return true - valid number is being returned
}

sub CheckTable ($) {

	# checks a table to see if it was compiled after modification
	#
	# ARGUMENTS:
	# 0: The full name of the .cot file
	my $namecot = $_[0];
	if (-e $namecot) {         # only makes sense to check it if it exists

		# generate name of table file from the .cot file name
		my $nametab = $namecot;
		my $lenname = length($namecot);
		if (substr($namecot, $lenname - 3, 3) ne "cot") { return 0; }      # must be a cot file or it's not valid
		substr($nametab, $lenname - 3, 3) = "tab";

		# check time of tab against cot
		my $lasttab = (stat($nametab))[9];            # get last modify time of table
		my $lastcot = (stat($namecot))[9];            # get last modify time of compiled table
		my $timediff = $lastcot - $lasttab;           # compute difference
		return ($timediff >= 0);                      # return ok if .cot on or after .tab
	}
	1;               # default is to return ok if it doesn't exist
}

sub CheckTime ($) {

	# takes an argument that is a string time (6 character - may have leading or trailing whitespace)
	# returns an 6 character string time
	# returns 0 if an error was encountered or the time is invalid - otherwise returns 1
	my $out = $_[0];
	TrimString($out);
	if ($out eq "its_null") { return 1; }           # pass through its_null
	
	# if string has bad length, return 0
	my $lenstr = length($out);
	if ($lenstr != 6 ) { return 0; }

	# if bad character found, return 0
	elsif ($out =~ m/[^0-9]/) { return 0; }

	else {      # have a number in the string - process it
		my $hour   = substr($out, 0, 2);
		my $minute = substr($out, 2, 2);
		my $second = substr($out, 4, 2);
		if ($hour   lt "00" || $hour   gt "23") { return 0; }
		if ($minute lt "00" || $minute gt "59") { return 0; }
		if ($second lt "00" || $second gt "59") { return 0; }
	}

	$_[0] = $out;     # return $out back into argument in case blanks were trimmed off
	1;                # return true - valid time is being returned
}
1;
