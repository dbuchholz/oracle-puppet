package FundrunProcess;

=head1  FundrunProcess.pm

=head2  ABSTRACT:
		Get Fundrun values - current routines are:
		Edfrval       - Given a feed string, interprets and returns the data value

=head2  REVISIONS:
		RAM 01-MAR-04 13354-0  JGF Initial development

=cut

use strict;
use ITSCheck;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edfrval);

sub Edfrval($) {

=head1 sub Edfrval($)

=head2 ABSTRACT:
	   Given a Fundrun string, returns the data value

=head2 ARGUMENTS:
	   0: input 8 character Fundrun string (7 digits and 1 character marker) and output returned data value

=cut

	my $denom;          # denominator for frational processing
	my $fract;          # fractional portion of number
	my $lenint;         # length of integer portion of value
	my $marker;         # marker value
	my $number;         # the incoming and outgoing number for this scope
	my $numdp;          # number of decimal places
	my $whole;          # whole portion of number

	# get number and marker
	if ($_[0] eq "") { return 1; }        # number is already null - just return
	$number = substr($_[0], 0, 7);
	$marker = substr($_[0], 7, 1);

	# check to see that number is all digits
	if ($number =~ m/\D/) {
		$number = "";            #  set bad value to null
	}
	else {
		$numdp = index("0123456ABCDEFGH", $marker);
		if ($numdp < 0) {
			$number = "";            #  set bad value to null
		}
		else {
			if ($numdp <= 6) {                            # numeric numdp - fraction is divisible by power of 10
				$lenint = 7 - $numdp;
				$whole = substr($number, 0, $lenint);
				$fract = substr($number, $lenint, $numdp);
				$number = $whole . "." . $fract;
			}
			else {                                        # alpha numdp - fraction is divisible by power of 2
				$denom = 1 << ($numdp - 6);               # denominator is 2 for A, 4 for B, 8 for C, ...
				$numdp = int (($numdp - 4) / 3);          # set numdp to 1 for ABC, 2 for DEF, 3 for GH
				$lenint = 7 - $numdp;
				$whole = substr($number, 0, $lenint);
				$fract = substr($number, $lenint, $numdp);
				$number = $whole + ($fract / $denom);
			}
		}
	}

	if (CheckNumber($number)) { $_[0] = $number; }       # recheck the number to get rid of non-significant zeros and return it
	return 1;
}
1;
