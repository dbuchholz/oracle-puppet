package ITSCode;

=head1  ITSCode.pm

=head2  ABSTRACT:
		Provide standard code routines - Current routines are:
		EncodeData - Encodes data
		DecodeData - Decodes data

=head2  REVISIONS:
		RAM 19-SEP-02 13354-0  JGF Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(EncodeData DecodeData);

sub EncodeData ($$) {

	# 1: key
	# 2: data

	my $dat = $_[1];             # the data to be encoded
	my $idxdat;                  # an index into the data
	my $idxkey = -1;             # an index into the key (a circular pointer)
	my $key = $_[0];             # the key value
	my $kval;                    # number value of this key character
	my $lendat = length($dat);   # the length of the data
	my $lenkey = length($key);   # the length of the key
	my $nval;                    # number value of this character
	my $result = "";             # the result

	# if there is no key or data, just quit
	if ($lenkey <= 0 || $lendat <= 0) { return 0; }

	# encode the data
	for $idxdat (0 .. ($lendat - 1)) {
		$nval = ord(substr($dat, $idxdat, 1));         # get a data character as a number
		if ($nval < 32) { return 0; }                  # invalid unprintable character
		$idxkey++;
		if ($idxkey >= $lenkey) { $idxkey = 0;}        # index into key is a circular pointer
		$kval = ord(substr($key, $idxkey, 1));         # get a key character as a number
		if ($kval < 32) { return 0; }                  # invalid unprintable character

		# compute encoded character and store it
		$nval = $nval + $kval + $idxdat;
		while ($nval > 126) {
			$nval -= 95; 
		}
		$result .= chr($nval);
	}

	$_[1] = $result;
	1;
}

sub DecodeData ($$) {

	# 1: key
	# 2: data

	my $dat = $_[1];             # the data to be decoded 
	my $idxdat;                  # an index into the data
	my $idxkey = -1;             # an index into the key (a circular pointer)
	my $key = $_[0];             # the key value
	my $kval;                    # number value of this key character
	my $lendat = length($dat);   # the length of the data
	my $lenkey = length($key);   # the length of the key
	my $nval;                    # number value of this character
	my $result = "";             # the result

	# if there is no key or data, just quit
	if ($lenkey <= 0 || $lendat <= 0) { return 0; }

	# decode the data
	for $idxdat (0 .. ($lendat - 1)) {
		$nval = ord(substr($dat, $idxdat, 1));         # get a data character as a number
		if ($nval < 32) { return 0; }                  # invalid unprintable character
		$idxkey++;
		if ($idxkey >= $lenkey) { $idxkey = 0;}        # index into key is a circular pointer
		$kval = ord(substr($key, $idxkey, 1));         # get a key character as a number
		if ($kval < 32) { return 0; }                  # invalid unprintable character

		# compute decoded character and store it
		$nval = $nval - $kval - $idxdat;
		while ($nval < 32) {
			$nval += 95; 
		}
		$result .= chr($nval);
	}

	$_[1] = $result;
	1;
}
