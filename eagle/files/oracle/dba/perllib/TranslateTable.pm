package TranslateTable;

=head1  TranslateTable.pm

=head2  ABSTRACT:
		Package used to process loader translations

=head2  ROUTINES:
		CreateTranslateHash($table_file)
			Creates the hash TranslateHash containing the translation definitions from the pricing translate table.

		ProcessTranslate($translate_name, $translate_value)
			Translates the value accoording to the translation name
			$translate_name		The name of the translation
			$translate_value	The value to be translated

=head2  REVISIONS:
		RCR 02-MAY-07 13354-0  RCR Changed parse_line to look for continuous whitespace
		RAM 30-AUG-05 13354-0  RCR Changed quotewords to parse_line
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 10-JUN-01 13354-0  JGF Initial Development

=cut

use strict;
use ITSCalc;
use ITSTrim;
use Text::ParseWords;

use vars qw(@ISA @EXPORT %TranslateHash);
use Exporter;
@ISA= ('Exporter');
@EXPORT= qw(CreateTranslateHash ProcessTranslate %TranslateHash);


sub CreateTranslateHash ($) {

=head1  sub CreateTranslateHash

=head2  ABSTRACT:
		Creates the hash TranslateHash containing the translation definitions from the pricing translate table.

=head2  REVISIONS:
		RAM 10-JUN-01 13354-0  JGF      Initial Development

=head2  ARGUMENTS:
		$table_file     The name of the file containing the pricing translations

=head2  OUTPUT:
		%TranslateHash - layout is:
		Hash{ $translate_name }
			Hash{ $from_value }
				Value - to value
=cut

my $lenexpr;        # length of MarkerExpr
my $locdash;        # location of dash in MarkerExpr

my $TranslateBeg;
my $TranslateEnd;
my $TranslateExpr;              # Translate expression
my $TranslateVal;
my $value;                      # Translated value

%TranslateHash = ();            # initialize hash to null in case table is not there

# Get file name from first parameter
	my $table_file = $_[0];

	# Open file or return if file can't be opened
	if (!open (Translate_Table, "< $table_file")) {
		return 0;
	}

	my $TranslateType = "";
	my %TranslateValues = ();

	# Loop processing each record in the file
	while (my $InputLine = <Translate_Table>) {
		chomp ($InputLine);

		# Split input line into array of fields
		if (substr($InputLine, 0, 1) eq "&") {
			# If previous record not null, this is not the first record so add value to hash
			if ($TranslateType ne "") {
				$TranslateHash{$TranslateType} = {%TranslateValues};
			}
			%TranslateValues = ();
			$TranslateType = substr($InputLine, 1);
		}
		else {
			# Add new value to the hash for the current translate type
		    ($TranslateExpr, $value) = parse_line('\s+', 0, $InputLine);
			# default is that translate expression is a constant
			# check for translate range and set up values
			$lenexpr = length($TranslateExpr);
			$locdash = index($TranslateExpr, "--");
			if ($locdash >= 1 && $locdash <= ($lenexpr - 2)) {
				# translate expression is range
				$TranslateBeg = substr($TranslateExpr, 0, $locdash);
				$TranslateEnd = substr($TranslateExpr, $locdash + 2);
			}
			else {
				# translate expression is single value
				$TranslateBeg = $TranslateExpr;
				$TranslateEnd = $TranslateExpr;
			}

            # loop through all values of the translation, associating it with the value
			$TranslateVal = $TranslateBeg;
			while ($TranslateVal le $TranslateEnd) {      # doing it this way so it will process strings or numerics
				$TranslateValues{$TranslateVal} = $value;
				IncrementChar($TranslateVal);
			}

		}
	}

	# Add entry for last translate type onto the hash
	$TranslateHash{$TranslateType} = {%TranslateValues};
	close (Translate_Table);
	1;
}


sub ProcessTranslate ($$) {

=head1  sub ProcessTranslate ($$)

=head2  ABSTRACT:
		Translates a value into another value by using the TranslateHash

=head2  ARGUMENTS:
		0: $translate_name - The name of the translation
		1: $translate_value - The value to be translated - this is replaced by the translation on return

=cut

	# put arguments into recognizable scalars
	my ($translate_name, $translate_value) = @_;
	TrimString($translate_value);                     # trim to ensure match with table

	# check to see if this translate value exists for this translate in the hash
	if (!exists($TranslateHash{ $translate_name } { $translate_value })) {
		# translate name or value does not exist in hash
		$_[1] = "";
		return 1;
	}

	# marker name and value exist - get resulting value in hash for this marker and value and return it
	$_[1] = $TranslateHash{ $translate_name } { $translate_value };
	1;
}
