package ITSTrim;

=head1  ITSTrim.pm

=head2  ABSTRACT:
		Provide standard trim routines - Current routines are:
		Trim       - Trims leading and trailing whitespace from a scalar or an the elements of an array
		TrimLine   - Trims leading and trailing whitespace from a string and removes anything after "#"
		TrimString - Trims leading and trailing whitespace from a string

=head2  REVISIONS:
		RAM 02-JUN-05 13354-0  RCR Changed to allow # to not indicate a comment if in "" string
		RAM 01-MAR-05 13354-0  RCR Changed to allow # to not indicate a comment in TrimLine if preceded by a \
		RAM 19-JUN-02 13354-0  JGF Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Trim TrimLine TrimString);


sub Trim {

	# Trims whitespace from the front and back of each element of an array
	# Works equally well on a scalar - note return mechanism

	my @out = @_;
	for (@out) {
		s/^\s+//;
		s/\s+$//;
	}
	return wantarray ? @out : $out[0];
}

sub TrimLine ($) {

	# Removes anything after "#" and trims whitespace from the front and back of a line

	my $line = $_[0];
	if ($line ne "") {              # if line is not null
		my $loc = index($line, "#");              # find comment character
		if    ($loc == 0) { 
			$line = "";                        # if # is first char, line is comment - set to null
		}
		else {
			if ($loc > 0) {
				my $numquo = 0;
				my $pos = -1;
				while (($pos = index(substr($line, 0, $loc), '"', $pos)) > -1) {
					$numquo++;
					$pos++;
				}
				if (substr($line, $loc -1, 1) eq "\\") {         # found # on line - just use what is before that
					substr($line, $loc -1, 1) = "";              # found \# on line - remove \
				}
				elsif (substr($line, $loc -1, 2) eq '$#') {         # found $# on line - ignore - Perl construct
				}
				else {
					if (($numquo % 2) == 0) {
						$line = substr($line, 0, $loc);
					}
				}
			}
			if ($line ne "") {                  # if line is still not null
				$line =~ s/^\s+//;              # trim leading and trailing whitespace
				$line =~ s/\s+$//;
			}
		}
		$_[0] = $line;                 # put modified line back into argument
	}
	1;
}

sub TrimString ($) {

	# Trims whitespace from the front and back of a string

	$_[0] =~ s/^\s+//;
	$_[0] =~ s/\s+$//;
	1;
}
