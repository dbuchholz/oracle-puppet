package ITSUser;

=head1  ITSUser.pm

=head2  ABSTRACT:
		Provide standard  user interface routines - current routines are:
		MonitorSleep - Provides a progress bar to the monitor for a sleep period
		PromptArgument - Gets an argument from ARGV, or, if it is not present there, prompts the user for it

=head2  REVISIONS:
		RAM 06-MAY-05 xxxxx-x  RCR Added MonitorSleep routine
		RAM 06-AUG-04 13354-0  RCR Removed stop on null entry - too restrictive
		RAM 20-AUG-02 13354-0  JGF Initial development

=cut

use strict;
use English;
use ITSCheck;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(MonitorSleep PromptArgument);

sub MonitorSleep($$$) {

=head1  sub MonitorSleep($$$)

=head2  ABSTRACT:
		Provides a progress bar for a sleep period

=head2  ARGUMENTS:
        0: filehandle of log file (may be null)
        1: sleep period in seconds
        2: message text for output

=cut

	use integer;                    # needed for intervals calculation

	my $fh_logfile;                 # filehandle of log file
	my $header;                     # header message for monitor progress bar
	my $interval;                   # computed sleep interval in seconds
	my $inttest;                    # computed sleep interval in seconds - test
	my $leftover;                   # number of leftover seconds at end
	my $lefttest;                    # number of leftover seconds at end - test
	my $length;                     # length of message
	my $message;                    # text message for output
	my $outmsg;                     # output message
	my $periods;                    # number of computed sleep periods
	my $pertest;                    # number of computed sleep periods - test
	my $promsg;                     # progress message
	my $seconds;                    # input sleep period in seconds

	($fh_logfile, $seconds, $message) = @_;
	if (!CheckNumber($seconds)) { return 0; }      # can't do if seconds not a number
	if ($seconds <= 0)          { return 0; }      # can't do if no seconds to sleep

	# construct output message and print to log file
	$outmsg = "- sleeping for " . $seconds . " seconds";
	if ($fh_logfile ne "") { print $fh_logfile "$message $outmsg\n"; }

	if ($seconds < 20) {     # only do progress bar if a significant period of time
		print "$message $outmsg\n";	
		sleep($seconds);
	}
	else {
		# construct new output header message
		$length = length($outmsg);
		$header = "|<<<<<<<<<<<<<<>>>>>>>>>>>>>>|";
		substr($header, ((32 - $length) / 2) , $length - 2) = substr($outmsg, 2);           # center sleeping message in header

		# compute parameters for progress bar
		$leftover = 99999;
		for $pertest (30,15,10,6,5,3,2) {                   # number of periods
			$inttest = $seconds / $pertest;                 # interval length in seconds
			$lefttest = $seconds - ($inttest * $pertest);   # leftover seconds
			# test for improvement in leftover - if so, save values
			if ($lefttest < $leftover) {
				$periods = $pertest;
				$interval = $inttest;
				$leftover = $lefttest;
			}
		}

		# now have parameters for sleeping with progress - print header and execute loop under it
		$OUTPUT_AUTOFLUSH = 1;                     # must autoflush to see progress
		print "$message $header\n";  
		$length = length($message);
		# clear message to blanks and construct progress interval bar
		$message =~ s/\S/ /g;
		$promsg = substr("]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]", 0, (30 / $periods));
		print "$message ";
		for (1 .. $periods) {
			sleep($interval);
			print "$promsg";
		}
		if ($leftover > 0) { sleep($leftover); }
		print "\n";                                # now we can go to the next line
		$OUTPUT_AUTOFLUSH = 0;                     # turn off autoflush
	}
	1;
}

sub PromptArgument ($$$) {

=head1  sub PromptArgument ($$$)

=head2  ABSTRACT:
		Gets an argument from ARGV, or, if it is not present there, prompts the user for it

=head2  ARGUMENTS:
        0: number of argument in ARGV
        1: prompt text
        2: returned value

=cut

	if ($#ARGV >= $_[0]) {
		# if ARGV requested is present, just return it
		$_[2] = $ARGV[$_[0]];
	}
	else {
		# if ARGV requested is not present, prompt user for it and return the reply
		print "$_[1]: ";
		my $reply = <STDIN>;
		chomp($reply);
		$_[2] = $reply;
	}
	1;
}
