package SIRSProcess;

=head1  SIRSProcess.pm

=head2  ABSTRACT:
		Provide SIRS custom edit routines - Current edits are:
		Edsiaccr   - SIRS edit for accrual method
		Edsiapf    - SIRS edit for accrual payment frequency
		Edsicoun   - SIRS edit for countries
		Edsictc    - SIRS edit for coupon type code
		Edsictce   - SIRS edit for coupon type code (enhanced)
		Edsicurr   - SIRS edit for currencies
		Edsidadj   - SIRS edit for delay adjustment
		Edsiexch   - SIRS edit for exchanges
		Edsifcd    - SIRS edit for first coupon date
		Edsifcde   - SIRS edit for first coupon date (enhanced)
		Edsiflt    - SIRS edit for floater flag
		Edsiinvt   - SIRS edit for investment type
		Edsiinvte  - SIRS edit for investment type (enhanced)
		Edsiipbc   - SIRS edit for inflation protected bonds
		Edsiipbd   - SIRS edit for inflation protected bonds
		Edsiipbm   - SIRS edit for inflation protected bonds
		Edsiipbp   - SIRS edit for inflation protected bonds
		Edsilcpn   - SIRS edit for missing LCPN
		Edsindpm   - SIRS edit for next call date at premium
		Edsindpr   - SIRS edit for next call date at par
		Edsinppm   - SIRS edit for next call price at premium
		Edsipst    - SIRS edit for processing security type
		Edsipste   - SIRS edit for processing security type
		Edsistyp   - SIRS edit for security type
		Findsidesc - Find description in saved file
		Getsidesc  - Get description from SIRS record

=head2  REVISIONS:
        RCR 01-JUL-08 13354-0  RCR Changed Edsipst to better correspond to STAR codes
        RCR 13-MAY-08 13354-0  RCR Changed Edsifcde to add TYP2, corrected Edsiapf
        RCR 21-APR-08 13354-0  RCR Changed Edsifcde to add CURCTYP for zero coupons
        RCR 11-MAR-08 13354-0  RCR Added Edsictce, Edsiinvte, Edsipste
        RCR 20-FEB-08 13354-0  RCR Changed Edsipst to better correspond to STAR codes
        RCR 14-FEB-08 13354-0  RCR Changed Edsipst to better correspond to STAR codes
        RCR 08-JAN-08 13354-0  RCR Added Edsindpm, Edsindpr, Edsinppm
        RCR 04-JAN-08 13354-0  RCR Added Edsilcpn for missing LCPN
        RCR 03-JAN-08 13354-0  RCR Changed Edsipst to better correspond to STAR codes
        RCR 12-DEC-07 13354-0  RCR Added Edsiibp_ for inflation protected bond fields
        RCR 11-DEC-07 13354-0  RCR Changed Edsipst to better correspond to STAR codes
        RCR 04-DEC-07 13354-0  RCR Changed Edsictc to better correspond to STAR codes
        RCR 30-NOV-07 13354-0  RCR Added Edsidadj for delay adjustment
        RCR 28-AUG-07 13354-0  RCR Added processing for ASSET code L
        RCR 13-OCT-06 13354-0  RCR Added Findsidesc and Getsidesc
        RAM 11-NOV-05 13354-0  RCR Corrected security type processing for ADR preferreds
        RAM 25-OCT-05 13354-0  RCR Added Edsiexch for exchanges
        RAM 31-AUG-05 13354-0  RCR Corrected coupt use in Edsiflt
		RAM 22-MAR-05 13354-0  JGF Changed Edsipst to correctly classify warrant as EQWRXX
		RAM 20-OCT-03 13354-0  JGF Initial development

=cut

use strict;
use ITSCheck;
use ITSDate;
use ITSTrim;
use MarkerTable;
use RawvalProcess;
use TranslateTable;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edsiaccr Edsiapf Edsicoun Edsictc Edsictce Edsicurr Edsidadj Edsiexch Edsifcd Edsifcde Edsiflt 
                      Edsiinvt Edsiinvte
                      Edsiipbc Edsiipbd Edsiipbm Edsiipbp Edsilcpn Edsindpm Edsindpr Edsinppm
                      Edsipst Edsipste Edsistyp Findsidesc Getsidesc);

my @cad_exch;
my @curr_start;
my $curr_date;

INIT {
	# set up Canadian exchange list - SIRS manual, A-15,16
	@cad_exch = (14,15,19,30,31);

	# get current date for comparisons
	@curr_start = (localtime)[0..5];   # get 6 value localtime array
	$curr_date = sprintf("%04d%02d%02d", $curr_start[5] + 1900, $curr_start[4] + 1, $curr_start[3]);

}

sub Edsiaccr ($) {

	# takes an argument that is the SIRS ASSET and ACCR items and SECT item and returns the coupon_type_code value
	# methodology from field 100, ACCR, of Corviant SIRS spec - see the spec for the detailed logic layout
	
	my ($asset, $accr) = split(",", $_[0]);

	my $daycnt = "";     # set null value for return in case there is no match

	if ($asset ne "" && $accr ne "") {
		if    ("ABCDEFGHIL" =~ /$asset/ && '0'        eq  $accr)  { $daycnt = "Default"; }        # 1
		elsif ("ABCDEFGHIL" =~ /$asset/ && "12345678" =~ /$accr/) { $daycnt = "30/360"; }         # 2
		elsif ("ABCDEFGHI"  =~ /$asset/ && '9'        eq  $accr)  { $daycnt = "ACT/ACT"; }        # 3
		elsif ("ABCDEFGHI"  =~ /$asset/ && 'A'        eq  $accr)  { $daycnt = "ACT/365"; }        # 4
		elsif ("L"          eq /$asset/ && '9A'       =~  $accr)  { $daycnt = "ACT/365"; }        # 5
		elsif ("ABCDEFGHIL" =~ /$asset/ && 'B'        eq  $accr)  { $daycnt = "30/360"; }         # 6
		elsif ("ABCDEFGHIL" =~ /$asset/ && 'CU'       =~  $accr)  { $daycnt = "ACT/ACT"; }        # 7
		elsif ("JK"         =~ /$asset/ && '0'        eq  $accr)  { $daycnt = "30/360"; }         # 8
		elsif ("JK"         =~ /$asset/ && '1'        eq  $accr)  { $daycnt = "ACT/ACT"; }        # 9
		elsif ("JK"         =~ /$asset/ && '2'        eq  $accr)  { $daycnt = "ACT/360"; }        #10
		elsif ("JK"         =~ /$asset/ && '3'        eq  $accr)  { $daycnt = "ACT/ACT"; }        #11
		elsif ("JK"         =~ /$asset/ && '4'        eq  $accr)  { $daycnt = "ACT/365"; }        #12
		elsif ("JK"         =~ /$asset/ && '5'        eq  $accr)  { $daycnt = "30/360"; }         #13
		elsif ("JK"         =~ /$asset/ && '6'        eq  $accr)  { $daycnt = "NA"; }             #14
		elsif ("JK"         =~ /$asset/ && '9'        eq  $accr)  { $daycnt = "New Situation"; }  #15
	}

	$_[0] = $daycnt;

	1;
}

sub Edsiapf ($) {

	# takes an argument that is the SIRS OPAYFRQ, ASSET, and IPFC items and returns the accrual payment frequency
	
	my ($opayfrq, $asset, $ipfc) = split(",", $_[0]);

	my $apf = "";     # set null value for return in case there is no match

	if ($asset ne "") {
		if    ("FGHI" =~ m/$asset/) {
			ProcessTranslate("TRSIPFEQ", $ipfc);                     # NA equities
			$apf = $ipfc;
		}
		elsif ("K" =~ m/$asset/) {
			ProcessTranslate("TRSIPFNEQ", $ipfc);                    # NNA equities
			$apf = $ipfc;
		}
		else {
			if ($opayfrq ne "") {
				if    ("ABCDEJL" =~ m/$asset/) {
					$apf = $opayfrq;
					ProcessTranslate("TRSIOPAY", $apf);                      # global fixed income
				}
			}
			else {
				if    ("ABCDEL" =~ m/$asset/) {
					ProcessTranslate("TRSIPFN", $ipfc);                      # NA fixed income
				}
				elsif ("J" =~ m/$asset/) {
					ProcessTranslate("TRSIPFNN", $ipfc);                     # NNA fixed income
				}
				$apf = $ipfc;
			}
		}
	}

	$_[0] = $apf;
	1;
}

sub Edsicoun ($) {

	# takes an argument that is the SIRS CTRY and CTRI items and returns the country value
	# this is necessary because some SIRS records do not have CTRI populated
	
	my ($ctry, $ctri) = split(",", $_[0]);
	my $test = "";

	if    ($ctri ne "") {
		$test = $ctri;
	}
	elsif ($ctry ne "") {
		if ($ctry ne "OUT") {
			$test = substr($ctry, 0, 2);
		}
	}

	$_[0] = $test;     # return country
	1;
}

sub Edsictc ($) {

	# takes an argument that is the SIRS SECT item and returns the coupon_type_code value
	
	my $sect = $_[0];
	CheckCharMin($sect, 3);
	if (substr($sect, 1, 1) eq "B") {             # if second character is a B, security is a bond - process this
		my $test = substr($sect, 2, 1);           # translate third character into a code
		if    ($test eq "M") { $_[0] = "I"; }      # Variable
		elsif ($test eq "N") { $_[0] = "S"; }      # Adjustable
		elsif ($test eq "P") { $_[0] = "X"; }      # Debt Issue/Floating
		else                 { $_[0] = "F"; }      # Fixed
	}
	else {
		$_[0] = "";     # return null for all other asset types
	}
	1;
}

sub Edsictce ($) {

	# takes an argument that is the SIRS ADJIND, COUPT, TYP2, and CURCTYP items and returns the coupon_type_code value
	
	my ($adjind, $coupt, $typ2, $curctyp) = split(",", $_[0]);
	my $ctc = "";                # set to null in case no match found	

	if ($adjind ne "") {
		if ($adjind eq "F") { $ctc = "X"; }     # floating
		if ($adjind eq "I") { $ctc = "R"; }     # inverse floating
		if ($adjind eq "R") { $ctc = "V"; }     # unscheduled variable
	}

	if ($ctc eq "") {        # if not defined, populate based on COUPT
		if ($coupt ne "") {
			if ("ABCFM" =~ m/$coupt/) { $ctc = "F"; }    # fixed
			if ("DGI"   =~ m/$coupt/) { $ctc = "S"; }    # step
			if ("EHJ"   =~ m/$coupt/) { $ctc = "I"; }    # variable
			if ("N"     =~ m/$coupt/) { $ctc = "X"; }    # floating
		}
	}

	if ($ctc eq "") {        # if not defined, populate based on CURCTYP
		if ($curctyp ne "") {
			if ("BCDOPSVWXZ" =~ m/$curctyp/) { $ctc = "F"; }    # fixed
			if ("N"          =~ m/$curctyp/) { $ctc = "S"; }    # step
			if ("ALR"        =~ m/$curctyp/) { $ctc = "I"; }    # variable
			if ("EFGT"       =~ m/$curctyp/) { $ctc = "X"; }    # floating
			if ("R"          =~ m/$curctyp/) { $ctc = "R"; }    # inverse floating
		}
	}

	if ($ctc eq "") {        # if not defined, populate based on CURCTYP
		if ($typ2 ne "") {
			if ($typ2 eq "A") {
				$ctc = "F";     # fixed
			}
		}
	}

	$_[0] = $ctc;     # return ctc value
	1;
}

sub Edsicurr ($) {

	# takes an argument that is the SIRS EXTK item and either ISC1, CPCC, or INCC
	# returns the currency value 
	# if the latter item is not null, that is returned.  
	# If the latter item is null, the currency is derived from the exchange code
	
	my ($exch, $curr) = split(",", $_[0]);
	my $test;

	# if currency is null, get currency from exchange code
	if ($curr eq "") {
		if ($exch ne "") {
			if (CheckNumber($exch)) {
				# exchange is a number - determine if USD or CAD
				$curr = "USD";
				foreach $test (@cad_exch) {
					if ($test == $exch) {
						$curr = "CAD";
						last;
					}
				}
			}
			else {
				$curr = substr($exch, 0, 2);                 # only use first two characters (country code)
				ProcessTranslate("TRSICURR", $curr);         # translate country code into currency
			}
		}
	}

	$_[0] = $curr;     # return currency
	1;
}

sub Edsidadj ($) {

	# takes an argument that is the SIRS DELAY item and adjusts down a date by that many days
	# if ther DELAY item is null, no adjustment occurs
	
	my $newdat;
	my ($delay, $mdat) = split(",", $_[0]);

	# check validity of date
	if (!CheckDate($mdat)) {
		$mdat = "";
	}
	else {
		# if delay is valid, adjust date down by delay days
		if (defined($delay)) {
			if ($delay ne "") {
				if (CheckNumber($delay)) {
					# delay is a number - adjust date down by delay days
					CalcDate8X2T7($mdat, - $delay, $newdat);
					$mdat = $newdat;
				}
			}
		}
	}

	$_[0] = $mdat;     # return date
	1;
}

sub Edsilcpn ($) {

	# takes several arguments and computes the last coupon date because LCPN was missing on the SIRS file
	
	my $locund;
	my $months;
	my $newdat;
	my $new_month;
	my $retdate = "";
	my $testdate;
	my $testyear;
	my $testmonth;
	my ($coupt, $opayfrq, $asset, $ipfc, $fcpn, $delay, $mdat) = split(",", $_[0]);
	my ($starfrq) = join(",", $opayfrq, $asset, $ipfc);

	Edsiapf($starfrq);

	# check validity of maturity date and compute new lcpn date
	if (CheckDate($mdat)) {
		if    ($coupt eq "B") {
			$retdate = $mdat;
		}
		elsif ($starfrq eq "MAT") {
			$retdate = $mdat;
		}
		else {
			$locund = index($starfrq, "_M");
			if ($locund > 0) {
				$months = substr($starfrq, 0, $locund);
				if (CheckNumber($months)) {
					if ($months >= 1 && $months <= 12) {
						$testdate = $mdat;
						$testyear = substr($testdate, 0, 4);
						$testmonth = substr($testdate, 4, 2);
						$new_month = $testmonth - $months;
						if ($new_month <= 0) {
							$testyear = $testyear - 1;
							$new_month = $new_month + 12;
						}
						$testdate = $testyear . sprintf("%02d",$new_month) . substr($testdate, 6, 2);
						
						if (CheckDate($fcpn)) {
							
							# check to see if day matches that of first coupon - if not, adjust by delay days
							if (substr($testdate, 6, 2) eq substr($fcpn, 6, 2)) {
								$retdate = $testdate;
							}
							else {
								# if delay is valid, adjust date down by delay days
								if (defined($delay)) {
									if ($delay ne "") {
										if (CheckNumber($delay)) {
											# delay is a number - adjust date down by delay days
											CalcDate8X2T7($mdat, - $delay, $newdat);
											$retdate = $newdat;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	$_[0] = $retdate;     # return date
	1;
	}

	sub Edsindpm ($) {

	# finds the next call date at premium
	my $cspr;
	my $date;
	my $idx;
	my $itemaddr;
	my $length;
	
	my $nxpr = $_[0];
	$_[0] = "";                          # set default return to null
	if ($nxpr eq "") { return; }         # if no next price, return null

	if ($nxpr > 100) { 
		# above premium - return NXDT
		$length = 0;
		$_[0] = GetRawval("NXDT", $length);
		return;
	}

	for ($idx = 1; $idx < 1000; $idx++) {
		$itemaddr = "CSPR" . $idx;
		$length = 0;
		$cspr = GetRawval($itemaddr, $length);
		if ($cspr eq "") { return; }           # if no more CSPR, return null

		if ($cspr > 100) {
			# above premium - return corresponding CSDT
			$itemaddr = "CSDT" . $idx;
			$length = 0;
			$date = GetRawval($itemaddr, $length);
			if ($date < $curr_date) { next; }        # date must be at least current date
			$_[0] = $date;
			return;
		}
	}
	1;
}

sub Edsindpr ($) {

	# finds the next call date at par
	my $cspr;
	my $date;
	my $idx;
	my $itemaddr;
	my $length;
	
	my $nxpr = $_[0];
	$_[0] = "";                          # set default return to null
	if ($nxpr eq "") { return; }         # if no next price, return null

	if ($nxpr == 100) { 
		# above premium - return NXDT
		$length = 0;
		$_[0] = GetRawval("NXDT", $length);
		return;
	}

	for ($idx = 1; $idx < 1000; $idx++) {
		$itemaddr = "CSPR" . $idx;
		$length = 0;
		$cspr = GetRawval($itemaddr, $length);
		if ($cspr eq "") { return; }           # if no more CSPR, return null

		if ($cspr == 100) {
			# above premium - return corresponding CSDT
			$itemaddr = "CSDT" . $idx;
			$length = 0;
			$date = GetRawval($itemaddr, $length);
			if ($date eq "") { return; }             # if no more CSDT, return null
			if ($date < $curr_date) { next; }        # date must be at least current date
			$_[0] = $date;
			return;
		}
	}
	1;
}

sub Edsinppm ($) {

	# finds the next call price at premium
	my $cspr;
	my $date;
	my $idx;
	my $itemaddr;
	my $length;
	
	my $nxpr = $_[0];
	$_[0] = "";                          # set default return to null
	if ($nxpr eq "") { return; }         # if no next price, return null

	if ($nxpr > 100) { 
		# above premium - return NXPR
		$_[0] = $nxpr;
		return;
	}

	for ($idx = 1; $idx < 1000; $idx++) {
		$itemaddr = "CSDT" . $idx;
		$length = 0;
		$date = GetRawval($itemaddr, $length);
		if ($date eq "") { return; }             # if no more CSDT, return null
		if ($date < $curr_date) { next; }        # date must be at least current date

		$itemaddr = "CSPR" . $idx;
		$length = 0;
		$cspr = GetRawval($itemaddr, $length);
		if ($cspr eq "") { return; }             # if no more CSPR, return null

		if ($cspr > 100) {
			# above premium - return CSPR
			$_[0] = $cspr;
			return;
		}
	}
	1;
}

sub Edsiexch ($) {

	# takes an argument that is the SIRS ASSET and EXCH items and returns the exchange code
	
	my ($asset, $exch) = split(",", $_[0]);

	my $code = "";     # set null value for return in case there is no match

	if ($asset ne "") {
		if    ("ABCDEFGHIL" =~ m/$asset/) {
			TrimString($exch);
			if (ProcessMarker("MKSIEXEQ", $exch, $exch)) { $code = $exch; }            # NA securities
		}
		elsif ("J" eq $asset) {
			TrimString($exch);
			if (ProcessMarker("MKSIEXFI", $exch, $exch)) { $code = $exch; }            # NNA fixed income
		}
		else {                                              # $asset must be K - NNA equities
			$code = $exch;                                  # just pass through EXSHARE code
		}
	}

	$_[0] = $code;               # return exchange code
	1;
}

sub Edsifcd ($) {

	# takes an argument that is the SIRS ISDT, MDAT, ASSET, DELAY, and FCPN items and returns the first coupon date
	
	my $days;
	my $fcd;
	my $years;
	my ($isdt, $mdat, $asset, $delay, $fcpn) = split(",", $_[0]);

	$fcd = $fcpn;                                             # usually fine - check for ST assets
	if ($fcd ne "") {
		if ($asset ne "") {
			if ("ABCDJL" =~ m/$asset/) {
				if (CheckDate($mdat) && CheckDate($isdt)) {           # both dates must be valid to process
					if (CalcDate8T2X7($isdt, $mdat, $days)) {
						$years = $days / 365.25;
						if ($years < 1.0) { $fcd = $mdat; }           # if less than one year from start to maturity, short term
																	  # return mdat as fcd
					}
				}
			}
			if ($fcd == 0) { $fcd = ""; }                              # if not enough information to process, set to null
		}
	}

	# adjust by delay days
	if ($fcd ne "" && $delay ne "" && $delay != 0) {
		CalcDate8X2T7($fcd, -$delay, $fcd);
	}

	$_[0] = $fcd;     # return first coupon date
	1;
}

sub Edsifcde ($) {

	# takes an argument that is the SIRS COUPT, OPAYFRQ, MDAT, ASSET, DELAY, CURCTYP, and FCPN items and 
	# returns the first coupon date
	
	my $fcd;
	my ($coupt, $opay, $mdat, $asset, $delay, $curctyp, $fcpn) = split(",", $_[0]);

	$fcd = $fcpn;                                       

	if ($asset ne "") {
		if ("ABCDJL" =~ m/$asset/) {
			if ($opay eq "H" || $opay eq "I" || $coupt eq "B") {
				# security pays at maturity
				if (CheckDate($mdat)) {  
					$fcd = $mdat;                     # return mdat as fcd
				}
			}
		}
	}
	if ($fcd ne "") {
		if ($fcd == 0) { $fcd = ""; }                              # if not enough information to process, set to null
	}

	# if still null, check for zero coupon type or strip
	if ($curctyp ne "") {
		if ("BCPSZ" =~ m/$curctyp/) {
			# security pays at maturity
			if (CheckDate($mdat)) {
				$fcd = $mdat;                     # return mdat as fcd
			}
		}
	}

	# adjust by delay days
	if ($fcd ne "" && $delay ne "" && $delay != 0) {
		CalcDate8X2T7($fcd, -$delay, $fcd);
	}

	$_[0] = $fcd;     # return first coupon date
	1;
}

sub Edsiflt ($) {

	# takes an argument that is the SIRS ASSET, CFRTI, and COUPT items and returns the floater_flag value
	
	my ($asset, $cfrti, $coupt) = split(",", $_[0]);
	my $ffv = "";                # return null if nothing matches
	if ($asset ne "") {
		if    ($asset eq "C") {
			if ($cfrti eq "F") { $ffv = "Y"; }
			else               { $ffv = "N"; }
		}
		elsif ("ABDJL" =~ /$asset/) {
			if ($coupt eq "E") { $ffv = "Y"; }
			else               { $ffv = "N"; }
		}
	}

	$_[0] = $ffv;     # return ffv
	1;
}

sub Edsiinvt ($) {

	# takes an argument that is the SIRS ISDT, MDAT, and ASSET items and returns the investment type
	
	my $days;
	my $invt;
	my $years;
	my ($isdt, $mdat, $asset) = split(",", $_[0]);

	$invt = "";                       # set to null in case nothing matches
	if ($asset ne "") {
		if    ("EFGHK" =~ m/$asset/) {
			$invt = "EQ";
		}
		elsif ("I" =~ m/$asset/) {
			$invt = "OP";
		}
		elsif ("ABCDJL" =~ m/$asset/) {
			$invt = "FI";
			if (CheckDate($mdat) && CheckDate($isdt)) {           # both dates must be valid to process
				if (CalcDate8T2X7($isdt, $mdat, $days)) {
					$years = $days / 365.25;
					if ($years < 1.0) { $invt = "ST"; }           # if less than one year from start to maturity, short term
				}
			}
		}
	}

	$_[0] = $invt;     # return investment type
	1;
}

sub Edsiinvte ($) {

	# takes an argument that is the SIRS ASSET, FLINV, PDT, SECT, and COUPT items and returns the investment type
	
	my $invt;
	my ($asset, $flinv, $pdt, $sect, $coupt) = split(",", $_[0]);

	$invt = "";                       # set to null in case nothing matches
	if ($asset ne "") {
		if    ("EFGHK" =~ m/$asset/) {
			$invt = "EQ";
			if ($asset eq "G" && $sect ne "") {
				if (substr($sect, 1, 1) eq "X") { $invt = "INDX"; }
			}
		}
		elsif ($asset eq "A") {
			$invt = "FI";
			if ($flinv ne "I") {
				if ($pdt ne "K") {
					if ($sect ne "") {
						if (substr($sect, 2, 1) eq "Z") { $invt = "ST"; }
					}
					if ($coupt eq "M") { $invt = "ST"; }
				}
			}
		}
		elsif ("BCDJ" =~ m/$asset/) {
			$invt = "FI";
		}
		elsif ($asset eq "L") {
			$invt = "ST";
		}
		elsif ($asset eq "I") {
			$invt = "OP";
		}
	}

	$_[0] = $invt;     # return investment type
	1;
}

sub Edsiipbc ($) {

	# takes an argument that is the SIRS ASSET, SECT, CTRY, and CTRI items and returns ipb special value
	
	my ($asset, $sect, $ctry, $ctri) = split(",", $_[0]);
	my $test = "";

	# check for IPB
	if ($asset eq "A") {
		if ($sect eq "GB1") {
			# check country
			if    ($ctri ne "") {
				if ($ctri eq "US") { $test = "CPIUSA"; }
			}
			elsif ($ctry eq "US")  { $test = "CPIUSA"; }
		}
	}

	$_[0] = $test;     # return ipb special falue
	1;
}

sub Edsiipbd ($) {

	# takes an argument that is the SIRS ASSET, SECT, CTRY, and CTRI items and returns ipb special value
	
	my ($asset, $sect, $ctry, $ctri) = split(",", $_[0]);
	my $test = "";

	# check for IPB
	if ($asset eq "A") {
		if ($sect eq "GB1") {
			# check country
			if    ($ctri ne "") {
				if ($ctri eq "US") { $test = "Y"; }
			}
			elsif ($ctry eq "US")  { $test = "Y"; }
		}
	}

	$_[0] = $test;     # return ipb special value
	1;
}

sub Edsiipbm ($) {

	# takes an argument that is the SIRS ASSET, SECT, CTRY, and CTRI items and returns ipb special value
	
	my ($asset, $sect, $ctry, $ctri) = split(",", $_[0]);
	my $test = "";

	# check for IPB
	if ($asset eq "A") {
		if ($sect eq "GB1") {
			# check country
			if    ($ctri ne "") {
				if ($ctri eq "US") { $test = "ACT_3M"; }
			}
			elsif ($ctry eq "US")  { $test = "ACT_3M"; }
		}
	}

	$_[0] = $test;     # return ipb special falue
	1;
}

sub Edsiipbp ($) {

	# takes an argument that is the SIRS ASSET, SECT, CTRY, and CTRI items and returns ipb special value
	
	my ($asset, $sect, $ctry, $ctri) = split(",", $_[0]);
	my $test = "";

	# check for IPB
	if ($asset eq "A") {
		if ($sect eq "GB1") {
			# check country
			if    ($ctri ne "") {
				if ($ctri eq "US") { $test = "5"; }
			}
			elsif ($ctry eq "US")  { $test = "5"; }
		}
	}

	$_[0] = $test;     # return ipb special falue
	1;
}

sub Edsipst ($) {

	# takes an argument that is the SIRS ASSET, SECT, TYP2, ISSRT, MDAT, ISDT, and ACCR items and 
	# returns the processing security type
	# domestic methodology from Note 2 of Corviant SIRS spec - see this spec note for the detailed logic layout
	# foreign methodology - done by ITS

	my $days;                   # temporary for maturity calculation
	my $years;                  # temporary for maturity calculation
	
	my $styp = "";              # set to null default value in case there is no match

	my ($asset, $sect, $typ2, $issrt, $mdat, $isdt, $accr) = split(",", $_[0]);
	CheckCharMin($sect, 3);

	# domestic
	my $sect1 = substr($sect, 1, 1);      # one character comparisons are done with second character
	my $sect2 = substr($sect, 0, 2);      # two character comparisons are done with first two characters
	my $sect3 = substr($sect, 2, 1);      # this is the third character of SECT
	# foreign
	my $sectfe = substr($sect, -2, 2);    # last two characters used for foreign equities
	my $sectfb = substr($sect, -1, 1);    # last character used for foreign bonds

	# Code in this module is very explicitly written on purpose
	# This is designed to correspond to Edsistyp below for easier verification

	if ($asset ne "") {
		if    ($asset eq "E") {
			if    ($sect1  eq "P" ) { $styp = "EQCSPF"; }
			elsif ($sectfe eq "AY") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "PC") { $styp = "EQCSPF"; }
		}
		elsif ($asset eq "F") {
			if    ($sect1 eq "W" ) { $styp = "EQWRXX"; }
		}
		elsif ($asset eq "G") {
			if    ($sect1 eq "A" ) { $styp = "EQCSCS"; }
			elsif ($sect1 eq "T" ) { $styp = "EQCSCS"; }
			elsif ($sect1 eq "C" ) { 
				$styp = "EQCSCS"; 
				if    ($typ2 eq "I") { $styp = "EQCSCS"; }
				elsif ($typ2 eq "G") { $styp = "EQEQMF"; }
				elsif ($typ2 eq "F") { $styp = "EQEQMF"; }
			}
			elsif ($sect1 eq "S" ) { $styp = "EQCSCS"; }
			elsif ($sect2 eq "XX") { $styp = "INXXXX"; }
			elsif ($sect1 eq "R" ) { $styp = "EQRTXX"; }
		}
		elsif ($asset eq "H") {
			if ($typ2 eq "1" ) { $styp = "EQCSCS"; }
		}
		elsif ($asset eq "I") {
			if ($sect1 eq "O" ) { $styp = "OPOPEQ"; }
		}
		elsif ($asset eq "A") {
			if    ("AJLP" =~ /$issrt/) { $styp = "DBIBFD"; }
			elsif ("GQRS" =~ /$issrt/) { 
				if    ($typ2 eq "B") { 
					$styp = "DBDCST"; 
				}
				elsif ($typ2 eq "A") { $styp = "DBIBFD"; }
				elsif ($typ2 eq "H" || $typ2 eq "D") { 
					$styp = "DBIBFD";
					if (CheckDate($mdat) && CheckDate($isdt)) {           # both dates must be valid to process
						if (CalcDate8T2X7($isdt, $mdat, $days)) {
							$years = $days / 365.25;
							if ($years >= 10.0) { $styp = "DBIBFD"; }
							else                { $styp = "DBIBFD"; }
						}
					}
				}
				elsif ($typ2 eq "N" || $typ2 eq "Z") { 
					$styp = "DBIBFD";
					if ($accr eq "A" || $accr eq "B") { $styp = "DBDCST"; }
				}
				if ($sect3 eq "1") { $styp = "DBFLTP"; }
			}
			elsif ($issrt eq "M") { $styp = "DBIBMU"; }
		}
		elsif ($asset eq "B") {
			if ($typ2 eq "Y" ) { 
				if ($sect1 ne "M") { $styp = "DBFBFB"; }
			}
		}
		elsif ($asset eq "C") {
			if ($typ2 eq "3") {
				if ($sect1 ne "M") { $styp = "DBFBFB"; }
			}
			if ($typ2 eq "2") {
				if ($sect1 ne "M") { 
					if ("QRS" =~ /$issrt/) {$styp = "DBFBFB"; }
					else                   {$styp = "DBFBFB"; }
				}
			}
			if ($typ2 eq "P") {
				if ($accr eq "B") {$styp = "DBDCCP"; }
				else              {$styp = "DBIBCP"; }
			}
		}
		elsif ($asset eq "D") {
			if ($sect1 eq "M" ) { 
				if ("GL" =~ /$typ2/) { $styp = "DBIBMU"; }
				else                 { $styp = "DBIBMU"; }
			}
			if ($sect1 eq "B" ) { 
				if ("AB" =~ /$typ2/) { $styp = "DBIBMU"; }
				else                 { $styp = "DBIBMU"; }
			}
		}
		elsif ($asset eq "L") {
			$styp = "DBDCST";
			if ($sect1 eq "B" ) { $styp = "DBIBFD"; }
		}

		# foreign
		elsif ($asset eq "J") {
			if ($sectfb ge "A" && $sectfb le "Z") { $styp = "DBIBFD"; }
		}
		elsif ($asset eq "K") {
			# foreign equities - in some cases there are bond definitions in here just in case
			# also, we define beyond what is in the SIRS manual, again, just in case
			if    ($sectfe eq "02") { $styp = "EQRTXX"; }
			elsif ($sectfe eq "05") { $styp = "CRCRCR"; }
			elsif ($sectfe ge "08" && $sectfe le "22") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "23") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "24") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "31") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "32") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "33") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "34") { $styp = "EQCSPF"; }
			elsif ($sectfe eq "35") { $styp = "EQCSPF"; }
			elsif ($sectfe ge "50" && $sectfe le "51") { $styp = "OPOPEQ"; }
			elsif ($sectfe ge "52" && $sectfe le "54") { $styp = "EQCSCS"; }
			elsif ($sectfe ge "55") { $styp = "OPOPEQ"; }
			elsif ($sectfe ge "56" && $sectfe le "57") { $styp = "EQCSCS"; }
			elsif ($sectfe ge "59") { $styp = "DBIBFD"; }
			elsif ($sectfe ge "5A" && $sectfe le "5E") { $styp = "EQCSCS"; }
			elsif ($sectfe ge "60" && $sectfe le "61") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "62") { $styp = "DBIBFD"; }
			elsif ($sectfe ge "63" && $sectfe le "70") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "71") { $styp = "FTXXXX"; }
			elsif ($sectfe ge "80" && $sectfe le "85") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "86") { $styp = "DBIBFD"; }
			elsif ($sectfe ge "87" && $sectfe le "88") { $styp = "EQCSCS"; }
			elsif ($sectfe eq "90") { $styp = "FWXXXX"; }
			elsif ($sectfe eq "XF") { $styp = "INXXXX"; }
		}
	}

	$_[0] = $styp;
	1;
}

sub Edsipste ($) {

	# takes an argument that is the SIRS CUSIP, ASSET, SECT, TYP2, FLINV, PDT, COUPT, CIOPO, CLDB, PIKAMT items and 
	# returns the processing security type
	# methodology from spreadsheet provided to us for BAM implementation

	my $styp = "";              # set to null default value in case there is no match

	my ($cusip, $asset, $sect, $typ2, $flinv, $pdt, $coupt, $ciopo, $cldb, $pikamt) = split(",", $_[0]);
	CheckCharMin($sect, 3);

	if ($asset ne "") {
		if    ($asset eq "A") {
			$styp = "DBIBFD";
			if    ($flinv eq "I") {
				$styp = "DBIVIV";
			}
			elsif ($pdt eq "K") {
				$styp = "DBIBPK";
			}
			elsif (substr($cusip, 0, 4) eq "9128" && substr($sect, 2, 1) eq "1") {
				$styp = "DBFLTP";
			}
			elsif (substr($sect, 2, 1) eq "Z") {
				$styp = "DBDCST";
			}
			elsif ($coupt eq "M") {
				$styp = "DBDCST";
			}
		}
		elsif ($asset eq "B") {
			$styp = "DBFBFB";
			if    ($ciopo eq "I" || $ciopo eq "E") {
				$styp = "DBFBIO";
			}
			elsif ($ciopo eq "P") {
				$styp = "DBFBPO";
			}
		}
		elsif ($asset eq "C") {
			$styp = "DBFBFB";
			if    ($ciopo eq "I" || $ciopo eq "E") {
				$styp = "DBFBIO";
			}
			elsif ($ciopo eq "P") {
				$styp = "DBFBPO";
			}
			elsif ($cldb eq "CARD") {
				$styp = "DBFBCC";
			}
			elsif ($cldb eq "AUTL" || $cldb eq "CARS" || $cldb eq "RECV" || $cldb eq "TRUC") {
				$styp = "DBFBAL";
			}
		}
		elsif ($asset eq "D") {
			$styp = "DBIBMU";
		}
		elsif ($asset eq "E") {
			$styp = "EQCSPF";
			if ($pikamt ne "") {
				if ($pikamt > 0.0) {
					$styp = "EQCSPK";
				}
			}
		}
		elsif ($asset eq "F") {
			$styp = "EQWRXX";
		}
		elsif ($asset eq "G") {
			$styp = "EQCSCS";
			if (substr($typ2, 0, 1) eq "G" || substr($typ2, 0, 1) eq "F") {
				$styp = "EQEQMF";
			}
			elsif (substr($sect, 1, 1) eq "X") {
				$styp = "INXXXX";
			}
			elsif (substr($sect, 1, 1) eq "R") {
				$styp = "EQRTXX";
			}
		}
		elsif ($asset eq "H") {
			$styp = "EQCSCS";
		}
		elsif ($asset eq "I") {
			$styp = "OPOPEQ";
		}
		elsif ($asset eq "J") {
			$styp = "DBIBFD";
		}
		elsif ($asset eq "K") {
			my $pos2 = substr($sect, 1, 1);
			my $pos3 = substr($sect, 2, 1);
			if    (($pos2 eq "0" || $pos2 eq "1" || $pos2 eq "2" || $pos2 eq "5" || $pos2 eq "6") && $pos3 ne "2") {
				$styp = "EQCSCS";
			}
			elsif ($pos2 eq "3") {
				$styp = "EQCSPF";
			}
			elsif ($pos2 eq "0" && $pos3 eq "2") {
				$styp = "EQCSPF";
			}
			elsif ($pos2 eq "5") {
				$styp = "EQWRXX";
			}
		}
		elsif ($asset eq "L") {
			$styp = "DBIBST";
		}
	}

	$_[0] = $styp;
	1;
}

sub Edsistyp ($) {

	# takes an argument that is the SIRS ASSET, SECT, TYP2, ISSRT, MDAT, ISDT, and ACCR items and 
	# returns the security type
	# domestic methodology from Note 2 of Corviant SIRS spec - see this spec note for the detailed logic layout
	# foreign methodology - done by ITS

	my $days;                   # temporary for maturity calculation
	my $years;                  # temporary for maturity calculation
	
	my $styp = "";              # set to null default value in case there is no match

	my ($asset, $sect, $typ2, $issrt, $mdat, $isdt, $accr) = split(",", $_[0]);
	CheckCharMin($sect, 3);

	# domestic
	my $sect1 = substr($sect, 1, 1);      # one character comparisons are done with second character
	my $sect2 = substr($sect, 0, 2);      # two character comparisons are done with first two characters
	# foreign
	my $sectfe = substr($sect, -2, 2);    # last two characters used for foreign equities
	my $sectfb = substr($sect, -1, 1);    # last character used for foreign bonds

	if ($asset ne "") {
		# domestic
		if    ($asset eq "E") {
			if    ($sect1  eq "P" ) { $styp = "PFD"; }
			elsif ($sectfe eq "AY") { $styp = "ADRP"; }
			elsif ($sectfe eq "PC") { $styp = "CPFD"; }
		}
		elsif ($asset eq "F") {
			if    ($sect1 eq "W" ) { $styp = "WARR"; }
		}
		elsif ($asset eq "G") {
			if    ($sect1 eq "A" ) { $styp = "ADR"; }
			elsif ($sect1 eq "T" ) { $styp = "CERT"; }
			elsif ($sect1 eq "C" ) { 
				$styp = "COM"; 
				if    ($typ2 eq "I") { $styp = "REIT"; }
				elsif ($typ2 eq "G") { $styp = "MFO"; }
				elsif ($typ2 eq "F") { $styp = "MFC"; }
			}
			elsif ($sect1 eq "S" ) { $styp = "COM"; }
			elsif ($sect2 eq "XX") { $styp = "INDX"; }
			elsif ($sect1 eq "R" ) { $styp = "RTS"; }
		}
		elsif ($asset eq "H") {
			if ($typ2 eq "1" ) { $styp = "UIT"; }
		}
		elsif ($asset eq "I") {
			if ($sect1 eq "O" ) { $styp = "OPTN"; }
		}
		elsif ($asset eq "A") {
			if    ("AJLP" =~ /$issrt/) { $styp = "CB"; }
			elsif ($issrt eq "G") { 
				if    ($typ2 eq "B") { $styp = "ST"; }
				elsif ($typ2 eq "A") { $styp = "STRP"; }
				elsif ($typ2 eq "H" || $typ2 eq "D") { 
					$styp = "TN";
					if (CheckDate($mdat) && CheckDate($isdt)) {           # both dates must be valid to process
						if (CalcDate8T2X7($isdt, $mdat, $days)) {
							$years = $days / 365.25;
							if ($years >= 10.0) { $styp = "TBD"; }
							else                { $styp = "TN";  }
						}
					}
				}
				elsif ($typ2 eq "N") { $styp = "TN"; }
			}
			elsif ($issrt eq "M") { $styp = "MUNI"; }
			elsif ("QRS" =~ /$issrt/) { $styp = "GAB"; }
		}
		elsif ($asset eq "B") {
			if ($typ2 eq "Y" ) { 
				if ($sect1 ne "M") { $styp = "MPT"; }
			}
		}
		elsif ($asset eq "C") {
			if ($typ2 eq "3") {
				if ($sect1 ne "M") { $styp = "ABAC"; }
			}
			if ($typ2 eq "2") {
				if ($sect1 ne "M") { 
					if ("QRS" =~ /$issrt/) {$styp = "CMOA"; }
					else                   {$styp = "CMON"; }
				}
			}
			if ($typ2 eq "P") {
				if ($accr eq "B") {$styp = "CP"; }
				else              {$styp = "CPIB"; }
			}
		}
		elsif ($asset eq "D") {
			if ($sect1 eq "M" ) { 
				if ("GL" =~ /$typ2/) { $styp = "MURB"; }
				else                 { $styp = "MUGO"; }
			}
			if ($sect1 eq "B" ) { 
				if ("AB" =~ /$typ2/) { $styp = "MURB"; }
				else                 { $styp = "MUGO"; }
			}
		}
		elsif ($asset eq "L") {
			$styp = "ST";
			if ($sect1 eq "B" ) { $styp = "CD"; }
		}

		# foreign
		elsif ($asset eq "J") {
			if ($sectfb ge "A" && $sectfb le "Z") { $styp = "CB"; }
		}
		elsif ($asset eq "K") {
			# foreign equities - in some cases there are bond definitions in here just in case
			# also, we define beyond what is in the SIRS manual, again, just in case
			if    ($sectfe eq "02") { $styp = "RTS"; }
			elsif ($sectfe eq "05") { $styp = "CASH"; }
			elsif ($sectfe ge "08" && $sectfe le "22") { $styp = "COM"; }
			elsif ($sectfe eq "23") { $styp = "PFC"; }
			elsif ($sectfe eq "24") { $styp = "COM"; }
			elsif ($sectfe eq "31") { $styp = "PFC"; }
			elsif ($sectfe eq "32") { $styp = "CPFD"; }
			elsif ($sectfe eq "33") { $styp = "PFC"; }
			elsif ($sectfe eq "34") { $styp = "PFC"; }
			elsif ($sectfe eq "35") { $styp = "CPFD"; }
			elsif ($sectfe ge "50" && $sectfe le "51") { $styp = "OPTN"; }
			elsif ($sectfe ge "52" && $sectfe le "54") { $styp = "COM"; }
			elsif ($sectfe ge "55") { $styp = "OPTN"; }
			elsif ($sectfe ge "56" && $sectfe le "57") { $styp = "COM"; }
			elsif ($sectfe ge "59") { $styp = "CERT"; }
			elsif ($sectfe ge "5A" && $sectfe le "5E") { $styp = "COM"; }
			elsif ($sectfe ge "60" && $sectfe le "61") { $styp = "COM"; }
			elsif ($sectfe eq "62") { $styp = "CB"; }
			elsif ($sectfe ge "63" && $sectfe le "70") { $styp = "COM"; }
			elsif ($sectfe eq "71") { $styp = "FUT"; }
			elsif ($sectfe ge "80" && $sectfe le "85") { $styp = "COM"; }
			elsif ($sectfe eq "86") { $styp = "GAB"; }
			elsif ($sectfe ge "87" && $sectfe le "88") { $styp = "COM"; }
			elsif ($sectfe eq "90") { $styp = "FWD"; }
			elsif ($sectfe eq "XF") { $styp = "INDX"; }
		}
	}
	$_[0] = $styp;
	1;
}

sub Getsidesc($$$) {

	my $ctry = "";       # pricing country
	my $desc = "";       # description
	my $line;            # SIRS input line
	my $strbeg;          # string begin
	my $strend;          # string end

	$line = $_[0];
	$strbeg = index($line, "IDES=") + 5;
	if ($strbeg >= 5) {
		if (substr($line, $strbeg, 1) eq '"') {
			$strend = index($line, '"', $strbeg + 1);
			if ($strend > $strbeg) {
				$desc = substr($line, $strbeg + 1, $strend - $strbeg - 1);
			}
		}
		else {
			$strend = index($line, ',', $strbeg + 1);
			if ($strend > $strbeg) {
				$desc = substr($line, $strbeg, $strend - $strbeg);
			}
		}
	}

	$strbeg = index($line, "CTRI=") + 5;
	if ($strbeg >= 5) {
		if (substr($line, $strbeg, 1) eq '"') {
			$strend = index($line, '"', $strbeg + 1);
			if ($strend > $strbeg) {
				$ctry = substr($line, $strbeg + 1, $strend - $strbeg - 1);
			}
		}
		else {
			$strend = index($line, ',', $strbeg + 1);
			if ($strend < $strbeg) {
				$strend = length($line) - 1;
			}
			$ctry = substr($line, $strbeg, $strend - $strbeg);
		}
	}

	$_[1] = $desc;
	$_[2] = $ctry;
	1;
}

1;

sub Findsidesc($$$) {

	my $line;            # SIRS input line
	my $testid;          # test security ID from file

	my $fh_savfile = $_[0];      # filehandle of saved descriptions file
	my $secid = $_[1];           # security ID to search for

	# read a line from the save file as a starting point
	if ($line = <$fh_savfile>) {
		($testid, undef, undef) = split(/\s+/, $line);
	}
	else {
		# could not read line - rewind file
		seek($fh_savfile, 0, 0);
		$testid = "";
	}

	# if we are past the ID, rewind the file
	if ($testid gt $secid) {
		seek($fh_savfile, 0, 0);
		$testid = "";
	}

	# if we are not yet at the ID, read until we are at least there
	if    ($testid lt $secid) {
		while ($testid lt $secid) {
			if ($line = <$fh_savfile>) {
				($testid, undef, undef) = split(/\s+/, $line);
			}
			else {
				$testid = "";
				last;
			}
		}
	}

	# determine if we found the ID or not
	if ($testid ne $secid) {
		$_[2] = "";
		return 0;
	}

	# found the ID - return the full description line
	if (!defined($line)) { $line = ""; }    # avoid EOF problems
	chomp($line);
	$_[2] = $line;
	1;
}

1;
