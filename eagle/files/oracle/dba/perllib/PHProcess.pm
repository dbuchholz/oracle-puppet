package PHProcess;

=head1  PHProcess.pm

=head2  ABSTRACT:
		Provide PH custom edit routines - Current edits are:
		Edphcnam - Portfolio history category name

=head2  REVISIONS:
		RAM 11-MAY-04 13354-0  JGF Initial development

=cut

use strict;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edphcnam);

sub Edphcnam ($) {

	# takes an argument that is the total cateory name and sub category name
	# returns the sub category name if it is not null; otherwise returns the total category name
	
	my ($totc, $subc) = split(",", $_[0]);

	if ($subc ne "") {
		$_[0] = $subc;
	}
	else {
		$_[0] = $totc;
	}

	1;
}

1;
