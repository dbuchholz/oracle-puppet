package ITSFile;

=head1  ITSFile.pm

=head2  ABSTRACT:
		Provide standard data check routines - current routines are:
		ShortenFilename - Shortens a file name to three name tokens and a type token, eliminating extra date/time stamps

=head2  REVISIONS:
		RCR 22-MAY-06 13354-0  RCR Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ShortenFilename);

sub ShortenFilename ($) {

=head1 sub ShortenFilename ($)

=head2 ABSTRACT:
       Shortens a file name to three name tokens and a type token, eliminating extra date/time stamps
		
=head2 ARGUMENTS:
       0: Filename

=cut

	my $filename;     # the full returned filename
	my @tokens;       # array of tokens of the name

	# split name and check numbr of tokens
    @tokens = split(/_|\./, $_[0]);
	if ($#tokens < 3) {
		# not enough tokens - just return original name
		return $_[0];
	}

	# join name portion of 3 tokens with _ and join type with .
	$filename = join("_", (@tokens)[0, 1, 2]);
	$filename = join(".", $filename, $tokens[$#tokens]);
	return $filename;

	1;
}
