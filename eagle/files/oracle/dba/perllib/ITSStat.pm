package ITSStat;

=head1  ITSStat.pm

=head2  ABSTRACT:
		Provide standard statistics routines - current routines are:
		WriteStatistics   - Writes a file containing statistics and optionally copies it to another directory

=head2  REVISIONS:
		RAM 30-AUG-05 13354-0  RCR Changes quotewords to parse_line
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 26-MAY-05 13354-0  RCR Changed error print statements to go to both monitor and log file
		RAM 11-FEB-04 13354-0  RCR Revise file deletion after successful stat file copy
        RAM 09-FEB-04 13354-0  RCR Added addtional argument to ExecFTP call
		RAM 18-AUG-02 13354-0  JGF Initial development

=cut

use strict;
use ITSCheck;
use ITSCode;
use ITSFTP;
use ITSSystem;
use ITSTrim;
use Text::ParseWords;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(WriteStatistics);

sub WriteStatistics ($$\@\@$$$$$$$$\%$) {

=head1 sub WriteStatistics ($$\@\@$$$$$$$$\%$)

=head2 ABSTRACT:
       WriteStatistics   - Writes a file containing statistics and optionally copies it to another directory

=head2 ARGUMENTS:
        0: Name of loader
        1: Effective date
        2: Start date/time array
        3: End   date/time array
        4: Elapsed time (as string)
        5: Number of records input
        6: Number of records output
        7: Number of errors
        8: Number of files ok
        9: Number of files not ok
       10: Tables directory
       11: Log file directory
       12: Hash of values
       13: Log File Handle

=cut

	my $client_id;
	my $delete ="1";                # delete file if copy successful
	my $dir_stat;
	my $endstr;
	my $fullhost;
	my $hashref;
	my $hostname;
	my $idx;
	my $ipaddr;
	my $line;
	my $log_dir;
	my $login;
	my $maxtry = 3;
	my $node_stat;
	my $numtry;
	my $psw_stat;
	my $startstr;
	my $stat_dest_tab;
	my $stat_file_name;
	my $stat_file_name_full;
	my $tab_dir;
	my $type;
	my $user_stat;
	my @tokens;

	# calc dval
	my $dval = "";
	for (my $idx = 33; $idx <= 126; $idx+=7) {
		$dval .= chr($idx);
	}

	# get initial arguments for processing
	my $name    = $_[0];
	my $effdate = $_[1];
	my $startdt = $_[2];
	my $enddt   = $_[3];
        my $fh_logfile = $_[13];


	# adjust internal representation of months and years to normal terms
	$$startdt[4]++;
	$$enddt[4]++;
	$$startdt[5] += 1900;
	$$enddt[5]   += 1900;

	# construct start and end strings in yyyymmddhhmmss format
	for $idx (0 .. 4) {
		$$startdt[$idx] = sprintf("%02d", $$startdt[$idx]);
		$$enddt[$idx]   = sprintf("%02d", $$enddt[$idx]);
	}
	$startstr = join("", reverse(@$startdt));
	$endstr   = join("", reverse(@$enddt));

	# get node address and name
	GetHostInfo($ipaddr, $hostname, $fullhost);

	# set client id and mail destinations based on destination table
	$tab_dir = $_[10];
	$stat_dest_tab = $tab_dir . "stats_destination.tab";
	CheckDirectory($tab_dir, $client_id);

	if (!open(STATTAB, "< $stat_dest_tab")) {
		print $fh_logfile "\nERROR: could not open statistics table: $stat_dest_tab\n";
		print             "\nERROR: could not open statistics table: $stat_dest_tab\n";
		return 1;
	}

	$node_stat = "";
	$user_stat = "";
	$psw_stat = "";
	$dir_stat = "";

	while ($line = <STATTAB>) {
		
		chomp($line);
		TrimString($line);
		@tokens = parse_line('\s+', 0, $line);

		if ($tokens[0] eq "WRITE_NODE_STAT")   { $node_stat = $tokens[1]; }
		if ($tokens[0] eq "WRITE_USER_STAT")   { $user_stat = $tokens[1]; }
		if ($tokens[0] eq "WRITE_PSW_STAT")    { $psw_stat  = $tokens[1]; }
		if ($tokens[0] eq "WRITE_DIR_STAT")    { $dir_stat  = $tokens[1]; }
	}
	close(STATTAB);
	DecodeData($dval, $psw_stat);
	DecodeData($dval, $psw_stat);

	# get account login
	$login = getlogin() || (getpwuid($<))[0] || "N/A";

	$log_dir = $_[11];
	$stat_file_name = join("_", $name, $effdate, $client_id, $startstr) . ".sta";
	$stat_file_name_full = $log_dir . $stat_file_name;

	# open statistics file and write out
	if (!open(STATFILE, "> $stat_file_name_full")) {
		print $fh_logfile "\nERROR: open of statistics file: $stat_file_name_full\n";
		print             "\nERROR: open of statistics file: $stat_file_name_full\n";
		return 0;
	}
	print STATFILE "CLIENT=$client_id\n";
	print STATFILE "NAME=$name\n";
	print STATFILE "IPADDR=$ipaddr\n";
	print STATFILE "HOSTNAME=$hostname\n";
	print STATFILE "FULLHOST=$fullhost\n";
	print STATFILE "ACCOUNT=$login\n";
	print STATFILE "EFFDATE=$effdate\n";
	print STATFILE "STARTDT=$startstr\n";
	print STATFILE "ENDDT=$endstr\n";
	print STATFILE "ELAPSED=$_[4]\n";
	print STATFILE "RECIN=$_[5]\n";
	print STATFILE "RECOUT=$_[6]\n";
	print STATFILE "ERRORS=$_[7]\n";
	print STATFILE "FILESOK=$_[8]\n";
	print STATFILE "FILESNOTOK=$_[9]\n";

	# put out all elements of hash
	$hashref = $_[12];
	foreach $type (sort keys %$hashref) {
		print STATFILE "\"$type\"=$$hashref{$type}\n";
	}
	close(STATFILE);

	# if we have a node and a directory to push the file to, try to copy it there
	TRYLOOP: for $numtry (1 .. $maxtry) {
		if (ExecFTP("put", $stat_file_name, $node_stat, $user_stat, $psw_stat, "ascii", 
					$log_dir, $dir_stat, $delete, $fh_logfile, "")) {
			# file copy was successful
			print $fh_logfile "\nStatistics file: $stat_file_name\nWritten to: $node_stat $dir_stat\n";
			print             "\nStatistics file: $stat_file_name\nWritten to: $node_stat $dir_stat\n";
			last TRYLOOP;
		}
		else {
			if ($numtry < $maxtry) {
				# on initial tries, sleep a minute and try again
				print $fh_logfile "Copy of $stat_file_name_full\nTo: $node_stat $dir_stat failed - will retry\n";
				print             "Copy of $stat_file_name_full\nTo: $node_stat $dir_stat failed - will retry\n";
				sleep 60;
			}
			else {
				# not successful on maxtry, so issue failure
				print $fh_logfile "\nERROR: ftp of statistics file: $stat_file_name_full\nTo: $node_stat $dir_stat failed\n";
				print             "\nERROR: ftp of statistics file: $stat_file_name_full\nTo: $node_stat $dir_stat failed\n";
			}
		}
	}
	1;
}
