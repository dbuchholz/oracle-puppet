package IDSIProcess;

=head1  IDSIProcess.pm

=head2  ABSTRACT:
		Provide IDSI custom edit routines - Current edits are:
		Edidcai  - IDSI corporate action ID
		Edidoba  - Takes an argument that contains IDSI Options bid/ask detail and returns the mean price

=head2  REVISIONS:
		RCR 27-AUG-07 13354-0  RCR Added Edidoba
		RAM 18-DEC-03 13354-0  JGF Initial development

=cut

use strict;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edidcai Edidoba);

my $alphabet;

INIT {
	$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
}

sub Edidcai ($) {

	# takes an argument that is the IDSI Announcement Sequence Code 
	# and returns a corporate action ID
	
	my $idsiasc = $_[0];       # IDSI Announcement Sequence Code
	my $index1;                # index into alphabet for first character
	my $index2;                # index into alphabet for second character
	my $starcai;               # star corporate action ID
	my $year;                  # year of announcement (yy)

	if ($idsiasc eq "" || $idsiasc eq "    ") {
		$starcai = "";
	}
	else {
		$index1 = index($alphabet, substr($idsiasc, 0, 1));
		$index2 = index($alphabet, substr($idsiasc, 1, 1));
		$year = substr($idsiasc, 2, 2);
		if ($index1 < 0 || $index2 < 0 || $year < 1) {
			$starcai = "";
		}
		else {
			$starcai = 1000 * $year + (26 * $index1) + $index2 + 1;
		}
	}

	$_[0] = $starcai;

1;
}

sub Edidoba ($) {

	# takes an argument that contains IDSI Options bid/ask detail and returns the mean price
	
	my $idsiasc = $_[0];       # IDSI Options bid/ask detail
	$_[0] = "";                # set default in case processing fails

	my $optprcd = substr($idsiasc,  0, 1);     # option price code

	my $ask     = substr($idsiasc,  1, 1) . substr($idsiasc,  9, 7);     # ask value
	my $bid     = substr($idsiasc,  2, 1) . substr($idsiasc, 16, 7);     # bid value

	# don't process if bid and ask are all zeros
	if ($bid eq "00000000" && $ask eq "00000000") { return 0; } 

	# adjust the bid by the options price code - do nothing on a code of 0 or 3
	substr($bid, 5, 0) = ".";   # put in the decimal point
	if ($optprcd eq "2") {
		$bid = $bid / 100.0;    # price per 100 shares
	}
	if ($optprcd eq "9") {
		$bid = $bid * 100.0;    # price in hundredths of a cent per unit
	}

	if ($ask eq "00000000") { 
		# ask is 0 - return the bid
		$_[0] = $bid;
		return 1;
	} 

	# adjust the ask by the options price code - do nothing on a code of 0 or 3
	substr($ask, 5, 0) = ".";   # put in the decimal point
	if ($optprcd eq "2") {
		$ask = $ask / 100.0;    # price per 100 shares
	}
	if ($optprcd eq "9") {
		$ask = $ask * 100.0;    # price in hundredths of a cent per unit
	}

	if ($bid > $ask) { return 0; }     # reject if bid is greater than ask

	$_[0] = ($bid + $ask) * 0.5;       # calculate mean of bid and ask

1;
}

1;
