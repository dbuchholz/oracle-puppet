package PricingTable;

=head1  PricingTable.pm

=head2  ABSTRACT:
		Package used to create a hash containing the information from the pricing field definition file.

=head2  ARGUMENTS:
		table_file     The name of the file containing the pricing field definitions

=head2  REVISIONS:
		RAM 30-AUG-05 13354-0  RCR Changed quotewords to parse_line
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 10-JUN-01 13354-0  JGF Initial Development

=cut

use strict;

use ITSFatal;
use Text::ParseWords;

use vars qw(@ISA @EXPORT %FormatRecordHash);
use Exporter;
@ISA= ('Exporter');
@EXPORT= qw(CreatePriceDefHash %FormatRecordHash);

sub CreatePriceDefHash ($$$) {
	my $InRecordType;
	my @AofRecordDefs;

	# Get parameters
	my $fh_logfile   = $_[0];
	my $PROGRAM_NAME = $_[1];
	my $table_file   = $_[2];

	# Open file or end if file can't be opened
	use Fcntl;
	open (Pricing_Table, "< $table_file") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "Could not open table file: $table_file", "VAULT", "ENS DIE");

	my $CurrentRecordType = "";

	# Loop processing each record in the file
	while (my $InputLine = <Pricing_Table>) {
		chomp ($InputLine);

		# Split input line into array of fields
		my @fields = parse_line(" ", 0, $InputLine);
		$InRecordType = shift(@fields);

		# If new record type encountered, process previous record if any and reinitialize array
		if ($InRecordType ne $CurrentRecordType) {
			# If previous record not null, this is not the first record so push entry on array
			if ($CurrentRecordType ne "") {
				push (@{$FormatRecordHash{$CurrentRecordType}}, @AofRecordDefs);
			}
			@AofRecordDefs = ();
			$CurrentRecordType = $InRecordType;
		}

		# push record onto the array for the current record type array
		push @AofRecordDefs, [@fields];
	}

	# Push entry for last record type onto the hash
	push (@{$FormatRecordHash{$InRecordType}}, @AofRecordDefs);
	close (Pricing_Table);
	1;
}
