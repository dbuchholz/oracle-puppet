package ExshareProcess;

=head1  ExshareProcess.pm

=head2  ABSTRACT:
		Get currency definitions and enable processing of Exshare values - current routines are:
		CreateExshareCurrency - Initializes the currencies array for use by other routines
		DecodeAmendDate       - Decodes the 3 digit Exshare amend date into yyyymmdd form
		Edexshdt              - Decodes the 3 digit Exshare amend date  and the 7 digit volume date and returns the latest
		GetExshareValue       - Given an FCVALUE, returns the currency and the data value

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 23-JUL-02 13354-0  JGF Initial development

=cut

use strict;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CreateExshareCurrency DecodeAmendDate Edexshdt GetExshareValue);

# program name constant
my $PROGRAM_NAME = "VAULTGP";

# variables declared here to be static across the routines
my $amend_today;          # the amend date of today
my $baseyear;             # base date of two year amend cycle
my $num_curr;             # number of currencies defined
my $thisday;              # the day of today
my $thismonth;            # the month of today
my $thisyear;             # the year of today
my @currencies;           # array of currencies

sub CreateExshareCurrency ($$) {

=head1 sub CreateExshareCurrency ($$)

=head2 ABSTRACT:
	   Populates currencies array from the currency table

=head2 ARGUMENTS:
	   0: File handle of log file
	   1: Name of the currencies table

=head2 OUTPUT:
	   @currencies     array   global   [currno] = [isocode, numdp, cents]

=cut

	use strict;
	use ITSCheck;
	use ITSError;
	use ITSFatal;
	use ITSTrim;
	use Text::ParseWords;

	my $fh_logfile = $_[0];
	my $table_name = $_[1];
	open(TABLE,   "< $table_name")  || NotifyFatal($fh_logfile, $PROGRAM_NAME, "Could not open $table_name", "VAULT", "ENS DIE");

	my $num_line	= -1;      # lines in input file - first line will be 0 to match currency number
	my $num_error   = 0;       # errors encountered
	my $numtok;                # number of tokens on input line
	my @tokens;                # line tokens array

	#
	# read each line in the table searching for this loader
	GETRECORD: while (my $line = <TABLE>) {
		$num_line++;
		TrimLine($line);                   # trim comments, leading and trailing blanks
		if ($line ne "") {

			# get tokens from table line
			@tokens = &parse_line('\s+', 0, $line);
			$numtok = scalar(@tokens);

			# parse tokens checking what we can for validity
			if    ($numtok != 4) {
					TokenError($num_line, $line, "There are $numtok tokens - must be 4", $num_error);
			}
			elsif ($tokens[0] != $num_line) {
				TokenError($num_line, $line, "Currency number $tokens[0] does not match position", $num_error);
			}
			elsif (length($tokens[2]) != 1 || $tokens[2] !~ m/[0-8]|f|-/) {
				TokenError($num_line, $line, "Decimal places $tokens[2] invalid - must be 0-8,f,-", $num_error);
			}
			elsif (length($tokens[3]) != 1 || $tokens[3] !~ m/0|1/) {
				TokenError($num_line, $line, "Cents flag $tokens[3] invalid - must be 0 or 1", $num_error);
			}
			else {
				# have valid currency definition - add to main array
				push @currencies, [ $tokens[1], $tokens[2], $tokens[3] ];
			}
		}
	}

	close(TABLE);
	$num_curr = $#currencies;        # save number of currencies to make it easier later

	if ($num_curr < 0 || $num_error > 0) {
		# don't have any currencies or had an error - must die
		printf "\n*** Number of currencies: $num_curr   Errors Encountered: $num_error ***\n"; 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "Currency table could not be initialized", "VAULT", "ENS DIE");
	}
	1;
}


sub DecodeAmendDate ($$) {

=head1 sub DecodeAmendDate ($$)

=head2 ABSTRACT:
       Decodes into yyyymmdd format a 3 digit Exshare amend date
       The amend date is the number of days since the beginning of the last even year
       For example, December 31, 2001 is 731 as an amend date
                    January 31, 2002  is 031 as an amend date

=head2 ARGUMENTS:
	   0: Input Exshare amend date
	   1: Output yyyymmdd date

=cut

	use integer;
	use strict;
	use ITSDate;

	my $amend;
	my $startyear;
	my $year;
	my $month;
	my $day;

	# if not yet done, must initialize today's amend date and the base date of the cycle
	# base date will be the last day of the year before the cycle begins
	if (!defined($baseyear)) {
		($thisday, $thismonth, $thisyear) = (localtime)[3,4,5];
		$thisyear += 1900;
		$thismonth++;
		$baseyear = $thisyear - ($thisyear % 2) - 1;
		CalcDateT2X7($baseyear, 12, 31, $thisyear, $thismonth, $thisday, $amend_today);
	}


	# get amend date and check for validity
	$amend = $_[0];
	if ($amend =~ m/[^0-9]/ || $amend < 1 || $amend > 731) { 
		return 0;                                                            # leave amend day as is and return false 
	}

	$startyear = $baseyear;
    # if amend date is much greater than current amend date, must be last year
	if ($amend > ($amend_today + 365)) { $startyear = $startyear - 2; }
	# set date as amend days from base date
	CalcDateX2T7($startyear, 12, 31, $amend, $year, $month, $day); 
	$_[1] = sprintf("%04d%02d%02d", $year, $month, $day);

	1;
}

sub Edexshdt ($) {

=head1 sub Edexshdt ($)

=head2 ABSTRACT:
       Decodes a 3 digit Exshare amend date and a 7 digit volume date and returns the latest

=head2 ARGUMENTS:
	   0: Input 10 character Exshare amend date and volume date; Output yyyymmdd date

=cut

	use integer;
	use strict;

	my $amend_date;       # date decoded from amend date
	my $instring;         # input string of 10 characters
	my $latest;           # the latest date
	my $volume_date;      # date decoded from volume date

	# get the amend date and the volume date
	$instring = $_[0];
	if (!DecodeAmendDate(substr($instring, 0, 3), $amend_date))  { $amend_date = 0; }
	$volume_date = substr($instring, 3, 7);
	if (!CheckDate7ZZ($volume_date)) { $volume_date = 0; }
	if ($volume_date eq "") { $volume_date = 0; }

	# get the later of the two to return, and see if it's a date
	$latest = $amend_date > $volume_date ? $amend_date : $volume_date;
	$_[0] = $latest;
	return CheckDate($latest);
	1;
}

sub GetExshareValue ($$$) {

=head1 sub GetExshareValue ($$$)

=head2 ABSTRACT:
	   Given an Exshare FCVALUE, returns the currency and the data value

=head2 ARGUMENTS:
	   0: input 11 character Exshare FCVALUE
	   1: returned ISO currency code
	   2: returned data value

=cut

	my $cents;          # cents flag
	my $curcod;         # the numeric currency code from the feed
	my $curr;           # the alpha currency value
	my $denom;          # denominator for frational processing
	my $lenint;         # length of integer portion of value
	my $numer;          # numerator for frational processing
	my $numdp;          # number of decimal places
	my $rawval;         # the raw value from the feed
	my $result;         # the resulting data value
	my $whole;          # whole portion of number

	# set both output arguments to null as a default so we can return out if a failure occurs
	$_[1] = "";
	$_[2] = "";           

	# check to see that field is all digits
	if ($_[0] =~ m/\D/) {
		return 0;            # signal bad value
	}

	# get currency and raw value
	$curcod = substr($_[0], 0, 3);
	$rawval = substr($_[0], 3, 8);

	# check for NA value of 999 in currency field
	if ($curcod eq "999") {
		return 1;            # signal value ok and return nulls
	}

	# check currency - must be between 0 and max in table
	if ($curcod < 0 || $curcod > $num_curr) {
		return 0;            # signal bad value
	}

	# get parameters for this currency 
	($curr, $numdp, $cents) = @{ $currencies[$curcod] };

	if    ($numdp eq "-") {
		return 0;            # signal bad value
	}
	elsif ($numdp eq "f") {
		$whole = substr($rawval, 0, 4);
		$numer = substr($rawval, 4, 2);
		$denom = substr($rawval, 6, 2);
		if ($denom == 0) {
			$result = $whole;
		}
		else {
			$result = $whole + ($numer / $denom);
		}
	}
	else {
		# process by number of decimal places - put in decimal point between integer and fractional portions
		$lenint = 8 - $numdp;                   # compute length of integer part
		$result = join(".", substr($rawval, 0, $lenint), substr($rawval, $lenint, $numdp) );
	}

	# Check the result to make sure it's a number and get rid of leading/trailing zeros
	if (!CheckNumber($result)) {
		return 0;            # signal bad value
	}

	# have valid result - check for special scaling
	if ($curcod eq "627" || $curcod eq "628") {
		$result *= 1000.0;
	}

	# have valid price - check for pence or cents type price
	if ($cents) {
		$result *= 0.01;
	}

	# put the results in the output arguments and give successful return
	if ($curr ne "%" && $curr ne "I") {      # only return if an ISO code, not these values
		$_[1] = $curr;
	}
	$_[2] = $result;
	return 1;
}
1;
