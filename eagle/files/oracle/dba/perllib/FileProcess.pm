package FileProcess;

=head1  FileProcess.pm

=head2  ABSTRACT:
		Performs basic file operations - current routines are:
		SortFileNodup        - Sorts a file and eliminates duplicates

=head2  REVISIONS:
		RCR 06-JUL-07 13354-0  RCR Added SortFile
		JML 27-JUL-06 13354-0  JML Initial development

=cut

use strict;
use ITSCheck;
use ITSTrim;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(SortFile SortFileNodup);

sub SortFile($$$$) {

=head1 sub SortFile($$$$) 

=head2 ABSTRACT:
	   Will sort a file

=head2 ARGUMENTS:
	   0: Name of the input unsorted file
	   1: Name of the output sorted file
	   2: Number of headers in the output file (unused)
	   3: Number of records in the output file

=cut

# SCALARS
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $num_recs	 = 0;      # records in input file
my $out_file_name;         # name of output file
my $status;                # the status of the file opens

$_[3] = 0;                 # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
$out_file_name = $_[1];

$status = system ("sort $inp_file_name  > $out_file_name");


$status = open(OUTFILE, "< $out_file_name");
if (!$status) { return 0; }

# loop reading a record
GETRECORD: while ($line = <OUTFILE>) {
	chomp($line);                           # get rid of \n
	$num_recs++;

}
close OUTFILE;

$_[3] = $num_recs;                 # set number of output records

1;
}

sub SortFileNodup($$$$) {

=head1 sub SortFileNodup($$$$) 

=head2 ABSTRACT:
	   Will sort a file by the first field and remove any records where that first record is duplicated

=head2 ARGUMENTS:
	   0: Name of the input unsorted file
	   1: Name of the output sorted file
	   2: Number of headers in the output file
	   3: Number of records in the output file

=cut

# SCALARS
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $num_recs	 = 0;      # records in input file
my $out_file_name;         # name of output file
my $status;                # the status of the file opens

$_[3] = 0;                        # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
#$status = open(INFILE, "< $inp_file_name");
#if (!$status) { return 0; } 

$out_file_name = $_[1];

$status = system ("sort -u -k 1.1,1.9 $inp_file_name  > $out_file_name");


$status = open(OUTFILE, "< $out_file_name");
if (!$status) { return 0; }

# loop reading a record
GETRECORD: while ($line = <OUTFILE>) {
	chomp($line);                           # get rid of \n
	$num_recs++;

}

$_[3] = $num_recs;                 # set number of output records

# close files
#close INFILE;
close OUTFILE;

1;
}
