package RatingsProcess;

=head1  RatingsProcess.pm

=head2  ABSTRACT:
		Provide special processing for ratings - current routines are:
		AppendRatings           - Appends the RT1 file to the RTG file

=head2  REVISIONS:
		RAM 14-JUN-05 13354-0  RCR Corrected strays
		RAM 22-OCT-02 13354-0  JGF Initial development

=cut

use strict;

use ITSCheck;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(AppendRatings);

sub AppendRatings($$$) {

=head1 sub AppendRatings($$$) 

=head2 ABSTRACT:
	   Appends the RT1 file to the RTG file

=head2 REVISIONS:
	   RCR 20-DEC-02 13354-0  RCR Initial development

=head2 ARGUMENTS:
	   0: Name of the file to append to the RTG file
	   1: Name of the RTG file
	   2: Number of records in the output file

=cut

# SCALARS
my $inp_file_name;         # name of input file
my $line;                  # input line from the feed file
my $num_outs	 = 0;      # records in output file
my $num_recs	 = 0;      # records in input file
my $out_hdr;               # number of headers in output file
my $out_file_name;         # name of output file
my $status;                # the status of the file opens

$out_hdr = $_[2];
$_[3] = 0;                        # set number of output records to 0 in case things go wrong

# open input and output files
$inp_file_name = $_[0];
$status = open(INFILE, "< $inp_file_name");
if (!$status) { return 0; } 

$out_file_name = $_[1];
$status = open(OUTFILE, ">> $out_file_name");           # open RTG file with append
if (!$status) { return 0; } 

# get rid of the header record if necessary
while ($num_recs < $out_hdr) {
	$line = <INFILE>;
	$num_recs++;
}

# loop reading a record and writing it to the RTG file
while ($line = <INFILE>) {
	$num_recs++;
	print OUTFILE "$line";
	$num_outs++;
}

$_[3] = $num_outs;                 # set number of output records

# close files
close INFILE;
close OUTFILE;

1;
}
