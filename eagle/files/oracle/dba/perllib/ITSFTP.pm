package ITSFTP;

=head1  ITSFTP.pm

=head2  ABSTRACT:
		Provide standard ftp access routines - current routines are:
		DistributeClient - distributes a file to a list of client destinations provided in a table
		DistributeFiles - distributes a file to a list of accounts provided in a table
		ExecFTP - provides standard FTP functions

=head2  REVISIONS:
        RAM 15-NOV-06 13354-0  RCR Added load_name to copy failure messages
        RAM 29-AUG-05 13354-0  RCR Added ipaddr to copy failure messages - changed quotewords to parse_line
        RAM 05-JAN-05 13354-0  RCR Added sendmail option to DistributeClient
        RAM 05-JAN-05 13354-0  RCR Added delete operation to ExecFTP
        RAM 25-JUN-04 13354-0  RCR Added getnew operation to ExecFTP
        RAM 21-JUN-04 13354-0  RCR Added DistributeClient
		RAM 01-JUN-04 13354-0  RCR Changed to print copy messages to monitor as well as log file
		RAM 27-MAY-04 13354-0  RCR Changed itsww to eagleaccess
		RAM 01-APR-04 13354-0  RCR Changed for exact match on client name
        RAM 10-MAR-04 13354-0  RCR Changed to use quotewords when reading distribution table
        RAM 09-FEB-04 13354-0  RCR Added rename and putren operatons
		RAM 02-FEB-04 13354-0  RCR Added check of input clients for list
		RAM 16-DEC-03 13354-0  JGF Added wait and retry of FTP of file on failure
		RAM 14-OCT-03 13354-0  JGF Added status check on cwd operation
		RAM 06-OCT-03 13354-0  JGF Added $ftp->quit calls to clean up ftp session
		RAM 25-SEP-03 13354-0  JGF Combined ITSFTPNT and ITSFTP to be 1 portable routine
		RAM 25-SEP-02 13354-0  JGF Initial development

=cut

use strict;
use English;
use File::Basename;
use ITSCode;
use ITSFatal;
use Net::FTP;
use Text::ParseWords;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(DistributeClient DistributeFiles ExecFTP);

sub DistributeClient (*$$$$$$$$$$) {

=head1  sub DistributeClient (*$$$$$$$$$$)

=head2  ABSTRACT:
		distributes files out of ClientLoad

=head2  ARGUMENTS:
		0: filehandle of LOGFILE
		1: program name of caller
		2: name of list to notify on failure
		3: tables file directory
		4: output file directory
		5: load date
		6: file key
		7: file name
		8: client
		9: number sent ok   
		10: number sent not ok

=cut

	my $client_name;            # descriptive name of client
	my $ipaddr;                 # IP address to distribute to
	my $line;                   # line from distribution table
	my $linerec;                # record from data file
	my $locsub;                 # location of substitution parameter
	my $maxtry = 3;             # maximum number of tries to FTP this file
	my $numnotok = 0;           # number of file copies not ok
	my $numok = 0;              # number of file copies ok
	my $numtry;                 # number of tries to FTP this file
	my $operation;              # operation (put or putren) for FTP
	my $pvalue;                 # computed password value
	my $remdir;                 # optional remote node directory
	my $remname;                # optional remote rename name
	my $table_key;              # key on this table record
	my $table_name;             # name of the loader distribution table
	my $username;               # user name on remote node

	my @array;                  # temporary array for parse_line use

    # calc dval
	my $dval = "";
	for (my $idx = 33; $idx <= 126; $idx+=7) {
		$dval .= chr($idx);
	}

	my $fh_logfile   = $_[0];     # file handle of log file
	my $program_name = $_[1];     # name of calling program
	my $list_name    = $_[2];     # name of list to notify on failure
	my $tables_dir   = $_[3];     # directory where the tables are on this node
	my $local_dir    = $_[4];     # directory where the output files are on this node
	my $load_date    = $_[5];     # load date
	my $file_key     = $_[6];     # load date
	my $file_name    = $_[7];     # name of file to be distributed
	my $client       = $_[8];     # name of client

	my $keep = "0";

	$table_name = $tables_dir . $client . "_distribution.tab";

	if (open(TABLE, "< $table_name")) {

		while ($line = <TABLE>) {
			chomp ($line);

			# Split input line into array of fields
			($table_key, $client_name, $ipaddr, $username, $pvalue, $remdir, $remname) = @array = parse_line('\s+', 0, $line);

			if ($table_key eq $file_key) {
				# table key matches file key
				if ($ipaddr eq "sendmail") {

					# send file in mail
					# set up name
					my $local_name = $local_dir . $file_name;
					# substitute &records in new file name template if present
					$locsub = index($pvalue, '&records');
					if ($locsub >= 0) {
						if (open(DATAFILE, "< $local_name")) {
							my $numrec = 0;
							while ($linerec = <DATAFILE>) {
								$numrec++;
							}
							substr($pvalue, $locsub, 8) = sprintf("(%d record%s)", $numrec, $numrec == 1 ? "" : "s");
							close(DATAFILE);
						}
					}

					# mail the file - using the NotifyReport subroutine
					NotifyReport($fh_logfile, $pvalue, $local_name, $username);
					# mailing file was successful - we hope - no way to get back rejection
					print $fh_logfile "Mail of $file_key file to client: $client_name was successful\n";
					print             "Mail of $file_key file to client: $client_name was successful\n";
					$numok++;                # increment number ok
				}
				else {

					# FTP file to destination
					if (!defined($remdir))  { $remdir  = ""; }      # set directory to null if not provided
					if (!defined($remname)) { $remname = ""; }      # set rename name to null if not provided
					if ($remname eq "") {
						$operation = "put";
					}
					else {
						$operation = "putnew";
						# substitute date in new file name template if present
						$locsub = index($remname, '&date');
						if ($locsub >= 0) {
							substr($remname, $locsub, 5) = $load_date;
						}
					}
					DecodeData($dval, $pvalue);
					DecodeData($dval, $pvalue);
					TRYLOOP: for $numtry (1 .. $maxtry) {
						if (ExecFTP($operation, $file_name, $ipaddr, $username, $pvalue, "ascii", $local_dir, $remdir, 
									 $keep, $fh_logfile, $remname)) {
							# file copy was successful
							print $fh_logfile "Feed: $file_name  Copy of $file_key file to client: $client_name was successful\n";
							print             "Feed: $file_name  Copy of $file_key file to client: $client_name was successful\n";
							$numok++;                # increment number ok
							last TRYLOOP;
						}
						else {
							# file copy was not successful
							if ($numtry < $maxtry) {
								# on initial tries, sleep a minute and try again
								print $fh_logfile 
									"Feed: $file_name  Copy of $file_key file to client: $client_name failed - will retry\n";
								print             
									"Feed: $file_name  Copy of $file_key file to client: $client_name failed - will retry\n";
								sleep 60;
							}
							else {
								# not successful on maxtry, so issue failure
								NotifyFatal($fh_logfile, $program_name, 
									"Feed: $file_name  Copy of $file_key file to client: $client_name ($ipaddr) failed", 
									$list_name, "ENS");
								$numnotok++;             # increment number not ok
							}
						}
					}
				}
			}
		}
		close(TABLE);
	}
	else {
		print $fh_logfile "*** WARNING *** $table_name not found - no files distributed\n";
		print             "*** WARNING *** $table_name not found - no files distributed\n";
	}

	$_[9] = $numok;              # return number ok
	$_[10] = $numnotok;          # return number not ok
	1;
}

sub DistributeFiles (*$$$$$$$$$$$) {

=head1  sub DistributeFiles (*$$$$$$$$$$$)

=head2  ABSTRACT:
		distributes files according to a table

=head2  ARGUMENTS:
		0: filehandle of LOGFILE
		1: program name of caller
		2: name of list to notify on failure
		3: tables file directory
		4: output file directory
		5: loader name
		6: date
		7: file key
		8: client
		9: number sent ok   
		10: number sent not ok
		11: log file directory

=cut

	my $client_name;            # descriptive name of client
	my $file_name;              # file name of file to distribute
	my $ipaddr;                 # IP address to distribute to
	my $line;                   # line from distribution table
	my $locsub;                 # location of substitution parameter
	my $maxtry = 3;             # maximum number of tries to FTP this file
	my $numnotok = 0;           # number of file copies not ok
	my $numok = 0;              # number of file copies ok
	my $numtry;                 # number of tries to FTP this file
	my $operation;              # operation (put or putren) for FTP
	my $pvalue;                 # computed password value
	my $remdir;                 # optional remote node directory
	my $remname;                # optional remote rename name
	my $table_key;              # key on this table record
	my $table_name;             # name of the loader distribution table
	my $username;               # user name on remote node

	my @array;                  # temporary array for parse_line use

    # calc dval
	my $dval = "";
	for (my $idx = 33; $idx <= 126; $idx+=7) {
		$dval .= chr($idx);
	}

	my $fh_logfile   = $_[0];     # file handle of log file
	my $program_name = $_[1];     # name of calling program
	my $list_name    = $_[2];     # name of list to notify on failure
	my $tables_dir   = $_[3];     # directory where the tables are on this node
	my $local_dir    = $_[4];     # directory where the output files are on this node
	my $load_name    = $_[5];     # loader name
	my $load_date    = $_[6];     # load effective date
	my $file_key     = $_[7];     # key of file to be distributed
	my $client       = $_[8];     # name of client, list of clients, or "" (all)
#### not used	my $log_dir      = $_[11];    # directory where the output files are on this node

	my $keep = "0";

	$file_name = join("", $load_name, "_", $load_date, "_", $file_key, ".dat");
	$table_name = $tables_dir . $load_name . "_distribution.tab";

	if (open(TABLE, "< $table_name")) {

		while ($line = <TABLE>) {
			chomp ($line);

			# Split input line into array of fields
			($table_key, $client_name, $ipaddr, $username, $pvalue, $remdir, $remname) = @array = 
                                                                                    parse_line('\s+', 0, $line);

			if ($table_key eq $file_key) {
				# table key matches file key

				if (($client eq "") || ($client eq $client_name)) {
					# doing all clients or have client match - try to FTP file to indicated location
					if (!defined($remdir))  { $remdir  = ""; }      # set directory to null if not provided
					if (!defined($remname)) { $remname = ""; }      # set rename name to null if not provided
					if ($remname eq "") {
						$operation = "put";
					}
					else {
						$operation = "putnew";

						my ($sec, $min, $hour, $day, $month, $year) = (localtime)[0 .. 5];
						my $curdate = sprintf("%04d%02d%02d", $year + 1900, $month + 1, $day);
						my $curtime = sprintf("%02d%02d%02d", $hour, $min, $sec);

						# substitute date in new file name template if present
						$locsub = index($remname, '&date');
						if ($locsub >= 0) {
							substr($remname, $locsub, 5) = $curdate;
						}
						# substitute time in new file name template if present
						$locsub = index($remname, '&time');
						if ($locsub >= 0) {
							substr($remname, $locsub, 5) = $curtime;
						}
					}

					DecodeData($dval, $pvalue);
					DecodeData($dval, $pvalue);
					TRYLOOP: for $numtry (1 .. $maxtry) {
						if (ExecFTP($operation, $file_name, $ipaddr, $username, $pvalue, "ascii", $local_dir, $remdir, 
									 $keep, $fh_logfile, $remname)) {
							# file copy was successful
							print $fh_logfile "Feed: $load_name  Copy of $file_key file to client: $client_name was successful\n";
							print             "Feed: $load_name  Copy of $file_key file to client: $client_name was successful\n";
							$numok++;                # increment number ok
							last TRYLOOP;
						}
						else {
							# file copy was not successful
							if ($numtry < $maxtry) {
								# on initial tries, sleep a minute and try again
								print $fh_logfile 
									"Feed: $load_name  Copy of $file_key file to client: $client_name failed - will retry\n";
								print             
									"Feed: $load_name  Copy of $file_key file to client: $client_name failed - will retry\n";
								sleep 60;
							}
							else {
								# not successful on maxtry, so issue failure
								NotifyFatal($fh_logfile, $program_name, 
									"Feed: $load_name  Copy of $file_key file to client: $client_name ($ipaddr) failed", 
									$list_name, "ENS");
								$numnotok++;             # increment number not ok
							}
						}
					}
				}
			}
		}
		close(TABLE);
	}
	else {
		print $fh_logfile "*** WARNING *** $table_name not found - no files distributed\n";
		print             "*** WARNING *** $table_name not found - no files distributed\n";
	}

	$_[9]  = $numok;             # return number ok
	$_[10] = $numnotok;          # return number not ok
	1;
}


sub ExecFTP ($$$$$$$$$$$) {

=head1  sub ExecFTP ($$$$$$$$$$$)

=head2  ABSTRACT:
		performs FTP functions
		returns 1 if successful, 0 otherwise

=head2  ARGUMENTS:
		0: Operation          (required)
		1: File name          (required)
		2: Node name          (required)
		3: Account            (required)
		4: Value              (required)
		5: File type          (may be "" if not needed)
		6: Local directory    (may be "" if not needed)
		7: Remote directory   (may be "" if not needed)
		8: Log directory      (may be "" if not needed)
		9: Delete File        (may be "" if not needed)
		10: New File name     (may be "" if not needed)

=cut

	my $ftp;               # ftp identifier returned on create
    my $ftp_status;		   # Status of FTP calls
	my $idxret;            # Index - retry number
	my $retry_num = 1;     # Number of times to retry
	my $retry_wait = 2;    # Seconds to wait on retry

	# get and check initial arguments for processing
	my $operation  = $_[0];     # 0: Operation          (required)
	my $file_name  = $_[1];     # 1: File name          (required)
	my $node_name  = $_[2];     # 2: Node name          (required)
	my $account    = $_[3];     # 3: Account            (required)
	my $value      = $_[4];     # 4: Value              (required)
	my $file_type  = $_[5];     # 5: File type          (may be "" if not needed)
	my $local_dir  = $_[6];     # 6: Local directory    (may be "" if not needed)
	my $remote_dir = $_[7];     # 7: Remote directory   (may be "" if not needed)
	my $delete     = $_[8];     # 8: Indicate whether to delete file (may be "" if not needed)
	my $fh_logfile = $_[9];     # 9: Log File handle    (may be "" if not needed)
	my $new_file_name = $_[10]; #10: New file name      (may be "" if not needed)

	my $program_name = basename($0);

	if ($operation eq "") {return 0;}
	if ($file_name eq "") {return 0;}
	if ($node_name eq "") {return 0;}
	if ($account   eq "") {return 0;}
	if ($value     eq "") {return 0;}
	if ( ( ($operation eq "getnew") || ($operation eq "putnew") || ($operation eq "putren") || ($operation eq "rename") ) 
			&& $new_file_name eq "") {return 0;}

    # Connect to FTP Site
	for $idxret (0 .. $retry_num) {
		$ftp = Net::FTP->new($node_name);
		if ($ftp) { last; }
		sleep($retry_wait);
	}
	if(!$ftp) {
		NotifyFatal($fh_logfile, $program_name, "Could not connect to $node_name - $EVAL_ERROR", "VAULT", "");
		return 0;
	}

    # Attempt to login to FTP Site using account and value
	for $idxret (0 .. $retry_num) {
		$ftp_status = $ftp->login($account, $value);
		if ($ftp_status) { last; }
		sleep($retry_wait);
	}
	if(!$ftp_status) {
	    NotifyFatal($fh_logfile, $program_name, "Unable to login to FTP user: $account - $EVAL_ERROR", "VAULT", "");
		$ftp->quit;     # end ftp session and close socket
		return 0;
	}

	# Change transfer type if provided
	if (substr($file_type,0,3) eq "bin") {
	    $ftp->binary();
	}
	elsif ($file_type eq "ascii") {
		$ftp->ascii();
	}

	if ($remote_dir ne "") {
		$ftp_status = $ftp->cwd($remote_dir);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Unable to cwd in FTP to directory: $remote_dir", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
	}

	my $full_file_name = $local_dir . $file_name;
	if    ($operation eq "get") {
		$ftp_status = $ftp->get ($file_name, $full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Error in FTP GET: $file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			if ($delete eq "1") {
				$ftp->delete($file_name);
			}
		}
	}

	elsif ($operation eq "getnew") {
		$full_file_name = $local_dir . $new_file_name;
		$ftp_status = $ftp->get ($file_name, $full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Error in FTP GETNEW: $file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			if ($delete eq "1") {
				$ftp->delete($file_name);
			}
		}
	}

	elsif ($operation eq "put") {
		$ftp_status = $ftp->put ($full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Error in FTP PUT: $full_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			if ($delete eq "1") {
				unlink ($full_file_name);
			}
		}
	}

	elsif ($operation eq "putnew") {
		$ftp_status = $ftp->put ($full_file_name, $new_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, 
						"Error in FTP PUTNEW: $full_file_name $new_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			if ($delete eq "1") {
				unlink ($full_file_name);
			}
		}
	}

	elsif ($operation eq "putren") {
		$ftp_status = $ftp->put ($full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, 
						"Error in FTP PUTREN/PUT: $full_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			$ftp_status = $ftp->rename ($file_name, $new_file_name);
			if (!$ftp_status) {
				NotifyFatal($fh_logfile, $program_name, 
							"Error in FTP PUTREN/REN: $file_name $new_file_name node: $node_name", "VAULT", "");
				$ftp->quit;     # end ftp session and close socket
				return 0;
			}
			if ($delete eq "1") {
				unlink ($full_file_name);
			}
		}
	}

	elsif ($operation eq "rename") {
		$ftp_status = $ftp->rename ($file_name, $new_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, 
						"Error in FTP RENAME: $file_name $new_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
	}

	elsif ($operation eq "append") {
		$ftp_status = $ftp->append ($full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Error in FTP APPEND: $full_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
		else {
			if ($delete eq "1") {
				unlink ($full_file_name);
			}
		}
	}

	elsif ($operation eq "delete") {
		$ftp_status = $ftp->delete ($full_file_name);
		if (!$ftp_status) {
			NotifyFatal($fh_logfile, $program_name, "Error in FTP DELETE: $full_file_name node: $node_name", "VAULT", "");
			$ftp->quit;     # end ftp session and close socket
			return 0;
		}
	}

	else {
	    NotifyFatal($fh_logfile, $program_name, "Invalid FTP operation $operation", "VAULT", "");
		$ftp->quit;     # end ftp session and close socket
		return 0;
	}

	$ftp->quit;     # end ftp session and close socket
	1;
}
1;
