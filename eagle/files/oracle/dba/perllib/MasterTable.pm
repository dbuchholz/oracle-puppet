package MasterTable;

=head1  MasterTable.pm

=head2  ABSTRACT:
		Get loader parameters and record types for a loader - current routines are:
		CreateMasterArray - Initializes the master array @RecordTypes for a given loader
		GetOutdlm         - Returns the output delimiter
		GetRecordType     - Given a record, returns its type using the master array @RecordTypes already initialized
		GlobalEdit        - Performs global edits
		ProcessHeader     - Performs file header processing
		ProcessPost       - Calls file post processing routines if they exist
		ProcessTrailer    - Performs file trailer processing
		VerifyTrailer     - Verifies trailer was processed

=head2  REVISIONS:
		RCR 26-FEB-08 13354-0  RCR Added ExrateProcessFull
		RCR 12-NOV-07 13354-0  RCR Added ExrateProcessRaw
        RCR 14-MAR-07 13354-0  RCR Added FIXHEAD
        RCR 05-MAR-07 13354-0  RCR Added OUTDLM
        JML 26-JUL-06 13354-0  RCR Added FileProcess
        RCR 16-APR-06 13354-0  RCR Added EVMProcess
        RCR 28-FEB-06 13354-0  RCR Added HFLProcess
        RAM 30-AUG-05 13354-0  RCR Changed quotewords to parse_line
        RAM 27-JUN-05 13354-0  RCR Added check for number of tokens on line for LOAD record
        RAM 14-JUN-05 13354-0  RCR Corrected strays
        RAM 01-MAR-05 13354-0  RCR Added GlobalEdit
        RAM 08-FEB-05 13354-0  RCR Changed to handle secidadd for tv type files
        RAM 20-JUN-04 13354-0  RCR Added AppendProcess
		RAM 10-JUN-04 13354-0  RCR Added ExrateProcessHist
		RAM 03-JUN-04 13354-0  RCR Added ExfwdProcess
		RAM 20-APR-04 13354-0  RCR Added VerifyTrailer
		RAM 06-APR-04 13354-0  RCR Added ADDTIME flag
        RAM 31-JAN-04 13354-0  RCR Changed to use .dat file in post processing
		RAM 18-JUN-02 13354-0  JGF Initial development

=cut

use strict;

use AppendProcess;
use ChangesProcess;
use CustomEdit;
use EVMProcess;
use ExfwdProcess;
use ExrateProcess;
use ExrateProcessFull;
use ExrateProcessHist;
use ExrateProcessRaw;
use FileProcess;
use HFLProcess;
use ITSCheck;
use ITSEdit;
use ITSFatal;
use RMProcess;
use RatingsProcess;
use RawvalProcess;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CreateMasterArray GetFixhead GetOutdlm GetRecordType GlobalEdit ProcessHeader 
			ProcessPost ProcessTrailer VerifyTrailer);

# STATIC DECLARATIONS - variables that must be retained across the routines
my $flg_gloedit;          # is there at least one global edit?
my $flg_fixhead;          # is there a fixed header to be added?
my $flg_trailer;          # was trailer processed?
my $linetype; 
my $loader_type;          # type of loader
my $mode;                 # mode for file
my $nullrec;              # edit for null record type
my $num_hdr;              # number of header records
my $num_rectype;          # input file items defined

my $out_hdr;              # number of output headers
my $outdlm;               # output delimiter

my $rawlentmp;            # rawval string length temporary variable

my $secidadd;             # security id address
my $secidlen;             # security id length

my $testname;             # name from the table to test against loader_name
my $trailadd;             # trailer string address
my $traillen;             # trailer string length
my $trailstr;             # trailer string

my @EditTypes;            # array of global edits
my @RecordTypes;          # array of record types

my %PostTypes;            # hash of Post Process types

INIT {
	$flg_gloedit = 0;      # no global edits yet
	$flg_fixhead = 0;      # no fixed header yet
	$flg_trailer = 0;      # trailer has not been processed
	$outdlm = "|";         # default output delimiter is "|";
}

sub CreateMasterArray ($$$$$$$$) {

=head1  sub CreateMasterArray

=head2  ABSTRACT:
        creates an array of parameters for a loader from the master table: master_loadtype.tab
        
=head2  ARGUMENTS:
        0: program name
        1: name of the loader, for example, idsi320, exhdr
        2: full name of the master table
        3: returned loader type
		4: number of output headers
		5: address of security id
		6: override effective date flag
		7: add time to filenames flag

=cut

	use strict;
	use ITSError;
	use ITSTrim;
	use Text::ParseWords;

#### not used	my $program_name = $_[0];
	my $loader_name  = $_[1];
	my $table_name   = $_[2];

	# initialize types structures
	@EditTypes = ();
	@RecordTypes = ();
	%PostTypes = ();

	open(TABLE,   "< $table_name")  || return 0;

	my $addtime;               # addtime to filenames flag
	my $flg_finding = 1;       # are we in find mode?
	my $idxtok;
	my $num_line	= 0;       # lines in input file
	my $num_error   = 0;       # errors encountered
	my $numtok;                # number of tokens on input line
	my $overeff;               # override effective date flag
	my $subname;               # subroutine edit name to be checked
	my @tokens;                # line tokens array

	#
	# read each line in the table searching for this loader
	GETRECORD: while (my $line = <TABLE>) {
		$num_line++;
		TrimLine($line);                   # trim comments, leading and trailing blanks
		if ($line ne "") {

			# get tokens from table line
			@tokens = parse_line('\s+', 0, $line);
			$linetype = shift(@tokens);             # first thing on line is the type
			if ($flg_finding) {
				if ($linetype eq "&LOAD") {
					# get loader values if it's the loader we're looking for
					($testname, $loader_type, $secidadd, $secidlen, $nullrec, $out_hdr, $num_hdr, $trailadd, $trailstr, 
						$overeff, $addtime) = @tokens;
					if ($testname eq $loader_name) {
						$numtok = scalar(@tokens) + 1;         # add one because we shifted off a token above
						if ($numtok != 12) {
							TokenError($num_line, $line, "There are $numtok tokens - must be 12", $num_error);
						}
						# found the loader we want
						$flg_finding = 0;                 # set flag to tell us we have the right loader values
						SetRawvalMode($secidlen);         # set mode for file
						GetRawvalMode($mode);             # get mode for further processing
						if (substr($mode, 1, 2) ne "tv") {
							# made addresses zero based for address based loads
							$secidadd--;
							$trailadd--;
						}
						else {
							# make addresses -1 for tv files if 0
							if ($secidadd eq "0") { $secidadd--; }
							if ($trailadd eq "0") { $trailadd--; }
						}
						$traillen = length($trailstr);    # get length of trailer string
						$nullrec = ucfirst(lc($nullrec));
						$_[3] = $loader_type;
						$_[4] = $out_hdr;                 # return number of output headers
						$_[5] = $secidadd;                # return secid address (-1 for no security id)
						$_[6] = $overeff;                 # return override effective date flag
						$_[7] = $addtime;                 # return add time flag
					}
				}
			}
			else {
				# we know we are on the right loader, so process the other records for it
				if ($linetype eq "&LOAD") {
					last GETRECORD;       # if we have reached the next loader, we are done initializing this one
				}
				$numtok = scalar(@tokens);
				if    ($linetype eq "&REC") {
					if ($numtok < 3 || ($numtok % 3) != 1) {
						TokenError($num_line, $line, "There are $numtok tokens - must be 5,8,11,...", $num_error);
					}
					# subtract one from the addresses to correspond to zero based arrays
					for ($idxtok = 0; $idxtok < ($numtok - 1); $idxtok += 3) {
						$tokens[$idxtok]--;
					}
					# save record type
					push @RecordTypes, [@tokens];
				}
				elsif ($linetype eq "&POST") {
					if ($numtok != 3) {
						TokenError($num_line, $line, "There are $numtok tokens - must be 4", $num_error);
					}
					# check to see if processing routine is there
					$subname = $tokens[2];
					if (!defined(&$subname)) {
						TokenError($num_line, $line, "Processing routine $tokens[2] does not exist", $num_error);
					}
					# save post type
					push (@{$PostTypes{$tokens[0]}}, @tokens);
				}
				elsif ($linetype eq "&GLOEDIT") {
					if ($numtok != 2) {
						TokenError($num_line, $line, "There are $numtok tokens - must be 3", $num_error);
					}
					# save global edit
					push @EditTypes, [@tokens];
					$flg_gloedit = 1;                               # turn flag on for easier checking later
				}
				elsif ($linetype eq "&FIXHEAD") {
					if ($numtok != 0) {
						TokenError($num_line, $line, "There are $numtok tokens - must be 1", $num_error);
					}
					# set fixhead flag
					$flg_fixhead = 1;
				}
				elsif ($linetype eq "&OUTDLM") {
					if ($numtok != 1) {
						TokenError($num_line, $line, "There are $numtok tokens - must be 2", $num_error);
					}
					# save output delimiter
					$outdlm = $tokens[0];
				}
			}
		}
	}

	close(TABLE);
	$num_rectype = $#RecordTypes;        # save number of types to make it easier later

	if ($num_rectype < 0) {
		# don't have any record types defined
		return 0;
	}

	if ($num_error > 0) {
		# errors encountered
		return 0;
	}
	1;
}

sub GetRecordType ($$$$) {

=head1 sub GetRecordType ($$$$) {

=head2 ABSTRACT:
       finds the record type of a feed line
       assumes that CreateMasterArray has been called first

=head2 ARGUMENTS:
       0: line from feed
       1: returned record type
       2: returned security ID
       3: returned process flag

=cut

	my $feed_value;     # the value of the record marker from the feed
	my $idxtab;         # index into the record types table
	my $idxtok;         # index for tokens in the record types line
	my $numtok;         # the number of tokens
	my $result;         # the resulting record type
	my $value;          # the concatenated value of all the feed_values
	my @tokens;         # array of tokens representing one feed value record

	# check line content for null type record
	if ($nullrec ne "Null") {
		no strict 'refs';
		if (&$nullrec($_[0])) {
			# line was null by the edit criteria
			$_[1] = " ";     # record type blank
			$_[2] = " ";     # record type blank
			$_[3] = 0;       # do not process this record
			return 1;        # return valid
		}
	}

	InitRawval($_[0]);                    # set up record for GetRawval calls

	GETTYPE: for $idxtab (0 .. $num_rectype) {
		@tokens = @{ $RecordTypes[$idxtab] };
		$numtok = $#tokens;

		# step through tokens three at a time processing as we go
		# tokens are: 0: address  1: length  2; value ...       n: result or =
		$value = "";
		for ($idxtok = 0; $idxtok < $numtok; $idxtok += 3) {
			$rawlentmp = $tokens[$idxtok+1];           # pass as a tempoarary as GetRawval can change the second argument
			$feed_value = GetRawval($tokens[$idxtok], $rawlentmp);
			if ($tokens[$idxtok+2] ne "*") {
				if ($feed_value ne $tokens[$idxtok+2]) {
					next GETTYPE;                    # did not match value - move on to next type
				}
			}
			$value .= $feed_value;               # append feed_value to value
		}
		$result = $tokens[$numtok];
		if ($result eq "=") {
			$result = $value;
		}
		$_[1] = $result;                                    # return record type result in second argument
		$rawlentmp = $secidlen;                             # pass as a tempoarary as GetRawval can change the second argument
		if (!CheckNumber($rawlentmp)) { $rawlentmp = 0; }   # for secid, this might be cdfq or tvfq..., so zero out
		$_[2] = GetRawval($secidadd, $rawlentmp);           # return security ID in third argument
		$_[3] = 1;                                          # process this record
		return 1;
	}                   # end of GETTYPE loop

	# did not find a match - return a type of blank
	$_[1] = " ";
	$_[2] = " ";
	$_[3] = 0;              # do not process this record
	return 0;
}

sub GlobalEdit($) {

=head1 sub GlobalEdit($) {

=head2 ABSTRACT:

       Changes a field according to any global edits defined

=head2 ARGUMENTS:
       0: rawval - checked against global edits and modified if necessary

=cut

	# if flag set, process global edits
	if ($flg_gloedit) {

		my $idxedit;        # index into array of array of edits
		my $rawval;         # the raw data value to be checked
		my $result;         # result of edit if test matches
		my $test;           # test value for edit
		$rawval = $_[0];

		foreach $idxedit (0 .. $#EditTypes) {
			($test, $result) = @{$EditTypes[$idxedit]};
			if ($rawval eq $test) {
				$_[0] = $result;                 # return new value to argument
				last;                            # and skip out
			}
		}
	}
	1;                   
}

sub ProcessHeader ($$$$$*) {

=head1 sub ProcessHeader ($$$$$*) {

=head2 ABSTRACT:
       Performs loader header processing
       Skips header records not used
       Returns 1 unless there are no headers, in which case returns 0

=head2 ARGUMENTS:
       0: input feed record
       1: record type  - if headers exist, set to HDR
       2: security id  - if headers exist, set to HEADER
       3: process flag - if headers exist, set to 1
       4: number of headers to skip
       5: fh of input file

=cut

	my $num_skip; # number of headers to skip
	my $temp;     # temporary buffer for input line

	# if there are no headers, just return false
	if ($num_hdr <= 0) {
		return 0;
	}

	$_[1] = "HDR";
	$_[2] = "HEADER";
	$_[3] = 1;
	$num_skip = $num_hdr - 1;
	$_[4] = $num_skip;

	# skip header records not used
	if ($num_skip > 0) {
		my $fhinp = $_[5];
		for (1 .. $num_skip) {
			$temp = <$fhinp>;            # read a record from the input file to skip it
		}
	}
	1;
}


sub ProcessPost($$$$) {

=head1 sub ProcessPost($$$$) {

=head2 ABSTRACT:

       Performs file Post-Processing
       Returns 1 if no post processing for this key
       Otherwise, returns result from called routine
       Below we use the term suffix to mean the key concatenated with .dat - for example, F4H.dat

=head2 ARGUMENTS:
       0: key - file key designation, for example, F4H
       1: filename - full file name
       2: outkey - the new file output key, for example, EF4
       3: numout - returned number of output records generated

=cut

	my $key = $_[0];
	my $lenfn;          # the length of the file name
	my $lensf;          # the length of the file suffix
	my $newfn;          # constructed new file name
	my $newkey;         # the new file key
	my $oldkey;         # the old file key
	my $num_out;        # number of records put out by post process routine
	my $oldfn;          # the old file name passed in
	my $oldsf;          # the old file suffix
	my $post_routine;   # the name of the post process routine
	my $suffst;         # start of the file suffix
	my $status;         # return value from the called post process routine

	if (exists($PostTypes{ $key })) {
		# post type exists, try to process
		($oldkey, $newkey, $post_routine) = @{$PostTypes{ $key }};

		# check key against file name and construct new file name
		$oldfn = $_[1];
		$lenfn = length($oldfn);
		$oldsf = $oldkey . ".dat";
		$lensf = length($oldsf);
		$suffst = $lenfn - length($oldsf);
		if ($oldsf ne substr($oldfn, $suffst, $lensf)) {  
			# file name end did not match key specified
			return 0;
		}
		$newfn = join("", substr($oldfn, 0, $suffst), $newkey, ".dat");
		{
			# call the post processing routine
			no strict 'refs';
			$status = &$post_routine($oldfn, $newfn, $out_hdr, $num_out);
		}
		$_[2] = $newkey;
		$_[3] = $num_out;
		return $status;       # return result of called post process routine
	}
	# no post process for this key
	$_[2] = " ";              # return valid arguments
	$_[3] = 0;
	1;                        # if post process did not exist, things are fine, so just return success
}

sub ProcessTrailer ($$$$) {

=head1 sub ProcessTrailer ($$$$) {

=head2 ABSTRACT:
       Performs loader trailer processing
       Returns 1 if the record is a trailer - otherwise, returns 0

=head2 ARGUMENTS:
       0: input feed record
       1: record type  - if a trailer, set to TRL
       2: security id  - if a trailer, set to TRAILER
       3: process flag - if a trailer, set to 1

=cut

	# if trailer zero based address is -1, there are no trailers, so just return false
	if ($trailadd eq "-1") { return 0; }

	# check for trailer string in the line (use 0 based trailadd)
	$rawlentmp = $traillen;                    # pass as a tempoarary as GetRawval can change the second argument
	if (GetRawval($trailadd, $rawlentmp) ne $trailstr) {
		return 0;       # return false if record was not a trailer
	}
	# record is a trailer
	$_[1] = "TRL";
	$_[2] = "TRAILER";
	$_[3] = 1;
	$flg_trailer = 1;           # trailer has been processed
	1;
}
1;

sub VerifyTrailer () {

=head1 sub VerifyTrailer () {

=head2 ABSTRACT:
       Verifies that trailer was processed
       Returns 1 if no trailer was expected or if trailer was processed - otherwise, returns 0

=head2 ARGUMENTS:
       none
=cut

	# if trailer zero based address is -1, there are no trailers, so return true
	if ($trailadd eq "-1") { return 1; }

	# otherwise, return flg_trailer
	return $flg_trailer;
}

sub GetOutdlm () {

=head1 sub GetOutdlm () {

=head2 ABSTRACT:
       Returns the output delimiter

=head2 ARGUMENTS:
       none
=cut

	return $outdlm;
}

sub GetFixhead () {

=head1 sub GetFixhead () {

=head2 ABSTRACT:
       Returns the fixed header status

=head2 ARGUMENTS:
       none
=cut

	return $flg_fixhead;
}
1;
