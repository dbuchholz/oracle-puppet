package RMProcess;

=head1  RMProcess.pm

=head2  ABSTRACT:
		Provide Russell Mellon post processing - current routines are:
		Edwbcrt     - Special edit for country names
		PostHGprf   - Provides special month and quarter end processing for Historical Gross Returns
		PostRGprf   - Provides special month and quarter end processing for Current Gross Returns
		PostRMprf   - Provides special month and quarter end processing for rmprf
		PostRMqtr   - Provides special quarter end processing for rmprf
		PostWBprf   - Provides special month and quarter end processing for rm perf files from WorkBench

=head2  REVISIONS:
		RAM 22-FEB-07 13354-0  RCR Added PostRMqtr
		RAM 08-DEC-05 13354-0  RCR Added PostRGprf
		RAM 02-DEC-05 13354-0  RCR Added PostHGprf
		RAM 19-OCT-05 13354-0  RCR Added PostWBprf, Edwbccrt
		RAM 14-JUN-05 13354-0  RCR Initial development

=cut

use strict;
use integer;                  # for date calculation logic

use File::Copy;
use ITSCheck;
use ITSDate;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Edwbcrt PostHGprf PostRGprf PostRMprf PostRMqtr PostWBprf);

my @beg_dates;    # quarter begin dates for use by all routines
my @end_dates;    # quarter end   dates for use by all routines

INIT {
	@beg_dates = ("1231", "0331", "0630", "0930");
	@end_dates = ("0331", "0630", "0930", "1231");
}

sub Edwbcrt($) {

=head1 sub Edwbcrt($) 

=head2 ABSTRACT:
       Special edit for country names

=head2 ARGUMENTS:
	   0: Country name to be edited

=cut

	my $country;                  # country name

	$country = $_[0];

	# set US INT EQUITY to null
	if ($country eq "US INTERNATIONAL EQUITY") { $country = ""; }

	# get rid of the word EQUITY and any leading and trailing blanks remaining
	$country =~ s/EQUITY//;
	TrimString($country);
	$_[0] = $country;

	1;
}

sub PostHGprf($$$$) {

=head1 sub PostHGprf($$$$) 

=head2 ABSTRACT:
       Special month and quarter end processing for Historical Gross Returns

=head2 ARGUMENTS:
	   0: Name of the post VAULT input file
	   1: Name of the output file
	   2: Number of output headers (not used)
	   3: Number of records in the output file

=cut

	my $idxend;                # index into end dates
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $mmdd;                  # part of date for checking
	my $monbeg;                # period begin
	my $monend;                # period end
	my $month;                 # month value
	my $num_outs = 0;          # records in output file
	my $num_recs = 0;          # records in input file
	my $out_file_name;         # name of output file
	my $qtrbeg;                # quarter begin
	my $status;                # the status of the file opens
	my $year;                  # year value

	my @fields;                # fields split from input record

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and processing it
	while ($line = <INFILE>) {
		chomp($line);
		$num_recs++;
		@fields = split(/\|/, $line, -1);

		# get date string field and derive month end date
		($year, $month) = split(",", $fields[1]);          # date is in field 2 as yyyy,[m]m
		$month = sprintf("%02d", $month);                  # put in leading 0 if missing

		# get beginning and end of month dates
		$monbeg = $year . $month . "01";
		$monend = $monbeg;
		if (!CheckDate($monend)) { next; }                 # skip record if invalid date
		CalcDateEOM($monend);
		$fields[1] = $monend;

		# process for month or quarter begin date
		if ($fields[5] eq "M") {
			# monthly
			$fields[2] = $monbeg;                    # field 3 is beginning of period
		}
		else {
			# quarterly
			$mmdd = substr($monend, 4);         # format is mmdd
			foreach $idxend (0 .. 3) {
				if ($mmdd eq $end_dates[$idxend]) {
					$qtrbeg = substr($monend, 0, 4);
					if ($idxend == 0) { $qtrbeg--; }
					$qtrbeg .= $beg_dates[$idxend];    
					$fields[2] = $qtrbeg;            # field 3 is beginning of period
				}
			}
		}

		# construct and output new record
		$line = join("|", @fields);
		print OUTFILE "$line\n";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}

sub PostRGprf($$$$) {

=head1 sub PostRGprf($$$$) 

=head2 ABSTRACT:
       Special month and quarter end processing for Current Gross Returns

=head2 ARGUMENTS:
	   0: Name of the post VAULT input file
	   1: Name of the output file
	   2: Number of output headers (not used)
	   3: Number of records in the output file

=cut

	my $idxend;                # index into end dates
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $mmdd;                  # part of date for checking
	my $monbeg;                # period begin
	my $monend;                # period end
	my $num_outs = 0;          # records in output file
	my $num_recs = 0;          # records in input file
	my $out_file_name;         # name of output file
	my $qtrbeg;                # quarter begin
	my $retmon;                # return - monthly
	my $retqtr;                # return - quarterly
	my $status;                # the status of the file opens

	my @fields;                # fields split from input record

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and processing it
	GETRECORD: while ($line = <INFILE>) {
		chomp($line);
		$num_recs++;
		@fields = split(/\|/, $line, -1);

		($retmon, $retqtr) = split(",", $fields[20]);     # field contains the two returns
		# get beginning and end of month dates
		$monend = $fields[1];                              # date is in field 2
		if (!CheckDate($monend)) { next; }                 # skip record if invalid date
		$monbeg = substr($monend, 0, 6) . "01";

		# check for and process for quarterly end date
		$mmdd = substr($monend, 4);         # format is mmdd
		foreach $idxend (0 .. 3) {
			if ($mmdd eq $end_dates[$idxend]) {
				$qtrbeg = substr($monend, 0, 4);
				if ($idxend == 0) { $qtrbeg--; }
				$qtrbeg .= $beg_dates[$idxend];    
				if (!CheckNumber($retqtr)) { next GETRECORD; }    # skip if return not a number
				$fields[2]  = $qtrbeg;            # field 3 is beginning of period
				$fields[5]  = "Q";                # set periodicity field to quarterly
				$fields[20] = $retqtr;            # set total return
				$fields[28] = $retqtr;            # set float3 return
			}
		}

		# if we got through loop and it wasn't a quarter end, must be monthly
		if ($fields[5] eq "M") {
			if (!CheckNumber($retmon)) { next GETRECORD; }    # skip if return not a number
			$fields[2]  = $monbeg;                # field 3 is beginning of period
			$fields[20] = $retmon;                # set total return
		}

		# construct and output new record
		$line = join("|", @fields);
		print OUTFILE "$line\n";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}

sub PostRMprf($$$$) {

=head1 sub PostRMprf($$$$) 

=head2 ABSTRACT:
       Special month and quarter end processing for rmprf

=head2 ARGUMENTS:
	   0: Name of the post VAULT input file
	   1: Name of the output file
	   2: Number of output headers (not used)
	   3: Number of records in the output file

=cut

	my $date;                  # date in input record
	my $idxend;                # index into end dates
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $mmdd;                  # part of date for checking
	my $monbeg;                # period begin
	my $num_outs = 0;          # records in output file
	my $num_recs = 0;          # records in input file
	my $out_file_name;         # name of output file
	my $qtrbeg;                # quarter begin
	my $status;                # the status of the file opens

	my @fields;                # fields split from input record

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and processing it
	# if a quarter end, two records will be written
	while ($line = <INFILE>) {
		chomp($line);
		$num_recs++;
		@fields = split(/\|/, $line, -1);

		# get date and determine if quarter end
		$date = $fields[4];               # date is in field 5 as yyyymmdd
		$mmdd = substr($date, 4);         # format is mmdd

		# setup and write out monthly record
		$monbeg = substr($date, 0, 6) . "01";
		$fields[72] = "M";                         # field 73 is frequency code
		$fields[73] = $monbeg;                     # field 74 is beginning of period
		$line = join("|", @fields);
		print OUTFILE "$line\n";
		$num_outs++;

		# setup and write out quarterly record
		foreach $idxend (0 .. 3) {
			if ($mmdd eq $end_dates[$idxend]) {
				$qtrbeg = substr($date, 0, 4);
				if ($idxend == 0) { $qtrbeg--; }
				$qtrbeg .= $beg_dates[$idxend];    
				$fields[72] = "Q";            # field 73 is frequency code
				$fields[73] = $qtrbeg;        # field 74 is beginning of period
				$line = join("|", @fields);
				print OUTFILE "$line\n";
				$num_outs++;
			}
		}
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}


sub PostRMqtr($$$$) {

=head1 sub PostRMprf($$$$) 

=head2 ABSTRACT:
       Special quarter end processing for rmprf

=head2 ARGUMENTS:
	   0: Name of the post VAULT input file
	   1: Name of the output file
	   2: Number of output headers (not used)
	   3: Number of records in the output file

=cut

	my $date;                  # date in input record
	my $idxend;                # index into end dates
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $mmdd;                  # part of date for checking
	my $monbeg;                # period begin
	my $num_outs = 0;          # records in output file
	my $num_recs = 0;          # records in input file
	my $out_file_name;         # name of output file
	my $qtrbeg;                # quarter begin
	my $status;                # the status of the file opens

	my @fields;                # fields split from input record

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and processing it
	# if a quarter end, two records will be written
	while ($line = <INFILE>) {
		chomp($line);
		$num_recs++;
		@fields = split(/\|/, $line, -1);

		# get date and determine if quarter end
		$date = $fields[4];               # date is in field 5 as yyyymmdd
		$mmdd = substr($date, 4);         # format is mmdd

		# setup and write out quarterly record
		foreach $idxend (0 .. 3) {
			if ($mmdd eq $end_dates[$idxend]) {
				$qtrbeg = substr($date, 0, 4);
				if ($idxend == 0) { $qtrbeg--; }
				$qtrbeg .= $beg_dates[$idxend];    
				$fields[72] = "Q";            # field 73 is frequency code
				$fields[73] = $qtrbeg;        # field 74 is beginning of period
				$line = join("|", @fields);
				print OUTFILE "$line\n";
				$num_outs++;
			}
		}
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}

sub PostWBprf($$$$) {

=head1 sub PostWBprf($$$$) 

=head2 ABSTRACT:
       Special month and quarter end processing for rm files from WorkBench

=head2 ARGUMENTS:
	   0: Name of the post VAULT input file
	   1: Name of the output file
	   2: Number of output headers (not used)
	   3: Number of records in the output file

=cut

	my $date;                  # date in input record

	my $fld_beg =  3;          # field - begin of period
	my $fld_end =  2;          # field - end of period
	my $fld_frq =  6;          # field - frequency code
	my $fld_l2  = 10;          # field - Level 2 group
	my $fld_tot = 14;          # field - total return
	my $fld_net = 15;          # field - net   return
	my $fld_use;               # field - use to output return
	# decrement for zero based array
	$fld_beg--;
	$fld_end--;
	$fld_frq--;
	$fld_l2--;
	$fld_tot--;
	$fld_net--;

	my $idxend;                # index into end dates
	my $idxfld;                # index into fields
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $mmdd;                  # part of date for checking
	my $num_outs = 0;          # records in output file
	my $num_recs = 0;          # records in input file
	my $out_file_name;         # name of output file
	my $perbeg;                # month begin
	my $qtrbeg;                # quarter begin
	my $retmon;                # return - monthly
	my $retqtr;                # return - quarterly
	my $status;                # the status of the file opens
	my $tmpbeg;                # temporary begin date

	my @fields;                # fields split from input record

	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");
	if (!$status) { return 0; } 

	# loop reading a record and processing it
	# if a quarter end, two records will be written
	while ($line = <INFILE>) {
		chomp($line);
		$num_recs++;
		@fields = split(/\|/, $line, -1);

		# get date 
		$date = $fields[$fld_end];        # end date is in field as yyyymmdd
		$mmdd = substr($date, 4);         # format is mmdd

		# setup and write out monthly record
		$tmpbeg = substr($date, 0, 6) . "01";
		CalcDate8X2T7($tmpbeg, -1, $perbeg);       # period begin is end of last month
		$fields[$fld_frq] = "M";                   # field is frequency code
		$fields[$fld_beg] = $perbeg;               # field is beginning of period

		($retmon, $retqtr) = split(",", $fields[$fld_net]);     # field contains the two returns
		if (!CheckNumber($retmon)) { return 0; }                # skip out if return number not valid
		$fields[$fld_net] = $retmon;                            # field contains the return
		$fld_use = $fld_net;

		# special processing for asset class file
		if ($_[0] =~ "pitasr") {
			if    ($fields[$fld_l2] eq "EQUITIES") {
				$fields[$fld_l2] = "EQUITY";
			}
			elsif ($fields[$fld_l2] eq "CASH & TEMPORARY") {
				$fields[$fld_l2] = "CASH & EQUIVALENTS";
			}
			elsif ($fields[$fld_l2] eq "TOTAL NET OF FEES") {
				$fields[$fld_l2] = "";
			}
			elsif ($fields[$fld_l2] eq "TOTAL GROSS OF FEES") {
				$fields[$fld_l2] = "";
				# put return in tot_return field, and null out all other returns
				$fields[$fld_tot] = $fields[$fld_net];
				$fld_use = $fld_tot;
				for $idxfld ($fld_net .. $#fields) {
					$fields[$idxfld] = "";
				}
			}
		}

		$line = join("|", @fields);
		print OUTFILE "$line\n";
		$num_outs++;

		# setup and write out quarterly record
		foreach $idxend (0 .. 3) {
			if ($mmdd eq $end_dates[$idxend]) {
				$qtrbeg = substr($date, 0, 4);
				if ($idxend == 0) { $qtrbeg--; }
				$qtrbeg .= $beg_dates[$idxend];    
				$fields[$fld_frq] = "Q";             # field is frequency code
				$fields[$fld_beg] = $qtrbeg;         # field is beginning of period
				if (!CheckNumber($retqtr)) { return 0; }          # skip out if return number not valid
				$fields[$fld_use] = $retqtr;         # field contains the return
				$line = join("|", @fields);
				print OUTFILE "$line\n";
				$num_outs++;
			}
		}
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;

	1;
}
1;
