package ITSTime;

=head1  ITSTime.pm

=head2  ABSTRACT:
		Provide standard time routines - current routines are:
		CalcElapsed           - Calculates the elapsed time between two arrays of date/time and returns it as a number and a string
		Convert_seconds_time  - Converts seconds to hh:mm:ss time
		Convert_time_seconds  - Converts hh:mm:ss time to seconds
		Convert_time6_hhmmssc - Converts hhmmss to hh:mm:ss format (c for colon)
		Convert_timee_elapsed - Converts an elapsed time into dd:hh:mm:ss elapsed format with zero and colon suppression

=head2  REVISIONS:
		RAM 18-AUG-02 13354-0  JGF Initial development

=cut

use strict;
use Time::Local;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(CalcElapsed Convert_seconds_time Convert_time_seconds
		 	Convert_time6_hhmmssc Convert_timee_elapsed);

sub CalcElapsed (\@\@$$) {

=head1  sub CalcElapsed

=head2  ABSTRACT:
		Calculates the elapsed time between two arrays of date/time and returns it as a number and a string

=head2  ARGUMENTS:
        1: Array of start date/time (sec, min, hour, day, month, year)
        2: Array of end   date/time (sec, min, hour, day, month, year)
        3: Returned number of elapsed seconds
        4: Returned elapsed time expressed as a string (ddd:hh:mm:ss)

=cut

	my $day;
	my $endes;
	my $hrs;
	my $min;
	my $sec;
	my $startes;
	my $temp;  
	my $starr;
	my $enarr;

	$starr = $_[0];
	$enarr = $_[1];

	$startes = timelocal(@$starr);
	$endes   = timelocal(@$enarr);

	$temp = $endes - $startes;  
	$_[2] = $temp;                # return number of elapsed seconds

	$sec  = $temp % 60;
	$temp = ($temp - $sec) / 60;
	$min  = $temp % 60;
	$temp = ($temp - $min) / 60;
	$hrs  = $temp % 24;
	$day  = ($temp - $hrs) / 24;

	$_[3] = sprintf("%02d:%02d:%02d:%02d", $day, $hrs, $min, $sec);     # return elapsed time as a string

	1;
}

sub Convert_seconds_time ($$) {

	# ABSTRACT:
	# Converts seconds into a time

	# ARGUMENTS:
	# seconds
	# time (dd:hh:mm:ss)

	use integer;

	my @numin = (9999999, 24, 60, 60);   # number of components of time in the next larger component
	my @vals;
	my $time;
	my $value;
	my $seconds = $_[0];

	my $factor = 1;
	$time = "";
	while ($seconds > 0) {
		$factor = pop(@numin);
		$value = sprintf("%02d", ($seconds % $factor));
		push(@vals, $value);
		$seconds = ($seconds - $value) / $factor;
	}
	$time = join(":", reverse(@vals));
	$time = Convert_timee_elapsed($time);
	TrimString($time);
	$_[1] = $time;
	1;
}

sub Convert_time_seconds ($$) {

	# ABSTRACT:
	# Converts a time into a number of seconds

	# ARGUMENTS:
	# time (dd:hh:mm:ss)
	# seconds

	use integer;

	my @numin = (1, 24, 60, 60);   # number of components of time in the next larger component
	my $seconds;
	my $time = $_[0];
	TrimString($time);
	while ($time =~ s/^0|^://) {1}          # remove leading 0 and :
	my @vals = split(":", $time);           # split into components

	my $factor = 1;
	$seconds = 0;
	while ($#vals >= 0) {
		$seconds += (pop(@vals) * $factor);
		$factor *= (pop(@numin));
	}
	$_[1] = $seconds;
	1;
}

sub Convert_time6_hhmmssc ($) {

	# ABSTRACT:
	# Converts a 6 digit time into hh:mm:ss format (c for colon)

	# ARGUMENTS:
	# 6 digit time (hhmmss)

	use ITSCheck;

	my $newtime;
	my $time = $_[0];

	if (!CheckTime($time)) {
		# time is bad
		$newtime = "        ";
	}
	else {
		$newtime = join(":", substr($time, 0, 2), substr($time, 2, 2), substr($time, 4, 2));
	}
	return $newtime;
	1;
}

sub Convert_timee_elapsed ($) {

	# ABSTRACT:
	# Converts an elapsed time into dd:hh:mm:ss elapsed format with zero and colon suppression
	# Returns value with a minimum left filled length of 4 (0:00)

	# ARGUMENTS:
	# elapsed time - dd:hh:mm:ss is the usual format

	use ITSTrim;

	my $time = $_[0];
	TrimString($time);                      # get rid of leading and trailing space
	while ($time =~ s/^0|^://) {1}          # remove leading 0 and :
	my $lentime = length($time);            # check length and fill to 4 if necessary
	if ($lentime < 4) {
		substr($time, 0, 0) = substr("0:00", 0, 4 - $lentime);
	}

	return $time;
	1;
}
1;
