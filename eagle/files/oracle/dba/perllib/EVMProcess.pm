package EVMProcess;

=head1  EVMProcess.pm

=head2  ABSTRACT:
		Provide EVM file service - current routines are:
		EVMPostMFP           - Changes to comma delimited, renames file and distributes it
		EVMPostfmat          - Checks if at least one of two required fields is present

=head2  REVISIONS:
		RCR 16-APR-06 13354-0  RCR Initial development

=cut

use strict;

use File::Copy;
use ITSCheck;
use ITSCode;
use ITSFTP;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(EVMPostMFP EVMPostfmat);

sub EVMPostMFP($$$$) {

=head1 sub EVMPostMFP($$$$) 

=head2 ABSTRACT:
	   Changes delimiter to comma and renames file

=head2 ARGUMENTS:
	   0: Name of the file to append 
	   1: Name of the file to be appended to
	   2: Number of output headers
	   3: Number of records in the output file

=cut

	# SCALARS
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $num_outs	 = 0;      # records in output file
	my $num_recs	 = 0;      # records in input file
	my $out_hdr;               # number of headers in output file
	my $out_file_name;         # name of output file
	my $status;                # the status of the file opens

	$out_hdr = $_[2];
	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");  
	if (!$status) { return 0; } 

	# get rid of the header record if necessary
	while ($num_recs < $out_hdr) {
		$line = <INFILE>;
		$num_recs++;
	}

	# loop reading a record and writing it out
	while ($line = <INFILE>) {
		$num_recs++;
		$line =~ s/\|/,/g;
		print OUTFILE "$line";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;
	
	my $locsl = rindex($out_file_name, "/");
	my $local_dir = substr($out_file_name, 0, $locsl + 1);
	$out_file_name = substr($out_file_name, $locsl + 1);

	my $locun = index($out_file_name, "_");
	my $datetime = "";
	if ($locun >= 0) { $datetime = substr($out_file_name, $locun, 16); }       # "_yyyymmdd_hhmmss"

	# set rename and distribution parameters based on input file name
	my $operation = "putren";
	my $file_type = "ascii";
	my $del_flag = 0;
	my $fh_logfile = "";

	# this is to get it to work for now
	my $nodeip;
	my $account;
	my $psw;
	my $working_dir;
	if (substr($out_file_name, 0, 9) eq "evmdlyprf" || substr($out_file_name, 0, 9) eq "evmmonprf") {
		$nodeip = "10.170.1.91";
		$account = "pevmtst1";
		$psw = '($o|*HSZj';
		$working_dir = "/apps/pevmtst1/pace/server/pacelog/EAGLEPACE/PRF";
	}
	else {
		# prod
		$nodeip = "10.60.5.65";
		$account = "pevmprd1";
		$psw = '($o|*HSZj';
		$working_dir = "/apps/pevmprd1/pace/server/pacelog/EAGLEPACE/PRF";
	}

	# calc dval
	my $dval = "";
	for (my $idx = 33; $idx <= 126; $idx+=7) {
		$dval .= chr($idx);
	}
	DecodeData($dval, $psw);
	DecodeData($dval, $psw);

	my $new_file_name;
	if ($out_file_name =~ m/hfldly/) {
		$new_file_name = "EatonVanceDailyMFP" . $datetime . ".txt";
	}
	else {
		$new_file_name = "EatonVanceMonthlyMFP" . $datetime . ".txt";
	}

	ExecFTP($operation, $out_file_name, $nodeip, $account, $psw, $file_type, $local_dir, $working_dir,
			  $del_flag,$fh_logfile, $new_file_name);

	1;
}

sub EVMPostfmat($$$$) {

=head1 sub EVMPostfmat($$$$) 

=head2 ABSTRACT:
		Checks if at least one of two required fields is present

=head2 ARGUMENTS:
	   0: Name of the file to append 
	   1: Name of the file to be appended to
	   2: Number of output headers
	   3: Number of records in the output file

=cut

	# SCALARS
	my $inp_file_name;         # name of input file
	my $line;                  # input line from the feed file
	my $num_outs	 = 0;      # records in output file
	my $num_recs	 = 0;      # records in input file
	my $out_hdr;               # number of headers in output file
	my $out_file_name;         # name of output file
	my $status;                # the status of the file opens
	my @fields;

	$out_hdr = $_[2];
	$_[3] = 0;                        # set number of output records to 0 in case things go wrong

	# open input and output files
	$inp_file_name = $_[0];
	$status = open(INFILE, "< $inp_file_name");
	if (!$status) { return 0; } 

	$out_file_name = $_[1];
	$status = open(OUTFILE, "> $out_file_name");  
	if (!$status) { return 0; } 

	# loop reading a record and writing it out
	while ($line = <INFILE>) {
		$num_recs++;
		@fields = split(/\|/,$line);
		#skip record if field 4 and 32 are null
		if ($fields[3] eq "" && $fields[31] eq "") { next; }
		print OUTFILE "$line";
		$num_outs++;
	}

	$_[3] = $num_outs;                 # set number of output records

	# close files
	close INFILE;
	close OUTFILE;
	
	1;
}
