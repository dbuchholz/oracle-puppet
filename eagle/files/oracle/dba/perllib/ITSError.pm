package ITSError;

=head1  ITSError.pm

=head2  ABSTRACT:
		Provide standard error routines - current routines are:
		FieldError     - Prints a field error and message to a logfile and increments an error counter
		LineError      - Prints an input line, formatted in 100 byte lines, to a logfile
		LogAppendLock  - Performs secure logging with lock
		LogCycleInit   - Initializes a log file for a cycle
		LogCyclePrint  - Prints a message to a log
		LogCycleEnd    - Closes a log
		MatchedSuccess - Prints a list of missing asset IDs to a log file
		MissingWarning - Prints a list of missing asset IDs to a log file
		TokenError     - Prints a text line and message and increments an error counter
		TokenErrorLog  - Prints a text line and message to the monitor and a log file and increments an error counter

=head2  REVISIONS:
		RAM 29-AUG-05 13354-0  RCR Added MatchedSuccess routine
		RAM 03-MAY-05 13354-0  RCR Added LogAppendLock routine
		RAM 02-APR-04 13354-0  RCR Added LogCycle routines
		RAM 24-JUN-02 13354-0  JGF Initial development

=cut

use strict;
use English;
use ITSDate;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(FieldError LineError LogAppendLock LogCycleInit LogCyclePrint LogCycleEnd 
			 MatchedSuccess MissingWarning TokenError TokenErrorLog);

# define log file printer hash
my %logfiles;

sub FieldError (*$$$$$$$) {

=head1  sub FieldError (*$$$$$$$)

=head2  ABSTRACT:
		prints a field error to the LOGFILE

=head2  ARGUMENTS:
		0: filehandle of LOGFILE
		1: line number in file
		2: security ID
		3: record type
		4: item name
		5: data value   
		6: type of error
		7: num_error (to be incremented)

=cut

	my $fh_logfile = $_[0];
	print $fh_logfile "ERROR: Record: $_[1]  SecID: \"$_[2]\" Type: \"$_[3]\"  Item: $_[4]  Value: \"$_[5]\"\n";
	print $fh_logfile "       Error $_[6]\n";
	$_[7]++;                  # increment error counter
	1;
}

sub LineError (*$$) {

=head1  sub LineError (*$$)

=head2  ABSTRACT:
		prints a line formatted in 100 byte chunks to the LOGFILE

=head2  ARGUMENTS:
		0: filehandle of LOGFILE
		1: label for line (keep under 25 characters so that full line will not exceed 132)
		2: input line

=cut

	my $begstr;                # beginning column of string chunk
	my $lenlab;                # length of the label
	my $lenstr;                # length of string chunk (1-100)
	my $lenline;               # length of the input line
	my $outline;               # constructed output line

	# get argument values
	my $fh_logfile = $_[0];    # filehandle of logfile
	my $label      = $_[1];    # label
	my $line       = $_[2];    # line

	$lenlab = length($label);
	if ($lenlab > 25) { $lenlab = 25; }        # limit length of label to 25

	# print line in 100 byte chunks
	$lenline = length($line);
	for ($begstr = 0; $begstr <= $lenline; $begstr += 100) {
		$lenstr = (($begstr + 99) <= $lenline) ? 100 : ($lenline - $begstr);
		$outline = substr($label, 0, $lenlab) . ": \"" . substr($line, $begstr, $lenstr) . "\"";
		print $fh_logfile "$outline\n";
		$label =~ s/\S/ /go;
	}

	1;
}

sub LogAppendLock ($$) {

=head1  sub LogAppendLock ($$)

=head2  ABSTRACT:
		Writes a log message with append and lock to avoid conflicts

=head2  ARGUMENTS:
		0: name of logfile
		1: message

=cut

	my $idxfil;                        # number of file
	my $idxtot = 0;                    # total number of tries so far
	my $idxtry;                        # number of tries on this file
	my $numprint;                      # printable version of total number of tries

	my $logname = $_[0];
	my $lennam = length($logname);

	# loop on files
	for $idxfil (0 .. 4) {
		if ($idxfil > 0) { 
			substr($logname, $lennam, 1) = $idxfil;            # if not on first file, append number
		}
		open (LOGFILE, ">>$logname") || return 0;              # error if we can not open log file

		# loop for tries for this file
		for $idxtry (1 .. 40) {
			$idxtot++;                                         # increment total tries
			if (flock(LOGFILE, 6)) {                           # 6 = LOCK_EX || LOCK_NB   (EXclusive lock with NonBlocking)
				# got the lock - form and write log message	- only print number of total tries if > 1
				$numprint = $idxtot <= 1 ? "" : "[" . $idxtot . "]"; 
				my $message = join(" ", GetDateTimeLog, $numprint, "PID:", $PROCESS_ID, "-", $_[1]);
				print LOGFILE "$message\n";
				close(LOGFILE);                                # closing file releases the lock
				return 1;                                      # return success
			}
			if (($idxtry % 8) == 0) { select (undef, undef, undef, 0.01); }
		}
		# out of tries for this file - close it
		close(LOGFILE);

	}
	# out of tries and out of files - could not write - return 0
	return 0;
	1;
}

sub LogCycleInit ($$) {

=head1  sub LogCycleInit ($$)

=head2  ABSTRACT:
		Initalizes a log cycle log file

=head2  ARGUMENTS:
		0: context
		1: name of logfile

=cut

	my $context = $_[0];
	my $logname = $_[1];

	if (!exists($logfiles{ $context })) {
		$logfiles{ $context }[0] = 0;               # set default as unopened
	}

	my $flgopen = $logfiles{ $context }[0];
	if ($flgopen <  0) { return 0; }                # file could not be opened previously - skip processing
	my $message = join(" ", GetDateTimeLog, "-> Starting", $context);

	if ($flgopen == 0) {
		local *FH;
		if (open(*FH, ">>$logname")) {
			$logfiles{ $context } = [(1, *FH)];     # mark as open and save filehandle for printing
			print FH "$message\n";
		}
		else {
			$logfiles{ $context }[0] = -1;          # open failed - mark for no future processing
			return 0;
		}
	}
	else {
		my $fh = $logfiles{ $context }[1];
		print $fh "$message\n";
	}

	1;
}

sub LogCyclePrint ($$$) {

=head1  sub LogCyclePrint ($$$)

=head2  ABSTRACT:
		prints a line to a log cycle log file

=head2  ARGUMENTS:
		0: context
		1: level
		2: message

=cut

	my $context = $_[0];
	my $level   = $_[1];
	my $message = $_[2];

	if (exists($logfiles{ $context })) {
		if ($logfiles{ $context }[0] == 1) {
			my $fh_logfile = $logfiles{ $context }[1];
			$message = join(" ", GetDateTimeLog, "-> Level", $level, $message);
			print $fh_logfile "$message\n";
		}
	}

	1;
}

sub LogCycleEnd ($) {

=head1  sub LogCycleEnd ($)

=head2  ABSTRACT:
		Closes a log cycle log file

=head2  ARGUMENTS:
		0: context

=cut

	my $context = $_[0];

	if (exists($logfiles{ $context })) {
		if ($logfiles{ $context }[0] == 1) {
			my $fh_logfile = $logfiles{ $context }[1];
			my $message = join(" ", GetDateTimeLog, "->   Ending", $context);
			print $fh_logfile "$message\n";
			close($fh_logfile);
			$logfiles{ $context }[0] = 0;
		}
	}

	1;
}

sub MatchedSuccess (*\@) {

=head1  sub MatchedSuccess (*\@)

=head2  ABSTRACT:
		prints a success message listing missing asset IDs

=head2  ARGUMENTS:
		0: filehandle of log file
		1: array of matched asset IDs

=cut

	my $fh_logfile = $_[0];
	my $refarray = $_[1];
	my $tempid;

	if ($#$refarray >= 0) {
		my $succ_buff = " Success - assets matched: ";
		foreach $tempid (@$refarray) {
			if (length($tempid) >= 12) { $tempid .= " "; }
			$succ_buff .= sprintf("%-13s", $tempid);
		}
		print $fh_logfile "$succ_buff\n";
		@$refarray = ();   # reinitialize buffer
	}
	1;
}

sub MissingWarning (*\@) {

=head1  sub MissingWarning (*\@)

=head2  ABSTRACT:
		prints a warning listing missing asset IDs

=head2  ARGUMENTS:
		0: filehandle of log file
		1: array of missing asset IDs

=cut

	my $fh_logfile = $_[0];
	my $refarray = $_[1];
	my $tempid;

	if ($#$refarray >= 0) {
		my $warn_buff = " Warning - assets not on feed: ";
		foreach $tempid (@$refarray) {
			if (length($tempid) >= 12) { $tempid .= " "; }
			$warn_buff .= sprintf("%-13s", $tempid);
		}
		print $fh_logfile "$warn_buff\n";
		@$refarray = ();   # reinitialize buffer
	}
	1;
}

sub TokenError ($$$$) {

=head1  sub TokenError ($$$$)

=head2  ABSTRACT:
		prints a token error

=head2  ARGUMENTS:
		0: line number
		1: line
		2: error message
		3: error counter (incremented before returning)

=cut

	my $endchar;         #  end character for line

	$endchar = substr($_[1], -1) eq "\n" ? "" : "\n";      # put eol on end if it doesn't exist
	print "ERROR: IN LINE $_[0]: $_[1]$endchar";
	$endchar = substr($_[2], -1) eq "\n" ? "" : "\n";      # put eol on end if it doesn't exist
	print "$_[2]$endchar\n";
	$_[3]++;                  # increment error counter
	1;
}

sub TokenErrorLog (*$$$$) {

=head1  sub TokenErrorLog (*$$$$)

=head2  ABSTRACT:
		prints a token error to the monitor and to a log file

=head2  ARGUMENTS:
		0: filehandle of log file
		1: line number
		2: line
		3: error message
		4: error counter (incremented before returning)

=cut

	my $endchar;                # end character for line
	my $fh_logfile = $_[0];     # filehandle of log file

	$endchar = substr($_[2], -1) eq "\n" ? "" : "\n";      # put eol on end if it doesn't exist
	print $fh_logfile "ERROR: IN LINE $_[1]: $_[2]$endchar";
	print             "ERROR: IN LINE $_[1]: $_[2]$endchar";
	$endchar = substr($_[3], -1) eq "\n" ? "" : "\n";      # put eol on end if it doesn't exist
	print $fh_logfile "$_[3]$endchar\n";
	print             "$_[3]$endchar\n";
	$_[4]++;                  # increment error counter
	1;
}
1;
