package ITSEvent;

=head1  ITSEvent.pm

=head2  ABSTRACT:
		ManageEvent - Checks, sets and clears an event

=head2  REVISIONS:
		RAM 08-APR-05 13354-0  RCR Initial development

=cut

use strict;
use ITSDate;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ManageEvent);

sub ManageEvent($$$$) {

	# arguments are:
	# 0: filehandle of logfile - if null (""), messages to log file will not be printed
	# 1: HOME directory
	# 2: event name
	# 3: operation:                            EVENT NOT PRESENT         EVENT PRESENT                RETRIES EXHAUSTED
	#                                          ---------------------     -------------------------    -----------------
	#   CHECKSET - checks for existing event - sets event, returns 1     retries for 20 minutes       returns 0
	#   QUICKSET - checks for existing event - sets event, returns 1     no retry - returns 0
	#   CLEAR    - clears event              - returns 1                 deletes event - returns 1

	my $fhlog;             # filehandle of logfile
	my $home_dir;          # home directory
	my $event_file;        # name of event file
	my $event_name;        # name of event
	my $operation;         # operation desired
	my $time;              # timestamp for logging

	($fhlog, $home_dir, $event_name, $operation) = @_;

	# check for last slash on home directory and construct event name
	if (substr($home_dir, -1) ne "/") { $home_dir .= "/"; }
	$event_file = $home_dir . $event_name . ".EVENT";

	# operations to set event
	if    ($operation eq "CHECKSET" || $operation eq "QUICKSET") {
		if (-e $event_file) {
			if ($operation eq "QUICKSET") {
				# event exists and QUICKSET specified - return 0
				$time = GetDateTimeLog;
				                    print        "$time  Event $event_name already exists - operation: QUICKSET\n";
				if ($fhlog ne "") { print $fhlog "$time  Event $event_name already exists - operation: QUICKSET\n"; }
				return 0;
			}
			# event exists and CHECKSET specified - retry for retmax seconds
			my $retint = 10;       # interval for retries
			my $retmax = 1200;     # maximum number of seconds to retry
			my $rettot = 0;        # total seconds in retry mode
			while (-e $event_file) {
				if ($rettot >= $retmax) { 
					# out of retries - return 0
					$time = GetDateTimeLog;
					                    print        "$time  CHECKSET Event $event_name failed after $retmax retries\n";
					if ($fhlog ne "") { print $fhlog "$time  CHECKSET Event $event_name failed after $retmax retries\n"; }
					return 0; 
				}
				sleep($retint);
				$rettot += $retint;
			}
			# while loop ended - event does not exist now
		}
		# event does not exist - create it and return 1
		open (EVENT, "> $event_file") ||
			NotifyFatal($fhlog, "ManageEvent", "Could not open event file: $event_file", "VAULT", "ENS DIE");
		close(EVENT);
		$time = GetDateTimeLog;
		                    print        "$time  Event $event_name set\n";
		if ($fhlog ne "") { print $fhlog "$time  Event $event_name set\n"; }
	}
	# operation to clear event
	elsif ($operation eq "CLEAR") {
		# for clear operation, just delete file if it exists and return 1
		if (-e $event_file) {
			unlink($event_file);
		}
		$time = GetDateTimeLog;
		                    print        "$time  Event $event_name cleared\n";
		if ($fhlog ne "") { print $fhlog "$time  Event $event_name cleared\n"; }
	}
	else {
		# operation invalid - return 0
		$time = GetDateTimeLog;
		                    print        "$time  Event $event_name - invalid operation: $operation\n";
		if ($fhlog ne "") { print $fhlog "$time  Event $event_name - invalid operation: $operation\n"; }
		return 0;
	}
	1;
}
1;
