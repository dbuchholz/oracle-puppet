package ITSEdit;

=head1  ITSEdit.pm

=head2  ABSTRACT:
		Provide standard edit routines - Current edits are:
		Ed0snull  - Sets a string to null if it is all zeros
		Ed09wpnl  - Sets a string with a decimal point to null if it is all zeros or all nines or if both halves are
		Ed0or9nl  - Sets a string to null if it is all zeros or all nines
		Ed9snull  - Sets a string to null if it is all nines
		EdblorZnl - Sets a string to null if it is all whitespace or Z's
		Edcffund  - Sets certain cffund numbers to null
		Edcomma   - Puts commas into a string number
		Edcomund  - Sets commas to underlines
		Eddt01nl  - Sets a string to null if it is 00010101
		Eddta(n)  - Adjusts a date by n days
		Eddtadj   - Adjusts a date by number of days given.  Example: Eddtadj(1, $value)
		Edeofnl   - Sets a string to null if it contains any case form of "END OF FILE" or "END_OF_FILE"
		Ednumint  - Sets a decimal number to its integer portion
		Edlzf(n)  - Left zero fills a string to length n.  Uses Edlzfil.  Example: Edlzf4
		Edlzfill  - Left zero fills a string to length given.  Example: Edlzfill(4, $value)
		Ednanull  - Sets a string to null if it is "NA"
		EdNdAdnl  - Sets a string to null if it is "N.A."
		EdNsAnl   - Sets a string to null if it is "N/A"
		Ednulln   - Sets a string to N if it is null
		Ed0pnull  - Sets a string to null if it is the number 0 with a decimal point in it
		Edpctage  - Computes the percentage one number is of another
		Edratio   - Computes the ratio of one number to another
		Edsmooth  - Smooths a value, getting rid of fractional garbage
		Edtrunc6  - Truncates a value to 6 characters
		Edtrunc8  - Truncates a value to 8 characters

=head2  REVISIONS:
        JML 05-JAN-09 13354-0  JML Modified Edcffund
		RCR 08-JUL-08 13354-0  RCR Added Edratio
		RCR 27-JUN-08 13354-0  RCR Added Edpctage
		RCR 25-JUL-06 13354-0  RCR Added EdNsAnl
		RCR 07-JUL-06 13354-0  RCR Added Edsmooth
		RAM 24-MAR-05 13354-0  JGF Added Ednumint
		RAM 10-MAY-02 13354-0  JGF Initial development

=cut

use strict;
use ITSCheck;
use ITSDate;
use ITSTrim;

use vars qw(@ISA @EXPORT);
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(Ed0snull Ed09wpnl Ed0or9nl Ed9snull EdblorZnl Edcffund Edcomma Edcomund Eddt01nl Eddta1 Eddtadj Edeofnl Ednumint 
             Edlzf4 Edlzfill Ednanull EdNdAdnl EdNsAnl Ednulln Ed0pnull Edpctage Edratio Edsmooth Edtrunc6 Edtrunc8);

my $zeros;

BEGIN {
	$zeros = "000000000000";
}

sub Ed0snull ($) {

	# takes an argument that is a string and checks to see if it is all 0s
	# if it is, sets the field to "" and returns 1, else returns 0
	
	if ($_[0] =~ m/[^0]/) { return 0; }
	$_[0] = "";
	1;                # return true - all zero field was found
}

sub Ed09wpnl ($) {

	# takes an argument that is a string with a decimal point and checks to see if it is all 0s or all 9s
	# or if both halves are
	# if it is, sets the field to "" and returns 1, else returns 0
	
	my $locpt = index($_[0], ".");            # must check for point - some feeds just set field to all 9s
	if ($locpt >= 0) {
		# point exists - process halves
		my ($front, $back) = split('\.', $_[0]);
		if ( ($front =~ m/[^0]/) && ($front =~ m/[^9]/) ) { return 0; }
		if ( ($back  =~ m/[^0]/) && ($back  =~ m/[^9]/) ) { return 0; }
	}
	else {
		# point does not exist - process as one number
		if ( ($_[0] =~ m/[^0]/) && ($_[0] =~ m/[^9]/) ) { return 0; }
	}
	$_[0] = "";
	1;                # return true - all zero or all nine field was found
}

sub Ed0or9nl ($) {

	# takes an argument that is a string and checks to see if it is all 0s or all 9s
	# if it is, sets the field to "" and returns 1, else returns 0
	
	if ( ($_[0] =~ m/[^0]/) && ($_[0] =~ m/[^9]/) ) { return 0; }
	$_[0] = "";
	1;                # return true - all zero or all nine field was found
}

sub Ed9snull ($) {

	# takes an argument that is a string and checks to see if it is all 9s
	# if it is, sets the field to "" and returns 1, else returns 0
	
	if ($_[0] =~ m/[^9]/) { return 0; }
	$_[0] = "";
	1;                # return true - all nine field was found
}

sub EdblorZnl ($) {

	# takes an argument that is a string and checks to see if it is all whitespace or Zs
	# if it is, sets the field to "" and returns 1, else returns 0
	
	if ( ($_[0] =~ m/[^\s]/) && ($_[0] =~ m/[^Z]/) ) { return 0; }
	$_[0] = "";
	1;                # return true - all whitespace or Z field was found
}

sub Edcffund($) {

	# Edits certain fund numbers to null

	my $fund = $_[0];
	if ($fund eq "30" || $fund eq "30  " || $fund eq " 30 " || $fund eq "  30" ||
		$fund eq "0030" || $fund eq "00030" || $fund eq "  30" || substr($fund, 0, 4) eq "IZZK") {
		$_[0] = "";
	}
	1;
}

sub Edcomma($) {

	# puts commas into a string number
	# code is from The Perl Cookbook, O'Reilly & Associates, 1998
	# string is reversed, and skipped up to any decimal point, and then commas put in by threes
	# string is then reversed again before it is returned

	my $text = reverse $_[0];
	$text =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
	$_[0] = scalar reverse $text;
	1;
}

sub Edcomund($) {

	# sets commas to underlines

	$_[0] =~ s/,/_/g;
	1;
}

sub Eddt01nl ($) {

	# takes an argument that is a string and checks to see if it is 00010101
	# if it is, sets the field to "" and returns 1, else returns 0
	
	TrimString($_[0]);
	if ($_[0] ne "00010101") { return 0; }
	$_[0] = "";
	1;                # return true - field was 00010101
}

sub Edeofnl ($) {
	# Sets a string to null if it contains any case form of "END OF FILE" or "END_OF_FILE"
	if ( ($_[0] =~ m/END OF FILE/i) || ($_[0] =~ m/END_OF_FILE/i) ) {
		$_[0] = "";
	}
	1;
}

sub Ednumint ($) {
	# takes a decimal number argument and returns the integer portion
	my $number = $_[0];
	if (!CheckNumber($number)) { return 0; }      # error if not a number
	my $locdot = index($number, ".");
	if ($locdot >= 0) { 
		substr($number, $locdot) = "";
	}
	$_[0] = $number;
	1;
}

sub Edlzf4 ($) {
	# takes an argument and left zero fills it to length 4
	Edlzfill(4, $_[0]);
	1;
}

sub Edlzfill($$) {

	# left zero fills a value to maxlen

	# ARGUMENTS:
	#    0: maxlen
	#    1: value

	my $maxlen = $_[0];
	my $value = $_[1];

	if ($value ne "") {
		my $vallen = length($value);
		if    ($maxlen > $vallen) {
			$value =~ s/^ +/0/;
			substr($value, 0, 0) = substr($zeros, 0, $maxlen - $vallen);
		}
		elsif ($maxlen == $vallen) {
			$value =~ s/^ +/0/;
		}
		else {          # $maxlen < $vallen
			substr($value, $vallen - $maxlen, $maxlen) =~ s/^ +/0/;
		}
		$_[1] = $value;
	}
}


sub Ednanull ($) {

	# takes an argument that is a string and checks to see if it is NA
	# if it is, sets the field to "" and returns 1, else returns 0
	
	TrimString($_[0]);
	if ($_[0] ne "NA" && $_[0] ne "na") { return 0; }
	$_[0] = "";
	1;                # return true - field was NA
}

sub EdNsAnl ($) {

	# takes an argument that is a string and checks to see if it is N.A.
	# if it is, sets the field to "" and returns 1, else returns 0
	
	TrimString($_[0]);
	if ($_[0] ne "N/A") { return 0; }
	$_[0] = "";
	1;                # return true - field was N/A
}

sub EdNdAdnl ($) {

	# takes an argument that is a string and checks to see if it is N.A.
	# if it is, sets the field to "" and returns 1, else returns 0
	
	TrimString($_[0]);
	if ($_[0] ne "N.A.") { return 0; }
	$_[0] = "";
	1;                # return true - field was N.A.
}

sub Ednulln ($) {

	# takes an argument that is a string and checks to see if it is null
	# if it is, sets the field to "N" and returns 1, else returns 0
	
	if ($_[0] ne "") { return 0; }
	$_[0] = "N";
	1;                # return true - field was NA
}

sub Ed0pnull($) {

	# takes an argument that is a string and checks to see if it is the number 0 (may have a decimal point)
	# if it is, sets the field to "" and returns 1, else returns 0
	
	my $text = $_[0];
	TrimString($text);

	$text =~ s/\.{1}/0/;                        # find only one decimal point and set to 0
	if ($text =~ m/[^0]/)  { return 0; }        # if anything not 0, return 0
	$_[0] = "";
	1;
}

sub Edpctage($) {

	# takes an argument that is composed of two comma separated numbers and 
	# computes the percentage the first is of the second
	# if successful, it sets the field to the percentage and returns 1, else it sets it to null and returns 0
	
	my ($numer, $denom) = split(',', $_[0]);

	if (CheckNumber($numer)) {
		if (CheckNumber($denom)) {
			if ($numer ne "" && $denom ne "") {
				if ($numer >= 0) {
					if ($denom > 0) {
						$_[0] = ($numer / $denom) * 100;
						return 1;
					}
				}
			}
		}
	}

	# computation failed - set argument to null and return 0
	$_[0] = "";
	return 0;
	1;
}

sub Edratio($) {

	# takes an argument that is composed of two comma separated numbers and 
	# computes the ratio of the first to the second
	# if successful, it sets the field to the ratio and returns 1, else it sets it to null and returns 0
	
	my ($numer, $denom) = split(',', $_[0]);

	if (CheckNumber($numer)) {
		if (CheckNumber($denom)) {
			if ($numer ne "" && $denom ne "") {
				if ($numer >= 0) {
					if ($denom > 0) {
						$_[0] = $numer / $denom;
						return 1;
					}
				}
			}
		}
	}

	# computation failed - set argument to null and return 0
	$_[0] = "";
	return 0;
	1;
}

sub Edsmooth ($) {

	# Edsmooth  - Smooths a value, getting rid of fractional garbage
	my $value = $_[0];
	my @idxstr;

	# only perform smoothing after the decimal point
	my $idxdot = index($value, ".");
	if ($idxdot >= 0) {

		# find telltale string values in the number's fraction and which is the first one
		my $idxbeg = $idxdot + 1;
		$idxstr[0] = index($value, "0000", $idxbeg);
		$idxstr[1] = index($value, "0001", $idxbeg);
		$idxstr[2] = index($value, "9998", $idxbeg);
		$idxstr[3] = index($value, "9999", $idxbeg);
		my $minstr = 9999;
		foreach (@idxstr) {
			if ($_ >= 0 && $_ < $minstr) { $minstr = $_; }
		}

		# if a required string found, setup and round the number at the appropriate point
		if ($minstr > 0 && $minstr < 9999) {
			my $numdp = $minstr - $idxbeg;
			my $format = "%." . $numdp . "f";
			$_[0] = sprintf($format, $value);
		}
	}
	
	1; 
}

sub Edtrunc6 ($) {

	# takes an argument that is a string and truncates it to 6 characters
	
	if (length($_[0]) > 6) {
		substr($_[0], 6) = "";
	}
	1; 
}

sub Edtrunc8 ($) {

	# takes an argument that is a string and truncates it to 8 characters
	
	if (length($_[0]) > 8) {
		substr($_[0], 8) = "";
	}
	1; 
}

sub Eddta1 ($) {
	# takes an argument and adjusts it one day forward
	Eddtadj(1, $_[0]);
	1;
}

sub Eddtadj($$) {

	# adjusts a date by a number of days

	# ARGUMENTS:
	#    0: numdays
	#    1: value

	my $numdays = $_[0];
	my $value   = $_[1];

	if ($value ne "") {
		my $newvalue;
		my $status;
		$status = CalcDate8X2T7($value, $numdays, $newvalue);
		if ($status) {
			# status OK, update value
			$_[1] = $newvalue;
		}
		else {
			# status not OK, update with null
			$_[1] = "";
		}
	}
	1;
}
1;
