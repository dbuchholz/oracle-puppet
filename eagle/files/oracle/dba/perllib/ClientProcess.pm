package ClientProcess;

=head1  ClientProcess.pm
 
=head2  ABSTRACT:
        Main subroutine for the client specific loader
 
=head2  ROUTINES:
		ClientLoad($inp_file_name, $out_file_name, $final_file_name, $data_dir)
			Processes the generic input file, performing two vital functions on it:
			1: only processes record with a security ID that exists in client_assets.dat
			2: processes any markers in env_marker.cot

=head2  REVISIONS:
        RCR 11-APR-08 13354-0  RCR Added distribution table messages
        RCR 18-OCT-07 13354-0  RCR Changed processing of file name to ignore added date/time stamp after type
        RAM 01-SEP-05 13354-0  RCR Added load type and date to fatal messages for clarity to recipients
        RAM 29-AUG-05 13354-0  RCR Changed quotewords to parse_line
        RAM 28-JUN-05 13354-0  RCR Added NotifyCondition calls for VAULT directed mail
        RAM 16-FEB-05 13354-0  RCR Changed to correct spacing in ending message
        RAM 19-NOV-04 13354-0  RCR Changed to correct writing of final asset to ASSETTEMP file
        RAM 19-AUG-04 13354-0  RCR Changed to retain held asset when creating ASSETTEMP file
        RAM 04-AUG-04 13354-0  RCR Added date to name of asset_file_temp
        RAM 14-JUL-04 13354-0  RCR Corrected distribution of files code
        RAM 21-JUN-04 13354-0  RCR Changed to allow distribution of files
		RAM 07-MAY-04 13354-0  RCR Changed to allow checking to load only held assets
		RAM 04-MAY-04 13354-0  RCR Changed to also check entity_id for match with asset ID
		RAM 13-JAN-04 13354-0  JGF Changed client marker table to use environment as part of name
        RAM 07-NOV-03 13354-0  JGF Changed split of header record to pick up nulls
        RAM 09-JUL-02 13354-0  JGF Initial development
=cut

use strict;
use Cwd;
use ITSCheck;
use ITSDate;
use ITSFTP;
use File::Copy;
use vars qw(@ISA @EXPORT);

use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(ClientLoad);

sub ClientLoad ($$$$$) {

=head1 sub ClientLoad($$$$$)

=head2 ARGUMENTS:
	   0: $inp_file_name - name of the input file
	   1: $out_file_name - name of the output file
	   2: $final_file_name - name of the final file in the PACE drop-off directory
	   3: $dir data - directory name where the loader_marker_xxxxxx.tab table is located
	   4: $asset_file_name - name of the assets file

=cut

	# USE
	use strict;
	use ITSError;
	use ITSFatal;
	use ITSStat;
	use ITSTime;
	use ITSTrim;
	use MarkerTable;
	use Text::ParseWords;

	# CONSTANTS
	my $PROGRAM_NAME = "ClientLoad V1.35";
	my $PROGRESS_INTVL = 100000; 

	# SCALARS
	my $asset_file_name;       # name of asset file 
	my $asset_file_temp;       # name of temporary asset file 
	my $assetid = "";          # asset ID read from assets file
	my $assetid_eof = 0;       # reached eof on assetid file?
	my $assetid_exist = "";    # last asset ID that exists on feed
	my $assetid_last = "";     # last asset ID read from assets file
	my $assetline;             # asset line read from assets file
	my $current;               # current date/time
	my $data_dir;              # directory of data files for client processing
	my $data_dir_sl;           # directory of data files for client processing
	my $elapsec;               # elapsed time in seconds
	my $elapstr;               # elapsed time as a string
	my $env_name;              # environment name

	my $fh_logfile;            # file handle for LOGFILE
	my $filesok;               # number of files ok in distribution
	my $filesnotok;            # number of files not ok in distribution
	my $final_file_name;       # name of feed final file
	my $flg_copy;              # is file copy ok?
	my $flg_curr;              # is this run for currencies?
	my $flg_heldonly = 0;      # is this run for heldonly?
	my $flg_marker;            # do marker processing?
	my $flg_recok;             # is this record ok (without errors)?

	my $head;                  # header containing load type and date
	my $held;                  # is security held?
	my $held_last;             # last value of is security held?

	my $idxcurr = -1;          # index of the to_curr in the items
	my $idxid;                 # index of the real security ID in the items
	my $idxitm;                # index into the item names on the first line
	my $idxtyp;                # index into filename looking for type
	my $index = 0;             # index into item and data value arrays
	my $inp_file_name;         # name of feed input file
	my $inp_file_name_sh;      # name of feed input file without the directory
	
	my $length;                # length of input directory
	my $load_date;             # date of load
	my $load_type;             # type of load, for example, idsi320
	my $load_type_full;        # type of load, for example, idsi320:SMF
	my $log_file_name;         # name of output log file

	my $marker;                # marker name
	my $marker_value;          # marker value

	my $num_assets   = 0;      # assets read from asset file
	my $num_error    = 0;      # errors encountered
	my $num_items;             # number of items in file header record
	my $num_markers;           # number of markers in hash
	my $num_rejected = 0;      # number of file records rejected
	my $num_outs     = 0;      # records output so far
	my $num_recs     = 0;      # records read from input file
	my $num_skip     = 0;      # records skipped in file
	my $num_values;            # number of values in file data record

	my $out_file_name;         # name of feed output file
	my $outrec;                # output record buffer
	my $rectyp;                # record type
	my $rej_file_name;         # name of rejected records file
	my $rmstat;                # system remove return status
	my $sav_file_name;         # name of saved feed input file
	my $secid;                 # security ID from feed record
	my $sectype;               # security type from asset record
	my $sectype_last;          # last security type from asset record
	my $sectypehash;           # security type from asset record
	my $source;                # source location
	my $status;                # system return status
	my $table_name;            # name of loader table to be processed
	my $tables_dir;            # directory of tables for client processing
	my $valid;                 # is field valid?
	my $warn_buff;             # warning message buffer

	# ARRAYS
	my @assets_missing = ();   # buffer of missing asset IDs
	my @data_values;           # value of each column
	my @date_start;            # localtime array of start
	my @date_end;              # localtime array of end
	my @item_names;            # names of each column
	my @file_name_words;       # parse of file name

	# HASHES
	my %curr_ids;              # hash for currencies
	my %itemid;                # hash of item numbers in the array
	my %stathash;              # hash for statistics
	
	# get file names from input arguments
	($inp_file_name, $out_file_name, $final_file_name, $data_dir, $asset_file_name) = @_;

	# split up input file name into components and set up important parameters from them
	$length = rindex($inp_file_name, "/");
	if ($length < 0) {
		$length = rindex($inp_file_name, "\\");
	}
	$inp_file_name_sh = substr($inp_file_name, $length + 1);
	@file_name_words = split(/_|\./, $inp_file_name_sh);
	$load_type = $file_name_words[0];
	$load_date = $file_name_words[1];

	# get record type, skipping over any appended date/time stamp
	for ($idxtyp = ($#file_name_words - 1); $idxtyp >= 0; $idxtyp--) {
		$rectyp = $file_name_words[$idxtyp];
		if (!CheckDate($rectyp) && !CheckTime($rectyp)) { last; }      # end loop if $rectyp is not a date or a time
	}
	if ($rectyp eq "") { $rectyp = "unk";}                             # just to be safe

	$marker = "MK" . $load_type . "," . $rectyp;
	$load_type_full = $load_type . "_" . $rectyp;
	$sav_file_name = join("", substr($inp_file_name, 0, $length+1), $load_type, "_", $rectyp, ".dat");
	$tables_dir = join("", $data_dir, "/tables/");
	$head = " For Load Type: $load_type_full  Load Date: $load_date\n";

	# set environment and setup for directed mail
	$source = cwd;
	CheckDirectory($source, $env_name);
	SetMailOverride($tables_dir, $env_name);

	# open log file
	$log_file_name = join("", $data_dir, "/logs/ClientLoad_", $load_type_full, "_", $load_date, ".log");
	open(LOGFILE, "> $log_file_name") || 
		NotifyFatal("", $PROGRAM_NAME, "$head Could not open log file: $log_file_name", "VAULT", "ENS DIE");
	$fh_logfile = *LOGFILE;

	$current = GetDateTime;
	print LOGFILE "$PROGRAM_NAME starting at $current\n\n";
	print         "$PROGRAM_NAME starting at $current\n\n";
	@date_start = (localtime)[0..5];   # get 6 values we use of the localtime array to compute elapsed time and write statistics

	print LOGFILE "Load type is: $load_type_full\n";
	print LOGFILE "Load date is: $load_date\n\n";
	
	print LOGFILE "        Running on file: $inp_file_name\n\n";
	print         "        Running on file: $inp_file_name\n\n";
	open(INFILE, "< $inp_file_name") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open input file: $inp_file_name", "VAULT", "ENS DIE");

	print LOGFILE "    Opening output file: $out_file_name\n\n";
	print         "    Opening output file: $out_file_name\n\n";
	open(OUTFILE, "> $out_file_name") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open output file: $out_file_name", "VAULT", "ENS DIE");

	$asset_file_temp = join("", $data_dir, "/", $load_type, "_assets_", $load_date, "_", $rectyp, ".tmp");
	print LOGFILE " Opening assettemp file: $asset_file_temp\n\n";
	print         " Opening assettemp file: $asset_file_temp\n\n";
	open(ASSETTEMP, "> $asset_file_temp") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open assets temp file: $asset_file_temp", "VAULT", "ENS DIE");

	print LOGFILE "    Opening assets file: $asset_file_name\n\n";
	print         "    Opening assets file: $asset_file_name\n\n";
	open(ASSETFILE, "< $asset_file_name") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open assets file: $asset_file_name", "VAULT", "ENS DIE");

	# eliminate duplicates in asset file, retaining sectype and held, if available
	# then, open temp file as the asset file
	$assetid_last = "";
	$sectype_last = "";
	$held_last = "";

	while ($assetline = <ASSETFILE>) {
		chomp($assetline);
		TrimString($assetline);
		($assetid, $sectype, $held) = parse_line('\s+', 0, $assetline);
		# check for assets out of order or for tokens not defined
		if ($assetid lt $assetid_last) {
			NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head AssetID: $assetid not greater than last: $assetid_last", 
								"VAULT", "ENS DIE");
		}
		if (!defined($sectype)) { $sectype = ""; }
		if (!defined($held))    { $held = ""; }
		if ($assetid ne $assetid_last) {
			# new asset - print last one and reset values
			if ($assetid_last ne "") {
				print ASSETTEMP "$assetid_last \"$sectype_last\" $held_last\n";
			}
			$assetid_last = $assetid;
			$sectype_last = $sectype;
			$held_last = $held;
		}
		else {
			# same asset - check values and replace as necessary
			# if what we had was not held and what we have is held, use that
			if ($held_last ne "Y" && $held eq "Y") {
				$sectype_last = $sectype;
				$held_last    = $held;
			}
			# if what we had was null and we now have a value, use that
			if ($sectype_last eq "" && $sectype ne "") { $sectype_last = $sectype; }
			if ($held_last    eq "" && $held    ne "") { $held_last    = $held; }
		}
	}

	print ASSETTEMP "$assetid_last \"$sectype_last\" $held_last\n";
	close ASSETTEMP;
	close ASSETFILE;

	open(ASSETFILE, "< $asset_file_temp") || 
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open assets temp file", "VAULT", "ENS DIE");

	print LOGFILE "\n---------------------- TABLE SECTION -----------------------\n";

	# get loader marker table
	$table_name = join("", $tables_dir, $env_name, "_marker.cot");
	print LOGFILE "    Loader Marker table: $table_name\n";
	CreateMarkerHash($table_name);
	$num_markers = scalar(keys(%MarkerDefinitionHash)); 
	$flg_marker = ($num_markers >= 1); 
	print LOGFILE "      Markers available: $num_markers\n";
	print         "      Markers available: $num_markers\n";

	# check for heldonly file and set flag
	$table_name = join("", $tables_dir, $env_name, "_heldonly.txt");
	if (open(HELDONLY, "< $table_name")) { 
		$flg_heldonly = 1;
		close(HELDONLY);
	}
	print LOGFILE "       Heldonly flag is: $flg_heldonly\n";
	print         "       Heldonly flag is: $flg_heldonly\n";

	print LOGFILE "\n-------------------- PROCESSING SECTION --------------------\n";
	
	# read first record from input file and get item names
	my $line = <INFILE>;
	chomp($line);
	TrimString($line);
	$num_recs++;
	@item_names = split(/\|/, $line, -1);
	$num_items = $#item_names; 
	if ($num_items < 0) {
		LineError($fh_logfile, "First record of input file", $line);
		NotifyFatal($fh_logfile, $PROGRAM_NAME, 
			"$head There were no items in the first record of the input file", "VAULT", "ENS DIE");
	}

	# find real security id in the items list - must be "security_id", "entity_id", or "from_curr"
	for $idxitm (0 .. $num_items) {
		if (($item_names[$idxitm] eq "security_id") ||
		    ($item_names[$idxitm] eq "entity_id")   ||
		    ($item_names[$idxitm] eq "from_curr"  )) {
			$idxid = $idxitm;
			last;        # found id field - idxid is the position
		}
	}
	# if no item found matching security id item - error
	if (!defined($idxid)) {
		LineError($fh_logfile, "First record of input file", $line);
		NotifyFatal($fh_logfile, $PROGRAM_NAME, 
			"$head No security_id, entity_id, or from_curr item in the header", "VAULT", "ENS DIE");
	}

	# find "to_curr" in the items list
	for $idxitm (0 .. $num_items) {
		if ($item_names[$idxitm] eq "to_curr") {
			$idxcurr = $idxitm;
			last;        # found to_curr - idxcurr is the position
		}
	}
	# set flag for currency run
	$flg_curr = ($idxcurr >= 0);
	printf LOGFILE "      Currency flag is: %s\n\n", $flg_curr == 1 ? "on" : "off";
	printf         "      Currency flag is: %s\n\n", $flg_curr == 1 ? "on" : "off";

	# if a currency run, get all asset IDs into hash
	if ($flg_curr) {
		while ($assetline = <ASSETFILE>) { 
			chomp($assetline);
			($assetid, $sectype, $held) = parse_line(" ", 0, $assetline);
			TrimString($assetid);
			$curr_ids{ $assetid } = 1;
			$num_assets++;
		}
	}

	# match marker hash items with item names
	if ($flg_marker) {
		GETVALUE: foreach $marker_value (keys %{$MarkerDefinitionHash{ $marker } } ) {
			for ($index = 0; $index < $num_items; $index++) {
				if ($item_names[$index] eq $marker_value) {
					$itemid{ $marker_value } = $index;       # store index number of this item in itemid
					next GETVALUE;
				}
			}
			# no item found matching item in marker table - error
			LineError($fh_logfile, "First record of input file", $line);
			NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Marker value \"$marker_value\" was not found in the file items header", 
									  "VAULT", "ENS DIE");
		}
	}

	# main loop - read and process a record from the input file
	$assetid = "";          # asset ID read from assets file
	$assetid_last = "";     # last asset ID read from assets file
	$warn_buff = " Warning - assets not on feed: ";

	GETRECORD: while (my $line = <INFILE>) {
		chomp($line);

		# count line and print progress message
		$num_recs++;
		if ($num_recs % $PROGRESS_INTVL == 0) {
			$current = GetDateTime;
			print "Processing at $current  Inprec: $num_recs   Outrec: $num_outs\n";
		}

		@data_values = split(/\|/, $line, -1);
		$num_values = $#data_values; 
		if ($num_values != $num_items) {
			LineError($fh_logfile, "Input line", $line);
			NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Number of values $num_values does not match number of items $num_items", 
									   "VAULT", "ENS DIE");
		}

		# check asset list to see if this ID should be loaded
		$secid = $data_values[$idxid];        # get security id

		if ($flg_curr) {
			# for currencies, check both ids against hash
			if (!exists($curr_ids{ $secid }))                 { next GETRECORD; }
			if (!exists($curr_ids{ $data_values[$idxcurr] })) { next GETRECORD; }
			$sectype = "CASH CURRENCY";
			$held = 1;   # currencies marked as always held
		}
		else {
			# for all else, check id against assets file
			while ($assetid lt $secid) {          # asset ID is lower - get next record from assets file
				if ($assetid ne "") {
					if ($assetid ne $assetid_exist) {
						# asset id missing from feed - push to buffer and write out if buffer full
						push (@assets_missing, $assetid);
						if ($#assets_missing >= 5) {
							MissingWarning($fh_logfile, @assets_missing);
						}
					}
				}
				if ($assetline = <ASSETFILE>) {
					$num_assets++;
					chomp($assetline);
					($assetid, $sectype, $held) = parse_line(" ", 0, $assetline);
					if (!defined($sectype)) { $sectype = "UNKNOWN"; }
					if ($sectype eq "")     { $sectype = "UNKNOWN"; }
					if (!defined($held))    { $held = 0; }
					$held = ($held eq "Y");
					if ($assetid le $assetid_last) {
						NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head AssetID: $assetid not greater than last: $assetid_last", 
											"VAULT", "ENS DIE");
					}
					if ($flg_heldonly && ! $held) {
						$assetid = "";                    # heldonly is on and it's not held - get rid of it
					}
					$assetid_last = $assetid;
				}
				else {
					$assetid_eof = 1;             # set eof flag
					last GETRECORD;               # if at end of assets file, end loader loop
				}
			}
			if ($assetid gt $secid) {             # asset ID is greater - skip this feed record and get another one
				$num_skip++;
				next GETRECORD;
			}
		}

		# asset exists, so we have valid asset - loop through markers
		$assetid_exist = $assetid;
		$flg_recok = 1;                       # record is ok so far
		if ($flg_marker) {
			foreach $marker_value (keys %{$MarkerDefinitionHash{ $marker } } ) {
				$index = $itemid { $marker_value };
				# process marker on the data value pointed to by index
				$valid = ProcessMarker($marker, $marker_value, $data_values[$index]);
				if (!$valid) {
					FieldError ($fh_logfile, $num_recs, $rectyp, $secid, $marker_value, $data_values[$index], 
								"Processing marker $marker", $num_error);
					$flg_recok = 0;        # record marked as not ok
				}
			}
		}                       

		# done processing fields within record 
		if ($flg_recok) {
			# remove security ID from front of array
			shift(@data_values);
			# input record is ok - join the data values and put them out
			$outrec = join("|", @data_values) . "\n";
			print OUTFILE "$outrec";
			$num_outs++;
			# increment security type counter
			$sectypehash = "STYP:" . $sectype;
			if (exists($stathash{ $sectypehash })) {
				$stathash{ $sectypehash }++;           # exists in hash of security types - bump counter
			}
			else {
				$stathash{ $sectypehash } = 1;         # not in hash of security types - add to hash with counter of 1
			}
		}
		else {
			# input record is not ok - write to rejected file
			if ($num_rejected == 0) {
				# on first record, must open rejected file
				$rej_file_name = join("", $data_dir, "/logs/", $load_type_full, "_", $load_date, "_rejected.cli");
				open(REJFILE, "> $rej_file_name") || 
				NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not open rejected file: $rej_file_name", "VAULT", "ENS DIE");
			}
			print REJFILE $line;
			$num_rejected++;
		}
	}         # end of GETRECORD while loop

	if (!$flg_curr) {
		if (!$assetid_eof) {
			while ($assetline = <ASSETFILE>) {
				$num_assets++;
				chomp($assetline);
				($assetid) = parse_line(" ", 0, $assetline);
				if ($assetid ne $assetid_exist) {
					# asset id missing from feed - push to buffer and write out if buffer full
					push (@assets_missing, $assetid);
					if ($#assets_missing >= 5) {
						MissingWarning($fh_logfile, @assets_missing);
					}
				}
			}
		}
		MissingWarning($fh_logfile, @assets_missing);
	}

	# now at end of file - print statistics, close files

	print LOGFILE "\n--------------------- SUMMARY SECTION ----------------------\n";

	print  LOGFILE "\n          Error Summary:\n";
	print          "\n          Error Summary:\n";
	printf LOGFILE   "     Field Errors total: %10i\n", $num_error;
	printf           "     Field Errors total: %10i\n", $num_error;
	printf LOGFILE   " Rejected Records total: %10i\n", $num_rejected;
	printf           " Rejected Records total: %10i\n", $num_rejected;

	print  LOGFILE "\n     Processing Summary:\n";
	print          "\n     Processing Summary:\n";
	printf LOGFILE   "    Asset Records total: %10i\n", $num_assets;
	printf           "    Asset Records total: %10i\n", $num_assets;
	printf LOGFILE   "    Input Records total: %10i\n", $num_recs;
	printf           "    Input Records total: %10i\n", $num_recs;
	printf LOGFILE   "  Skipped Records total: %10i\n", $num_skip;
	printf           "  Skipped Records total: %10i\n", $num_skip;
	printf LOGFILE   "   Output Records total: %10i\n", $num_outs;
	printf           "   Output Records total: %10i\n", $num_outs;

	# close files and delete temporary asset file
	if ($num_rejected > 0) { close REJFILE; } 
	close INFILE;
	close OUTFILE;
	close ASSETFILE;
	unlink($asset_file_temp);

	# copy file to final destination
	$current = GetDateTime;
	print         "\n$PROGRAM_NAME copying at $current\n";

	$status = 0;
	$table_name = $tables_dir . $env_name . "_distribution.tab";
	printf LOGFILE "\nDistribution table name is: %s\n", $table_name;

	if (-e $table_name) { 
		print LOGFILE "\nDistribution table exists\n";
		# distribution table exists - must try FTP distribution
		DistributeClient($fh_logfile, $PROGRAM_NAME, "VAULT", $tables_dir, "",
						$load_date, $rectyp, $out_file_name, $env_name, $filesok, $filesnotok);
		print LOGFILE "Distributed file: $rectyp  Succeeded: $filesok  Failed: $filesnotok\n";
		print         "Distributed file: $rectyp  Succeeded: $filesok  Failed: $filesnotok\n";
		$status = (($filesok > 0) && ($filesnotok == 0));
	}
	if (!$status) {
		# distribution table not present or file type not in distribution table - normal copy to final file name
		$status = copy("$out_file_name", "$final_file_name");
		print LOGFILE   "Output copied to $final_file_name\n";
		print           "Output copied to $final_file_name\n";
	}
	if ($status) {
		# copy was successful - delete intermediate files
		$rmstat = rename("$inp_file_name", "$sav_file_name");
		$rmstat = unlink("$out_file_name");
		$flg_copy = 1;

		# call for transmit condition
		NotifyCondition($fh_logfile, $PROGRAM_NAME, "TRANSMIT", $load_type_full, "File: $out_file_name transmited", "");
	}
	else {
		NotifyFatal($fh_logfile, $PROGRAM_NAME, "$head Could not copy to $final_file_name", "VAULT", "ENS");
		$flg_copy = 0;
	}

	# check to see if nodata condition existed for output file
	if ($num_outs <= 0) {
		NotifyCondition($fh_logfile, $PROGRAM_NAME, "NODATA", $load_type_full, "File: $final_file_name contained no data", "");
	}

	# call for succeed condition
	NotifyCondition($fh_logfile, $PROGRAM_NAME, "SUCCEED", $load_type_full,
		"\nOriginal file: $inp_file_name  \n   Final name: $final_file_name", "");

	# get current local time, compute elapsed time, and write out statistics
	@date_end = (localtime)[0..5];   # get 6 values we use of the localtime array to compute elapsed time and write statistics
	CalcElapsed(@date_start, @date_end, $elapsec, $elapstr);
	$data_dir_sl = $data_dir . "/";
	WriteStatistics($load_type_full, $load_date, @date_start, @date_end, $elapstr, $num_recs, $num_outs, $num_error, 
				$flg_copy, 1 - $flg_copy, $tables_dir, $data_dir_sl, %stathash, $fh_logfile);

	print LOGFILE "\n$PROGRAM_NAME total elapsed time: $elapstr\n";
	print         "\n$PROGRAM_NAME total elapsed time: $elapstr\n";

	$current = GetDateTime;
	print         "\n$PROGRAM_NAME   ending at $current\n";
	print LOGFILE "\n$PROGRAM_NAME   ending at $current\n";
	close LOGFILE;
	1;
}
