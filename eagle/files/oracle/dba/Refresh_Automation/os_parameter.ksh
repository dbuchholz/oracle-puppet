#!/bin/ksh
touch /u01/app/oracle/test.txt
grep oracle /etc/security/limits.conf >> /u01/app/oracle/test.txt
grep fs.suid_dumpable /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep fs.aio-max-nr /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep fs.file-max /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep kernel.shmall /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep kernel.shmmax  /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep kernel.shmmni /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep kernel.sem /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep net.ipv4.ip_local_port_range /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep net.core.rmem_default /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep net.core.rmem_max /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep net.core.wmem_default /etc/sysctl.conf >> /u01/app/oracle/test.txt
grep net.core.wmem_max  /etc/sysctl.conf >> /u01/app/oracle/test.txt

