set lines 140
set pages 1000
col event format a60

select event,count(*) from v$session_wait group by event order by 2
/

col event format a50
col sql format a60
col state format a12

select distinct w.sid,w.event,substr(w.state,1,12) state,
substr(q.sql_text,1,25)||'.....'||substr(q.sql_text,instr(q.sql_text,'FROM',1),25) sql
from v$session_wait w,v$session s,v$sql q where w.event like '%&event%'
and w.sid=s.sid
and s.SQL_HASH_VALUE=q.HASH_VALUE
and s.status='ACTIVE'
and s.username is not null
order by 2
/

