drop role EA_INSTALL_ROLE;
create role EA_INSTALL_ROLE;
grant CREATE ANY PROCEDURE,ALTER ANY PROCEDURE,CREATE ANY VIEW,SELECT ANY TABLE,CREATE SESSION to EA_INSTALL_ROLE;

drop user abanerjee cascade;
CREATE USER abanerjee
PROFILE EA_PROFILE
IDENTIFIED BY abanerjee_KLL1ti52 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO abanerjee;
audit all by abanerjee by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by abanerjee by access;

drop user akhaladkar cascade;
CREATE USER akhaladkar
PROFILE EA_PROFILE
IDENTIFIED BY akhaladkar_i6H2Bb3v PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO akhaladkar;
audit all by akhaladkar by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by akhaladkar by access;

drop user bswain cascade;
CREATE USER bswain
PROFILE EA_PROFILE
IDENTIFIED BY bswain_He8k5yt3 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO bswain;
audit all by bswain by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by bswain by access;

drop user ctaft cascade;
CREATE USER ctaft
PROFILE EA_PROFILE
IDENTIFIED BY ctaft_nV2gw8sb PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO ctaft;
audit all by ctaft by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by ctaft by access;

drop user dbelambe cascade;
CREATE USER dbelambe
PROFILE EA_PROFILE
IDENTIFIED BY dbelambe_x312F1t6 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO dbelambe;
audit all by dbelambe by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by dbelambe by access;

drop user gsethi cascade;
CREATE USER gsethi
PROFILE EA_PROFILE
IDENTIFIED BY gsethi_wBv86x2l PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO gsethi;
audit all by gsethi by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by gsethi by access;

drop user gdesista cascade;
CREATE USER gdesista
PROFILE EA_PROFILE
IDENTIFIED BY gdesista_wt22nO9c PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO gdesista;
audit all by gdesista by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by gdesista by access;

drop user jdoran cascade;
CREATE USER jdoran
PROFILE EA_PROFILE
IDENTIFIED BY jdoran_B1gM28gL PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO jdoran;
audit all by jdoran by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by jdoran by access;

drop user jwright cascade;
CREATE USER jwright
PROFILE EA_PROFILE
IDENTIFIED BY jwright_w463SFyV PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO jwright;
audit all by jwright by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by jwright by access;

drop user mdubey cascade;
CREATE USER mdubey
PROFILE EA_PROFILE
IDENTIFIED BY mdubey_g5n1NA69 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO mdubey;
audit all by mdubey by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by mdubey by access;

drop user mporeddy cascade;
CREATE USER mporeddy
PROFILE EA_PROFILE
IDENTIFIED BY mporeddy_7JKb3cw3 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO mporeddy;
audit all by mporeddy by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by mporeddy by access;

drop user pkar cascade;
CREATE USER pkar
PROFILE EA_PROFILE
IDENTIFIED BY pkar_8uYn63HM PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO pkar;
audit all by pkar by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by pkar by access;

drop user pkumar cascade;
CREATE USER pkumar
PROFILE EA_PROFILE
IDENTIFIED BY pkumar_jh6Tr22s PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO pkumar;
audit all by pkumar by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by pkumar by access;

drop user psherry cascade;
CREATE USER psherry
PROFILE EA_PROFILE
IDENTIFIED BY psherry_a0K38p8b PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO psherry;
audit all by psherry by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by psherry by access;

drop user racharya cascade;
CREATE USER racharya
PROFILE EA_PROFILE
IDENTIFIED BY racharya_j7Jgf4dM PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO racharya;
audit all by racharya by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by racharya by access;

drop user rdey cascade;
CREATE USER rdey
PROFILE EA_PROFILE
IDENTIFIED BY rdey_nTG4926g PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO rdey;
audit all by rdey by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by rdey by access;

drop user rverity cascade;
CREATE USER rverity
PROFILE EA_PROFILE
IDENTIFIED BY rverity_2x6l5pKk PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO rverity;
audit all by rverity by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by rverity by access;

drop user pjedhe cascade;
CREATE USER pjedhe
PROFILE EA_PROFILE
IDENTIFIED BY pjedhe_O18Nz4w2 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO pjedhe;
audit all by pjedhe by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by pjedhe by access;

drop user ssingh cascade;
CREATE USER ssingh
PROFILE EA_PROFILE
IDENTIFIED BY ssingh_q74FnhT6 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO ssingh;
audit all by ssingh by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by ssingh by access;

drop user stam cascade;
CREATE USER stam
PROFILE EA_PROFILE
IDENTIFIED BY stam_mb345UbV PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO stam;
audit all by stam by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by stam by access;

drop user spatankar cascade;
CREATE USER spatankar
PROFILE EA_PROFILE
IDENTIFIED BY spatankar_mbrxqWe3 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO spatankar;
audit all by spatankar by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by spatankar by access;

drop user tfennema cascade;
CREATE USER tfennema
PROFILE EA_PROFILE
IDENTIFIED BY tfennema_NH7fe6T1 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO tfennema;
audit all by tfennema by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by tfennema by access;

drop user yshaik cascade;
CREATE USER yshaik
PROFILE EA_PROFILE
IDENTIFIED BY yshaik_mbrxqWe3 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;
GRANT EA_INSTALL_ROLE TO yshaik;
audit all by yshaik by access;
audit ALTER SEQUENCE, ALTER TABLE, DELETE TABLE, EXECUTE PROCEDURE, GRANT DIRECTORY, GRANT PROCEDURE, GRANT SEQUENCE, GRANT TABLE,GRANT TYPE,INSERT TABLE,LOCK TABLE,UPDATE TABLE by yshaik by access;
