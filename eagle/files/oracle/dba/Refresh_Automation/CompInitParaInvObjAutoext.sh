#!/bin/ksh
# ==================================================================================================
# NAME:		CompInitParaInvObjAutoext.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:  This script will check:
#			1. The Difference of init parameters between Source and Target database.
#			2. Invalid Objects.
#			3. The Datafile and Tempfile with autoextensible NO and set the autoextensible on. 
#			
#
# USAGE:	CompInitParaInvObjAutoext.sh  <ORACLE_SID>
#
#
# ==================================================================================================
function funct_check_initpara
{
print "\n\nChecking the Difference of init parameter between Source and Target Database...\n\n"
read source_sid?"Please Enter the name of source database from where the $ORACLE_SID has been refreshed: "
if [[ -n $source_sid ]]
then
 DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off
 set feedback off
 select sid from inf_monitor.databases
 where sid='$source_sid'
 and dataguard='N'
 and status='OPEN';
EOF`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here...."
 exit 4
fi

DbIns=$(print $DbIns|tr -d " ")
if [[ -n $DbIns ]]
then
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set lines 215 pages 9999 trimspool on feedback off
 col PARAMETER for a30
 col pvalue for a40
 spool $REF_HOME/${ORACLE_SID}_scripts/initparadiff_${ORACLE_SID}_${DbIns}.txt
 select d.sid, dp.PARAMETER,dp.PVALUE, d1.sid, dp1.PARAMETER, dp1.PVALUE 
 FROM DB_PARAMETERS dp, DB_PARAMETERS dp1, databases d, databases d1
 WHERE d.sid='$ORACLE_SID' and d1.sid='$DbIns'
 AND d.instance=dp.db_instance 
 AND d1.instance=dp1.db_instance
 AND dp.PARAMETER = dp1.PARAMETER
 AND dp.PVALUE != dp1.PVALUE
 AND dp.PARAMETER not in ('instance_name','log_archive_dest_1', 'sp-file','utl_file_dir','db_name','db_domain','db_unique_name') and
 dp.parameter not like ('%dest') and
 dp.parameter not like ('dg_%')
 ORDER BY dp.PARAMETER;
 spool off
EOF
 if [[ -s $REF_HOME/${ORACLE_SID}_scripts/initparadiff_${ORACLE_SID}_${DbIns}.txt ]]
 then
  print "\n\nAlso saved The spool file initparadiff_${ORACLE_SID}_${DbIns}.txt  under $REF_HOME/${ORACLE_SID}_scripts for this...Please check\n" 
  else
  print "\n\nNot found any differece in init parameters for the source and target datbase which you mentioned\n"
  fi
else
 print "\n\nYou have entered the Wrong source Database name....init.ora parameter not checked\n"
fi
else
 print "\n\nPlease check the name of source database you have entered...init.ora parameter not checked\n"
fi
} 



function funct_invalid_objects
{
print "\n\nChecking for Invalid Objects....\n\n"
sleep 1
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 select count(*) from dba_objects where status='INVALID';
 col OBJECT_NAME format a30
 set linesize 132
 set pagesize 100
 select object_name,owner,object_type from dba_objects where status='INVALID';
EOF
}

function funct_Autoextend_on
{
print "\n\nChecking Datafiles and Tempfiles with autoextensible NO....\n\n"
sleep 1
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 spool $REF_HOME/${ORACLE_SID}_scripts/autoextend_${ORACLE_SID}.txt
 set heading off
 set feedback off
 select 'alter database datafile '||''''||file_name||''''||' autoextend on next 100M maxsize unlimited;'
 from dba_data_files 
 where autoextensible='NO'          
 union all
 select 'alter database tempfile '||''''||file_name||''''||' autoextend on next 100M maxsize unlimited;'
 from dba_temp_files
 where autoextensible='NO';
 spool off
EOF
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/autoextend_${ORACLE_SID}.txt ]]
then
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 @$REF_HOME/${ORACLE_SID}_scripts/autoextend_${ORACLE_SID}.txt
EOF
 print "\nAll Datafiles and Tempfiles are set with autoextend on\n"
else
 print "\n\nThere are no Datafiles and Tempfiles with autoextensible NO\n\n"
fi
}

#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi


export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

 funct_check_initpara
 
 funct_invalid_objects

 funct_Autoextend_on

print "\n\n*******SUCCESSFUL*******\n\n"
exit 0
