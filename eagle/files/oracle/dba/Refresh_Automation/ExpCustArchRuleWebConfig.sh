#!/bin/ksh
# ==================================================================================================
# NAME:         ExpCustArchRuleWebConfig.sh
#
# AUTHOR:       Basit Khan
#
# PURPOSE:      This script will export the data for custom archive rule and web configuration
#                       For MFC and TER it will export all the pace application users
#
# USAGE:        ExpCustArchRuleWebConfig.sh  <ORACLE_SID>
#
#
# ==================================================================================================
funct_get_pass()
{
#uncomment the below line to debug
#set -x
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select instance from inf_monitor.databases
 where sid='$ORACLE_SID'
 and dataguard='N'
 and status='OPEN';
EOF`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the password for pace
Aborting Here...."
 exit 3
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 passwd=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select inf_monitor.bb_get(nvl(temp_code,code))
 from inf_monitor.user_codes
 where db_instance=$DbIns
 and username='$appuser';
EOF`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
  print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 4
 fi
fi
passwd=$(print $passwd|tr -d " ")
}
funct_get_environment()
{
#uncomment the below line to debug
#set -x
 ${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF>$REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
 set heading off
 set feedback off
 select sid from inf_monitor.databases
 where sid like '%mfc%'
 and dataguard='N'
 and sid not like '%prd%'
 union all
 select sid from inf_monitor.databases
 where sid like '%ter%'
 and dataguard='N'
 and sid not like '%prd%'
union all
select sid from inf_monitor.databases where sid like '%nwi%' and
dataguard='N';
EOF
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the password for pace
Aborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 5
fi
cat $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt | sed -e '/^$/d' > $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt
rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
}

#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 2
fi

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

 print "\nExporting Custom Archive Rule and Web Configuraton tables.............................Please Wait"
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

 export appuser=PACE_MASTERDBO
 funct_get_pass
 export pace_pass=$passwd
 export pace_user=$appuser
 export appuser=ESTAR
 funct_get_pass
 export star_pass=$passwd
 export star_user=$appuser
##########added for BrandyWine Dataexchange requirement####
 export appuser=DATAEXCHDBO
 funct_get_pass
 export dxch_pass=$passwd
 export dxch_user=$appuser
###########################################################

 funct_get_environment
 export exst_sid=$(cat $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt | grep $ORACLE_SID)
 if [[ -n $exst_sid ]]
 then
  $ORACLE_HOME/bin/exp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pace_tablesExp.log tables=pace_system,custom_archive_rules,pace_users,pace_user_groups,pace_user_role_details,starsec_properties,starsec_group_detail,starsec_group_rel,starsec_group_sum,starsec_user_detail,starsec_user_group,user_client_maintenance,user_entity_maintenance statistics=none  > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Custom Archive Rule Tables...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 6
fi

  grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/pace_tablesExp.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
print "\n\n***************************************FAILURE*********************************************\n\n"
print "Check the log $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log for more info."
exit 7
fi

  $ORACLE_HOME/bin/exp $star_user/$star_pass file=$REF_HOME/${ORACLE_SID}_scripts/estar_web_conf.dmp log=$REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log tables=web_configuration statistics=none  > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 8
fi
  grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
print "\n\n***************************************FAILURE*********************************************\n\n"
print "Check the log $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log for more info."
exit 9
fi

 else
  $ORACLE_HOME/bin/exp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.log tables=pace_system,custom_archive_rules statistics=none > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Custom Archive Rule Tables...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 10
fi

  grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
print "\n\n***************************************FAILURE*********************************************\n\n"
print "Check the log $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log for more info."
exit 11
fi

  $ORACLE_HOME/bin/exp $star_user/$star_pass file=$REF_HOME/${ORACLE_SID}_scripts/estar_web_conf.dmp log=$REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log tables=web_configuration statistics=none > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 12
fi

  grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
print "\n\n***************************************FAILURE*********************************************\n\n"
print "Check the log $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log for more info."
exit 13
fi

#########For BrandyWine dataexchange request##############

 export brwsid=$(print $ORACLE_SID | cut -c1-6)

 if [ $brwsid == 'brwdev' -o $brwsid == 'brwtst' ]
 then

 $ORACLE_HOME/bin/exp $dxch_user/$dxch_pass  file=$REF_HOME/${ORACLE_SID}_scripts/acct_info.dmp log=$REF_HOME/${ORACLE_SID}_scripts/acct_info.log tables=acct_info statistics=none > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
   print "\n\n\t\tThere is Some Problem while Exporting DATAEXCHANGE  Configuration Table...Please check\n\nAborting Here...."
   print "\n\n***************************************FAILURE*********************************************\n\n"
  exit 14
  fi

  grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/acct_info.log > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
   print "\n\n\t\tThere is Some Problem while Exporting DATAEXCHANGE Configuration Table...Please check\n\nAborting Here...."
   print "\n\n***************************************FAILURE*********************************************\n\n"
   print "Check the log $REF_HOME/${ORACLE_SID}_scripts/acct_info.log for more info."
  exit 15
  fi

 fi
#########################################################

 print "\n\nLog files are under $REF_HOME/${ORACLE_SID}_scripts for More Details"
 print "\n\n\t\t\t*************SUCCESSFULL**************\n\n"
 rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt
 fi

exit 0
