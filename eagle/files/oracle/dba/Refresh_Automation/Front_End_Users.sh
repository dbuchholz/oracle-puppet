#!/bin/ksh
# ==================================================================================================
# NAME:		Front_End_Users.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will Generate the spool file for front end users.
#
# USAGE:	Front_End_Users.sh  <ORACLE_SID>
#
#
# ==================================================================================================

#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi


export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export cdate=$(date +'%Y%m%d_%H%M')

 $ORACLE_HOME/bin/sqlplus -s '/as sysdba '<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 alter session set nls_date_format='MM DD YYYY HH24:MI:SS';
 set lines 215 pages 9999 trimspool on
 col USERNAME format a36
 col PASSWORD format a20
 col UPD_USER format a20
 col upd_datetime format a20
 col passwordsetdate format a20
 col create_date format a20
 col last_op_time format a20
 col "LOCKED BY" format a50
 spool $REF_HOME/${ORACLE_SID}_scripts/front_end_users_$cdate.txt
 select trim(username) "USERNAME",trim(password) "PASSWORD",trim(upd_user) "UPD_USER",create_date,passwordsetdate,upd_datetime,last_op_time,trim(lockedby) "LOCKED BY" from pace_masterdbo.pace_users order by USERNAME;
 spool off
EOF

 print "\n\nSpool file front_end_users_$cdate.txt kept under $REF_HOME/${ORACLE_SID}_scripts...Please Check...\n\n"
 print "********************************SUCCESSFULL*****************************************\n\n"
