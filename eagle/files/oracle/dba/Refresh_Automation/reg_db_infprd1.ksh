#!/bin/ksh

# Author : Nikhil Kulkarni 
# Notes : This script registers a new db in databases and user_codes in Big Brother and changes their paswd to a temporary one.For new db created from blank template the users are taken from an existing Prod db of same product version(If prod db is not present for this version then dev or test db's users are copied in BigB). For new db created from existing db users are taken from source db
#              


#########################################################################


#####function to generate sqls with the permanent passwords and register the users in user_codes@BigB 

function register_users
{
echo -e "

**************************************************

"
if [[ $tgt_env = 'dr' ]]
then
echo -e "

Passwords for DR are to be set according to Production and only sys and system is registered in user_codes@BigB...Initially temporary password will be set to $temp_paswd

Press Enter to proceed

"
elif [[ $tgt_env = 'parprod' ]]
then
echo -e "
Passwords for parallel prod are to be set according to Production ...Initially  temporary password will be set to $temp_paswd

Press Enter to proceed

"
else
cat ${script_dir}/password_policy.txt 
echo -e "

--  Passwords will be set according to PASSWORD POLICY ABOVE  -- 

   ( You can refer this CONFIDENTIAL policy file at the script location )

--- Waiting till you Press ENTER so that you can read this..... zoom in on the $tgt_env section for our situation 

"
fi

read x
clear



if [ $tgt_env = 'dev' ]
then
sqlplus -s / as sysdba<<eof
set head off feed off lines 200 pages 999 trimspo on
alter session set current_schema=inf_monitor;
spool ${log_dir}/ins_uc_new_${db}_$$.sql
select 'insert into user_codes values(''$new_inst'','''||username||''',bb_store(''$clntcod'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','CAMRA'),1,1)||'88'||decode(username,'SYS','1_dba9','SYSTEM','2_dba9','_dev9')||'''),bb_store(''$temp_paswd''),'''','''');' from user_codes where db_instance=$src_inst;
spool off
eof
fi



if [ $tgt_env = 'test' ]
then
sqlplus -s / as sysdba<<eof
set head off feed off lines 200 pages 999 trimspo on 
alter session set current_schema=inf_monitor;
spool ${log_dir}/ins_uc_new_${db}_$$.sql
select 'insert into user_codes values(''$new_inst'','''||username||''',bb_store(''$clntcod'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','CAMRA'),1,1)||'11'||decode(username,'SYS','1_dba9','SYSTEM','2_dba9','_tst9')||'''),bb_store(''$temp_paswd''),'''','''');' from user_codes where db_instance=$src_inst;
spool off 
eof
fi



if [ $tgt_env = 'prod' ]
then
sqlplus -s / as sysdba<<eof
set head off feed off lines 200 pages 999 trimspo on
alter session set current_schema=inf_monitor;
spool ${log_dir}/ins_uc_new_${db}_$$.sql
select 'insert into user_codes values(''$new_inst'','''||username||''',bb_store(''$clntcod'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','CAMRA'),1,1)||'59'||decode(username,'SYS','1_dba2','SYSTEM','2_dba2','_prd2')||'''),bb_store(''$temp_paswd''),'''','''');' from user_codes where db_instance=$src_inst;
spool off
eof
fi

if [ $tgt_env = 'parprod' ]
then
sqlplus -s / as sysdba<<eof
set head off feed off lines 200 pages 999 trimspo on
alter session set current_schema=inf_monitor;
spool ${log_dir}/ins_uc_new_${db}_$$.sql
select 'insert into user_codes values(''$new_inst'','''||username||''',bb_store('''||bb_get(code)||'''),bb_store(''$temp_paswd''),'''','''');' from user_codes  where db_instance=$src_inst ;
spool off
eof
fi


if [ $tgt_env = 'dr' ]
then
echo -e "
Setting passwds for an open DR 
"

##Only sys and system users registered for a DR 

sqlplus -s / as sysdba<<eof
set lines 180 pages 999 trimspo on feed off head off 
spool ${log_dir}/ins_uc_new_${db}_$$.sql
alter session set current_schema=inf_monitor;
select 'insert into user_codes values(''$new_inst'','''||username||''',bb_store('''||bb_get(code)||'''),bb_store(''$temp_paswd''),'''','''');' from user_codes  where db_instance=$src_inst and username in ('SYS','SYSTEM');
prompt commit;; 
spool off
eof
fi

egrep -iq 'ORA-|SP2-' ${log_dir}/ins_uc_new_${db}_$$.sql
if [ $? -eq 0 ]
then
echo -e "
 ** Check ${log_dir}/ins_uc_new_${db}_$$.sql for errors ... Note actions taken so far
Exiting ....
"
exit 2
fi

sqlplus -s / as sysdba<<eof
set feed off head off
alter session set current_schema=inf_monitor;
set lines 200 pages 999 trimspo on feed on head off echo on
spool ${log_dir}/ins_uc_new_${db}_$$.log
@${log_dir}/ins_uc_new_${db}_$$.sql
spool off 
eof

egrep -iq 'ORA-|SP2-' ${log_dir}/ins_uc_new_${db}_$$.log
if [ $? -eq 0 ]
then
echo -e " 

** Check ${log_dir}/ins_uc_new_${db}_$$.log for errors ... Note actions taken so far
Exiting ....
"
exit 2
fi

echo "

****  Users registered  successfully in user_codes@BigBrother 

"

}
###Function to register users of new db in BigB ends 




#####Asks sys passwd set with orapwd and then logs in to newly created db and changes to temp paswd
function alter_tempas
{
#### SPOOLING the sql to alter to temp paswd 
sqlplus -s / as sysdba<<eof
set head off feed off lines 200 pages 999 trimspo on
alter session set current_schema=inf_monitor;
spool ${log_dir}/alter_tempass_${db}_$$.sql
select 'alter user '||username||' identified by $temp_paswd ;' from user_codes where db_instance='$src_inst';
spool off
eof
egrep -iq 'ORA-|SP2-' ${log_dir}/alter_tempass_${db}_$$.sql
if [ $? -eq 0 ]
then
echo -e "
 ** Check ${log_dir}/alter_tempass_${db}_$$.sql for errors ... Note actions taken so far

Exiting ....
"
exit 2
fi

echo -e "
Running the above SQLs to alter password by connecting to db on $svr
"
export x=`ssh -nq $svr date +%d%m%y|awk '{print $1}'`
export y=`date +%d%m%y|awk '{print $1}'`

if [[ $x != $y ]]
then
echo -e "
Issues in ssh to $svr... Run above SQLs for altering temp pass by logging in $tgt_env $db on $svr

( These SQLs are spooled in ${log_dir}/alter_tempass_${db}_$$.sql )

"
exit 002
fi

echo -e "

---- Scp-ing files silently ----

"
ssh -nq $svr mkdir -p ${log_dir}
scp -pq ${log_dir}/alter_tempass_${db}_$$.sql ${script_dir}/run_sql.ksh $svr:${log_dir} &
the_pid=$!

sleep 1

xx=`ps -ef|egrep 'scp -p'|egrep  'alter_tempass'|egrep "$$" |grep -v grep |awk '{print $2}'`
if [[ $xx = $the_pid ]]
then
sleep 1
yy=`ps -ef|egrep 'scp -p'|egrep 'alter_tempass'|egrep "$$" |grep -v grep |awk '{print $2}'`
if [[ $yy = $the_pid ]]
then
kill -9  $the_pid 2>/dev/null
echo -e "

Problems in Scp ..... Plz run above SQLs ( spooled in ${log_dir}/alter_tempass_${db}_$$.sql ) manually in $db on $svr
"
exit 001
fi
fi

echo -e "

--- Checking existence of required files on $svr -----
"

ssh -nq $svr ls -lart ${log_dir}/alter_tempass_${db}_$$.sql ${log_dir}/run_sql.ksh
ssh -nq $svr ${log_dir}/run_sql.ksh $db ${log_dir}/alter_tempass_${db}_$$.sql |tee ${log_dir}/run_sql_alter_tempass_${db}_$$.log

egrep -iq 'ORA-|SP2-' ${log_dir}/run_sql_alter_tempass_${db}_$$.log
if [ $? -eq 0 ]
then
echo -e " 
** Check ${log_dir}/run_sql_alter_tempass_${db}_$$.log ... Note actions taken so far
Exiting ....
"
exit 2
fi

echo -e "

--- Password altered successfully in $db on $svr ---

"

}


######## MAIN ##############

##. /usr/bin/setsid.sh inftst1 

if [ $(dirname $0) = "." ]
then
   export script_dir=$(pwd)
else
   export script_dir=$(dirname $0)
fi
export log_dir=${script_dir}/LOGS


echo -e "

Enter the name of  newly created db to be registered in user_codes and databases  : \c"
read DB 
export db=`echo $DB|tr '[A-Z]' '[a-z]'`

[[ -z $db ]] && {
echo -e "
Null input ..Plz run again
"
exit 2
}

. /usr/bin/setsid.sh $db 


###to connect infprd1
print "\n\nThis script will change the passwords after refresh to as they were before refresh for the users registered in BigBrother\n\n"
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/LOG
export dump_dir=/u01/app/oracle/local/dba/Refresh_Automation
export   curr_dir=$(pwd)


if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi


export new_inst=`sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<eof
set lines 20 pages 0 head off feed off 
set numwidth 10
select inf_monitor.databases_seq.nextval from dual;
eof`

export new_inst=`echo $new_inst|awk '{print $1}'`

[[ -z $new_inst ]] && {
echo -e "
Null input ..Plz run again
"
exit 2
}


echo -e "

=> Instance of new db $db will be : $new_inst 

"

sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<eof
set lines 180 pages 999 feed off 
select instance,name,code from inf_monitor.clients;
eof

echo -e " 

***********************************************************************************

Enter carefully the 3-lettered code for the client, if not sure refer above list : \c
"
read clntcod

[[ -z $clntcod ]] && {
echo "

No input given...Run again

"
exit 2
}

export CLNTCOD=`echo $clntcod|tr '[a-z]' '[A-Z]'`
export clntcod=`echo $clntcod|tr -d " "|tr '[A-Z]' '[a-z]'`
export CLNTCOD=`echo $CLNTCOD|awk '{print $1}'`

export clnt_inst=`sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<eof
set lines 10 pages 0 head off feed off
select instance from inf_monitor.clients where code='$CLNTCOD';
eof`

export clnt_inst=`echo $clnt_inst|awk '{print $1}'` 

[[ -z $clnt_inst ]] && {
echo "

This client code doesnt have an instance number .... Back out if you are NOT going to be fine with a null entry for client_code in inf_monitor.databases table
Press Y/y to continue , Press N/n to exit 
"
read answer 
export answer=`echo $answer|tr '[A-Z]' '[a-z]'`

if [[ $answer = 'y' ]]  
then
export clnt_inst=NULL 
echo -e "
Continuing ....
"
else 
echo " 
Exiting .....
"
exit 2
fi
}



echo -e "

Instance id of CLIENT $CLNTCOD is  $clnt_inst
"



echo -e "

Enter the environment for $db [ dev |test |prod |parprod | dr ] :\c
"
read TGT_ENV

[[ -z $TGT_ENV ]] && {
echo "

No input given...Enter the environment for $db [ dev |test |prod |parprod | dr ] :\c

"
read TGT_ENV
[[ -z $TGT_ENV ]] && {
echo "

Null input again ...Exiting ..
"
exit 2
}
}

export tgt_env=`echo $TGT_ENV|awk '{print $1}'|tr '[A-Z]' '[a-z]'`
if [ $tgt_env != 'prod' -a $tgt_env != 'dev' -a $tgt_env != 'test' -a $tgt_env != 'dr' -a $tgt_env != 'parprod' ]
then
echo -e "
Enter CORRECTLY the environment of the newly created database [ prod | dev |test | dr |parprod ] : \c"
read TGT_ENV
export tgt_env=`echo $TGT_ENV|awk '{print $1}'|tr '[A-Z]' '[a-z]'`
  if [ $tgt_env != 'prod' -a $tgt_env != 'dev' -a $tgt_env != 'test' -a $tgt_env != 'dr' -a $tgt_env != 'parprod' ]
  then
  echo -e "
  AGAIN the environment is wrongly entered ... Exiting ...
  "
  exit 2
  fi
fi
if [ $tgt_env = 'dr' ]
  then
  export tns=${db}_dr
  export sfx=dr
  export DG=Y
  else
  export tns=$db
  export DG=N
fi


export temp_paswd=eagle_$clntcod

echo -e "

Enter server name for $db : \c 
"
read SVR

[[ -z $SVR ]] && {
echo "

Null input ..Exiting 

"
exit 2
}

export svr=`echo $SVR|awk '{print $1}'|tr '[A-Z]' '[a-z]'`

ping $svr -c 1
if [ $? -ne 0 ]
then 
echo -e "
Unable to reach given server $svr
Enter correct server name for $db : \c
"
read SVR

export svr=`echo $SVR|awk '{print $1}'|tr '[A-Z]' '[a-z]'`

ping $svr -c 1
if [ $? -ne 0 ]
then
echo -e "
Unable to reach $svr ......Exiting ....

"
exit 2
fi
fi

export vip=`ssh -nq $svr egrep "${db}-${sfx}" /etc/hosts|awk '{print $1}'`

echo -e " 

---------------------------------------------
    VIP of $db is $vip 
---------------------------------------------

"



##### Can add checks here for improper inputs of srv and NULL loc
sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<xyz>/dev/null
set lines 130 pages 0 trimspo on head off feed off echo off termout off
spool ${log_dir}/instloc_$$.lst
select instance,location from inf_monitor.machines where name='$svr';
spool off
xyz

export mac_inst=`awk '{print $1}' ${log_dir}/instloc_$$.lst`
export loc=`awk '{print $2}' ${log_dir}/instloc_$$.lst`



echo $loc|egrep -iq EVER

[[ $? -eq 0 ]] && {
export tns=sb_${db}
}

echo -e "

**** TNS alias will be $tns since Location is $loc

"


sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<eof
set echo on lines 200 pages 999 trimspo on 
spool ${log_dir}/ins_databases_$$.log
prompt insert into inf_monitor.databases values($new_inst,'$db',$clnt_inst,'$DG','OPEN','$tns','$CLNTCOD $tgt_env $loc','10.2.0.4.0','','','$vip','2025340593',sysdate,'','',$mac_inst);
insert into inf_monitor.databases values($new_inst,'$db',$clnt_inst,'$DG','OPEN','$tns','$CLNTCOD $tgt_env $loc','10.2.0.4.0','','','$vip','2025340593',sysdate,'','',$mac_inst);
commit;
spool off
eof

echo -e "

------------   SUCCESSFULLY registered in inf_monitor.databases    ----------------

"


echo -e "

Was $tgt_env database $db newly created from Blank Template [ y|n ]:\c"
read ANS

if [ -z $ANS ]
then
export ans=y
fi

export ans=`echo $ANS|tr '[A-Z]' '[a-z]'`

if [ $ans != 'y' -a $ans != 'n' ] 
then
echo -e "

Plz run again and give proper input y|n

Exiting........ 
"
exit 1
fi



######If db WASNT created from a Blank template

if [ $ans = 'n' ]
then
echo -e "


Enter the source database sid from which $db was created : \c"
read SRC_DB

if [[ -z $SRC_DB ]]
then
echo -e "
No input given ... Enter the source database sid from which $db was created : \c"
read SRC_DB
fi

export src_db=`echo $SRC_DB|tr '[A-Z]' '[a-z]'`




sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<eof
set lines 150 pages 999 feed off
col environment for a50
select d.sid, d.instance as "database instance" ,m.name as machine,environment from inf_monitor.databases d,inf_monitor.machines m where sid like '%$src_db%' and m.instance=d.mac_instance;
eof

echo -e " 

Enter the source db instance by referring above :\c"
read src_inst

export src_inst
register_users
alter_tempas



echo "

 ---------   Success   --------  

" 
exit 0 
fi
#######Code for db created from EXISTING db ends 







#Code to make entry when new db created from Blank template 
if [ $ans = 'y' ]
then
	if [ $tgt_env = 'dr' ]
	then
	echo -e "
	**** Open DR cannot have been be created from a blank template .... Exiting....
	"
	exit 77
	fi
echo -e " 

Enter the product version of the blank template db from which $db was created [ 9 | 10 |11 ] :\c
"
read version

if [[ $tgt_env = 'parprod' ]]
then
export sim_env=prod
else
export sim_env=$tgt_env
fi

export ORACLE_SID=$db
export ORAENV_ASK=NO
. /usr/local/bin/oraenv

export ref_inst=`sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<eof
set lines 20 pages 0 trimspo on feed off head off
select instance from inf_monitor.databases where pace_version like '$version%' and lower(environment) like '%prod%' and rownum<2;
eof`

echo $ref_inst >${log_dir}/ref_inst_$$.lst
egrep '[0-9]' ${log_dir}/ref_inst_$$.lst
if [ $? -ne 0 ]
then
echo -e "

No Prod db's exist with Version $version.Seeing for Dev/Test dbs with this version whose list of users can be copied for the new db in BigB...

"
export ref_inst=`sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<eof
set lines 20 pages 0 trimspo on feed off head off
select instance from inf_monitor.databases where pace_version like '$version%' and (lower(environment) like '%dev%' or lower(environment) like '%test%') and rownum<2;
eof`

echo $ref_inst >${log_dir}/ref_inst_$$.lst
egrep '[0-9]' ${log_dir}/ref_inst_$$.lst
if [ $? -ne 0 ]
then
echo -e "
There is no existing db of Version $version of any type of environment. Plz enter instance id of an existing db of any other version whose users in BigB can be copied for new db $db. Or abort and do the rest MANUALLY after taking note of actions done so far 
"
read ref_inst
fi
fi

clear 

echo -e "
Taking $ref_inst as reference db instance from which to copy users for $db in user_codes@BigB
"

export src_inst=`echo $ref_inst|awk '{print $1}'`
register_users
alter_tempas


echo "

 ---------   Success   --------  

" 
rm -f ${log_dir}/*_$$.lst 
exit 0 
fi
#Code for blank template ends 




