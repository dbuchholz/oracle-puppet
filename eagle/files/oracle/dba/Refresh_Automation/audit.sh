# ==================================================================================================
# NAME:        audit.sh
#
# AUTHOR:     Nikhil Kulkarni 
#
# PURPOSE:  Implement Auditing on database
#
#
# USAGE:       ./audit.sh
#
#
# ==================================================================================================

export   curr_dir=$(pwd)
export dump_dir=/u01/app/oracle/local/dba/Refresh_Automation
print  "\nTHIS SCRIPT IS MADE FOR IMPLEMENATTAION OF AUDITING.\n\nPLEASE ENTER DBNAME for which you want to implement auditing.\n\nYou can chose one of the following existing database SID\n\n"
##ps -ef | grep -v grep |grep ora_pmon_ | awk '{print $8}' | awk -F_ '{print $3}'

ps -ef | grep -v grep |grep ora_pmon_ | awk '{print $8}' | awk -F_ '{print $3}' > /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt

cat /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt | while read LINE
   do
    print "\t\t$LINE"
   done
rm -f /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt

print  "\n\nPLEASE ENTER DBNAME HERE TO PROCEED : \c"
read dbname

if [ -z $dbname ]
then
print "You must enter the Oracle SID of the database for which you are performing refresh"
print "EXITING......"
exit 0
fi


   if $(ps -ef | grep pmon | grep ${dbname} | grep -v grep >/dev/null 2>&1); then
        . /usr/bin/setsid.sh $dbname
else
      print "\nGiven SID is not valid...choose only one of the above !!!\n"
      exit 0
   fi


mkdir -p /u01/app/oracle/audit_$dbname
print  "\n\nFor 9i database we need to treat auditing diffrently."
print "\n\tPlease press :\n\n\t\t(9I/9i) for ORACLE version 9i.\n\n\t\t OR\n\n\t\t(10G/10g) for oracle version 10G.\n\n\t\t OR\n\n\t\t(11G/11g) for oracle version 11G."
print "\n\nENTER YOUR CHOICE : \c"
read verchoicex


export VERCHOICE=$(print $verchoicex|tr '[A-Z]' '[a-z]')
if [ $VERCHOICE != '10g' -a $VERCHOICE != '11g' -a $VERCHOICE != '9i' -a $VERCHOICE != 'a' ]
then
 print "\n\n\t\tPlease Execute this again and Enter your choice properly either (9I/9i) or (10G/10g) or (11G/11g) only\n\nAborting Here........\n"
 exit 4
fi

if [[ $VERCHOICE = 9i ]]
then
print "\n\nYou made choice of $VERCHOICE database,Implementation of Auditing is in progress...\n"
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
select 'shut down of database is in progress...please wait for next response' from dual;

alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system checkpoint;
alter system checkpoint;
alter system checkpoint;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;

select '                                                                  ' from dual;
select '                                                                  ' from dual;

shut abort;
EOF

egrep -q '*.audit_trail=TRUE' $dump_dir/${dbname}_scripts/init${dbname}.ora
export exsts=$?

if [[ $exsts -ne 0 ]]
then print "command failure"
cat $dump_dir/${dbname}_scripts/init${dbname}.ora $dump_dir/${dbname}_scripts/audit_train.ini > $dump_dir/${dbname}_scripts/init${dbname}1.ora
mv $dump_dir/${dbname}_scripts/init${dbname}1.ora $dump_dir/${dbname}_scripts/init${dbname}.ora
cp $dump_dir/${dbname}_scripts/init${dbname}.ora $ORACLE_HOME/dbs

sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off

startup pfile='$ORACLE_HOME/dbs/init${dbname}.ora';

@$ORACLE_HOME/rdbms/admin/cataudit.sql
audit insert, update, delete on sys.aud$ by access;
audit create session whenever not successful;
@$dump_dir/ea_verify_function.sql

create profile EA_PROFILE LIMIT
CPU_PER_SESSION UNLIMITED
CPU_PER_CALL UNLIMITED
SESSIONS_PER_USER UNLIMITED
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 30
PASSWORD_LIFE_TIME 60
PASSWORD_REUSE_TIME 365
PASSWORD_REUSE_MAX 13
PASSWORD_VERIFY_FUNCTION EA_VERIFY_FUNCTION
PASSWORD_GRACE_TIME 5;


CREATE ROLE EA_DBA_ROLE;
GRANT DBA TO EA_DBA_ROLE;
GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;

CREATE ROLE EA_INSTALL_ROLE;
grant execute on PACE_MASTERDBO.REGISTEREDITS to EA_INSTALL_ROLE;
GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;
GRANT CREATE ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT ALTER ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT CREATE ANY VIEW TO EA_INSTALL_ROLE;

audit ALTER ANY CLUSTER;
audit ALTER ANY INDEX;
audit ALTER ANY INDEXTYPE;
audit ALTER ANY LIBRARY;
audit ALTER ANY PROCEDURE;
audit ALTER ANY SEQUENCE;
audit ALTER ANY TABLE;
audit ALTER ANY TRIGGER;
audit ALTER TABLESPACE;
audit CREATE ANY CLUSTER;
audit CREATE ANY INDEX;
audit CREATE ANY INDEXTYPE;
audit CREATE ANY LIBRARY;
audit CREATE ANY PROCEDURE;
audit CREATE ANY SEQUENCE;
audit CREATE ANY TABLE;
audit CREATE ANY TRIGGER;
audit CREATE CLUSTER;
audit CREATE INDEXTYPE;
audit CREATE LIBRARY;
audit CREATE PROCEDURE;
audit CREATE SEQUENCE;
audit CREATE SESSION;
audit CREATE TABLE;
audit CREATE TABLESPACE;
audit CREATE TRIGGER;
audit DROP ANY CLUSTER;
audit DROP ANY INDEX;
audit DROP ANY INDEXTYPE;
audit DROP ANY LIBRARY;
audit DROP ANY PROCEDURE;
audit DROP ANY SEQUENCE;
audit DROP ANY TABLE;
audit DROP ANY TRIGGER;
audit DROP TABLESPACE;
audit EXECUTE ANY INDEXTYPE;
audit EXECUTE ANY LIBRARY;
audit EXECUTE ANY PROCEDURE;


CREATE ROLE READONLY;
GRANT SELECT ANY TABLE TO READONLY;
GRANT SELECT ANY DICTIONARY TO READONLY;
GRANT CREATE SESSION TO READONLY;
GRANT CONNECT TO READONLY;

create spfile from pfile;
select '                                                           ' from dual;
select 'Restarting database with spfile....please wait for next response' from dual;
shut abort;
startup;

EOF
print "\nAudit enbaling,ea_verify_function,EA_PROFILE creation and enabling supplemental logging is completed successfully !!!!\n"
fi

if [[ $exsts = 0 ]]
then
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
startup
spool $dump_dir/${dbname}_scripts/audit.txt
@$ORACLE_HOME/rdbms/admin/cataudit.sql
audit insert, update, delete on sys.aud$ by access;
audit create session whenever not successful;
@$dump_dir/ea_verify_function.sql

create profile EA_PROFILE LIMIT
CPU_PER_SESSION UNLIMITED
CPU_PER_CALL UNLIMITED
SESSIONS_PER_USER UNLIMITED
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 30
PASSWORD_LIFE_TIME 60
PASSWORD_REUSE_TIME 365
PASSWORD_REUSE_MAX 13
PASSWORD_VERIFY_FUNCTION EA_VERIFY_FUNCTION
PASSWORD_GRACE_TIME 5;


CREATE ROLE EA_DBA_ROLE;
GRANT DBA TO EA_DBA_ROLE;
GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;

CREATE ROLE EA_INSTALL_ROLE;
grant execute on PACE_MASTERDBO.REGISTEREDITS to EA_INSTALL_ROLE;
GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;
GRANT CREATE ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT ALTER ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT CREATE ANY VIEW TO EA_INSTALL_ROLE;

audit ALTER ANY CLUSTER;
audit ALTER ANY INDEX;
audit ALTER ANY INDEXTYPE;
audit ALTER ANY LIBRARY;
audit ALTER ANY PROCEDURE;
audit ALTER ANY SEQUENCE;
audit ALTER ANY TABLE;
audit ALTER ANY TRIGGER;
audit ALTER TABLESPACE;
audit CREATE ANY CLUSTER;
audit CREATE ANY INDEX;
audit CREATE ANY INDEXTYPE;
audit CREATE ANY LIBRARY;
audit CREATE ANY PROCEDURE;
audit CREATE ANY SEQUENCE;
audit CREATE ANY TABLE;
audit CREATE ANY TRIGGER;
audit CREATE CLUSTER;
audit CREATE INDEXTYPE;
audit CREATE LIBRARY;
audit CREATE PROCEDURE;
audit CREATE SEQUENCE;
audit CREATE SESSION;
audit CREATE TABLE;
audit CREATE TABLESPACE;
audit CREATE TRIGGER;
audit DROP ANY CLUSTER;
audit DROP ANY INDEX;
audit DROP ANY INDEXTYPE;
audit DROP ANY LIBRARY;
audit DROP ANY PROCEDURE;
audit DROP ANY SEQUENCE;
audit DROP ANY TABLE;
audit DROP ANY TRIGGER;
audit DROP TABLESPACE;
audit EXECUTE ANY INDEXTYPE;
audit EXECUTE ANY LIBRARY;
audit EXECUTE ANY PROCEDURE;


CREATE ROLE READONLY;
GRANT SELECT ANY TABLE TO READONLY;
GRANT SELECT ANY DICTIONARY TO READONLY;
GRANT CREATE SESSION TO READONLY;
GRANT CONNECT TO READONLY;
create spfile from pfile;

select 'Restarting database with spfile....please wait for next response' from dual;
shut abort;
startup;
EOF
print "\nAudit enbaling,ea_verify_function,EA_PROFILE creation and enabling supplemental logging is completed successfully !!!!\n"
fi


fi




if [[ $VERCHOICE = 10g ]]
then
print "\n\nYou made choice of $VERCHOICE database,Implementation of Auditing is in progress...\n"
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off

alter system set utl_file_dir='/u01/app/oracle/audit_$dbname' scope=SPFILE;
alter system set audit_trail=db_extended scope=SPFILE;
alter system set audit_sys_operations=TRUE scope=SPFILE;
create pfile from spfile;

Spool $dump_dir/${dbname}_scripts/LockDefault.sql

select 'alter user '||username||' identified by '||'eagle_lock'||' account lock password expire;' from dba_users where username in ('ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS','HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS','ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS','QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM');

spool off
@$dump_dir/${dbname}_scripts/LockDefault.sql
select '                                                                  ' from dual;
select '                                                                  ' from dual;
select 'shut down of database is in progress...please wait for next response' from dual;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system checkpoint;
alter system checkpoint;
alter system checkpoint;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;

select '                                                                  ' from dual;
select '                                                                  ' from dual;

shut abort;

startup pfile='$ORACLE_HOME/dbs/init${dbname}.ora;
select '                                                                  ' from dual;
select '                                                                  ' from dual;

select 'Restarting database with spfile....please wait !!!' from dual;
select '                                                                  ' from dual;
select '                                                                  ' from dual;

shut abort;

startup;
EOF

sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off

spool $dump_dir/${dbname}_scripts/audit.txt

audit insert, update, delete on sys.aud$ by access;
audit create session whenever not successful;

@$dump_dir/ea_verify_function.sql

create profile EA_PROFILE LIMIT
CPU_PER_SESSION UNLIMITED
CPU_PER_CALL UNLIMITED
SESSIONS_PER_USER UNLIMITED
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 30
PASSWORD_LIFE_TIME 60
PASSWORD_REUSE_TIME 365
PASSWORD_REUSE_MAX 13
PASSWORD_VERIFY_FUNCTION EA_VERIFY_FUNCTION
PASSWORD_GRACE_TIME 5;


CREATE ROLE EA_DBA_ROLE;
GRANT DBA TO EA_DBA_ROLE;
GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;


CREATE ROLE EA_INSTALL_ROLE;
grant execute on PACE_MASTERDBO.REGISTEREDITS to EA_INSTALL_ROLE;
GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;
GRANT CREATE ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT ALTER ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT CREATE ANY VIEW TO EA_INSTALL_ROLE;

audit ALTER ANY CLUSTER;
audit ALTER ANY INDEX;
audit ALTER ANY INDEXTYPE;
audit ALTER ANY LIBRARY;
audit ALTER ANY PROCEDURE;
audit ALTER ANY SEQUENCE;
audit ALTER ANY TABLE;
audit ALTER ANY TRIGGER;
audit ALTER TABLESPACE;
audit CREATE ANY CLUSTER;
audit CREATE ANY INDEX;
audit CREATE ANY INDEXTYPE;
audit CREATE ANY LIBRARY;
audit CREATE ANY PROCEDURE;
audit CREATE ANY SEQUENCE;
audit CREATE ANY TABLE;
audit CREATE ANY TRIGGER;
audit CREATE CLUSTER;
audit CREATE INDEXTYPE;
audit CREATE LIBRARY;
audit CREATE PROCEDURE;
audit CREATE SEQUENCE;
audit CREATE SESSION;
audit CREATE TABLE;
audit CREATE TABLESPACE;
audit CREATE TRIGGER;
audit DROP ANY CLUSTER;
audit DROP ANY INDEX;
audit DROP ANY INDEXTYPE;
audit DROP ANY LIBRARY;
audit DROP ANY PROCEDURE;
audit DROP ANY SEQUENCE;
audit DROP ANY TABLE;
audit DROP ANY TRIGGER;
audit DROP TABLESPACE;
audit EXECUTE ANY INDEXTYPE;
audit EXECUTE ANY LIBRARY;
audit EXECUTE ANY PROCEDURE;


CREATE ROLE READONLY;
GRANT SELECT ANY TABLE TO READONLY;
GRANT SELECT ANY DICTIONARY TO READONLY;
GRANT CREATE SESSION TO READONLY;
GRANT CONNECT TO READONLY;

Alter database add supplemental log data;
Alter database add supplemental log data(primary key) columns;
spool off;
EOF
print "\nAudit enbaling,ea_verify_function,EA_PROFILE creation and enabling supplemental logging is completed successfully !!!!\n"
fi

if [[ $VERCHOICE = 11g ]]
then
print "\n\nYou made choice of $VERCHOICE database,Implementation of Auditing is in progress...\n"
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
spool $dump_dir/${dbname}_scripts/passvery.log

alter system set utl_file_dir='/u01/app/oracle/audit_$dbname' scope=SPFILE;
alter system set audit_trail=db_extended scope=SPFILE;
alter system set audit_sys_operations=TRUE scope=SPFILE;
create pfile from spfile;

Spool $dump_dir/${dbname}_scripts/LockDefault.sql

select 'alter user '||username||' identified by '||'eagle_lock'||' account lock password expire;' from dba_users where username in ('ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS','HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS','ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS','QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM');

spool off
@$dump_dir/${dbname}_scripts/LockDefault.sql

alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system checkpoint;
alter system checkpoint;
alter system checkpoint;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;
alter system switch logfile;

select 'shut down of database is in progress...please wait for next response' from dual;
select '                                                                  ' from dual;
select '                                                                  ' from dual;

shut immediate;

startup pfile='$ORACLE_HOME/dbs/init${dbname}.ora;
select '                                                                  ' from dual;
select '                                                                  ' from dual;


select 'Restarting database with spfile....please wait !!!' from dual;
select '                                                                  ' from dual;
select '                                                                  ' from dual;

shut immediate;

startup;
EOF

sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off

spool $dump_dir/${dbname}_scripts/audit.txt

audit insert, update, delete on sys.aud$ by access;
audit create session whenever not successful;

alter profile default limit FAILED_LOGIN_ATTEMPTS unlimited;
alter profile default limit PASSWORD_GRACE_TIME unlimited;
alter profile default limit PASSWORD_LIFE_TIME unlimited;
alter profile default limit PASSWORD_LOCK_TIME unlimited;

@$dump_dir/ea_verify_function.sql

create profile EA_PROFILE LIMIT
CPU_PER_SESSION UNLIMITED
CPU_PER_CALL UNLIMITED
SESSIONS_PER_USER UNLIMITED
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 30
PASSWORD_LIFE_TIME 60
PASSWORD_REUSE_TIME 365
PASSWORD_REUSE_MAX 13
PASSWORD_VERIFY_FUNCTION EA_VERIFY_FUNCTION
PASSWORD_GRACE_TIME 5;


CREATE ROLE EA_DBA_ROLE;
GRANT DBA TO EA_DBA_ROLE;
GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;


CREATE ROLE EA_INSTALL_ROLE;
grant execute on PACE_MASTERDBO.REGISTEREDITS to EA_INSTALL_ROLE;
GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;
GRANT CREATE ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT ALTER ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT CREATE ANY VIEW TO EA_INSTALL_ROLE;


audit ALTER ANY CLUSTER;
audit ALTER ANY INDEX;
audit ALTER ANY INDEXTYPE;
audit ALTER ANY LIBRARY;
audit ALTER ANY PROCEDURE;
audit ALTER ANY SEQUENCE;
audit ALTER ANY TABLE;
audit ALTER ANY TRIGGER;
audit ALTER TABLESPACE;
audit CREATE ANY CLUSTER;
audit CREATE ANY INDEX;
audit CREATE ANY INDEXTYPE;
audit CREATE ANY LIBRARY;
audit CREATE ANY PROCEDURE;
audit CREATE ANY SEQUENCE;
audit CREATE ANY TABLE;
audit CREATE ANY TRIGGER;
audit CREATE CLUSTER;
audit CREATE INDEXTYPE;
audit CREATE LIBRARY;
audit CREATE PROCEDURE;
audit CREATE SEQUENCE;
audit CREATE SESSION;
audit CREATE TABLE;
audit CREATE TABLESPACE;
audit CREATE TRIGGER;
audit DROP ANY CLUSTER;
audit DROP ANY INDEX;
audit DROP ANY INDEXTYPE;
audit DROP ANY LIBRARY;
audit DROP ANY PROCEDURE;
audit DROP ANY SEQUENCE;
audit DROP ANY TABLE;
audit DROP ANY TRIGGER;
audit DROP TABLESPACE;
audit EXECUTE ANY INDEXTYPE;
audit EXECUTE ANY LIBRARY;
audit EXECUTE ANY PROCEDURE;


CREATE ROLE READONLY;
GRANT SELECT ANY TABLE TO READONLY;
GRANT SELECT ANY DICTIONARY TO READONLY;
GRANT CREATE SESSION TO READONLY;
GRANT CONNECT TO READONLY;

Alter database add supplemental log data;
Alter database add supplemental log data(primary key) columns;

EOF
print "\nAudit enbaling,ea_verify_function,EA_PROFILE creation and enabling supplemental logging is completed successfully !!!!\n"
fi


export datafile=`sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
spool $dump_dir/${dbname}_scripts/$dbname.ini
select file_name from dba_Data_files where tablespace_name='SYSTEM';
spool off
EOF`
export indatafile=`egrep '^/' $dump_dir/${dbname}_scripts/$dbname.ini |awk -F/ '{print "/"$2"/"$3}'`
print "\n*******************************************************************\n"
print "\n\nChecking and enabling block chnage tracking...\n\nDo you want to enable block chnage tracking"
print "\n\tPlease press :\n\n\t\t(Y/y) to Enable block chnage tracking \n\n\t\tOR\n\n\t\t(N/n) or any other key to continue without Enabling block change tracking\n\n"

print "ENTER YOUR CHOICE : \c"

read bctchoicex

export BCTCHOICE=$(print $bctchoicex|tr '[A-Z]' '[a-z]')
if [ $BCTCHOICE != 'y' -a $BCTCHOICE != 'n' ]
then
 print "\n\n\t\tPlease Execute this again and Enter your choice properly either (Y/y) or (N/n) only\n\nAborting Here........\n"
 exit 4
fi

if [[ $BCTCHOICE = 'y' ]]
then
export isbct=`sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
select status FROM v\\$block_change_tracking;
EOF`


if [[ $isbct = 'DISABLED' ]]
then
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
alter database enable block change tracking using file '$indatafile/block_change_tracking.dbf' reuse; 
EOF
fi

if [[ $isbct = 'ENABLED' ]]
then
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
alter database disable block change tracking;
alter database enable block change tracking using file '$indatafile/block_change_tracking.dbf' reuse;
EOF
fi
print "\nBlock cange tracking has been enabled using file $indatafile/block_change_tracking.dbf.\n "
print "\n*****************************************************************************"
fi

if [[ $BCTCHOICE != 'y' ]]
then
print "\nYou made choice to continue without enabling Block Change Tracking ...."
print "\n*****************************************************************************"
fi

print "\n\nIf this is new refresh(first refresh) and No team_install and team_dba user is present on this database then only continue otherwise everything is all set,Preserve user will take care of users otherwise"

print "\n\tPlease press :\n\n\t\t(Y/y) to continue for new refresh \n\n\t\t OR\n\n\t\t(N/n) to exit\n\n"
print "ENTER YOUR CHOICE : \c"

read usechoicex

export USECHOICE=$(print $usechoicex|tr '[A-Z]' '[a-z]')
if [ $USECHOICE != 'y' -a $USECHOICE != 'n' ]
then
 print "\n\n\t\tPlease Execute this again and Enter your choice properly either (Y/y) or (N/n) only\n\nAborting Here........\n"
 exit 4
fi

if [[ $USECHOICE = y ]]
then
sqlplus -s / as sysdba<<EOF
set lines 200 pages 0 head off feed off
spool $dump_dir/${dbname}_scripts/${dbname}_inst_dba_users.log
@$dump_dir/INSTALLUser.sql
@$dump_dir/DBAUsers.sql
spool off
EOF
sh $dump_dir/SendEmail.sh
fi

if [[ $USECHOICE = n ]]
then
print "As its not first refresh of DB.....all set for auditing....\n\texiting now !!!!"
exit 5;
fi

