#!/bin/ksh

###This script will change the passwords after refresh to as they were before refresh for the users registered in BigB. 
##This is not for an existing Open DR since Open DR is hardly refreshed   

##Author : NIKHIL KULKARNI 

###to connect infprd1
print "\n\nThis script will change the passwords after refresh to as they were before refresh for the users registered in BigBrother\n\n"
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/LOG
export dump_dir=/u01/app/oracle/local/dba/Refresh_Automation
export   curr_dir=$(pwd)


if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi


if [ $(dirname $0) = "." ]
then
  export  curr_dir=$(pwd)
else
   export script_dir=$(dirname $0)
fi
export log_dir=$dump_dir/${dbname}_scripts

print "\nplease enter sid of the refreshed DB,You can chose one of following database\n" 
#ps -ef | grep -v grep |grep ora_pmon_ | awk '{print $8}' | awk -F_ '{print $3}'

ps -ef | grep -v grep |grep ora_pmon_ | awk '{print $8}' | awk -F_ '{print $3}' > /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt
cat /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt | while read LINE
   do
    print "\t\t$LINE"
   done
rm -f /u01/app/oracle/local/dba/Refresh_Automation/test_sid.txt

print "\n\n\tEnter the sid of the refreshed DB :\c"
read dbname
[[ -z $dbname ]]  && {
echo "

No input given...Run again

"
exit 2
}


export log_dir=$dump_dir/${dbname}_scripts
export db=`echo  $dbname|tr '[A-Z]' '[a-z]'`

export DATE=`date +%dth%h%Y_%Hrs_%Mmin_%Ssec`


   if $(ps -ef | grep pmon | grep ${dbname} | grep -v grep >/dev/null 2>&1); then
        . /usr/bin/setsid.sh $dbname
else
      print "\nGiven SID is not valid...choose only one of the above !!!\n"
      exit 0
   fi




print "\n***********************************************************************************************\n\ninstance,database name and environment details  reextracted from hawaii to confirm the database.\n\n"

sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set lines 180 pages 999 trimspo on feed off
col environment for a45
select instance,sid,environment from inf_monitor.databases where sid='$dbname';
EOF
echo -e " 

Enter the instance id by referring above : \c

"
read inst

[[ -z $inst ]]  && {
echo "

No input given...Run again

"
exit 2
}

export ispasswd=`sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set lines 200 pages 0 head off feed off
select inf_monitor.bb_get(nvl(temp_code,code)) from inf_monitor.user_codes where db_instance = $inst and username='SYS';
EOF`

sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set lines 200 pages 999 trimspo on head off feed off  
spool ${log_dir}/setpas_aft_refresh_${dbname}.sql
select  'alter user ' || username || ' identified by  "'|| inf_monitor.bb_get(nvl(temp_code,code)) ||'" ;' from inf_monitor.user_codes where db_instance =$inst;
spool off 
EOF

print "\n\nspool file is setpas_aft_refresh_${dbname}.sql ,created at $log_dir and contents are as above."
print "\nAre you sure ,you want to set the passwords as mentioned above\n"
print "\n\tPlease press \n\n\t\t(Y/y) for Proceed with the above commands \n\n\t\tOR\n\n\t\tany other key to exit\n"
print "\nENTER YOUR CHOICE : \c"
read passchoicex
export PASS_CHOICE=$(print $passchoicex|tr '[A-Z]' '[a-z]')

if [[ $PASS_CHOICE = 'y' ]]
then
sqlplus -s / as sysdba<<EOF
set pagesize 30000  linesize 132  heading off  feedback off trimspool on
spool $log_dir/setpas_aft_refresh_$db.log
@$log_dir/setpas_aft_refresh_$dbname.sql
spool off;
EOF
print "\n\nPassword set back  as it was before refresh !!!!!"

cd $ORACLE_HOME/dbs
rm orapw$dbname
orapwd file=orapw$dbname password=$ispasswd entries=20
cd $dump_dir
print "\n\nNew password file is created with name orapw$DB at ORACLE_HOME/dbs !!!\n\n"
print "**************************************completed***********************************"
fi

if [[ $PASS_CHOICE != 'y' ]]
then
print "exiting !!!" 
exit 4
fi


