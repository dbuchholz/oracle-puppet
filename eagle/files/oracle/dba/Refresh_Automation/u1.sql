set lines 200 
column "DB User" format a20 
column "Logon Time" format a15 
column "Status" format a10 
column "Session" format a15 
column "oracle Process" format a10 
column "Program" format a20 
column "OS User" format a10 
column "Machine Name" format a20 
column "Processid" format 99999 
select 
rpad(s.username,13,' ') as "DB User", 
to_char
(s.logon_time,'dd-mm-yy hh24:mi') as "Logon Time", 
initcap
(s.status) as "Status",s.sid||','||s.serial# as "Session",p.spid "Processid",s.process as "oracle Process", 
rpad
(upper(substr(s.program,instr(s.program,'\',-1)+1)),25,' ') as "Program", 
rpad
(lower(s.osuser),10,' ') as "OS User", 
rpad
(initcap(s.machine),15,' ') as "Machine Name" 
from v$session s,v$process p 
where s.paddr = p.addr and s.username like upper('%&USERNAME%') 
order by 2,s.machine,s.program
/
