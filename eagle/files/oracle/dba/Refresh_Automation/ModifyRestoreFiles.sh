#!/bin/ksh
# ==================================================================================================
# NAME:		ModifyRestoreFiles.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will Modify the Restore files.
#
# USAGE:	ModifyRestoreFiles.sh  <ORACLE_SID>
#
#
# ==================================================================================================
create_init_ora_file()
{
#uncomment the below line to debug
#set -x
cdate=$(date +'%Y%m%d_%H%M%S')
if [[ -f $ORACLE_HOME/dbs/spfile${ORACLE_SID}.ora ]]
then
 mv $ORACLE_HOME/dbs/spfile${ORACLE_SID}.ora $ORACLE_HOME/dbs/spfile${ORACLE_SID}_${cdate}.ora
else
 print "spfile Does not  exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi
if [[ -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora ]]
then
 mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora $ORACLE_HOME/dbs/init${ORACLE_SID}_${cdate}.ora
else
 print "\ninit.ora Does  not exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi
if [[ -f $ORACLE_HOME/dbs/orapw${ORACLE_SID} ]]
then
 mv $ORACLE_HOME/dbs/orapw${ORACLE_SID} $ORACLE_HOME/dbs/orapw${ORACLE_SID}_${cdate}
else
 print "\nPassword File Does  not exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi

 print "\nCreating Password file...\n"
 export ver_9i=$(print $ORACLE_HOME | grep 9.2)
 if [[ -n $ver_9i ]]
 then
 orapwd file=$ORACLE_HOME/dbs/orapw$ssid password=eagle entries=20
 else
 orapwd file=$ORACLE_HOME/dbs/orapw${ORACLE_SID} password=eagle entries=20
 fi
# export prod_sid=$(grep db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F= '{print $2}' | awk -F\' '{print $2}')
 init_file="$(ls -ltr $REF_HOME/${ORACLE_SID}_scripts/${ssid}*${bkp_dt}*init${ssid}.ora | awk '{print $9}')" 
 print "\n\nCreating init.ora file from Restore File `basename $init_file` ...\n\n"
if [[ -n $init_file ]]
then 
 cp $REF_HOME/${ORACLE_SID}_scripts/${ssid}*${bkp_dt}*init${ssid}.ora  $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
else
 print "\n\n*****FAILURE...You Did Not Copied the Restore files Using CopyRestoreFiles.sh script...Please check*****\n"
 exit 7
fi

if [[ -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora ]]
then
 grep -vi fal_client $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi fal_server $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi standby_archive_dest $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi standby_file_management $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi log_archive_dest_2 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi log_archive_dest_state_2 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi db_flashback_retention_target $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi db_recovery_file_dest_size $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi db_recovery_file_dest $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 cp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
else
 print "\n\ninit.ora file seems to be missing....\n"
exit 8
fi
 
 export prod_sid=$(grep db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F= '{print $2}' | awk -F\' '{print $2}')
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | grep / | sed 's/'${prod_sid}'/'${ORACLE_SID}'/g' | sed 's/#ORACLE_BASE set from environment//g'> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 | awk -F= '{print $1}' | awk -F. '{print $2}' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list | while read LINE
  do
  grep -vi $LINE $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
  mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
  done
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 | sed -e "s/*.//g" -e "s/,/ /g" -e "s/'//g" -e "s/LOCATION=//g" | awk -F= '{print $2}' | sed 's/ /\n/g' | sed 's/control.*//g' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2 | while read LINE
  do
  mkdir -p $LINE
  if [ $? -ne 0 ]
  then
   print "\n\nThere seems to be some problem while creating Directory $LINE1...Please Check and Execute Again...\n\nAborting Here...."
   print "\n\n*********FAILURE*********\n\n"
  exit 9
  fi
  done
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2

 export dbname=$(grep db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora)
 export instname=$(grep instance_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora)
 
 egrep -vi "db_name|instance_name" $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2 | sed 's/'${prod_sid}'/'${ORACLE_SID}'/g' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 print "$dbname\n$instname" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 print "\n\ninit.ora File Created\n\n"
}
modify_control_file()
{
#uncomment the below line to debug
#set -x
 export controlfile=$(basename $REF_HOME/${ORACLE_SID}_scripts/Restore*ControlFile_${prod_sid}_${bkp_dt}_*.rcv)
 print "\nDo you want to Modify the Restore Control File[y/n]: \c"
 read inpt2
 inpt2=$(print $inpt2 | tr -d " ")
 inpt2=$(print $inpt2 | tr '[A-Z]' '[a-z]')
if [[ -n $inpt2 ]]
then
 if [ $inpt2 == 'y' -o $inpt2 == 'Y' ]
 then
 bkp_loc=$(cat $REF_HOME/${ORACLE_SID}_scripts/Restore*ControlFile_${prod_sid}_${bkp_dt}_*.rcv | grep datadomain | awk -F\' '{print $2}' | sed -r 's/(.*)\/.*/\1/' )
 bkp_loc=$(print $bkp_loc | tr -d " ") 
 ctrl_file=$(find $bkp_loc -name "cf_${prod_sid}_${bkp_dt}_*")
 ctrl_file=$(print $ctrl_file | tr -d " ")
 rest_ctrl=$(print $ctrl_file)
 rest_ctrl=$(print $rest_ctrl | tr -d " ")
 print "run {" > $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 print "\trestore controlfile from '$rest_ctrl';" >> $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 print "alter database mount;" >> $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 print "CONFIGURE CONTROLFILE AUTOBACKUP OFF;" >> $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 print "}" >> $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 mv $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt $REF_HOME/${ORACLE_SID}_scripts/$controlfile
 rm -f $REF_HOME/${ORACLE_SID}_scripts/restore_ctrl_temp.txt
 print "Modified $controlfile"
 fi
else
 print "\n\nAborting Here..."
 exit 10
fi
}
modify_DataTemp_file()
{
#uncomment the below line to debug
set -x
 ###added by Basit Khan after Frank's new logic of mount point threshold
 $REF_HOME/build_datafile_tempfile.ksh $prod_sid $ORACLE_SID 
 ######
 export datatemp=$(basename $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}*.rcv)
 export ncpu=$(grep processor /proc/cpuinfo |wc -l)
 ncpu=$(print $ncpu | tr -d "")
 print "\nNumber of CPUs are $ncpu"
 export acha=$(print $ncpu/2 | bc)
 print "\nNumber of Channels which will be assign are $acha\n"
 export rcha=$(grep RCHL $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}*.rcv | wc -l)
if [ $rcha -ge $acha ]
then
  print "\n$acha Channels already assiged\n"
fi
 print "run {" > $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv
 if [ $rcha == $acha ]
 then
 cat $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}_*.rcv | grep allocate >> $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv
 else
 i=1
 while [ $i -le  $acha ]
  do
  print "\t\tallocate channel 'RCHL${i}' device type disk;" >> $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv
  i=$(( $i+1 ))
  done
 fi

###added by Basit Khan after Frank's new logic of mount point threshold
 cat $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv >> $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_final.rcv
#######
 cat $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${ORACLE_SID}.rcv| egrep -vi "run|allocate" >> $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_final.rcv
#######Commented by Basit khan after Frank implemented mount point logic
# cat $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}*.rcv | egrep -vi "run|allocate" >> $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv
# cat $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv | sed 's/xxxxxx/'${ORACLE_SID}'/g' > $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_final.rcv
#######

 mv $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_final.rcv $REF_HOME/${ORACLE_SID}_scripts/$datatemp
 rm -f $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${prod_sid}_${bkp_dt}_temp.rcv $REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${ORACLE_SID}.rcv
 print "\n\nModified $datatemp\n\n"
}
modify_Redo_file()
{
#uncomment the below line to debug
#set -x
 export redofile=$(basename $REF_HOME/${ORACLE_SID}_scripts/Restore*Redo_${prod_sid}_${bkp_dt}*.rcv)
 cat $REF_HOME/${ORACLE_SID}_scripts/Restore*Redo_${prod_sid}_${bkp_dt}*.rcv | sed 's/xxxxxx/'${ORACLE_SID}'/g' > $REF_HOME/${ORACLE_SID}_scripts/RestoreRedo_${prod_sid}_${bkp_dt}_final.rcv

 cat $REF_HOME/${ORACLE_SID}_scripts/RestoreRedo_${prod_sid}_${bkp_dt}_final.rcv | awk -FTO '{print $2}' | sed  -e "s/'//g" -e "s/;//g" | sed -r 's/(.*)\/.*/\1/' > $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt

 cat $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt | sed '/^$/d' | while read LINE
  do
  mkdir -p $LINE
  if [ $? -ne 0 ]
  then
   print "\n\nThere seems to be some problem while creating Directory $LINE...Please Check $REF_HOME/${ORACLE_SID}_scripts/Restore10gRedo_${prod_sid}_${bkp_dt}*.rcv"
   print "Modify RestoreRedo_${prod_sid}_${bkp_dt}*.rcv Manully as per the reuirement\n\n"
   rm -f $REF_HOME/${ORACLE_SID}_scripts/RestoreRedo_${prod_sid}_${bkp_dt}_final.rcv
   rm -f $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt
  fi
 done
 mv $REF_HOME/${ORACLE_SID}_scripts/RestoreRedo_${prod_sid}_${bkp_dt}_final.rcv $REF_HOME/${ORACLE_SID}_scripts/$redofile
 rm -f $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt
 print "\n\nModified $redofile\n\n"
}
modify_Temp_file()
{
#uncomment the below line to debug
#set -x
 export tempfile=$(basename $REF_HOME/${ORACLE_SID}_scripts/Restore*TempFiles_${prod_sid}_${bkp_dt}*.rcv)
 cat $REF_HOME/${ORACLE_SID}_scripts/Restore*TempFiles_${prod_sid}_${bkp_dt}*.rcv | sed 's/xxxxxx/'${ORACLE_SID}'/g' > $REF_HOME/${ORACLE_SID}_scripts/RestoreTempfile_${prod_sid}_${bkp_dt}_final.rcv

 cat $REF_HOME/${ORACLE_SID}_scripts/RestoreTempfile_${prod_sid}_${bkp_dt}_final.rcv | awk '{print $6}' | awk -F\' '{print $2}' | sed  -e "s/'//g" -e "s/;//g" | sed -r 's/(.*)\/.*/\1/' > $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt

 cat $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt | sed '/^$/d' | while read LINE
  do
  mkdir -p $LINE
  if [ $? -ne 0 ]
  then
   print "\n\nThere seems to be some problem while creating Directory $LINE...Please Check $REF_HOME/${ORACLE_SID}_scripts/Restore10gRedo_${prod_sid}_${bkp_dt}*.rcv"
   print "Modify RestoreTempFiles_${prod_sid}_${bkp_dt}*.rcv Manully as per the reuirement\n\n"
   rm -f $REF_HOME/${ORACLE_SID}_scripts/RestoreTempfile_${prod_sid}_${bkp_dt}_final.rcv
   rm -f $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt
  fi
 done
 mv $REF_HOME/${ORACLE_SID}_scripts/RestoreTempfile_${prod_sid}_${bkp_dt}_final.rcv $REF_HOME/${ORACLE_SID}_scripts/$tempfile
 rm -f $REF_HOME/${ORACLE_SID}_scripts/dir_list.txt
 print "\n\nModified $tempfile\n\n"
}

#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')

 print "\nPlease Enter the date of backup which you are using YYYYMMDD :\c"
 read bkp_dt
 print "Please Enter the Source Database name from which you are restoring $ORACLE_SID: \c"
 read ssid
 bkp_dt=$(print $bkp_dt | tr -d " ")
 ssid=$(print $ssid | tr -d " ")
if [[ -n $bkp_dt && -n $ssid ]]
then
 if [[ $bkp_dt == [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] ]]
 then
 find $REF_HOME/${ORACLE_SID}_scripts -name "*${ssid}_${bkp_dt}*" -type f > $REF_HOME/${ORACLE_SID}_scripts/restore_file.txt
 if [[ -s $REF_HOME/${ORACLE_SID}_scripts/restore_file.txt ]]
 then
  print "\n\nBelow are the Files which will be used for Restore under $REF_HOME/${ORACLE_SID}_scripts lcation\n"
  cat $REF_HOME/${ORACLE_SID}_scripts/restore_file.txt
  rm -f $REF_HOME/${ORACLE_SID}_scripts/restore_file.txt
  print "\n\nTo Continue Press [y/Y]: \c"
  read inpt
  inpt=$(print $inpt | tr '[A-Z]' '[a-z]')
  if [ $inpt == 'y' -o $inpt == 'Y' ]
  then
   print "\n\nAre you going to Restore 9i Database ? [y/n]: \c"
   read ans
   ans=$(print $ans | tr -d " ")
   ans=$(print $ans | tr '[A-Z]' '[a-z]')
   if [ $ans == 'n' -o $ans == 'N' ]
   then
    create_init_ora_file
    modify_control_file
    modify_DataTemp_file
    modify_Redo_file
   fi
   if [ $ans == 'y' -o $ans == 'Y' ]
   then
    create_init_ora_file
    modify_control_file
    modify_DataTemp_file
    modify_Temp_file
    modify_Redo_file
   fi
 else
  print "\n\nYou selected not to Proceed...\n"
 exit 4
 fi
 else
  print "\n\nRestore files not copied to $REF_HOME/${ORACLE_SID}_scripts for the Mention date\n"
  print "Please Copy Restore files to $REF_HOME/${ORACLE_SID}_scripts and execute Again\n"
  print "Aborting Here..." 
 exit 5
 fi
else
 print "\n\nPlease enter the Proper date as mentioned\nAborting Here..."
 exit 6
fi
else
 print "\n\nYou Did not Enter the Backup date...\nAborting Here..."
 print "*********FAILURE*********"
exit 7
fi
print "\n\n**************SUCCESSFULL*****************\n\n"
exit 0
