#!/bin/ksh
# ==================================================================================================
# NAME:		UpdateDeleteProdData.sh                                 
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will Delete the Production data while refreshing the Database
#			
#
# USAGE:	UpdateDeleteProdData.sh $ORACLE_SID
#
#
# ==================================================================================================


if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of the database for which you want to perform the Update Delete step."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

clear

$ORACLE_HOME/bin/sqlplus  '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
set serveroutput on  verify on  feedback on
spool $REF_HOME/${ORACLE_SID}_scripts/trunc_tables.out

truncate table pace_masterdbo.pace_hosts_def;
truncate table pace_masterdbo.pace_ftp_def;
truncate table pace_masterdbo.host_def_details;
truncate table pace_masterdbo.pace_app_hosts_def;
truncate table pace_masterdbo.pace_hosts_profiles;
truncate table pace_masterdbo.hosts_profiles_active;
truncate table pace_masterdbo.hosts_profiles_details;
truncate table pace_masterdbo.rpt_profile_override;
truncate table pace_masterdbo.appserver_log;
truncate table pace_masterdbo.update_journal;
truncate table pace_masterdbo.generic_journal_details;
truncate table pace_masterdbo.schedule_def_details;
truncate table pace_masterdbo.schedule_queue;
truncate table pace_masterdbo.feed_results;
truncate table pace_masterdbo.feed_errors;
truncate table pace_masterdbo.feed_error_details;
truncate table pace_masterdbo.adv_rpt_results;
truncate table pace_masterdbo.printer_def;
truncate table rulesdbo.values_set;
truncate table pace_masterdbo.cr_batch_detail;
truncate table pace_masterdbo.xml_reports_log;
truncate table pace_masterdbo.xml_package_log;
truncate table pace_masterdbo.xml_reports_log_detail;
truncate table pace_masterdbo.xml_report_info;
truncate table pace_masterdbo.xml_report_status;
truncate table pace_masterdbo.reports_log;
truncate table pace_masterdbo.email_process;
truncate table pace_masterdbo.email_process_attmts;
truncate table pace_masterdbo.email_process_recipients;
truncate table pace_masterdbo.rpt_profile_email;
-- STAR Tables
truncate table estar.engine_configuration;
truncate table estar.engine_configuration_hist;
truncate table estar.engine_instances;
truncate table estar.web_configuration;
truncate table msgcenter_dbo.msg_instances;
truncate table msgcenter_dbo.msg_instance_services;
truncate table msgcenter_dbo.msg_mc_failover;
truncate table msgcenter_dbo.schedule_run;
truncate table msgcenter_dbo.schedule_event_summary;
truncate table msgcenter_dbo.schedule_pending_reports;
truncate table msgcenter_dbo.schedule_srvr_status;
truncate table eaglemgr.eagle_services;
truncate table eaglemgr.eagle_service_properties;
truncate table eaglemgr.eagle_service_relations;

-- Remove all adhoc jobs from the schedule_def  table

delete from pace_masterdbo.schedule_def where freq_type = 'N';

delete from pace_masterdbo.pace_mail_map_info
where user_instance in (select instance from pace_masterdbo.pace_mail_user_info     
       					where mail_address not like '%eagleaccess.com%' 
						and mail_address not like '%eagleinvsys.com%');

delete from pace_masterdbo.pace_mail_user_info 
where mail_address not like '%eagleaccess.com%'  
and mail_address not like '%eagleinvsys.com%';

-- Disable PACE and STAR schedules

UPDATE pace_masterdbo.schedule_def
SET ENABLE = 'N'
WHERE ENABLE = 'Y';
 
UPDATE msgcenter_dbo.schedule sch
SET sch.active = 0
WHERE sch.active != 0;

commit;
spool off
EOF
if [ $? -ne 0 ]
then
 print "\n\n\n***************** FAILURE SEE SPOOL FILE ***********************\n\n\n"
exit 3
else
 print "\n\nSpool File trunc_tables.out generated under $REF_HOME/${ORACLE_SID}_scripts\n\n"
 print "************************************** SUCCESSFUL ***************************************\n\n\n"
fi
exit 0
