#!/usr/bin/ksh
#####################################################################################
# Author: Eagle Access LLC															#	
# Date: 10/13/2010																	#
# This requires file teaminstall.lst with 3 parameters on the same line per user:	#
#	1. emailid																		#
#	2. username																		#
#	3. Password																		#	
# You can append a line to send email to any user.									#
# Run setsid for which you want to include the SID for								#
#####################################################################################
OS=`uname`
SID=$ORACLE_SID
while read recv uid pwd
do
        echo "From: TEAM_DBA" > tmp.dat
        echo "To: ${recv}" >> tmp.dat

        echo "Subject: Your username password " >> tmp.dat
        echo "Message Your username and temporary password on $SID is now " >> tmp.dat
		echo " " >> tmp.dat
	    echo "Username-> ${uid}, Password-> ${pwd}. " >> tmp.dat
		echo "You will be prompted to change the password on you first logon. The new password policy is:

			1.	Password length should be greater than or equal to 8 character.
			2.	It should contain :
				a.	At least one letter
				b.	At least one digit
				c.	One punctuation
				d.	Should not contain $ sign.
				e.	Should differ by 3 characters from the previous password.
    Thanks

    Team DBA" >> tmp.dat

if [ "$OS" == "Linux" ]
 then
/bin/mailx -s "Your Username and Password" "${recv}" < tmp.dat
else
/usr/bin/mailx -s "Your Username and Password" "${recv}" < tmp.dat
fi
        rm tmp.dat

done < teaminstall.lst
