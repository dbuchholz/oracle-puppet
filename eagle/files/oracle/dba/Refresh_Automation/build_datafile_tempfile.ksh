#!/bin/ksh
#Script name: build_datafile_tempfile.ksh
#Usage: build_datafile_tempfile.ksh [Oracle Source SID] [Oracle Target SID]
#set -x
if [ $# -ne 2 ]
then
 print "\nUsage: $0 [Oracle Source SID] [Oracle Target SID]\n"
 print "Must enter the Oracle SID of source and target to refresh or create."
 print "The Source SID is the backup in DataDomain"
 print "The Target SID is the database on the local server you want to refresh or create."
 exit 1
fi

export source_sid=$1
export target_sid=$2

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

backup_mp=/datadomain

in_pgh=$(find $backup_mp/pgh -type d -name $source_sid)
in_evt=$(find $backup_mp/evt -type d -name $source_sid)
if [[ -n $in_evt && -n $in_pgh ]]
then
 print "Found $source_sid in Everett and Pittsburgh locations."
 print "Cannot be sure which is the correct location."
 print "Consider removing $in_pgh or $in_evt that is not the current or correct one."
 exit 1
fi
if [[ -z $in_evt && -z $in_pgh ]]
then
 print "Cound not find $source_sid in Everett or Pittsburgh locations."
 exit 2
fi
if [[ -n $in_evt ]]
then
 backup_dir=$in_evt
fi
if [[ -n $in_pgh ]]
then
 backup_dir=$in_pgh
fi

refresh_log_file=$(ls -ltr $backup_dir/${source_sid}_*_LOG.TXT  | tail -1 | awk '{print $NF}')

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$target_sid

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 print "If you want to refresh or create Database ${ORACLE_SID}, make an entry in $ORATAB for it."
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
mp=$(df -Ph | grep $ORACLE_SID | awk '{print $NF}' |sort -t- -nk2 | tail -1 | sed 's|/||g')
DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
set echo off feedback off heading off
col value for a80
select value from v\\$parameter where name='log_archive_dest_1';
EOF`
if [ $? -eq 0 ]
then
 if [[ -n $DIR_NAME ]]
 then
  mp=$(print $DIR_NAME | awk -F/ '{print $2}')
 fi
fi

(( lowest_mp=$(df -P | grep $ORACLE_SID | awk '{print $NF}' | awk -F- '{print $NF}'|sort -n|head -1) ))
df -P | grep $ORACLE_SID | grep -v '^Filesystem' | awk '{printf "%-20s %d\n",$NF,$2/1024}' | grep -v "$mp" > $log_dir/where_df_files_go_${ORACLE_SID}.txt
((buckets=$(wc -l $log_dir/where_df_files_go_${ORACLE_SID}.txt | awk '{print $1}')))
cp $log_dir/where_df_files_go_${ORACLE_SID}.txt $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt
sed -n '/List of Permanent Datafiles/,/^$/p' $refresh_log_file | sed -e '1,4d' -e '$d' | awk '{print $2,$NF}' | sort -nrk1 > $log_dir/file_list_and_size_${ORACLE_SID}.txt
(( highest_mp = lowest_mp + buckets - 1 )) 

for i in {${lowest_mp}..$highest_mp}
do
 cat /dev/null > $log_dir/bucket_file_${ORACLE_SID}_${i}.txt
done

while read size name
do
 most_free_space_mp=$(sort -nk2 $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt | tail -1 | awk '{print $1}')
 (( most_free_space_mb=$(sort -nk2 $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt | tail -1 | awk '{print $2}') ))
 largest_bucket_number=$(print $most_free_space_mp | awk -F_ '{print $NF}' | awk -F0 '{print $NF}')
 print "$size $name" >> $log_dir/bucket_file_${ORACLE_SID}_${largest_bucket_number}.txt
 remaining_mb_in_bucket=$(print "scale=0; ($most_free_space_mb - $size)" | bc)
 grep -v "$most_free_space_mp" $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt > $log_dir/temp_where_df_files_go_decrement_${ORACLE_SID}.txt
 print "$most_free_space_mp $remaining_mb_in_bucket" >> $log_dir/temp_where_df_files_go_decrement_${ORACLE_SID}.txt
 mv $log_dir/temp_where_df_files_go_decrement_${ORACLE_SID}.txt $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt
done<$log_dir/file_list_and_size_${ORACLE_SID}.txt

rm -f $log_dir/intermediate_${ORACLE_SID}.txt
for i in {${lowest_mp}..$highest_mp}
do
 ((size_of_files=$(awk '{sum+=$1} END {printf "%9d\n",sum}' $log_dir/bucket_file_${ORACLE_SID}_${i}.txt | sed 's/ //g')))
 sed -e "s/${source_sid}-../${ORACLE_SID}-0$i/g" $log_dir/bucket_file_${ORACLE_SID}_${i}.txt > $log_dir/df_bucket_file_${ORACLE_SID}_${i}.txt
 print "df_bucket_file_${ORACLE_SID}_${i}.txt $size_of_files" >> $log_dir/intermediate_${ORACLE_SID}.txt
done
paste $log_dir/where_df_files_go_${ORACLE_SID}.txt $log_dir/intermediate_${ORACLE_SID}.txt > $log_dir/result_output_${ORACLE_SID}.txt
rm -f $log_dir/bucket_list_${ORACLE_SID}.txt $log_dir/file_list_and_size_${ORACLE_SID}.txt $log_dir/intermediate_${ORACLE_SID}.txt $log_dir/where_df_files_go_${ORACLE_SID}.txt

full_threshold=95
((too_full=0))
while read mp capacity bucket_file bucket_size
do
 pct_full=$(print "scale=3; $bucket_size / $capacity * 100" | bc | awk -F. '{print $1}')
 if [ $pct_full -ge $full_threshold ]
 then
 ((too_full=1))
  print "\n$mp would be over ${full_threshold}% full.  Refresh should not continue as mount point would be ${pct_full}% full."
  print "$mp current size is: $capacity MB.  Size of files in this mount point would be $bucket_size MB.\n"
 fi
done<$log_dir/result_output_${ORACLE_SID}.txt
if [ $too_full -eq 1 ]
then
 print "Expand the mount point(s) as required by the data above."
 print "Aborting refresh"
 rm -f $log_dir/df_bucket_file_${ORACLE_SID}_*.txt $log_dir/bucket_file_${ORACLE_SID}_*.txt $log_dir/result_output_${ORACLE_SID}.txt
 exit 1
fi

restore_df_script=$(ls -ltr $backup_dir/Restore*DataTempFiles_${source_sid}_*.rcv | tail -1 | awk '{print $NF}')

rm -f $log_dir/the_df_files_${ORACLE_SID}.txt
grep 'SET NEWNAME FOR DATAFILE ' $restore_df_script > $log_dir/set_newnames_datafile_${ORACLE_SID}.txt
while read first second third fourth fifth sixth file_name
do
 file_base_name=$(print $file_name | awk '{print $NF}' | awk -F/ '{print $NF}' | awk -F\' '{print $1}')
 delimited_file=$(grep -w "$file_base_name" $log_dir/df_bucket_file_${ORACLE_SID}_*.txt | awk '{print $2}' | sed "s/$/';/g")
 print "$first $second $third $fourth $fifth $sixth $delimited_file" | sed "s/ TO / TO '/g" >> $log_dir/the_df_files_${ORACLE_SID}.txt
done<$log_dir/set_newnames_datafile_${ORACLE_SID}.txt

rm -f $log_dir/the_tempfiles_${ORACLE_SID}.txt
grep 'SET NEWNAME FOR TEMPFILE ' $restore_df_script > $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt
((tempfile_count=$(wc -l $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt | awk '{print $1}')))
((mp_count=$(wc -l $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}')))
whole_number=$(print "scale=3; $tempfile_count / $mp_count" | bc | awk -F. '{print $1}')
remainder=$(print "scale=3; $tempfile_count - $whole_number * $mp_count" | bc | awk -F. '{print $1}')

rm -f $log_dir/more_out_${ORACLE_SID}.txt
for i in {1..$whole_number}
do
 cat $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}' >> $log_dir/more_out_${ORACLE_SID}.txt
done
head -${remainder} $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}' >> $log_dir/more_out_${ORACLE_SID}.txt

rm -f $log_dir/beginning_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt $log_dir/all_the_files_${ORACLE_SID}.txt
while read first second third fourth fifth sixth file_name
do 
 print "$first $second $third $fourth $fifth $sixth" >> $log_dir/beginning_${ORACLE_SID}.txt
 print "$file_name" | awk -F/ '{print "/"$3"/"$4}' >> $log_dir/ending_${ORACLE_SID}.txt
done<$log_dir/set_newnames_tempfile_${ORACLE_SID}.txt
paste -d'\0' $log_dir/beginning_${ORACLE_SID}.txt $log_dir/more_out_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt > $log_dir/new_temp_files_${ORACLE_SID}.txt
sed "s/ TO\// TO '\//g" $log_dir/new_temp_files_${ORACLE_SID}.txt | sort -nk5 > $log_dir/brand_new_temp_files_${ORACLE_SID}.txt
cat $log_dir/the_df_files_${ORACLE_SID}.txt >> $log_dir/all_the_files_${ORACLE_SID}.txt
print "" >> $log_dir/all_the_files_${ORACLE_SID}.txt
cat $log_dir/all_the_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt > $log_dir/really_all_the_files_${ORACLE_SID}.txt
rm -f $log_dir/all_the_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt $log_dir/new_temp_files_${ORACLE_SID}.txt
rm -f $log_dir/set_newnames_datafile_${ORACLE_SID}.txt $log_dir/the_files_${ORACLE_SID}.txt $log_dir/df_bucket_file_${ORACLE_SID}_*.txt $log_dir/the_df_files_${ORACLE_SID}.txt
rm -f $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt $log_dir/more_out_${ORACLE_SID}.txt $log_dir/result_output_${ORACLE_SID}.txt $log_dir/bucket_file_${ORACLE_SID}_*.txt

sed -n '1,/^$/p' $restore_df_script > $log_dir/beginning_${ORACLE_SID}.txt
sed -n '/set until sequence /,$p' $restore_df_script > $log_dir/ending_${ORACLE_SID}.txt
print "" >> $log_dir/final_ending_${ORACLE_SID}.txt
cat $log_dir/ending_${ORACLE_SID}.txt >> $log_dir/final_ending_${ORACLE_SID}.txt
cat $log_dir/beginning_${ORACLE_SID}.txt $log_dir/really_all_the_files_${ORACLE_SID}.txt $log_dir/final_ending_${ORACLE_SID}.txt > $log_dir/RestoreDataTempFiles_${ORACLE_SID}.rcv
rm -f $log_dir/beginning_${ORACLE_SID}.txt $log_dir/really_all_the_files_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt $log_dir/final_ending_${ORACLE_SID}.txt
rm -f $log_dir/where_df_files_go_decrement_${ORACLE_SID}.txt
mkdir -p $log_dir/${ORACLE_SID}_scripts
mv $log_dir/RestoreDataTempFiles_${ORACLE_SID}.rcv $log_dir/${ORACLE_SID}_scripts/

exit 0
