#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/14/2011                                                                 #
# Usage: ImplementTrackDB.sh <SID>                                                 #
# Program: This script prepares the database for capturing REDO UNDO information   #
####################################################################################

function Chk_database {
   if $(ps -ef | grep pmon | grep ${sid} | grep -v grep >/dev/null 2>&1); then
      return 0
   else
      echo "Given SID is not valid..."
      exit 0
   fi
}


function RunUpdateDB {
set -x

DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set echo off
set feedback off
set head off
select name from v\\$datafile where name like '%02%' and rownum < 2;
!`

dname=`dirname ${DIR_NAME}`

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set echo off
set feedback off
set head off
set linesize 500
spool on
select 'create tablespace audit_undo_ts1 datafile '||''''||'${dname}/AUDIT_UNDO_TS1_01.dbf'||''''||' size 500M autoextend on next 50M maxsize unlimited logging extent management local segment space management auto'||';' from v\\$datafile where name like '%02%' and rownum < 2;
spool off

drop tablespace audit_undo_ts1 including contents and datafiles;

@on.lst

drop user audit_undo cascade;
create user audit_undo identified by audit_eagle2011 default tablespace audit_undo_ts1 temporary tablespace temp;
alter user audit_undo quota unlimited on audit_undo_ts1;
grant connect,resource,create session to audit_undo;
create table audit_undo.undo_acl(username varchar2(30));
insert into audit_undo.undo_acl values(upper('Abanerjee')); 	
insert into audit_undo.undo_acl values(upper('bmistri'));	
insert into audit_undo.undo_acl values(upper('creddy')); 		
insert into audit_undo.undo_acl values(upper('dbelambe')); 	
insert into audit_undo.undo_acl values(upper('dsahoo')); 		
insert into audit_undo.undo_acl values(upper('gsethi')); 		
insert into audit_undo.undo_acl values(upper('gdesista')); 	
insert into audit_undo.undo_acl values(upper('jwright')); 	
insert into audit_undo.undo_acl values(upper('jbumpus')); 	
insert into audit_undo.undo_acl values(upper('mchiniwar')); 	
insert into audit_undo.undo_acl values(upper('psherry')); 	
insert into audit_undo.undo_acl values(upper('rdey')); 		
insert into audit_undo.undo_acl values(upper('rverity')); 	
insert into audit_undo.undo_acl values(upper('nsarri')); 		
insert into audit_undo.undo_acl values(upper('achowdhary')); 	
insert into audit_undo.undo_acl values(upper('svyahalkar')); 	
insert into audit_undo.undo_acl values(upper('rmadhavi')); 	
insert into audit_undo.undo_acl values(upper('pkrishnan')); 	
insert into audit_undo.undo_acl values(upper('mbuotte')); 	
insert into audit_undo.undo_acl values(upper('jalcordo')); 	
insert into audit_undo.undo_acl values(upper('bkhan')); 		
insert into audit_undo.undo_acl values(upper('fdavis')); 		
insert into audit_undo.undo_acl values(upper('nkulkarni'));
insert into audit_undo.undo_acl values(upper('dbile'));

commit;

drop table audit_undo.sql_redoundo;

create table audit_undo.sql_redoundo (username varchar2(30), scn number, undo_date date,operation varchar2(32),sql_redo varchar2(4000),sql_undo varchar2(4000), sessionid number,xid raw(8));


execute dbms_logmnr_d.build ('dictionary.ora', -
 '/u01/app/oracle/audit_${sid}', -
 options => dbms_logmnr_d.store_in_flat_file);
/

exit
!`

rm on.lst
}

######## MAIN #######

sid=$1

. /usr/bin/setsid.sh $sid

Chk_database

RunUpdateDB

exit 0
