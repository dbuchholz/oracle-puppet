#!/bin/ksh
# ==================================================================================================
# NAME:		UnregisterOldDbidRman.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:  This script will Unregister the database.
#			
#
# USAGE:	UnregisterOldDbidRman.sh  <ORACLE_SID>
#
#
# ==================================================================================================
function funct_list_incarnation_of_DB
{
# Uncomment next line for debugging
#set -x
 print "\n\nListing The Incarnation Of $ORACLE_SID...\n"
 $ORACLE_HOME/bin/rman target $TRGT_DB catalog=$CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB<<EOF>$REF_HOME/${ORACLE_SID}_scripts/inc_list.log
 list incarnation of database ${ORACLE_SID};
EOF
 cat $REF_HOME/${ORACLE_SID}_scripts/inc_list.log | egrep "DB Name|--|$(print $ORACLE_SID|tr '[a-z]' '[A-Z]')" | grep -vi connected>>$REF_HOME/$(print $ORACLE_SID|tr '[A-Z]' '[a-z]')_scripts/inc_list_temp.log
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/inc_list_temp.log ]]
then
 print "\n"
 rm -f $REF_HOME/${ORACLE_SID}_scripts/inc_list.log
 cat $REF_HOME/${ORACLE_SID}_scripts/inc_list_temp.log
 rm -f $REF_HOME/${ORACLE_SID}_scripts/inc_list_temp.log
 print "\n\nDO YOU WANT TO UNREGISTER THE DATABASE AS PER ABOVE OUTPUT[y/n]: \c"
 read input
 input=$(print $input|tr '[A-Z]' '[a-z]')
 if [ $input == 'y' ]
 then
  read dbkey?"Please enter the old DB Key from above OUTPUT: "
  read dbid?"Please enter the DBID from above OUTPUT: "
  if [[ -n $dbkey ]] && [[ -n $dbid ]]
  then
   funct_unregister_database
  else
   print "\n\n*******FAILURE.....You Did Not Selected The Correct DB Key And DB ID...Please execute Again...\n"
   exit 7
  fi
 else
  print "\n\n*****You Do Not Want to Proceed...Thank You*****\n"
  exit 8
 fi
else
 print "\n\n***There is no incarnation of database $ORACLE_SID in catalog***\n\n"
 rm -f $REF_HOME/${ORACLE_SID}_scripts/inc_list.log
 rm -f $REF_HOME/${ORACLE_SID}_scripts/inc_list_temp.log
 print "\n\n***************SUCCESSFULL*************\n"
 exit 0
fi
  print "\n\nIs there More Old Incarnation exist which you want to check in the Catalog for $ORACLE_SID [y/n]: \c"
  read input2
  input2=$(print $input2|tr '[A-Z]' '[a-z]')
  if [ $input2 == 'y' ]
  then
   funct_list_incarnation_of_DB
  else
   print "\n\n***************SUCCESSFULL*************\n"
  exit 0
  fi
}

function funct_unregister_database
{
#Uncomment next line for debugging
#set -x
 print "\n\nUnregistering Database $ORACLE_SID..."
 $ORACLE_HOME/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 EXECUTE dbms_rcvcat.unregisterdatabase(${dbkey},${dbid});
EOF
}
#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

export TRGT_DB="/"
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
mkdir -p $REF_HOME/${ORACLE_SID}_scripts
INIFILE=$HOME/local/dba/backups/rman/RMANParam.ini
if [[ -a $INIFILE ]] 
then
 CATALOG_ID=$(grep CATALOG_ID ${INIFILE} | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 
 if [[ x$CATALOG_ID = 'x' ]]
 then
  print "\n\nCATALOG_ID not defined in $INIFILE\n"
 exit 4
 fi

 CATALOG_PASSWD=$( grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [[ x$CATALOG_PASSWD = 'x' ]]
 then
  print "\n\nCATALOG_PASSWD not defined in $INIFILE\n"
 exit 5
 fi
 
export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [[ x$CATALOG_DB = 'x' ]]
 then
  print "CATALOG_DB not defined in $INIFILE"
 exit 6
 fi
fi

funct_list_incarnation_of_DB

exit 0
