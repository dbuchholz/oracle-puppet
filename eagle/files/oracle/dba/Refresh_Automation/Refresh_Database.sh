#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear
if [ $# -ne 2 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
   print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
   exit 1
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 2
fi
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi
 $REF_HOME/CheckApplicationVersions.sh $1 $2
 print "\n\nDo You Want to Continue:[y/n]: \c"
 read inpti
 export inpti=$(print $inpti | tr '[A-Z]' '[a-z]')
 if [[ -n $inpti ]]
 then
 if [ $inpti == 'y' -o $inpti == 'Y' ]
 then
   $REF_HOME/ExpCustArchRuleWebConfig.sh $1 
   $REF_HOME/preserve_user_info.sh $1
   export bgimsid=$(print $1 | cut -c1-3)
   if [ $bgimsid == 'brw' ]
    then
     $REF_HOME/Export_BGIM_APP_RWRO.sh $1
   fi
#  $REF_HOME/DeleteDatabase.sh $1
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptii
   export inptii=$(print $inptii | tr '[A-Z]' '[a-z]')
   if [[ -n $inptii ]]
    then
    if [ $inptii == 'y' -o $inptii == 'Y' ]
    then
     $REF_HOME/CopyRestoreFiles.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 3
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptiii
   export inptiii=$(print $inptiii | tr '[A-Z]' '[a-z]')
   if [[ -n $inptiii ]]
    then
    if [ $inptiii == 'y' -o $inptiii == 'Y' ]
    then
     $REF_HOME/ModifyRestoreFiles.sh $1
    else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
    exit 4
    fi
    else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
    fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptiv
   export inptiv=$(print $inptiv | tr '[A-Z]' '[a-z]')
   if [[ -n $inptiv ]]
    then
    if [ $inptiv == 'y' -o $inptiv == 'Y' ]
    then
   $REF_HOME/RestoreDatabase.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 5
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptv
   export inptv=$(print $inptv | tr '[A-Z]' '[a-z]')
   if [[ -n $inptv ]]
    then
    if [ $inptv == 'y' -o $inptv == 'Y' ]
    then
   $REF_HOME/ChangeDBID.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 6
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptvi
   export inptvi=$(print $inptvi | tr '[A-Z]' '[a-z]')
   if [[ -n $inptvi ]]
    then
    if [ $inptvi == 'y' -o $inptvi == 'Y' ]
    then
   $REF_HOME/Front_End_Users.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 7
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptvii
   export inptvii=$(print $inptvii | tr '[A-Z]' '[a-z]')
   if [[ -n $inptvii ]]
    then
    if [ $inptvii == 'y' -o $inptvii == 'Y' ]
    then
   $REF_HOME/ImplementTrackDB.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 8
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptviii
   export inptviii=$(print $inptviii | tr '[A-Z]' '[a-z]')
   if [[ -n $inptviii ]]
    then
    if [ $inptviii == 'y' -o $inptviii == 'Y' ]
    then
   #add step 20 and 21 here
    print "add step 20 and 21 here"
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 9
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptvix
   export inptvix=$(print $inptvix | tr '[A-Z]' '[a-z]')
   if [[ -n $inptvix ]]
    then
    if [ $inptvix == 'y' -o $inptvix == 'Y' ]
    then
   $REF_HOME/UnregisterOldDbidRman.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 10
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi
   print "\n\nDo You Want to Continue:[y/n]: \c"
   read inptvx
   export inptvx=$(print $inptvx | tr '[A-Z]' '[a-z]')
   if [[ -n $inptvx ]]
    then
    if [ $inptvx == 'y' -o $inptvx == 'Y' ]
    then
   $REF_HOME/UpdateDeleteProdData.sh $1
   else
     print "\n\n\t\tYou Selected Not To Proceed....Thank You"
     print "\n\n\t\t************ WARNING *************************"
   exit 11
   fi
   else
    print "\n\n\t\t***** Incorrect Input *****"
    print "\n\n\t\t************ FAILURE *************************"
   fi

 else
   print "\n\n\t\tYou Selected Not To Proceed....Thank You"
   print "\n\n\t\t************ WARNING *************************"
  exit 12
 fi
 fi

exit 0
