#!/bin/ksh
# Script name: put_back_preserved_users.ksh
# Usage: put_back_preserved_users.ksh <Oracle SID>
#
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 1
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
now=$(date +'%Y%m%d_%H%M%S')


$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set feedback off heading off echo off pagesize 0
spool $REF_HOME/${ORACLE_SID}_scripts/revoke_proxies_${ORACLE_SID}.sql
select 'alter user '||client||' revoke connect through '||proxy||';' from dba_proxies;
spool off
spool $REF_HOME/${ORACLE_SID}_scripts/revoke_proxies_${ORACLE_SID}.log
@$REF_HOME/${ORACLE_SID}_scripts/revoke_proxies_${ORACLE_SID}.sql
EOF

preserve_user_file=$(ls -1tr $REF_HOME/${ORACLE_SID}_scripts/PreserveUser_*.sql)
sed '1i set echo on feedback on' $preserve_user_file > ${preserve_user_file}_${now}
cp ${preserve_user_file}_${now} $preserve_user_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
spool $REF_HOME/${ORACLE_SID}_scripts/preserve_users_${ORACLE_SID}.log
@$preserve_user_file
EOF
if [ $? -ne 0 ]
then
 print "There was an error when preserve user script was run."
 print "See log file $REF_HOME/${ORACLE_SID}_scripts/preserve_users_${ORACLE_SID}.log"
 exit 2
fi
exit 0
