#!/bin/ksh
# ==================================================================================================
# NAME:		CopyRestoreFiles.sh                            
# USAGE:	CopyRestoreFiles.sh <Target ORACLE_SID> <Source ORACLE_SID> <Backup Set> <Latest Backup> <Backup Location>
# AUTHOR:       Basit Khan
# PURPOSE:	This script will copy the Restore files from backup location to $REF_HOME/{ORACLE_SID}_scripts
#			location for the database which is being refreshed.
# ==================================================================================================

function funct_restore_files
{
 redo_rcv_file=$(ls $Bkp_Loc/Restore*Redo_${sid}_${backup_set}.rcv)
 control_rcv_file=$(ls $Bkp_Loc/Restore*ControlFile_${sid}_${backup_set}.rcv)
 datafile_rcv_file=$(ls $Bkp_Loc/Restore*DataTempFiles_${sid}_${backup_set}.rcv)
 init_ora_file=$(ls $Bkp_Loc/${sid}_${backup_set}_init${sid}.ora)
 cp -p $redo_rcv_file $REF_HOME/${ORACLE_SID}_scripts
 cp -p $control_rcv_file $REF_HOME/${ORACLE_SID}_scripts
 cp -p $datafile_rcv_file $REF_HOME/${ORACLE_SID}_scripts
 cp -p $init_ora_file $REF_HOME/${ORACLE_SID}_scripts
 print "Restore files for $ORACLE_SID is copied to $REF_HOME/${ORACLE_SID}_scripts location from $Bkp_Loc ...Please check"
}

function funct_check_bkp_location
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
set heading off feedback off
select instance from inf_monitor.databases
where sid='$sid'
and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem Please Rectify and Execute Again
Aborting Here...."
 exit 1 
fi

MacIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select mac_instance from inf_monitor.databases
 where sid='$sid'
 and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem Please Rectify and Execute Again
Aborting Here...."
 exit 1
fi

DbIns=$(print $DbIns|tr -d " ")
if [[ -n $DbIns ]]
then
 Bkp_Loc=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select backup_location
 from inf_monitor.backup_runs
 where db_instance='$DbIns' and
 start_time=(select max(start_time) from inf_monitor.backup_runs where db_instance='$DbIns') and rownum<2;
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
 exit 2
 fi
fi

MacIns=$(print $MacIns|tr -d " ")
if [[ -n $MacIns ]]
then
 Dbserver=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select name from inf_monitor.machines
 where instance='$MacIns';
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
  exit 2
 fi
fi

if [[ -n $Bkp_Loc  ]] && [[ -n $Dbserver ]]
then
 Bkp_Loc=$(print $Bkp_Loc|tr -d " ")
 Dbserver=$(print $Dbserver|tr -d " ")
 print "Database $sid is on $Dbserver and it's Backup Location is $Bkp_Loc ."
else
 print "\n\nYou Entered the invalid SID to check the backup which does not exist in INFPRD1 database\nAborting Here....\n"
 exit 3
fi

if [[ ! -d $Bkp_Loc ]]
then
 print  "\n\n***Backup Location May not exist...Please check***\n"
 exit 7
fi

}


#################### MAIN ##########################
if [ $# -lt 4 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <Source ORACLE_SID> <Target ORACLE_SID> <Backup Set> <Latest Backup> [Backup Location]\n"
   print "Backup Set is of the form: YYYYMMDD_######"
   print "Latest Backup = 0 means to use the latest backup"
   print "Latest Backup = 1 means to not use the latest backup"
   print "Backup Location is an optional parameter"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
export sid=$2
# Verify that the second parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "\n\n\t\tCannot find Instance $ORACLE_SID in $ORATAB\n\n"
 exit 2
fi
backup_set=$3
latest_backup=$4
backup_location=$5

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/LOG

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

if [[ -z $backup_location ]]
then
 funct_check_bkp_location
else
 Bkp_Loc=$backup_location
fi

funct_restore_files

specific_control_file=$(ls $Bkp_Loc/cf_${sid}_${backup_set}_*_full_ctrl)
grep -vi autobackup $control_rcv_file > $REF_HOME/${sid}_int_cf.txt

sed -n '1,/^run {/p' $REF_HOME/${sid}_int_cf.txt > $REF_HOME/specific_top_section_${sid}.txt
print "restore controlfile from '$specific_control_file';" > $REF_HOME/specific_middle_section_${sid}.txt
sed -n '1,/^run {/!p' $REF_HOME/${sid}_int_cf.txt > $REF_HOME/specific_bottom_section_${sid}.txt

cat $REF_HOME/specific_top_section_${sid}.txt $REF_HOME/specific_middle_section_${sid}.txt $REF_HOME/specific_bottom_section_${sid}.txt > $REF_HOME/${ORACLE_SID}_scripts/${sid}_specific_cf.rcv
rm -f $REF_HOME/specific_top_section_${sid}.txt $REF_HOME/specific_middle_section_${sid}.txt $REF_HOME/specific_bottom_section_${sid}.txt

exit 0
