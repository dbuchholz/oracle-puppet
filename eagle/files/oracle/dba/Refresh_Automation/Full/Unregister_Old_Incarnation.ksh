#!/bin/ksh
# ==================================================================================================
# NAME:		Unregister_Old_Incarnation.ksh
# AUTHOR:	Basit Khan                                    
# PURPOSE:  This script will Unregister the database.
# USAGE:	Unregister_Old_Incarnation.ksh <ORACLE_SID>
# ==================================================================================================
function funct_list_incarnation_of_DB
{
 $ORACLE_HOME/bin/rman target $TRGT_DB catalog=$CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB<<EOF>$REF_HOME/${ORACLE_SID}_scripts/inc_list.log
 list incarnation of database ${ORACLE_SID};
EOF
current_dbid=$(grep DBID= $REF_HOME/${ORACLE_SID}_scripts/inc_list.log | awk -F= '{print $2}' | awk -F\) '{print $1}')
sed -e '1,/^\-\-\-\-\-/d' -e '/^$/,$d' $REF_HOME/${ORACLE_SID}_scripts/inc_list.log | grep -v $current_dbid | awk '{print $1,$4}' | sort -u > $REF_HOME/${ORACLE_SID}_scripts/old_incarnations.log
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/old_incarnations.log ]]
then
 while read old_dbkey old_dbid
 do
  funct_unregister_database
 done<$REF_HOME/${ORACLE_SID}_scripts/old_incarnations.log
fi
}

function funct_unregister_database
{
 print "Unregistering Database $ORACLE_SID..."
 $ORACLE_HOME/bin/sqlplus -s $CATALOG_ID/$CATALOG_PASSWD@$CATALOG_DB<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 EXECUTE dbms_rcvcat.unregisterdatabase(${old_dbkey},${old_dbid});
EOF
 if [ $? -eq 0 ]
 then
  print "Unregistered Old Incarnation for $ORACLE_SID      DBKEY: $old_dbkey  DBID: $old_dbid from RMAN catalog."
 else
  print "There was an error when attempting to unregister DBKEY: $old_dbkey  DBID: $old_dbid from RMAN catalog."
 fi
}
#################### MAIN ##########################
if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

export TRGT_DB="/"
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts
INIFILE=$HOME/local/dba/backups/rman/RMANParam.ini
if [[ -f $INIFILE ]] 
then
 CATALOG_ID=$(grep CATALOG_ID ${INIFILE} | awk -F= '{print $2}')
 export CATALOG_ID=$(print $CATALOG_ID | tr -d "'" | tr -d '"')
 
 if [[ x$CATALOG_ID = 'x' ]]
 then
  print "\n\nCATALOG_ID not defined in $INIFILE\n"
 exit 4
 fi

 CATALOG_PASSWD=$( grep CATALOG_PASSWD $INIFILE | awk -F= '{print $2}')
 CATALOG_PASSWD=$(print $CATALOG_PASSWD | tr -d "'" | tr -d '"')
 if [[ x$CATALOG_PASSWD = 'x' ]]
 then
  print "\n\nCATALOG_PASSWD not defined in $INIFILE\n"
 exit 5
 fi
 
export CATALOG_PASSWD=$($PAR_HOME/GetDec.cmd $CATALOG_PASSWD)

 CATALOG_DB=$(grep CATALOG_DB $INIFILE | awk -F= '{print $2}')
 export CATALOG_DB=$(print $CATALOG_DB | tr -d "'" | tr -d '"')
 if [[ x$CATALOG_DB = 'x' ]]
 then
  print "CATALOG_DB not defined in $INIFILE"
 exit 6
 fi
fi

funct_list_incarnation_of_DB

exit 0
