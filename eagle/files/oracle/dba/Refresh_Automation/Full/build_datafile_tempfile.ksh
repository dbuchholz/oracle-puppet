#!/bin/ksh
# Script name: build_datafile_tempfile.ksh
# Usage: build_datafile_tempfile.ksh -t [Oracle Target SID] -s [Oracle Source SID] -b [Backup Set] -m [Backup Location] -a [Recovery Timestamp]
# The -t, -s and -b parameters are mandatory.  You must supply values to these switches.
#
# If Recovery Timestamp is specified, it must be of this format: YYYY-MM-DD:hh24:mi:ss
# For example: 2014-06-15:05:50:56
# This specifies June 15, 2014 at 5:50am plus 56 seconds.
# Quotes are not required around this value when passed in with the "-a" Switch
#

# Parse input arguments
while (( $# > 0 ))
do
 case "$1" in
  -h|-help) print
            usage $0
            ;;
  -t) ORACLE_SID=$2
      shift
      ;;
  -s) sid=$2
      shift
      ;;
  -b) backup_set=$2
      if [[ -n $backup_set ]]
      then
       shift
      fi
      ;;
  -m) backup_location=$2
      if [[ -n $backup_location ]]
      then
       shift
      fi
      ;;
  -a) recovery_timestamp=$2
      if [[ -n $recovery_timestamp ]]
      then
       shift
      fi
      ;;
   *) print "\n\t\tInvalid Arguments!"
      usage $0
      ;;
 esac
 shift
done

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

if [[ -n $ORACLE_SID ]]
then
 export target_sid=$ORACLE_SID
else
 print "A target Oracle sid was not entered."
 print "Aborting here."
 exit 1
fi
log_dir=$script_dir/${target_sid}_scripts
mkdir -p $log_dir

if [[ -n $sid ]]
then
 export source_sid=$sid
else
 print "A source Oracle sid was not entered."
 print "Aborting here."
 exit 2
fi

if [[ -n $backup_location ]]
then
 if [[ ! -d $backup_location ]]
 then
  print "A custom backup location was entered but it does not exist."
  print "The location entered is: $backup_location"
  exit 3
 fi 
else
 backup_mp=/datadomain
 in_pgh=$(find $backup_mp/pgh -type d -name $source_sid)
 in_evt=$(find $backup_mp/evt -type d -name $source_sid)
 if [[ -n $in_evt && -n $in_pgh ]]
 then
  print "Found $source_sid in Everett and Pittsburgh locations."
  print "Cannot be sure which is the correct location."
  print "Consider removing $in_pgh or $in_evt that is not the current or correct one."
  exit 4
 fi
 if [[ -z $in_evt && -z $in_pgh ]]
 then
  print "Could not find $source_sid in Everett or Pittsburgh locations."
  exit 5
 fi
 if [[ -n $in_evt ]]
 then
  backup_location=$in_evt
 fi
 if [[ -n $in_pgh ]]
 then
  backup_location=$in_pgh
 fi
fi

if [[ -z $backup_set ]]
then
 print "The backup set is mandatory."
 print "Aborting here."
 exit 6
fi

if [[ -n $recovery_timestamp ]]
then
 print "$recovery_timestamp" | grep -w '20[1-9][0-9]-[0-1][0-9]-[0-3][0-9]:[0-2][0-9]:[0-5][0-9]:[0-5][0-9]' > /dev/null
 if [ $? -ne 0 ]
 then
  print "The recovery timestamp was not specified in the correct format."
  print "The format must be:  YYYY-MM-DD:hh24:mi:ss"
  exit 7
 fi
fi

refresh_log_file=$(ls -l $backup_location/${source_sid}_${backup_set}_LOG.TXT  | awk '{print $NF}')

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 DF='df -lP'
 DFH='df -lPh'
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 DF='df -lk'
 DFH='df -lh'
fi

export ORACLE_SID=$target_sid

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 print "If you want to refresh or create Database ${ORACLE_SID}, make an entry in $ORATAB for it."
 exit 8
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
mp=$($DFH | grep "\/${ORACLE_SID}-" | awk '{print $NF}' |sort -t- -nk2 | tail -1 | sed 's|/||g')
DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
set echo off feedback off heading off
col value for a80
select value from v\\$parameter where name='log_archive_dest_1';
EOF
`
if [ $? -eq 0 ]
then
 if [[ -n $DIR_NAME ]]
 then
  mp=$(print $DIR_NAME | awk -F/ '{print $2}')
 fi
fi

$DF | grep "\/${ORACLE_SID}-" | grep -v '^Filesystem' | awk '{printf "%-20s %d\n",$NF,$2/1024}' | grep -v "$mp" > $log_dir/where_df_files_go_${ORACLE_SID}.txt
((buckets=$(wc -l $log_dir/where_df_files_go_${ORACLE_SID}.txt | awk '{print $1}')))
sed -n '/List of Permanent Datafiles/,/^$/p' $refresh_log_file | sed -e '1,4d' -e '$d' | awk '{print $2,$NF}' | sort -nrk1 > $log_dir/file_list_and_size_${ORACLE_SID}.txt

i=1
while [[ $i -le $buckets ]]
do
 cat /dev/null > $log_dir/bucket_file_${ORACLE_SID}_${i}.txt
 ((i+=1))
done


while read size name
do
 rm -f $log_dir/bucket_list_${ORACLE_SID}.txt
 i=1
 while [[ $i -le $buckets ]]
 do
  ((size_of_bucket=$(awk '{sum+=$1} END {printf "%9d\n",sum}' $log_dir/bucket_file_${ORACLE_SID}_${i}.txt | sed 's/ //g')))
  print "$i $size_of_bucket" >> $log_dir/bucket_list_${ORACLE_SID}.txt
  ((i+=1))
 done
 ((bucket_number=$(sort -nk2 $log_dir/bucket_list_${ORACLE_SID}.txt | head -1 | awk '{print $1}')))
 smallest_bucket=$(sort -nk2 $log_dir/bucket_list_${ORACLE_SID}.txt | head -1 | awk '{print $NF}')
 print "$size $name" >> $log_dir/bucket_file_${ORACLE_SID}_${bucket_number}.txt
done<$log_dir/file_list_and_size_${ORACLE_SID}.txt
rm -f $log_dir/intermediate_${ORACLE_SID}.txt

i=1
while [[ $i -le $buckets ]]
do
 ((size_of_files=$(awk '{sum+=$1} END {printf "%9d\n",sum}' $log_dir/bucket_file_${ORACLE_SID}_${i}.txt | sed 's/ //g')))
 sed "s/$source_sid/$ORACLE_SID/g" $log_dir/bucket_file_${ORACLE_SID}_${i}.txt > $log_dir/df_bucket_file_${ORACLE_SID}_${i}.txt
 print "df_bucket_file_${ORACLE_SID}_${i}.txt $size_of_files" >> $log_dir/intermediate_${ORACLE_SID}.txt
 ((i+=1))
done
paste $log_dir/where_df_files_go_${ORACLE_SID}.txt $log_dir/intermediate_${ORACLE_SID}.txt > $log_dir/result_output_${ORACLE_SID}.txt
rm -f $log_dir/bucket_list_${ORACLE_SID}.txt $log_dir/file_list_and_size_${ORACLE_SID}.txt $log_dir/intermediate_${ORACLE_SID}.txt $log_dir/where_df_files_go_${ORACLE_SID}.txt

full_threshold=95
((too_full=0))
while read mp capacity bucket_file bucket_size
do
 pct_full=$(print "scale=3; $bucket_size / $capacity * 100" | bc | awk -F. '{print $1}')
 if [ $pct_full -ge $full_threshold ]
 then
 ((too_full=1))
  print "\n$mp would be over ${full_threshold}% full.  Refresh should not continue as mount point would be ${pct_full}% full."
  print "$mp current size is: $capacity MB.  Size of files in this mount point would be $bucket_size MB.\n"
 fi
done<$log_dir/result_output_${ORACLE_SID}.txt
if [ $too_full -eq 1 ]
then
 print "Expand the mount point(s) as required by the data above."
 print "Aborting refresh"
 rm -f $log_dir/df_bucket_file_${ORACLE_SID}_*.txt $log_dir/bucket_file_${ORACLE_SID}_*.txt $log_dir/result_output_${ORACLE_SID}.txt
 exit 9
fi

i=1
while [[ $i -le $buckets ]]
do
 sed "s/\/${ORACLE_SID}-../${ORACLE_SID}-0$i/g" $log_dir/df_bucket_file_${ORACLE_SID}_${i}.txt > $log_dir/bucket_file_${ORACLE_SID}_${i}.txt
 ((i+=1))
done
rm -f $log_dir/df_bucket_file_${ORACLE_SID}_${i}.txt
restore_df_script=$(ls -l $backup_location/Restore*DataTempFiles_${source_sid}_${backup_set}.rcv | awk '{print $NF}')
((exce=$(grep 'SET NEWNAME ' $restore_df_script | awk '{print $NF}' | awk -F/ '{print $NF}' | sed -e 's/;//g' -e "s/'//g" | sort | uniq -d | wc -l)))
if [ $exce -gt 0 ]
then
 print "The following file names exist in two mount points.  Cannot continue."
 print "Must separate these files manually so as not to place them in the same filesystem"
 print "and risk overwriting one of the files."
 print "The duplicate file names are shown here:\n"
 grep 'SET NEWNAME ' $restore_df_script | awk '{print $NF}' | awk -F/ '{print $NF}' | sed -e 's/;//g' -e "s/'//g" | sort | uniq -d
 exit 10
fi

rm -f $log_dir/the_df_files_${ORACLE_SID}.txt
grep 'SET NEWNAME FOR DATAFILE ' $restore_df_script > $log_dir/set_newnames_datafile_${ORACLE_SID}.txt
while read first second third fourth fifth sixth file_name
do
 file_base_name=$(print $file_name | awk '{print $NF}' | awk -F/ '{print $NF}' | awk -F\' '{print $1}')
 delimited_file=$(grep -w "$file_base_name" $log_dir/bucket_file_${ORACLE_SID}_*.txt | awk '{print $2}' | sed -e "s/^/'\//g" -e "s/$/';/g")
 print "$first $second $third $fourth $fifth $sixth $delimited_file" >> $log_dir/the_df_files_${ORACLE_SID}.txt
done<$log_dir/set_newnames_datafile_${ORACLE_SID}.txt

rm -f $log_dir/the_tempfiles_${ORACLE_SID}.txt
grep 'SET NEWNAME FOR TEMPFILE ' $restore_df_script > $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt
((tempfile_count=$(wc -l $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt | awk '{print $1}')))
((mp_count=$(wc -l $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}')))
whole_number=$(print "scale=3; $tempfile_count / $mp_count" | bc | awk -F. '{print $1}')
if [[ -z $whole_number ]]
then
 whole_number=0
fi
remainder=$(print "scale=3; $tempfile_count - $whole_number * $mp_count" | bc | awk -F. '{print $1}')

rm -f $log_dir/more_out_${ORACLE_SID}.txt
i=1
while [[ $i -le $whole_number ]]
do
 cat $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}' >> $log_dir/more_out_${ORACLE_SID}.txt
 ((i+=1))
done
head -${remainder} $log_dir/result_output_${ORACLE_SID}.txt | awk '{print $1}' >> $log_dir/more_out_${ORACLE_SID}.txt

rm -f $log_dir/beginning_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt $log_dir/all_the_files_${ORACLE_SID}.txt
while read first second third fourth fifth sixth file_name
do 
 print "$first $second $third $fourth $fifth $sixth" >> $log_dir/beginning_${ORACLE_SID}.txt
 print "$file_name" | awk -F/ '{print "/"$3"/"$4}' >> $log_dir/ending_${ORACLE_SID}.txt
done<$log_dir/set_newnames_tempfile_${ORACLE_SID}.txt
paste -d'\0' $log_dir/beginning_${ORACLE_SID}.txt $log_dir/more_out_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt > $log_dir/new_temp_files_${ORACLE_SID}.txt
sed "s/ TO\// TO '\//g" $log_dir/new_temp_files_${ORACLE_SID}.txt | sort -nk5 > $log_dir/brand_new_temp_files_${ORACLE_SID}.txt
cat $log_dir/the_df_files_${ORACLE_SID}.txt >> $log_dir/all_the_files_${ORACLE_SID}.txt
print "" >> $log_dir/all_the_files_${ORACLE_SID}.txt
cat $log_dir/all_the_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt > $log_dir/really_all_the_files_${ORACLE_SID}.txt
rm -f $log_dir/all_the_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt $log_dir/brand_new_temp_files_${ORACLE_SID}.txt $log_dir/new_temp_files_${ORACLE_SID}.txt
rm -f $log_dir/set_newnames_datafile_${ORACLE_SID}.txt $log_dir/the_files_${ORACLE_SID}.txt $log_dir/df_bucket_file_${ORACLE_SID}_*.txt $log_dir/the_df_files_${ORACLE_SID}.txt
rm -f $log_dir/set_newnames_tempfile_${ORACLE_SID}.txt $log_dir/more_out_${ORACLE_SID}.txt $log_dir/result_output_${ORACLE_SID}.txt $log_dir/bucket_file_${ORACLE_SID}_*.txt

sed -n '1,/^$/p' $restore_df_script > $log_dir/beginning_${ORACLE_SID}.txt
sed -n '/set until sequence /,$p' $restore_df_script > $log_dir/ending_${ORACLE_SID}.txt
print "" >> $log_dir/final_ending_${ORACLE_SID}.txt
cat $log_dir/ending_${ORACLE_SID}.txt >> $log_dir/final_ending_${ORACLE_SID}.txt
cat $log_dir/beginning_${ORACLE_SID}.txt $log_dir/really_all_the_files_${ORACLE_SID}.txt $log_dir/final_ending_${ORACLE_SID}.txt > $log_dir/RestoreDataTempFiles_${ORACLE_SID}.rcv
rm -f $log_dir/beginning_${ORACLE_SID}.txt $log_dir/really_all_the_files_${ORACLE_SID}.txt $log_dir/ending_${ORACLE_SID}.txt $log_dir/final_ending_${ORACLE_SID}.txt

if [[ -n $recovery_timestamp ]]
then
 sed "s/.*set until sequence .*/SET UNTIL TIME \"to_date('$recovery_timestamp','YYYY-MM-DD:hh24:mi:ss')\";/g" $log_dir/RestoreDataTempFiles_${ORACLE_SID}.rcv > $log_dir/working_RestoreDataTempFiles_${ORACLE_SID}.rcv
 mv $log_dir/working_RestoreDataTempFiles_${ORACLE_SID}.rcv $log_dir/RestoreDataTempFiles_${ORACLE_SID}.rcv
fi

exit 0
