#!/bin/ksh
# Script name: put_back_dba_install_users.ksh
# Usage: put_back_dba_install_users.ksh <Oracle SID>

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

PAR_HOME=$HOME/local/dba
REF_HOME=$PAR_HOME/Refresh_Automation/Full

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
spool $REF_HOME/${ORACLE_SID}_scripts/dba_install_users_${ORACLE_SID}.log
@$REF_HOME/DBAUsers.sql
@$REF_HOME/INSTALLUser.sql
EOF

exit 0
