#!/bin/ksh
# ==================================================================================================
# NAME:		CheckApplicationVersions.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will check the star and pace version for the target and source database
#
# USAGE:	CheckApplicationVersions.sh  <Target_SID> <Source_SID>
#
#
# ==================================================================================================
funct_pace_version()
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from inf_monitor.databases
 where sid='$psid'
 and dataguard='N'
 and status='OPEN';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the password for pace."
 print "Aborting Here..."
 exit 6
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 version=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select $app_ver 
 from inf_monitor.databases
 where instance=$DbIns;
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again."
  print "Aborting Here..."
 exit 7
 fi
version=$(print $version|tr -d " ")
fi
}
#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
 print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 print "If you want to refresh $1 on this host, make an entry in $ORATAB for it.\n"
 exit 2
fi

export ORACLE_SID=$1  #Target
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again."
 print "Aborting Here..."
 exit 3
fi

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

print "\t\tChecking the application version for $1 and $2"
export psid=$1
export app_ver=pace_version
funct_pace_version
export tpver=$version
export app_ver=star_version
funct_pace_version
export tsver=$version

export psid=$2
export app_ver=pace_version
funct_pace_version
export spver=$version 
export app_ver=star_version
funct_pace_version
export ssver=$version

ps -ef | grep "ora_smon_$ORACLE_SID" | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
# PACE of Target
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/pace_of_target_${ORACLE_SID}.txt
set pagesize 0 feedback off trimspool on echo off
select version from pace_masterdbo.eagle_product_version 
where instance = (select max(instance) from pace_masterdbo.eagle_product_version 
where  upper(product_type) in  ('EDM', 'PACE') 
and instance not in (select instance from pace_masterdbo.eagle_product_version 
where upper(comments) like '%ADDENDUM%'));
EOF
tpver=$(cat $REF_HOME/pace_of_target_${ORACLE_SID}.txt)

# STAR of Target
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/star_of_target_${ORACLE_SID}.txt
set pagesize 0 feedback off trimspool on echo off
select version from pace_masterdbo.eagle_product_version 
where instance = (select max(instance)from pace_masterdbo.eagle_product_version 
where  upper(product_type) in  ('EDM', 'STAR-DB') 
and instance not in (select instance from pace_masterdbo.eagle_product_version 
where upper(comments) like '%ADDENDUM%'));
EOF
tsver=$(cat $REF_HOME/star_of_target_${ORACLE_SID}.txt)
fi

rm -f $REF_HOME/pace_of_target_${ORACLE_SID}.txt $REF_HOME/star_of_target_${ORACLE_SID}.txt

#Check Pace Version
if [[ -n $spver && -n $tpver ]]
then
 if [ $spver = $tpver ]
 then 
  print "\t\t$1's pace version and $2's pace version is same"
  print "\t\t\t\t$1's pace version:$spver"
  print "\t\t\t\t$2's pace version:$tpver"
 else
  print "\t\t$1's pace version and $2's pace version is different"
  print "\t\t\t\t$1's pace version:$spver"
  print "\t\t\t\t$2's pace version:$tpver"
  print "\t\tPlease Do not Proceed With Refresh As the Application Versions are Different"
  exit 4
 fi
else
 print "\t\tApplication versions for PACE are not updated in infprd1 database"
 exit 9
fi

#Check Star Version
if [[ -n $ssver && -n $tsver ]]
then
 if [ $ssver = $tsver ]
 then 
  print "\t\t$1's star version and $2's star version is same"
  print "\t\t\t\t$1's star version:$ssver"
  print "\t\t\t\t$2's star version:$tsver"
 else
  print "\t\t$1's star version and $2's star version is different"
  print "\t\t\t\t$1's star version:$ssver"
  print "\t\t\t\t$2's star version:$tsver"
  print "\t\tPlease Do not Proceed With Refresh As the Application Versions are Different"
  print "\t\t********************** WARNING ******************************"
  exit 5
 fi
else
 print "\t\tApplication versions for STAR are not updated in infprd1 database"
 exit 8
fi

exit 0
