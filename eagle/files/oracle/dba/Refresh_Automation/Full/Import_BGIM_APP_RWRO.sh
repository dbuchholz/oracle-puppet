#!/bin/ksh

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

PAR_HOME=$HOME/local/dba
REF_HOME=$PAR_HOME/Refresh_Automation/Full
LOG_DIR=$REF_HOME/${ORACLE_SID}_scripts
mkdir -p $LOG_DIR

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
drop directory exp_bgim_app_rwro;
create directory exp_bgim_app_rwro as '$LOG_DIR';
grant read,write on directory exp_bgim_app_rwro to public;
drop user BGIM_APP_RW cascade;
drop user BGIM_APP_RO cascade;
EOF

$ORACLE_HOME/bin/impdp userid=\"/ as sysdba\" directory=exp_bgim_app_rwro dumpfile=bgim_app_rwro.dmp schemas=BGIM_APP_RW,BGIM_APP_RO logfile=import_bgim_app_rwro.log content=all

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
@$LOG_DIR/BGIM_TAB_PRIVS_RUN.sql
EOF

print "\n\n\nBGIM_APP_RW and BGIM_APP_RO are dropped and reimported...\n\n\n"

exit 0
