#!/bin/ksh
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

PAR_HOME=$HOME/local/dba
REF_HOME=$PAR_HOME/Refresh_Automation/Full
LOG_DIR=$REF_HOME/${ORACLE_SID}_scripts
mkdir -p $LOG_DIR

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
drop directory exp_bgim_app_rwro;
WHENEVER SQLERROR EXIT FAILURE
create directory exp_bgim_app_rwro as '$LOG_DIR';
grant read,write on directory exp_bgim_app_rwro to public;
set long 9999999 pagesize 0 trimspool on feedback off linesize 200 longchunksize 9999999
spool $LOG_DIR/BGIM_TAB_PRIVS.sql
select dbms_metadata.get_granted_ddl('OBJECT_GRANT','BGIM_APP_RW') from dual;
select dbms_metadata.get_granted_ddl('OBJECT_GRANT','BGIM_APP_RO') from dual;
EOF
if [ $? -ne 0 ]
then
 print "Error when extracting grants for BGIM_APP_RW or BGIM_APP_RO"
 print "Aborting here..."
 exit 2
fi

egrep -v '^ERROR:$|^ORA-31608:|^ORA-06512:|^$' $LOG_DIR/BGIM_TAB_PRIVS.sql | sed -e 's/$/;/g' -e 's/"//g' -e 's/^[ \t]*//' > $LOG_DIR/BGIM_TAB_PRIVS_RUN.sql

rm -f $LOG_DIR/bgim_app_rwro.dmp
$ORACLE_HOME/bin/expdp userid=\"/ as sysdba\" directory=exp_bgim_app_rwro dumpfile=bgim_app_rwro.dmp schemas=BGIM_APP_RW,BGIM_APP_RO logfile=export_bgim_app_rwro.log content=all
grep 'successfully completed at ' $LOG_DIR/export_bgim_app_rwro.log > /dev/null
if [ $? -ne 0 ]
then
 print "Error when exporting Schemas BGIM_APP_RW or BGIM_APP_RO"
 print "Aborting here..."
 exit 3
fi

exit 0
