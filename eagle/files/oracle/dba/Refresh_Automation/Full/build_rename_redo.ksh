#!/bin/ksh
#Script name: build_rename_redo.ksh
#Usage: build_rename_redo.ksh [Oracle Target SID] [Oracle Source SID]

if [ $# -ne 2 ]
then
 print "\nUsage: $0 [Oracle Target SID] [Oracle Source SID]\n"
 print "Must enter the Oracle SID of source and target to refresh or create."
 print "The Source SID is the backup in DataDomain"
 print "The Target SID is the database on the local server you want to refresh or create."
 exit 1
fi

export target_sid=$1
export source_sid=$2

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir/${target_sid}_scripts

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 DF='df -lP'
 DFH='df -lPh'
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 DF='df -lk'
 DFH='df -lh'
fi

export ORACLE_SID=$target_sid

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 print "If you want to refresh or create Database ${ORACLE_SID}, make an entry in $ORATAB for it."
 exit 2
fi

PAR_HOME=$HOME/local/dba
REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
mp=$($DFH | grep $ORACLE_SID | awk '{print $NF}' |sort -t- -nk2 | tail -1 | sed 's|/||g')
DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
set echo off feedback off heading off
col value for a80
select value from v\\$parameter where name='log_archive_dest_1';
EOF
`
if [ $? -eq 0 ]
then
 if [[ -n $DIR_NAME ]]
 then
  mp=$(print $DIR_NAME | awk -F/ '{print $2}')
 fi
fi

$DF /${ORACLE_SID}* | grep -v '^Filesystem' | grep -v "$mp" | awk '{print $NF}' | sort | head -2 |sed 's|$|/oradata/|g' > $log_dir/first_two_mp_${ORACLE_SID}.txt

sed '/^$/d' $log_dir/Restore*Redo_${source_sid}_*.rcv > $log_dir/file5.txt
awk '{print $NF}' $log_dir/file5.txt | sed -e "s/'//g" -e 's/;//g' | awk -F/ '{print $NF}' > $log_dir/basenames.txt
(( redo_file_count=$(wc -l $log_dir/basenames.txt | awk '{print $1}') ))
if [ $(($redo_file_count % 2)) -ne 0 ]
then
 (( redo_file_count = redo_file_count + 1))
fi

(( redo_group_count=$(print "scale=0; $redo_file_count / 2" | bc) ))

rm -f $log_dir/file3.txt

i=1
while [[ $i -le $redo_group_count ]]
do
 cat $log_dir/first_two_mp_${ORACLE_SID}.txt >> $log_dir/file3.txt
 ((i+=1))
done

paste -d'\0' $log_dir/file3.txt $log_dir/basenames.txt > $log_dir/file4.txt
grep -v '.*\/$' $log_dir/file4.txt > $log_dir/file8.txt

sed "s/' TO '.*$//g" $log_dir/file5.txt | sed "s/$/' TO '/g" > $log_dir/file6.txt
paste -d'\0' $log_dir/file6.txt $log_dir/file8.txt | sed "s/$/';/g" > $log_dir/run_rename_redo.rcv
rm -f $log_dir/file3.txt $log_dir/file4.txt $log_dir/file5.txt $log_dir/file6.txt $log_dir/file8.txt
rm -f $log_dir/first_two_mp_${ORACLE_SID}.txt $log_dir/basenames.txt

exit 0
