#!/bin/ksh
# ==================================================================================================
# NAME:		verify_oracle_versions.ksh                            
# USAGE:        verify_oracle_versions.ksh  <Target_SID> <Source_SID>
# AUTHOR:       Frank Davis                   
# PURPOSE:	To compare the source and target versions of a refresh to verify the Oracle versions
#               are the same.
# ==================================================================================================
funct_db_version()
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from inf_monitor.databases
 where sid='$psid'
 and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem when getting DB Instance for $psid"
 print "Aborting Here...."
 exit 6
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 version=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select oracle_version from inf_monitor.databases where instance=$DbIns;
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem getting Oracle version for $psid\nAborting Here....\n"
  exit 7
 fi
version=$(print $version|tr -d " ")
fi
}
#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
 print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 print "If you want to refresh $1 on this host, make an entry in $ORATAB for it.\n"
 exit 2
fi

export ORACLE_SID=$1  #Target
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

print "\t\tVerifying the database versions are the same for $1 and $2\n"
export psid=$ORACLE_SID
funct_db_version
target_db_ver=$version

export psid=$2
funct_db_version
source_db_ver=$version 

ps -ef | grep "ora_smon_$ORACLE_SID" | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/oracle_version_of_target_${ORACLE_SID}.txt
 set pagesize 0 feedback off trimspool on echo off
 select * from v\$version;
EOF
 target_db_ver=$(grep 'Oracle Database ' $REF_HOME/oracle_version_of_target_${ORACLE_SID}.txt | awk '{print $7}')
fi

if [ $target_db_ver != $source_db_ver ]
then
 print "Oracle versions are not equal between source and target database"
 print "The source Oracle version is: $source_db_ver"
 print "The target Oracle version is: $target_db_ver"
 exit 4
fi

rm -f $REF_HOME/oracle_version_of_target_${ORACLE_SID}.txt

exit 0
