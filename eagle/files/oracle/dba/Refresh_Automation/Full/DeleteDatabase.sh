#!/bin/ksh
# ==================================================================================================
# NAME:		DeleteDatabase.sh
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:  This script will create Pfile for backup, delete_database_files.sh, 
#			Shutdown the database and Delete the Dump directories and archive location
#			
#
# USAGE:	DeleteDatabase.sh <ORACLE_SID>
#
#
# ==================================================================================================
#################### MAIN ##########################
if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID>\n"
 print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts
 
# This code does not work as intended.  This expects to see an error if there are connections, which is untrue.
#print "\t\tChecking for Application Connections"
#$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$PAR_HOME/session_${ORACLE_SID}_temp.txt
# WHENEVER SQLERROR EXIT FAILURE
# set heading off feedback off
# select username,count(*) from v\$session
# where TYPE!='BACKGROUND'
# and username not in('SYS','SYSTEM','DBSNMP')
# group by username;
#EOF
#if [ $? -ne 0 ]
#then
# print "\n\n\t\t There is some issue while checking the application connections...Please check\n\nAborting Here...."
# exit 2
#fi

if [[ -s $PAR_HOME/session_${ORACLE_SID}_temp.txt ]]
then
 print "\n\n***Below Application Session Are Still Connected.Please ask Team Install To bring Them Down***"
 cat $PAR_HOME/session_${ORACLE_SID}_temp.txt
 rm -f $PAR_HOME/session_${ORACLE_SID}_temp.txt
 exit 3
fi

$ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 prompt Creating PFILE from SPFILE...
 create pfile from spfile;
 prompt Creating delete_database_files.sh to Delete the Database...
 set pagesize 30000 linesize 132 heading off feedback off
 spool $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
 select 'rm ' || name from v\$datafile
 UNION
 select 'rm ' || name from v\$controlfile
 UNION
 select 'rm ' || name from v\$tempfile
 UNION
 select 'rm ' || member name from v\$logfile;
 spool off
EOF
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is a problem while creating delete_database_files.sh...Please check\n\nAborting Here...."
 "\n\n\t\t*************************************** FAILURE *************************************"
 exit 4
fi
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh ]]
then
 chmod 755 $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
else
 "\n\n\t\tdelete_database_files.sh not created.....Please check"
 "\n\n\t\t*************************************** FAILURE *************************************"
fi
 $ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF>$REF_HOME/${ORACLE_SID}_scripts/delete_location.txt
 WHENEVER SQLERROR EXIT FAILURE
 set pagesize 30000 linesize 132 heading off feedback off
 select VALUE from v\$parameter where NAME in('background_dump_dest','core_dump_dest','user_dump_dest','audit_file_dest','log_archive_dest_1');
EOF
 print "Bringing Down the Database"
 $ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF
 shutdown abort
EOF
 ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
 if [ $? -eq 0 ]
 then
  print "\n\n\t\tThere is some Problem while shutting down the database before deleting the database files...Please check\n\nAborting Here...."
  print "\n\n\t\t*************************************** FAILURE *************************************"
  exit 7
 fi
 print "Deleting the Database files from OS Level"
 $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
 print "Deleting the DUMP_DEST and Archive Log Files from OS Level"
 cat $REF_HOME/${ORACLE_SID}_scripts/delete_location.txt | sed -e '/^$/d' | sed 's/LOCATION=//g' > $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt
 rm -f $REF_HOME/${ORACLE_SID}_scripts/delete_location.txt
 if [[ -s $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt ]]
 then
  cat $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt | while read LINE
  do
   rm -fr $LINE
   if [ $? -ne 0 ]
   then
    print "\nProblem while removing $LINE ... Please Remove $LINE Manually..."
    print "Please Delete and Create (adump, bdump, udump, cdump, etc.) the administrator directories under $HOME/admin/xxxxxx manually"
   fi
   mkdir -p $LINE
   if [ $? -ne 0 ]
   then
    print "\nProblem while Creating $LINE ... Please Create $LINE Manually..."
    print "Please Delete and Create (adump, bdump, udump, cdump, etc.) the administrator directories under $HOME/admin/xxxxxx manually"
   fi
  done
 fi

 exit 0
