#!/bin/ksh
# ==================================================================================================
# NAME:		UpdateDeleteSourceData.sh                                 
# AUTHOR:	Basit Khan                                    
# PURPOSE:	This script will update/delete the source data in the target database after the refreshing target the database
# USAGE:	UpdateDeleteSourceData.sh <ORACLE_SID> [SQL Script for update/deletes]
#
# ==================================================================================================
if [ $# -lt 1 ]
then
 print "\nMust enter the Oracle SID of the database for which you want to perform the Update Delete step."
 print "Usage: $0 <Oracle SID>\n"
 print "There is a second optional parameter for the SQL script that contains the truncate/delete statements."
 print "If no second parameter is used, the SQL script defaults to UpdateDeleteSourceData.sql"
 print "If the second parameter is included, it will use that script instead of the default script."
 print "Usage: $0 <Oracle SID> [SQL Script for update/deletes]\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

sql_script=$2

if [[ -z $sql_script ]]
then
 sql_script=$REF_HOME/UpdateDeleteSourceData.sql
else
 if [[ ! -f $REF_HOME/$sql_script || ! -s $REF_HOME/$sql_script ]]
 then
  print "SQL Script $REF_HOME/$sql_script does not exist or is empty in $REF_HOME"
  exit 3
 fi
 sql_script=$REF_HOME/$sql_script
fi

print "Truncating SYS.AUD$ Table at: $(date)"
$ORACLE_HOME/bin/sqlplus -s  '/ as sysdba'<<EOF
spool $REF_HOME/${ORACLE_SID}_scripts/trunc_audit_table.out
WHENEVER SQLERROR EXIT FAILURE
truncate table SYS.AUD$;
EOF
if [ $? -ne 0 ]
then
 print "\n\n\n***************** There was an error on truncation of SYS.AUD$ Table ***********************\n\n\n"
 print "\n\nSpool File trunc_audit_table.out generated under $REF_HOME/${ORACLE_SID}_scripts\n\n"
 print "You can truncate that table manually when this process is complete."
fi

print "Using SQL Script for truncates: $sql_script at $(date)"
$ORACLE_HOME/bin/sqlplus -s  '/ as sysdba'<<EOF
set pagesize 0
WHENEVER SQLERROR EXIT FAILURE
spool $REF_HOME/${ORACLE_SID}_scripts/trunc_tables.out
@$sql_script
EOF
if [ $? -ne 0 ]
then
 print "\n\n\n***************** There was an error on truncation of tables using $sql_script ***********************\n\n\n"
 print "\n\nSpool File trunc_tables.out generated under $REF_HOME/${ORACLE_SID}_scripts\n\n"
 exit 4
fi

exit 0
