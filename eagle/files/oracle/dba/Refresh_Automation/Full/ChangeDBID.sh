#!/bin/ksh
# ==================================================================================================
# NAME:		ChangeDBID.sh                            
# USAGE:	ChangeDBID.sh  <ORACLE_SID>
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will perform the below steps in sequence:
#				1. shutdown immediate
#				2. startup mount pfile='$ORACLE_HOME/dbs/init{Target SID}.ora'
#				3. Change DBID
#				4. If 9i database and not completly shutdown then shutdown the database
#				5. Change db_name and Instance Name in init.or file after DBID change
#				6. startup mount pfile='$ORACLE_HOME/dbs/init{Target SID}.ora'
#				7. add supplemental  log data if it is implementation database 
#				8. open the database in resetlogs mode
#				9. change global_name to {Target db_name}
#				10.create spfile from pfile
#				11.shutdown immediate
#				12.startup
#	
# ==================================================================================================

function shutdown_database
{
ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 print "***Shutting down database***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate;
EOF
if [ $? -ne 0 ]
then
 print "Error on shut down of $ORACLE_SID  database."
 print "Aborting here."
 exit 1
fi
else
 print "No database is running with $ORACLE_SID"
 exit 2
fi
}


function startup_mount
{
print "***Starting database in mount phase***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup mount pfile='${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora';
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of $ORACLE_SID database."
 print "Aborting here."
 exit 3
fi
}


function change_dbid
{
print "***DATABASE DBID will get change here***"
$ORACLE_HOME/bin/nid target=sys/eagle dbname=$ORACLE_SID<<EOF
Y
EOF
if [ $? -ne 0 ]
then
 print "Error for Changing the DBID...Please check."
 print "Aborting here."
 exit 4
fi
}


#################### MAIN ##########################
if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID>\n"
 print "\t\t<ORACLE_SID> is the target database on this host which is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

shutdown_database

startup_mount

change_dbid

ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 shutdown_database
fi

grep -vi db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
grep -vi instance_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
grep -vi db_unique_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
print "*.db_name='${ORACLE_SID}'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
print "*.instance_name='${ORACLE_SID}'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp

print "***Creating Password file...***"
mv $ORACLE_HOME/dbs/orapw${ORACLE_SID} $ORACLE_HOME/dbs/orapw${ORACLE_SID}_$(date +'%Y%m%d%H%M')_bkp
$ORACLE_HOME/bin/orapwd file=$ORACLE_HOME/dbs/orapw${ORACLE_SID} password=eagle entries=20

startup_mount

print "***Adding supplemental log...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database add supplemental log data;
alter database add supplemental log data(primary key) columns;
EOF
if [ $? -ne 0 ]
then
 print "\nError while adding supplemental log...Please check.\n"
fi

print "***Opening database in OPEN RESETLOGS MODE***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database open resetlogs;
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while opening the database in OPEN RESETLOGS...Please check.\n"
 print "Aborting here."
 exit 6
fi

print "***Changing Global Name...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database rename global_name to ${ORACLE_SID};
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while changing global name...."
fi

print "***Creating SPFILE...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
create spfile from pfile;
EOF
if [ $? -ne 0 ]
then
 print "Error on creating spfile for standby database."
 print "Aborting here."
 exit 8
fi

shutdown_database

print "***Starting Up the database***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while starting up the database...."
 print "Aborting here."
 exit 9
fi

exit 0
