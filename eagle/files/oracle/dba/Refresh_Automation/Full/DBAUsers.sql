drop role EA_DBA_ROLE;
create role EA_DBA_ROLE;
grant DBA to EA_DBA_ROLE;

drop user achowdhary cascade;

drop user rmadhavi cascade;

drop user jalcordo cascade;

drop user bkhan cascade;

drop user dambekar cascade;

drop user nsarri cascade;

CREATE USER nsarri
PROFILE EA_PROFILE
IDENTIFIED BY "nsarri_55LF2ga8" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO nsarri;
audit all by nsarri by access;
audit update table, insert table, delete table,execute procedure by nsarri by access;

drop user svyahalkar cascade;

CREATE USER svyahalkar
PROFILE EA_PROFILE
IDENTIFIED BY "svyahalkar_YobaI4hq" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO svyahalkar;
audit all by svyahalkar by access;
audit update table, insert table, delete table,execute procedure by svyahalkar by access;

drop user pkrishnan cascade;

CREATE USER pkrishnan
PROFILE EA_PROFILE
IDENTIFIED BY "pkrishnan_q730PwUw" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO pkrishnan;
audit all by pkrishnan by access;
audit update table, insert table, delete table,execute procedure by pkrishnan by access;


drop user mbuotte cascade;

CREATE USER mbuotte
PROFILE EA_PROFILE
IDENTIFIED BY "mbuotte_QNsnyl1t" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO mbuotte;
audit all by mbuotte by access;
audit update table, insert table, delete table,execute procedure by mbuotte by access;


drop user fdavis cascade;

CREATE USER fdavis
PROFILE EA_PROFILE
IDENTIFIED BY "fdavis_4qFLPeUd" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO fdavis;
audit all by fdavis by access;
audit update table, insert table, delete table,execute procedure by fdavis by access;

drop user nkulkarni cascade;

CREATE USER nkulkarni
PROFILE EA_PROFILE
IDENTIFIED BY "nkulkarni_2nFnoPq" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO nkulkarni;
audit all by nkulkarni by access;
audit update table, insert table, delete table,execute procedure by nkulkarni by access;


drop user jpawar cascade;

CREATE USER jpawar
PROFILE EA_PROFILE
IDENTIFIED BY "jpawar_G43Q02x1" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO jpawar;
audit all by jpawar by access;
audit update table, insert table, delete table,execute procedure by jpawar by access;

drop user LWhitmore cascade;

CREATE USER LWhitmore
PROFILE EA_PROFILE
IDENTIFIED BY "LWhitmore_G44Ru2x1" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO LWhitmore;
audit all by LWhitmore by access;
audit update table, insert table, delete table,execute procedure by LWhitmore by access;


drop user hdeshmukh cascade;

CREATE USER hdeshmukh
PROFILE EA_PROFILE
IDENTIFIED BY "hdeshmukh_G44Ru2x1" PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_DBA_ROLE TO hdeshmukh;
audit all by hdeshmukh by access;
audit update table, insert table, delete table,execute procedure by hdeshmukh by access;
