#!/bin/ksh
#==================================================================================================
# NAME:		RestoreDatabase.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will:
#			1. Startup the database in nomount
#			2. Restore the Control file
#           3. Restore the Datafiles and Temp files
#			4. Disable the Flashback
#			5. Open the database in OPEN RESETLOGS mode
#
# USAGE:	RestoreDatabase.sh  <ORACLE_SID> <Log File> [Upgrade 11.2.0.1 to 11.2.0.3]
#           The third parameter is optional.  The values are:
#           0 = Do not upgrade
#           1 = Upgrade
#
#
#==================================================================================================
function shutdown_if_necessary
{
ps -ef | grep ora_smon_$ORACLE_SID | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 shutdown abort
EOF
fi 
}
function startdb_nomount
{
 print "Please Monitor RMAN log file $REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log ..." >> $log_file
 print "Connecting to RMAN to startup the database in nomount..." >> $log_file
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log<<EOF>>/dev/null
 startup nomount pfile='$ORACLE_HOME/dbs/init${ORACLE_SID}.ora';
EOF
 if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Starting the database in nomount...Please check...\nAborting Here..." >> $log_file
  print "\n\n***************FAILURE***************\n\n" >> $log_file
  exit 9
 fi
}
function restore_ctrlfile
{
 print "Connecting to RMAN to Restore the Control File" >> $log_file
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$log_file APPEND<<EOF>>/dev/null
 @$ctrlfile
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Restoring the Controlfile...Please check...\nAborting Here..." >> $log_file
 print "\n\n***************FAILURE***************\n\n" >> $log_file
 exit 10
fi
}
function check_flashback
{
 print "Turn Flashback Off" >> $log_file
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 ALTER DATABASE FLASHBACK OFF;
EOF
 if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Disabling the FLASHBACK...Please check...\nAborting Here..." >> $log_file
 fi
}
function rename_redofiles
{
 print "Connecting to Database to Rename the Redo Files..." >> $log_file
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 @$redofile
EOF
if [ $? -ne 0 ]
then
 grep 'ORA-01523' $log_file > /dev/null
 if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Renaming the Redo Files...Please check...\nAborting Here..." >> $log_file
  exit 11
 fi
fi
}
function restore_datafiles
{
 print "Connecting to RMAN to Restore the DataFiles...Please Wait until the Restoration completes" >> $log_file
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$log_file APPEND<<EOF
 @$datatempfile
EOF
}
#################### MAIN ##########################
if [ $# -lt 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID> <Log File> [0|1]\n"
 print "\t\t<ORACLE_SID> is the target database on this host which is being refreshed\n"
 print "\t\t<Log File> is for the main log for checking output\n"
 print "\t\t[0|1] 0=Do not upgrade from 11.2.0.1 to 11.2.0.3  1=Upgrade from 11.2.0.1 to 11.2.0.3  Optional, Default=0\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n" >> $log_file
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

log_file=$2

upgrade=$3
if [[ -z $upgrade ]]
then
 upgrade=0
fi
if [[ ! $upgrade = 0 ]] && [[ ! $upgrade = 1 ]]
then
 print "If the optional third parameter is entered, it must be a 0 or 1."
 print "0 = Do not upgrade from 11.2.0.1 to 11.2.0.3."
 print "1 = Upgrade from 11.2.0.1 to 11.2.0.3."
 exit 3
fi 

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export cdate=$(date +'%Y%m%d')

prod_sid=$(grep -i db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F\' '{print $2}')
print "Below Restore Control file will be used for this Refresh" >> $log_file
if [[ -f $REF_HOME/${ORACLE_SID}_scripts/${prod_sid}_specific_cf.rcv ]]
then
 ctrlfile=$REF_HOME/${ORACLE_SID}_scripts/${prod_sid}_specific_cf.rcv
else
 print "The controlfile $ctrlfile to create $ORACLE_SID does not exist." >> $log_file
 print "Cannot continue.  Aborting..." >> $log_file
 exit 4
fi

print $ctrlfile >> $log_file

datatempfile=$REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${ORACLE_SID}.rcv
redofile=$REF_HOME/${ORACLE_SID}_scripts/run_rename_redo.rcv

shutdown_if_necessary
startdb_nomount
restore_ctrlfile
check_flashback
restore_datafiles
rename_redofiles
 
print "Checking if any files are in RECOVER or OFFLINE Mode." >> $log_file
${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
WHENEVER SQLERROR EXIT FAILURE
set feedback off heading off
select file#,name,status from v\$datafile where status in('RECOVER','OFFLINE');
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Checking the Datafiles in RECOVER or OFFLINE Mode...Please check...\nAborting Here..." >> $log_file
exit 5
fi
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt ]]
then
 print "\n\nThere are some file in RECOVER or OFFLINE Mode\n" >> $log_file
 cat $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
 rm -f $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
 exit 6
fi

print "Disable Block Change Tracking" >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_file
ALTER DATABASE DISABLE BLOCK CHANGE TRACKING;
EOF

if [ $upgrade -eq 0 ]
then
print "Opening the Database in OPEN RESETLOGS Mode" >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database open resetlogs;
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Opening the Database in OPEN RESETLOGS Mode...Please check...\nAborting Here..." >> $log_file
 exit 7
fi
else
##### Shut down database
print "Shut down database." >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_file
shutdown immediate
EOF

##### Start database in mount state
print "Start database in mount state" >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_file
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while starting database in mount state after refresh ...\nAborting Here..." >> $log_file
 exit 8
fi

##### Add upgrade to resetlogs
print "Opening the Database in OPEN RESETLOGS Mode with Upgrade Option" >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_file
WHENEVER SQLERROR EXIT FAILURE
alter database open resetlogs upgrade;
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Opening the Database in OPEN RESETLOGS Mode with Upgrade Option...Please check...\nAborting Here..." >> $log_file
 exit 9
fi

##### Upgrade to 11.2.0.3.  This shuts down the database after upgrade.
print "Upgrade database to 11.2.0.3" >> $log_file
print "See Log File: $REF_HOME/${ORACLE_SID}_scripts/upgrade_${ORACLE_SID}.log" >> $log_file
print "This step will take from 10 to 30 minutes to complete." >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo on
spool $REF_HOME/${ORACLE_SID}_scripts/upgrade_${ORACLE_SID}.log
@$ORACLE_HOME/rdbms/admin/catupgrd.sql
EOF
print "\nThe following lines that contain ORA- were found in the upgrade log:\n" >> $log_file
grep 'ORA-' $REF_HOME/${ORACLE_SID}_scripts/upgrade_${ORACLE_SID}.log >> $log_file
print "\nEvaluate the above lines to determine if any are significant:\n" >> $log_file

##### Start database after upgrade
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_file
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while starting database after upgrading...Please check...\nAborting Here..." >> $log_file
 exit 10
fi

##### Run post-upgrade scripts
print "Post-upgrade scripts." >> $log_file
print "See Log File: $REF_HOME/${ORACLE_SID}_scripts/post_upgrade_${ORACLE_SID}.log" >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo on
spool $REF_HOME/${ORACLE_SID}_scripts/post_upgrade_${ORACLE_SID}.log
@$ORACLE_HOME/rdbms/admin/utlu112s.sql
@$ORACLE_HOME/rdbms/admin/catuppst.sql
@$ORACLE_HOME/rdbms/admin/utlrp.sql
EOF
print "Check log file $REF_HOME/${ORACLE_SID}_scripts/post_upgrade_${ORACLE_SID}.log for errors." >> $log_file
fi

exit 0
