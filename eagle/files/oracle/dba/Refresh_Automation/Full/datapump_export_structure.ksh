#!/bin/ksh
#######################################################################################
#
# Script  : datapump_export_structure.ksh <ORACLE_SID>
# Purpose : To export structure(DDL) from an Oracle database using expdp utility.
#
#######################################################################################

if [ $# -lt 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!"
 print "\t\tUsage : $0 <ORACLE_SID>${BOFF}"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $(dirname $0) = "." ]
then
 SCRIPT_DIR=$(pwd)
else
 SCRIPT_DIR=$(dirname $0)
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
 
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export PATH=$PATH:${ORACLE_HOME}/bin
mailrecipients=team_dba@eagleaccess.com
mailflag=FALSE
now=$(date +'%Y%m%d_%H%M%S')

LOGDIR=$SCRIPT_DIR/${ORACLE_SID}_scripts

FILENAME=export_${ORACLE_SID}_${now}.dmpdp
FILESIZE=30G   # Set FILESIZE=integer[B | K | M | G]
LOGFILE=export_datapump_structure_${ORACLE_SID}_${now}.log
IMPORT_LOGFILE=import_datapump_structure_${ORACLE_SID}_${now}.log

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE ;
set echo off feed off pages 0 trim on trims on
spool $LOGDIR/nls_lang_val.lst
SELECT lang.value || '_' || terr.value || '.' || chrset.value
FROM v\$nls_parameters lang,v\$nls_parameters terr,v\$nls_parameters chrset
WHERE lang.parameter='NLS_LANGUAGE' AND terr.parameter='NLS_TERRITORY' AND chrset.parameter='NLS_CHARACTERSET';
EOF
if [ $? -ne 0 ]
then
 print "ERROR: When getting NLS Parameter from database."
else
 export NLS_LANG=$(cat $LOGDIR/nls_lang_val.lst)
 rm -f $LOGDIR/nls_lang_val.lst
fi

print "NLS_LANG is $NLS_LANG"

datapump_dump_directory=/datadomain/export/$ORACLE_SID
mkdir -p $datapump_dump_directory
if [ $? -ne 0 ]
then
 print "Cannot create directory $datapump_dump_directory on this host."
 exit 3
fi
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set head off feed off lines 150 trims on pages 0 sqlp ""
drop directory DATA_PUMP_DIR;
WHENEVER SQLERROR EXIT FAILURE
create directory DATA_PUMP_DIR as '$datapump_dump_directory';
EOF
if [ $? -ne 0 ]
then
 print "Cannot create DATA_PUMP_DIR on $ORACLE_SID as $datapump_dump_directory"
 exit 4
fi

print "DATA_PUMP_DIR = $datapump_dump_directory"

$ORACLE_HOME/bin/expdp \"/ as sysdba\" DUMPFILE=$FILENAME FILESIZE=$FILESIZE LOGFILE=$LOGFILE FULL=Y CONTENT=METADATA_ONLY JOB_NAME=EXPDP_${now}

grep "^Job .*EXPDP_${now}.* successfully completed" $datapump_dump_directory/$LOGFILE > /dev/null
if [ $? -ne 0 ]
then
 print "Datapump export did not complete successfully for ${ORACLE_SID}!"
 print "Please check the logfile $datapump_dump_directory/$LOGFILE for more information."
else
 print "Datapump export of $ORACLE_SID structure completed succesfully."
fi

SQLFILE=ddl_structure_${ORACLE_SID}_${now}.log
$ORACLE_HOME/bin/impdp \"/ as sysdba\" DUMPFILE=$FILENAME SQLFILE=$SQLFILE FULL=Y LOGFILE=$IMPORT_LOGFILE
print "See $datapump_dump_directory/$SQLFILE for $ORACLE_SID Metadata"

find /datadomain/export -name "export_datapump_structure_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].log" -type f -mtime +62 -exec rm -f {} \;
find /datadomain/export -name "export_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].dmpdp" -type f -mtime +62 -exec rm -f {} \;
find /datadomain/export -name "import_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].log" -type f -mtime +62 -exec rm -f {} \;
find /datadomain/export -name "ddl_structure_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].log" -type f -mtime +62 -exec rm -f {} \;

exit 0
