#!/bin/ksh
# ==================================================================================================
# NAME:		DoNotChangeDBID.sh                            
#
# USAGE:	DoNotChangeDBID.sh  <ORACLE_SID>
# 
# PURPOSE:	This script will perform the below steps in sequence:
#				1. create password file
#				2. add supplemental log data
#				3. create spfile from pfile
#				4. shut down database
#				5. start up database
#
# ==================================================================================================

function shutdown_database
{
ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 print "***Shutting down database***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate;
EOF
if [ $? -ne 0 ]
then
 print "Error on shut down of $ORACLE_SID  database."
 print "Aborting here."
 exit 1
fi
else
 print "No database is running with $ORACLE_SID"
 exit 2
fi
}

function startup_mount
{
print "***Starting database in mount phase***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup mount pfile='${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora';
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of $ORACLE_SID database."
 print "Aborting here."
 exit 3
fi
}

#################### MAIN ##########################
if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

print "***Creating Password file...***"
mv $ORACLE_HOME/dbs/orapw${ORACLE_SID} $ORACLE_HOME/dbs/orapw${ORACLE_SID}_$(date +'%Y%m%d%H%M')_bkp
$ORACLE_HOME/bin/orapwd file=$ORACLE_HOME/dbs/orapw${ORACLE_SID} password=eagle entries=20

print "***Adding supplemental log...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database add supplemental log data;
alter database add supplemental log data(primary key) columns;
EOF
if [ $? -ne 0 ]
then
 print "\nError while adding supplemental log...Please check.\n"
fi

print "***Creating SPFILE...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
create spfile from pfile;
EOF
if [ $? -ne 0 ]
then
 print "Error on creating spfile for standby database."
 print "Aborting here."
 exit 4
fi

shutdown_database

print "***Starting Up the database***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while starting up the database...."
 print "Aborting here."
 exit 5
fi

exit 0
