#!/bin/ksh
# ==================================================================================================
# NAME:		pace_star_version.ksh                            
# AUTHOR:	Frank Davis
# PURPOSE:	This script will check the star and pace version for a database
# USAGE:	pace_star_version.ksh <Oracle SID>
# ==================================================================================================
if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Oracle SID>\n"
 print "\t\tYou must enter one parameter into this script: the Oracle SID\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 print "If you want to refresh $1 on this host, make an entry in $ORATAB for it.\n"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

ps -ef | grep "ora_smon_$ORACLE_SID" | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
# PACE of Target
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/pace_version_${ORACLE_SID}.txt
WHENEVER SQLERROR EXIT FAILURE
set pagesize 0 feedback off trimspool on echo off
select version from pace_masterdbo.eagle_product_version 
where instance = (select max(instance) from pace_masterdbo.eagle_product_version 
where  upper(product_type) in  ('EDM', 'PACE') 
and instance not in (select instance from pace_masterdbo.eagle_product_version 
where upper(comments) like '%ADDENDUM%'));
EOF
if [ $? -ne 0 ]
then
 print "There is no PACE installed in ${ORACLE_SID}.  This is not an application database."
 exit 3
fi
pver=$(awk -F. '{print $1}' $REF_HOME/pace_version_${ORACLE_SID}.txt)

# STAR of Target
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$REF_HOME/star_version_${ORACLE_SID}.txt
set pagesize 0 feedback off trimspool on echo off
select version from pace_masterdbo.eagle_product_version 
where instance = (select max(instance)from pace_masterdbo.eagle_product_version 
where  upper(product_type) in  ('EDM', 'STAR-DB') 
and instance not in (select instance from pace_masterdbo.eagle_product_version 
where upper(comments) like '%ADDENDUM%'));
EOF
sver=$(awk -F. '{print $1}' $REF_HOME/star_version_${ORACLE_SID}.txt)
fi

rm -f $REF_HOME/pace_version_${ORACLE_SID}.txt $REF_HOME/star_version_${ORACLE_SID}.txt

if [ $pver -ne $sver ]
then
 print "PACE and STAR Versions do not match.  PACE is Version $pver and STAR is Version ${sver}."
 exit 4
fi

print "$pver"

exit 0
