#!/bin/ksh
# Script Name: preserve_passwords_ea_service.ksh
# Usage: preserve_passwords_ea_service.ksh [Oracle SID]

if [ $# -ne 1 ]
then
 print "\nMust enter one parameter into this script."
 print "The parameter must be the Oracle SID.\n"
 print "$0 <Oracle SID>"
 exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)

# Get the Oracle Home where the current listener is running.
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
log_dir=$script_dir/${ORACLE_SID}_scripts

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

(( db_version=$(print $ORACLE_HOME | awk -F/ '{print $6}' | awk -F. '{print $1}') ))

user_list=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 0 feedback off
select username from dba_users where profile='EA_SERVICE';
EOF
`

rm -f $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql
if [ $db_version -gt 10 ]
then

for the_user in $user_list
do
password_version=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set heading off
select password_versions from dba_users where username='$the_user';
EOF
`
password_version=$(print $password_version | tr -d " ")

case $password_version in
10G11G)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo off linesize 200 trimspool on pagesize 0 sqlp "" verify off trimout on
spool $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql APPEND
select 'alter user $the_user identified by values '||''''||SPARE4||';'||PASSWORD||''''||';' from user$ where name='$the_user';
EOF
;;  
10G)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo off linesize 200 trimspool on pagesize 0 sqlp "" verify off trimout on
spool $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql APPEND
select 'alter user $the_user identified by values '||''''||PASSWORD||''''||';' from user$ where name='$the_user';
EOF
;;
11G)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo off linesize 200 trimspool on pagesize 0 sqlp "" verify off trimout on
spool $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql APPEND
select 'alter user $the_user identified by values '||''''||SPARE4||''''||';' from user$ where name='$the_user';
EOF
;;
esac
done

else

for the_user in $user_list
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set echo off heading off linesize 200 trimspool on sqlp "" verify off trimout on
spool $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql APPEND
select 'alter user $the_user identified by values '||''''||PASSWORD||''''||';' from dba_users where username='$the_user';
EOF
done

fi

sed '/^$/d' $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql > $log_dir/extra_preserve_ea_service_pwd_${ORACLE_SID}.sql
mv $log_dir/extra_preserve_ea_service_pwd_${ORACLE_SID}.sql $log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql

exit 0
