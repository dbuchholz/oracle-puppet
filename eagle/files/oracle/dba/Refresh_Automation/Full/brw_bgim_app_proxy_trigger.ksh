#!/bin/ksh
# Script name: brw_bgim_app_proxy_trigger.ksh
# Usage: brw_bgim_app_proxy_trigger.ksh <Oracle SID>
#
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 1
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full


$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
set feedback off heading off echo off pagesize 0
spool $REF_HOME/${ORACLE_SID}_scripts/brw_bgim_app_proxy_trigger_${ORACLE_SID}.log
create or replace trigger SYS.BRW_LOGON_BGIM_APP_RW before ddl on BGIM_APP_RW.schema
declare
    l_proxy varchar2(1000);
      
    begin
       SELECT NVL(trim(sys_context('USERENV', 'PROXY_USER')),'x') into l_proxy from dual;
       
IF trim(l_proxy) = 'x' THEN
         --dbms_output.put_line('PROXY USER is:'||l_proxy);
        raise_application_error(-20002,'insufficient privileges');
END IF;

IF trim(l_proxy) != 'JBILOTTI' AND trim(l_proxy) != 'RKLINE' AND trim(l_proxy) != 'CHILKER'  THEN
         --dbms_output.put_line('PROXY USER is:'||l_proxy);
        raise_application_error(-20002,'insufficient privileges');
END IF;
end;
/
--LOGIN AS BGIM_APP_RO(TEST):
--------------------
create or replace trigger SYS.BRW_LOGON_BGIM_APP_RO before ddl on BGIM_APP_RO.schema
declare
    l_proxy varchar2(1000);
      
    begin
       SELECT NVL(trim(sys_context('USERENV', 'PROXY_USER')),'x') into l_proxy from dual;
       
IF trim(l_proxy) = 'x' THEN
         --dbms_output.put_line('PROXY USER is:'||l_proxy);
        raise_application_error(-20002,'insufficient privileges');
END IF;

IF trim(l_proxy) != 'JBILOTTI' AND trim(l_proxy) != 'RKLINE' AND trim(l_proxy) != 'CHILKER'  THEN
         --dbms_output.put_line('PROXY USER is:'||l_proxy);
        raise_application_error(-20002,'insufficient privileges');
END IF;
end;
/
EOF

exit 0
