#!/bin/ksh
# ==================================================================================================
# NAME:		ImpCustArchRuleWebConfig.sh                            
# PURPOSE:	This script will import the data for custom archive rule and web configuration
#			For MFC and TER it will export all the pace application users
# USAGE:    ImpCustArchRuleWebConfig.sh  <Oracle SID> <Application Version>
#           Application Version is the Major Number before the first period of the full version.
#           For example, in 11.1.0.15, the <Application Version> is 11
# ==================================================================================================
funct_get_pass()
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 select instance from inf_monitor.databases where sid='$ORACLE_SID' and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the password for pace"
 print "Aborting Here...."
 exit 11
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 passwd=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 select inf_monitor.bb_get(nvl(temp_code,code)) from inf_monitor.user_codes where db_instance=$DbIns and username='$appuser';
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
  print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 12
 fi
fi
passwd=$(print $passwd|tr -d " ")
}
funct_get_environment()
{
 ${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF>$REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 select sid from inf_monitor.databases where
 (sid like 'mfc%' or sid like 'ter%')
 and dataguard='N' and sid not like '%prd%';
EOF
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem while determining if database is among the databases that will preserve the user manager Tables."
  print "Aborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  exit 13
 fi
 cat $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt | sed -e '/^$/d' > $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt
 rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
}

#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID> <Application Version>\n"
 print "\t\t<ORACLE_SID> should be of target database on this host which is being refreshed\n"
 print "\t\t<Application Version> is the major version number of the PACE/STAR application such as 10,11,12,etc.\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 2
fi

app_version=$2
app_version=$(print $app_version | awk -F. '{print $1}')

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -f $PARFILE ]]
then
 STATUS=$(sed '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
 fi
fi
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

####### PACE_MASTERDBO requirement ######
export appuser=PACE_MASTERDBO
funct_get_pass
export pace_pass=$passwd
export pace_user=$appuser
####### ESTAR requirement ###############
export appuser=ESTAR
funct_get_pass
export star_pass=$passwd
export star_user=$appuser
####### BRW DATAEXCHDBO requirement ####
export appuser=DATAEXCHDBO
funct_get_pass
export dxch_pass=$passwd
export dxch_user=$appuser
####### MSGCENTER_DBO requirement #######
export appuser=MSGCENTER_DBO
funct_get_pass
export msg_pass=$passwd
export msg_user=$appuser
####### EAGLEMGR requirement ############
export appuser=EAGLEMGR
funct_get_pass
export emgr_pass=$passwd
export emgr_user=$appuser
######################################### 
funct_get_environment
export exst_sid=$(cat $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt | grep $ORACLE_SID)
if [[ -n $exst_sid ]]
then 
 print "Truncating the Custom Archives Tables..."
 ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF
 set heading off feedback off
 WHENEVER SQLERROR EXIT FAILURE
 truncate table pace_masterdbo.pace_users;                        
 truncate table pace_masterdbo.pace_user_groups;                  
 truncate table pace_masterdbo.pace_user_role_details;            
 truncate table pace_masterdbo.starsec_properties;                
 truncate table pace_masterdbo.starsec_group_detail;              
 truncate table pace_masterdbo.starsec_group_rel;                 
 truncate table pace_masterdbo.starsec_group_sum;                 
 truncate table pace_masterdbo.starsec_user_detail;               
 truncate table pace_masterdbo.starsec_user_group;                
 truncate table pace_masterdbo.user_client_maintenance;           
 truncate table pace_masterdbo.user_entity_maintenance;
EOF
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Truncating Tables.  Please Rectify and Execute Again\nAborting Here....\n"
  print "\n\n***************************************FAILURE*********************************************\n\n"
  exit 3
 fi
 print "Importing the PACE User Manager Tables..."
 $ORACLE_HOME/bin/imp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.log fromuser=$pace_user touser=$pace_user tables=pace_users,pace_user_groups,pace_user_role_details,starsec_properties,starsec_group_detail,starsec_group_rel,starsec_group_sum,starsec_user_detail,starsec_user_group,user_client_maintenance,user_entity_maintenance ignore=y
 grep 'Import terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Importing PACE User Manager Tables...Please check\n\nAborting Here...."
  print "\n\n\t\tPlease check log $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.log\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  exit 4
 fi
fi

print "Truncating Tables..."
 ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 truncate table pace_masterdbo.pace_system;
 truncate table pace_masterdbo.custom_archive_rules;
 truncate table estar.web_configuration;
 truncate table msgcenter_dbo.msg_streams;
EOF
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem truncating Tables\nAborting Here....\n"
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 5
fi

print "Importing the Pace System and Custom Archives Tables..."
$ORACLE_HOME/bin/imp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pacetables_imp.log fromuser=$pace_user touser=$pace_user tables=pace_system,custom_archive_rules ignore=y
grep "Import terminated successfully without warnings\." $REF_HOME/${ORACLE_SID}_scripts/pacetables_imp.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Importing Pace System and Custom Archive Tables...Please check\n\nAborting Here...."
 print "\n\n\t\tPlease check log $REF_HOME/${ORACLE_SID}_scripts/pacetables_imp.log\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 6
fi

print "Importing the Web Configuration Table..."
$ORACLE_HOME/bin/imp $star_user/$star_pass file=$REF_HOME/${ORACLE_SID}_scripts/estar_web_conf.dmp log=$REF_HOME/${ORACLE_SID}_scripts/imp_estar_webconf.log fromuser=$star_user touser=$star_user tables=web_configuration ignore=y
grep "Import terminated successfully without warnings\." $REF_HOME/${ORACLE_SID}_scripts/imp_estar_webconf.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Importing Web Configuration Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 print "Check the log $REF_HOME/${ORACLE_SID}_scripts/imp_estar_webconf.log for more info."
 exit 7
fi

print "Importing the Message Streams Table..."
$ORACLE_HOME/bin/imp $msg_user/$msg_pass file=$REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.dmp log=$REF_HOME/${ORACLE_SID}_scripts/imp_msgcenter_dbo_msg_streams.log fromuser=$msg_user touser=$msg_user tables=msg_streams ignore=y
grep "Import terminated successfully without warnings\." $REF_HOME/${ORACLE_SID}_scripts/imp_msgcenter_dbo_msg_streams.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Importing Message Streams Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 print "Check the log $REF_HOME/${ORACLE_SID}_scripts/imp_msgcenter_dbo_msg_streams.log for more info."
 exit 8
fi

if [ $app_version -ge 11 ]
then
 print "Truncating Tables for EAGLEMGR EAGLE_SERVICES and EAGLE_SERVICE_PROPERTIES..."
 ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 truncate table eaglemgr.eagle_services;
 truncate table eaglemgr.eagle_service_properties;
EOF
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem truncating EAGLEMGR EAGLE_SERVICES and EAGLE_SERVICE_PROPERTIES Tables"
 print "\nAborting Here....\n"
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 9
fi
 print "Importing the Eagle Services Table..."
 $ORACLE_HOME/bin/imp $emgr_user/$emgr_pass file=$REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.dmp log=$REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log fromuser=$emgr_user touser=$emgr_user tables=eagle_services,eagle_service_properties ignore=y
 grep "Import terminated successfully without warnings\." $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Importing Eagle Services Table...Please check\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  print "Check the log $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log for more info."
  exit 10
 fi
fi

################################Added for BrandyWine Dataexchange requirement ####################
export brwsid=$(print $ORACLE_SID | cut -c1-6)
if [ $brwsid == 'brwdev' -o $brwsid == 'brwtst' ]
then
 print "Truncating Dataexchange table as this is Brandywine Refresh ..."
 ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set heading off feedback off
 truncate table dataexchdbo.acct_info;
EOF
 $ORACLE_HOME/bin/imp $dxch_user/$dxch_pass  file=$REF_HOME/${ORACLE_SID}_scripts/acct_info.dmp log=$REF_HOME/${ORACLE_SID}_scripts/acct_info_imp.log fromuser=dataexchdbo touser=dataexchdbo tables=acct_info ignore=y
 grep "Import terminated successfully without warnings\." $REF_HOME/${ORACLE_SID}_scripts/acct_info_imp.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Importing DATAEXCHANGE  Configuration Table...Please check\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  print "Check the log $REF_HOME/${ORACLE_SID}_scripts/acct_info_imp.log for more info."
  exit 10
 fi
fi
#################################################################################################
print "Log files are under /u01/app/oracle/local/dba/Refresh_Automation/Full/${ORACLE_SID}_scripts for More Details"
rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt

exit 0
