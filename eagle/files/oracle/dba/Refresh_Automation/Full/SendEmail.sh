#!/usr/bin/ksh
#####################################################################################
# Script name: SendEmail.sh                                                         #
# Usage: SendEmail.sh <Oracle SID>                                                  #
# This requires file teaminstall.lst with 3 parameters on the same line per user:   #
#	1. emailid                                                                  #
#	2. username                                                                 #
#	3. Password                                                                 #
# You can append a line to send email to any user.                                  #
# Run setsid for which you want to include the SID for                              #
#####################################################################################
ostype=$(uname)
if [ $ostype = Linux ]
then
 MAIL=/bin/mailx
fi
if [ $ostype = SunOS ]
then
 MAIL=/usr/bin/mailx
fi
script_dir=/u01/app/oracle/local/dba/Refresh_Automation/Full
ORACLE_SID=$1
while read recv uid pwd
do
 print "From: TEAM_DBA" > $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print "To: ${recv}" >> $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print "Subject: Your username password " >> $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print "Message Your username and temporary password on $ORACLE_SID is now " >> $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print " " >> $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print "Username-> ${uid}, Password-> ${pwd}. " >> $script_dir/${ORACLE_SID}_email_to_db_users.dat
 print "You will be prompted to change the password on you first logon. The new password policy is:

			1.	Password length should be greater than or equal to 8 character.
			2.	It should contain :
				a.	At least one letter
				b.	At least one digit
				c.	One punctuation
				d.	Should not contain $ sign.
				e.	Should differ by 3 characters from the previous password.
    Thanks

    Team DBA" >> $script_dir/${ORACLE_SID}_email_to_db_users.dat

 $MAIL -s "Your Username and Password" ${recv} < $script_dir/${ORACLE_SID}_email_to_db_users.dat
 rm $script_dir/${ORACLE_SID}_email_to_db_users.dat
done < $script_dir/teaminstall.lst

exit 0
