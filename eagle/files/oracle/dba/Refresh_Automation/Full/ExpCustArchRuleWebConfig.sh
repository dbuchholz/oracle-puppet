#!/bin/ksh
# ==================================================================================================
# NAME:		ExpCustArchRuleWebConfig.sh                            
# PURPOSE:	This script will export the data for custom archive rule and web configuration
#			For MFC and TER it will export all the pace application users
# USAGE:	ExpCustArchRuleWebConfig.sh  <Oracle SID> <Application Version>
#           Application Version is the Major Number before the first period of the full version.
#           For example, in 11.1.0.15, the <Application Version> is 11
# ==================================================================================================
funct_get_pass()
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from inf_monitor.databases
 where sid='$ORACLE_SID'
 and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the instance for ${ORACLE_SID}\n\n"
 print "Aborting Here...."
 exit 8
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 passwd=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select inf_monitor.bb_get(nvl(temp_code,code)) 
 from inf_monitor.user_codes
 where db_instance=$DbIns
 and username='$appuser';
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
  print "\n\n***************************************FAILURE*********************************************\n\n"
  exit 9
 fi
fi
passwd=$(print $passwd|tr -d " ")
}
funct_get_environment()
{
 ${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF>$REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
 set heading off feedback off
 WHENEVER SQLERROR EXIT FAILURE
 select sid from inf_monitor.databases where
 (sid like 'mfc%' or sid like 'ter%')
 and dataguard='N' and sid not like '%prd%';
EOF
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while determining if database is among the databases that will preserve the user manager Tables."
 print "Aborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 exit 10
fi
cat $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt | sed -e '/^$/d' > $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt
rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list_temp.txt
}

#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID> <Application Version>\n"
 print "\t\t<ORACLE_SID> is the target database on this host which is being refreshed\n"
 print "\t\t<Application Version> is the major version number, such as 10, 11, 12, 13\n"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

app_version=$2
app_version=$(print $app_version | awk -F. '{print $1}')

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
now=$(date +'%Y%m%d_%H%M%S')

if [[ -f $PARFILE ]]
then
 STATUS=$(sed '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep  "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
 fi
fi

print "\nExporting Custom Archive Rule and Web Configuraton tables..."
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

#####PACE_MASTERDBO requirement##############
export appuser=PACE_MASTERDBO
funct_get_pass
export pace_pass=$passwd
export pace_user=$appuser
#####ESTAR requirement#######################
export appuser=ESTAR
funct_get_pass
export star_pass=$passwd
export star_user=$appuser
#####BrandyWine Dataexchange requirement#####
export appuser=DATAEXCHDBO
funct_get_pass
export dxch_pass=$passwd
export dxch_user=$appuser
#####MSGCENTER_DBO requirement###############
export appuser=MSGCENTER_DBO
funct_get_pass
export msg_pass=$passwd
export msg_user=$appuser
#####EAGLEMGR requirement####################
export appuser=EAGLEMGR
funct_get_pass
export emgr_pass=$passwd
export emgr_user=$appuser
############################################# 
funct_get_environment
export exst_sid=$(cat $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt | grep $ORACLE_SID)
rm -f $REF_HOME/${ORACLE_SID}_scripts/sid_list.txt
if [[ -n $exst_sid ]]
then 
 $ORACLE_HOME/bin/exp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab_Exp.log tables=pace_users,pace_user_groups,pace_user_role_details,starsec_properties,starsec_group_detail,starsec_group_rel,starsec_group_sum,starsec_user_detail,starsec_user_group,user_client_maintenance,user_entity_maintenance statistics=none  > /dev/null 2>&1
 grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab_Exp.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Exporting User Manager Tables...Please check\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  print "Check the log $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab_Exp.log for more info."
  exit 2
 fi
 cp $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab.dmp $REF_HOME/${ORACLE_SID}_scripts/pace_user_manager_tab_${now}.dmp
fi

$ORACLE_HOME/bin/exp $star_user/$star_pass file=$REF_HOME/${ORACLE_SID}_scripts/estar_web_conf.dmp log=$REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log tables=web_configuration statistics=none  > /dev/null 2>&1
grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Web Configuration Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 print "Check the log $REF_HOME/${ORACLE_SID}_scripts/estar_webconf.log for more info."
 exit 3
fi
cp $REF_HOME/${ORACLE_SID}_scripts/estar_web_conf.dmp $REF_HOME/${ORACLE_SID}_scripts/estar_web_conf_${now}.dmp 

$ORACLE_HOME/bin/exp $pace_user/$pace_pass file=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.dmp log=$REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.log tables=pace_system,custom_archive_rules statistics=none > /dev/null 2>&1
grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting Custom Archive Rules and Pace System Tables...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 print "Check the log $REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.log for more info."
 exit 4
fi
cp $REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab.dmp $REF_HOME/${ORACLE_SID}_scripts/pace_sys_customArch_users_tab_${now}.dmp

$ORACLE_HOME/bin/exp $msg_user/$msg_pass file=$REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.dmp log=$REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.log tables=msg_streams statistics=none > /dev/null 2>&1
grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is Some Problem while Exporting MSGCENTER_DBO.MSG_STREAMS Table...Please check\n\nAborting Here...."
 print "\n\n***************************************FAILURE*********************************************\n\n"
 print "Check the log $REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.log for more info."
 exit 5
fi
cp $REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams.dmp $REF_HOME/${ORACLE_SID}_scripts/msgcenter_dbo_msg_streams_${now}.dmp

if [ $app_version -ge 11 ]
then
 $ORACLE_HOME/bin/exp $emgr_user/$emgr_pass file=$REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.dmp log=$REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log tables=eagle_services,eagle_service_properties statistics=none > /dev/null 2>&1
 grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Exporting EAGLEMGR.EAGLE_SERVICES Table...Please check\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  print "Check the log $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.log for more info."
  exit 6
 fi
 cp $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services.dmp $REF_HOME/${ORACLE_SID}_scripts/eaglemgr_eagle_services_${now}.dmp
fi

#########For BrandyWine dataexchange request##############
export brwsid=$(print $ORACLE_SID | cut -c1-6)
if [ $brwsid = 'brwdev' -o $brwsid = 'brwtst' ]
then
 $ORACLE_HOME/bin/exp $dxch_user/$dxch_pass  file=$REF_HOME/${ORACLE_SID}_scripts/acct_info.dmp log=$REF_HOME/${ORACLE_SID}_scripts/acct_info.log tables=acct_info statistics=none > /dev/null 2>&1
 grep 'Export terminated successfully without warnings\.' $REF_HOME/${ORACLE_SID}_scripts/acct_info.log > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "\n\n\t\tThere is Some Problem while Exporting DATAEXCHANGE Configuration Table...Please check\n\nAborting Here...."
  print "\n\n***************************************FAILURE*********************************************\n\n"
  print "Check the log $REF_HOME/${ORACLE_SID}_scripts/acct_info.log for more info."
  exit 7
 fi
 cp $REF_HOME/${ORACLE_SID}_scripts/acct_info.dmp $REF_HOME/${ORACLE_SID}_scripts/acct_info_${now}.dmp
fi
#########################################################
print "Log files are under $REF_HOME/${ORACLE_SID}_scripts for More Details"
print "\n\t\t\t*************SUCCESSFUL**************\n"

exit 0
