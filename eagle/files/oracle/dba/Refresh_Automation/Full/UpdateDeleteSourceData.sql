-- Customization versions of this script exist for:
-- agfppd1
-- nwiprd1
--
set serveroutput on verify on feedback on echo on
truncate table pace_masterdbo.pace_hosts_def;
truncate table pace_masterdbo.pace_ftp_def;
truncate table pace_masterdbo.host_def_details;
truncate table pace_masterdbo.pace_app_hosts_def;
truncate table pace_masterdbo.pace_hosts_profiles;
truncate table pace_masterdbo.hosts_profiles_active;
truncate table pace_masterdbo.hosts_profiles_details;
truncate table pace_masterdbo.rpt_profile_override;
truncate table pace_masterdbo.appserver_log;
truncate table pace_masterdbo.update_journal;
truncate table pace_masterdbo.generic_journal_details;
truncate table pace_masterdbo.schedule_def_details;
truncate table pace_masterdbo.schedule_queue;
truncate table pace_masterdbo.feed_results;
truncate table pace_masterdbo.feed_errors;
truncate table pace_masterdbo.feed_error_details;
truncate table pace_masterdbo.adv_rpt_results;
truncate table pace_masterdbo.printer_def;
truncate table rulesdbo.values_set;
truncate table pace_masterdbo.cr_batch_detail;
truncate table pace_masterdbo.xml_reports_log;
truncate table pace_masterdbo.xml_package_log;
truncate table pace_masterdbo.xml_reports_log_detail;
truncate table pace_masterdbo.xml_report_info;
truncate table pace_masterdbo.xml_report_status;
truncate table pace_masterdbo.reports_log;
truncate table pace_masterdbo.email_process;
truncate table pace_masterdbo.email_process_attmts;
truncate table pace_masterdbo.email_process_recipients;
truncate table pace_masterdbo.rpt_profile_email;
-- STAR Tables
truncate table estar.engine_configuration;
truncate table estar.engine_configuration_hist;
truncate table estar.engine_instances;
truncate table estar.web_configuration;
truncate table msgcenter_dbo.msg_instances;
truncate table msgcenter_dbo.msg_instance_services;
truncate table msgcenter_dbo.msg_mc_failover;
truncate table msgcenter_dbo.schedule_run;
truncate table msgcenter_dbo.schedule_event_summary;
truncate table msgcenter_dbo.schedule_pending_reports;
truncate table msgcenter_dbo.schedule_srvr_status;
truncate table eaglemgr.eagle_services;
truncate table eaglemgr.eagle_service_properties;
truncate table eaglemgr.eagle_service_relations;
-- Remove all adhoc jobs from the schedule_def  table
delete from pace_masterdbo.schedule_def where freq_type = 'N';
delete from pace_masterdbo.pace_mail_map_info
where user_instance in (select instance from pace_masterdbo.pace_mail_user_info     
			where mail_address not like '%eagleaccess.com%' 
			and mail_address not like '%eagleinvsys.com%');
delete from pace_masterdbo.pace_mail_user_info 
where mail_address not like '%eagleaccess.com%'  
and mail_address not like '%eagleinvsys.com%';
-- Disable PACE and STAR schedules
UPDATE pace_masterdbo.schedule_def SET ENABLE = 'N' WHERE ENABLE = 'Y';
UPDATE msgcenter_dbo.schedule sch SET sch.active = 0 WHERE sch.active != 0;
commit;
