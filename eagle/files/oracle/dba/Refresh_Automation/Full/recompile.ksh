#!/bin/ksh
# Script name: recompile.ksh
# Usage: recompile.ksh <Oracle SID>
if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

print "\nCompiling objects in the database:\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
spool $REF_HOME/${ORACLE_SID}_scripts/recompile.log
@$ORACLE_HOME/rdbms/admin/utlrp.sql
EOF
if [ $? -ne 0 ]
then
 print "There was an error when recompiling objects in the database."
 print "See $REF_HOME/${ORACLE_SID}_scripts/recompile.log for details."
fi

print "\nList of invalid objects in the database:"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 pagesize 999 trimspool on
col owner format a30
col object_type format a25
col object_name format a30
WHENEVER SQLERROR EXIT FAILURE
spool $REF_HOME/${ORACLE_SID}_scripts/invalid_objects.log
select owner,object_type,object_name from dba_objects where status<>'VALID' order by 1,2,3;
EOF
if [ $? -ne 0 ]
then
 print "There was an error when getting invalid objects in the database."
 print "See $REF_HOME/${ORACLE_SID}_scripts/invalid_objects.log for details."
fi
exit 0
