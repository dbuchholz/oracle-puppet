#!/bin/ksh
# Script name: backup_set_to_use.ksh
# Usage: backup_set_to_use.ksh <Target Oracle SID> <Source Oracle SID>

funct_db_location()
{
DbLoc=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select m.location from inf_monitor.machines m,inf_monitor.databases d
 where m.instance=d.mac_instance and d.sid='$ORACLE_SID' and d.dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is some problem when getting DB Location for $ORACLE_SID in Big Brother."
 print "Aborting Here...."
 return 1
fi

DbLoc=$(print $DbLoc|tr -d "  ")

}
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {
    PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off feedback off
        select 1 from dual;
        exit
EOF
`
    PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
        PERFORM_CRON_STATUS=0
    fi
}

# ------------------------------------------------------
# funct_chk_datadomain(): Make sure Datadomain is mounted 
# ------------------------------------------------------
funct_chk_datadomain()
{
 DD_MOUNTED=$(mount | grep $BACKUP_MOUNT)
 DD_MOUNTED=$(print $DD_MOUNTED|tr -d " ")
 if [[ x$DD_MOUNTED = 'x' ]]
 then
  print "$PROGRAM_NAME can not run because datadomain is not mounted"
  exit 3
 fi
}


############################################################
#                       MAIN
############################################################
if [ $(dirname $0) = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir

if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
 print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 print "If you want to refresh $1 on this host, make an entry in $ORATAB for it.\n"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PROGRAM_NAME=$(print  $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
BACKUP_MOUNT=/datadomain

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

export source_sid=$2

if [[ -a $PARFILE ]]
then
 STATUS=$(cat $PARFILE | sed -e '/^#/d' > $NO_COMMENT_PARFILE)
 LINE=$(cat $NO_COMMENT_PARFILE | grep "INFRASTRUCTURE_DATABASE:")
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
rm -f $NO_COMMENT_PARFILE

funct_chk_datadomain

# Get the backup location
backup_area=$(find $BACKUP_MOUNT/pgh -name $source_sid -type d) > /dev/null 2>&1
if [[ -z $backup_area ]]
then
 backup_area=$(find $BACKUP_MOUNT/evt -name $source_sid -type d) > /dev/null 2>&1
fi
if [[ -z $backup_area ]]
then
 print "Could not find backup area in DataDomain for Database $source_sid"
 exit 4
fi
backup_location=$(print $backup_area | awk -F/ '{print $3}')

not_latest_backup=1      # Not using latest backup.  Requires control file modifications.
replication_complete=1   # Not complete when 1
if [[ -f $backup_area/START_TIME.txt ]]
then
 # Backup is in progress.  Use backup from previous day.  Replication is safe to assume complete for the previous day's backup.
 print "Backup is still in progress.  Will use the previous day's backup."
 last_completed_backup=$(ls -tr $backup_area/*_RESTORE_INFO*.TXT | tail -2 | head -1 | awk -F/ '{print $NF}' | awk -F_ '{print $2"_"$3}')
else
 # Assuming no backup is in progress.  Potential for replication to not be complete.
 not_latest_backup=0
 last_completed_backup=$(ls -tr $backup_area/*_RESTORE_INFO*.TXT | tail -1 | awk -F/ '{print $NF}' | awk -F_ '{print $2"_"$3}')
 if [ $backup_location = pgh ]
 then
  replication_location=evt
 else
  replication_location=pgh
 fi

 # Location of target database
 funct_db_location
 if [ $? -ne 0 ]
 then
  print "Cannot get the location of Target Database $ORACLE_SID from Big Brother."
  print "Aborting here"
  exit 5
 fi
 DbLoc=$(print $DbLoc | awk '{print $1}')
 if [ $DbLoc = PITTSBURGH ]
 then
  DbLoc=pgh
 else
  DbLoc=evt
 fi

 if [ $backup_location = $DbLoc ]
 then
  replication_complete=0
  # This is because there is no START_TIME.txt File present and replication is not required
  # due to source and target being in the same location.
  print "Current backup is complete.  Source and Target database in same location."
 else
  if [[ -f $(ls $backup_area/${source_sid}_${last_completed_backup}_CONTROL_FILE.sql) ]]
  then
   replication_complete=0
   # The preceding file present means that the replication is complete
   # as this is the last file created as part of the backup.
   print "Source and Target database are in different locations and replication is complete."
  fi
 fi
 if [ $replication_complete -ne 0 ]
 then
  print "Replication is not complete for the current backup."
  print "Will use the previous day's backup."
  not_latest_backup=1
  last_completed_backup=$(ls -tr $backup_area/*_RESTORE_INFO*.TXT | tail -2 | head -1 | awk -F/ '{print $NF}' | awk -F_ '{print $2"_"$3}')
 fi
fi

print $not_latest_backup
print $last_completed_backup

exit 0
