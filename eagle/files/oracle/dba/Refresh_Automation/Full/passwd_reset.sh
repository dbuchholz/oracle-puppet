#!/bin/ksh
# Script name:  passwd_reset.sh
# Usage:  passwd_reset.sh <ORACLE SID>
# This script will change the passwords after refresh to as they were before refresh for the users registered in BigB. 
# This is not for an existing Open DR
# Author : NIKHIL KULKARNI 

print "This script will change the passwords after refresh to the values they had before the refresh for the users registered in BigBrother"
export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/LOG

if [[ -f ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

export DATE=$(date +%dth%h%Y_%Hrs_%Mmin_%Ssec)

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts

inst=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set pagesize 0 trimspool on feed off
col environment for a45
select instance from inf_monitor.databases where sid='$ORACLE_SID';
EOF
`
inst=$(print $inst | tr -d " ")

export ispasswd=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set lines 200 pages 0 head off feed off
select inf_monitor.bb_get(nvl(temp_code,code)) from inf_monitor.user_codes where db_instance = $inst and username='SYS';
EOF
`

$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} << EOF
set lines 200 pages 999 trimspool on head off feed off  
spool $REF_HOME/${ORACLE_SID}_scripts/setpas_aft_refresh_${ORACLE_SID}.sql
select 'set echo on feedback on' from dual;
select  'alter user ' || username || ' identified by  "'|| inf_monitor.bb_get(nvl(temp_code,code)) ||'" ;' from inf_monitor.user_codes where db_instance =$inst;
EOF

# Set the passwords in the target database to match the TEMP_CODE values stored for target database in infprd1.  If no TEMP_CODE
# but there is CODE values, it will use the CODE values.
# If there is no passwords stored in infprd1 for target database, this will not do anything.
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pagesize 30000 linesize 132 trimspool on echo on
spool $REF_HOME/${ORACLE_SID}_scripts/setpas_aft_refresh_${ORACLE_SID}.log
@$REF_HOME/${ORACLE_SID}_scripts/setpas_aft_refresh_${ORACLE_SID}.sql
EOF
if [ $? -ne 0 ]
then
 print "There was an error when resetting passwords back to pre-refresh values."
 print "See $REF_HOME/${ORACLE_SID}_scripts/setpas_aft_refresh_${ORACLE_SID}.log for details."
else
 print "Password set back  as it was before refresh "
fi

if [[ -n $ispasswd ]]
then
 rm -f $ORACLE_HOME/dbs/orapw$ORACLE_SID
 $ORACLE_HOME/bin/orapwd file=$ORACLE_HOME/dbs/orapw$ORACLE_SID password=$ispasswd entries=20
 print "New password file is created with name orapw$ORACLE_SID at $ORACLE_HOME/dbs"
fi

exit 0
