#!/bin/ksh
if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID as a parameter into this script."
 print "Usage: $0 <Oracle SID>"
 exit 2
fi

if [ $(dirname $0) = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 3
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
application_database=0    # Is an application database.
user_count=$($ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pages 0
WHENEVER SQLERROR EXIT FAILURE
select count(*) from dba_users where username in ('PACE_MASTERDBO','ESTAR');
EOF)
if [ $? -ne 0 ]
then
 print "There was an error when attempting to retrieve PACE/STAR Schema count from $ORACLE_SID."
 exit 4
fi
if [ $user_count -ne 2 ]
then
 application_database=1    # Is not an application database.
fi

exit $application_database
