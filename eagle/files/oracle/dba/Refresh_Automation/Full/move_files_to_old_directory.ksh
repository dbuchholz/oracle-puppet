#!/bin/ksh
# Script name: move_files_to_old_directory.ksh
# Usage: move_files_to_old_directory.ksh <Oracle SID>
if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <ORACLE_SID>\n"
 print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 2
fi
ORACLE_SID=$1
PAR_HOME=$HOME/local/dba
REF_HOME=$PAR_HOME/Refresh_Automation/Full
mkdir -p $REF_HOME/${ORACLE_SID}_scripts/old_scripts
print "Move previous refresh logs and related files to old_scripts subdirectory."
find $REF_HOME/${ORACLE_SID}_scripts -name "*" -type f -exec mv {} $REF_HOME/${ORACLE_SID}_scripts/old_scripts \; > /dev/null 2>&1
exit 0
