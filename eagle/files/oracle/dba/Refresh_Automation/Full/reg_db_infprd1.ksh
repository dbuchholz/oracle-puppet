#!/bin/ksh
# Script name: reg_db_infprd1.ksh
# Usage: reg_db_infprd1.ksh <Target Oracle SID> <Source Oracle SID> <Three letter Client Code> <Environment DEV|TST|PRD|PPD|POC|DR>
# Author : Nikhil Kulkarni 
# Notes : This script registers a new database in DATABASES and USER_CODES Tables in Big Brother infprd1 Database.  It populates the
# CODES and TEMP_CODE Fields.
# For a new database created from an existing database, users are taken from source database.
# This script does not take any action if the target database is registered in the DATABASES Table and has at least one record in the USER_CODES Table.
# This script will take action on either the DATABASES Table and USER_CODES Table if both entries is missing,
# or it will add the USER_CODES Table entries for the target database if they are missing but the target database entry is present in the DATABASES Table.
if [ $# -ne 4 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <Target Oracle SID> <Source Oracle SID> <Three letter Client Code> <Environment DEV|TST|PRD|PPD|POC|DR>\n"
   exit 1
fi

host=$(print $(hostname) | awk -F. '{print $1}')

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
log_dir=$REF_HOME/${ORACLE_SID}_scripts

sid=$2
client_code=$3
tgt_env=$4

lower_client_code=$(print $client_code | tr '[A-Z]' '[a-z]')
upper_client_code=$(print $client_code | tr '[a-z]' '[A-Z]')

today=$(date "+%d-%b-%y")

temp_paswd=eagle_$lower_client_code

tnsname=$(print $ORACLE_SID | tr '[a-z]' '[A-Z]')
if [ $tgt_env != 'PRD' -a $tgt_env != 'DEV' -a $tgt_env != 'TST' -a $tgt_env != 'PPD' -a $tgt_env != 'POC' -a $tgt_env != 'DR' ]
then
 print "Environment does not match the expected values."
 exit 2
fi

case $tgt_env in
 PRD) environment=PROD;;
 DEV) environment=DEV;;
 TST) environment=TEST;;
 PPD) environment=TEST;;
 POC) environment=TEST;;
 DR) environment=DR;;
 *) environment=TEST;;
esac

VIP=$(grep "${ORACLE_SID}\-" /etc/hosts | awk '{print $1}')
if [[ -z $VIP ]]
then
 VIP=$(grep -w $(uname -a | awk '{print $2}') /etc/hosts | awk '{print $1}')
fi

###to connect infprd1
print "\n\nThis script will change the passwords after refresh to as they were before refresh for the users registered in BigBrother\n\n"
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -f $PARFILE ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

mac_instance=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
set pagesize 0
select instance from inf_monitor.machines where name='$host';
EOF
`
mac_instance=$(print $mac_instance | tr -d " ")

(( target_not_in_bb = 0 ))  # Target database is registered in Big Brother infprd1
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
set pagesize 0
spool $REF_HOME/${ORACLE_SID}_does_target_exist_in_bb.txt
select 1 from inf_monitor.databases where sid='$ORACLE_SID';
EOF
grep '^no rows selected' $REF_HOME/${ORACLE_SID}_does_target_exist_in_bb.txt > /dev/null
if [ $? -eq 0 ]
then
 (( target_not_in_bb = 1 )) # Target database is not registered in Big Brother infprd1
fi
# Get information on Source  database from Big Brother infprd1.
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
set pagesize 0 feedback off echo off verify off
col source_instance format 9999
col oracle_version format a10
col pace_version format a10
col star_version format a10
spool $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt
select instance,oracle_version,pace_version,star_version
from inf_monitor.databases
where sid='$sid' and dataguard='N';
EOF
source_instance=$(awk '{print $1}' $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt)
oracle_version=$(awk '{print $3}' $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt)
pace_version=$(awk '{print $4}' $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt)
star_version=$(awk '{print $5}' $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt)

# Get client instance from supplied client code in Big Brother infprd1.
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
set pagesize 0 feedback off echo off verify off
col source_instance format 9999
spool $REF_HOME/${ORACLE_SID}_target_db_client_instance_from_bb.txt
select instance from clients where code='$upper_client_code';
EOF
target_client_instance=$(awk '{print $1}' $REF_HOME/${ORACLE_SID}_target_db_client_instance_from_bb.txt)

if [ $target_not_in_bb -eq 1 ]
then
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
 insert into inf_monitor.databases (instance,sid,client,dataguard,status,tnsname,environment,oracle_version,pace_version,star_version,vip,dbid,creation_date,update_date,update_user,mac_instance) values
 (inf_monitor.databases_seq.nextval,'$ORACLE_SID',$target_client_instance,'N','OPEN','$tnsname','$environment','$oracle_version','$pace_version','$star_version','$VIP',NULL,to_date('$today'),to_date('$today'),NULL,$mac_instance);
EOF
fi

rm -f $REF_HOME/${ORACLE_SID}_does_target_exist_in_bb.txt $REF_HOME/${ORACLE_SID}_source_db_info_from_bb.txt $REF_HOME/${ORACLE_SID}_target_db_client_instance_from_bb.txt

# Get information on Target database from Big Brother infprd1
target_instance=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
set pagesize 0
select instance from inf_monitor.databases where sid='$ORACLE_SID';
EOF
`
target_instance=$(print $target_instance | tr -d " ")

# If there are no entries in the USER_CODES Table, put the standard values in there.
# Use the source database users and populate USER_CODES Table for the target database.
entries_in_user_codes=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
set pagesize 0
select count(*) from user_codes where db_instance=$target_instance;
EOF
`
entries_in_user_codes=$(print $entries_in_user_codes | tr -d " ")

if [ $entries_in_user_codes -eq 0 ]
then
 if [ $tgt_env = 'DR' ]
 then
  print "Passwords for DR are to be set according to Production and only sys and system is registered in user_codes@BigB...Initially temporary password will be set to $temp_paswd"
 fi

 if [ $tgt_env = 'DEV' ]
 then
  $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  set head off feed off lines 200 pages 999 trimspool on
  spool ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
  select 'insert into user_codes (db_instance,username,code,temp_code) values('||'$target_instance'||','||''''||username||''''||',bb_store('||''''||'$lower_client_code'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','C'),1,1)||'88'||decode(username,'SYS','1_dba9','SYSTEM','2_dba9','_dev9')||''''||'),bb_store('||''''||'$temp_paswd'||''''||'));' from user_codes where db_instance=$source_instance;
EOF
  sed -e "/'SYSTEM'/s/C882_dba9/882_dba9/g" -e "/'SYS'/s/C881_dba9/881_dba9/g" ${log_dir}/ins_uc_new_${ORACLE_SID}.txt > ${log_dir}/ins_uc_new_${ORACLE_SID}.sql
  rm -f ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
 fi

 if [ $tgt_env = 'TST' -o $tgt_env = 'POC' ]
 then
  $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  set head off feed off lines 200 pages 999 trimspool on 
  spool ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
  select 'insert into user_codes (db_instance,username,code,temp_code) values('||'$target_instance'||','||''''||username||''''||',bb_store('||''''||'$lower_client_code'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','C'),1,1)||'11'||decode(username,'SYS','1_dba9','SYSTEM','2_dba9','_tst9')||''''||'),bb_store('||''''||'$temp_paswd'||''''||'));' from user_codes where db_instance=$source_instance;
EOF
  sed -e "/'SYSTEM'/s/C112_dba9/112_dba9/g" -e "/'SYS'/s/C111_dba9/111_dba9/g" ${log_dir}/ins_uc_new_${ORACLE_SID}.txt > ${log_dir}/ins_uc_new_${ORACLE_SID}.sql
  rm -f ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
 fi

 if [ $tgt_env = 'PRD' -o $tgt_env = 'PPD' ]
 then
  $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  set head off feed off lines 200 pages 999 trimspool on
  spool ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
  select 'insert into user_codes (db_instance,username,code,temp_code) values('||'$target_instance'||','||''''||username||''''||',bb_store('||''''||'$lower_client_code'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','C'),1,1)||'59'||decode(username,'SYS','1_dba2','SYSTEM','2_dba2','_prd2')||''''||'),bb_store('||''''||'$temp_paswd'||''''||'));' from user_codes where db_instance=$source_instance;
EOF
  sed -e "/'SYSTEM'/s/C592_dba2/592_dba2/g" -e "/'SYS'/s/C591_dba2/591_dba2/g" ${log_dir}/ins_uc_new_${ORACLE_SID}.txt > ${log_dir}/ins_uc_new_${ORACLE_SID}.sql
  rm -f ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
fi

 if [ $tgt_env = 'DR' ]
 then
  print "Setting passwds for an open DR"
  ##Only sys and system users registered for a DR 
  $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
  set head off feed off lines 200 pages 999 trimspool on
  spool ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
  select 'insert into user_codes (db_instance,username,code,temp_code) values('||'$target_instance'||','||''''||username||''''||',bb_store('||''''||'$lower_client_code'||substr(decode(username,'PACE_MASTERDBO','PACE_MASTERDBO','SCRUBDBO','SCRUBDBO','DATAEXCHDBO','DATAEXCHDBO','ESTAR','ESTAR','C'),1,1)||'59'||decode(username,'SYS','1_dba2','SYSTEM','2_dba2','_prd2')||''''||'),bb_store('||''''||'$temp_paswd'||''''||'));' from user_codes where db_instance=$source_instance and username in ('SYS','SYSTEM');
EOF
  sed -e "/'SYSTEM'/s/C592_dba2/592_dba2/g" -e "/'SYS'/s/C591_dba2/591_dba2/g" ${log_dir}/ins_uc_new_${ORACLE_SID}.txt > ${log_dir}/ins_uc_new_${ORACLE_SID}.sql
  rm -f ${log_dir}/ins_uc_new_${ORACLE_SID}.txt
 fi

 grep 'ORA-' ${log_dir}/ins_uc_new_${ORACLE_SID}.sql > /dev/null
 if [ $? -eq 0 ]
 then
  print "** Check $log_dir/ins_uc_new_${ORACLE_SID}.sql for errors ... Note actions taken so far"
  print "Exiting ...."
  exit 2
 fi

 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
 set feed off head off
 set lines 200 pages 999 trimspo on feed on head off echo on
 spool ${log_dir}/ins_uc_new_${ORACLE_SID}.log
 @${log_dir}/ins_uc_new_${ORACLE_SID}.sql
EOF
 grep 'ORA-' ${log_dir}/ins_uc_new_${ORACLE_SID}.log > /dev/null
 if [ $? -eq 0 ]
 then
  print "** Check ${log_dir}/ins_uc_new_${db}_$$.log for errors ... Note actions taken so far"
  print "Exiting ...."
  exit 2
 fi
 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
  set feedback off linesize 200 pagesize 0 trimspool on
  spool ${log_dir}/set_temp_passwords_in_db_${ORACLE_SID}.txt
  select 'alter user '||username||' identified by $temp_paswd ;' from user_codes where db_instance=(select instance from databases where sid='$sid' and dataguard='N');
EOF

 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 @${log_dir}/set_temp_passwords_in_db_${ORACLE_SID}.txt
EOF
 rm -f ${log_dir}/set_temp_passwords_in_db_${ORACLE_SID}.txt
else
 print "Database $ORACLE_SID already registered in Big Brother Database infprd1"
fi

exit 0 
