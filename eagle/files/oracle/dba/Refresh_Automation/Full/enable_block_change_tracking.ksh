# ==================================================================================================
# NAME:       enable_block_change_tracking.ksh
# USAGE:      enable_block_change_tracking.ksh <Oracle SID>
# AUTHOR:     Frank Davis
# PURPOSE:    Used for RMAN fast incremental backups
# ==================================================================================================
if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Oracle SID>\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 exit 2
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

REF_HOME=/u01/app/oracle/local/dba/Refresh_Automation/Full
datafile=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 pages 0 head off feed off
spool $REF_HOME/${ORACLE_SID}_scripts/${ORACLE_SID}_bct.txt
select file_name from dba_data_files where tablespace_name='SYSTEM' and rownum<2;
EOF
`
directory_name=$(dirname $(print $datafile | sed '/^$/d'))

isbct=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 pages 0 head off feed off
select status FROM v\\$block_change_tracking;
EOF
`

if [ $isbct = 'DISABLED' ]
then
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 set lines 200 pages 0 head off feed off
 alter database enable block change tracking using file '$directory_name/block_change_tracking.dbf' reuse; 
EOF
else
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 set lines 200 pages 0 head off feed off
 alter database disable block change tracking;
 alter database enable block change tracking using file '$directory_name/block_change_tracking.dbf' reuse;
EOF
fi
print "\nBlock change tracking has been enabled using file $directory_name/block_change_tracking.dbf.\n"

exit 0
