#!/bin/ksh
# ==================================================================================================
# NAME:		is_source_and_target_on_same_host.ksh                           
# 
# AUTHOR:   Frank Davis                                   
#
# PURPOSE:	This script will check to see if the source and target database are on the same host
#
# USAGE:	is_source_and_target_on_same_host.ksh <Target_SID> <Source_SID>
#
#
# ==================================================================================================
funct_host_count()
{
host_count=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select count(distinct mac_instance) from databases where sid in ('$source_sid','$target_sid') and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while fetching the host count."
 print "Aborting Here..."
 return 1
fi

(( host_count=$(print $host_count|tr -d " ") ))
return $host_count
}
#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
 print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

target_sid=$1

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $1 entry in $ORATAB file"
 print "If you want to refresh $1 on this host, make an entry in $ORATAB for it.\n"
 exit 2
fi

export ORACLE_SID=$target_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

source_sid=$2

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi

funct_host_count
if [ $? -eq 1 ]
then
 exit 3
fi

exit 0
