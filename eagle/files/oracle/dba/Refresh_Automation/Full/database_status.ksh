#!/bin/ksh
# Script name: database_status.ksh
# Usage: database_status.ksh [Oracle SID]
# Determine the operating status of an Oracle database
# This program can be called from any script to
# get the status of the database.
# Exit Codes:
# 0	Database running and considered operational
# 1	Oracle SID passed into script does not exist in oratab file
# 2	At least one mandatory database background process missing
# 3	Database is in NOMOUNT State
# 4	Database is in MOUNT State
# 5	Database appears to be OPEN but cannot select from sys.dual table
# 6	Database is down as no background processes are present
#
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
 status=1
 exit $status
fi
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

# Are any background processes there
ps -ef | grep $ORACLE_SID | grep "ora_...._$ORACLE_SID" | grep -v grep > /dev/null
if [ $? -eq 0 ]
then

# Are any mandatory background processes missing
for process in dbw0 smon lgwr reco ckpt pmon arc0
do
 found_process=$(ps -ef | grep ora_${process}_$ORACLE_SID | grep -v grep | awk '{print $NF}')
 if [[ -z $found_process ]]
 then
  status=2
  exit $status
 fi
done

query=$($ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
select status from v\$instance; 
EOF)
status=$(print $query | awk '{print $3}')
typeset -L1 status
case $status in
 S) status=3;;
 M) status=4;;
 O) query=$($ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
select dummy from dual; 
EOF)
dummy=$(print $query | awk '{print $3}')
if [ $dummy != X ]
then
 status=5
fi;;
esac
else
 status=6
fi
exit $status
