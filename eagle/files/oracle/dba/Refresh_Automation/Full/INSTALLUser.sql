drop role EA_INSTALL_ROLE;
create role EA_INSTALL_ROLE;
grant CREATE ANY PROCEDURE,ALTER ANY PROCEDURE,CREATE ANY VIEW,SELECT ANY TABLE,CREATE SESSION to EA_INSTALL_ROLE;

DROP USER Abanerjee cascade;

CREATE USER Abanerjee
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO Abanerjee;
audit all by Abanerjee by access;
audit update table, insert table, delete table by Abanerjee by access;
audit execute procedure by Abanerjee;

drop user bmistri cascade;

drop user stewari cascade;

CREATE USER stewari
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO stewari;
audit all by stewari by access;
audit update table, insert table, delete table by stewari by access;
audit execute procedure by stewari ;

drop user creddy cascade;

drop user dbelambe cascade;

CREATE USER dbelambe
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO dbelambe;
audit all by dbelambe by access;
audit update table, insert table, delete table by dbelambe by access;
audit execute procedure by dbelambe;

drop user dsahoo cascade;

drop user gsethi cascade;

CREATE USER gsethi
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO gsethi;
audit all by gsethi by access;
audit update table, insert table, delete table by gsethi by access;
audit execute procedure by gsethi ;


drop user gdesista cascade;

CREATE USER gdesista
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO gdesista;
audit all by gdesista by access;
audit update table, insert table, delete table by gdesista by access;
audit execute procedure by gdesista ;

drop user jwright cascade;

CREATE USER jwright
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO jwright;
audit all by jwright by access;
audit update table, insert table, delete table by jwright by access;
audit execute procedure by jwright ;

drop user akhaladkar cascade;

CREATE USER akhaladkar
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO akhaladkar;
audit all by akhaladkar by access;
audit update table, insert table, delete table by akhaladkar by access;
audit execute procedure by akhaladkar;

drop user mchiniwar cascade;

drop user cmeyer cascade;

CREATE USER cmeyer
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO cmeyer;
audit all by cmeyer by access;
audit update table, insert table, delete table by cmeyer by access;
audit execute procedure by cmeyer;


drop user psherry cascade;

CREATE USER psherry
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO psherry;
audit all by psherry by access;
audit update table, insert table, delete table by psherry by access;
audit execute procedure by psherry;


drop user rdey cascade;

CREATE USER rdey
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO rdey;
audit all by rdey by access;
audit update table, insert table, delete table by rdey by access;
audit execute procedure by rdey;

drop user rverity cascade;

drop user ppjoseph cascade;

CREATE USER ppjoseph
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO ppjoseph;
audit all by ppjoseph by access;
audit update table, insert table, delete table by ppjoseph by access;
audit execute procedure by ppjoseph;


drop user tjoshi cascade;

drop user tfennema cascade;

CREATE USER tfennema
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO tfennema;
audit all by tfennema by access;
audit update table, insert table, delete table by tfennema by access;
audit execute procedure by tfennema;

drop user stam cascade;

CREATE USER stam
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO stam;
audit all by stam by access;
audit update table, insert table, delete table by stam by access;
audit execute procedure by stam;

drop user MGavva cascade;

drop user pjedhe cascade;

CREATE USER pjedhe
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO pjedhe;
audit all by pjedhe by access;
audit update table, insert table, delete table by pjedhe by access;
audit execute procedure by pjedhe;


drop user spatankar cascade;

CREATE USER spatankar
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO spatankar;
audit all by spatankar by access;
audit update table, insert table, delete table by spatankar by access;
audit execute procedure by spatankar;


drop user jdoran cascade;

CREATE USER jdoran
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO jdoran;
audit all by jdoran by access;
audit update table, insert table, delete table by jdoran by access;
audit execute procedure by jdoran;

drop user ctaft cascade;

CREATE USER ctaft
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO ctaft;
audit all by ctaft by access;
audit update table, insert table, delete table by ctaft by access;
audit execute procedure by ctaft;

drop user yshaik cascade;

CREATE USER yshaik
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO yshaik;
audit all by yshaik by access;
audit update table, insert table, delete table by yshaik by access;
audit execute procedure by yshaik;

drop user MPoreddy cascade;

drop user bswain cascade;

CREATE USER bswain
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO bswain;
audit all by bswain by access;
audit update table, insert table, delete table by bswain by access;
audit execute procedure by bswain;

drop user mdubey cascade;

CREATE USER mdubey
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO mdubey;
audit all by mdubey by access;
audit update table, insert table, delete table by mdubey by access;
audit execute procedure by mdubey;

drop user pkumar cascade;

CREATE USER pkumar
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO pkumar;
audit all by pkumar by access;
audit update table, insert table, delete table by pkumar by access;
audit execute procedure by pkumar;

drop user pkar cascade;

CREATE USER pkar
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO pkar;
audit all by pkar by access;
audit update table, insert table, delete table by pkar by access;
audit execute procedure by pkar;


drop user racharya cascade;

CREATE USER racharya
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO racharya;
audit all by racharya by access;
audit update table, insert table, delete table by racharya by access;
audit execute procedure by racharya;

drop user ssingh cascade;

CREATE USER ssingh
PROFILE EA_PROFILE
IDENTIFIED BY install#123 PASSWORD EXPIRE
DEFAULT TABLESPACE USERS
QUOTA UNLIMITED ON USERS
TEMPORARY TABLESPACE TEMP
ACCOUNT UNLOCK;

GRANT EA_INSTALL_ROLE TO ssingh;
audit all by ssingh by access;
audit update table, insert table, delete table by ssingh by access;
audit execute procedure by ssingh;
