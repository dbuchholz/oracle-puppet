#!/bin/ksh
# Script Name: apply_passwords_ea_service.ksh
# Usage: apply_passwords_ea_service.ksh [Oracle SID]

if [ $# -ne 1 ]
then
 print "\nMust enter one parameter into this script."
 print "The parameter must be the Oracle SID.\n"
 print "$0 <Oracle SID>"
 exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)

# Get the Oracle Home where the current listener is running.
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
log_dir=$script_dir/${ORACLE_SID}_scripts

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
spool $log_dir/apply_ea_service_pwd_${ORACLE_SID}.log
@$log_dir/preserve_ea_service_pwd_${ORACLE_SID}.sql
EOF

exit 0
