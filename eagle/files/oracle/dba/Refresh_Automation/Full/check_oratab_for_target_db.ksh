#!/bin/ksh
# ==================================================================================================
# NAME:		check_oratab_for_target_db.ksh
# USAGE:        check_oratab_for_target_db.ksh  <Target_SID> <Source_SID>
# AUTHOR:       Frank Davis                   
# PURPOSE:	There must be an entry in the oratab file for the target database
#               If the entry is not there, the refresh will abort.
#               The proper entry will be shown in this script.
#               The entry will need to put in oratab and the refresh script re-run.
# ==================================================================================================
funct_db_version()
{
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from inf_monitor.databases
 where sid='$psid'
 and dataguard='N';
EOF
`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem when getting DB Instance for $psid"
 print "Aborting Here...."
 exit 6
fi

DbIns=$(print $DbIns|tr -d " ")

if [[ -n $DbIns ]]
then
 version=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select oracle_version from inf_monitor.databases where instance=$DbIns;
EOF
`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem getting Oracle version for $psid\nAborting Here....\n"
  exit 7
 fi
 version=$(print $version|tr -d " ")
fi
}
#################### MAIN ##########################
if [ $# -ne 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!\n"
 print "\t\tUsage : $0 <Target_SID> <Source_SID>\n"
 print "\t\t<Target_SID> on this host and <Source_SID> from where the Target database is being refreshed\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 any_sid=$(ps -ef | grep ora_smon_ | tail -1 | awk '{print $NF}' | awk -F_ '{print $NF}')
 export ORACLE_SID=$any_sid
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH
 . /usr/local/bin/oraenv > /dev/null

 export PAR_HOME=$HOME/local/dba
 export REF_HOME=$PAR_HOME/Refresh_Automation/Full
 export PARFILE=$PAR_HOME/BigBrother.ini
 export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
 export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
 export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
 export PERFORM_CRON_STATUS=0

 if [[ -a ${PARFILE} ]]
 then
  STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
  LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
  rm -f $NO_COMMENT_PARFILE
  if [[ -n $LINE ]]
  then
   INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
   export CRON_SID=$(print $INFO | awk '{print $1}')
   export CRON_USER=$(print $INFO | awk '{print $2}')
   CRON_CONNECT=$(print $INFO | awk '{print $3}')
   export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  fi
 fi
 export psid=$2
 funct_db_version
 version_shortened=$(print $version | awk -F. '{print $1"."$2"."$3}')
 oracle_home=$(find /u01/app/oracle/product -name sqlplus | grep '\/bin\/' | sed 's|/bin/sqlplus||g' | grep "$version_shortened")
 oratab_entry=$(print "$1:$oracle_home:N")
 print "\nThe refresh cannot continue because there is no entry for the local target database in the $ORATAB File.\n"
 print "Add this entry into the $ORATAB File and re-run the refresh script."
 print "$oratab_entry\n"
 exit 2
fi

exit 0
