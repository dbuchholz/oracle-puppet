#!/bin/ksh
# Script name:  refresh_automated.ksh
# Usage:
# refresh_automated.ksh -t <Target SID> -s <Source SID> -c <3 letter Client Code> -e <Environment DEV|TST|PRD|PPD|POC|DR> -b <Optional Backup Set YYYYMMDD> -r <Optional> -p <Purpose> -n <RT Ticket> -d <DBA> -q <SQL Script for truncates> -m <Custom Backup Location> -a <Optional Recovery Timestamp> -u <0|1>
#
# Prerequisites to running this script:
# 1. The source database of the refresh must exist in Big Brother infprd1 and be fully populated in DATABASE, MACHINES and USER_CODES.
# 2. An entry must exist in the oratab File for the Target database to be refreshed.
# 3. The Big Brother infprd1 CLIENTS Table must have an entry for the client of the Target database to be refreshed.
# 4. The Big Brother infprd1 MACHINES Table must have an entry for the server where the Target database is running.
# 5. Only use the -m option when the backup set to be restored has been backed up originally to the location specified with
#    the -m option or the backup set is symbolically linked to the original backup location.  In all other cases, copy
#    the special backup from its location to the original location.
#
# Notes:
#
# -m Switch:  Use this switch to specify a non-standard backup location.  If there are multiple backups in this location, the most recent
#             backup is the one that will be used.  It bases most recent by the timestamps on the files, not the date tag in the file names.
#             You cannot use the "-b" Switch with this switch.
#
#             You can use an older backup in the location specified by this switch if you set the timestamp on the file  *_RESTORE_INFO*.TXT
#             in the desired backup set to the current time.
#             -rw-r--r-- 1 oracle oinstall 3164 Oct 21 16:30 bfrprd1_20121021_163004_RESTORE_INFO_level_1.TXT
#             -rw-r--r-- 1 oracle oinstall 3164 Oct 22 16:30 bfrprd1_20121022_163004_RESTORE_INFO_level_1.TXT
#             -rw-r--r-- 1 oracle oinstall 3164 Oct 23 16:30 bfrprd1_20121023_163004_RESTORE_INFO_level_1.TXT
#             For example, let's say you want the backup from October 21st.  Issue the command:   touch -t bfrprd1_20121021_163004_RESTORE_INFO_level_1.TXT
#             This will set the timestamp on the file to the current time.  The script will now use the backup set identified by 20121021_163004 for the restore.
#             To return the *_RESTORE_INFO*.TXT file to the original timestamp after the refresh, in this example use:  touch -t "201210211630"
#             Returning the timestamp to its original value would make the next restore from this specified location use the backup that was most recently created.
#             Do not use this timestamp modification method when restoring from the standard location.  Such as when you are not using the "-m" Switch.
#             You will use the "-b" Switch when restoring from the standard location.
#
# -b Switch:  Use this switch when the backup is in the standard location.  Not to be used with the "-m" Switch.  Specify the specific backup to use.
#             Locate the backup you want to use.  Copy the file tag of the form YYYYMMDD_###### and use with this switch.  For example above, the 
#             file tag is: 20121021_163004
#
# -a Switch:  If Recovery Timestamp is specified, it must be of this format: YYYY-MM-DD:hh24:mi:ss
#             For example: 2014-06-15:05:50:56
#             This specifies June 15, 2014 at 5:50am plus 56 seconds.
#             Quotes are not required around this value when passed in with the "-a" Switch
#             It is up to the operator to determine the correct backup "-b" to use with the specified recovery time "-a".
#             With one exception, both the "-a" and "-b" Switches must be used if the "-a" Switch is used.
#             The "-b" Switch must specify a backup file tag that is the next backup available after the "-a" timestamp specified.
#             For example, the combination of: -a 2014-04-12:19:56:03 -b 20140413_022003
#             is a recovery timestamp of April 12, 2014 at 7:56pm and the backup tag is April 13, 2014, which is the next backup after the recovery timestamp.
#             This is required as the control file used in the restore must know of the archive logs required to roll forward to the recovery timestamp specified.
#             The only time that the "-b" Switch does not need to be specified with the "-a" Switch is when the recovery timestamp is between the most recent backup
#             and the backup previous to the most recent backup.  In this case, the script will use the most recent backup by default.
#
function usage
{
 this_script=$1
 print "Usage:"
 print -R "$this_script [-help]"
 print -R "$this_script -t <Target ORACLE_SID> -s <Source ORACLE_SID> -c <Client Code> -e <Environment> -b <Backup Set> -p <Purpose> -n <RT Ticket> -d <DBA> -q <SQL Script for truncates> -m <Custom Backup Location> -u <Upgrade Flag>\n"
 cat<<EOF

                Help about switches
                -------------------
                -t : Database target instance name (Mandatory)
                -s : Database source instance name.  This is the Oracle SID name from the backup set. (Mandatory)
                -c : Client Three Letter Code (Mandatory)
                -e : Environment Name in: DEV|TST|PPD|POC|DR|PRD (Mandatory)
                -b : Backup set to use.  In the form of YYYYMMDD_######  where ###### is a six digit number. (Optional)
                -r : Rerun failed refresh requires copying dump files back to working directory (Optional)
                -p : Purpose of Refresh.  Limit to 30 characters. Surround input by single quotes if space present. (Optional)
                -n : RT Ticket Number of Refresh (Optional)
                -d : DBA Performing the Refresh.  Limit to 40 Characters. Surround input by single quotes if space present. (Optional)
                -q : SQL Script for truncate/update/deletes. Defaults to UpdateDeleteSourceData.sql (Optional)
                -m : Custom Backup Location.  Full directory path to location of backup files.  Only one backup allowed in this location. (Optional)
                -a : Recovery Timestamp in the form of: YYYY-MM-DD:hh24:mi:ss  .  For example: 2014-05-06:17:43:00 represents May 6th at 5:43pm (Optional)
                -u : Upgrade from 11.2.0.1 to 11.2.0.3.  Values can be: 0 = No upgrade  1 = Upgrade.  Default is 0. (Optional)

EOF
}
function send_email_alert
{
 mailx -s "$machine_name $PROGRAM_NAME $ORACLE_SID Failure" $MAILTO<$MAIL_FILE
}
# ******************* MAIN *****************
clear
now=$(date +'%Y%m%d_%H%M%S')
(( start_second=$(date +'%s') ))
start_time=$(date "+%m/%d/%Y %I:%M:%S %p")

machine_name=$(hostname | awk -F. '{print $1}')
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
help_me=$(print -R $1 | cut -c1-2)
if [ $help_me = '-h' ]
then
 print "\nHere is help on how to pass parameters into this script:\n"
 usage $0
 exit 1
fi
if [ $# -lt 8 ]
then
 print "\n\t\tInvalid Arguments!"
 print "Must pass a minimum of eight parameters into this script:"
 usage $0
 exit 2
fi
# Parse input arguments
while (( $# > 0 ))
do
 case "$1" in
  -h|-help) print
            usage $0
            ;;
  -t) ORACLE_SID=$2
      shift
      ;;
  -s) sid=$2
      shift
      ;;
  -c) client=$2
      shift
      ;;
  -e) tgt_env=$2
      shift
      ;;
  -m) backup_location=$2
      if [[ -n $backup_location ]]
      then
       shift
      fi
      ;;
  -b) specific_backup_set=$2
      if [[ -n $specific_backup_set ]]
      then
       shift
      fi
      ;;
  -r) rerun_failed=$1
      ;;
  -p) reason=$2
      if [[ -n $reason ]]
      then
       shift
      fi
      ;;
  -n) ticket=$2
      if [[ -n $ticket ]]
      then
       shift
      fi
      ;;
  -d) DBA=$2
      if [[ -n $DBA ]]
      then
       shift
      fi
      ;;
  -q) sql_script=$2
      if [[ -n $sql_script ]]
      then
       shift
      fi
      ;;
  -a) recovery_timestamp=$2
      if [[ -n $recovery_timestamp ]]
      then
       shift
      fi
      ;;
  -u) upgrade=$2
      if [[ -n $upgrade ]]
      then
       shift
      fi
      ;;
   *) print "\n\t\tInvalid Arguments!"
      usage $0
      ;;
 esac
 shift
done

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "\nThere is no $ORACLE_SID entry in $ORATAB file"
 print "If you want to refresh $ORACLE_SID on this host, make an entry in $ORATAB for it.\n"
 exit 3
fi

export ORACLE_SID  #Target
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again."
 print "Aborting Here."
 exit 4
fi

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat $PARFILE | sed -e '/^#/d'  > $NO_COMMENT_PARFILE)
 LINE=$(cat $NO_COMMENT_PARFILE | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
 fi
fi

log_dir=$script_dir/${ORACLE_SID}_scripts
mkdir -p $log_dir
log_file=$log_dir/refresh_database_${ORACLE_SID}_${now}.log
export MAIL_FILE=$log_dir/mail_${ORACLE_SID}_${now}.dat
export MAILTO=team_dba@eagleaccess.com

start_timestamp=$(date)
print "Refresh started at: $(date)" | tee $log_file
print "\nSee log file $log_file to monitor this process."

tgt_env=$(print $tgt_env | tr '[a-z]' '[A-Z]')
if [ $tgt_env != 'PRD' -a $tgt_env != 'DEV' -a $tgt_env != 'TST' -a $tgt_env != 'DR' -a $tgt_env != 'PPD' -a $tgt_env != 'POC' ]
then
 print "Environment does not match the expected values of TST, PRD, DEV, DR, PPD or POC." >> $log_file
 print "Aborting here." >> $log_file
 exit 5
fi

if [[ -n $specific_backup_set ]]
then
 print "$specific_backup_set" | grep -w '20[0-9][0-9][0-1][0-9][0-3][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]' > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  print "The backup set must be of the form YYYYMMDD_######" >> $log_file
  print "YYYY is four digit year, MM is the month of year and DD is the day of month. ###### is a six-digit number" >> $log_file
  print "Aborting here." >> $log_file
  exit 6
 fi
fi

if [[ -z $upgrade ]]
then
 upgrade=0
fi
if [[ ! $upgrade = 0 ]] && [[ ! $upgrade = 1 ]]
then
 print "If the "-u" parameter is entered, it must be a 0 or 1."
 print "0 = Do not upgrade from 11.2.0.1 to 11.2.0.3."
 print "1 = Upgrade from 11.2.0.1 to 11.2.0.3."
 exit 7
fi

print "$script_dir/check_oratab_for_target_db.ksh $ORACLE_SID $sid" >> $log_file
$script_dir/check_oratab_for_target_db.ksh $ORACLE_SID $sid >> $log_file 2>&1
if [ $? -ne 0 ]
then
 print "There is no entry for the target database in the $ORATAB File." >> $log_file
 print "Aborting here." >> $log_file
 exit 8
fi

not_latest_backup=0
if [[ -z $backup_location ]]
then
 # Backup location in the standard location.
 if [[ -z $specific_backup_set ]]
 then
  print "$script_dir/backup_set_to_use.ksh $ORACLE_SID $sid" >> $log_file
  $script_dir/backup_set_to_use.ksh $ORACLE_SID $sid | tee -a $log_file | tee $log_dir/backup_set_to_use_${ORACLE_SID}.log > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
   print "Could not get the backup set to use for the refresh." >> $log_file
   print "Aborting here." >> $log_file
   exit 9
  fi
  backup_set=$(tail -1 $log_dir/backup_set_to_use_${ORACLE_SID}.log)
  not_latest_backup=$(tail -2 $log_dir/backup_set_to_use_${ORACLE_SID}.log | head -1)
  print "Will use the latest backup available in DataDomain for $sid" >> $log_file
 else
  print "As specified, will use a backup that is not the latest available in DataDomain for $sid" >> $log_file
  backup_set=$specific_backup_set
  not_latest_backup=1
 fi
 print "Backup Set of $sid that will be used is from timestamp: $backup_set" >> $log_file
else
 # If a custom backup location is specified, it will take the most recent backup in that location.
 backup_set=$(ls -tr $backup_location/*_RESTORE_INFO*.TXT | tail -1 | awk -F/ '{print $NF}' | awk -F_ '{print $2"_"$3}')
 not_latest_backup=1
fi

((source_target_same_host=0))
if [[ -z $backup_location ]]
then
 $script_dir/is_source_and_target_on_same_host.ksh $ORACLE_SID $sid >> $log_file
 if [ $? -ne 0 ]
 then
  # Source and Target database are on the same host
  ((source_target_same_host=1))
 fi
fi

print "$script_dir/move_files_to_old_directory.ksh $ORACLE_SID" >> $log_file
$script_dir/move_files_to_old_directory.ksh $ORACLE_SID >> $log_file 2>&1
print "$script_dir/CopyRestoreFiles.sh $ORACLE_SID $sid $backup_set $not_latest_backup $backup_location" >> $log_file
$script_dir/CopyRestoreFiles.sh $ORACLE_SID $sid $backup_set $not_latest_backup $backup_location >> $log_file 2>&1

if [[ -n $backup_location ]]
then
 set_bkup="-m $backup_location"
fi
if [[ -n $recovery_timestamp ]]
then
 set_recov="-a $recovery_timestamp"
fi

echo "$script_dir/build_datafile_tempfile.ksh -t $ORACLE_SID -s $sid -b $backup_set $set_bkup $set_recov" >> $log_file
$script_dir/build_datafile_tempfile.ksh -t $ORACLE_SID -s $sid -b $backup_set $set_bkup $set_recov >> $log_file 2>&1
if [ $? -ne 0 ]
then
 exit 10
fi

((db_was_running = 1))  # Database was not running when this script started.
print "$script_dir/database_status.ksh $ORACLE_SID" >> $log_file
$script_dir/database_status.ksh $ORACLE_SID >> $log_file 2>&1
if [ $? -eq 0 ]
then
 ((db_was_running = 0)) # Database was running when this script started.
fi

if [ $db_was_running -eq 0 ]
then
 $script_dir/application_database.ksh $ORACLE_SID
 application_database=$?
 if [ $application_database -gt 1 ]
 then
  print "Aborting here."
  exit 11
 fi
 #application_database=0 means that it has PACE/STAR Schemas and is a standard Eagle application database.
 #application_database=1 means that it does not have PACE/STAR Schemas.

 if [ $application_database -eq 0 ]
 then
  if [[ -z $backup_location ]]
  then
   print "$script_dir/CheckApplicationVersions.sh $ORACLE_SID $sid" >> $log_file
   $script_dir/CheckApplicationVersions.sh $ORACLE_SID $sid >> $log_file 2>&1
   if [ $? -ne 0 ]
   then
    print "Pace and Star Versions from source to target database do not match." >> $log_file
    # print "Aborting here."
    # exit 10
   fi
  fi
 fi
 if [[ -z $backup_location ]]
 then
  # Backup in standard location, not a custom location
  if [ $upgrade -eq 0 ]
  then
   print "$script_dir/verify_oracle_versions.ksh $ORACLE_SID $sid" >> $log_file
   $script_dir/verify_oracle_versions.ksh $ORACLE_SID $sid >> $log_file 2>&1
   if [ $? -ne 0 ]
   then
    print "Oracle Versions from source to target database do not match." >> $log_file
    print "Aborting here." >> $log_file
    exit 12
   fi
  fi
 fi
 if [ $ORACLE_SID = brwdev2 ]
 then
  print "Exporting Brandywine custom schemas." >> $log_file
  $script_dir/Export_BGIM_APP_RWRO.sh $ORACLE_SID >> $log_file 2>&1
  if [ $? -ne 0 ]
  then
   print "\nFailure in Export_BGIM_APP_RWRO.sh" >> $log_file
   print "Aborting here." >> $log_file
   exit 13
  fi
 fi
 if [ $application_database -eq 0 ]
 then
  print "$script_dir/pace_star_version.ksh $ORACLE_SID" >> $log_file
  app_version=$($script_dir/pace_star_version.ksh $ORACLE_SID)
  if [ $? -ne 0 ]
  then
   print "Cannot get the version of PACE/STAR in $ORACLE_SID." >> $log_file
   $script_dir/pace_star_version.ksh $ORACLE_SID >> $log_file 2>&1
   print "Aborting here." >> $log_file
   exit 14
  fi
  print "$script_dir/ExpCustArchRuleWebConfig.sh $ORACLE_SID $app_version" >> $log_file
  $script_dir/ExpCustArchRuleWebConfig.sh $ORACLE_SID $app_version >> $log_file 2>&1
  if [ $? -ne 0 ]
  then
   print "Export of tables did not complete successfully." >> $log_file
   print "Aborting here." >> $log_file
   exit 15
  fi
 fi
 print "$script_dir/preserve_user_info.sh $ORACLE_SID" >> $log_file
 $script_dir/preserve_user_info.sh $ORACLE_SID >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "Problem preserving user info pre-refresh." >> $log_file
  print "Aborting here." >> $log_file
  exit 16
 fi
 print "$script_dir/datapump_export_structure.ksh $ORACLE_SID" >> $log_file
 $script_dir/datapump_export_structure.ksh $ORACLE_SID >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "Problem extracting Metadata info pre-refresh." >> $log_file
  print "Aborting here." >> $log_file
  exit 17
 fi

 print "$script_dir/preserve_passwords_ea_service.ksh $ORACLE_SID" >> $log_file
 $script_dir/preserve_passwords_ea_service.ksh $ORACLE_SID >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "Problem with Preserving EA_SERVICE Accounts." >> $log_file
  print "This is either the Oracle SID does not exist in the oratab file" >> $log_file
  print "or an Oracle SID was not passed in as a parameter to the script." >> $log_file
  print "Aborting here." >> $log_file
  exit 17
 fi

 print "Switch logfile to get current state of database." >> $log_file
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 alter system archive log current;
EOF
 # Back up archive logs to be able to recover database to the current point-in-time.
 print "/u01/app/oracle/local/dba/backups/rman/RMANArcLogBkp_All_Clean.sh $ORACLE_SID" >> $log_file
 /u01/app/oracle/local/dba/backups/rman/RMANArcLogBkp_All_Clean.sh $ORACLE_SID >> $log_file 2>&1

 print "$script_dir/DeleteDatabase.sh $ORACLE_SID" >> $log_file
 $script_dir/DeleteDatabase.sh $ORACLE_SID >> $log_file 2>&1
else
 # Database was not in OPEN Mode
 print "Database was not open at the beginning of the refresh." >> $log_file
 ps -ef | grep ora_${ORACLE_SID}_smon > /dev/null
 if [ $? -eq 0 ]
 then
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 shutdown immediate
 shutdown abort
EOF
 fi
 find /${ORACLE_SID}-*/oradata -type f -name "*.dbf" > $log_dir/${ORACLE_SID}_datafile_listing.txt
 ((found_an_open_file = 1))
 while read datafile
 do
  /usr/sbin/lsof $datafile
  if [ $? -eq 0 ]
  then
   ((found_an_open_file = 0))
   print "To get to this point in the script it is determined that the database is not open." >> $log_file
   print "This refresh will work on a database this is completely shut down or open." >> $log_file
   print "An open datafile was found in database: $ORACLE_SID" >> $log_file
   print "The datafile is: $datafile" >> $log_file
   print "There may be other datafiles that are open." >> $log_file
   print "If it is required to refresh a database that is suppose to be down" >> $log_file
   print "please shut it down manually and rerun the script." >> $log_file
   print "This is to avoid an unwanted refresh of a database." >> $log_file
   print "The database may be in mount or nomount state." >> $log_file
   print "Aborting the refresh here." >> $log_file
   exit 18
  fi
 done<$log_dir/${ORACLE_SID}_datafile_listing.txt
 print "Removing any potential datafiles and tempfiles from database that is to be refreshed." >> $log_file
 find /${ORACLE_SID}-*/oradata -type f -name "*.dbf" -exec rm -vf {} \; >> $log_file
fi

print "$script_dir/ModifyRestoreFiles.sh $ORACLE_SID $sid $source_target_same_host" >> $log_file
$script_dir/ModifyRestoreFiles.sh $ORACLE_SID $sid $source_target_same_host >> $log_file 2>&1
if [ $? -ne 0 ]
then
 print "Error on running $script_dir/ModifyRestoreFiles.sh" >> $log_file
 print "Aborting here." >> $log_file
 exit 19
fi
print "$script_dir/build_rename_redo.ksh $ORACLE_SID $sid" >> $log_file
$script_dir/build_rename_redo.ksh $ORACLE_SID $sid >> $log_file 2>&1
if [ $? -ne 0 ]
then
 print "Error on running $script_dir/build_rename_redo.ksh" >> $log_file
 print "Aborting here." >> $log_file
 exit 20
fi

print "$script_dir/RestoreDatabase.sh $ORACLE_SID $log_file $upgrade" >> $log_file
$script_dir/RestoreDatabase.sh $ORACLE_SID $log_file $upgrade >> $log_file 2>&1
if [ $? -ne 0 ]
then
 print "Error on running $script_dir/RestoreDatabase.sh" >> $log_file
 print "Aborting here." >> $log_file
 exit 21
fi

print "$script_dir/database_status.ksh $ORACLE_SID" >> $log_file
$script_dir/database_status.ksh $ORACLE_SID >> $log_file 2>&1
if [ $? -ne 0 ]
then
 print "Database is not in OPEN Mode." >> $log_file
 print "Aborting here." >> $log_file
 exit 22
fi
if [ $ORACLE_SID = $sid ]
then
 print "$script_dir/DoNotChangeDBID.sh $ORACLE_SID" >> $log_file
 $script_dir/DoNotChangeDBID.sh $ORACLE_SID >> $log_file 2>&1
else
 print "$script_dir/ChangeDBID.sh $ORACLE_SID" >> $log_file
 $script_dir/ChangeDBID.sh $ORACLE_SID >> $log_file 2>&1
fi
$script_dir/application_database.ksh $ORACLE_SID
application_database=$?
if [ $application_database -gt 1 ]
then
 print "Aborting here." >> $log_file
 exit 23
fi
if [ $application_database -eq 0 ]
then
 front_end_users=$log_dir/front_end_users_${ORACLE_SID}_${now}.txt
 print "$script_dir/Front_End_Users.sh $ORACLE_SID" >> $log_file
 print "See file: $front_end_users" >> $log_file
 print "for a list of front end users information." >> $log_file
 $script_dir/Front_End_Users.sh $ORACLE_SID > $front_end_users 2>&1
fi
print "$script_dir/ImplementTrackDB.sh $ORACLE_SID" >> $log_file
$script_dir/ImplementTrackDB.sh $ORACLE_SID >> $log_file 2>&1

if [ $db_was_running -eq 0 ]
then
 print "$script_dir/passwd_reset.sh $ORACLE_SID" >> $log_file
 $script_dir/passwd_reset.sh $ORACLE_SID >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "There was an error when resetting passwords back to pre-refresh values." >> $log_file
 fi
 print "$script_dir/Unregister_Old_Incarnation.ksh $ORACLE_SID" >> $log_file
 $script_dir/Unregister_Old_Incarnation.ksh $ORACLE_SID >> $log_file 2>&1
 print "$script_dir/apply_passwords_ea_service.ksh $ORACLE_SID" >> $log_file
 $script_dir/apply_passwords_ea_service.ksh $ORACLE_SID >> $log_file 2>&1
fi
if [ $application_database -eq 0 ]
then
 print "$script_dir/UpdateDeleteSourceData.sh $ORACLE_SID $sql_script" >> $log_file
 $script_dir/UpdateDeleteSourceData.sh $ORACLE_SID $sql_script >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "There was an error when truncating tables." >> $log_file
  print "See log file and correct errors before continuing." >> $log_file
  print "Aborting here." >> $log_file
  exit 24
 fi
fi
if [[ -n $rerun_failed ]]
then
 cp $script_dir/${ORACLE_SID}_scripts/old_scripts/*.dmp $script_dir/${ORACLE_SID}_scripts
 old_preserve_user=$(ls -ltr $script_dir/${ORACLE_SID}_scripts/old_scripts/PreserveUser_*.sql | tail -1 | awk '{print $NF}')
 cp $old_preserve_user $script_dir/${ORACLE_SID}_scripts
fi
if [ $db_was_running -eq 0 ]
then
 print "$script_dir/put_back_preserved_users.ksh $ORACLE_SID" >> $log_file
 $script_dir/put_back_preserved_users.ksh $ORACLE_SID >> $log_file 2>&1
else
 if [ $application_database -eq 0 ]
 then
  goldcopy=$(print $sid | cut -c1-3)
  if [ $goldcopy = gcv ]
  then
   print "$script_dir/put_back_dba_install_users.ksh $ORACLE_SID" >> $log_file
   $script_dir/put_back_dba_install_users.ksh $ORACLE_SID >> $log_file 2>&1
   $REF_HOME/SendEmail.sh $ORACLE_SID
  fi
 fi
fi
print "$script_dir/recompile.ksh $ORACLE_SID" >> $log_file
$script_dir/recompile.ksh $ORACLE_SID >> $log_file 2>&1
if [ $ORACLE_SID = brwdev2 ]
then
 print "Importing Brandywine custom schemas." >> $log_file
 $script_dir/Import_BGIM_APP_RWRO.sh $ORACLE_SID >> $log_file 2>&1
 if [ $? -ne 0 ]
 then
  print "\nFailure in Import_BGIM_APP_RWRO.sh" >> $log_file
 fi
fi

if [ $ORACLE_SID = brwtst2 ]
then
 print "Creating Brandywine BGIM_APP_RW and BGIM_APP_RO proxy triggers in ${ORACLE_SID}." >> $log_file
 $script_dir/brw_bgim_app_proxy_trigger.ksh $ORACLE_SID >> $log_file 2>&1
fi

if [ $db_was_running -eq 0 ]
then
 if [ $application_database -eq 0 ]
 then
  print "$script_dir/ImpCustArchRuleWebConfig.sh $ORACLE_SID $app_version" >> $log_file
  $script_dir/ImpCustArchRuleWebConfig.sh $ORACLE_SID $app_version >> $log_file 2>&1
  if [ $? -ne 0 ]
  then
   print "$script_dir/ImpCustArchRuleWebConfig.sh $ORACLE_SID $app_version\n" >> $MAIL_FILE
   print "There was a fatal error in the refresh of $ORACLE_SID" >> $MAIL_FILE
   print "See log files generated by $script_dir/ImpCustArchRuleWebConfig.sh" >> $MAIL_FILE
   send_email_alert
  fi
 fi
fi

print "$script_dir/enable_block_change_tracking.ksh $ORACLE_SID" >> $log_file
$script_dir/enable_block_change_tracking.ksh $ORACLE_SID >> $log_file 2>&1
print "/u01/app/oracle/local/dba/freespace/set_autoextend_next.ksh $ORACLE_SID" >> $log_file
/u01/app/oracle/local/dba/freespace/set_autoextend_next.ksh $ORACLE_SID >> $log_file 2>&1
if [ $db_was_running -eq 1 ]
then
 if [[ -z $backup_location ]]
 then
  print "$script_dir/reg_db_infprd1.ksh $ORACLE_SID $sid $client $tgt_env" >> $log_file
  $script_dir/reg_db_infprd1.ksh $ORACLE_SID $sid $client $tgt_env >> $log_file 2>&1
 else
  print "\n****** It will be required to set up Big Brother for $ORACLE_SID Database *****************" >> $log_file
  print "****** DATABASES and USER_CODES Tables as it was refreshed from a non-standard backup. ****\n" >> $log_file
 fi
fi

print "Refresh completed at: $(date)" | tee -a $log_file
(( end_second=$(date +'%s') ))

elapsed_time_min=$(print "scale=1;($end_second - $start_second) / 60" | bc)
refresh_time="$elapsed_time_min Min"
print "Refresh Time: $refresh_time" >> $log_file

if [[ -z $reason ]]
then
 reason='Client Request'
fi
if [[ -z $DBA ]]
then
 DBA=$(print $LOGNAME)
fi
if [[ -z $ticket ]]
then
 ticket=0
fi

if [ $db_was_running -eq 1 ]
then
 print "\nConfigure the tnsnames.ora and listener.ora file for this database: $ORACLE_SID" >> $log_file
 print "Stop and start Listener to enable remote connectivity to database." >> $log_file
 print "Include tnsnames.ora Connect String Entry for this database in hawaii tnsnames.ora and test with tnsping.\n" >> $log_file
fi

print "Updating DB_REFRESHES Table in infprd1 with refresh information." >> $log_file
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF
set feedback off
WHENEVER SQLERROR EXIT FAILURE
insert into db_refreshes values
(inf_monitor.db_refreshes_seq.nextval,'$ORACLE_SID','$machine_name','$client','$reason','$sid',$ticket,'$DBA',to_date('$start_time','MM/DD/YYYY HH:MI:SS AM'),'$refresh_time');
EOF
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem while inserting record into DB_REFRESHES Table in infprd1." >> $log_file
 print "Aborting Here." >> $log_file
 exit 25
fi

print "Refresh Status:  SUCCESS" | tee -a $log_file

exit 0
