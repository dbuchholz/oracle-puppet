#!/bin/ksh
####################################################################################
# Script name: preserve_user_info.sh
# Usage: preserve_user_info.sh <SID>                                               #
# Program: This script should be run before any database refresh. This program     # 
#          scripts  existing dba users, team install users and any read-only users #
#          with their original passwords and privileges.                           #
####################################################################################
NARG=$#
ORA_SID=$1


log_dir=${HOME}

export ORACLE_SID=$ORA_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

## Checking for input parameter

if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
		echo "Not enough parameter..."
        echo ""
        echo "Usage: ./preserve_user_info.sh <Oracle SID>"
        echo ""
        echo "Your <Oracle SID> options are:"
   for sid in `ps -ef | grep ora_pmon_ | awk '{print $8}'`
    do
     s_sid=`expr substr $sid 10 10`
     echo "     "$s_sid
    done
     echo ""
		exit 1
fi

export ORACLE_SID=${ORA_SID}

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
WORK_DIR=$script_dir/${ORACLE_SID}_scripts

if  [ ! -d  $WORK_DIR ]
then
 mkdir -p $WORK_DIR
fi

/usr/bin/find $WORK_DIR -name "*.sql" -mtime +7 -exec rm -f {} \;

now=$(date +'%Y%m%d_%H%M%S')
export USERFILE=${WORK_DIR}/PreserveUser_${now}.sql

## Createing user list to preserve

UserList=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set head off feedback off pagesize 200 trimspool on
select username from dba_users where profile='EA_PROFILE' and username not in ('SYS','SYSTEM');
exit
!`

# Drop Users.

for users in $(print $UserList)
do
  DropUsers=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 2000 pages 0
  spool $USERFILE append
   select 'drop user '||username||' cascade;' from dba_users where username=upper('$users');
  spool off
  exit
!`
done

## Create Users

for users in `echo $UserList`
do
  GetUserCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set heading off echo off long 200000 linesize 200
  col UserCode for a200
  set pages 0
   select dbms_metadata.get_ddl( 'USER', '$users' ) "UserCode" from dual;
   select ';' from dual;
  exit
!`
echo $GetUserCreate >> $USERFILE
done

## Granting SYSTEM PRIVILEGES

for users in $(print $UserList)
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 2000 pagesize 0
  spool $USERFILE append
   select 'grant '||PRIVILEGE||' to '||grantee||decode(admin_option,'YES',' with Admin option')||';' from dba_sys_privs where grantee=upper('$users');
  spool off
  exit
!`
done

## Grant Roles to users..

for users in $(print $UserList)
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set heading off echo off long 2000 pagesize 0
  spool $USERFILE append
  select 'grant '||GRANTED_ROLE||' to '||grantee||';' from dba_role_privs where grantee=upper('$users');
  exit
  spool off
!`
done

## Granting users privileges.

for users in $(print $UserList)
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 20000 pagesize 0
 spool $USERFILE append
 select 'grant '||privilege||' on ' || owner||'.'||table_name || ' to '|| grantee||';'
from dba_tab_privs where grantee like upper('$users') order by privilege;
spool off
  exit
!`
done


for users in $(print $UserList)
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 2000 pagesize 0
  spool $USERFILE append
  select 'alter user '||username||' quota unlimited on '||tablespace_name||';' from dba_ts_quotas where max_blocks = -1 and username=upper('$users');
  spool off
  exit
!`
done

## Enable Audit on all users

for users in $(print $UserList)
do
  GetAuditing=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 2000 pagesize 0 linesize 200
  spool $USERFILE append
	select 'audit all by '||upper('$users')||' by access;' from dual;
	select 'audit update table, insert table, delete table,execute procedure by '||upper('$users')||' by access;' from dual;
  spool off
  exit
!`
done

GetAudit=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off heading off echo off long 2000 pagesize 0 linesize 200
  spool $USERFILE append
select 'audit '||name||';'
from system_privilege_map
where (name like 'CREATE%TABLE%'
or name like 'CREATE%INDEX%'
or name like 'CREATE%CLUSTER%'
or name like 'CREATE%SEQUENCE%'
or name like 'CREATE%PROCEDURE%'
or name like 'CREATE%TRIGGER%'
or name like 'CREATE%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'ALTER%TABLE%'
or name like 'ALTER%INDEX%'
or name like 'ALTER%CLUSTER%'
or name like 'ALTER%SEQUENCE%'
or name like 'ALTER%PROCEDURE%'
or name like 'ALTER%TRIGGER%'
or name like 'ALTER%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'DROP%TABLE%'
or name like 'DROP%INDEX%'
or name like 'DROP%CLUSTER%'
or name like 'DROP%SEQUENCE%'
or name like 'DROP%PROCEDURE%'
or name like 'DROP%TRIGGER%'
or name like 'DROP%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'EXECUTE%INDEX%'
or name like 'EXECUTE%LIBRARY%');
spool off
exit
!`

## Granting proxy users
$ORACLE_HOME/bin/sqlplus -s "/ as sysdba"<<EOF>/dev/null
set feedback off heading off echo off pagesize 0
spool $USERFILE append
select 'alter user '||client||' grant connect through '||proxy||';' from dba_proxies;
EOF

if [[ -f $USERFILE ]] && [[ -s $USERFILE ]] && [[ $(find $WORK_DIR -name "PreserveUser_*.sql" -mtime -1) ]]
then
:
else
 exit 1
fi

exit 0
