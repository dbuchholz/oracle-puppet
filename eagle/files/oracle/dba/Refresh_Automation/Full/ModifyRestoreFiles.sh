#!/bin/ksh
# ==================================================================================================
# NAME:		ModifyRestoreFiles.sh                            
# AUTHOR:	Basit Khan                                    
# PURPOSE:	This script will Modify the Restore files.
# USAGE:	ModifyRestoreFiles.sh  <Target ORACLE_SID> <Source ORACLE_SID> <Source_Target_on_same_host_flag>
#  
#           source_target_same_host=0   Source and Target database are NOT on the same host
#           source_target_same_host=1   Source and Target database are on the same host
#
# ==================================================================================================
create_init_ora_file()
{
#uncomment the below line to debug
cdate=$(date +'%Y%m%d_%H%M%S')
if [[ -f $ORACLE_HOME/dbs/spfile${ORACLE_SID}.ora ]]
then
 mv $ORACLE_HOME/dbs/spfile${ORACLE_SID}.ora $ORACLE_HOME/dbs/spfile${ORACLE_SID}_${cdate}.ora
else
 print "spfile does not exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi
if [[ -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora ]]
then
 mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora $ORACLE_HOME/dbs/init${ORACLE_SID}_${cdate}.ora
else
 print "\ninit.ora does not exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi
if [[ -f $ORACLE_HOME/dbs/orapw${ORACLE_SID} ]]
then
 mv $ORACLE_HOME/dbs/orapw${ORACLE_SID} $ORACLE_HOME/dbs/orapw${ORACLE_SID}_${cdate}
else
 print "\nPassword File does not exist for $ORACLE_SID in $ORACLE_HOME/dbs"
fi

 print "Creating Password file..."
 export ver_9i=$(print $ORACLE_HOME | grep 9.2)
 if [[ -n $ver_9i ]]
 then
  orapwd file=$ORACLE_HOME/dbs/orapw$sid password=eagle entries=20
 else
  orapwd file=$ORACLE_HOME/dbs/orapw${ORACLE_SID} password=eagle entries=20
 fi
 init_file="$(ls -ltr $REF_HOME/${ORACLE_SID}_scripts/*init${sid}.ora | awk '{print $9}')" 
 print "Creating init.ora file from Restore File $(basename $init_file) ..."
if [[ -n $init_file ]]
then 
 cp $REF_HOME/${ORACLE_SID}_scripts/${sid}*init${sid}.ora $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
else
 print "\n\n*****FAILURE...You did not copy the Restore files using CopyRestoreFiles.sh Script...Please check*****\n"
 exit 7
fi

if [[ -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora ]]
then
 grep -vi fal_client $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi fal_server $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi standby_archive_dest $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi standby_file_management $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi log_archive_dest_2 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi log_archive_dest_state_2 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi db_flashback_retention_target $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 grep -vi db_recovery_file_dest_size $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 grep -vi db_recovery_file_dest $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
 mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
else
 print "\n\ninit.ora file seems to be missing....\n"
 exit 8
fi
export dbname=$(grep -i db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora)
export instname=$(grep -i instance_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora)
 export prod_sid=$(grep -i db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F= '{print $2}' | awk -F\' '{print $2}')
 sed 's/#ORACLE_BASE set from environment//g' $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 | awk -F= '{print $1}' | awk -F. '{print $2}' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list | while read LINE
  do
  grep -vi $LINE $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
  mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
  done
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 | sed -e "s/*.//g" -e "s/,/ /g" -e "s/'//g" -e 's/#.*$//g' -e "s|/${ORACLE_SID}-||g" | awk -F= '{print $2}' | sed 's/ /\n/g' | sed 's/control.*//g' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 log_archive_dest_1=$($DFH /${ORACLE_SID}* | sort -k6 | grep -v '^Filesystem' | tail -1 | awk '{print $NF"/oradata/arch"}')
 mkdir -p $log_archive_dest_1
 log_archive_dest_1="*.log_archive_dest_1='LOCATION=${log_archive_dest_1}'"
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_list $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2

 egrep -vi "db_name|instance_name|cursor_sharing" $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2 | sed 's/'${prod_sid}'/'${ORACLE_SID}'/g' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 print "$dbname" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 if [[ -n $instname ]]
 then
  print "$instname" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 fi
 print "*.cursor_sharing='EXACT'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 if [ $source_target_same_host -eq 1 ]
 then
  print "db_unique_name='$ORACLE_SID'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 fi

 full_control_files_line=$(grep -i control_files= $ORACLE_HOME/dbs/init${ORACLE_SID}.ora)
 control_files_line=$(grep -i control_files= $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | sed 's/^.*control_files=//gi')
 ((commas=$(print $control_files_line | tr -d -c , |wc -c)))
 first_mount_point_source=$(print $control_files_line | sed 's/\,/=/g' | tr "=" "\n" | awk -F/ '{print $2}' | grep $ORACLE_SID | sort -nk2 -t- | grep -v '\-[^0-9]' | head -1 | awk -F- '{print $NF}')
 last_mount_point_source=$(grep -i log_archive_dest_1= $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F= '{print $NF}' | awk -F/ '{print $2}' | awk -F- '{print $NF}')
 grep -v "\/${ORACLE_SID}-" $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 first_mount_point_target=$(df -Ph /${ORACLE_SID}-* | sed '1d' | awk '{print $NF}' | sort -nk2 -t- | grep -v '\-[^0-9]' | head -1)
 second_mount_point_target=$(df -Ph /${ORACLE_SID}-* | sed '1d' | awk '{print $NF}' | sort -nk2 -t- | grep -v '\-[^0-9]' | head -2 | tail -1)
 last_mount_point_target=$(df -Ph /${ORACLE_SID}-* | sed '1d' | awk '{print $NF}' | sort -nk2 -t- | grep -v '\-[^0-9]' | tail -1)
 first_numeric_mp_target=$(print $first_mount_point_target | awk -F- '{print $NF}')
 last_numeric_mp_target=$(print $last_mount_point_target | awk -F- '{print $NF}')

 if [ $first_mount_point_source = $first_numeric_mp_target ]
 then
  print "$full_control_files_line" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 else
  print "*.control_files='$first_mount_point_target/oradata/control01.ctl','$second_mount_point_target/oradata/control02.ctl'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 fi

 if [ $last_mount_point_source = $last_numeric_mp_target ]
 then
  print "$log_archive_dest_1" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 else
  print "*.log_archive_dest_1='LOCATION=$last_mount_point_target/oradata/arch'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2
 fi
 mv $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp2 $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 sed -e "s/'//g" $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | awk -F= '{print $2}' | grep '^\/' | sed -e 's/,.*$//g' -e 's/\/control.*$//g' > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1
 while read LINE
 do
  mkdir -p $LINE
  if [ $? -ne 0 ]
  then
   print "\n\nThere seems to be some problem while creating Directory ${LINE}...Please Check and Execute Again...\n\nAborting Here...."
   print "\n\n*********FAILURE*********\n\n"
   exit 9
  fi
 done<$ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1
 rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp1 

 print "init.ora File Created"
}

modify_DataTemp_file()
{
 datafiletempfile=$REF_HOME/${ORACLE_SID}_scripts/RestoreDataTempFiles_${ORACLE_SID}.rcv
 grep 'allocate channel ' $datafiletempfile > $REF_HOME/${ORACLE_SID}_scripts/wtemp_df.txt
 egrep -v '^run {|allocate channel ' $datafiletempfile > $REF_HOME/${ORACLE_SID}_scripts/xtemp_df.txt
 ncpu=$(grep processor /proc/cpuinfo |wc -l)
 print "\nNumber of CPUs are ${ncpu}.  Will assign RMAN channels up to 1/2 the number of CPUs"
 acha=$(print $ncpu/2 | bc)
 if [ $acha -gt 12 ]
 then
  acha=12
 fi
 print "\nNumber of Channels which will be assign are $acha\n"

 rcha=$(grep RCHL $datafiletempfile | wc -l)

 print "run {" > $REF_HOME/${ORACLE_SID}_scripts/ytemp_df.txt
 if [ $rcha -eq $acha ]
 then
  cat $REF_HOME/${ORACLE_SID}_scripts/wtemp_df.txt >> $REF_HOME/${ORACLE_SID}_scripts/ytemp_df.txt
 else
  i=1
  while [ $i -le  $acha ]
  do
   print "\t\tallocate channel 'RCHL${i}' device type disk;" >> $REF_HOME/${ORACLE_SID}_scripts/ytemp_df.txt
   i=$(( $i+1 ))
  done
 fi
 cat $REF_HOME/${ORACLE_SID}_scripts/ytemp_df.txt $REF_HOME/${ORACLE_SID}_scripts/xtemp_df.txt > $REF_HOME/${ORACLE_SID}_scripts/ztemp_df.txt
 rm -f $REF_HOME/${ORACLE_SID}_scripts/wtemp_df.txt $REF_HOME/${ORACLE_SID}_scripts/xtemp_df.txt $REF_HOME/${ORACLE_SID}_scripts/ytemp_df.txt
 mv $REF_HOME/${ORACLE_SID}_scripts/ztemp_df.txt $datafiletempfile
}

#################### MAIN ##########################
if [ $# -ne 3 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <Target ORACLE_SID> <Source ORACLE_SID> <Source_Target_on_same_host_flag>\n"
   print "Target is the database you are refreshing.  Source is the the database used for the refresh"
   print "source_target_same_host=0   Enter 0 if Source and Target database are NOT on the same host"
   print "source_target_same_host=1   Enter 1 if Source and Target database are on the same host"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 DF='df -lP'
 DFH='df -lPh'
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 DF='df -lk'
 DFH='df -lh'
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi
export sid=$2
export source_target_same_host=$3

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation/Full
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')

modify_DataTemp_file

create_init_ora_file

exit 0
