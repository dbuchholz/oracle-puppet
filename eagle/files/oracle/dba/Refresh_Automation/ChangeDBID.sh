#!/bin/ksh
# ==================================================================================================
# NAME:		ChangeDBID.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will perform the below steps in sequence:
#				1. shutdown immediate
#				2. startup mount pfile='$ORACLE_HOME/dbs/init{Target SID}.ora'
#				3. Change DBID
#				4. If 9i database and not completly shutdown then shutdown the database
#				5. Change db_name and Instance Name in init.or file after DBID change
#				6. startup mount pfile='$ORACLE_HOME/dbs/init{Target SID}.ora'
#				7. add supplemental  log data if it is implementation database 
#				8. open the database in resetlogs mode
#				9. change global_name to {Target db_name}
#				10.create spfile from pfile
#				11.shutdown immediate
#				12.startup
#	
#			
#
# USAGE:	ChangeDBID.sh  <ORACLE_SID>
#
#
# ==================================================================================================

function shutdown_database
{
#uncomment the below line to debug
#set -x
ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 print "\n\n***Shutting down database...PLEASE WAIT***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate;
EOF
if [ $? -ne 0 ]
then
 print "Error on shut down of $ORACLE_SID  database."
 print "Aborting here."
 exit 1
fi
else
 print "\nNo database is runing with $ORACLE_SID\n"
 exit 2
fi
}


function startup_mount
{
#uncomment the below line to debug
#set -x
print "\n\n***Starting database mount...PLEASE WAIT***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup mount pfile='${ORACLE_HOME}/dbs/init${ORACLE_SID}.ora';
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of $ORACLE_SID database."
 print "Aborting here."
 exit 3
fi
}


function change_dbid
{
#uncomment the below line to debug
#set -x
print "\n\n***DATABASE DBID will get change here...Please Wait***\n\n"
$ORACLE_HOME/bin/nid target=sys/eagle dbname=$ORACLE_SID<<EOF
Y
EOF
if [ $? -ne 0 ]
then
 print "Error for Changing the DBID...Please check."
 print "Aborting here."
 exit 4
fi
}


#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

 shutdown_database

 startup_mount

 change_dbid

ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
	shutdown_database
fi

grep -vi db_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp
grep -vi instance_name $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp > $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
print "*.db_name='${ORACLE_SID}'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
print "*.instance_name='${ORACLE_SID}'" >> $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
rm -f $ORACLE_HOME/dbs/init${ORACLE_SID}.ora_temp

print "\n\n***Creating Password file...***\n\n"
mv $ORACLE_HOME/dbs/orapw${ORACLE_SID} $ORACLE_HOME/dbs/orapw${ORACLE_SID}_$(date +'%Y%m%d%H%M')_bkp
$ORACLE_HOME/bin/orapwd file=$ORACLE_HOME/dbs/orapw${ORACLE_SID} password=eagle entries=20

startup_mount

export region=$(print $ORACLE_SID | cut -c4-6)

if [ $region == 'prd' ]
then
 print "\n\n***Production database No need to add supplemental  log data***\n\n"
else
 print "\n\n***Adding supplemental  log...***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database add supplemental  log data;
alter database add supplemental  log data(primary key) columns;
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while adding supplemental log...Please check.\n"
 print "Aborting here."
 exit 5
fi
fi

print "\n\n***Opening database in OPEN RESETLOGS MODE...Please Wait***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database open resetlogs;
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while opening the database in OPEN RESETLOGS...Please check.\n"
 print "Aborting here."
 exit 6
fi

print "\n\n***Changing Global Name...***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database rename global_name to ${ORACLE_SID};
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while changing global name...."
 print "Aborting here."
 exit 7
fi


print "\n\n***Creating SPFILE...***"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
create spfile from pfile;
EOF
if [ $? -ne 0 ]
then
 print "Error on creating spfile for standby database."
 print "Aborting here."
 exit 8
fi

shutdown_database

print "\n\n***Starting Up the database...Please Wait***\n\n"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -ne 0 ]
then
 print "\n\nError while starting up the database...."
 print "Aborting here."
 exit 9
fi

 print "\n\n\t\t*****SUCCESSFUL*****\n\n"
 exit 0
