#!/bin/ksh
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
drop directory exp_bgim_app_rwro;

create directory exp_bgim_app_rwro as '/tmp';
grant read,write on directory exp_bgim_app_rwro to public;

set long 9999999 pagesize 0 trimspool on feedback off linesize 200 longchunksize 9999999

spool /tmp/BGIM_TAB_PRIVS.sql
select dbms_metadata.get_granted_ddl('OBJECT_GRANT','BGIM_APP_RW') from dual;
select dbms_metadata.get_granted_ddl('OBJECT_GRANT','BGIM_APP_RO') from dual;
spool off

exit

!

egrep -v '^ERROR:$|^ORA-31608:|^ORA-06512:|^$' /tmp/BGIM_TAB_PRIVS.sql | \
sed -e 's/$/;/g' -e 's/"//g' -e 's/^[ \t]*//' > /tmp/BGIM_TAB_PRIVS_RUN.sql

expdp userid=\"/ as sysdba\" directory=exp_bgim_app_rwro dumpfile=bgim_app_rwro.dmp schemas=BGIM_APP_RW,BGIM_APP_RO logfile=bgim_app_rwro.log content=all

exit 0
