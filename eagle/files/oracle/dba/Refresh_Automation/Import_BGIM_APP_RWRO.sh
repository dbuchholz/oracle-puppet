#!/bin/ksh
#set -x
read YN?"This script will DROP BGIM_APP_RW and BGIM_APP_RO users, do you want to continue?(Y/N):"
if [[ ${YN} = 'Y' ]] || [[ ${YN} = 'y' ]]; then
  echo ""
  echo ""
  echo ""
  echo ""
  echo ""
  echo ""
  echo "Please wait while I process your request..."
  echo ""
  echo ""
  echo ""

else
        echo "Script Terminated..."
        exit
fi
stty echo


$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
drop directory exp_bgim_app_rwro;

create directory exp_bgim_app_rwro as '/tmp';
grant read,write on directory exp_bgim_app_rwro to public;

drop user BGIM_APP_RW cascade;
drop user BGIM_APP_RO cascade;
exit

!


impdp userid=\"/ as sysdba\" directory=exp_bgim_app_rwro dumpfile=bgim_app_rwro.dmp schemas=BGIM_APP_RW,BGIM_APP_RO logfile=bgim_app_rwro.log content=all

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!

@/tmp/BGIM_TAB_PRIVS_RUN.sql

exit
!


echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo "BGIM_APP_RW and BGIM_APP_RO are dropped and reimported..."
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
echo ""
exit 0
