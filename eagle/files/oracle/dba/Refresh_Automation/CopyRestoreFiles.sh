#!/bin/ksh
# ==================================================================================================
# NAME:		CopyRestoreFiles.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will copy the Restore files from backup location to $REF_HOME/{ORACLE_SID}_scripts
#			location for the database which is being refreshed.
#			
#
# USAGE:	CopyRestoreFiles.sh <ORACLE_SID>
#
#
# ==================================================================================================

function funct_copy_restore_file_goldcopy
{
#uncomment the below line to debug
#set -x
read gold_loc?"Enter the Location for GoldCopy: "
if [[ -n $ORACLE_SID  ]] && [[ -n $gold_loc ]]
then
 gold_loc=$(print $gold_loc|tr -d " ")
 dbid=$(print $ORACLE_SID|tr -d " ")
 if [[ ! -d $gold_loc ]]
 then    	
  print  "\n\n***GoldCopy Location May not exist...Please check***\n"
  exit 11
 fi
 mkdir -p $REF_HOME/${dbid}_scripts/old_scripts
 find $REF_HOME/${dbid}_scripts -maxdepth 1 -name "*" -mtime +1 -type f -exec mv {} $REF_HOME/${dbid}_scripts/old_scripts \; 
find ${gold_loc} -name "*.ora*"  > $REF_HOME/LOG/restore_files.txt
 find ${gold_loc} -name "Restore*" >> $REF_HOME/LOG/restore_files.txt
 egrep "Restore|init" $REF_HOME/LOG/restore_files.txt >  $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').txt
 if [[ -s $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').txt ]]
 then
  rm $REF_HOME/LOG/restore_files.txt
  cat $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').txt | while read LINE
   do
    print "cp -p $LINE $REF_HOME/${dbid}_scripts" >> $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').sh
   done
  rm -f $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').txt
  cp $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').sh $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d%H%M').log
  chmod 755 $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').sh
  . $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').sh
  rm -f $REF_HOME/LOG/restore_files_${dbid}_$(date +'%Y%m%d').sh
  print "\nRestore files for $dbid is copied to $REF_HOME/${dbid}_scripts location from $gold_loc ...Please check\n\n"
 fi
else
 print "\nInvalid Inputs...OR...Backup May not available...Please check\n"
exit 12
fi
exit 0
}



function funct_restore_files_for_date
{
#uncomment the below line to debug
#set -x
 print "Please enter the date for which you want to copy the restore files, DATE FORMAT SHOULD BE  YYYYMMDD : \c"
 read dt
 mkdir -p $REF_HOME/${ORACLE_SID}_scripts/old_scripts
 find $REF_HOME/${ORACLE_SID}_scripts  -maxdepth 1 -name "*" -mtime +1 -type f -exec mv {} $REF_HOME/${ORACLE_SID}_scripts/old_scripts \; 
find ${Bkp_Loc} -name "*${dt}*"  > $REF_HOME/LOG/restore_files.txt
 egrep "Restore|init" $REF_HOME/LOG/restore_files.txt >  $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt
if [[ -s $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt ]]
then
 rm -f $REF_HOME/LOG/restore_files.txt
 cat $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt | while read LINE
 do
  print "cp -p $LINE $REF_HOME/${ORACLE_SID}_scripts" >> $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
 done
 rm -f $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt
 cp $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d%H%M').log
 chmod 755 $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
 . $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
 rm -f $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
 print "\nRestore files for $ORACLE_SID is copied to $REF_HOME/${ORACLE_SID}_scripts location from $Bkp_Loc ...Please check\n\n"
else
print "\n\n***FAILURE....Backup May not Available...Please check\n\n"
exit 10
fi
exit 0
}


function funct_restore_files
{
#uncomment the below line to debug
#set -x
mkdir -p $REF_HOME/${ORACLE_SID}_scripts/old_scripts
find $REF_HOME/${ORACLE_SID}_scripts -maxdepth 1 -name "*" -mtime +1 -type f -exec mv {} $REF_HOME/${ORACLE_SID}_scripts/old_scripts \;
find $Bkp_Loc -name "${sid}*" -mtime -1 > $REF_HOME/LOG/restore_files.txt
find $Bkp_Loc -name "Restore*" -mtime -1 >> $REF_HOME/LOG/restore_files.txt
egrep "Restore|init" $REF_HOME/LOG/restore_files.txt >  $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt
if [[ -s $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt ]]
then
rm -f $REF_HOME/LOG/restore_files.txt
cat $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt | while read LINE
do
 print "cp -p $LINE $REF_HOME/${ORACLE_SID}_scripts" >> $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
done
rm -f $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').txt
cp $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d%H%M').log
chmod 755 $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
. $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
rm -f $REF_HOME/LOG/restore_files_${sid}_$(date +'%Y%m%d').sh
print "\nRestore files for $ORACLE_SID is copied to $REF_HOME/${ORACLE_SID}_scripts location from $Bkp_Loc ...Please check\n\n"
else
print "\n\n***FAILURE....Backup May not Available...Please check\n\n"
exit 9
fi
exit 0
}


function funct_check_bkp_location
{
#uncomment the below line to debug
#set -x
 
DbIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
set heading off
set feedback off
select instance from inf_monitor.databases
where sid='$sid'
and dataguard='N'
and status='OPEN';
EOF`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem Please Rectify and Execute Again
Aborting Here...."
 exit 1 
fi

MacIns=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select mac_instance from inf_monitor.databases
 where sid='$sid'
 and dataguard='N'
 and status='OPEN';
EOF`
if [ $? -ne 0 ]
then
 print "\n\nThere is Some Problem Please Rectify and Execute Again
Aborting Here...."
 exit 1
fi

DbIns=$(print $DbIns|tr -d " ")
if [[ -n $DbIns ]]
then
 Bkp_Loc=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select distinct backup_location from inf_monitor.backup_runs
 where db_instance='$DbIns'
 and status='SUCCESS'
 and start_time between sysdate - 3 and sysdate;
EOF`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
 exit 2
 fi
fi

MacIns=$(print $MacIns|tr -d " ")
if [[ -n $MacIns ]]
then
 Dbserver=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select name from inf_monitor.machines
 where instance='$MacIns';
EOF`
 if [ $? -ne 0 ]
 then
  print "\n\nThere is Some Problem Please Rectify and Execute Again\nAborting Here....\n"
  exit 2
 fi
fi

if [[ -n $Bkp_Loc  ]] && [[ -n $Dbserver ]]
then
 Bkp_Loc=$(print $Bkp_Loc|tr -d " ")
 Dbserver=$(print $Dbserver|tr -d " ")
 print "\n\nDatabase $sid is on $Dbserver and it's Backup Location is $Bkp_Loc .\n\n"
else
 print "\n\nYou Entered the invalid SID to check the backup which does not exist in INFPRD1 database\nAborting Here....\n"
 exit 3
fi

print "\t\t\nDo you want to copy the Recent Restore file OR for a purticular date\n\n\t\t(y/Y):\t- Recent restore files\n\t\t(p/P):\t- Purticular date\n\nPlease enter you choice: \c"
read chc
export chc1=$(print $chc|tr '[A-Z]' '[a-z]')
if [ $chc1 != 'y' -a $chc1 != 'p' ]
then
 print "\n\n\t\tPlease Execute this again and Enter your choice properly\n\nAborting Here........\n"
 exit 4
fi

if [ $chc1 == 'y' ]
then
 if [[ ! -d $Bkp_Loc ]]
 then
  print  "\n\n***Backup Location May not exist...Please check***\n"
  exit 7
 fi
 funct_restore_files
fi

if [ $chc1 == 'p' ]
then
 if [[ ! -d $Bkp_Loc ]]
 then
  print  "\n\n***Backup Location May not exist...Please check***\n"
  exit 8
 fi
 funct_restore_files_for_date
fi
}


#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi


export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PERFORM_CRON_STATUS=0
mkdir -p $REF_HOME/LOG

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
 fi
fi


 print "\n\t\tDO YOU WANT TO COPY THE RESTORE FILES FOR REFRESH\n\t\tOR\n\t\tDO YOU WANT TO COPY THE RESTORE FILES TO CREATE NEW DATABASE FROM GOLDCOPY?\n\n\t\t(r/R):\t - REFRESH\n\t\t(g/G):\t - GOLDCOPY\n\nPlease enter you choice : \c"
 read ch

if [[ -n $ch ]]
then
 export ch1=$(print $ch|tr '[A-Z]' '[a-z]')

 if [ $ch1 != 'r' -a $ch1 != 'g' ] 
 then
  print "\n\n\t\tPlease Enter your choice properly and execute it again \n\nAborting Here........"
 exit 4
 fi

 if [ $ch1 == 'r' ]
 then
  print "\nPlease Enter the Source Database name from which you are refreshing $1 : \c "
  read sid
  if [[ -n $sid ]]
  then
   funct_check_bkp_location
  else
  print "\n\n\t\tPlease Enter the Source Database name\n\nAborting Here....\n"
  exit 5
  fi
 fi

 if [ $ch1 == 'g' ]
 then
 funct_copy_restore_file_goldcopy
 fi
else
 print "\n\n\t\tPlease Enter your choice properly and Execute it Again\n\nAborting Here........\n"
exit 6
fi
