#!/bin/ksh
# ==================================================================================================
# NAME:		RestoreDatabase.sh                            
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:	This script will:
#			1. Startup the database in nomount
#			2. Restore the Control file
#           3. Restore the Datafiles and Temp files
#			4. Disable the Flashback
#			5. Open the database in OPEN RESETLOGS mode
#
# USAGE:	RestoreDatabase.sh  <ORACLE_SID>
#
#
# ==================================================================================================
funct_startdb_nomount()
{
#uncomment the below line to debug
#set -x
 print "Please Monitor RMAN log file $REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log ...\n\n"
 print "\n\nConnecting to RMAN to startup the database in nomount..."
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log<<EOF>>/dev/null
 startup nomount pfile='?/dbs/init${ORACLE_SID}.ora';
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Starting the database in nomount...Please check...\nAborting Here..."
 print "\n\n***************FAILURE***************\n\n"
exit 19
fi
}
funct_restore_ctrlfile()
{
#uncomment the below line to debug
#set -x
 restctrlfile=$REF_HOME/${ORACLE_SID}_scripts/Restore*ControlFile_${prod_sid}_${bkp_dt}_*.rcv
 export restctrlfile=$(print $restctrlfile | tr -d " ")
 print "\n\nConnecting to RMAN to Restore the Control File...Please Wait\n\n"
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log APPEND<<EOF>>/dev/null
 @$restctrlfile
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Restoring the Controlfile...Please check...\nAborting Here..."
 print "\n\n***************FAILURE***************\n\n"
exit 20
fi
}
funct_restore_datafiles()
{
#uncomment the below line to debug
#set -x
 restdatafiles=$REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}_*.rcv
 export restdatafiles=$(print $restdatafiles | tr -d " ")
 print "\n\nConnecting to RMAN to Restore the DataFiles...Please Wait until the Restoration completes\n\n"
 $ORACLE_HOME/bin/rman target=/ nocatalog log=$REF_HOME/${ORACLE_SID}_scripts/restore_${ORACLE_SID}_$cdate.log APPEND<<EOF>>/dev/null
 @$restdatafiles
EOF
if [ $? -ne 0 ]
then
 print "\n\n\There is some problem while Restoring the Datafiles...Please check...\nAborting Here..."
 print "\n\n***************FAILURE***************\n\n"
exit 21
fi
print "\n\n***Restoration Completed Successfully***\n\n"
}
funct_rename_redofiles()
{
#uncomment the below line to debug
#set -x
 renmredofiles=$REF_HOME/${ORACLE_SID}_scripts/Restore*Redo_${prod_sid}_${bkp_dt}_*.rcv
 export renmredofiles=$(print $renmredofiles | tr -d " ")
 print "\n\nConnecting to Database to Rename the Redo Files...\n\n"
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba '<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 @$renmredofiles
EOF
if [ $? -ne 0 ]
then
print "\n\n\There is some problem while Renaming the Redo Files...Please check...\nAborting Here..."
exit 22
fi
}
funct_rename_tempfile()
{
#uncomment the below line to debug
#set -x
 renmtmpfile=$REF_HOME/${ORACLE_SID}_scripts/Restore*TempFiles_${prod_sid}_${bkp_dt}_*.rcv
 export renmtmpfile=$(print $renmtmpfile | tr -d " ")
 print "\n\nConnecting to Database to Rename the Temp Files...\n\n"
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba '<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 @$renmtmpfile
EOF
if [ $? -ne 0 ]
then
print "\n\n\There is some problem while Renaming the Temp Files...Please check...\nAborting Here..."
exit 23
fi
}
funct_check_flashback()
{
#uncomment the below line to debug
#set -x
export flbk=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba '<<EOF
  WHENEVER SQLERROR EXIT FAILURE
  set feedback off
  set heading off
  select FLASHBACK_ON from v\\$DATABASE;
EOF`
 if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Checking the FLASHBACK status...Please check...\nAborting Here..."
 exit 24
 fi

 flbk=$(print $flbk | tr -d " ")
 if [ $flbk == 'YES' ]
 then
  print "\nFLASHBACK IS ON FOR $1 DATABASE"
  print "\nDo you want to disable flashback[y/n]: \c"
  read finpt1
  finpt1=$(print $finpt1 | tr -d " ")
  finpt1=$(print $finpt1 | tr [A-Z] [a-z])
  if [[ -n $finpt1 ]]
  then
   if [ $finpt1 == 'y' ]
   then
   ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba '<<EOF
   WHENEVER SQLERROR EXIT FAILURE
   ALTER DATABASE FLASHBACK OFF;
EOF
if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Disabling the FLASHBACK...Please check...\nAborting Here..."
 exit 25
fi
   fi
  fi
 else
  print "\n\nFLASHBACK IS OFF FOR $1 DATABASE"
 fi
}

#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi


export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
export cdate=$(date +'%Y%m%d')

 print "\nPlease Enter the date of backup which you are using YYYYMMDD :\c"
 read bkp_dt
 bkp_dt=$(print $bkp_dt | tr -d " ")
if [[ -n $bkp_dt ]]
then
 if [[ $bkp_dt == [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] ]]
 then
 print "\nBelow is the init.ora file which will be used for Refresh\n\n"
 export initorafile=$(print $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | tr -d " ")
 print "$initorafile\n"
 cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora
 if [ $? -ne 0 ]
 then
  print "\n\n\init.ora file seems to be missing...Please check...\nAborting Here..."
  print "\n\n***************FAILURE***************\n\n"
 exit 4
 fi
 print "${BOLD}\nIt is Strongly Recommended to Check the above parameter carefully"
 print "If any parameter needs to be modified than modify and execute again"
 print "\nDo You Want To Continue [y/n]: \c"
 read inpt1
 inpt1=$(print $inpt1 | tr '[A-Z]' '[a-z]')
 if [[ -n inpt1 ]]
 then
  if [ $inpt1 == 'y' -o $inpt1 == 'Y' ]
  then
   prod_sid=$(cat $ORACLE_HOME/dbs/init${ORACLE_SID}.ora | grep db_name | awk -F\' '{print $2}')
   prod_sid=$(print $prod_sid | tr -d " ")
   print "\nBelow RestoreControl file will be used for this Refresh\n\n"
   export ctrlfileloc=$(print $REF_HOME/${ORACLE_SID}_scripts/Restore*ControlFile_${prod_sid}_${bkp_dt}*.rcv | tr -d " ")
   print "$ctrlfileloc\n"
   cat $REF_HOME/${ORACLE_SID}_scripts/Restore*ControlFile_${prod_sid}_${bkp_dt}*.rcv
   if [ $? -ne 0 ]
   then
    print "\n\n\Restore Control file seems to be missing...Please check...\nAborting Here..."
    print "\n\n***************FAILURE***************\n\n"
   exit 5
   fi
   print "It is Strongly Recommended to verify the above file\n"
   print "If needs modifications than modify and execute again"
   print "\n\nDo You Want To Continue [y/n]: \c"
   read inpt2
   inpt2=$(print $inpt2 | tr '[A-Z]' '[a-z]')
   if [[ -n inpt2 ]]
   then
    if [ $inpt2 == 'y' -o $inpt2 == 'Y' ]
    then
     print "\nBelow RestoreDatafile will be used for this Refresh\n\n"
     export datafileloc=$(print $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}_*.rcv | tr -d " ")
     print "$datafileloc\n"
     cat $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}_*.rcv
     if [ $? -ne 0 ]
     then
       print "\n\n\Restore Data file seems to be missing...Please check...\nAborting Here..."
       print "\n\n***************FAILURE***************\n\n"
     exit 6
     fi
     print "It is Strongly Recommended to verify the above file"
     print "If needs modifications than modify and execute again"
     print "\n\nDo You Want To Continue [y/n]: \c"
     read inpt3
     inpt3=$(print $inpt3 | tr '[A-Z]' '[a-z]')
     if [[ -n inpt3 ]]
     then
      if [ $inpt3 == 'y' -o $inpt3 == 'Y' ]
      then
       grep "\.dbf'" $REF_HOME/${ORACLE_SID}_scripts/Restore*Data*_${prod_sid}_${bkp_dt}_*.rcv | awk '{print $NF}'|sed -e "s/'//g" -e 's/;//g' | sed -r 's/(.*)\/.*/\1/'|sort -u |xargs ls -d > $REF_HOME/${ORACLE_SID}_scripts/temp_dir.txt
      if [ $? -ne 0 ]
      then
       print "\n\nDirectories does not exist as per Restore10gDataTempFiles please check and Execute Again\n\nAborting Here...."
       print "\n\n***************FAILURE***************\n\n"
      exit 7
      fi
      else
       print "\n\nYou Do not want to Proceed...Thank You\n\n"
       exit 8
      fi
     else
      print "\n\nYou Did not enter your Choice Properly [y/Y]...Please execute Again...\n\n"
      print "\n\n***************FAILURE***************\n\n"
      exit 9
     fi
    else
     print "\n\nYou Do not want to Proceed...Thank You\n\n"
     exit 10
    fi
   else
    print "\n\nYou Did not enter your Choice Properly [y/Y]...Please execute Again...\n\n"
    print "\n\n***************FAILURE***************\n\n"
    exit 11
   fi
   else
    print "\n\nYou Do not want to Proceed...Thank You\n\n"
    exit 12
   fi
 else 
  print "\n\nYou Did not enter your Choice Properly [y/Y]...Please execute Again...\n\n"
  print "\n\n***************FAILURE***************\n\n"
  exit 13
 fi
 else
  print "\n\nPlease Enter the Proper Backup Date...Aborting Here...Please Execute Again...\n\n"
  print "\n\n***************FAILURE***************\n\n"
  exit 14
 fi
else
 print "\n\nPlease Enter the Proper Backup Date...Aborting Here...Please Execute Again...\n\n"
 print "\n\n***************FAILURE***************\n\n"
 exit 15
fi

 funct_startdb_nomount
 funct_restore_ctrlfile
 funct_check_flashback
 funct_restore_datafiles
 print "\n\nDo you want Rename Redolog Files[y/n]: \c"
 read a
 if [ $a == 'y' -o $a == 'Y' ]
 then
 funct_rename_redofiles
 else
 print "\n You selected not to rename the Redologs"
 fi
 
 ${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba '<<EOF>$REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
  WHENEVER SQLERROR EXIT FAILURE
  set feedback off
  set heading off
  select file#,name,status from v\$datafile where status in('RECOVER','OFFLINE');
EOF
 if [ $? -ne 0 ]
 then
  print "\n\n\There is some problem while Checking the Datafiles in RECOVER or OFFLINE Mode...Please check...\nAborting Here..."
 exit 16
 fi
 if [[ -s $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt ]]
 then
  print "\n\nThere are some file in RECOVER or OFFLINE Mode\n"
  cat $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
  rm -f $REF_HOME/${ORACLE_SID}_scripts/recr_off.txt
 exit 16
 fi

 print "\n\nDo you want to Open the Database in OPEN RESETLOGS Mode ?[y/n]: \c"
 read inpt4
 inpt4=$(print $inpt4 | tr -d " ")
 inpt4=$(print $inpt4 | tr '[A-Z]' '[a-z]')
 if [ $inpt4 == 'y' -o $inpt4 == 'Y' ]
 then

##################Commented to directly disable the block change tracking as suggested by Abhi Chowdhary######################
#  bct_status=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba '<<EOF
#  WHENEVER SQLERROR EXIT FAILURE
#  set feedback off
#  set heading off
#  select status FROM v\\$block_change_tracking;
#EOF`
# if [ $? -ne 0 ]
# then
#  print "\n\n\There is some problem while Checking the status of Block Change Tracking...Please check...\nAborting Here..."
# exit 16
# fi
# export bct_status=$(print $bct_status | tr -d " ")
# if [ $bct_status == 'ENABLED' ]
# then
  $ORACLE_HOME/bin/sqlplus -s '/ as sysdba '<<EOF
  WHENEVER SQLERROR EXIT FAILURE
  ALTER DATABASE DISABLE BLOCK CHANGE TRACKING;
EOF
   if [ $? -ne 0 ]
   then
    print "\n\n\There is some problem while Disabling the status of Block Change Tracking...Please check...\nAborting Here..."
   exit 17
   fi
# fi
##################################################################################################################################


 print "\nOpening the Database in OPEN RESETLOGS...Please Wait...\n\n"
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba '<<EOF
  WHENEVER SQLERROR EXIT FAILURE
  alter database open resetlogs;
EOF
  if [ $? -ne 0 ]
  then
   print "\n\n\There is some problem while Opening the Database in OPEN RESETLOGS Mode...Please check...\nAborting Here..."
  exit 18
  fi
  print "\n\nDid you restore a 9i Database ? [y/n]: \c"
  read ch
  export ch=$(print $ch | tr -d " ")
   if [ $ch == 'y' -o $ch == 'Y' ]
   then
    funct_rename_tempfile
   fi
 else
  print "\n\nYou Selected Not to Open the Database in OPEN RESETLOGS\n"
 fi

 print "\n\n*******************************SUCCESSFULL******************************************\n\n"
exit 0
