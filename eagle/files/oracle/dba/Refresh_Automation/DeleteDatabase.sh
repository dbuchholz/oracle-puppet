#!/bin/ksh
# ==================================================================================================
# NAME:		DeleteDatabase.sh
# 
# AUTHOR:	Basit Khan                                    
#
# PURPOSE:  This script will create Pfile for backup, delete_database_files.sh, 
#			Shutdown the database and Delete the Dump directories and archive location
#			
#
# USAGE:	DeleteDatabase.sh <ORACLE_SID>
#
#
# ==================================================================================================
#################### MAIN ##########################
#uncomment the below line to debug
#set -x
clear

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on this host  which is being refreshed\n"
   exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 2
fi

export PAR_HOME=$HOME/local/dba
export REF_HOME=$PAR_HOME/Refresh_Automation
mkdir -p $REF_HOME/${ORACLE_SID}_scripts
 
 print "\n\n\t\tChecking for Application Connections...Please Wait\n\n"
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$PAR_HOME/session_${ORACLE_SID}_temp.txt
 WHENEVER SQLERROR EXIT FAILURE
 set heading off
 set feedback off
 select username,count(*) from v\$session
 where TYPE!='BACKGROUND'
 and username not in('SYS','SYSTEM','DBSNMP')
 group by username;
EOF
if [ $? -ne 0 ]
then
 print "\n\n\t\t There is some issue while checking the application connections...Please check\n\nAborting Here...."
 exit 3
fi

if [[ -s $PAR_HOME/session_${ORACLE_SID}_temp.txt ]]
then
 print "\n\n***Below Application Session Are Still Connected.Please ask Team Install To bring Them Down***"
 cat $PAR_HOME/session_${ORACLE_SID}_temp.txt
 rm -f $PAR_HOME/session_${ORACLE_SID}_temp.txt
exit 4
fi

 $ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 prompt Creating PFILE from SPFILE...
 create pfile from spfile;
 prompt Creating delete_database_files.sh to Delete the Database...
 set pagesize 30000  linesize 132  heading off  feedback off
 spool $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
 select 'rm ' || name from v\$datafile
 UNION
 select 'rm ' || name from v\$controlfile
 UNION
 select 'rm ' || name from v\$tempfile
 UNION
 select 'rm ' || member name from v\$logfile;
 spool off
EOF
if [ $? -ne 0 ]
then
 print "\n\n\t\tThere is a problem while creating delete_database_files.sh...Please check\n\nAborting Here...."
 "\n\n\t\t*************************************** FAILURE *************************************"
 exit 5
fi
if [[ -s $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh ]]
then
 chmod 755 $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
else
 "\n\n\t\tdelete_database_files.sh no created.....Please check"
 "\n\n\t\t*************************************** FAILURE *************************************"
fi
 $ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF>$REF_HOME/${ORACLE_SID}_scripts/delete_location.txt
 WHENEVER SQLERROR EXIT FAILURE
 set pagesize 30000  linesize 132  heading off  feedback off
 select VALUE from V\$parameter where NAME in('background_dump_dest','core_dump_dest','user_dump_dest','audit_file_dest','log_archive_dest_1');
EOF
if [ $? -ne 0 ]
then
 print "\n\n\t\t There is a problem while fetching Dump and Archive location...Please check\n\nAborting Here...."
 print "\n\n\t\t*************************************** FAILURE *************************************"
 exit 6
fi
 print "\n\nDo you want to Continue to Delete the Database [y/n]: \c"
 read inpti
 if [[ -n $inpti ]]
 then
  if [ $inpti == 'y' -o $inpti == 'Y' ]
  then
   print "\n\nBringing Down the Database...Please Wait\n\n"
   $ORACLE_HOME/bin/sqlplus -s '/as sysdba'<<EOF
   WHENEVER SQLERROR EXIT FAILURE
   shutdown immediate
EOF
  if [ $? -ne 0 ]
  then
   print "\n\n\t\tThere is some Problem while Deleting the database...Please check\n\nAborting Here...."
   print "\n\n\t\t*************************************** FAILURE *************************************"
  exit 7
  fi
 print "\n\nDeleting the Database files from OS Level...Please Wait\n\n"
. $REF_HOME/${ORACLE_SID}_scripts/delete_database_files.sh
 print "\n\nDeleting the DUMP and Archive files from OS Level...Please Wait\n\n"
 cat $REF_HOME/${ORACLE_SID}_scripts/delete_location.txt | sed -e '/^$/d' | sed 's/LOCATION=//g' > $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt
 rm -f $REF_HOME/${ORACLE_SID}_scripts/delete_location.txt
 if [[ -s $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt ]]
 then
  cat $REF_HOME/${ORACLE_SID}_scripts/delete_location_temp.txt | while read LINE
   do
   rm -fr $LINE
   if [ $? -ne 0 ]
   then
   print "\nProblem while removing $LINE ... Please Remove $LINE Manually..."
   print "Please Delete and Create (adump, bdump, udump, cdump, etc.) the administrator directories under $HOME/admin/xxxxxx manually"
   fi
   mkdir -p $LINE
   if [ $? -ne 0 ]
   then
   print "\nProblem while Creating $LINE ... Please Create $LINE Manually..."
   print "Please Delete and Create (adump, bdump, udump, cdump, etc.) the administrator directories under $HOME/admin/xxxxxx manually"
   fi
   done
 fi
  else
   print "\n\n\t\tYou Selected not to proceed....Thank You"
   print "\n\n\t\t************ Aborting Here *************\n\n"
  exit
  fi
  else
  print "\n\n\t\t***** Incorrect Input *****"
   print "\n\n\t\t**** FAILURE *************\n\n"
   exit
  fi
print "\n\n******************************************SUCCESSFULL********************************\n\n"
 exit 0
