#!/bin/ksh
# ==================================================================================================
# NAME:		ASM_CheckAlert.sh                                 
# 
# AUTHOR:	Maureen Buotte                                    
#
# PURPOSE:	This utility will monitors the Alert Logs for all databases and sends 
#			mail when problems are detected. This script will read an exception file CheckAlert.exc
#           where a list of errors are contained that can be ignored.  Enter one error per line.
#
# USAGE:	CheckAlert.sh 
#
# Frank Davis	05/16/2015  Added compatibility with ASM Managed Databases
# Frank Davis	09/30/2013  Removed team_dba@eagleaccess.com from email
# Frank Davis	09/24/2013  Removed tech_support@eagleaccess.com from email
# Frank Davis	06/14/2013  Removed curly brackets from AlertLogFile= to make the script the same as on Solaris.
# Frank Davis	06/13/2013  Updated Service Now Values
# Frank Davis	06/11/2013  Moved Service Now Category, Subcategory, Impact and Urgency below Program Name and Machine.
# Maureen Buotte	05/01/2013  Change email notifications for use with Service Now
# Frank Davis  04/16/2012  In IPCheck, change 10.60.5. to (10.60.5. or 10.60.5X.) and changed 10.60.3. to (10.60.3. or 10.60.3X.)
# Frank Davis  04/09/2012  In funct_initial_audit_update, changed check to look for database listed in infprd1 to distinguish between really not there or a connectivity issue to infprd1.
# Frank Davis  04/05/2012  Change grep to "grep -a" as in "grep -a ORA-".  The alert log can exist as a binary file and is seen during a refresh.
# Frank Davis  03/26/2012  Added a sed exclusion file CheckAlert.exc instead of including the exclusions in this script.
# Frank Davis  10/14/2011  Added exclusion for ORA-16401 archive log rejected by RFS.  This can be ignored as VerifyDataguard.sh will alert if a problem.
# Frank Davis  10/07/2011  Made compatible with Oracle 11g.  Alert log for Oracle 11g is located in a different location.
# Frank Davis  07/01/2011  Added exclusion to ignore errors ORA-1547, ORA-01013, ORA-12170, ORA-27090, ORA-279
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis           24-Jun-2011  Modified IPCheck Variable for updated list of VLANs
# Nishigandha Sathe     02-Mar02010  Modified to use new primary key machines(instance)
# Maureen Buotte   	30-Sep-2009  Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte    	05-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    	01-Jun-2008  Added reporting to DBA database
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

 # Uncomment for debug
 set -x

 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat

 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail.dat
  rm -f $ERROR_FILE
 fi

 cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
 rm -f $WORKING_DIR/mail.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1

 return 0
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

 # Uncomment next line for debugging
 set -x

 PERFORM_CRON_STATUS=`$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off feedback off
        select 1 from dual;
EOF
`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [[ ${PERFORM_CRON_STATUS} != 1 ]]
 then
  PERFORM_CRON_STATUS=0
 fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {

 # Uncomment for debug
 set -x

 $ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID}<<EOF>/dev/null
 set pagesize 0 echo off trimspool on
 WHENEVER SQLERROR EXIT FAILURE
 spool $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt
 select instance from databases where sid='$ORACLE_SID' and mac_instance = (select instance from machines where lower(name)='$BOX');
EOF
 grep 'no rows selected' $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt > /dev/null
 if [ $? -eq 0 ]
 then
  rm -f $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt
  IMPACT=2
  URGENCY=3
  SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated"
  PERFORM_CRON_STATUS=0
  return 1
 fi
 DB_INSTANCE=$(sed -e '/^$/d' -e 's/ //g' $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt)
 rm -f $WORKING_DIR/${ORACLE_SID}_does_database_exist_in_big_brother.txt

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ x$Sequence_Number = 'x' ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000 heading off feedback off
  declare i number;
  begin
   insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
 return 0
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

 # Uncomment for debug
 set -x

 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;

 update INSTANCE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from instance_cron_job_runs where instance=${Sequence_Number})
 where instance=${Sequence_Number};
 commit;
/
EOF
`

 print "$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS"
}

# **********************************************************************************
# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
set -x

export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export DB_INSTANCE 

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 NAWK=awk
 BINARY_GREP="grep -a"
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 NAWK=nawk
 BINARY_GREP=grep
fi

# Get environment variables set up - need to do this differently for RHEL 5
dummy_sid=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORACLE_SID=$dummy_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH:/usr/local/scripts/dba/perllib
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/monitor
else
 export WORKING_DIR="$HOME/local/dba/monitor"
fi

export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt


# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/grid/CRONLOG ]]
 then
  mkdir -p /home/grid/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

ps -ef | grep asm_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}' > $DATABASES_FILE

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep  "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(grep  "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

HOSTNAME=$(hostname)
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
IPCheck=$(grep $HOSTNAME /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=1
 export URGENCY=1
else
 export TYPE=TEST
 export IMPACT=2
 export URGENCY=3
fi
export MAILTO='eaglecsi@eagleaccess.com'

NumericDate=$(date +%Y%m%d)

# Loop through the list of Oracle instances to run against
cat $DATABASES_FILE | while read LINE
do
 Sequence_Number=0
 ORA_SID=$LINE
 if [[ -z $ORA_SID ]]
 then
  continue
 fi

 export MAIL_COUNT=0

 ERROR_FILE=$WORKING_DIR/ErrorFile_$ORA_SID
 rm -f $ERROR_FILE

 export ORACLE_SID=$ORA_SID
 sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
 if [[ -z $sid_in_oratab ]]
 then
  print "Invalid SID -> $ORA_SID"
  print "There is no $ORACLE_SID entry in $ORATAB file"
  IMPACT=2
  URGENCY=3
  SendNotification "Invalid SID -> $ORA_SID"
  continue
 fi
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH
 . /usr/local/bin/oraenv > /dev/null
 unset ORA_NLS33

 # Make sure this database is not in list of SIDs that nothing should be run against
 EXCEPTION=$($NAWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
 if [[ $EXCEPTION != "0" ]]
 then
  continue
 fi

 # Make sure this database is not in list of SIDs that this job should not be run for
 EXCEPTION=$($NAWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
 if [[ $EXCEPTION != "0" ]]
 then
  continue
 fi

 # Add entry into DBA Admin database
# if [[ $PERFORM_CRON_STATUS = 1 ]]
# then
#  funct_initial_audit_update
#  if [[ $? -ne 0 ]]
#  then
#   PERFORM_CRON_STATUS=0
#  fi
#  export DB_INSTANCE
# fi

 # Initialization
 Errors=0
 $ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<EOF>/dev/null
 set pagesize 0 heading off feedback off trimspool on
 spool $WORKING_DIR/background_dump_dest_$ORACLE_SID.txt
 select value from v\$parameter where name='background_dump_dest';
EOF
 bdump_dest=$(cat $WORKING_DIR/background_dump_dest_$ORACLE_SID.txt)
 AlertLogFile=$bdump_dest/alert_$ORACLE_SID
 rm -f $WORKING_DIR/ver_db_$ORACLE_SID.txt $WORKING_DIR/diag_dest_$ORACLE_SID.txt $WORKING_DIR/background_dump_dest_$ORACLE_SID.txt

 ExceptionErrorFile=$WORKING_DIR/CheckAlert.exc
 if [[ ! -f $ExceptionErrorFile ]] || [[ ! -s $ExceptionErrorFile ]]
 then
  touch $ExceptionErrorFile
  print "\n\nThe error exceptions file for the alert log check is missing."
  print "An empty file was created so the check can be run."
  print "The result will be that errors can be reported that have been determined to be non-significant."
  print "Please copy the $ExceptionErrorFile Error Exception File to this host: $(hostname | awk -F. '{print $1}')"
  print "The Error Exception File must be copied to the same directory as the script: $0\n\n"
  IMPACT=2
  URGENCY=3
  SendNotification "No Error File: $ExceptionErrorFile on $BOX for $0\n"
 fi
 if [[ -s ${AlertLogFile}.log ]]
 then
  $BINARY_GREP "ORA-" ${AlertLogFile}.log | sed '/ORA-12012/{N;N;N;N;d;}' | sort -u | egrep -vwf $ExceptionErrorFile > $ERROR_FILE
  if [ $? -eq 0 ]
  then
   Errors=1
  fi
 fi

 if [[ -s ${AlertLogFile}.log ]]
 then
  cat ${AlertLogFile}.log >> ${AlertLogFile}.$NumericDate
  rm ${AlertLogFile}.log
  touch ${AlertLogFile}.log
 fi

 # Send errors to DBA
 if [ $Errors -eq 1 ]
 then
  SendNotification "Errors found in Alert log for ${ORACLE_SID}\n"
 fi

 # Update Infrastructure database
# if [ $PERFORM_CRON_STATUS = 1 ]
# then
#  if [ $MAIL_COUNT -gt 0 ]
#  then
#   PROCESS_STATUS='WARNING'
#  else
#   PROCESS_STATUS='SUCCESS'
#  fi
#   funct_final_audit_update
# fi

 # Purge old alerts -> keep 60 days
 KEEPDEPTH=60
 bname=$(dirname ${AlertLogFile}.log)
 find $bname -name "alert_*" -mtime +$KEEPDEPTH -exec rm -f {} \;

done

exit 0
