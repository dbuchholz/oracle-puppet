#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
ps -ef | grep ora_smon_${ORACLE_SID} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/tmp/$$_size_of_db_${ORACLE_SID}.txt
set pages 0 trimspool on feedback off echo off
select ceil(sum(bytes)/1024/1024/1024) from dba_data_files union
select ceil(sum(bytes)/1024/1024/1024) from dba_temp_files union
select ceil(sum(bytes)/1024/1024/1024) from v\$log;
EOF
size_of_db=$(cat /tmp/$$_size_of_db_${ORACLE_SID}.txt | awk '{sum+=$1} END {printf "%6d\n",sum}')
print "$ORACLE_SID: $size_of_db GB"
rm -f /tmp/$$_size_of_db_${ORACLE_SID}.txt
fi

exit 0
