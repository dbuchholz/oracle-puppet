#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 04/26/2011																							 #
#Usage : ReportServiceAccounts.sh																     		 #
#Purpose: This report generate monthly Reports from all the Service Accounts on all databases.               #
#         															                                         #
##############################################################################################################
#set -x
. /usr/bin/setsid.sh infprd1

EMAIL=Y


##### Get a list of SIDs #######

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance
 and d.sid not like '%mfc%' and d.dataguard='N' and d.status='OPEN' and substr(d.oracle_version,1,2)='10'
group by d.sid order by 1;
exit
!`


REPORT_DIR=/u01/app/oracle/local/dba/Auditing/reports/serviceaccounts

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi

RESULT=/u01/app/oracle/local/dba/Auditing/reports/serviceaccounts/ServiceAccounts_`date +\%d_\%m_\%Y`.txt
rm ${RESULT}
touch ${RESULT}

for line in `echo ${Sid_List}`; do
#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance 
and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' and d.dataguard='N' and d.status='OPEN'
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`

#### Printing the Report here #######

$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!
spool ${RESULT} append
set trimspool on
set serveroutput on
set linesize 220
set pages 200
set head on
set feedback off
set verify off
set echo off
SET TERMOUT OFF
col curdate for a15

break on instance_name

select v.instance_name, p.GRANTEE as "SERVICE ACCOUNT" from dba_role_privs p, v\$instance v where GRANTED_ROLE in ('READONLY','READ_ONLY') and grantee not in ('SYS','SYSTEM')
    and grantee in (select distinct owner from dba_objects)
and grantee in (select username from dba_users where (profile='DEFAULT' or profile='EA_PROFILE'));
spool off
exit
!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
echo Finished writing report ${SHORT_NAME}

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 37 ]]
 then
if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
to=nsarri@eagleaccess.com
subject="SERVICE ACCOUNT REPORT. Best viewed with WordPad....."
Message="Hi COM's,
                Here is the report of Service accounts for review ...

Thanks

Nirmal S Arri"
#mailx -s "$subject" $to < ${RESULT}
(echo "Hi COM's,
         Here is the report of Service accounts for review ...

Thanks

Nirmal S Arri";/usr/bin/uuencode ${RESULT} ${SHORT_NAME}) | /bin/mailx -s "${subject}" ${to} 

echo "Emailed report"
fi
fi


/usr/bin/find /u01/app/oracle/local/dba/Auditing/reports/serviceaccounts/ServiceAccounts_* -type f -mtime +160 -exec rm -f {} \;

exit 0

