##############################################################################
#              CREATING FOSG USERS IN FOSG-CLIENT DATABASES                  #
##############################################################################
# Purpose: To have a special schema EA_FOSG in all the FOGS clients TEST     #
#          databases where developers and analysts can proxy to and work     #
#          independent without disturbing the existing code.                 #
#                                                                            #
#          On development evironments apart from EA_FOSG developers and      #
#          analysts can log to application schemas as a proxy.               #
##############################################################################


There are two run scripts:

1. Create_fosg_TEST_env_users.sh
2. Create_fosg_DEV_PROXY_users.sh


1. Create_fosg_TEST_env_users.sh(This script can be run for DEV,TEST or PROD for all FOSG databases ONLY):

	This script takes two input files. List of users that needs to be created and list of databases that 
	it needs to implemented in.

	The script takes the input and for each database, creates the user if it does not already exists, 
	listed in the users file with a random password, and emails it to the user. In addition, 
	it creates EA_FOSG schema if it does not already exists, with required privileges and grants proxy to the user.

2. Create_fosg_DEV_PROXY_users.sh(This script should be run ONLY on development databases):

	This script also takes two input files. List of users that needs to be created and list of development databases that
	it needs to implemented in.

	The script takes the input and for each database, creates the user if it does not already exists, 
	listed in the users file with a random password, and emails it to the user. In addition, 
	it grants proxy to the user to all the application users with profile as EA_APPLICATION.


