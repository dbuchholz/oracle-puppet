#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/28/2014                                                                 #
# Usage: Remove_fosg_users.sh <db_list.txt> <user_list.txt>                        #
# Program: This script DROPS FOSG users that are in the list.                      #
####################################################################################

CreateDBAUser() {
#set -x

while read db_user
do

IFS=' ' read db_user email <<< ${db_user}
#echo ${db_user}
#echo ${email}
#echo ${pwd}
fosg_sid=`echo ${db_instance} |awk '{print substr($0,1,3)}'`

#echo ${fosg_sid}
fosg_sid=fosg_${fosg_sid}
#echo ${fosg_sid}

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${db_instance} as sysdba" <<!
set serveroutput on
set pagesize 0 feedback off verify off heading off echo off
SET SERVEROUTPUT ON SIZE UNLIMITED;
WHENEVER SQLERROR EXIT FAILURE;
DECLARE
   cmd varchar2(500);
	err_num NUMBER;
    err_msg VARCHAR2(100);
	cnt number;
        fosg_cnt number;
	sql_stmt varchar2(1000);
BEGIN
  select count(*) into cnt from dba_users where username = upper('${db_user}');
  if cnt > 0 then
  sql_stmt := 'drop user ${db_user} cascade';
  execute immediate sql_stmt;
dbms_output.put_line('Dropped Username is: ${db_user} in ${db_instance}');
 else
dbms_output.put_line('USERNAME: ${db_user} Does not exists in ${db_instance}');
  end if;
EXCEPTION
 WHEN OTHERS THEN 
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM, 1, 100);
END;
/
exit
!


if [ $? -ne 0 ]; then
 echo "Problem dropping user in ${ORA_SID} ....." 
 exit 1
fi

done < ${user_file} 

}


######## MAIN #######
#set -x

read db_file?"Enter full path with DB file name: "
if [ -f ${db_file} ] 
  then
  if [[ $(tr -d "\r\n" < ${db_file}|wc -c) -eq 0 ]]
    then
     echo "${db_file} is empty.."
     exit 1
  fi
   else
  echo "This File does not exist. Please enter full path and the db file name..."
exit 1
fi

read user_file?"Enter full path with user file name:"
if [ -f ${user_file} ]
  then  
  if [[ $(tr -d "\r\n" < ${user_file}|wc -c) -eq 0 ]]
   then
    echo "${user_file} is empty..."
     exit 2
  fi
   else
  echo "This File does not exist. Please enter full path and the db file name..."
exit 2
fi

clear

### Create users in the listed databases #####

while read db_instance
do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0

select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance
and u.db_instance=d.instance and u.username='SYS' and lower(d.tnsname)='${db_instance}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;

exit
!`

CreateDBAUser
done < ${db_file}

exit 0 
