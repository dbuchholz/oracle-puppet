#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/28/2014                                                                 #
# Usage: Create_fosg_env_users.sh <db_list.txt> <user_list.txt>                    #
# Program: This script sets up the FOSG environment and create users with          #
#          required privileges.                                                    #	
####################################################################################

GetPass() {
#set -x
MAXSIZE=8
array1=( 
q w e r t y u i o p a s d f g h j k l z x c v b n m Q W E R T Y U I O P A S D 
F G H J K L Z X C V B N M 1 2 3 4 5 6 7 8 9 0 \_ 
) 
unset res
i=0
MODNUM=${#array1[*]} 
pwd_len=0 
while [ $pwd_len -lt $MAXSIZE ] 
do 
    index=$(($RANDOM%$MODNUM)) 
str[i]=${array1[$index]} 
    ((pwd_len++)) 
    ((i++))
done 
for j in "${str[@]}"; do
res=${res}$j
done
}

CreateDBAUser() {
#set -x

while read db_user
do

j=0
until (test $j -eq 1)
do
GetPass
echo ${res} |grep [0-9] > /dev/null
if [[ $? -eq 0 ]] ; then
 j=1
fi
done

pwd=${res}

IFS=' ' read db_user email <<< ${db_user}
#echo ${db_user}
#echo ${email}
#echo ${pwd}
fosg_sid=`echo ${db_instance} |awk '{print substr($0,1,3)}'`

#echo ${fosg_sid}
fosg_sid=fosg_${fosg_sid}
#echo ${fosg_sid}

v_code=`$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${db_instance} as sysdba" <<!
set pagesize 0 feedback off verify off heading off echo off
VARIABLE return_code number;
WHENEVER SQLERROR EXIT FAILURE;
--SET SERVEROUTPUT ON SIZE UNLIMITED;
--SET FEEDBACK OFF;
DECLARE
   cmd varchar2(500);
	err_num NUMBER;
    err_msg VARCHAR2(100);
	cnt number;
        fosg_cnt number;
	sql_stmt varchar2(1000);
BEGIN
  select count(*) into fosg_cnt from dba_users where username = 'EA_FOSG';
  if fosg_cnt = 0 then
  sql_stmt := 'create user EA_FOSG PROFILE ea_service IDENTIFIED BY EA_FOSG_001 DEFAULT TABLESPACE users QUOTA UNLIMITED ON users TEMPORARY TABLESPACE  TEMP ACCOUNT UNLOCK';
  execute immediate sql_stmt;
   execute immediate 'GRANT connect, create session TO EA_FOSG';
   execute immediate 'GRANT readonly TO EA_FOSG';
   execute immediate 'GRANT RESOURCE TO EA_FOSG';
   execute immediate 'GRANT DEBUG CONNECT SESSION TO EA_FOSG';
   execute immediate 'GRANT SELECT ANY TABLE TO EA_FOSG';
  end if;

  select count(*) into cnt from dba_users where username = upper('${db_user}');
  --dbms_output.put_line('The name is ${db_user}');
  if cnt = 0 then
  sql_stmt := 'create user ${db_user} PROFILE ea_profile IDENTIFIED BY ${db_user}_${pwd} PASSWORD EXPIRE DEFAULT TABLESPACE users QUOTA UNLIMITED ON users TEMPORARY TABLESPACE  TEMP ACCOUNT UNLOCK';
  --dbms_output.put_line(sql_stmt);
   execute immediate sql_stmt;
   execute immediate 'GRANT connect, create session TO ${db_user}';
   execute immediate 'GRANT readonly TO ${db_user}';
   execute immediate 'audit all by ${db_user} by access';
   execute immediate 'audit update table, insert table, delete table, execute procedure by ${db_user} by access';
   execute immediate 'alter user EA_FOSG grant connect through ${db_user}';
--   dbms_output.put_line('Please send this username and password to the requester...');
--   dbms_output.put_line('USERNAME: ${db_user}');
--   dbms_output.put_line('PASSWORD: ${db_user}_${pwd}');
--cmd := '/dshome/xb3ym0k/local/dba/Auditing/FOSG/email_fosg.sh ${email} ${db_user} ${db_instance} ${db_user}_${pwd}';
--host(cmd);
:return_code := 5;
 else
:return_code := 6;
--  dbms_output.put_line('User ${db_user} already exists within the database.....');
  execute immediate 'alter user EA_FOSG grant connect through ${db_user}';
  end if;
EXCEPTION
 WHEN OTHERS THEN 
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM, 1, 100);
--  dbms_output.put_line('Error Code: '||err_num);
--  dbms_output.put_line('Error Message: '||err_msg);
--  dbms_output.put_line('There was an error with the Password. The Generated password is: ${db_user}_${pwd}');
END;
/
print return_code;
exit
!`


if [ $? -ne 0 ]; then
 echo "Problem creating user in ${ORA_SID} ....." 
 exit 1
fi

if [ ${v_code} -eq 5 ]; then
to=${email}
subject="Your password as an FOSG user ${db_user} for database ${db_instance} is ${db_user}_${pwd}"
Message="Your password as an FOSG user ${db_user} for database ${db_instance} is ${db_user}_${pwd}"
echo "${Message}"|mailx -s "$subject" $to 
echo "Password SENT to ${db_user} for database ${db_instance} is ${db_user}_${pwd}"
else
echo "Password NOT Change for ${db_user} for database ${db_instance}"
fi

done < ${user_file} 

}


######## MAIN #######
#set -x

read db_file?"Enter full path with DB file name: "
if [ -f ${db_file} ] 
  then
  if [[ $(tr -d "\r\n" < ${db_file}|wc -c) -eq 0 ]]
    then
     echo "${db_file} is empty.."
     exit 1
  fi
   else
  echo "This File does not exist. Please enter full path and the db file name..."
exit 1
fi

read user_file?"Enter full path with user file name:"
if [ -f ${user_file} ]
  then  
  if [[ $(tr -d "\r\n" < ${user_file}|wc -c) -eq 0 ]]
   then
    echo "${user_file} is empty..."
     exit 2
  fi
   else
  echo "This File does not exist. Please enter full path and the db file name..."
exit 2
fi

clear

### Create users in the listed databases #####

while read db_instance
do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0

select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance
and u.db_instance=d.instance and u.username='SYS' and lower(d.tnsname)='${db_instance}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;

exit
!`

CreateDBAUser
done < ${db_file}

exit 0 
