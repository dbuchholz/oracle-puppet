#!/bin/ksh
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/05/2011                                                                 #
# Usage: AuditReport.sh                                                            #
# Program: This script Reports the Oracle Audit Information on a given database    #
####################################################################################
#set -x

NARG=$#
ORA_SID=$1

## Checking for input parameter

if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./AuditReport.sh <SID>"
        echo ""
        echo "Your <SID> options are:"
   for sid in `ps -ef | grep ora_pmon_ | awk '{print $8}'`
    do
     s_sid=`expr substr $sid 10 10`
     echo "     "$s_sid
    done
     echo ""
        exit 1
fi

. /usr/bin/setsid.sh ${ORA_SID}

export ORACLE_SID=${ORA_SID}
REPORT_DIR=/u01/app/oracle/local/dba/Auditing/reports
if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi
RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_WEEKLY_${ORACLE_SID}_`date +\%d_\%m_\%Y`.txt
EMAIL=Y

STATUS=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!


spool ${RESULT}
set trimspool on
set serveroutput on 
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col curdate for a15
SELECT TO_CHAR(SYSDATE,'dd-Mon-yyyy') curdate FROM DUAL;
COLUMN curdate NEW_VALUE report_date
SET TERMOUT ON


ttitle center "FAILED LOGON ATTEMPTS" skip 2

col terminal for a25
col username for a15
col DATE for a15
select count(*) as "No. of Attempts" ,username,terminal,to_char(timestamp,'DD-MON-YYYY') as "DATE"
from dba_audit_session
where returncode<>0 and timestamp >= sysdate-7
group by username,terminal,to_char(timestamp,'DD-MON-YYYY') order by to_char(timestamp,'DD-MON-YYYY') desc
/

ttitle skip 2
ttitle center "ATTEMPTS TO ACCESS DATABASE WITH NON EXISTANT USERS" skip 2

select username,terminal,to_char(timestamp,'DD-MON-YYYY HH24:MI:SS')
from dba_audit_session
where returncode<>0 and timestamp >= sysdate-7
and not exists (select 'x'
   from dba_users
   where dba_users.username=dba_audit_session.username)
/

ttitle center "USERS SHARING DATABASE LOGONS" skip 2

select count(distinct(terminal)),username
from dba_audit_session where timestamp >= sysdate-7
having count(distinct(terminal))>1
group by username
/

--
--PROMPT    -- Check for multiple database accounts being used from one terminal. This could indicate
--PROMPT    -- wrong doing as each terminal should in theory be used by one person and one account?
--
ttitle center "MULTIPLE DATABASE ACCOUNTS BEING USED FROM ONE TERMINAL" skip 2

select count(distinct(username)),terminal
from dba_audit_session where timestamp >= sysdate-7
having count(distinct(username))>1
group by terminal
/

--
--PROMPT    -- Check the audit trail for any changes being made to the structure of the database schema.
--
ttitle center "CHANGES MADE TO THE STRUCTURE OF THE DATABASE SCHEMA" skip 2

col username for a20
col priv_used for a36
col obj_name for a30
col timestamp for a17
col returncode for 9999
select  username,
        priv_used,
        obj_name,
        to_char(timestamp,'DD-MON-YYYY HH24:MI') timestamp,
        returncode
from dba_audit_trail
where priv_used is not null
and priv_used<>'CREATE SESSION' and timestamp >= sysdate-7
/

ttitle center "FAILED LOGIN ATTEMPTS USING DBSNMP" skip 2

col userhost format a35
col terminal format a35
col os_username format a25

select userhost, terminal, os_username, count(1)
from dba_audit_trail
where returncode = 1017
and username = 'DBSNMP' and timestamp >= sysdate-7
group by userhost, terminal, os_username
/

ttitle center "CHECK IF DATABASE HAS BEEN COMPROMISED" skip 2

col type_name format a30
col name format a35
col action format a35

select *  from JAVA\\$POLICY\\$ where grantee# in (select grantee# from sys.JAVA\\$POLICY\\$ minus select user# from sys.user\\$)
/

ttitle center "USER ACTIVITY FOR LAST 7 DAYS" skip 2

col os_username format a20
col userhost format a30
col owner for a15
col username for a15
col terminal format a20
col obj_name format a20
col sql_text format a35 word wrap
col LogoffTime for a15
select username, to_char(timestamp,'MM/DD/YYYY HH24:MM:SS') as "TimeStamp",
    owner, obj_name, action_name, sql_text,
    to_char(logoff_time,'MM/DD/YYYY HH24:MM:SS') as "LogoffTime"
  from
  dba_audit_trail where username not in ('ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS','HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS','ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS','QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','SYS','SYSTEM','ESTAR','PACE_MASTERDBO','DATAMARTDBO', 'DATAEXCHDBO', 'SCRUBDBO', 'PRICING', 'LEDGERDBO', 'SECURITYDBO', 'TRADESDBO', 'PERFORMDBO', 'HOLDINGDBO', 'CASHDBO', 'RULESDBO', 'EAGLEKB', 'ARCH_CTRLCTR', 'CTRLCTR', 'MSGCENTER_DBO', 'EAGLEMGR') and timestamp >= sysdate-7 order by 1,2,7
/

spool off

exit
!`

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
echo Finished writing report ${SHORT_NAME}
 
if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
 then
if [ "$EMAIL" = "Y" ]
then
to=team_dba@eagleaccess.com
#to=narri@eagleinvsys.com
subject="Audit Exception Report for ${ORACLE_SID}. Best viewed with WordPad....."
Message="Auditing report for ${ORACLE_SID} is ready to review..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi
fi
/usr/bin/find /u01/app/oracle/local/dba/Auditing/reports/Audit_* -type f -mtime +160 -exec rm -f {} \;

exit 0
