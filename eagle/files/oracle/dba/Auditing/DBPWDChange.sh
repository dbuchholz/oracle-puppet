#!/bin/ksh

. /usr/bin/setsid.sh $1

USR=$2
PWD=$3
$ORACLE_HOME/bin/sqlplus "/ as sysdba" <<!
WHENEVER SQLERROR EXIT FAILURE
alter user ${USR} identified by "${PWD}";
!
if [ $? -ne 0 ]; then
 exit 1
fi
exit 0
