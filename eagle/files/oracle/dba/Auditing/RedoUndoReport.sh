#!/bin/ksh
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/15/2011                                                                 #
# Usage: RedoUndoReport.sh                                                         #
# Program: This script Reports on all the databases which has RedoUndo implemented #
####################################################################################
#set -x

function Chk_Parameters {

#if [ ${NARG} -ne 1 ]; then
        #clear
        #echo ""
        #echo "Not enough parameter..."
        #echo ""
        #echo "Usage: ./preserve_user_info.sh <SidFileName.lst>"
        #echo ""
        #exit 1
#fi

if [ -f ${List_SID} ]
 then
  print "Looks good"
else
 Print "File does not exists... Make sure the SidFileName.lst exists under /u01/app/oracle/local/dba/Auditing"
fi

}

function SendEmail {
if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
to=narri@eagleinvsys.com
subject="Redo Undo Report for ${line}. Best viewed with WordPad....."
Message="Redo Undo report for ${line} is ready to review..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${RESULT} | mailx -s "${subject}" $to 
echo "Emailed report"
fi

}

function CreateReports {
#set -x
while read line
do

print ${line}
RESULT=/u01/app/oracle/local/dba/Auditing/reports/RedoUndo_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s audit_undo/audit_eagle2011@${line} <<!
set trimspool on
set serveroutput on 
set linesize 250
set pages 200
set head on
set feedback off
set verify off
set echo off
SET TERMOUT OFF
col DATE format a20
col OPERATION for a10
col username for a10
col sql_redo format a45 wrapped
col sql_undo format a45 wrapped
spool ${RESULT}
select username,scn,to_char(undo_date,'MM/DD/YYYY HH24:MI:SS') as "DATE",operation,sql_redo,sql_undo,sessionid,xid from audit_undo.sql_redoundo  where to_char(undo_date,'MM/DD/YYYY') >= to_char(sysdate-1,'MM/DD/YYYY') order by 1,3;
spool off
!

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
echo Finished writing report ${SHORT_NAME}

SendEmail

done < ${List_SID}

}

function CleanUp {

/usr/bin/find /u01/app/oracle/local/dba/Auditing/reports/RedoUndo_* -type f -mtime +7 -exec rm -f {} \;

}

###########################################################################
#                               MAIN                                      #
###########################################################################
NARG=$#
List_SID=/u01/app/oracle/local/dba/Auditing/SidFileName.lst
EMAIL=Y

Chk_Parameters
CreateReports
CleanUp

exit 0
 


