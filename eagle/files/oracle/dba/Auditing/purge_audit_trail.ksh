#!/bin/ksh
########################################################################################
#
# Script  : purge_audit_trail.ksh [Optional Parameter Days to retain trail.  Defaults to 365.]
# Purpose : To purge audit trail from the database by retaining the specified days worth of trail.
#
# 05/16/2015 Frank Davis  Made compatible with ASM Managed Databases
########################################################################################

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

### MAIN ###

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH:
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/Auditing
else
 export WORKING_DIR="$HOME/local/dba/Auditing"
fi

if [ $# -eq 1 ]
then
 PURGE_OLDER_THAN_DAYS=$1
else
 PURGE_OLDER_THAN_DAYS=365
fi

sids=$(ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
for a_sid in $sids
do
export ORACLE_SID=$a_sid
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

$ORACLE_HOME/bin/sqlplus '/nolog'<<EOF>oracle_version.txt
EOF
(( oracle_version=$(grep '^SQL\*Plus: Release ' oracle_version.txt | awk '{print $3}' | awk -F. '{print $1}') ))
rm -f oracle_version.txt

if [ $oracle_version -lt 10 ]
then
 if [[ ! -s $WORKING_DIR/purge_audit_trail_ora9.sql ]]
 then
  print "Script $WORKING_DIR/purge_audit_trail_ora9.sql not found!!!"
  print "Please copy from location epxd0001:$WORKING_DIR"
  exit 3
 fi
 run_this_script=purge_audit_trail_ora9.sql
else
 if [[ ! -s $WORKING_DIR/purge_audit_trail.sql ]]
 then
  print "Script $WORKING_DIR/purge_audit_trail.sql not found!!!"
  print "Please copy from location epxd0001:$WORKING_DIR"
  exit 2
 fi
 run_this_script=purge_audit_trail.sql
fi

print "DATABASE: $a_sid"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 @$WORKING_DIR/$run_this_script $PURGE_OLDER_THAN_DAYS
EOF
done

exit 0
