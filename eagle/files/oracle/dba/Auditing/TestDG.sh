#!/bin/ksh
set -x
. /usr/bin/setsid.sh infprd1

LOGFILE=$HOME/local/dba/Auditing/my.log

DBLIST=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set heading off
set feedback off
set pagesize 0
select rtrim(m.name)||':'||rtrim(d.sid)||':'||d.dataguard  from inf_monitor.databases d, inf_monitor.machines m
   where m.instance=d.mac_instance  and m.location in ('EVERETT','PITTSBURGH','NEWTON') 
 group by m.name,d.sid,d.sid,d.dataguard order by d.sid;
!`
LAST_SID="Dummy"

for line in `echo ${DBLIST}`; do
echo "This the line : $line" >> ${LOGFILE}
HOSTNAMES=`echo -n $line | sed s/:.*//g`;
SID=`echo $line | awk -F ":" '{print $2}'`
DG=`echo $line | awk -F ":" '{print $3}'`
if  [ ${DG} -eq 'N' ]; then
  		echo "This  is NOT DataGuard Machine is ${HOSTNAMES} and SID is $SID" >> ${LOGFILE}
  		echo "I am processing the password change..." >> ${LOGFILE}
 else
   if [ ${LAST_SID} -eq ${SID} ] && [ ${DG} -eq 'Y' ]; then
 		echo "This IS DATAGUARD Machine is ${HOSTNAMES} and SID is  $SID" >> ${LOGFILE}
 		echo "I am copying the PWD File" >> ${LOGFILE}
 fi
fi 
LAST_SID=${SID}

done

exit 0
