DEFINE PURGE_OLDER_THAN_DAYS = &1
set serverout on size 1000000
set echo off feed off veri off
DECLARE
  CURSOR c1 IS
  SELECT ROWID
  FROM   sys.aud$
  WHERE  timestamp# < SYSDATE - &&PURGE_OLDER_THAN_DAYS ;
  commit_count NUMBER := 0 ;
  tot_rec_count NUMBER := 0 ;
BEGIN
  FOR rec IN c1
  LOOP
    DELETE sys.aud$
    WHERE  ROWID = rec.ROWID ;
    commit_count := commit_count + 1 ;
    IF commit_count > 50000 THEN
       COMMIT ;
       tot_rec_count := tot_rec_count + commit_count ;
       commit_count := 0 ;
    END IF ;
  END LOOP ;
  COMMIT ;
  tot_rec_count := tot_rec_count + commit_count ;
  dbms_output.put_line(TO_CHAR(sysdate,'DD-MON-YYYY HH24:MI:SS') || TO_CHAR(tot_rec_count,'999,999,999,999')) ;
END;
/
exit ;
