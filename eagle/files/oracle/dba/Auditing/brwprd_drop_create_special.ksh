#!/bin/ksh
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 11/23/2010                                                                 #
# Usage: preserve_user_info.sh <SID>                                               #
# Program: This script sould be run before any database refresh. This program      # 
#          scripts  existing dba users, team install users and any read-only users #
#          with their original passwords and privileges.                           #
####################################################################################
#set -x
NARG=$#
ORA_SID=$1

## Checking for input parameter

if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
		echo "Not enough parameter..."
        echo ""
        echo "Usage: ./preserve_user_info.sh <SID>"
        echo ""
        echo "Your <SID> options are:"
   for sid in `ps -ef | grep ora_pmon_ | awk '{print $8}'`
    do
     s_sid=`expr substr $sid 10 10`
     echo "     "$s_sid
    done
     echo ""
		exit 1
fi

export ORACLE_SID=$ORA_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

export WORK_DIR=/u01/app/oracle/admin/$ORA_SID/preserve_users

if  [ ! -d  ${WORK_DIR} ]; then mkdir -p ${WORK_DIR}; fi

/usr/bin/find ${WORK_DIR}/*.sql -mtime +7 -exec rm -f {} \;

export USERFILE=${WORK_DIR}/PreserveUser_`date +\%d_\%m_\%Y`.sql


## User list to preserve

UserList='BMASON DBUI MGILLIS PMAZZOTTA TMANKA'


# Drop Users.

for users in `echo $UserList`
do
  DropUsers=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
   select 'drop user '||username||' cascade;' from dba_users where username=upper('$users');
  spool off
  exit
!`
done

## Create Users

for users in `echo $UserList`
do
  GetUserCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set head off
  set echo off
  set long 2000
  set pages 0
   select dbms_metadata.get_ddl( 'USER', '$users' )||';' from dual;
  exit
!`
echo $GetUserCreate >> $USERFILE
done

## Granting SYSTEM PRIVILEGES

for users in `echo $UserList`
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
   select 'grant '||PRIVILEGE||' to '||grantee||decode(admin_option,'YES',' with Admin option')||';' from dba_sys_privs where grantee=upper('$users');
  spool off
  exit
!`
#echo $GetRoleCreate >> $USERFILE
done

## Grant Roles to users..

for users in `echo $UserList`
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
  select 'grant '||GRANTED_ROLE||' to '||grantee||';' from dba_role_privs where grantee=upper('$users');
  exit
  spool off
!`
#echo $GetRoleCreate >> $USERFILE
done

## Granting users privileges.

for users in `echo $UserList`
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 20000
  set pages 0
 spool $USERFILE append
 select 'grant '||privilege||' on ' || owner||'.'||table_name || ' to '|| grantee||';'
from dba_tab_privs where grantee like upper('$users') order by privilege;
spool off
  exit
!`
#echo $GetRoleCreate >> $USERFILE
done


for users in `echo $UserList`
do
  GetRoleCreate=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
  select 'alter user '||username||' quota unlimited on '||tablespace_name||';' from dba_ts_quotas where max_blocks = -1 and username=upper('$users');
  spool off
  exit
!`
#echo $GetRoleCreate >> $USERFILE
done

## Enable Audit on all users

for users in `echo $UserList`
do
  GetAuditing=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
	select 'audit all by '||upper('$users')||' by access;' from dual;
	select 'audit update table, insert table, delete table,execute procedure by '||upper('$users')||' by access;' from dual;
  spool off
  exit
!`
#echo $GetRoleCreate >> $USERFILE
done

GetAudit=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
  set feedback off
  set head off
  set echo off
  set long 2000
  set pages 0
  spool $USERFILE append
select 'audit '||name||';'
from system_privilege_map
where (name like 'CREATE%TABLE%'
or name like 'CREATE%INDEX%'
or name like 'CREATE%CLUSTER%'
or name like 'CREATE%SEQUENCE%'
or name like 'CREATE%PROCEDURE%'
or name like 'CREATE%TRIGGER%'
or name like 'CREATE%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'ALTER%TABLE%'
or name like 'ALTER%INDEX%'
or name like 'ALTER%CLUSTER%'
or name like 'ALTER%SEQUENCE%'
or name like 'ALTER%PROCEDURE%'
or name like 'ALTER%TRIGGER%'
or name like 'ALTER%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'DROP%TABLE%'
or name like 'DROP%INDEX%'
or name like 'DROP%CLUSTER%'
or name like 'DROP%SEQUENCE%'
or name like 'DROP%PROCEDURE%'
or name like 'DROP%TRIGGER%'
or name like 'DROP%LIBRARY%')
union
select 'audit '||name||';'
from system_privilege_map
where (name like 'EXECUTE%INDEX%'
or name like 'EXECUTE%LIBRARY%');
spool off
exit
!`

exit 0
