#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 06/20/2012																							 #
#Usage : User_Appropriateness_APP_DEFAULT_rpt.sh  																	 #
#Purpose: List Users that are under DEFAULT Profile.(There should be NONE)									 #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=$HOME/local/dba/Auditing/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select lower(d.tnsname) from inf_monitor.databases d,inf_monitor.machines m
where d.dataguard='N' and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%' order by d.tnsname;
exit
!`

rm $REPORT_DIR/User_Appropriateness_prod.txt
RESULT=$REPORT_DIR/User_Appropriateness_prod.txt
touch $REPORT_DIR/User_Appropriateness_prod.txt

echo "                                   USER APPROPRIATENESS EXCEPTION REPORT PRODUCTION   " >> $REPORT_DIR/User_Appropriateness_prod.txt
echo "                                         NO USER SHOULD BE UNDER DEFAULT PROFILE      " >> $REPORT_DIR/User_Appropriateness_prod.txt
echo "     "  >> $REPORT_DIR/User_Appropriateness_prod.txt
echo "     "   >> $REPORT_DIR/User_Appropriateness_prod.txt

for line in `echo ${Sid_List}`; do

#echo "For Database : $line" >> $REPORT_DIR/User_Appropriateness_prod.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance
and u.db_instance=d.instance and u.username='SYS' and lower(d.tnsname)='${line}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;
exit
!`

#$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set head off
#spool $REPORT_DIR/User_Appropriateness_prod.txt append
#
#select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';
#
#exit
#!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a25
col account_status for a15
set pagesize 200
set linesize 200
--break on DataBase on username on created on default_tablespace on account_status on profile on granted_role
break on DataBase on username 
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/User_Appropriateness_prod.txt append

 
select distinct d.name as "DataBase",a.username,a.account_status,a.profile,a.created
from dba_users a, v\$database d
where profile = 'DEFAULT' and username != 'XS\$NULL'; 

set pagesize 0

--select ' ' from dual;
--select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="USER_APPROPRIATENESS EXCEPTION REPORT. Best viewed with WordPad....."
Message="Report on user appropriateness exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

