#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 06/20/2012																							 #
#Usage : User_Appropriateness_Check_rpt.sh  																 #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=$HOME/local/dba/Auditing/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select lower(d.tnsname) from inf_monitor.databases d, inf_monitor.machines m
where d.dataguard='N' and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%' 
--where d.dataguard='N' and environment like '%PROD%' and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%' 
--where d.dataguard='N' and (environment like '%DEV%' or environment like '%TEST%') and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%'
order by lower(d.tnsname);
exit
!`

rm $REPORT_DIR/User_Appropriateness_Check_rpt.txt
RESULT=$REPORT_DIR/User_Appropriateness_Check_rpt.txt
touch $REPORT_DIR/User_Appropriateness_Check_rpt.txt

echo "                                  ROLES AND USER ASSIGNMENT REPORT TEST/DEV   " >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt
--echo "                                  ROLES AND PRIVILEGES EXCEPTION REPORT TEST/DEV   " >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt
--echo "                                  ROLES AND PRIVILEGES EXCEPTION REPORT PRODUCTION   " >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt
--echo "                                    ROLES AND USER ASSIGNMENT REPORT PRODUCTION   " >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt
echo "     "  >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt
echo "     "   >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt

for line in `echo ${Sid_List}`; do

#echo "For Database : $line" >> $REPORT_DIR/User_Appropriateness_Check_rpt.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0

select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance
and u.db_instance=d.instance and u.username='SYS' and lower(d.tnsname)='${line}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;

exit
!`

#$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set head off
#spool $REPORT_DIR/User_Appropriateness_Check_rpt.txt append
#
#select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';
#
#exit
#!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a35
col account_status for a15
col typ for a8
col role for a25
col PACE_LVer for a10
col PACE_UVer for a10
col STAR_LVer for a10
col STAR_UVer for a12
set pagesize 200
set linesize 200
--break on DataBase on username on created on default_tablespace on account_status on profile on granted_role
break on DataBase on PACE_LVer on PACE_UVer on STAR_LVer on STAR_UVer on grantee on role on typ
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';


spool $REPORT_DIR/User_Appropriateness_Check_rpt.txt append

--spool $REPORT_DIR/GrantREADONLY.sql

--select 'grant readonly to '||grantee||';' from dba_role_privs where granted_role = 'READ_ONLY' and grantee != 'SYS';
--select 'revoke read_only from '||grantee||';' from dba_role_privs where granted_role = 'READ_ONLY' and grantee != 'SYS';
--Who is granted this ROLE:

--Without Version:

select b.name Database,a.GRANTEE,r.ROLE
from DBA_ROLE_PRIVS a, v\$database b,
dba_roles r
where r.role = a.granted_role(+) and r.role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV',
'WKUSER','IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS','ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','QUEST_SPC_APP_PRIV','ORA_ASPNET_MEM_BASICACCESS','ORA_ASPNET_MEM_REPORTACCESS','ORA_ASPNET_ROLES_BASICACCESS',
'INFOSEC_OPS_ADMIN','ORACLE_OIM_OPS_ADMIN','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES','READONLY','EA_INSTALL_ROLE','EA_DBA_ROLE','CONNECT_EAGLE','DBFS_ROLE','EXP_FULL_DATABASE','GATHER_SYSTEM_STATIS') order by b.name,r.role,a.grantee;

--With Version:
/*
select b.name Database,a.GRANTEE,r.ROLE,e.version "PACE_UVer",f.version "STAR_UVer"
from DBA_ROLE_PRIVS a, v\$database b, 
dba_roles r,
(select version from pace_masterdbo.eagle_product_version where instance = (select max(instance) from pace_masterdbo.eagle_product_version where  upper(product_type) in  ('EDM', 'PACE') and instance not in (select instance from pace_masterdbo.eagle_product_version where upper(comments) like '%ADDENDUM%'))) e,
(select version from pace_masterdbo.eagle_product_version where instance = (select max(instance) from pace_masterdbo.eagle_product_version where  upper(product_type) in  ('EDM', 'STAR-DB') and instance not in (select instance from pace_masterdbo.eagle_product_version where upper(comments) like '%ADDENDUM%'))) f
where r.role = a.granted_role(+) and r.role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV',
'WKUSER','IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS','ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','QUEST_SPC_APP_PRIV','ORA_ASPNET_MEM_BASICACCESS','ORA_ASPNET_MEM_REPORTACCESS','ORA_ASPNET_ROLES_BASICACCESS',
'INFOSEC_OPS_ADMIN','ORACLE_OIM_OPS_ADMIN','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES','EXP_FULL_DATABASE','GATHER_SYSTEM_STATIS') order by b.name,r.role,a.grantee;
*/

--spool off
--Roles and their Privileges
/*
select b.name DataBase,
      a.role role,
      '' typ,
      '' privilege
       from dba_roles a, v\$database b
where a.role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV',
'WKUSER','IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS','ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','QUEST_SPC_APP_PRIV' ,'INFOSEC_OPS_ADMIN','ORACLE_OIM_OPS_ADMIN','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE',
'ORA_ASPNET_MEM_BASICACCESS','ORA_ASPNET_MEM_REPORTACCESS','ORA_ASPNET_ROLES_BASICACCESS','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES')
union
select
      b.name DataBase,
      a.grantee role,
      'ROLE' typ,
      a.granted_role privilege
         from    dba_role_privs a, v\$database b
 where a.grantee in (select role from dba_roles where role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV',
'WKUSER','IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE',
'ORA_ASPNET_MEM_BASICACCESS','ORA_ASPNET_MEM_REPORTACCESS','ORA_ASPNET_ROLES_BASICACCESS','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS','ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','QUEST_SPC_APP_PRIV' ,'INFOSEC_OPS_ADMIN','ORACLE_OIM_OPS_ADMIN','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES'))
         union
    select
      b.name DataBase,
      a.grantee role,
      'SYSTEM' typ,
      a.privilege privilege
         from    dba_sys_privs a, v\$database b
 where a.grantee in (select role from dba_roles  where role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','WKUSER','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','ORA_ASPNET_MEM_BASICACCESS','ORA_ASPNET_MEM_REPORTACCESS','ORA_ASPNET_ROLES_BASICACCESS',
'LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV', 'IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS',
'ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','QUEST_SPC_APP_PRIV' ,'INFOSEC_OPS_ADMIN',
'ORACLE_OIM_OPS_ADMIN','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES')) order by 1;

*/ 
--select distinct d.name as "DataBase",a.username,a.account_status,a.profile,a.created
--from dba_users a, v\$database d
--where profile = 'DEFAULT' and username != 'XS\$NULL'; 

set pagesize 0

--select ' ' from dual;
--select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
to=nsarri@eagleaccess.com
subject="USER_APPROPRIATENESS EXCEPTION REPORT. Best viewed with WordPad....."
Message="Report on user appropriateness exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

