#!/usr/bin/ksh
# ==================================================================================================
# NAME:         QueryDBs.sh
#
# AUTHOR:       Maureen Buotte
#
# PURPOSE:      Generic script to query a list of databases and return the results in text files
#
# USAGE:        QueryDBs.sh  DatabaseFile QueryFile
#						Database File format - Instance
# CHANGES:      NVS     03-02-2010      Modified to add new primary key machines(instance)
#               Nirmal  08-18-2011 		Modified to Send Report via Email to Frank and other dba's
# ==================================================================================================

# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

        # Uncomment for debug
       # set -x

        #Machine=`uname -a | awk '{print $2}'`
        ## Add any additional information to mail message
        #if [[ x$1 != 'x' ]]; then
        #        echo -e "\n\n$1\n" >> mail.dat
        #fi

	#cat ${MISMATCH_FILE} >> mail.dat

        cat mail.dat | mail -s "Monthly Report from Dev and Test for Users with DBA Privileges..." ${MAILTO}
        rm mail.dat
        return 0
}

# **********************************************************************************
# funct_chk_parm: Check for input parameters
# **********************************************************************************
funct_chk_parm() {
        # Uncomment next line for debugging
         set -x

        if  [ ${NARG} -ne 2 ] ; then
                echo -e "Incorrect number of arguments QueryDBs.sh requires 2 parameters "
                echo -e "		1:  Name of File Containing list of Database Instances (location $HOME/local/dba/Auditing/CRONED_SCRIPTS/scripts)"
                echo -e "		2:  Name of File containing SQL to run (location $HOME/local/dba/Auditing/CRONED_SCRIPTS/scripts)"
                exit 1
        fi

}

# **********************************************************************************
# funct_run_statement: Run the Oracle Statement
# **********************************************************************************
funct_run_statement() {
        # Uncomment next line for debugging
         set -x

        USER_CODE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
				set heading off
                set feedback off
				select case when exists 
				(select bb_get(temp_code) from user_codes where 
					db_instance=${DB_INSTANCE} and username='SYS' and temp_code is not null)
                then (select bb_get(temp_code) from user_codes where db_instance=${DB_INSTANCE} and username='SYS')
                else (select bb_get(code) from user_codes where db_instance=${DB_INSTANCE} and username='SYS') end
				from dual;
                exit
EOF`
		USER_CODE=`echo -e $USER_CODE|tr -d " "`

        status=`${ORACLE_HOME}/bin/sqlplus -s SYS/${USER_CODE}@${TNSNAME} as sysdba <<EOF
                set pagesize 999
                set linesize 100
				set feedback off
                spool ${OUTPUT_FILE}
                @${SCRIPT_FILE}
                spool off
                exit
EOF`

}

# *****************************
#  MAIN
# *****************************
#
# Uncomment for debug
set -x

. /usr/bin/setsid.sh infprd1

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export WORKING_DIR=$HOME/local/dba/Auditing/CRONED_SCRIPTS
export SCRIPT_DIR=$WORKING_DIR/scripts
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini

if [[ -a ${PARFILE} ]]; then
    STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
    if [[ -n $LINE ]]; then
		INFO=`echo $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g' `
		export PERFORM_CRON_STATUS=1
		export CRON_SID=`echo -e $INFO | awk '{print $1}' `
		export CRON_USER=`echo -e $INFO | awk '{print $2}' `
		CRON_CONNECT=`echo -e $INFO | awk '{print $3}' `
		export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
	fi
fi

NARG=$#
funct_chk_parm
export INP_INPUT_FILE=$1
export INP_SCRIPT_FILE=$2

export INPUT_FILE=$SCRIPT_DIR/$INP_INPUT_FILE
export SCRIPT_FILE=$SCRIPT_DIR/$INP_SCRIPT_FILE

if [[ ! -a ${PARFILE} ]]; then

    export MAILTO=nsarri@eagleaccess.com
    
	echo " Important PARAMETER File -> ${PARFILE} does not exist" > mail.dat

    SendNotification
	exit
fi

if [[ ! -a ${INPUT_FILE} ]]; then
    export MAILTO=nsarri@eagleaccess.com
	echo "File containing list of databases -> ${INPUT_FILE} does not exist" > mail.dat
    SendNotification
	exit
fi

if [[ ! -a ${SCRIPT_FILE} ]]; then
    export MAILTO=nsarri@eagleaccess.com
	echo "File containing SQL statement -> ${SCRIPT_FILE} does not exist" > mail.dat
    SendNotification
exit
fi

export SCRIPT_FILE_BASE=`echo ${INP_SCRIPT_FILE} | awk -F. '{print $1}'`

HOST_NAME=`echo -e $(hostname) | awk -F "." '{print $1}'`
export BOX_NAME="'"${HOST_NAME}"'"
#export MAILTO=nsarri@eagleaccess.com
export MAILTO=nsarri@eagleaccess.com,mbuotte@eagleaccess.com,lwhitmore@eagleinvsys.com
##export MAILTO=team_dba@eagleaccess.com
##export MAILTO=mbuotte@eagleaccess.com

NumericDate=`date +%Y%m%d`
export OUTPUT_FILE=$HOME/local/dba/Auditing/CRONED_SCRIPTS/results/tmp.txt
export RESULT_FILE=$HOME/local/dba/Auditing/CRONED_SCRIPTS/results/${SCRIPT_FILE_BASE}_${NumericDate}.txt
rm ${RESULT_FILE}
if [ -a $OUTPUT_FILE ]; then
	rm $OUTPUT_FILE
fi
if [ -a $RESULT_FILE ]; then
	rm $RESULT_FILE
fi

set -A DATABASE_LIST `cat ${INPUT_FILE}`
typeset -i i=0	
num_elements=${#DATABASE_LIST[*]} 
while [ ${i} -lt ${num_elements} ]
do
	DB_INSTANCE=${DATABASE_LIST[${i}]}
	i=${i}+1;
	if [[ x${DB_INSTANCE} = 'x' ]]; then
		continue
	fi
	export DB_INSTANCE

	TNSNAME=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			select trim(tnsname) from databases where instance=$DB_INSTANCE;
			exit
EOF`
	export TNSNAME=`echo -e $TNSNAME|tr -d " "`

	BOX=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
			set heading off
			set feedback off
			select trim(name) from machines where instance = (select mac_instance from databases where instance=$DB_INSTANCE);
			exit
EOF`
	export BOX=`echo -e $BOX|tr -d " "`

	funct_run_statement
	echo -e "Database INSTANCE NUMBER: 		$DB_INSTANCE BOX: $BOX 			TNSNAME: $TNSNAME \n" >> $RESULT_FILE
	cat $OUTPUT_FILE >> $RESULT_FILE
	echo -e "=========================================================== \n" >> $RESULT_FILE
	echo -e "\n" >> $RESULT_FILE
	rm $OUTPUT_FILE
done	

cat $RESULT_FILE > mail.dat

SendNotification

exit 0
