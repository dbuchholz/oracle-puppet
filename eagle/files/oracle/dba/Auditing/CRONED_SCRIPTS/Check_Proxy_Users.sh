#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 02/11/201r3																						 #
#Usage : Check_Proxy_Users.sh              																	 #
#Purpose: This script reports is any proxy user exists in TEST or PROD. There should not be any Proxy TST/PRD#
#         /u01/app/oracle/local/dba/Auditing/reports/User_Appropriateness directory.                         #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=$HOME/local/dba/Auditing/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select lower(d.tnsname) from inf_monitor.databases d,inf_monitor.machines m
where d.dataguard='N' and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%' and (environment like '%PROD%' or environment like '%TEST%') order by d.tnsname;

exit
!`

rm $REPORT_DIR/Check_Proxy_Users.txt
RESULT=$REPORT_DIR/Check_Proxy_Users.txt
touch $REPORT_DIR/Check_Proxy_Users.txt

echo "                                     PROXY USER EXCEPTION REPORT TEST AND PRODUCTION   " >> $REPORT_DIR/Check_Proxy_Users.txt
echo "                                     -----------------------------------------------"  >> $REPORT_DIR/Check_Proxy_Users.txt
echo "     "   >> $REPORT_DIR/Check_Proxy_Users.txt
echo "Except for BrandyWine PROXY users should not exist in any other databases.     " >> $REPORT_DIR/Check_Proxy_Users.txt
echo "Please verify before taking any action.....     " >> $REPORT_DIR/Check_Proxy_Users.txt
echo "     "   >> $REPORT_DIR/Check_Proxy_Users.txt

for line in `echo ${Sid_List}`; do

#echo "For Database : $line" >> $REPORT_DIR/Check_Proxy_Users.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance 
and u.db_instance=d.instance and u.username='SYS' and lower(d.tnsname)='${line}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;

exit
!`

#$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set head off
#spool $REPORT_DIR/Check_Proxy_Users.txt append
#
#select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';
#
#exit
#!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col name for a30
col proxy for a20
col client for a20
set pagesize 200
set linesize 200
break on Database
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/Check_Proxy_Users.txt append

select d.name "Database",p.proxy "Proxy User",p.client "connect to" from dba_proxies p, v\$database d group by d.name,p.proxy,p.client order by d.name,p.proxy; 

set pagesize 0

--select ' ' from dual;
--select ' ' from dual;

spool off

!
done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="PROXY USER EXCEPTION REPORT FOR TEST AND PRODUCTION. Best viewed with WordPad....."
Message="Report on proxy user exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

