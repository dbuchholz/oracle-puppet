#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 02/08/2012																							 #
#Usage : Global_Audit_Check_IMP_rpt.sh 																		 #
#Purpose: Check on all Databases which does not have Auditing on                                             #
#         /u01/app/oracle/local/dba/Auditing/reports/UM/<SID> directory.                                     #
##############################################################################################################
#set -x
EMAIL=Y

. /usr/bin/setsid.sh infprd1

REPORT_DIR=${HOME}/local/dba/Auditing/reports/Audit_Check

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi

RESULT=${REPORT_DIR}/Audit_Check_NotExists_IMP_`date +"%m-%d-%Y"`.log
rm ${REPORT_DIR}/Audit_Check_NotExists_IMP_`date +"%m-%d-%Y"`.log
touch ${RESULT}

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set serveroutput on
set heading off
set feedback off
col value for a20
set pagesize 0
set linesize 200
spool $RESULT append

select m.name, d.sid as "DB NOT AUDITED" from inf_monitor.db_parameters p, inf_monitor.databases d, inf_monitor.machines m
 where d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%'
 and p.db_instance = d.instance and p.parameter='audit_trail' and p.pvalue not in ('DB','DB_EXTENDED','TRUE')
 and d.sid not in ('ccoctx','easprd3','easptst2','gglbac','eabprd1','easprd5','easptst1','easpprd1','easprd32') and m.name<>'dbatrain'
 and d.dataguard='N'  order by m.name,d.sid;

select ' ' from dual;
spool off

!

RESULT_02=${REPORT_DIR}/Audit_Check_NotExists_imp_`date +"%m-%d-%Y"`.log

awk '{ if(NF != 7) {print $0 } }' ${RESULT} > ${RESULT_02}

RESULT=${RESULT_02}

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="Report on all databases that are NOT AUDIT ENABLED..."
Message="Report on all databases that are NOT AUDIT ENABLED..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi



exit 0

