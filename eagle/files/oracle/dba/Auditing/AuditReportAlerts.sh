#!/bin/ksh
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/05/2011                                                                 #
# Usage: AuditReportAlerts.sh                                                      #
# Program: This script Reports the Oracle Audit Information on a given database    #
####################################################################################
#set -x


function FailedLogins {
while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

STATUS=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
spool ${RESULT}
ttitle center "FAILED LOGON ATTEMPTS" skip 2
select count(*) as "No. of Attempts" ,username,terminal,to_char(timestamp,'DD-MON-YYYY') as "DATE"
from dba_audit_session
where returncode<>0 and timestamp >= sysdate-1
group by username,terminal,to_char(timestamp,'DD-MON-YYYY') order by to_char(timestamp,'DD-MON-YYYY') desc;
spool off
exit
!`

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}

function AttempNonExistantUsers {

while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
spool ${RESULT}
ttitle skip 2
ttitle center "ATTEMPTS TO ACCESS DATABASE WITH NON EXISTANT USERS" skip 2

select username,terminal,to_char(timestamp,'DD-MON-YYYY HH24:MI:SS')
from dba_audit_session
where returncode<>0 and timestamp >= sysdate-1
and not exists (select 'x'
   from dba_users
   where dba_users.username=dba_audit_session.username);
spool off
exit
!

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}

function StructureChange {

while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
spool ${RESULT}
ttitle skip 2
ttitle center "CHANGES MADE TO THE STRUCTURE OF THE DATABASE SCHEMA" skip 2

col username for a20
col priv_used for a36
col obj_name for a30
col timestamp for a17
col returncode for 9999
select  username,
        priv_used,
        obj_name,
        to_char(timestamp,'DD-MON-YYYY HH24:MI') timestamp,
        returncode
from dba_audit_trail
where priv_used is not null
and priv_used<>'CREATE SESSION' and timestamp >= sysdate-1;
spool off
exit
!

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}

function DbsnmpLoginAttempt {

while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
spool ${RESULT}
ttitle skip 2
ttitle center "FAILED LOGIN ATTEMPTS USING DBSNMP" skip 2

col userhost format a35
col terminal format a35
col os_username format a25

select userhost, terminal, os_username, count(1)
from dba_audit_trail
where returncode = 1017
and username = 'DBSNMP' and timestamp >= sysdate-1
group by userhost, terminal, os_username;
spool off
exit
!

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}


function IfCompromised {

while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
spool ${RESULT}
ttitle skip 2
ttitle center "CHECK IF DATABASE HAS BEEN COMPROMISED" skip 2

col type_name format a30
col name format a35
col action format a35

select *  from JAVA\$POLICY\$ where grantee# in (select grantee# from sys.JAVA\$POLICY\$ minus select user# from sys.user\\$);
spool off
exit
!

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}

function DropCreateActivity {

while read line
do

. /usr/bin/setsid.sh ${line}

RESULT=/u01/app/oracle/local/dba/Auditing/reports/Audit_Alert_${line}_`date +\%d_\%m_\%Y`.txt

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set trimspool on
set serveroutput on
set linesize 120
set pages 200
set head on
set feedback off
set verify off
set echo off
SET UNDERLINE =
SET TERMOUT OFF
col terminal for a25
col username for a15
col DATE for a15
col sql_text for a35 wrap
col LogoffTime for a20
col TimeStamp for a25
spool ${RESULT}
ttitle skip 2
select username, to_char(timestamp,'MM/DD/YYYY HH24:MM:SS') as "TimeStamp",
    owner, obj_name, action_name, sql_text
  from
  dba_audit_trail where (action_name like '%DROP%' or action_name like '%CREATE%') and username not in ('ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS','HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS','ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS','QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','SYS','SYSTEM','ESTAR','PACE_MASTERDBO','DATAMARTDBO', 'DATAEXCHDBO', 'SCRUBDBO', 'PRICING', 'LEDGERDBO', 'SECURITYDBO', 'TRADESDBO', 'PERFORMDBO', 'HOLDINGDBO', 'CASHDBO', 'RULESDBO', 'EAGLEKB', 'ARCH_CTRLCTR', 'CTRLCTR', 'MSGCENTER_DBO', 'EAGLEMGR') and timestamp >= sysdate-1 order by 1,2,6;
spool off
exit
!

if [[ $(cksum ${RESULT} | awk '{print $2}') -gt 0 ]]
then
	SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`
	echo Finished writing report ${SHORT_NAME}
	if [ "$EMAIL" = "Y" ]
	then
		#to=team_dba@eagleaccess.com
		to=narri@eagleinvsys.com
		subject="Audit ALERT! for ${ORACLE_SID}. Best viewed with WordPad....."
		Message="Auditing report for ${ORACLE_SID} is ready to review..."
		uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
		echo "Emailed report"
	fi
fi
done <${DBList}

}
function CleanUp {

/usr/bin/find /u01/app/oracle/local/dba/Auditing/reports/Audit_Alert* -type f -mtime +160 -exec rm -f {} \;

}

####### MAIN ######

REPORT_DIR=/u01/app/oracle/local/dba/Auditing/reports
if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi
DBList=/u01/app/oracle/local/dba/Auditing/DBlist.lst
EMAIL=Y
ps -ef |grep pmon|grep -v grep| awk -F_ '{print $3}' > ${DBList}


### Execute Functions ###

#FailedLogins
#AttempNonExistantUsers
#StructureChange
DbsnmpLoginAttempt
IfCompromised
#DropCreateActivity
CleanUp

exit 0
