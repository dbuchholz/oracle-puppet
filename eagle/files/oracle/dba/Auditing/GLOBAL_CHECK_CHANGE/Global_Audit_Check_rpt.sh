#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 02/08/2012																							 #
#Usage : Global_Audit_Check_rpt.sh         																		 #
#Purpose: Check on all Databases which does not have Auditing on                                             #
#         /u01/app/oracle/local/dba/Auditing/reports/UM/<SID> directory.                                     #
##############################################################################################################
set -x

#NARG=$#
#NumUsers=`expr ${NARG} - 1`
#echo $*
#if [ ${NARG} -ne 2 ]; then
        #clear
        #echo ""
        #echo "Not enough parameter..."
        #echo ""
        #echo "Usage: ./GlobalPasswd_Change.sh <USERNAME> <PASSWORD> "
        #echo ""
        #exit 1
#fi


. /usr/bin/setsid.sh infprd1

UserName=$1
UserPWD=$2

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d
where d.sid not like '%mfc%' and d.dataguard='N'
order by d.sid;

exit
!`

rm Audit_Check_NotExists.log
touch Audit_Check_NotExists.log

for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`


$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!

spool  Audit_Check_NotExists.log append
declare
v_instance varchar2(50);
v_value varchar2(50);
BEGIN

select INSTANCE_NAME into v_instance from v\$instance;

select value into v_value from v\$parameter where name='audit_trail' and value != 'DB_EXTENDED';

dbms_output.put_line('INSTANCE: '||v_instance||' VALUE: '||v_value);

END;
/
!

done


exit 0

