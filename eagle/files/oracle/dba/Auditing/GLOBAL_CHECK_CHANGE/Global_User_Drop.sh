#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 01/10/2012																							 #
#Usage : Global_User_Drop.sh <USERNAME>                 												     #
#Purpose: Drop user on all  environment(DEV/TEST/PROD) with profile=EA_PROFILE only                          #
#         /u01/app/oracle/local/dba/Auditing/reports/UM/<SID> directory.                                     #
##############################################################################################################
#set -x

NARG=$#
NumUsers=`expr ${NARG} - 1`
echo $*
if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./Global_User_Drop.sh <USERNAME> "
        echo ""
        exit 1
fi

UserName=$1
UserPWD=$2

typeset -u v2
v2=${UserName}
UserName=${v2}

username_str='ARCH CTRLCTR CASHDBO CTRLCTR DATAEXCHDBO DATAMARTDBO EAGLEKB EAGLEMART EAGLEMGR EAGLE TEMP ESTAR HOLDINGDBO LEDGERDBO MSGCENTER DBO PACE MASTERDBO PERFORMDBO PRICING RULESDBO SCRUBDBO SECURITYDBO SYS SYSTEM TRADESDBO'

for line in `echo ${username_str}`; do
typeset -u v1
v1=${line}
s1=${v1}
 if [[ $s1 == *$UserName* ]]
   then
     echo "You cannot have username with word $s1 "
     exit 1
 fi

done

. /usr/bin/setsid.sh infprd1


Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select lower(d.tnsname) from inf_monitor.databases d,inf_monitor.machines m
where  d.dataguard='N' and d.mac_instance = m.instance and upper(m.os_type) not like '%WINDOWS%'
order by d.sid;

exit
!`
RESULT=User_${UserName}_Drop.log
touch User_${UserName}_Drop.log

for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) as code from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and lower(d.tnsname)='${line}' group by d.sid,inf_monitor.bb_get(u.temp_code),inf_monitor.bb_get(u.code) order by 1;
exit
!`

#SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set heading off
#set feedback off
#set pagesize 0
#select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
#where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
#group by d.sid,inf_monitor.bb_get(u.code) order by 1;
#exit
#!`


#### Drop is user exist

$ORACLE_HOME/bin/sqlplus -s "system/${SysPwd}@${line}" <<!
set serveroutput on
set feedback off
spool  ${RESULT} append
DECLARE
v_count       INTEGER := 0;
sql_stmt   VARCHAR2 (1000);
BEGIN

SELECT COUNT(*) INTO v_count FROM dba_users WHERE username = UPPER ('${UserName}') and profile='EA_PROFILE';

IF v_count = 0
THEN
DBMS_OUTPUT.put_line('User ${UserName} does not exists in database ${line}');
DBMS_OUTPUT.put_line('OR User ${UserName} and PROFILE does not match to EA_PROFILE in database ${line}');
ELSE
 sql_stmt := 'drop user ${UserName} cascade';
execute immediate sql_stmt;
DBMS_OUTPUT.put_line('User ${UserName} has been dropped from ${line}');
END IF;

v_count := 0;

EXCEPTION
 WHEN OTHERS
   THEN
       DBMS_OUTPUT.put_line (SQLERRM);
       DBMS_OUTPUT.put_line ('   ');
END;
/

spool off
!

done


exit 0

