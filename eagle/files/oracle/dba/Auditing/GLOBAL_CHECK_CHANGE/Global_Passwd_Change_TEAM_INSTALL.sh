#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 01/10/2012																							 #
#Usage : Global_Create_NONDBA_Users.sh <USERNAME> <PASSWORD> 											     #
#Purpose: Creates a NON-DBA user on all the databases.														 #
##############################################################################################################
#set -x

clear
read UserName?"Enter Username:> "

stty -echo
read Pwd1?"Enter your new password:> "
print "\n"
read Pwd2?"Re-enter your new password:> "
stty echo
echo

if [ ${Pwd1} != ${Pwd2} ]
 then
  echo "Sorry! Password Mismatch..."
  exit 1
fi

print "Please wait while we process your request....."

RESULT=$HOME/local/dba/Auditing/Results
if  [[ ! -d  ${RESULT} ]]; then mkdir -p ${RESULT}; fi

UserPWD='"'${Pwd1}'"'

typeset -u v2
v2=${UserName}
UserName=${v2}

username_str='ARCH CTRLCTR CASHDBO CTRLCTR DATAEXCHDBO DATAMARTDBO EAGLEKB EAGLEMART EAGLEMGR EAGLE TEMP ESTAR HOLDINGDBO LEDGERDBO MSGCENTER DBO PACE MASTERDBO PERFORMDBO PRICING RULESDBO SCRUBDBO SECURITYDBO SYS SYSTEM TRADESDBO'

for line in `echo ${username_str}`; do
typeset -u v1
v1=${line}
s1=${v1}
 if [[ $s1 == *$UserName* ]]
   then
     echo "You cannot have username with word $s1 "
     exit 1
 fi

done

. /usr/bin/setsid.sh infprd1


Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0

select lower(d.sid) from inf_monitor.databases d, inf_monitor.machines m
where d.dataguard='N' and d.mac_instance = m.instance and m.os_type != 'Windows' and (sid not like '%ea%' and sid not like '%etl%' and sid not like '%idr%')
and sid not in ('inftst1','eabprd1','infpoc1','ggldev1','clapoc1','ccoctx','gglbac','grcprd1','infprd1','inftst2')
 order by d.sid;

!`

me=`whoami`

LOGFILE=${RESULT}/CreateUser_${UserName}_by_$me.log

if  [ -f  ${LOGFILE} ]; then mv ${LOGFILE} ${LOGFILE}_`date +'%m%d%Y'`; fi


for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) as code from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid='${line}' group by d.sid,inf_monitor.bb_get(u.temp_code),inf_monitor.bb_get(u.code) order by 1;
!`

#SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set heading off
#set feedback off
#set pagesize 0
#select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
#where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
#group by d.sid,inf_monitor.bb_get(u.code) order by 1;
#exit
#!`


#### Create if user doesn't exist

$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!
set serveroutput on
set feed off
spool  ${LOGFILE} append
DECLARE
PwdExists EXCEPTION;
PRAGMA EXCEPTION_INIT(PwdExists,-28007);
v_count       INTEGER := 0;
sql_stmt   VARCHAR2 (1000);
BEGIN

SELECT COUNT(*) INTO v_count FROM dba_users WHERE username = UPPER ('${UserName}');

IF v_count = 0
THEN

sql_stmt := 'create user ${UserName} IDENTIFIED BY ${UserPWD} DEFAULT TABLESPACE USERS QUOTA UNLIMITED ON USERS TEMPORARY TABLESPACE  TEMP ACCOUNT UNLOCK PROFILE EA_PROFILE';

execute immediate sql_stmt;
execute immediate 'GRANT connect, create session TO ${UserName}';
execute immediate 'GRANT EA_INSTALL_ROLE TO ${UserName}';
execute immediate 'audit all by ${UserName} by access';
execute immediate 'audit update table, insert table, delete table, execute procedure by ${UserName} by access';

ELSE
 sql_stmt := 'alter user ${UserName} identified by ${UserPWD} account unlock profile ea_profile';
execute immediate sql_stmt;
END IF;

v_count := 0;

EXCEPTION
 WHEN PwdExists THEN
      DBMS_OUTPUT.put_line('Password cannot be reused...');
 WHEN OTHERS
   THEN
       DBMS_OUTPUT.put_line ('${line}'||': '||SQLERRM);
       DBMS_OUTPUT.put_line ('   ');
END;
/

spool off
!

done


exit 0

