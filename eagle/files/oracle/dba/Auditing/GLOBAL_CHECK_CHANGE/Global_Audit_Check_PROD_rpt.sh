#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 02/08/2012																							 #
#Usage : Global_Audit_Check_PROD_rpt.sh 																	 #
#Purpose: Check on all Databases which does not have Auditing on                                             #
#         /u01/app/oracle/local/dba/Auditing/reports/UM/<SID> directory.                                     #
##############################################################################################################
#set -x
EMAIL=Y
#NARG=$#
#NumUsers=`expr ${NARG} - 1`
#echo $*
#if [ ${NARG} -ne 2 ]; then
        #clear
        #echo ""
        #echo "Not enough parameter..."
        #echo ""
        #echo "Usage: ./GlobalPasswd_Change.sh <USERNAME> <PASSWORD> "
        #echo ""
        #exit 1
#fi


. /usr/bin/setsid.sh infprd1

UserName=$1
UserPWD=$2

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d
where d.sid not like '%mfc%' and d.dataguard='N' 
and d.sid not in ('infprd1','eabprd1')
and environment like '%PROD%'
orderi by d.sid;

exit
!`

REPORT_DIR=${HOME}/local/dba/Auditing/reports/Audit_Check

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi

RESULT=${REPORT_DIR}/Audit_Check_NotExists_PROD_`date +"%m-%d-%Y"`.log
rm ${REPORT_DIR}/Audit_Check_NotExists_PROD_`date +"%m-%d-%Y"`.log
touch ${RESULT}

for line in `echo ${Sid_List}`; do

servname=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select m.name
from inf_monitor.databases d, inf_monitor.machines m 
where d.sid like '%${line}%' and d.mac_instance = m.instance  and d.dataguard='N'; 
exit
!`


LOGMNRFILE=`ssh -q oracle@${servname} ls /u01/app/oracle/audit_${line}/dictionary.ora`

#if [ "/u01/app/oracle/audit_${line}/dictionary.ora" != ${LOGMNRFILE} ]
#then
  #echo "" >> ${RESULT}
  #echo "Logminer on ${servname} for ${servname} for ${line} is ENABLED" >> ${RESULT}
 #else
  #echo "Logminer on ${servname} for ${line} is DISABLED" >> ${RESULT}
#fi

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`


STATUS=`$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!

spool $RESULT append
set serveroutput on
set heading off
set feedback off
col value for a20
set pagesize 0
set linesize 200

select 'Audit Exceptions for DATABASE => ${line}: ' from dual;
select 'DATABASE ${line}: AUDIT TRAIL Value  = '||value from v\\$parameter where name in ('audit_trail');
select decode(count(*),0,'PASSWORD VERIFY FUNCTION = NO.') from dba_objects where object_name ='EA_VERIFY_FUNCTION';
select decode(count(*),0,'USERS CREATED and AUDIT ENABLED = NO.') from dba_priv_audit_opts where user_name is not null;
select decode(SUPPLEMENTAL_LOG_DATA_MIN,'NO','SUPPLEMENTAL_LOG_DATA = NO.') from v\\$database;

select ' ' from dual;
spool off

!`

echo ${STATUS} > tmp.lst

ORA=`grep ORA- tmp.lst`
Err=`grep -i error tmp.lst`
if [[ "$ORA" = "" && "$Err" = "" ]]
 then
if [ "$STATUS" != "" ]
 then
echo ${STATUS} >> ${RESULT} 
fi
fi

rm tmp.lst

done

RESULT_02=${REPORT_DIR}/Audit_Check_NotExists_`date +"%m-%d-%Y"`.lst

awk '{ if(NF != 7) {print $0 } }' ${RESULT} > ${RESULT_02}

RESULT=${RESULT_02}

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
to=nsarri@eagleaccess.com
subject="ADMIN USER_LISTING. Best viewed with WordPad....."
Message="Report on admin users in all databases ..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi



exit 0

