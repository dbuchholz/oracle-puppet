#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 01/10/2012																							 #
#Usage : Global_Create_DB_User.sh                       												     #
#Purpose: Create User on all databases.      																 #
##############################################################################################################
#set -x

#NARG=$#
#NumUsers=`expr ${NARG} - 1`
#echo $*
#if [ ${NARG} -ne 2 ]; then
        #clear
        #echo ""
        #echo "Not enough parameter..."
        #echo ""
        #echo "Usage: ./Global_Create_DB_User.sh <USERNAME> <PASSWORD> "
        #echo ""
        #exit 1
#fi

clear
echo ""
echo ""
echo ""
read UserName?"                                                          Enter Username:> "

if [[ ${UserName} = '' ]]
 then
  echo "Sorry, Username cannot be blank.."
  exit 1
fi

stty -echo
read Pwd1?"                                                 Enter your new password:> "
print "\n"
read Pwd2?"                                               e-enter your new password:> "
stty echo
echo

if [[ ${Pwd1} != ${Pwd2} ]]
 then
  echo "Sorry! Password Mismatch..."
  exit 1
fi

UserPWD=${Pwd1}

echo "                                                Enter 1 for EA_PROFILE"
echo "                                                Enter 2 for EA_SERVICE"
read UPro?"                                                       Enter the profile:>"

if [[ ${UPro} = 1 ]]
 then
 UserProfile='EA_PROFILE'
 elif [[ ${UPro} = 2 ]]
   then
 UserProfile='EA_SERVICE' 
else
 echo "Sorry! Wrong Choice..."
 exit 1
fi

echo "                                                Enter 1 for EA_DBA_ROLE"
echo "                                                Enter 2 for EA_INSTALL_ROLE"
echo "                                                Enter 3 for READONLY"
read URol?"                                                          Enter the Role:>"

if [[ ${URol} = 1 ]]
 then
 UserRole='EA_DBA_ROLE'
 elif [[ ${URol} = 2 ]]
   then
 UserRole='EA_INSTALL_ROLE'
 elif [[ ${URol} = 3 ]]
   then
 UserRole='READONLY'
else 
 echo "Sorry! wrong choice...."
 exit 1
fi

echo ""
read DBEnv?"                                             Enter Environment(IMP/PROD):>"

if [[ ${DBEnv} != 'IMP' && ${DBEnv} != 'imp' ]] && [[ ${DBEnv} != 'PROD' && ${DBEnv} != 'prod' ]]
then
echo "Sorry you did not choose the right environment...."
exit 1
fi

read YN?" Caution: This will create user ${UserName} in all ${DBEnv} databases. Are you sure(Y/N):"

if [[ ${YN} != 'Y' ]] && [[ ${YN} != 'y' ]] 
 then
  echo "OK! You may try again later..."
  exit 1
fi

echo ""
print "Please wait while we process your request....."

#UserName=$1
#UserPWD=$2


typeset -u v2
v2=${UserName}
UserName=${v2}

username_str='ARCH CTRLCTR CASHDBO CTRLCTR DATAEXCHDBO DATAMARTDBO EAGLEKB EAGLEMART EAGLEMGR EAGLE TEMP ESTAR HOLDINGDBO LEDGERDBO MSGCENTER DBO PACE MASTERDBO PERFORMDBO PRICING RULESDBO SCRUBDBO SECURITYDBO SYS SYSTEM TRADESDBO'

for line in `echo ${username_str}`; do
typeset -u v1
v1=${line}
s1=${v1}
 if [[ $s1 == *$UserName* ]]
   then
     echo "You cannot have username with word $s1 "
     exit 1
 fi

done

exit 1

. /usr/bin/setsid.sh infprd1

if [[ ${DBEnv} = 'IMP' ]] || [[ ${DBEnv} = 'imp' ]]
then
Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d
where d.dataguard='N' and (environment like '%TEST%' or environment like '%DEV%') and (sid not like '%ea%' and sid not like '%etl%' and sid not like '%idr%')
and sid not in ('inftst1','eabprd1','infpoc1','ggldev1','clapoc1','ccoctx','gglbac','grcprd1')
 order by d.sid;
!`

fi

if [[ ${DBEnv} = 'PROD' ]] || [[ ${DBEnv} = 'prod' ]]
then
Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d
where d.dataguard='N' and environment like '%PROD%' and (sid not like '%ea%' and sid not like '%etl%' and sid not like '%idr%')
and sid not in ('inftst1','eabprd1','infpoc1','ggldev1','clapoc1','ccoctx','gglbac','grcprd1')
 order by d.sid;
!`
fi




touch UserPWD_${UserName}_Check_Exists.log

for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) as code from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' group by d.sid,inf_monitor.bb_get(u.temp_code),inf_monitor.bb_get(u.code) order by 1;
!`

#SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set heading off
#set feedback off
#set pagesize 0
#select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
#where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
#group by d.sid,inf_monitor.bb_get(u.code) order by 1;
#exit
#!`


#### Create if user doesn't exist

$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!

spool  UserPWD_${UserName}_Check_Exists.log append
DECLARE
v_count       INTEGER := 0;
sql_stmt   VARCHAR2 (1000);
BEGIN

SELECT COUNT(*) INTO v_count FROM dba_users WHERE username = UPPER ('${UserName}');

IF v_count = 0
THEN

sql_stmt := 'create user ${UserName} PROFILE ${UserProfile} IDENTIFIED BY ${UserPWD} PASSWORD EXPIRE DEFAULT TABLESPACE USERS QUOTA UNLIMITED ON USERS TEMPORARY TABLESPACE  TEMP ACCOUNT UNLOCK';

execute immediate sql_stmt;
execute immediate 'GRANT connect, create session TO ${UserName}';
--execute immediate 'GRANT EA_DBA_ROLE TO ${UserName}';
execute immediate 'GRANT ${UserRole} TO ${UserName}';
execute immediate 'audit all by ${UserName} by access';
execute immediate 'audit update table, insert table, delete table, execute procedure by ${UserName} by access';

ELSE
 sql_stmt := 'alter user ${UserName} identified by ${UserPWD} account unlock password expire profile ${UserProfile}';
execute immediate sql_stmt;
END IF;

v_count := 0;

EXCEPTION
 WHEN OTHERS
   THEN
       DBMS_OUTPUT.put_line (SQLERRM);
       DBMS_OUTPUT.put_line ('   ');
END;
/

spool off
!

done

echo ""
print "Your Request has been processed Successfully..."

exit 0

