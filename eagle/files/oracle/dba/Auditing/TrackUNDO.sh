#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/25/2011                                                                 #
# Usage: TrackUNDO.sh <SID>                                                        #
# Program: This script captures undo statement into sql_redoundo table under       #
#			audit_undo schema, and is used for audit reporting for all the         #
#			update/insert/deletes within the script.		                       #
####################################################################################
function SendNotification {

        # Uncomment for debug
         #set -x

        Machine=`uname -a | awk '{print $2}'`
        # Add any additional information to mail message
        if [[ x$1 != 'x' ]]; then
                echo -e "\n\n$1\n" >> mail.dat
        fi

        cat mail.dat | mail -s "TrackUNDO.sh failure on ${Machine}" nsarri@eagleaccess.com
        rm mail.dat
        return 0
}

function Chk_database {
   if $(ps -ef | grep pmon | grep ${sid} | grep -v grep >/dev/null 2>&1); then
      return 0
   else
      echo "Given SID is not valid..."
      exit 0
   fi
}


function RunUpdateDB {
set -x

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set linesize 200
col sql_redo format a55
col sql_undo for a55
col OPERATION for a20

ALTER SESSION SET nls_date_format='DD-MON-RRRR hh24:mi:ss';
BEGIN
        DBMS_LOGMNR.START_LOGMNR(
            STARTTIME => '${starttime}' ,
            ENDTIME => '${endtime}',
            OPTIONS => DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG +
                DBMS_LOGMNR.CONTINUOUS_MINE +
		DBMS_LOGMNR.COMMITTED_DATA_ONLY +
		DBMS_LOGMNR.PRINT_PRETTY_SQL);
END;
/

insert into audit_undo.sql_redoundo (SELECT username,scn,timestamp,operation, sql_redo,sql_undo,AUDIT_SESSIONID,xid FROM v\\$logmnr_contents where 
operation in ('INSERT','UPDATE','DELETE','MERGE') and SEG_NAME <> 'SQL_REDOUNDO' 
and username not in ('SYS','SYSTEM','UNKNOWN') and sql_undo not like '%SYS%' and username in (select username from audit_undo.undo_acl));

commit;

exec dbms_logmnr.end_logmnr;

commit;

!`

}

######## MAIN #######

sid=$1
starttime=$2
starttime="$starttime $3"
endtime=$4
endtime="$endtime $5"
Machine=`hostname`

echo $starttime
echo $endtime

. /usr/bin/setsid.sh $sid

Chk_database

RunUpdateDB

if [ $? != 0 ]
then
 SendNotification "There is an issue with TrackUNDO.sh on ${Machine} for ${sid}"
exit 1
fi

exit 0
