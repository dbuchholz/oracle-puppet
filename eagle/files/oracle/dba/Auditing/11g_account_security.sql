alter profile default limit FAILED_LOGIN_ATTEMPTS unlimited;
alter profile default limit PASSWORD_GRACE_TIME unlimited;
alter profile default limit PASSWORD_LIFE_TIME unlimited;
alter profile default limit PASSWORD_LOCK_TIME unlimited;
BEGIN
  DBMS_AUTO_TASK_ADMIN.DISABLE(
    client_name => 'auto optimizer stats collection', 
    operation => NULL, 
    window_name => NULL);
END;
/

create or replace FUNCTION ea_verify_function
(username varchar2,
  password varchar2,
  old_password varchar2)
  RETURN boolean IS
   n boolean;
   m integer;
   differ integer;
   isdigit boolean;
   ischar  boolean;
   ispunct boolean;
   digitarray varchar2(20);
   punctarray varchar2(25);
   chararray varchar2(52);
BEGIN
   digitarray:= '0123456789';
   chararray:= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   punctarray:='!"#$%&()``*+,-/:;<=>?_';
   -- Check if the password is same as the username
   IF NLS_LOWER(password) = NLS_LOWER(username) THEN
     raise_application_error(-20001, 'Password same as or similar to user');
   END IF;
   -- Check for the minimum length of the password
   IF length(password) < 8 THEN
      raise_application_error(-20002, 'Password length less than 8');
   END IF;
   -- Check if the password is too simple. A dictionary of words may be
   -- maintained and a check may be made so as not to allow the words
   -- that are too simple for the password.
   IF NLS_LOWER(password) IN ('welcome', 'database', 'account', 'user', 'password', 'oracle', 'computer', 'abcd') THEN
      raise_application_error(-20002, 'Password too simple');
   END IF;
   -- Check if the password contains at least one letter, one digit and one
   -- punctuation mark.
   -- 1. Check for the digit
   isdigit:=FALSE;
   m := length(password);
   FOR i IN 1..10 LOOP
      FOR j IN 1..m LOOP
	 IF substr(password,j,1) = substr(digitarray,i,1) THEN
	    isdigit:=TRUE;
	     GOTO findchar;
	 END IF;
      END LOOP;
   END LOOP;
   IF isdigit = FALSE THEN
      raise_application_error(-20003, 'Password should contain at least one digit, one character and one punctuation');
   END IF;
   -- 2. Check for the character
   <<findchar>>
   ischar:=FALSE;
   FOR i IN 1..length(chararray) LOOP
      FOR j IN 1..m LOOP
	 IF substr(password,j,1) = substr(chararray,i,1) THEN
	    ischar:=TRUE;
	     GOTO findpunct;
	 END IF;
      END LOOP;
   END LOOP;
   IF ischar = FALSE THEN
      raise_application_error(-20003, 'Password should contain at least one \
	      digit, one character and one punctuation');
   END IF;
   -- 3. Check for the punctuation
   <<findpunct>>
   ispunct:=FALSE;
   FOR i IN 1..length(punctarray) LOOP
      FOR j IN 1..m LOOP
	 IF substr(password,j,1) = substr(punctarray,i,1) THEN
	    ispunct:=TRUE;
	     GOTO endsearch;
	 END IF;
      END LOOP;
   END LOOP;
   IF ispunct = FALSE THEN
      raise_application_error(-20003, 'Password should contain at least one \
	      digit, one character and one punctuation');
   END IF;
   <<endsearch>>
   -- Check if the password differs from the previous password by at least
   -- 3 letters
   IF old_password IS NOT NULL THEN
     differ := length(old_password) - length(password);
     IF abs(differ) < 3 THEN
       IF length(password) < length(old_password) THEN
	 m := length(password);
       ELSE
	 m := length(old_password);
       END IF;
       differ := abs(differ);
       FOR i IN 1..m LOOP
	 IF substr(password,i,1) != substr(old_password,i,1) THEN
	   differ := differ + 1;
	 END IF;
       END LOOP;
       IF differ < 3 THEN
	 raise_application_error(-20004, 'Password should differ by at \
	 least 3 characters');
       END IF;
     END IF;
   END IF;
   -- Everything is fine; return TRUE ;
   RETURN(TRUE);
END;
/

create profile EA_PROFILE LIMIT
CPU_PER_SESSION UNLIMITED
CPU_PER_CALL UNLIMITED
SESSIONS_PER_USER UNLIMITED
FAILED_LOGIN_ATTEMPTS 3
IDLE_TIME 30
PASSWORD_LIFE_TIME 60
PASSWORD_REUSE_TIME 365
PASSWORD_REUSE_MAX 13
PASSWORD_VERIFY_FUNCTION EA_VERIFY_FUNCTION
PASSWORD_GRACE_TIME 5;

CREATE ROLE EA_DBA_ROLE;
GRANT DBA TO EA_DBA_ROLE;
GRANT ALL ON PLAN_TABLE TO EA_DBA_ROLE;

CREATE ROLE EA_INSTALL_ROLE;
grant execute on PACE_MASTERDBO.REGISTEREDITS to EA_INSTALL_ROLE;
GRANT CREATE SESSION TO EA_INSTALL_ROLE;
GRANT SELECT ANY TABLE TO EA_INSTALL_ROLE;
GRANT CREATE ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT ALTER ANY PROCEDURE TO EA_INSTALL_ROLE;
GRANT CREATE ANY VIEW TO EA_INSTALL_ROLE;

set head off 
set feed off 
set pages 0 
spool aud.lis 

select 'audit '||name||';' from system_privilege_map where 
(name like 'CREATE%TABLE%' or name like 'CREATE%INDEX%' or name like 'CREATE%CLUSTER%' 
or name like 'CREATE%SEQUENCE%' or name like 'CREATE%PROCEDURE%' 
or name like 'CREATE%TRIGGER%' or name like 'CREATE%LIBRARY%') 
union select 'audit '||name||';' from system_privilege_map where (name like 'ALTER%TABLE%' 
or name like 'ALTER%INDEX%' or name like 'ALTER%CLUSTER%' or name like 'ALTER%SEQUENCE%' 
or name like 'ALTER%PROCEDURE%' or name like 'ALTER%TRIGGER%' or name like 'ALTER%LIBRARY%') 
union select 'audit '||name||';' from system_privilege_map where (name like 'DROP%TABLE%' 
or name like 'DROP%INDEX%' or name like 'DROP%CLUSTER%' or name like 'DROP%SEQUENCE%' 
or name like 'DROP%PROCEDURE%' or name like 'DROP%TRIGGER%' or name like 'DROP%LIBRARY%') 
union select 'audit '||name||';' from system_privilege_map where (name like 'EXECUTE%INDEX%' 
or name like 'EXECUTE%LIBRARY%'  or name like 'EXECUTE%PROCEDURE%' 
or name like 'CREATE%SESSION%') 
/ 
spool off  

@aud.lis

CREATE ROLE READONLY;
GRANT SELECT ANY TABLE TO READONLY;
GRANT SELECT ANY DICTIONARY TO READONLY;
GRANT CREATE SESSION TO READONLY;
GRANT CONNECT TO READONLY;

alter database add supplemental log data;
alter database add supplemental log data(primary key) columns;

alter system set audit_trail=db_extended scope=SPFILE ;
alter system set audit_sys_operations=TRUE scope=SPFILE;
create pfile from spfile;
