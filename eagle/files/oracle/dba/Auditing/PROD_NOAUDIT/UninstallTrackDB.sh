#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/14/2011                                                                 #
# Usage: ImplementTrackDB.sh <SID>                                                 #
# Program: This script prepares the database for capturing REDO UNDO information   #
####################################################################################

function Chk_database {
   if $(ps -ef | grep pmon | grep ${sid} | grep -v grep >/dev/null 2>&1); then
      return 0
   else
      echo "Given SID is not valid..."
      exit 0
   fi
}


function RunUpdateDB {
set -x

DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set echo off
set feedback off
set head off
select name from v\\$datafile where name like '%02%' and rownum < 2;
!`

dname=`dirname ${DIR_NAME}`

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!

drop user audit_undo cascade;

drop table audit_undo.sql_redoundo;


exit
!`

}

######## MAIN #######

sid=$1

. /usr/bin/setsid.sh $sid

Chk_database

RunUpdateDB

exit 0
