#!/bin/ksh
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 11/14/2011                                                                 #
# Usage: 10g_ImplementAuditing.sh <SID>                                            #
# Program: This script implements auditing for a given sid                         #
####################################################################################
#set -x

NARG=$#
ORA_SID=$1

## Checking for input parameter

if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./10g_ImplementAuditing.sh <SID>"
        echo ""
        echo "Your <SID> options are:"
   for sid in `ps -ef | grep ora_pmon_ | awk '{print $8}'`
    do
     s_sid=`expr substr $sid 10 10`
     echo "     "$s_sid
    done
     echo ""
        exit 1
fi

. /usr/bin/setsid.sh ${ORA_SID}

export ORACLE_SID=${ORA_SID}
REPORT_DIR=/u01/app/oracle/local/dba/Auditing/result/${ORA_SID}
if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi
RESULT=${REPORT_DIR}/ImplementAuditing_${ORACLE_SID}_`date +\%d_\%m_\%Y`.log

typeset -u v2
v1=${ORA_SID}
v2=${v1}
echo "                                       REPORT FOR DATABASE => ${v2}                           " > ${RESULT}

STATUS=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

spool ${RESULT} append
set linesize 200
set pages 200
set head on

noaudit insert, update, delete on sys.aud$;
noaudit create session whenever not successful;

noaudit all;
noaudit all privileges;

drop profile EA_PROFILE;
drop ROLE EA_DBA_ROLE;
drop ROLE EA_INSTALL_ROLE;

noaudit ALTER ANY CLUSTER;
noaudit ALTER ANY INDEX;
noaudit ALTER ANY INDEXTYPE;
noaudit ALTER ANY LIBRARY;
noaudit ALTER ANY PROCEDURE;
noaudit ALTER ANY SEQUENCE;
noaudit ALTER ANY TABLE;
noaudit ALTER ANY TRIGGER;
noaudit ALTER TABLESPACE;
noaudit CREATE ANY CLUSTER;
noaudit CREATE ANY INDEX;
noaudit CREATE ANY INDEXTYPE;
noaudit CREATE ANY LIBRARY;
noaudit CREATE ANY PROCEDURE;
noaudit CREATE ANY SEQUENCE;
noaudit CREATE ANY TABLE;
noaudit CREATE ANY TRIGGER;
noaudit CREATE CLUSTER;
noaudit CREATE INDEXTYPE;
noaudit CREATE LIBRARY;
noaudit CREATE PROCEDURE;
noaudit CREATE SEQUENCE;
noaudit CREATE TABLE;
noaudit CREATE TABLESPACE;
noaudit CREATE TRIGGER;
noaudit DROP ANY CLUSTER;
noaudit DROP ANY INDEX;
noaudit DROP ANY INDEXTYPE;
noaudit DROP ANY LIBRARY;
noaudit DROP ANY PROCEDURE;
noaudit DROP ANY SEQUENCE;
noaudit DROP ANY TABLE;
noaudit DROP ANY TRIGGER;
noaudit DROP TABLESPACE;
noaudit EXECUTE ANY INDEXTYPE;
noaudit EXECUTE ANY LIBRARY;
noaudit EXECUTE ANY PROCEDURE;


Alter database drop supplemental log data;
Alter database drop supplemental log data(primary key) columns;

@DropInstallUsers.sql

spool off

exit
!`

exit 0

