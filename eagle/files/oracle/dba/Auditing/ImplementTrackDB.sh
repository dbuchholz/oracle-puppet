#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 02/14/2011                                                                 #
# Usage: ImplementTrackDB.sh <SID>                                                 #
# Program: This script prepares the database for capturing REDO UNDO information   #
####################################################################################

function Chk_database
{
 if $(ps -ef | grep ora_smon_ | grep ${sid} | grep -v grep >/dev/null 2>&1)
 then
  return 0
 else
  print "Given SID is not valid..."
  return 1
 fi
}


function RunUpdateDB
{
DIR_NAME=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<EOF
set echo off feedback off heading off
select name from v\\$datafile where rownum < 2;
EOF`
dname=$(dirname $DIR_NAME)

print "create tablespace audit_undo_ts1 datafile '${dname}/AUDIT_UNDO_TS1_01.dbf' size 50M autoextend on next 50M maxsize unlimited extent management local;" > /tmp/${ORACLE_SID}_audit_undo_ts1.sql

mkdir -p /u01/app/oracle/audit_${ORACLE_SID}
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
drop tablespace audit_undo_ts1 including contents and datafiles;
@/tmp/${ORACLE_SID}_audit_undo_ts1.sql
drop table audit_undo.sql_redoundo;
create table audit_undo.sql_redoundo(username varchar2(30),sql_undo varchar2(4000));
execute dbms_logmnr_d.build('dictionary.ora','/u01/app/oracle/audit_${ORACLE_SID}',options => dbms_logmnr_d.store_in_flat_file);
EOF

rm -f /tmp/${ORACLE_SID}_audit_undo_ts1.sql
}

######## MAIN #######

sid=$1

export ORAENV_ASK=NO
export ORACLE_SID=$sid
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

Chk_database
if [ $? -ne 0 ]
then
 exit 1
fi

RunUpdateDB

exit 0
