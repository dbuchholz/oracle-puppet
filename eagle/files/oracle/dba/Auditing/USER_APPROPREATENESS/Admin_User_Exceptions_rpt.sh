#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 07/20/2012																							 #
#Usage : Admin_User_Exceptions_rpt.sh     																	 #
#Purpose: Report on all Database where the users do not complies with the security standards.                #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=/tmp/dba_users_exception_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select d.tnsname from inf_monitor.databases d
where d.dataguard='N' order by d.tnsname;

exit
!`

rm $REPORT_DIR/Admin_User_Exceptions.txt
RESULT=$REPORT_DIR/Admin_User_Exceptions.txt
touch $REPORT_DIR/Admin_User_Exceptions.txt

echo "                                     ADMIN USERS EXCEPTION REPORT    " >> $REPORT_DIR/Admin_User_Exceptions.txt
echo "     "  >> $REPORT_DIR/Admin_User_Exceptions.txt
echo "     "   >> $REPORT_DIR/Admin_User_Exceptions.txt

for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYS' and d.sid='${line}' 
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`

v_count=`$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!
set head off
set feedback off

select count(*) 
from dba_users a, dba_role_privs b
where
a.username=b.grantee and
b.granted_role in ('EA_DBA_ROLE','DBA') and
username not in ('SYS','SYSTEM', 'WKSYS', 'PERFSTAT', 'SYSMAN',
'FDAVIS','INF_MONITOR','JPAWAR','LWHITMORE','MBUOTTE','MHILLWIG','NKULKARNI',
'NSARRI','PKRISHNAN','RMADHAVI','SVYAHALKAR','PGOMASE','PJODHE','HDESHMUKH','HDUTTA','PTAUFIQ','PBHOSALE') order by username,GRANTED_ROLE;


!`

if [ ${v_count} -ne 0 ]
 then
 echo ""
else
 continue
fi

echo "For Database : $line" >> $REPORT_DIR/Admin_User_Exceptions.txt


$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set head off
spool $REPORT_DIR/Admin_User_Exceptions.txt append

select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';

exit
!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a25
col account_status for a15
set pagesize 200
set linesize 200
break on username
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/Admin_User_Exceptions.txt append

select a.username,b.granted_role
from dba_users a, dba_role_privs b
where
a.username=b.grantee and
b.granted_role in ('EA_DBA_ROLE','DBA') and
username not in ('SYS','SYSTEM', 'WKSYS', 'PERFSTAT', 'SYSMAN',
'BKHAN','DAMBEKAR','FDAVIS','INF_MONITOR','JPAWAR','LWHITMORE','MBUOTTE','MHILLWIG','NKULKARNI',
'NSARRI','PKRISHNAN','RMADHAVI','SVYAHALKAR','PGOMASE'
) order by username,GRANTED_ROLE;


set pagesize 0

select ' ' from dual;
select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="ADMIN USER_LISTING. Best viewed with WordPad....."
Message="Report on admin users in all databases ..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

