#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 06/20/2012																							 #
#Usage : NonAdmin_user_imp_rpt.sh      																		 #
#Purpose: Check on all Database where the users do not complies with the security standards.                 #
#         /u01/app/oracle/local/dba/Auditing/reports/User_Appropriateness directory.                         #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=/tmp/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select d.tnsname from inf_monitor.databases d
where d.dataguard='N' and (environment like '%TEST%' or environment like '%DEV%')
order by d.tnsname;

exit
!`

rm $REPORT_DIR/NonAdmin_user_imp.txt
RESULT=$REPORT_DIR/NonAdmin_user_imp.txt
touch $REPORT_DIR/NonAdmin_user_imp.txt

echo "                                     NON ADMIN USERS REPORT IMPLEMENTATION   " >> $REPORT_DIR/NonAdmin_user_imp.txt
echo "     "  >> $REPORT_DIR/NonAdmin_user_imp.txt
echo "     "   >> $REPORT_DIR/NonAdmin_user_imp.txt

for line in `echo ${Sid_List}`; do

echo "For Database : $line" >> $REPORT_DIR/NonAdmin_user_imp.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYS' and d.sid not like '%mfc%' and d.sid='${line}' 
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set head off
spool $REPORT_DIR/NonAdmin_user_imp.txt append

select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';

exit
!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a25
col account_status for a15
set pagesize 200
set linesize 200
break on username
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/NonAdmin_user_imp.txt append

select a.username,b.granted_role
from dba_users a, dba_role_privs b
where
--a.profile != 'EA_PROFILE' and
a.username=b.grantee and
b.granted_role not in ('EA_DBA_ROLE','DBA','CONNECT','RESOURCE') and
username not in ('SYS','SYSTEM','ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS',
'HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS',
'ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS',
'QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','OPS$ORACLE',
'WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','ARCH_CTRLCTR', 'CASHDBO', 'CTRLCTR', 'DATAEXCHDBO',
'DATAMARTDBO', 'EAGLEKB', 'EAGLEMGR', 'ESTAR', 'HOLDINGDBO', 'LEDGERDBO', 'MSGCENTER_DBO',
'PACE_MASTERDBO','AUDIT_UNDO', 'ISTAR','SPATIAL_CSW_ADMIN_USR','SPATIAL_WFS_ADMIN_USR',
'PERFSTAT','PERFORMDBO', 'PRICING', 'RULESDBO', 'SCRUBDBO', 'SECURITYDBO', 'SYSMAN', 'TRADESDBO') order by username,GRANTED_ROLE;

set pagesize 0

select ' ' from dual;
select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="USER_APPROPRIATENESS EXCEPTION REPORT. Best viewed with WordPad....."
Message="Report on user appropriateness exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

