#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri											     #
#Date  : 06/20/2012											     #
#Usage : Drop_Roles_NOT_Assigned.sh <SID>								     #
##############################################################################################################
#set -x

EMAIL=N
REPORT_DIR=/tmp/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi



ARG=$#
if [ ${ARG} -ne 1 ]; then
        clear
        echo "Not enough parameter..."
        echo ""
        echo ""
        echo "Usage: MultipleUpdates.sh <ScriptName> <SID>"
        exit 1
fi

STATUS=`ps -fu oracle |grep -v grep| grep ora_pmon_$1`
if [ $? != 0 ]; then
     echo -e "Database $1 is not running. Please try with valid SID..."
     exit 1
fi

. /usr/bin/setsid.sh $1

rm $REPORT_DIR/List_Roles_NOT_Assigned.txt
RESULT=$REPORT_DIR/List_Roles_NOT_Assigned.txt
touch $REPORT_DIR/List_Roles_NOT_Assigned.txt

echo "--                                  ROLES NOT ASSIGNED TO ANY USER    " >> $REPORT_DIR/List_Roles_NOT_Assigned.txt
echo "                                    ------------------------------     "  >> $REPORT_DIR/List_Roles_NOT_Assigned.txt

Roles_NOT_Assigned_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set feedback off
set head off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a35
col account_status for a15
col typ for a8
col role for a25
col PACE_LVer for a10
col PACE_UVer for a10
col STAR_LVer for a10
col STAR_UVer for a12
set pagesize 200
set linesize 200
--break on DataBase on username on created on default_tablespace on account_status on profile on granted_role
break on DataBase on PACE_LVer on PACE_UVer on STAR_LVer on STAR_UVer on grantee on role on typ
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';


spool $REPORT_DIR/List_Roles_NOT_Assigned.txt append

--Who is granted this ROLE:

select 'drop role '||r.ROLE||';' from dba_roles r
where r.role not in (select granted_role from dba_role_privs where grantee not in ('SYS','SYSTEM')) and r.role not in ('AQ_ADMINISTRATOR_ROLE','SELECT_CATALOG_ROLE',
'SCHEDULER_ADMIN','RESOURCE','RECOVERY_CATALOG_OWNER','OLAP_USER','OLAP_DBA','OEM_MONITOR','OEM_ADVISOR','LOGSTDBY_ADMINISTRATOR','JAVASYSPRIV','JAVADEBUGPRIV',
'WKUSER','IMP_FULL_DATABASE','EXP_FULL_DATABASE','EXECUTE_CATALOG_ROLE','DBA','AQ_ADMINISTRATOR_ROLE','CONNECT','DATAPUMP_EXP_FULL_DATABASE','MGMT_USER','DATAPUMP_IMP_FULL_DATABASE','OPERATOR','HS_ADMIN_ROLE','ORA_ASPNET_MEM_FULLACCESS','ORA_ASPNET_ROLES_FULLACCESS','ORA_ASPNET_ROLES_REPORTACCESS','XDBADMIN','XDBADMIN','CONNECT_EAGLE','QUEST_SPC_APP_PRIV' ,'INFOSEC_OPS_ADMIN','ORACLE_OIM_OPS_ADMIN','EA_DBA_ROLE','EA_INSTALL_ROLE','READONLY','NORMAL_USER','SUPER_USER','ADM_PARALLEL_EXECUTE_TASK','AQ_USER_ROLE','DELETE_CATALOG_ROLE','EJBCLIENT','GATHER_SYSTEM_STATISTICS','GLOBAL_AQ_USER_ROLE','HS_ADMIN_EXECUTE_ROLE','HS_ADMIN_SELECT_ROLE','JAVAIDPRIV','JAVAUSERPRIV','JAVA_ADMIN','JAVA_DEPLOY','PLUSTRACE','WM_ADMIN_ROLE','AUTHENTICATEDUSER','CSW_USR_ROLE','CTXAPP','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','WFS_USR_ROLE','XDB_SET_INVOKER','XDB_WEBSERVICES','XDB_WEBSERVICES_OVER_HTTP','XDB_WEBSERVICES_WITH_PUBLIC','OLAPI_TRACE_USER','OLAP_XS_ADMIN','ORDADMIN','SPATIAL_CSW_ADMIN','SPATIAL_WFS_ADMIN','XDBWEBSERVICES') order by r.role;
spool off

!`
clear
echo ""
echo ""
echo "Please Verify $REPORT_DIR/List_Roles_NOT_Assigned.txt"
echo "and run in using SQLPLUS for database $1"
echo ""
echo ""
echo "Below is the listing of $REPORT_DIR/List_Roles_NOT_Assigned.txt"
echo ""
echo ""

cat $REPORT_DIR/List_Roles_NOT_Assigned.txt
echo ""
echo ""
echo ""
SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
to=nsarri@eagleaccess.com
subject="USER_APPROPRIATENESS EXCEPTION REPORT. Best viewed with WordPad....."
Message="Report on user appropriateness exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

