#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 02/08/2012																							 #
#Usage : Global_Disable_Inactive_account.sh     															 #
#Purpose: Check on all Databases for user who has not logged in for over 90 days and disable the account.    # 
##############################################################################################################
#set -x

NARG=$#
#NumUsers=`expr ${NARG} - 1`



. /usr/bin/setsid.sh infprd1


Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d
where d.dataguard='N' and d.sid not in ('infprd1','eabprd1')
and (environment like '%TEST%' or environment like '%DEV%')
--and environment like '%PROD%'
order by d.sid;

exit
!`

REPORT_DIR=${HOME}/local/dba/Auditing/reports/Disable_inactive_account

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi

RESULT=${REPORT_DIR}/Disable_Inactive_account_ALL_`date +"%m-%d-%Y"`.log
rm ${RESULT}
touch ${RESULT}

for SList in `echo ${Sid_List}`; do

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${SList}'
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`

$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${SList} <<!
set serveroutput on size unlimited
set head off
set feedback off
alter session set nls_date_format='MM-DD-YYYY';

spool ${RESULT} append
DECLARE
 v_count number;
 v_timestamp varchar2(10);
 sql_stmt varchar2(1000);
BEGIN
 dbms_output.put_line('Checking for the database => '||'${SList}');
 FOR rec in (select distinct(user_name) from dba_priv_audit_opts where USER_NAME is not null and USER_NAME <> 'INF_MONITOR') LOOP
    BEGIN
     select count(*) into v_count from dba_audit_session where username = rec.user_name;
     select max(timestamp) into v_timestamp from dba_audit_session where username = rec.user_name;
      IF v_count > 0 AND to_date(v_timestamp) <= sysdate-90 
       THEN
         sql_stmt := 'alter user '||rec.user_name||' account lock password expire';
         execute immediate sql_stmt;
         dbms_output.put_line(rec.user_name||' disabled. Last Login '||v_timestamp);
     END IF; 
    EXCEPTION
      WHEN OTHERS
       THEN
        DBMS_OUTPUT.put_line (SQLERRM);
        DBMS_OUTPUT.put_line ('   ');
   END;
 END LOOP;
END;

/

spool off
!


done

exit 0

