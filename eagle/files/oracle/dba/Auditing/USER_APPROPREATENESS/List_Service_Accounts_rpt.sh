#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 06/20/2012																							 #
#Usage : List_Service_Accounts_rpt.sh      																	 #
#Purpose: Check on all Database for Service Accounts.														 #
#         /u01/app/oracle/local/dba/Auditing/reports/User_Appropriateness directory.                         #
##############################################################################################################
set -x

v_date=`date +'%m-%d-%Y_%H%M%S'`
r_date=`date +%m-%d-%Y'`
EMAIL=Y
REPORT_DIR=/admin/arri/local/dba/Auditing/reports/Service_Accounts_Reports

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0

select d.tnsname from inf_monitor.databases d
where d.dataguard='N' and d.pace_version is not null and d.sid not in ('eabprd1','infprd1','inftst1','etlprd1')
order by d.tnsname;

exit
!`

rm $REPORT_DIR/Service_Accounts_${v_date}.txt
RESULT=$REPORT_DIR/Service_Accounts_${v_date}.txt
touch $REPORT_DIR/Service_Accounts_${v_date}.txt

echo "                                      SERVICE ACCOUNT REPORT PRODUCTION   " >> $REPORT_DIR/Service_Accounts_${v_date}.txt
echo "     "  >> $REPORT_DIR/Service_Accounts_${v_date}.txt
echo "                                               DATED: ${r_date}                         " >> $REPORT_DIR/Service_Accounts_${v_date}.txt
echo "     "   >> $REPORT_DIR/Service_Accounts_${v_date}.txt

for line in `echo ${Sid_List}`; do

echo "For Database : $line" >> $REPORT_DIR/Service_Accounts_${v_date}.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYS' and d.sid='${line}' 
group by d.sid,inf_monitor.bb_get(u.code) order by 1;
exit
!`

$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set head off
spool $REPORT_DIR/Service_Accounts_${v_date}.txt append

select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';

exit
!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a25
col account_status for a15
set pagesize 200
set linesize 200
break on username
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/Service_Accounts_${v_date}.txt append

 
select a.username,b.granted_role,c.privilege,a.default_tablespace,a.account_status,a.profile,a.created
from dba_users a, dba_role_privs b, role_sys_privs c
where
a.profile = 'DEFAULT' and
a.username=b.grantee and b.GRANTED_ROLE=c.role and
b.granted_role not in ('EA_DBA_ROLE','EA_INSTALL_ROLE','CONNECT') and
username not in ('SYS','SYSTEM','ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS',
'HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS',
'ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS',
'QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST','OPS$ORACLE',
'WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','ARCH_CTRLCTR', 'CASHDBO', 'CTRLCTR', 'DATAEXCHDBO',
'DATAMARTDBO', 'EAGLEKB', 'EAGLEMGR', 'ESTAR', 'HOLDINGDBO', 'LEDGERDBO', 'MSGCENTER_DBO',
'PACE_MASTERDBO','AUDIT_UNDO', 'ISTAR','SPATIAL_CSW_ADMIN_USR','SPATIAL_WFS_ADMIN_USR',
'PERFSTAT','PERFORMDBO', 'PRICING', 'RULESDBO', 'SCRUBDBO', 'SECURITYDBO', 'SYSMAN', 'TRADESDBO','EA_ADMIN','VAULT_READ','PACE_READ','BGIM_APP_RO','BGIM_APP_RW','PACE_DEV') order by username,GRANTED_ROLE;

set pagesize 0

select ' ' from dual;
select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
#to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
to=nsarri@eagleaccess.com
subject="SERVICE ACCOUNTS REPORT. Best viewed with WordPad....."
Message="Report Service accounts in the database..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

