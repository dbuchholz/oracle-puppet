#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 06/20/2012																							 #
#Usage : User_Appropriateness_prod_rpt.sh  																	 #
#Purpose: Report on all PROD Database where the users do not complies with the security standards.           #
#         /u01/app/oracle/local/dba/Auditing/reports/User_Appropriateness directory.                         #
##############################################################################################################
set -x

EMAIL=Y
REPORT_DIR=/tmp/audit_reports/User_Appropriateness

if  [ ! -d  ${REPORT_DIR} ]; then mkdir -p ${REPORT_DIR}; fi


. /usr/bin/setsid.sh infprd1

Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set echo off
set heading off
set feedback off
set pagesize 0
select lower(d.tnsname) from inf_monitor.databases d
where d.dataguard='N' and (environment like '%PROD%') and d.sid not in ('grcprd1','nwiprd1','nwiprd2','infprd1') and d.sid not like '%mfc%' order by d.tnsname;

exit
!`

rm $REPORT_DIR/User_Appropriateness_prod.txt
RESULT=$REPORT_DIR/User_Appropriateness_prod.txt
touch $REPORT_DIR/User_Appropriateness_prod.txt

echo "                                     USER APPROPRIATENESS EXCEPTION REPORT PRODUCTION   " >> $REPORT_DIR/User_Appropriateness_prod.txt
echo "     "  >> $REPORT_DIR/User_Appropriateness_prod.txt
echo "     "   >> $REPORT_DIR/User_Appropriateness_prod.txt

for line in `echo ${Sid_List}`; do

#echo "For Database : $line" >> $REPORT_DIR/User_Appropriateness_prod.txt

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) code
from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance
and u.db_instance=d.instance and u.username='SYS' and d.sid='${line}'
group by d.sid,decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) order by 1;
exit
!`

#$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set head off
#spool $REPORT_DIR/User_Appropriateness_prod.txt append
#
#select 'On server    : '||b.name from inf_monitor.databases a, inf_monitor.machines b where a.mac_instance=b.instance and a.tnsname='${line}';
#
#exit
#!

$ORACLE_HOME/bin/sqlplus -s "sys/${SysPwd}@${line} as sysdba" <<!

set feedback off
col username for a20
col profile for a15
col default_tablespace for a15
col granted_role for a20
col privilege for a25
col account_status for a15
set pagesize 200
set linesize 200
--break on DataBase on username on created on default_tablespace on account_status on profile on granted_role
break on DataBase on username 
alter session set nls_date_format='DD/MM/YYYY HH24:MI:SS';

spool $REPORT_DIR/User_Appropriateness_prod.txt append

 
--select distinct d.name as "DataBase",a.username,b.granted_role,c.privilege,a.default_tablespace,a.account_status,a.profile,a.created
----select distinct d.name as "DataBase",a.username,a.created,a.default_tablespace,a.account_status,a.profile,b.granted_role,c.privilege
--from dba_users a, dba_role_privs b, role_sys_privs c, v\$database d
--where
--a.profile not in ('EA_PROFILE','EA_SERVICE','EA_APPLICATION') and
--a.username=b.grantee and b.GRANTED_ROLE=c.role and
--b.granted_role not in ('EA_DBA_ROLE','EA_INSTALL_ROLE','CONNECT') and username not like '%\$%' and username not like '%DBO%' and
--username not in ('SYS','SYSTEM','ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS',
--'HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS',
--'ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS',
--'QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST',
--'WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','ARCH_CTRLCTR', 'CASHDBO', 'CTRLCTR', 'DATAEXCHDBO','DATAEXCH','EAGLE_TEMP','ORDDATA',
--'DATAMARTDBO', 'EAGLEKB', 'EAGLEMGR', 'ESTAR', 'HOLDINGDBO', 'LEDGERDBO', 'MSGCENTER_DBO','AURORA\$JIS\$UTILITY\$','ORASYS_CSRS_PROD',
--'PACE_MASTERDBO','AUDIT_UNDO', 'ISTAR','SPATIAL_CSW_ADMIN_USR','SPATIAL_WFS_ADMIN_USR','OSE\$HTTP\$ADMIN',
--'OPS\$ORACLE','PERFSTAT','SECURITIES','PERFORMDBO', 'PRICING','APPQOSSYS','EAGLEMART','XS\$NULL', 'RULESDBO', 'SCRUBDBO', 'SECURITYDBO', 'SYSMAN', 'TRADESDBO','EA_ADMIN','VAULT_READ','PACE_READ','BGIM_APP_RO','BGIM_APP_RW','PACE_DEV') order by username,GRANTED_ROLE;

select distinct d.name "DataBase",a.username,a.account_status,a.profile,a.created
from dba_users a, v\$database d
where
a.profile not in ('EA_PROFILE','EA_SERVICE','EA_APPLICATION') and username not like '%\$%' and username not like '%DBO%' and username not like '%ESTAR%' and
username not in ('SYS','SYSTEM','ANONYMOUS','CTXSYS','DBSNMP','DIP','DMSYS','EXFSYS',
'HR','LBACSYS','MDDATA','MDSYS','MGMT_VIEW','ODM','ODM_MTR','OE','OLAPSYS',
'ORDPLUGINS','ORDSYS','OUTLN','PM','QS','QS_ADM','QS_CB','QS_CBADM','QS_CS',
'QS_ES','QS_OS','QS_WS','RMAN','SCOTT','SH','SI_INFORMTN_SCHEMA','TSMSYS','WK_TEST',
'WKPROXY','WKSYS','WMSYS','XDB','ORACLE_OCM','ARCH_CTRLCTR', 'CASHDBO', 'CTRLCTR', 'DATAEXCHDBO','DATAEXCH','ORASYS_CSRS_PROD',
'DATAMARTDBO', 'EAGLEKB', 'EAGLEMGR', 'ESTAR', 'HOLDINGDBO', 'LEDGERDBO', 'MSGCENTER_DBO','AURORA\$JIS\$UTILITY\$','OSE\$HTTP\$ADMIN','EAGLE_TEMP','ORDDATA',
'PACE_MASTERDBO','SECURITIES','AUDIT_UNDO', 'ISTAR','SPATIAL_CSW_ADMIN_USR','SPATIAL_WFS_ADMIN_USR','AURORA\$JIS\$UTILITY\$',
'OPS\$ORACLE','PERFSTAT','PERFORMDBO','PRICING','XS\$NULL','APPQOSSYS','EAGLEMART','RULESDBO', 'SCRUBDBO', 'SECURITYDBO', 'SYSMAN', 'TRADESDBO') order by username;

set pagesize 0

--select ' ' from dual;
--select ' ' from dual;

spool off

!

done

SHORT_NAME=`echo -e ${RESULT} | awk -F "/" '{print $NF}'`

if [ "$EMAIL" = "Y" ]
then
#to=team_dba@eagleaccess.com
to=nsarri@eagleaccess.com,lwhitmore@eagleinvsys.com
#to=nsarri@eagleaccess.com
subject="USER_APPROPRIATENESS EXCEPTION REPORT. Best viewed with WordPad....."
Message="Report on user appropriateness exception..."
#mailx -s "$subject" $to < ${RESULT}
uuencode ${RESULT} ${SHORT_NAME} | mailx -s "${subject}" $to $cc
echo "Emailed report"
fi


exit 0

