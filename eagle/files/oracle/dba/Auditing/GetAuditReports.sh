#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/10/2011                                                                 #
# Usage: GetAuditReports.sh                                                        #
# Program: This script pulls Audit Reports from all the Dev and Test servers and   #
#          Stores on Hawaii under a given directory and under its own SID.         #
####################################################################################


GetAuditReports() {
set -x

REMOTE_AUDIT_DIR=/u01/app/oracle/local/dba/Auditing/reports

# Get a list of Machines

HOST_LIST=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set heading off
set feedback off
set pagesize 0
select rtrim(m.name) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance and m.location in ('EVERETT IMP','PITTSBURGH IMP','NEWTON') and m.name='capecod' 
group by m.name order by 1;

!`

for line in `echo ${HOST_LIST}`; do

   for rsid in `ssh ${line} ps -ef |grep pmon|grep -v grep| awk -F_ '{print $3}'`; do
  
		LOCAL_AUDIT_DIR=$HOME/local/dba/AuditReports/${rsid}
		if  [ ! -d  ${LOCAL_AUDIT_DIR} ]; then mkdir -p ${LOCAL_AUDIT_DIR}; fi

		for rptfile in `ssh ${line} find  ${REMOTE_AUDIT_DIR}/Audit_*_${rsid}*.txt -mtime 160`; do
    	  echo "scp ${line}:${REMOTE_AUDIT_DIR}/${rptfile} ${LOCAL_AUDIT_DIR}"
    	done

   done
 
done
}


######## MAIN #######

. /usr/bin/setsid.sh infprd1

GetAuditReports

exit 0 
