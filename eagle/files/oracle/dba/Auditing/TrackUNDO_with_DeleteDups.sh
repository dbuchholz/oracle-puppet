#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/25/2011                                                                 #
# Usage: TrackUNDO.sh <SID>                                                        #
# Program: This script captures undo statement into sql_redoundo table under       #
#			audit_undo schema, and is used for audit reporting for all the         #
#			update/insert/deletes within the script.		                       #
####################################################################################

function Chk_database {
   if $(ps -ef | grep pmon | grep ${sid} | grep -v grep >/dev/null 2>&1); then
      return 0
   else
      echo "Given SID is not valid..."
      exit 0
   fi
}


function RunUpdateDB {
set -x

STATUS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
BEGIN
        DBMS_LOGMNR.START_LOGMNR(
            STARTTIME => SYSDATE - 6/24 ,
            ENDTIME => sysdate,
            OPTIONS => DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG +
                DBMS_LOGMNR.CONTINUOUS_MINE +
		DBMS_LOGMNR.COMMITTED_DATA_ONLY +
		DBMS_LOGMNR.PRINT_PRETTY_SQL);
END;
/

delete from audit_undo.sql_redoundo where undo_date <= sysdate-2;

commit;

insert into audit_undo.sql_redoundo (SELECT username,scn,timestamp,operation, sql_redo,sql_undo,AUDIT_SESSIONID,xid FROM v\\$logmnr_contents where 
operation in ('INSERT','UPDATE','DELETE','MERGE') and SEG_NAME <> 'SQL_REDOUNDO' 
and username not in ('SYS','SYSTEM','UNKNOWN') and sql_undo not like '%SYS%' and username in (select username from audit_undo.undo_acl));

commit;

DELETE FROM  audit_undo.sql_redoundo A
WHERE
  a.rowid >  ANY (
     SELECT  B.rowid FROM  audit_undo.sql_redoundo B
     WHERE
        A.sql_redo = B.sql_redo
     AND
        A.sql_undo = B.sql_undo
        );

commit;


exec dbms_logmnr.end_logmnr;


!`

}

######## MAIN #######

sid=$1

. /usr/bin/setsid.sh $sid

Chk_database

RunUpdateDB

exit 0
