#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 09/23/2011                                                                 #
# Usage: ResetUserPassword.sh <List of users separated by space>                   #
# Program: This script resets the password for given users                         #
#                    															   #
####################################################################################

GetPass() {
#set -x
MAXSIZE=8
array1=( 
q w e r t y u i o p a s d f g h j k l z x c v b n m Q W E R T Y U I O P A S D 
F G H J K L Z X C V B N M 1 2 3 4 5 6 7 8 9 0 \_ 
) 
unset res
i=0
MODNUM=${#array1[*]} 
pwd_len=0 
while [ $pwd_len -lt $MAXSIZE ] 
do 
    index=$(($RANDOM%$MODNUM)) 
str[i]=${array1[$index]} 
    ((pwd_len++)) 
    ((i++))
done 
for j in "${str[@]}"; do
res=${res}$j
done
}

ResetUserPassWD() {
#set -x
usr=${i}
usrprofile=${UserProfile}
usrrole=${UserRole}

j=0
until (test $j -eq 1)
do
GetPass
echo ${res} |grep [0-9] > /dev/null
if [[ $? -eq 0 ]] ; then
 j=1
fi
done

pwd=${res}
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
WHENEVER SQLERROR EXIT FAILURE;
SET SERVEROUTPUT ON SIZE UNLIMITED;
SET FEEDBACK OFF;
DECLARE
	err_num NUMBER;
    err_msg VARCHAR2(100);
	cnt number;
	sql_stmt varchar2(1000);
BEGIN
  select count(*) into cnt from dba_users where username = upper('${usr}');
  --dbms_output.put_line('The name is ${usr}');
  if cnt <> 0 then
  sql_stmt := 'alter user ${usr} IDENTIFIED BY ${usr}_${pwd} PASSWORD EXPIRE ACCOUNT UNLOCK';
  --dbms_output.put_line(sql_stmt);
   execute immediate sql_stmt;
   execute immediate 'audit all by ${usr} by access';
   execute immediate 'audit update table, insert table, delete table, execute procedure by ${usr} by access';
   dbms_output.put_line('Please send this username and password to the requester...');
   dbms_output.put_line('USERNAME: ${usr}');
   dbms_output.put_line('PASSWORD: ${usr}_${pwd}');
 else
  dbms_output.put_line('User ${usr} does not exists within this database.....');
  end if;
EXCEPTION
 WHEN OTHERS THEN 
  err_num := SQLCODE;
  err_msg := SUBSTR(SQLERRM, 1, 100);
  dbms_output.put_line('Error Code: '||err_num);
  dbms_output.put_line('Error Message: '||err_msg);
  dbms_output.put_line('There was an error with the Password. The Generated password is: ${usr}_${pwd}');
END;
/
exit
!

if [ $? -ne 0 ]; then
 echo "Problem Resetting the password for this user in ${ORA_SID} ....." 
 exit 1
fi

}


######## MAIN #######

NARG=$#
NumUsers=`expr ${NARG} - 1`
echo $*
if [ ${NARG} -eq 0 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./CreateDBAUser.sh <USER1> <USER2> <...>"
        echo ""
        exit 1
fi

clear
echo "        "
for sid in `ps -ef | grep -v grep |grep ora_pmon_ | awk '{print $8}'`
    do
     s_sid=`expr substr $sid 10 10`
     echo "                                                   "$s_sid
done
echo "     "
read ORA_SID?"                                                Enter the SID: "

STATUS=`ps -fu oracle |grep -v grep| grep ora_pmon_${ORA_SID}`
if [ $? != 0 ]; then
     echo -e "Database ${ORACLE_SID} is not running. Please try with valid SID..."
     exit 1
fi

. /usr/bin/setsid.sh ${ORA_SID}

clear

clear
for i in $@
do
ResetUserPassWD
read dummy?"Hit Enter to Proceed.."
done

exit 0 
