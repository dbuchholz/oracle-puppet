#!/bin/ksh
#set -x
####################################################################################
# Author: Nirmal S Arri                                                            #
# Date: 01/10/2011                                                                 #
# Usage: ChangePWD_PRD.sh <USR> (SYS or SYSTEM)                                    #
# Program: This script changes SYS and SYSTEM password on all the Production       #
#          Databases.															   #
####################################################################################

GetPass() {
#set -x
MAXSIZE=8
array1=( 
q w e r t y u i o p a s d f g h j k l z x c v b n m Q W E R T Y U I O P A S D 
F G H J K L Z X C V B N M 1 2 3 4 5 6 7 8 9 0 \_ \!
) 
unset res
i=0
MODNUM=${#array1[*]} 
pwd_len=0 
while [ $pwd_len -lt $MAXSIZE ] 
do 
    index=$(($RANDOM%$MODNUM)) 
str[i]=${array1[$index]} 
    ((pwd_len++)) 
    ((i++))
done 
for j in "${str[@]}"; do
res=${res}$j
done
}


UpdatePWD() {
#set -x

AUDIT_DIR=$HOME/local/dba/Auditing

# Get a list of Machines

HOST_LIST=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set heading off
set feedback off
set pagesize 0
select rtrim(m.name) from inf_monitor.databases d, inf_monitor.machines m
where m.instance=d.mac_instance and m.location in ('EVERETT IMP','PITTSBURGH IMP','NEWTON') 
   and d.dataguard='N' 
group by m.name order by 1;

!`

# Get a list of servername:sid 

HOSTS=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
set heading off   
set feedback off
set pagesize 0
 select rtrim(m.name)||':'||rtrim(d.sid) from inf_monitor.databases d, inf_monitor.machines m
   where m.instance=d.mac_instance  and m.location in ('EVERETT IMP','PITTSBURGH IMP','NEWTON') and d.sid='bactst1'
     and d.dataguard='N'
 group by m.name,d.sid order by 1;
!`


for line in `echo ${HOST_LIST}`; do
	HOSTNAMES=`echo $line | awk  '{print $1}'`
	#HOSTNAMES=`echo $line | sed s/:.*//g`;
	#ssh $HOSTNAMES if [ ! -d  ${AUDIT_DIR} ] then mkdir -p ${AUDIT_DIR} fi 
	#echo "scp ${AUDIT_DIR}/DBPWDChange.sh ${HOSTNAMES}:${AUDIT_DIR}" >> ${LOGFILE}
	scp ${AUDIT_DIR}/DBPWDChange.sh ${HOSTNAMES}:${AUDIT_DIR} 
         if [ $? -ne 0 ]; then
          echo "There was a problem copying DBPWDChange.sh on $HOSTNAMES ..." >> ${ERRFILE}
          continue
         fi
done


for line in `echo $HOSTS`; do
GetPass
pwd=${res}

		HOSTNAMES=`echo -n $line | sed s/:.*//g`;
		SID=`echo $line | awk -F ":" '{print $2}'`
       	#echo "ssh $HOSTNAMES local/dba/Auditing/DBPWDChange.sh ${SID} ${pwd}" >> ${LOGFILE}
       	#ssh $HOSTNAMES local/dba/Auditing/DBPWDChange.sh ${SID} ${USR} ${pwd} >> ${LOGFILE}

         if [ $? -ne 0 ]; then
          echo "There was a problem running DBPWDChange.sh for $SID on $HOSTNAMES ..." >> ${ERRFILE}
          continue
         fi

#### Run DBPWDChange.sh to update the sys and system passwords #####

echo "Running BigBrother Update..." >> ${LOGFILE}

#$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<!
#WHENEVER SQLERROR EXIT FAILURE
#update inf_monitor.user_codes set code=inf_monitor.bb_store('${pwd}') where db_instance=(select instance from inf_monitor.databases where sid='${SID}') and username='SYSTEM';
#
#commit;
#
#!
if [ $? -ne 0 ]; then
 echo "Problem updating BigBrother Database for ${SID}..." >> ${ERRFILE}
 continue
fi

done
}


######## MAIN #######

. /usr/bin/setsid.sh infprd1

NARG=$#
if [ ${NARG} -ne 1 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./ChangePWD_PRD.sh <USR>"
        echo ""
        echo "Your <USR> options are: SYS or SYSTEM"
        exit 1
fi

USR=$1

clear
echo ""
echo ""
echo "Are you sure, you wanted to process $1 (y/n)?:"
read yes
if [ $yes = "y" ] || [ $yes = "Y" ]; then
echo "Processing for user ${USR}......."
else
echo ""
echo "Thanks you.. See you next time...."
exit 1
fi


LOGFILE=$HOME/local/dba/Auditing/log/ChangeSYSTEM_PWD_`date +"%m-%d-%y_%T"`.log
ERRFILE=$HOME/local/dba/Auditing/log/ChangeSYSTEM_PWD_`date +"%m-%d-%y_%T"`.err

echo "Start Time: `date +"%m-%d-%y_%T"`" > ${LOGFILE}

UpdatePWD

echo "End Time: `date +"%m-%d-%y_%T"`" >> ${LOGFILE}

exit 0 
