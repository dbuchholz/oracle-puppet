#!/bin/ksh
# Script name: flashback_database.ksh
# Usage: flashback.ksh [ Oracle SID ]

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

this_host=$(hostname | awk -F. '{print $1}')
location=$(grep $ORACLE_SID /etc/hosts | awk '{print $NF}' | awk -F- '{print $NF}')
if [[ -n $location ]]
then
 if [ "$location" != "dr" ]
 then
  print "$ORACLE_SID Database on $this_host Server is not a DR database"
  exit 3
 fi
else
 print "$ORACLE_SID Database on $this_host Server is not a DR database"
 exit 4
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

print "Shut down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate
EOF
if [ $? -ne 0 ]
then
 print "Error shutting down database."
 print "Aborting here."
 exit 5
fi

print "Start database in mount phase"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error during startup mount."
 print "Aborting here."
 exit 6
fi

print "Flashback database to previously defined restore point"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
flashback database to restore point in_dataguard;
EOF
if [ $? -ne 0 ]
then
 print "Error during flashback of database to restore point."
 print "Aborting here."
 exit 7
fi

print "Convert database back to physical standby database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
alter database convert to physical standby;
EOF
if [ $? -ne 0 ]
then
 print "Error during converting database to physical standby."
 print "Aborting here."
 exit 8
fi

print "$ORACLE_SID Database has been flashed back to restore point and is now a physical standby database."
exit 0
