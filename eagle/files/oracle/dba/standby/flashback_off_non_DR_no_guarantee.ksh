#!/bin/ks
# Script name: flashback_off_non_DR_no_guarantee.ksh
# Usage: flashback_off_non_DR_no_guarantee.ksh [ Oracle SID ]

if [ $# -ne 1 ]
then
 print "\n\t\tInvalid Arguments!"
 print "\t\tUsage : $0 <Oracle SID>\n"
 exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

print "Determine the location of the flashback area"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$script_dir/${ORACLE_SID}_find_log_dest.log
set echo off pages 0 trimspool on feedback off
WHENEVER SQLERROR EXIT FAILURE
select value from v\$parameter where name='log_archive_dest_1';
EOF
if [ $? -ne 0 ]
then
 print "Could not get the value of log_archive_dest_1 for $ORACLE_SID"
 print "This value is needed to locate/create flash recovery area."
 print "Aborting here."
 exit 3
fi
awk -F= '{print $2}' $script_dir/${ORACLE_SID}_find_log_dest.log > $script_dir/${ORACLE_SID}_find_log_dest_2.log
flashback_area="$(dirname $(cat $script_dir/${ORACLE_SID}_find_log_dest_2.log))/flash_recovery"
rm -f $script_dir/${ORACLE_SID}_find_log_dest.log $script_dir/${ORACLE_SID}_find_log_dest_2.log

print "Shut down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate
EOF
if [ $? -ne 0 ]
then
 print "Error shutting down database."
 print "Aborting here."
 exit 4
fi

print "Start database in mount phase"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error during startup mount."
 print "Aborting here."
 exit 5
fi

print "Turn flashback off"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE FLASHBACK OFF;
EOF
if [ $? -ne 0 ]
then
 print "Error on turning flashback database off."
 print "Aborting here."
 exit 6
fi

print "Drop restore point, unset DB_RECOVERY_FILE_DEST_SIZE, db_flashback_retention_target and db_recovery_file_dest"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
alter system reset DB_RECOVERY_FILE_DEST_SIZE scope=spfile sid='*';
alter system reset db_flashback_retention_target scope=spfile sid='*';
alter system reset db_recovery_file_dest scope=spfile sid='*';
EOF

print "Shut down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
shutdown immediate 
EOF

print "Start database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 7
fi

print "Query flashback state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
SELECT FLASHBACK_ON FROM V\$DATABASE;
EOF
if [ $? -ne 0 ]
then
 print "Error on selecting flashback status."
fi

print "Remove the flashback area"
rm -r $flashback_area

exit 0
