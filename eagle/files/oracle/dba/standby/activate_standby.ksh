#!/bin/ksh
# Script Name: activate_standby.ksh
# Usage: activate_standby.ksh <Oracle SID>
 
if [[ $# -ne 1 ]]
then
 print "Error: Usage: activate_standby.ksh <Oracle SID>"
 exit 1
else
 ORACLE_SID=$1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

this_host=$(hostname | awk -F. '{print $1}')
location=$(grep $ORACLE_SID /etc/hosts | awk '{print $NF}' | awk -F- '{print $NF}')
if [[ -n $location ]]
then
 if [ "$location" != "dr" ]
 then
  print "$ORACLE_SID Database on $this_host Server is not a DR database"
  exit 3
 fi
else
 print "$ORACLE_SID Database on $this_host Server is not a DR database"
 exit 4
fi

export ORACLE_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH 
. /usr/local/bin/oraenv > /dev/null

query=$($ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
select status from v\$instance; 
EOF)
status=$(print $query | awk '{print $3}')
if [ $status != MOUNTED ]
then
 print "Database must be in MOUNTED State to proceed with activating database."
 exit 5
fi

print "**************************************************"
print "* Activating this STANDBY database will start it *"
print "* up and invalidate it as a standby database.    *"
print "**************************************************\n"
read a?'OKAY to continue? Enter [Y] or [y] for yes to continue.  Any other response will abort here.'
 
if [[ ! $a = "Y" ]] && [[ ! $a = "y" ]]
then
 print "Exiting ..."
 exit 6
fi

print "Cancel Managed Recovery Mode of Standby Database"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE RECOVER MANAGED STANDBY DATABASE CANCEL;
EOF
if [ $? -ne 0 ]
then
 print "Error on canceling managed recovery mode"
 exit 7
fi

print "Activate Standby Database"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database activate standby database;
EOF
if [ $? -ne 0 ]
then
 print "Error on activating standby database"
 exit 8
fi

print "Shut down database"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
shutdown immediate
EOF

print "Start up database"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF>$script_dir/sqlplus_startup_error_${ORACLE_SID}.txt
WHENEVER SQLERROR EXIT FAILURE
startup
EOF
if [ $? -eq 0 ]
then
 grep 'ORA-32004' $script_dir/sqlplus_startup_error_${ORACLE_SID}.txt
 if [ $? -ne 0 ]
 then
  print "Unexpected error on starting up database"
 else
  print "There is an obsolete or deprecated parameter in database.  Please remove."
  print "See alert log to determine obsolete or deprecated parameters."
 fi
fi
cat $script_dir/sqlplus_startup_error_${ORACLE_SID}.txt
rm -f $script_dir/sqlplus_startup_error_${ORACLE_SID}.txt

print "Verify database is running in OPEN mode"
query=$($ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
select dummy from dual;
EOF)
dummy=$(print $query | awk '{print $3}')
if [ $dummy != X ]
then
 print "$ORACLE_SID Database is not running in OPEN Mode."
 exit 9
else
 print "$ORACLE_SID Database is running in OPEN Mode".
fi

if [ $ORACLE_SID = freprd1 ]
then
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
@$script_dir/tempfile_statements_freprd1.sql
EOF
fi

print "\n\n\t\tActivation of $ORACLE_SID Standby Database Successful."
exit 0
