#!/usr/bin/ksh
# Script name: check_arch_gap_standby.ksh
# Script to check standby archive log gap

function get_primary_max_seq
{
   SQLVAL=$(${EXEDIR}/sqlplus -s '/ as sysdba' <<EOF
       WHENEVER SQLERROR EXIT FAILURE ;
       set echo off veri off feed off pages 0 sqlp ""
       SELECT sequence# FROM(SELECT rownum rn, al.* FROM v\$archived_log al order by COMPLETION_TIME) where rn > (select count(*)-1 from v\$archived_log);
EOF)
   if [ $? -ne 0 ]
   then
      print "Error while getting max arch seq from primary database..."
      return 1
   else
      PRMY_MAX_ARCH=$(print $SQLVAL | sed s/[Cc]onnected\.//g)
   fi

   return 0
}

# Main script starts here...

if [ $# -eq 0 ]
then
   print "Invalid Arguments!"
   print "Usage : $0 <ORACLE_SID> [Logs Falling Behind By Threshold]"
   exit 1
fi

remote_server=ppld0008

export ORACLE_SID=$1
export ORAENV_ASK=NO
. /usr/local/bin/oraenv > /dev/null
EXEDIR=${ORACLE_HOME}/bin
ssh_dir=/usr/local/bin
mailrecipients=fdavis@eagleinvsys.com
script_dir=/orascripts/tools
log_dir=$script_dir/logs

ARCH_APPLY_THRESH=$2
if [[ -z $ARCH_APPLY_THRESH ]]
then
 ARCH_APPLY_THRESH=7
fi

typeset -i STBY_MAX_ARCH
STBY_MAX_ARCH=$($ssh_dir/ssh -o BatchMode=yes -nq $remote_server "$script_dir/get_standby_max_arch_seq.ksh ${ORACLE_SID}")
print ${STBY_MAX_ARCH} >> ${log_dir}/s.log
if [ $STBY_MAX_ARCH -eq 0 ]
then
 print 'failure' >> $log_dir/consecutive_failures_standby_${ORACLE_SID}.log
 failure_count=$(wc -l $log_dir/consecutive_failures_standby_${ORACLE_SID}.log | awk '{print $1}')
 if [ $failure_count -gt 1 ]
 then
  mailx -s "$(hostname): ${ORACLE_SID} standby database unreachable" $mailrecipients<<EOF
.
EOF
 fi
 exit 4
fi
rm -f $log_dir/consecutive_failures_standby_${ORACLE_SID}.log

typeset -i PRMY_MAX_ARCH
get_primary_max_seq
if [ $? -ne 0 ]
then
 mailx -s "$(hostname): $ORACLE_SID Primary database problem at $(date)" $mailrecipients<<EOF
 Cannot get the maximum log sequence number from primary database
 It is possible that the $ORACLE_SID primary database is shut down.
EOF
 exit 2
fi
print "Last archived log sequence on Primary is : ${PRMY_MAX_ARCH}"
print "Last archived log sequence on Standby is : ${STBY_MAX_ARCH}"

if [ $(( $PRMY_MAX_ARCH - $STBY_MAX_ARCH )) -gt $ARCH_APPLY_THRESH ]
then
   mailx -s "$(hostname): ${ORACLE_SID} standby database archive gap at $(date)" $mailrecipients<<EOF
   ERROR: Standby archive is falling too far behind the primary.
   Number of archive files falling behind : $(( $PRMY_MAX_ARCH - $STBY_MAX_ARCH ))
EOF
   exit 3
fi
exit 0
