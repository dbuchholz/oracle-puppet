#!/bin/ksh
# Script name: flashback_on.ksh
# Usage: flashback_on.ksh [ Oracle SID ]
####################### Run against Dataguard database only ######################################
##### Prerequisites
# database must be in archivelog mode
# log_archive_dest_1 must be used, not log_archive_dest
#
# The order to run the scripts is:
# flashback_on.ksh
# activate_standby.ksh
# flashback_database.ksh
# flashback_off.ksh

function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

function funct_check_db_code
{
 pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
 pcode=$(print $pcode|tr -d " ")

 tnsname_prod=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='N' and status='OPEN';
EOF`
 tnsname_prod=$(print $tnsname_prod|tr -d " ")
}

# MAIN ***************************************************************************************

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

this_host=$(hostname | awk -F. '{print $1}')
location=$(grep $ORACLE_SID /etc/hosts | awk '{print $NF}' | awk -F- '{print $NF}')
if [[ -n $location ]]
then
 if [ "$location" != "dr" ]
 then
  print "$ORACLE_SID Database on $this_host Server is not a DR database"
  exit 3
 fi
else
 print "$ORACLE_SID Database on $this_host Server is not a DR database"
 exit 4
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

query=$($ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
select status from v\$instance; 
EOF)
status=$(print $query | awk '{print $3}')
if [ $status != MOUNTED ]
then
 print "Database must be in MOUNTED State to proceed with turning Flashback off."
 exit 5
fi

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
puser=SYS
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 exit 6
fi

funct_check_db_code

print "Connect to primary database and switch archive log file."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter system archive log current;
EOF
if [ $? -ne 0 ]
then
 print "Could not switch Production archive log file.  Go to Production and do this manually."
 exit 7
fi
sleep 5

print "Connect to primary database and determine maximum archive log sequence number."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF>$script_dir/${ORACLE_SID}_prod_max_log_seq_no.txt
WHENEVER SQLERROR EXIT FAILURE
set echo off pages 0 trimspool on feedback off
SELECT sequence# FROM(SELECT rownum rn, al.* FROM v\$archived_log al order by COMPLETION_TIME) where rn > (select count(*)-1 from v\$archived_log);
EOF
if [ $? -ne 0 ]
then
 print "Could not determine maximum log sequence number in Production.  Go to Production and do this manually."
 print "Run this script again after verifying that you can determine maximum log sequence number in Production."
 exit 8
fi
prod_max_seq_no=$(cat $script_dir/${ORACLE_SID}_prod_max_log_seq_no.txt)
rm -f $script_dir/${ORACLE_SID}_prod_max_log_seq_no.txt
print "Maximum Log Sequence Number in Production is: $prod_max_seq_no"

print "Waiting 10 seconds to start checking for log being applied to DR."
sleep 10

print "Poll DR until Production archive log applied to DR."
while true
do
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$script_dir/${ORACLE_SID}_dr_max_log_seq_no.txt
set echo off pages 0 trimspool on feedback off
WHENEVER SQLERROR EXIT FAILURE
SELECT sequence# FROM(SELECT rownum rn, al.* FROM v\$archived_log al order by COMPLETION_TIME) where rn > (select count(*)-1 from v\$archived_log);
EOF
if [ $? -ne 0 ]
then
 print "Could not get maximum log sequence number applied to DR.  Aborting at this point."
 exit 9
fi
dr_max_seq_no=$(cat $script_dir/${ORACLE_SID}_dr_max_log_seq_no.txt)
rm -f $script_dir/${ORACLE_SID}_dr_max_log_seq_no.txt
if [ $dr_max_seq_no -eq $prod_max_seq_no ]
then
 print "Maximum Log Sequence Number in DR is: ${dr_max_seq_no}.  Waiting another 5 seconds to check again."
 break
fi
print "Maximum Log Sequence Number in DR is: ${dr_max_seq_no}.  Waiting another 5 seconds to check again."
sleep 5
done
print "\n\tMaximum Log Sequence Number in Prod is: ${prod_max_seq_no}"
print "\tMaximum Log Sequence Number in DR   is: ${dr_max_seq_no}"

print "Production archive log has been applied to DR."

print "Connect to primary database and defer log_archive_dest_2."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter system set log_archive_dest_state_2='defer';
EOF
if [ $? -ne 0 ]
then
 print "Could not defer log_archive_dest_state_2.  Go to Production and do this manually."
fi

print "Determine the value of log_archive_dest_1 and creating the flashback area"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$script_dir/${ORACLE_SID}_find_log_dest.log
set echo off pages 0 trimspool on feedback off
WHENEVER SQLERROR EXIT FAILURE
select value from v\$parameter where name='log_archive_dest_1';
EOF
if [ $? -ne 0 ]
then
 print "Could not get the value of log_archive_dest_1 for $ORACLE_SID"
 print "This value is needed to locate/create flash recovery area."
 print "Aborting here."
 exit 10
fi
awk -F= '{print $2}' $script_dir/${ORACLE_SID}_find_log_dest.log > $script_dir/${ORACLE_SID}_find_log_dest_2.log
flashback_area="$(dirname $(cat $script_dir/${ORACLE_SID}_find_log_dest_2.log))/flash_recovery"
mkdir -p $flashback_area
rm -f $script_dir/${ORACLE_SID}_find_log_dest.log $script_dir/${ORACLE_SID}_find_log_dest_2.log

print "Shutting down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
shutdown immediate
EOF

print "Starting database in mount state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 11
fi

print "Set DB_RECOVERY_FILE_DEST_SIZE,db_flashback_retention_target and db_recovery_file_dest"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
alter system set DB_RECOVERY_FILE_DEST_SIZE=100G;
/* target value */
alter system set db_flashback_retention_target=2880;
/* two days (value is in minutes) */
alter system set db_recovery_file_dest='$flashback_area';
EOF
if [ $? -ne 0 ]
then
 print "Error on setting flashback parameters."
 print "Aborting here."
 exit 12
fi

print "Turn flashback on"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE FLASHBACK ON;
EOF
if [ $? -ne 0 ]
then
 print "Error on turning flashback on."
 print "Aborting here."
 exit 13
fi

print "Select flashback information in database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
set lines 200
WHENEVER SQLERROR EXIT FAILURE
SELECT FLASHBACK_ON,CURRENT_SCN FROM V\$DATABASE;
EOF
if [ $? -ne 0 ]
then
 print "Error on selecting flashback information from database."
 print "Aborting here."
 exit 14
fi

print "Create restore point"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
set lines 200
WHENEVER SQLERROR EXIT FAILURE
create restore point in_dataguard guarantee flashback database;
EOF
if [ $? -ne 0 ]
then
 print "Error creating restore point."
 print "Aborting here."
 exit 15
fi

print "Query restore point"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
set lines 200
WHENEVER SQLERROR EXIT FAILURE
select * from v\$restore_point;
EOF

print "Put DR back in managed recovery mode."
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE RECOVER MANAGED STANDBY DATABASE DISCONNECT FROM SESSION;
EOF
if [ $? -ne 0 ]
then
 print "Error on putting database in managed recovery mode."
 print "Aborting here."
 exit 16
fi

print "\n\nFlashback is enabled and active and restore point created."
print "Run flashback_database.ksh to return database to restore point."
exit 0
