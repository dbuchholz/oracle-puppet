#!/bin/ksh
# Script name: flashback_off.ksh
# Usage: flashback_off.ksh [ Oracle SID ]

function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

function funct_check_db_code
{
 pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
 pcode=$(print $pcode|tr -d " ")

 tnsname_prod=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='N' and status='OPEN';
EOF`
 tnsname_prod=$(print $tnsname_prod|tr -d " ")
}

# MAIN ***************************************************************************************

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

this_host=$(hostname | awk -F. '{print $1}')
location=$(grep $ORACLE_SID /etc/hosts | awk '{print $NF}' | awk -F- '{print $NF}')
if [[ -n $location ]]
then
 if [ "$location" != "dr" ]
 then
  print "$ORACLE_SID Database on $this_host Server is not a DR database"
  exit 3
 fi
else
 print "$ORACLE_SID Database on $this_host Server is not a DR database"
 exit 4
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

print "Shut down the database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
shutdown immediate
EOF

print "Start the database in mount state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 5
fi

query=$($ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
select status from v\$instance; 
EOF)
status=$(print $query | awk '{print $3}')
if [ $status != MOUNTED ]
then
 print "Database must be in MOUNTED State to proceed with turning Flashback off."
 exit 6
fi

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
puser=SYS
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 exit 7
fi

funct_check_db_code

print "Determine the location of the flashback area"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$script_dir/${ORACLE_SID}_find_log_dest.log
set echo off pages 0 trimspool on feedback off
WHENEVER SQLERROR EXIT FAILURE
select value from v\$parameter where name='log_archive_dest_1';
EOF
if [ $? -ne 0 ]
then
 print "Could not get the value of log_archive_dest_1 for $ORACLE_SID"
 print "This value is needed to locate/create flash recovery area."
 print "Aborting here."
 exit 8
fi
awk -F= '{print $2}' $script_dir/${ORACLE_SID}_find_log_dest.log > $script_dir/${ORACLE_SID}_find_log_dest_2.log
flashback_area="$(dirname $(cat $script_dir/${ORACLE_SID}_find_log_dest_2.log))/flash_recovery"
rm -f $script_dir/${ORACLE_SID}_find_log_dest.log $script_dir/${ORACLE_SID}_find_log_dest_2.log

print "Shut down the database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
shutdown immediate
EOF

print "Start the database in mount state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 9
fi

print "Turn flashback off"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE FLASHBACK OFF;
EOF
if [ $? -ne 0 ]
then
 print "Error on turning flashback database off."
 print "Aborting here."
 exit 10
fi

print "Drop restore point, unset DB_RECOVERY_FILE_DEST_SIZE, db_flashback_retention_target and db_recovery_file_dest"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
drop restore point in_dataguard;
alter system reset DB_RECOVERY_FILE_DEST_SIZE scope=spfile sid='*';
alter system reset db_flashback_retention_target scope=spfile sid='*';
alter system reset db_recovery_file_dest scope=spfile sid='*';
EOF

print "Shut down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
shutdown immediate 
EOF

print "Start database in mount state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 11
fi

print "Query flashback state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
SELECT FLASHBACK_ON FROM V\$DATABASE;
EOF
if [ $? -ne 0 ]
then
 print "Error on selecting flashback status."
fi

print "Put database in managed standby mode"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
alter database recover managed standby database disconnect from session;
EOF
if [ $? -ne 0 ]
then
 print "Error on placing standby database in managed recovery mode."
 print "Aborting here."
 exit 12
fi

print "Remove the flashback area"
rm -r $flashback_area

print "Connect to primary database and enable log_archive_dest_2."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter system set log_archive_dest_state_2='enable';
EOF
if [ $? -ne 0 ]
then
 print "Could not enable log_archive_dest_state_2.  Go to Production and do this manually."
 exit 13
fi

exit 0
