#!/bin/ksh
# Script name: flashback_on_non_DR_no_guarantee.ksh
# Usage: flashback_on_non_DR_no_guarantee.ksh [ Oracle SID ]
##### Prerequisites
# database must be in archivelog mode
# log_archive_dest_1 must be used, not log_archive_dest
#
if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

print "Determine the value of log_archive_dest_1 and creating the flashback area"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$script_dir/${ORACLE_SID}_find_log_dest.log
set echo off pages 0 trimspool on feedback off
WHENEVER SQLERROR EXIT FAILURE
select value from v\$parameter where name='log_archive_dest_1';
EOF
if [ $? -ne 0 ]
then
 print "Could not get the value of log_archive_dest_1 for $ORACLE_SID"
 print "This value is needed to locate/create flash recovery area."
 print "Aborting here."
 exit 3
fi
awk -F= '{print $2}' $script_dir/${ORACLE_SID}_find_log_dest.log > $script_dir/${ORACLE_SID}_find_log_dest_2.log
flashback_area="$(dirname $(cat $script_dir/${ORACLE_SID}_find_log_dest_2.log))/flash_recovery"
mkdir -p $flashback_area
rm -f $script_dir/${ORACLE_SID}_find_log_dest.log $script_dir/${ORACLE_SID}_find_log_dest_2.log

print "Shutting down database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
shutdown immediate
EOF
if [ $? -ne 0 ]
then
 print "Error on shut down of database."
 print "Aborting here."
 exit 4
fi

print "Starting database in mount state"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
startup mount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup mount of database."
 print "Aborting here."
 exit 5
fi

print "Set DB_RECOVERY_FILE_DEST_SIZE,db_flashback_retention_target and db_recovery_file_dest"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
alter system set DB_RECOVERY_FILE_DEST_SIZE=100G;
/* target value */
alter system set db_flashback_retention_target=2880;
/* two days (value is in minutes) */
alter system set db_recovery_file_dest='$flashback_area';
EOF
if [ $? -ne 0 ]
then
 print "Error on setting flashback parameters."
 print "Aborting here."
 exit 6
fi

print "Turn flashback on"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE FLASHBACK ON;
EOF
if [ $? -ne 0 ]
then
 print "Error on turning flashback on."
 print "Aborting here."
 exit 7
fi

print "Select flashback information in database"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
set lines 200
WHENEVER SQLERROR EXIT FAILURE
SELECT FLASHBACK_ON,CURRENT_SCN FROM V\$DATABASE;
EOF
if [ $? -ne 0 ]
then
 print "Error on selecting flashback information from database."
 print "Aborting here."
 exit 8
fi

print "Opening database."
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo on
WHENEVER SQLERROR EXIT FAILURE
ALTER DATABASE OPEN;
EOF
if [ $? -ne 0 ]
then
 print "Error opening database."
 print "Aborting here."
 exit 10
fi

print "\n\nFlashback is enabled and active."
exit 0
