#!/bin/ksh
#
# Script name: refresh_standby.ksh
# Usage: refresh_standby.ksh [ Oracle SID ]
# Author: Frank Davis 3/10/2011
#
# Run this script on the server where the Dataguard Standby Database resides.
#
#
# Prerequisites to run this script:
#
# The following init.ora parameters need to be defined on the Primary database.  Substitute your Oracle SID for easprd1 below.
# If any changes to the Production init.ora parameters are required, the changes must be in the backup area for the database.
# This script will use the init.ora from the backup area.  So after you make changes, create a pfile from spfile and overlay the newest init.ora in the backup area before running this script.
# Keep the file name the same as it was as this script looks for a particular file pattern.
# If a backup occurred since the changes, you already have that information in the backup area in the init.ora.
# fal_client='easprd1'
# fal_server='easprd1_dr'
# standby_file_management=auto
# log_archive_dest_2='service=easprd1_dr optional reopen=15'
#
# log_archive_dest_state_2='enable'   # See below for explanation
# If existing standby database is running in some other location, leave log_archive_dest_state_2='enable' until time of refresh.
# If no existing standby database, which includes Open DR situation, set log_archive_dest_state_2='defer'

# The above parameters need to be set before the backup to be used for creation of Standby Database.
#
# 1.  The mount points must match in name and size from Production to Standby.  No db_name_file_convert and log_file_name_convert allowed.
# 2.  In Production, there must be an entry in this ORACLE_HOME tnsnames.ora file for the Production and Standby database.
# 3.  In DR, there must be an entry in this ORACLE_HOME tnsnames.ora file for the Standby and Production database.
# 4.  There must be an entry in this ORACLE_HOME tnsnames.ora for the Big Brother infprd1 database
# 5.  The Production and Standby databases must have listener entries for their respective databases and running in listener.
# 6.  The DATABASES, MACHINES and USER_CODES Tables in infprd1 must be configured completely and correctly for the Production and Standby databases
#     This means there must be a password in USER_CODES Table CODE Field for SYS User in Production database, Production server must be in MACHINES Table.
#
# 7.  The DATABASES Table for Production(Primary) must have STATUS='OPEN'    and DATAGUARD='N'.  No other database with this Oracle SID can have that combination of values.
#     The DATABASES Table for DR(Standby) must have                              DATAGUARD='Y'.  No other database with this Oracle SID can have this value.
#
#     The DATABASES Table TNSNAME Field must have entries that match the values in 2. and 3. above for both the Production and Standby databases.
# 8.  The file /u01/app/oracle/local/dba/GetDec.cmd must exist on this host.
# 9.  The file /u01/app/oracle/local/dba/BigBrother.ini must exist on this host and have valid infprd1 inf_monitor connect data.
#10.  The RMAN Production backup used to create the Standby database must reside in: /datadomain/*/prod?/{Production ORACLE_SID} on the standby host.
#     You can create a symbolic link in this location that points to somewhere else where files are actually located.  As long as the link location is the same
#     as the original location where the Production database was backed up.
#11.  There must be an entry in the oratab file for the standby database.
#
#  For example:  Creating a Pittsburgh standby in place of an existing Newton standby:
#  Set DATAGUARD='N' for existing standby.  Also, on host where existing standby resides add to BigBrother.ini to not run anything for existing standby.
#
# This script will check to see if the mount point names match from Production to Standby if the standby database is in at least mount phase.
# If the mount point names do not match, the script will abort as it will require manual action to define db_name_file_convert in init.ora
# Therefore, do not shut down the standby database before running this script.  Leave the Standby in at least mount state --- managed recovery or not.
# If the standby database is shut down or does not exist at the time this script is run, the script cannot determine if mount point names
# match between Production and Standby, as the Standby files cannot be determined.  The script will continue and will fail if the mount point
# names do not match.  The RMAN recovery for this standby creation is NOFILENAMECHECK.
#
# Here is an example of the infprd1 Big Brother insert statement for the Standby Database:
#
# insert into databases (instance,sid,client,dataguard,status,tnsname,environment,vip,mac_instance)
# values (databases_seq.nextval,'freprd1',201,'Y','MOUNTED','sb_freprd1','Freddie Mac DR','10.60.52.111',26);
#
# Note that the infprd1 DATABASES Table DATAGUARD and TNSNAME field values are read by this script.  The DATAGUARD value must equal Y for the standby database.
#
#
# In the RMAN recovery section, you will see errors of the following types:
#
#starting media recovery
#
#Oracle Error: 
#ORA-01547: warning: RECOVER succeeded but OPEN RESETLOGS would get error below
#ORA-01152: file 1 was not restored from a sufficiently old backup 
#ORA-01110: data file 1: '/nliprd1-01/oradata/system01.dbf'
#
#released channel: RCHL1
#released channel: RCHL2
#released channel: RCHL3
#released channel: RCHL4
#RMAN-00571: ===========================================================
#RMAN-00569: =============== ERROR MESSAGE STACK FOLLOWS ===============
#RMAN-00571: ===========================================================
#RMAN-03002: failure of Duplicate Db command at 06/20/2011 14:53:26
#RMAN-03015: error occurred in stored script Memory Script
#RMAN-06053: unable to perform media recovery because of missing log
#RMAN-06025: no backup of log thread 1 seq 28965 lowscn 432945879 found to restore
#RMAN-06025: no backup of log thread 1 seq 28964 lowscn 432934297 found to restore
#RMAN-06025: no backup of log thread 1 seq 28963 lowscn 432932384 found to restore
#
# These errors are normal and expected.
#
# What this is saying is the archive logs needed for recovery to move the database to the present time are not in the backup area.
# The expected archive logs are on disk.  For the example above, seq 28963 through 28965 are on disk and have not been backed up yet.
# The script will put the database in managed recovery mode and transfer and apply those archive logs from the Primary side that are on disk.
# After those archive logs are applied the database will be current.
#
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

function funct_prod_archivelog_mode
{
 arch_mode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select log_mode from v\\$database;
EOF`
 arch_mode=$(print $arch_mode|tr -d " ")
 if [ $arch_mode != ARCHIVELOG ]
 then
  return 1
 fi
 return 0
}

function funct_check_db_code
{
 pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
 pcode=$(print $pcode|tr -d " ")

 tnsname_prod=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='N' and status='OPEN';
EOF`
 tnsname_prod=$(print $tnsname_prod|tr -d " ")

 tnsname_dr=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='Y';
EOF`
 tnsname_dr=$(print $tnsname_dr|tr -d " ")
}

function funct_check_not_on_primary
{
 number_primary=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
select count(*) from databases where dataguard='N' and status='OPEN' and sid='$ORACLE_SID';
EOF`
 number_primary=$(print $number_primary|tr -d " ")
 if [ $number_primary -gt 1 ]
 then
  print "There is more than one candidate for primary database for $ORACLE_SID in Big Brother Database."
  print "It cannot be determined which one is the actual primary and which one is the standby."
  print "Aborting here."
  exit 8
 fi
 primary_host=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
select m.name from machines m,databases d where d.mac_instance=m.instance and d.dataguard='N' and d.status='OPEN' and d.sid='$ORACLE_SID';
EOF`
 primary_host=$(print $primary_host|tr -d " ")
}

function funct_only_one_standby
{
 number_standbys=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
select count(*) from databases where dataguard='Y' and sid='$ORACLE_SID';
EOF`
 number_standbys=$(print $number_standbys|tr -d " ")
 if [ $number_standbys -gt 1 ]
 then
  print "There is more than one candidate for standby database for $ORACLE_SID in Big Brother Database."
  print "It cannot be determined which one is the actual standby."
  print "Aborting here."
  exit 9
 fi
}

function funct_prod_archivelog_mode
{
 arch_mode=`${ORACLE_HOME}/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba" <<EOF
 set heading off
 set feedback off
 select log_mode from v\\$database;
EOF`
 arch_mode=$(print $arch_mode|tr -d " ")
 if [ $arch_mode != ARCHIVELOG ]
 then
  return 1
 fi
 return 0
}

function funct_list_primary_mount_points
{
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF>$log_dir/only_mount_points_prod_${ORACLE_SID}.txt
WHENEVER SQLERROR EXIT FAILURE
set pages 0 linesize 200 trimspool on feedback off
select name from v\$datafile UNION select name from v\$controlfile UNION
select name from v\$tempfile UNION select member name from v\$logfile;
EOF
if [ $? -ne 0 ]
then
 print "Cannot get the list of primary database mount points."
 print "Aborting here."
 exit 6
fi
awk -F/ '{print "/"$2}' $log_dir/only_mount_points_prod_${ORACLE_SID}.txt | sort -u > $log_dir/just_mount_points_prod_${ORACLE_SID}.txt
rm -f $log_dir/only_mount_points_prod_${ORACLE_SID}.txt
}

function funct_force_logging
{
 forced_logging=`${ORACLE_HOME}/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba" <<EOF
 set heading off
 set feedback off
 select force_logging from v\\$database;
EOF`
 forced_logging=$(print $forced_logging|tr -d " ")
 if [ $forced_logging = NO ]
 then
  return 1
 fi
 return 0
}

#################### MAIN ##########################
print  "Start standby $ORACLE_SID creation at $(date)"
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of the standby database you want to refresh or create."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi

export ORACLE_SID=$1

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 print "If you want to refresh or create Standby Database ${ORACLE_SID}, make an entry in $ORATAB for it."
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
catl=rman
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
puser=SYS
tnsname_catalog=eabprd1
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 exit 3
fi

datadomain_root=/datadomain/*/prod?
find $datadomain_root -name "$ORACLE_SID" > /dev/null 2>&1
if [ $? -eq 0 ]
then
 backup_dir=$(find $datadomain_root -name "$ORACLE_SID")
else
 print "There is no backup directory for this database in $datadomain_root for $ORACLE_SID"
 print "Aborting here."
 exit 4
fi

now=$(date +'%Y%m%d_%H%M%S')

print "Create spfile from pfile"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
create spfile from pfile;
EOF
if [ $? -ne 0 ]
then
 print "Error on creating spfile for standby database."
 print "Aborting here."
 exit 13
fi

funct_check_db_code

print "Starting database nomount"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
startup nomount
EOF
if [ $? -ne 0 ]
then
 print "Error on startup nomount of standby database in preparation for refresh."
 print "Aborting here."
 exit 14
fi

print "Start RMAN standby refresh"
$ORACLE_HOME/bin/rman<<EOF
connect catalog $catl/${catl}@$tnsname_catalog
connect target $puser/${pcode}@$tnsname_prod
connect auxiliary /
run
{
 allocate AUXILIARY channel 'RCHL1' device type disk;
 allocate AUXILIARY channel 'RCHL2' device type disk;
 allocate AUXILIARY channel 'RCHL3' device type disk;
 allocate AUXILIARY channel 'RCHL4' device type disk;
 duplicate target database for standby NOFILENAMECHECK dorecover;
}
EOF

print "Connect to primary database and enable log_archive_dest_2.  Switch a logfile."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter system set log_archive_dest_state_2='enable';
alter system archive log current;
EOF

funct_force_logging
if [ $? -ne 0 ]
then
print "Force Logging was not turned on in Production.  Will do it now."
$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
alter database FORCE LOGGING;
EOF
if [ $? -ne 0 ]
then
 print "Could not turn on forced logging in Primary database."
 print "Resolve issue after Standby database is created."
fi
fi

print "Put standby in managed recovery mode."
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
alter database recover managed standby database disconnect from session;
EOF
if [ $? -ne 0 ]
then
 print "Error on putting standby database in managed recovery mode."
 print "Aborting here."
 exit 15
fi

print  "End standby $ORACLE_SID creation at $(date)"

exit 0
