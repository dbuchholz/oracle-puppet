#!/usr/bin/ksh
# ==================================================================================================
# NAME:		PurgeReports.sh                                 
# 
# AUTHOR:	Maureen Buotte                                    
#
# PURPOSE:	This utility will purge reports from the database and create a file which will
#		be used to delete the actual reports from the report store.  The file will be 
#		FTP'd to the clients FTP site and used for the delete
#
#			pace_masterdbo.xml_package_log
#			pace_masterdbo.xml_reports_log_detail
#			pace_masterdbo.xml_report_info
#			pace_masterdbo.xml_package_log
#
# USAGE:	PurgeFeedResults.sh [SID]
#
# Frank Davis  09/24/2013  Removed tech_support@eagleaccess.com from email
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis      24-Jun-2011  Modified IPCheck Variable to use updated list of VLANs 
# Nishigandha Sathe 2-Mar-2010  Modified to use new primary key machines(instance)
# Maureen Buotte    5-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    1-Jun-2008  Added reporting to DBA database
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

	# Uncomment for debug
	 set -x

	echo -e "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat
	if [[ x$1 != 'x' ]]; then
		echo -e "\n$1\n" >> mail.dat
	fi

	if [[ -a ${ERROR_FILE} ]]; then
		cat $ERROR_FILE >> mail.dat
		rm ${ERROR_FILE}
	fi

	if [[ $2 = 'FATAL' ]]; then
		echo "*** This is a FATAL ERROR - ${PROGRAM_NAME} aborted at this point *** " >> mail.dat
	fi

	cat mail.dat | /bin/mail -s "PROBLEM WITH ${TYPE} Environment -- ${PROGRAM_NAME} on ${BOX} for ${ORACLE_SID}" ${MAILTO}
	rm mail.dat

	# Update the mail counter
	MAIL_COUNT=${MAIL_COUNT}+1

	return 0
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging
     set -x

	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=`echo -e $PERFORM_CRON_STATUS|tr -d " "`
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
		PERFORM_CRON_STATUS=0
	fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {

	# Uncomment for debug
	 set -x

	DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
		exit
EOF`
	DB_INSTANCE=`echo -e $DB_INSTANCE |tr -d " "`
	export DB_INSTANCE
	if [ "x$DB_INSTANCE" == "x" ]; then
		SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
        PERFORM_CRON_STATUS=0
		return 1	
	fi

	Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
		start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
		exit
EOF`
	Sequence_Number=`echo $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set serveroutput on size 1000000
		set heading off
		set feedback off
		declare i number;
		begin
		insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
		commit;
		dbms_output.put_line (i);
		end;
/
		exit
EOF`
	else
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
		commit;
EOF`

	fi
	export Sequence_Number
	return 0

}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

	# Uncomment for debug
	 set -x

    # Update DBA Auditing Database with end time and backup size
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off 
		set feedback off 
		update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
		commit;

		update INSTANCE_CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
		from instance_cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
/
		exit
EOF`

    echo  "$PROGRAM_NAME Completed `date +\"%c\"`with a status of ${PROCESS_STATUS} "
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open {

	# Uncomment next line for debugging
	 set -x

    typeset -i OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
		exit
EOF`

    OPEN=`echo -e $OPEN|tr -d " "`

	if [[ ${OPEN} == 1 ]]; then
		return 1
	else
		return 0
	fi
}


# --------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------
funct_chk_parm() {
	# Uncomment next line for debugging
	# set -x
	if [ ${NARG} -ne 2 ]; then
		echo "     Incorrect parameters - Must enter SID and Number of days worth of reports to keep"  >> $LOGFILE
		echo "     ${PROGRAM_NAME} FAILED:  Incorrect parameters - Must enter SID and Number of days worth of reports to keep"
		SendNotification "Incorrect parameters - Must enter SID and Number of days worth of reports to keep "
		exit 1
	fi
}

# ------------------------------------------------------------
# funct_purge_reports(): Purge reports tables
# ------------------------------------------------------------
funct_purge_reports(){

	# Uncomment next line for debugging
	 set -x

	status=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set pagesize 30000
		set linesize 132
		set heading off
		set feedback off
		spool $PURGEFILE
		select report_location from pace_masterdbo.xml_reports_log
		where upd_datetime <= (sysdate - $DAYS_TO_KEEP);
		spool off

		delete from pace_masterdbo.xml_package_log where report_instance in
		(select instance from pace_masterdbo.xml_reports_log where upd_datetime <= (sysdate-$DAYS_TO_KEEP));
		commit;

		delete from pace_masterdbo.xml_reports_log_detail where instance in
		(select instance from pace_masterdbo.xml_reports_log where upd_datetime <= (sysdate-$DAYS_TO_KEEP));
		commit;

		delete from pace_masterdbo.xml_report_info where pinstance in
		(select instance from pace_masterdbo.xml_reports_log where upd_datetime <= (sysdate-$DAYS_TO_KEEP));
		commit;

		delete from pace_masterdbo.xml_reports_log where upd_datetime <= (sysdate-$DAYS_TO_KEEP);
		commit;

		exit
EOF`
        return 0
}

# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
 set -x

NARG=$#
INPUT_SID=$1

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export PROGRAM_NAME_FIRST=`echo ${PROGRAM_NAME} | awk -F "." '{print $1}'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export DB_INSTANCE

export ORACLE_BASE=$HOME
export ORATAB_LOC="/etc"
export WORKING_DIR="${ORACLE_BASE}/local/dba/purge"
export ERROR_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_error.txt
export LOGDIR=$WORKING_DIR/logs

HOSTNAME=`hostname`
IPCheck=`cat /etc/hosts | grep ${HOSTNAME} | awk '{print $1}' | egrep '10\.60\.5\.|10\.60\.3\.|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
    export TYPE=PROD
else
    export TYPE=TEST
fi
export MAILTO=team_dba@eagleaccess.com
funct_chk_parm
INPUT_SID=$1
typeset -i DAYS_TO_KEEP=$2

# Check if CRONLOG directory exists
if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
    mkdir -p ${ORACLE_BASE}/CRONLOG
fi

NumericDate=`date +%Y%m%d`
NumericTime=`date +%H%M%S`
export PARAMFILE=$WORKING_DIR/Param.dat
export LOGFILE=$LOGDIR/PurgeReports_${NumericDate}_${NumericTime}.log

RunDate=`date +%m/%d/%y`
RunTime=`date +%H:%M:%S`
echo "PurgeReports.sh started `date +\"%c\"`" > $LOGFILE

# Get environment variables set up - need to do this differently for RHEL 5
dummy_sid=`cat ${ORATAB_LOC}/oratab | grep ${ORACLE_BASE} | grep -v "^#" | grep -v "^*" | awk -F: '{print $1}' | tail -1`
. /usr/bin/setsid.sh ${dummy_sid}

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a ${PARFILE} ]]; then
    STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
    if [[ -n $LINE ]]; then
        INFO=`echo $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g' `
        export PERFORM_CRON_STATUS=1
        export CRON_SID=`echo -e $INFO | awk '{print $1}' `
        export CRON_USER=`echo -e $INFO | awk '{print $2}' `
        CRON_CONNECT=`echo -e $INFO | awk '{print $3}' `
        export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
        # Make sure you can get to the infrastructure database
        funct_check_inf_database
        echo $PERFORM_CRON_STATUS
    fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_ANYTHING=`echo $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
    fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "${PROGRAM_NAME}:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_THIS_JOB=`echo $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
    fi
fi
rm -f ${NO_COMMENT_PARFILE}

export NumericDate=`date +%Y%m%d`
export MAIL_COUNT=0

. /usr/bin/setsid.sh $INPUT_SID
Status=$?
if [ $Status != 0 ]; then
    echo -e "Invalid SID -> ${INPUT_SID}"
    SendNotification "Invalid SID -> ${INPUT_SID_SID}"
    exit
fi
export ORACLE_SID
export ORACLE_HOME
export PATH
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

# Make sure this database is not in list of SIDs that nothing should be run against
EXCEPTION=`awk -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
    SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} no jobs should run for this SID "
    continue
fi

# Make sure this database is not in list of SIDs that this job should not be run for
EXCEPTION=`awk -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
    SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} because it is explicitly excluded in CronStatus.ini file"
    continue
fi

# Make sure database is OPEN if not, skip to the next database
funct_verify_open
Status=$?
if [[ $Status == 0 ]]; then
    echo -e "${INPUT_SID} is not OPEN -> ${PROGRAM_NAME} will not run"
    SendNotification "${INPUT_SID} is not OPEN -> ${PROGRAM_NAME} will not run"
    exit 1
fi

# Add entry into DBA Admin database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
    funct_initial_audit_update
    Status=$?
    if [[ $Status != 0 ]]; then
        PERFORM_CRON_STATUS=0
    fi
    echo ${DB_INSTANCE}
    export DB_INSTANCE
fi

echo "	Starting Purge of Reports in $ORACLE_SID `date +\"%c\"`" >> $LOGFILE
PARLINE=`cat ${PARAMFILE} |grep ${ORACLE_SID}`
if [ "$PARLINE" == "" ]; then
	echo "${PROGRAM_NAME} Failed: Parameters not found for ${ORACLE_SID}"
	SendNotification "Parameters not found for ${ORACLE_SID}"
else
	IP=`echo $PARLINE | awk '{print $2}'`
	if [ "$IP" == "" ]; then
		echo "${PROGRAM_NAME} Failed: IP Parameter for FTP Site not found"
		SendNotification "IP Parameter for FTP Site not found"
	else
		FTPUSER=`echo $PARLINE | awk '{print $3}'`
		if [ "$FTPUSER" == "" ]; then
			 echo "${PROGRAM_NAME} Failed: FTP User Parameter not found"
			 SendNotification "FTP User Parameter not found"
		else
			FTPCODE=`echo $PARLINE | awk '{print $4}'`
			if [ "$FTPCODE" == "" ]; then
				echo "${PROGRAM_NAME} Failed: FTP Password Parameter not found"
				SendNotification "FTP Password Parameter not found"
			else
				export CONNECT=`${PAR_HOME}/GetDec.cmd ${FTPCODE}`
				export PurgeReports=${LOGDIR}/purge_${INPUT_SID}_${NumericDate}_${NumericTime}.log
				funct_purge_reports
echo $IP $FTPUSER $CONNECT $PURGEFILE
ftp -n $IP << ENDFTP
quote USER $FTPUSER
quote PASS $CONNECT
ascii
cd feeds_to_windows
put $PURGEFILE $PURGEFILENAME
quit
ENDFTP
			fi
		fi
	fi
fi

# Update Infrastructure database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
    if [[ ${MAIL_COUNT} > 0 ]]; then
        PROCESS_STATUS='WARNING'
    else
        PROCESS_STATUS='SUCCESS'
    fi
    funct_final_audit_update
fi

# Purge Old Log Files
KEEPDEPTH=30
OLD_LOGS=$LOGDIR/PurgeReports_????????_????????.log
OLDLOGS=`find ${OLD_LOGS} -mtime +1`
if [ "$OLDLOGS" != "" ] ; then
    rm ${OLDLOGS}
    echo "Removing old log file(s): \n$OLDLOGS";
fi

# Purge Old Report Files
KEEPDEPTH=30
OLD_REPORTS=$LOGDIR/ReportPurge_${INPUT_SID}_????????_????????.log
OLDREPORTS=`find ${OLD_REPORTS} -mtime +1`
if [ "$OLDREPORTS" != "" ] ; then
    rm ${OLDREPORTS}
    echo "Removing old log file(s): \n$OLDREPORTS";
fi

echo "PurgeReports.sh ended `date +\"%c\"`" >> $LOGFILE

######## END MAIN  #########################
