#!/bin/ksh
################################################################################################## 
# NAME:		PurgeDumpDirs.sh                                                                     #
# 																								 #
# AUTHOR:	Nirmal S Arri																		 #
#																								 #
# PURPOSE:	This utility will Purge the all files under $HOME/admin/$ORACLE_SID/cdump,adump,udump#
#           except $HOME/admin/$ORACLE_SID/bdump												 #
#																								 #
# USAGE:	PurgeDumpDirs.sh																	 #
#																								 #
# Nirmal S Arri		11/29/2010																	 #
#																								 #
################################################################################################## 
# set -x


# Check if CRONLOG directory exists
if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
	mkdir -p ${ORACLE_BASE}/CRONLOG
fi

/usr/bin/find ${ORACLE_HOME}/network/log/ -type f -name '*.trc' -mtime +30 -print -exec rm {} \;

# Loop through the list of Running Oracle instances to run against

ps -ef | grep ora_pmon_ | grep -v grep | awk -F_ '{print $NF}' | while read LINE
do

ORACLE_SID=${LINE}

#/usr/bin/find ${HOME}/admin/${ORACLE_SID}/bdump/ -type f -name '*.trc' -mtime +30 -print -exec rm {} \;
/usr/bin/find ${HOME}/admin/${ORACLE_SID}/cdump/ -type f -name '*.trc' -mtime +30 -print -exec rm {} \;
/usr/bin/find ${HOME}/admin/${ORACLE_SID}/udump/ -type f -name '*.trc' -mtime +30 -print -exec rm {} \;
/usr/bin/find ${HOME}/admin/${ORACLE_SID}/adump/ -type f -name '*.aud' -mtime +30 -print -exec rm {} \;

done 

exit 0
