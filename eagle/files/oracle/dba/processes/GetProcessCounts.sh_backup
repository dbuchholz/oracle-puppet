#!/bin/ksh
# ==================================================================================================
# NAME:		GetProcessCounts.sh                                 
# 
# AUTHOR:	Maureen Buotte                                    
#
# PURPOSE:	This utility will check the number of processes on each databases and send 
#			mail when the process count for a database reaches a specified alert number.
#
# USAGE:	GetProcessCounts.sh
#
# Frank Davis  10/10/2013  Fixed syntax error in script.
# Frank Davis  09/30/2013  Remove team_dba@eagleaccess.com from email
# Frank Davis  09/24/2013  Remove tech_support@eagleaccess.com from email
# Frank Davis  06/14/2013  Made script compatible with Linux and Solaris.
# Frank Davis  06/13/2013  Made script compatible with Service Now.
# Frank Davis  06/30/2011  Added team_dba@eagleaccess.com email address whereever tech_support@eagleaccess.com was specified
# Frank Davis           24-Jun-2011  Modified IPCheck Variable to included updated list of VLANs
# Nishigandha Sathe     02-Mar-2010  Modified to use new primary key machines(instance)
# Maureen Buotte   	20-Jul-2009  Modified to run on Hawaii using the new infprd1 database
# Maureen Buotte   	17-Jan-2009  Allow for more than 1 SID Exception, change print to print, fix tail + with tail -n
# Maureen Buotte   	05-Aug-2008  Added serveroutput setting because glogin.sql is not consistent on all database installs
# Maureen Buotte    	01-Jun-2008  Added reporting to DBA database
# ==================================================================================================
#
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
    print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail.dat
    print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail.dat
    print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail.dat
    print "Impact: $IMPACT" >> $WORKING_DIR/mail.dat
    print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail.dat

    if [[ -n $1 ]]
    then
     print "\n$1\n" >> $WORKING_DIR/mail.dat
    fi

    if [[ -f $ERROR_FILE ]]
    then
     cat $ERROR_FILE >> $WORKING_DIR/mail.dat
     rm $ERROR_FILE
    fi

    cat $WORKING_DIR/mail.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX for $ORACLE_SID" $MAILTO
    rm -f $WORKING_DIR/mail.dat

    # Update the mail counter
    MAIL_COUNT=${MAIL_COUNT}+1
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database
{
	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
	set heading off feedback off
        select 1 from dual;
EOF
`
    PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
    if [ $PERFORM_CRON_STATUS -ne 1 ]
    then
     PERFORM_CRON_STATUS=0
    fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
EOF
`
 DB_INSTANCE=$(print $DB_INSTANCE |tr -d " ")
 export DB_INSTANCE
 if [[ -z $DB_INSTANCE ]]
 then
  SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
  PERFORM_CRON_STATUS=0
  return 1	
 fi

 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ -z $Sequence_Number ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000 heading off feedback off
  declare i number;
  begin
   insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off 
 update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;

 update INSTANCE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from instance_cron_job_runs where instance=${Sequence_Number})
 where instance=${Sequence_Number};
 commit;
/
EOF
`
 print  "$PROGRAM_NAME for $ORACLE_SID Completed $(date +\"%c\") with a status of $PROCESS_STATUS"
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open
{
 OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
 set heading off feedback off
 select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
EOF
`
 OPEN=$(print $OPEN|tr -d " ")
 if [ $OPEN -eq 1 ]
 then
  return 1
 else
  return 0
 fi
}

# -------------------------------------------------------------------------------------------------------------
# funct_check_process_count(): Check database process count and send mail if greater than 90% of the maximum
# -------------------------------------------------------------------------------------------------------------
funct_check_process_count ()
{
 MAX_PROCESS=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
 set heading off feedback off 
 select value from v\\$parameter where name='processes';
EOF
`
 MAX_PROCESS=$(print $MAX_PROCESS|tr -d " ")
 
 proc_count=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
 set heading off feedback off
 select count(*) from v\\$process;
EOF
`
 proc_count=$(print $proc_count|tr -d " ")

 print "${RunTime}	${proc_count}" >> $CountFile

 percent_used=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
 set heading off feedback off
 select ceil ( (${proc_count}/${MAX_PROCESS} ) * 100)from dual;
EOF
`
 percent_used=$(print $percent_used|tr -d " ")

 if [ $percent_used -ge 90 ]
 then
  URGENCY=1
  SendNotification "The current number of processes (${proc_count}) is ${percent_used}% of the maximum number (${MAX_PROCESS}) \n The database may need to be taken down so that the processes parameter can be increased \n "
  status=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
  set feedback off heading off termout off pagesize 0 linesize 132
  spool $FullProcessReport
  select osuser, username, program from  v\\$session order by osuser;
EOF
`
 fi
}

# *****************************
#  MAIN 
# *****************************
#
set -x

CMP=0
ps -ef | grep asm_smon_+ASM | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 CMP=1
fi

export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export DB_INSTANCE 

if [ $CMP -eq 0 ]
then
 export WORKING_DIR="/u01/app/oracle/local/dba/processes"
else
 export WORKING_DIR=/usr/local/scripts/dba/processes
fi

export DATABASES_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_databases.txt
export ERROR_FILE=$WORKING_DIR/${PROGRAM_NAME_FIRST}_error.txt

RunDate=$(date +%m/%d/%y)
RunTime=$(date +%H:%M:%S)
NumericDate=$(date +%Y%m%d)
NumericTime=$(date +%H%M%S)

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 AWK=awk
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 AWK=nawk
fi

# Get environment variables set up - need to do this differently for RHEL 5
export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

# Create file with all databases currently running on this box
ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}' > $DATABASES_FILE

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
if [ $CMP -eq 0 ]
then
 export PAR_HOME=$HOME/local/dba
else
 export PAR_HOME=/usr/local/scripts/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -f $PARFILE ]]
then
 STATUS=$(sed -e '/^#/d' $PARFILE > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(grep "DONT_RUN_ANYTHING:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(grep "${PROGRAM_NAME}:" $NO_COMMENT_PARFILE)
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f $NO_COMMENT_PARFILE

HOSTNAME=$(hostname)
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
IPCheck=$(cat /etc/hosts | grep $HOSTNAME | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=2
 export URGENCY=3
else
 export TYPE=TEST
 export IMPACT=2
 export URGENCY=3
fi
#export MAILTO='eaglecsi@eagleaccess.com'
export MAILTO='sn_alert_dev@eagleinvsys.com,sn_alert_test@eagleinvsys.com'

NumericDate=$(date +%Y%m%d)

# Loop through the list of Oracle instances to run against
cat $DATABASES_FILE | while read LINE
do
 Sequence_Number=0
 export ORACLE_SID=$LINE
 if [[ -z $ORACLE_SID ]]
 then
  continue
 fi

 export MAIL_COUNT=0

 ErrorFile=$WORKING_DIR/ErrorFile_$ORACLE_SID
 rm -f $ErrorFile

 export PATH=/usr/local/bin:$PATH
 . /usr/local/bin/oraenv > /dev/null
 export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
 export LD_LIBRARY_PATH=$ORACLE_HOME/lib

 # Make sure this database is not in list of SIDs that nothing should be run against
 EXCEPTION=$($AWK -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
 if [ $EXCEPTION -ne 0 ]
 then
  continue
 fi

 # Make sure this database is not in list of SIDs that this job should not be run for
 EXCEPTION=$($AWK -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}')
 if [ $EXCEPTION -ne 0 ]
 then
  continue
 fi

 # Make sure database is OPEN if not, skip to the next database
 funct_verify_open
 if [ $? -eq 0 ]
 then
  continue
 fi

 # Add entry into DBA Admin database
 if [ $PERFORM_CRON_STATUS -eq 1 ]
 then
  funct_initial_audit_update
  if [ $? -ne 0 ]
  then
   PERFORM_CRON_STATUS=0
  fi
  export DB_INSTANCE
 fi

 mkdir -p $WORKING_DIR/counts

 # Check the process count for the current database
 CountBaseFile=${ORACLE_SID}_ProcessCount_${NumericDate}.dat
 FullBaseFile=${ORACLE_SID}_FullProcessReport_${NumericDate}_${NumericTime}.dat
 CountFile=$WORKING_DIR/counts/$CountBaseFile
 FullProcessReport=$WORKING_DIR/counts/$FullBaseFile
 funct_check_process_count

 # Update Infrastructure database
 if [ $PERFORM_CRON_STATUS -eq 1 ]
 then
  if [ $MAIL_COUNT -gt 0 ]
  then
   PROCESS_STATUS='WARNING'
  else
   PROCESS_STATUS='SUCCESS'
  fi
  funct_final_audit_update
 fi

 # Purge any old Files
 find $WORKING_DIR/counts -name "${ORACLE_SID}_*.dat" -mtime +30 -exec rm -f {} \;

done

exit 0
