
set lines 1000 pages 9999
col elapsed_time format 999,999,999
col tot_exec format 999,999,999,999
col BUFF_GETS format 999,999,999,999,999
select * from (
select stat.sql_id as sql_id, sum(elapsed_time_delta) / 1000000 as 
elapsed_time,sum(executions_delta) as TOT_EXEC,NVL(sum(buffer_gets_delta),0) as BUFF_GETS,
     (select st.sql_text
     from dba_hist_sqltext st
     where st.dbid = stat.dbid and st.sql_id = stat.sql_id) as SQL_TEXT
from dba_hist_sqlstat stat, dba_hist_sqltext text,dba_hist_snapshot snap
where stat.sql_id = text.sql_id and
       stat.dbid   = text.dbid and
       snap.SNAP_ID = stat.SNAP_ID and
       snap.begin_interval_time > (sysdate-1)
group by stat.dbid, stat.sql_id
) where ROWNUM <= 10
order by elapsed_time desc;
