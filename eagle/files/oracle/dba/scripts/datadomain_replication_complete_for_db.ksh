#!/bin/ksh
# Script name:  datadomain_replication_complete_for_db.ksh
# Usage:  datadomain_replication_complete_for_db.ksh <Oracle SID>

function everett_listing
{
 find $backup_dir -type f -mtime -1 | xargs ls -lrt > $tmp_dir/local_${sid}_directory_listing.txt
 grep "\_RESTORE_INFO\.TXT" $tmp_dir/local_${sid}_directory_listing.txt > $tmp_dir/local_${sid}_directory_just_restore_info.txt
 restore_info_file=$(grep "\_RESTORE_INFO\.TXT" $tmp_dir/local_${sid}_directory_listing.txt | awk '{print $NF}' | awk -F/ '{print $NF}')
 head -1 $tmp_dir/local_${sid}_directory_listing.txt > $tmp_dir/local_${sid}_directory_listing3.txt
 grep "\_RESTORE_INFO\.TXT" $tmp_dir/local_${sid}_directory_listing3.txt > /dev/null
 if [ $? -ne 0 ]
 then
  sed "1,/$restore_info_file/d" $tmp_dir/local_${sid}_directory_listing.txt > $tmp_dir/local_${sid}_directory_listing2.txt
  cat $tmp_dir/local_${sid}_directory_just_restore_info.txt > $tmp_dir/local_${sid}_directory_listing.txt
  cat $tmp_dir/local_${sid}_directory_listing2.txt >> $tmp_dir/local_${sid}_directory_listing.txt
 fi
 rm -f $tmp_dir/local_${sid}_directory_listing3.txt
 sort $tmp_dir/local_${sid}_directory_listing.txt > $tmp_dir/local_${sid}_directory_listing2.txt
 rm -f $tmp_dir/local_${sid}_directory_listing.txt $tmp_dir/local_${sid}_directory_just_restore_info.txt
}

function pittsburgh_listing
{
 ssh -q oracle@pnld0002 "find $backup_dir -type f -mtime -1 | xargs ls -lrt" > $tmp_dir/remote_${sid}_directory_listing.txt
 grep "\_RESTORE_INFO\.TXT" $tmp_dir/remote_${sid}_directory_listing.txt > $tmp_dir/remote_${sid}_directory_just_restore_info.txt
 restore_info_file=$(grep "\_RESTORE_INFO\.TXT" $tmp_dir/remote_${sid}_directory_listing.txt | awk '{print $NF}' | awk -F/ '{print $NF}')
 head -1 $tmp_dir/remote_${sid}_directory_listing.txt > $tmp_dir/remote_${sid}_directory_listing3.txt
 grep "\_RESTORE_INFO\.TXT" $tmp_dir/remote_${sid}_directory_listing3.txt > /dev/null
 if [ $? -ne 0 ]
 then
  sed "1,/$restore_info_file/d" $tmp_dir/remote_${sid}_directory_listing.txt > $tmp_dir/remote_${sid}_directory_listing2.txt
  cat $tmp_dir/remote_${sid}_directory_just_restore_info.txt > $tmp_dir/remote_${sid}_directory_listing.txt
  cat $tmp_dir/remote_${sid}_directory_listing2.txt >> $tmp_dir/remote_${sid}_directory_listing.txt
 fi
 rm -f $tmp_dir/remote_${sid}_directory_listing3.txt
 sort $tmp_dir/remote_${sid}_directory_listing.txt > $tmp_dir/remote_${sid}_directory_listing2.txt
 rm -f $tmp_dir/remote_${sid}_directory_listing.txt $tmp_dir/remote_${sid}_directory_just_restore_info.txt
}

########### MAIN ###################
sid=$1
backup_dir=$(find /datadomain/*/prod?/ -name $sid)
location=$(print $backup_dir | awk -F/ '{print $3}')
tmp_dir=/tmp

bad=0
if [ $location = evt ]
then
 everett_listing
 pittsburgh_listing
 diff $tmp_dir/local_${sid}_directory_listing2.txt $tmp_dir/remote_${sid}_directory_listing2.txt
 if [ $? -ne 0 ]
 then
  bad=1
 fi
else
 pittsburgh_listing
 everett_listing
 diff $tmp_dir/remote_${sid}_directory_listing2.txt $tmp_dir/local_${sid}_directory_listing2.txt
 if [ $? -ne 0 ]
 then
  bad=1
 fi
fi

if [ $bad -ne 1 ]
then
 print "DataDomain Replication Complete for $sid"
else
 print "DataDomain Replication NOT Complete for $sid"
 print "See differences above."
 print "< means the source of backup.  > means the target of backup"
 if [ $location = evt ]
 then
  print "For $sid Source is Everett and Target is Pittsburgh."
 else
  print "For $sid Source is Pittsburgh and Target is Everett."
 fi
fi
rm -f $tmp_dir/local_${sid}_directory_listing2.txt $tmp_dir/remote_${sid}_directory_listing2.txt

exit 0
