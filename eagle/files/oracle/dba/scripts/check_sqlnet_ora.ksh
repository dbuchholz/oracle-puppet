#!/bin/ksh
# ==================================================================================================
# NAME:     check_sqlnet_ora.ksh
#
# AUTHOR:   Frank Davis
#
# PURPOSE:  Reports on sqlnet.ora file with non-standard contents.
#
# USAGE:    check_sqlnet_ora.ksh
# This script does not take any parameters.
#
# Frank Davis  09/30/2013  Remove team_dba@eagleaccess.com from email
# Frank Davis  09/24/2013  Remove tech_support@eagleaccess.com from email
# Frank Davis  06/13/2013  Made script compatible with Service Now.
# Frank Davis  09/06/2012  Initial Implementation
#
# ==================================================================================================
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

HOSTNAME=$(hostname)
PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
BOX=$(print $HOSTNAME | awk -F. '{print $1}')

export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE
export TYPE=PROD
export IMPACT=2
export URGENCY=3
export MAILTO='eaglecsi@eagleaccess.com'

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/machine_info_sqlnet.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where lower(OS_TYPE) not in ('sunos','windows') order by name;
EOF

rm -f $log_dir/all_exceptions_in_sqlnet_ora.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 5
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  print "$name Did not respond in five seconds.  Killing the ssh command."
  kill -9 $the_pid
  ((no_response = 1))
  break
 done
 if [ $no_response -eq 0 ]
 then
  os_type=$(ssh -nq $ip_address uname)
  ORATAB=/etc/oratab
  if [[ -n $os_type ]]
  then
   if [ $os_type = SunOS ]
   then
    ORATAB=/var/opt/oracle/oratab
   fi
  fi
  sqlnet=network/admin/sqlnet.ora
  ora_homes=$(ssh -nq $ip_address grep -v '^\*' $ORATAB | grep -v '^#' | grep -v '^$' | awk -F: '{print $2}' | sort -u)
  for an_ora_home in $ora_homes
  do
   invalid_home=$(print $an_ora_home | grep -v oracle)
   if [[ -n $invalid_home ]]
   then
    continue
   fi
   ssh -nq $ip_address ls -l $an_ora_home/$sqlnet > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
    print "$name $an_ora_home/$sqlnet does not exist" >> $log_dir/all_exceptions_in_sqlnet_ora.txt
    print " " >> $log_dir/all_exceptions_in_sqlnet_ora.txt
    continue
   fi
   ssh -nq $ip_address grep -v '^#' $an_ora_home/$sqlnet | grep -vi 'NAMES.DIRECTORY_PATH= (TNSNAMES)' | grep -vi 'Sqlnet.expire_time=2' | grep -vi 'SQLNET.INBOUND_CONNECT_TIMEOUT = 0' > $log_dir/exceptions_in_sqlnet_ora.txt
   if [[ -s $log_dir/exceptions_in_sqlnet_ora.txt ]]
   then
    print "Server: $name   Oracle Home: $an_ora_home" >> $log_dir/all_exceptions_in_sqlnet_ora.txt
    cat $log_dir/exceptions_in_sqlnet_ora.txt >> $log_dir/all_exceptions_in_sqlnet_ora.txt
    print " " >> $log_dir/all_exceptions_in_sqlnet_ora.txt
   fi
  done
 fi
done<$log_dir/machine_info_sqlnet.txt

if [[ -s $log_dir/all_exceptions_in_sqlnet_ora.txt ]]
then
 print "$PROGRAM_NAME \n     Machine: $BOX " > mail_sqlnet_ora.dat
 print "\nCategory: $CATEGORY" >> $log_dir/mail_sqlnet_ora.dat
 print "Subcategory: $SUBCATEGORY" >> $log_dir/mail_sqlnet_ora.dat
 print "Impact: $IMPACT" >> $log_dir/mail_sqlnet_ora.dat
 print "Urgency: $URGENCY\n" >> $log_dir/mail_sqlnet_ora.dat
 print "The output below shows where the sqlnet.ora files are not what is expected:" >> $log_dir/mail_sqlnet_ora.dat
 cat $log_dir/all_exceptions_in_sqlnet_ora.txt >> $log_dir/mail_sqlnet_ora.dat
 print "\nThis is what the contents should be:" >> $log_dir/mail_sqlnet_ora.dat
 print "NAMES.DIRECTORY_PATH= (TNSNAMES)" >> $log_dir/mail_sqlnet_ora.dat
 print "Sqlnet.expire_time=2" >> $log_dir/mail_sqlnet_ora.dat
 print "SQLNET.INBOUND_CONNECT_TIMEOUT = 0\n" >> $log_dir/mail_sqlnet_ora.dat
 print "The white space is significant on the three lines and on other lines of the file.  The three lines needs to match exactly as above." >> $log_dir/mail_sqlnet_ora.dat
 print "No empty lines allowed, including white space only lines.\n" >> $log_dir/mail_sqlnet_ora.dat
 print "The best option is to copy another sqlnet.ora file over this one if an exception is found." >> $log_dir/mail_sqlnet_ora.dat

 cat $log_dir/mail_sqlnet_ora.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX " $MAILTO
 rm -f $log_dir/mail_sqlnet_ora.dat

fi
rm -f $log_dir/exceptions_in_sqlnet_ora.txt
exit 0
