#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where OS_TYPE<>'SunOS' order by name;
EOF

rm -f expire_time.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   print "Cannot reach $name"
   break
  fi
  sleep 1
 done
 if [ $no_response -eq 0 ]
 then
  the_line=$(ssh -nq $ip_address grep -i INBOUND_CONNECT_TIMEOUT $ORACLE_HOME/network/admin/listener.ora)
  print "$name $the_line" >> expire_tim2.txt
 fi
done<machine_info.txt
exit 0
