####################################################################################
# Author: Nikhil Kulkarni                                                          #
# Date:                                                                            #
# Usage: v12standard_report.sh <SID>                                                      #
# Program:                                                                         #
#                                                                                  #
#                                                                                  #
####################################################################################
#set -x

if [ $# -ne 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!\n"
   print "\t\tUsage : $0 <ORACLE_SID>\n"
   print "\t\t<ORACLE_SID> should be of target database on the host \n"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "\n\n\t\tCannot find Instance $1 in $ORATAB\n\n"
   exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $? -ne 0 ]
then
 print "\n\n\t\t There seems to be some problem please rectify and Execute Again\n\nAborting Here...."
 exit 3
fi

export log_dir=$(pwd)


snapshot_interval=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
        set heading off
        set feedback off
select extract( day from snap_interval) *24*60+extract( hour from snap_interval) *60+extract( minute from snap_interval ) from
dba_hist_wr_control a,V\\$database b where a.dbid=b.dbid;
EOF`
v_snapshot_interval=`echo $snapshot_interval`


retention_period=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
        set heading off
        set feedback off
select extract( day from retention) *24*60+extract( hour from retention) *60+ extract( minute from retention)
 from dba_hist_wr_control a,V\\$database b where a.dbid=b.dbid;
EOF`
v_retention_period=`echo $retention_period`

VERSION=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F/ '{print $6}')
if [ $VERSION = 11.2.0 ]
then
COMPATIBLE_VALUE=11.2.0.1
fi
if [ $VERSION = 11.2.0.3.0 ]
then
COMPATIBLE_VALUE=11.2.0.3.0
fi


${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
spool insert.log
set lines 200 pages 0 head off feed off
create table standard_parameters ( PARAMETER  VARCHAR2(50),standard_value VARCHAR2(450),suggested_value VARCHAR2(450) );
create table database_parameters ( PARAMETER  VARCHAR2(50),value VARCHAR2(450) );
create table EA_TRIG_PARA ( owner VARCHAR2(30),PARAMETER VARCHAR2(30),ON_OFF NUMBER(1),VALUE VARCHAR2(254) );
create table WRONG_TRIG_PARA (owner VARCHAR2(30),PARAMETER VARCHAR2(30),ON_OFF NUMBER(1),VALUE VARCHAR2(254) );

insert into database_parameters values ('snapshot_interval','$v_snapshot_interval');
insert into database_parameters values ('retention_period','$v_retention_period');
insert into database_parameters (parameter,value) select name,value from V\$parameter;
commit;


DECLARE
   l_is_matching_row NUMBER(10);
begin
    select count (*)
    into   l_is_matching_row
    from   dba_objects
    where  object_name = 'ESTAR_LOGON';
        if (l_is_matching_row = 2)
    then
      insert into database_parameters
             (parameter, value)
      values ('estar_logon','Login triggers EXIST')
      ;
      commit;
    else
insert into database_parameters
             (parameter, value)
      values ('estar_logon','Login triggers DOEN NOT EXIST')
      ;
commit;
    end if;	
end;
/

insert into standard_parameters values('sga_target','Should not be 0 / Recommended is 4GB',4294967296);
insert into standard_parameters values('undo_management','AUTO','AUTO');
insert into standard_parameters values('optimizer_mode','ALL_ROWS','ALL_ROWS');
insert into standard_parameters values('db_block_size','8192','8192');
insert into standard_parameters values('processes','More than 1500',1500);
insert into standard_parameters values('disk_asynch_io','TRUE','TRUE');
insert into standard_parameters values('cursor_sharing','EXACT','EXACT');
insert into standard_parameters values('db_files','More than 350','350');
insert into standard_parameters values('open_cursors','4000','4000');
insert into standard_parameters values('log_buffer','10MB or More than 10MB',10485760);
insert into standard_parameters values('log_checkpoints_to_alert','TRUE','TRUE');
insert into standard_parameters values('log_checkpoint_timeout','0','0');
insert into standard_parameters values('db_cache_advice','ON','ON');
insert into standard_parameters values('db_file_multiblock_read_count','8','8');
insert into standard_parameters values('workarea_size_policy','AUTO','AUTO');
insert into standard_parameters values('parallel_adaptive_multi_user','FALSE','FALSE');
insert into standard_parameters values('parallel_automatic_tuning','FALSE','FALSE');
insert into standard_parameters values('pga_aggregate_target','More than or equal to 2560MB','2560');
insert into standard_parameters values('query_rewrite_enabled','TRUE','TRUE');
insert into standard_parameters values('query_rewrite_integrity','TRUSTED','TRUSTED');
insert into standard_parameters values('sessions','Beween 2000 - 4500','2000');
insert into standard_parameters values('session_cached_cursors','1000 or more than 1000','1000');
insert into standard_parameters values('timed_statistics','TRUE','TRUE');
insert into standard_parameters values('db_flashback_retention_target','1440','1440');
insert into standard_parameters values('fast_start_mttr_target','300','300');
insert into standard_parameters values('undo_retention','10800','10800');
insert into standard_parameters values('recyclebin','OFF','OFF');
insert into standard_parameters values('remote_login_passwordfile','EXCLUSIVE','EXCLUSIVE');
insert into standard_parameters values('audit_sys_operations','TRUE','TRUE');
insert into standard_parameters values('audit_trail','DB_EXTENDED','DB_EXTENDED');
insert into standard_parameters values('compatible','$COMPATIBLE_VALUE','$COMPATIBLE_VALUE');
insert into standard_parameters values('sec_case_sensitive_logon','FALSE','FALSE');
insert into standard_parameters values('dml_locks','parameter called transaction * 4','20000');
insert into standard_parameters values('snapshot_interval','30','30');
insert into standard_parameters values('retention_period','44640','44640');
insert into standard_parameters values('estar_logon','Login trigger should exist','Login triggers EXIST');
commit;

DECLARE
   l_is_exist varchar2(50);
begin
    select value
    into   l_is_exist
    from   database_parameters
    where  parameter = 'estar_logon';
        if (l_is_exist = 'Login triggers EXIST')
    then
insert into EA_TRIG_PARA values ('ESTAR','ORACLE_SESSION',1,'alter session set "cursor_sharing"=EXACT');
insert into EA_TRIG_PARA values ('ESTAR','ORACLE_SESSION',0,'alter session set "_optim_peek_user_binds"=false');
insert into EA_TRIG_PARA values ('PACE_MASTERDBO','ORACLE_SESSION',1,'alter session set "cursor_sharing"=EXACT');
insert into EA_TRIG_PARA values ('PACE_MASTERDBO','ORACLE_SESSION',1,'alter session set "_unnest_subquery"=FALSE');
insert into EA_TRIG_PARA values ('PACE_MASTERDBO','ORACLE_SESSION',1,'alter session set "_unnest_subquery"=FALSE');
commit;
insert into WRONG_TRIG_PARA (select * from EA_TRIG_PARA minus select * from eaglemgr.startup_parameters);
commit;
    end if;	
end;
/

spool off


spool select_report.log
set pages 1000
set lines 200
set feedback off
set echo off
col CURRENT_VALUE for a35
col PARAMETER for a35
col DESCRIPTION for a35
col SUGGESTED_VALUE for a25


select '******************************LIST OF PARAMETERS WHICH DO NOT MATCH WITH STANDARD VALUES*************' from dual;
select '                                                                                                        ' from dual;
set heading on
select a.PARAMETER,a.VALUE CURRENT_VALUE,b.STANDARD_VALUE DESCRIPTION,b.SUGGESTED_VALUE from
(select * from (
select parameter ,value from database_parameters where parameter='undo_management' and value !='AUTO' union
select parameter ,value from database_parameters where parameter='optimizer_mode' and value !='ALL_ROWS' union
select parameter ,value from database_parameters where parameter='db_block_size' and value !=8192 union
select parameter ,value from database_parameters where parameter='processes' and value < 1500 union
select parameter ,value from database_parameters where parameter='disk_asynch_io' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='cursor_sharing' and value != 'EXACT' union
select parameter ,value from database_parameters where parameter='db_files' and value < 350 union
select parameter ,value from database_parameters where parameter='open_cursors' and VALUE < 4000 union
select parameter ,value from database_parameters where parameter='optimizer_index_cost_adj' and value !=100 union
select parameter ,value from database_parameters where parameter='log_buffer' and value/1024/1024 < 10 union
select parameter ,value from database_parameters where parameter='log_checkpoints_to_alert' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='log_checkpoint_interval' and value != 0 union
select parameter ,value from database_parameters where parameter='log_checkpoint_timeout' and value != 0 union
select parameter ,value from database_parameters where parameter='sga_target' and value = 0 and value < 3221225472 union
select parameter ,value from database_parameters where parameter='db_cache_advice' and value != 'ON' union
select parameter ,value from database_parameters where parameter='db_file_multiblock_read_count' and value < 8 union
select parameter ,value from database_parameters where parameter='workarea_size_policy' and value != 'AUTO' union
select parameter ,value from database_parameters where parameter='parallel_adaptive_multi_user' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='parallel_automatic_tuning' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='pga_aggregate_target' and value/1024/1024 < 2560 union
select parameter ,value from database_parameters where parameter='query_rewrite_enabled' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='query_rewrite_integrity' and value != 'TRUSTED' union
select parameter ,value from database_parameters where parameter='sessions' and value < 2000 and  value > 4500 union
select parameter ,value from database_parameters where parameter='session_cached_cursors' and  value < 1000 union
select parameter ,value from database_parameters where parameter='timed_statistics' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='db_flashback_retention_target' and value != 1440 union
select parameter ,value from database_parameters where parameter='fast_start_mttr_target' and value != 300 union
select parameter ,value from database_parameters where parameter='undo_retention' and value < 10800 union
select parameter ,value from database_parameters where parameter='recyclebin' and value != 'OFF' union
select parameter ,value from database_parameters where parameter='remote_login_passwordfile' and value != 'EXCLUSIVE' union
select parameter ,value from database_parameters where parameter='audit_sys_operations' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='audit_trail' and value != 'DB_EXTENDED' union
select parameter ,value from database_parameters where parameter='compatible' and value not like '$COMPATIBLE_VALUE' union
select parameter ,value from database_parameters where parameter='sec_case_sensitive_logon' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='snapshot_interval' and value != '30' union
select parameter ,value from database_parameters where parameter='estar_logon' and value != 'Login triggers EXIST' union
select parameter ,value from database_parameters where parameter='retention_period' and value != '44640' union
select parameter ,value from database_parameters where parameter='dml_locks' and
value != (select c.value *4 from database_parameters c where c.parameter='transactions'))) a,
standard_parameters b where a.parameter= b.parameter;

set serveroutput on
DECLARE
   l_match_row NUMBER(10);
begin
    select count (*)
    into   l_match_row
    from   WRONG_TRIG_PARA;
            if (l_match_row > 0)
    then
     dbms_output.put(CHR(10));
     DBMS_OUTPUT.PUT_LINE ('Triggers that you created are wrong ONE.Please re-execute the correct trigger script kept at Hawaii.');
    dbms_output.put(CHR(10));
dbms_output.put(CHR(10));
 DBMS_OUTPUT.PUT_LINE ('BEST OF LUCK');
    end if;	
end;
/

set heading off
exit

${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
set head off
set feedback off

spool implement_parameter1.sql

select 'alter system set '||a.PARAMETER||'='||b.SUGGESTED_VALUE||' scope=spfile;' from
(select * from (
select parameter ,value from database_parameters where parameter='undo_management' and value !='AUTO' union
select parameter ,value from database_parameters where parameter='optimizer_mode' and value !='ALL_ROWS' union
select parameter ,value from database_parameters where parameter='db_block_size' and value !=8192 union
select parameter ,value from database_parameters where parameter='processes' and value < 1500 union
select parameter ,value from database_parameters where parameter='disk_asynch_io' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='cursor_sharing' and value != 'EXACT' union
select parameter ,value from database_parameters where parameter='db_files' and value < 350 union
select parameter ,value from database_parameters where parameter='open_cursors' and VALUE < 4000 union
select parameter ,value from database_parameters where parameter='optimizer_index_cost_adj' and value !=100 union
select parameter ,value from database_parameters where parameter='log_buffer' and value/1024/1024 < 10 union
select parameter ,value from database_parameters where parameter='log_checkpoints_to_alert' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='log_checkpoint_interval' and value != 0 union
select parameter ,value from database_parameters where parameter='log_checkpoint_timeout' and value != 0 union
select parameter ,value from database_parameters where parameter='sga_target' and value = 0 and value < 3221225472 union
select parameter ,value from database_parameters where parameter='db_cache_advice' and value != 'ON' union
select parameter ,value from database_parameters where parameter='db_file_multiblock_read_count' and value < 8 union
select parameter ,value from database_parameters where parameter='workarea_size_policy' and value != 'AUTO' union
select parameter ,value from database_parameters where parameter='parallel_adaptive_multi_user' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='parallel_automatic_tuning' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='pga_aggregate_target' and value/1024/1024 < 2560 union
select parameter ,value from database_parameters where parameter='query_rewrite_enabled' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='query_rewrite_integrity' and value != 'TRUSTED' union
select parameter ,value from database_parameters where parameter='sessions' and value < 2000 and  value > 4500 union
select parameter ,value from database_parameters where parameter='session_cached_cursors' and  value < 1000 union
select parameter ,value from database_parameters where parameter='timed_statistics' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='db_flashback_retention_target' and value != 1440 union
select parameter ,value from database_parameters where parameter='fast_start_mttr_target' and value != 300 union
select parameter ,value from database_parameters where parameter='undo_retention' and value < 10800 union
select parameter ,value from database_parameters where parameter='recyclebin' and value != 'OFF' union
select parameter ,value from database_parameters where parameter='remote_login_passwordfile' and value != 'EXCLUSIVE' union
select parameter ,value from database_parameters where parameter='audit_sys_operations' and value != 'TRUE' union
select parameter ,value from database_parameters where parameter='audit_trail' and value != 'DB_EXTENDED' union
select parameter ,value from database_parameters where parameter='compatible' and value not like '$COMPATIBLE_VALUE' union
select parameter ,value from database_parameters where parameter='sec_case_sensitive_logon' and value != 'FALSE' union
select parameter ,value from database_parameters where parameter='snapshot_interval' and value != '30' union
select parameter ,value from database_parameters where parameter='estar_logon' and value != 'Login triggers EXIST' union
select parameter ,value from database_parameters where parameter='retention_period' and value != '44640' union
select parameter ,value from database_parameters where parameter='dml_locks' and
value != (select c.value *4 from database_parameters c where c.parameter='transactions'))) a,
standard_parameters b where a.parameter= b.parameter and 
a.parameter not in ('compatible','estar_logon','retention_period','snapshot_interval'); 

spool off

exit


${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF

drop table database_parameters;
drop table standard_parameters;
drop table EA_TRIG_PARA;
drop table WRONG_TRIG_PARA;

exit

/n/n/n print " COMPLETED !!!!!! BYE BYE "



