#!/bin/ksh
# Script name: metadata.ksh
# Usage:       metadata.ksh [Oracle SID] {Username}
#
if [ $# -ne 2 ]
then
   print "Must pass the Oracle SID and username/role name into this script."
   exit 1
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" /etc/oratab | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "Cannot find Instance $ORACLE_SID in /etc/oratab"
 exit 2
fi

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

schema=$2
if [[ -n $schema ]]
then
 schema=$(print $schema | tr '[a-z]' '[A-Z]')
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$log_dir/${schema}_grants_with_errors_in_${ORACLE_SID}.txt
set long 9999999 pagesize 0 trimspool on feedback off linesize 200 longchunksize 9999999
select dbms_metadata.get_granted_ddl('OBJECT_GRANT','$schema') from dual;
select dbms_metadata.get_granted_ddl('SYSTEM_GRANT','$schema') from dual;
select dbms_metadata.get_granted_ddl('ROLE_GRANT','$schema') from dual;
EOF
egrep -v '^ERROR:$|^ORA-31608:|^ORA-06512:|^$' $log_dir/${schema}_grants_with_errors_in_${ORACLE_SID}.txt | \
sed -e 's/$/;/g' -e 's/"//g' -e 's/^[ \t]*//' > $log_dir/all_${schema}_grants_in_${ORACLE_SID}.txt
rm -f $log_dir/${schema}_grants_with_errors_in_${ORACLE_SID}.txt
fi
rm -f $log_dir/${schema}_all_grants_with_errors_in_${ORACLE_SID}.txt
exit 0
