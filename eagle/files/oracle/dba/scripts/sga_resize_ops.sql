set lines 200
col timed_at format a20
col oper_type format a20
col component format a50
col parameter format a35
break on parameter skip 1
select
    to_char(start_time,'hh24:mi:ss') timed_at,
    oper_type,
    component,
    parameter,
    oper_mode,
    initial_size,
    final_size
from
    v$sga_resize_ops
where
    start_time >= trunc(sysdate)
order by
    component,timed_at;
