drop TRIGGER sys.estar_logon;

ALTER SYSTEM SET CURSOR_SHARING=EXACT;

CREATE OR REPLACE PROCEDURE eaglemgr.startup_parameters_apply
AS
BEGIN
  FOR rec IN
  (SELECT VALUE
  FROM eaglemgr.startup_parameters
  WHERE parameter = 'ORACLE_SESSION'
  AND on_off      = 1
  AND owner       = USER
  )
  LOOP
    BEGIN
      EXECUTE IMMEDIATE rec.VALUE;
      DBMS_OUTPUT.PUT_LINE('SUCCESS (' || rec.VALUE || ')') ;
    EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('FAILED (' || rec.VALUE || ')') ;
    END;
  END LOOP;
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/

CREATE OR REPLACE TRIGGER estar.estar_logon AFTER logon ON estar.schema
  BEGIN
    eaglemgr.startup_parameters_apply;
  END;
/

CREATE OR REPLACE TRIGGER pace_masterdbo.estar_logon AFTER logon ON pace_masterdbo.schema
  BEGIN
    eaglemgr.startup_parameters_apply;
  END;
/

delete from eaglemgr.startup_parameters where value like 'alter %cursor_sharing%';
delete from eaglemgr.startup_parameters where value like 'alter %unnest_subquery%';

Insert into eaglemgr.startup_parameters (OWNER,PARAMETER,ON_OFF,VALUE) 
select 'ESTAR','ORACLE_SESSION',1, mysetting from
(select 'alter session set "cursor_sharing"=EXACT' mysetting from v$instance where substr(version, 1, 3) = '11.'
union all
select 'alter session set "cursor_sharing"=SIMILAR' mysetting from v$instance where substr(version, 1, 3) = '10.') t;

Insert into eaglemgr.startup_parameters (OWNER,PARAMETER,ON_OFF,VALUE) 
select 'PACE_MASTERDBO','ORACLE_SESSION',1, mysetting from
(select 'alter session set "cursor_sharing"=EXACT' mysetting from v$instance where substr(version, 1, 3) = '11.'
union all
select 'alter session set "cursor_sharing"=SIMILAR' mysetting from v$instance where substr(version, 1, 3) = '10.') t;

Insert into eaglemgr.startup_parameters (OWNER,PARAMETER,ON_OFF,VALUE) values 
('ESTAR','ORACLE_SESSION',1,'alter session set "_unnest_subquery"=FALSE');

commit;
/

GRANT EXECUTE ON eaglemgr.startup_parameters_apply TO public;
