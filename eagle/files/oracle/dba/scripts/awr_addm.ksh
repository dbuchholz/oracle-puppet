#!/bin/ksh
#### Added by Nirmal
####################################
NARG=$#
NumUsers=`expr ${NARG} - 1`
if [ ${NARG} -eq 0 ]; then
        clear
        echo ""
        echo ""
        echo "Usage: ./awr_addm.ksh <SID> "
        echo ""
        exit 1
fi
ORA_SID=$1

STATUS=`ps -fu oracle |grep -v grep| grep ora_pmon_${ORA_SID}`
if [ $? != 0 ]; then
     clear
     echo ""
     echo ""
     echo -e "Database ${ORA_SID} is not running or you typed a wrong SID. Please try with valid SID..."
     echo ""
     echo "Usage: ./awr_addm.ksh <SID> "
     echo ""
     echo ""
     echo ""
     exit 1
fi
####################################
export ORACLE_SID=$ORA_SID
export ORAENV_ASK=NO
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>dbid.txt
select dbid from v\$database;
EOF
dbid=$(awk '/DBID/{getline;getline;print $1}' dbid.txt)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
col BEGIN_INTERVAL_TIME format a25
col END_INTERVAL_TIME format a25
set pagesize 0 feedback off trimspool on
select snap_id,END_INTERVAL_TIME from dba_hist_snapshot where dbid=$dbid order by snap_id;
EOF
read begin_snap?"Enter begin snap ID: "
read end_snap?"Enter end snap ID: "
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
define  inst_num     = 1;
define  num_days     = 10;
define  inst_name    = '${ORA_SID}';
define  db_name      = '${ORA_SID}';
define  dbid         = $dbid;
define  begin_snap   = $begin_snap;
define  end_snap     = $end_snap;
define  report_type  = 'html';
define  report_name  = /u01/app/oracle/awr_report_${begin_snap}_${end_snap}.html
@@?/rdbms/admin/awrrpti
define  inst_num     = 1;
define  num_days     = 10;
define  inst_name    = '${ORA_SID}';
define  db_name      = '${ORA_SID}';
define  dbid         = $dbid;
define  begin_snap   = $begin_snap;
define  end_snap     = $end_snap;
define  report_name  = /u01/app/oracle/addm_report_${begin_snap}_${end_snap}.txt
@@?/rdbms/admin/addmrpti
EOF
rm -f dbid.txt
exit 0
