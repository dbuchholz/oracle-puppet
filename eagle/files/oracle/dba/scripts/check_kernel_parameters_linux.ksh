#!/bin/ksh
host=$(print $(hostname) | awk -F. '{print $1}')
print "\n\n*********************** kernel parameter checks for: ************************"
print "\n$host\n"
/sbin/sysctl -a > sysctl.txt 2>&1

suid_dumpable=$(grep 'fs\.suid_dumpable' sysctl.txt | awk '{print $NF}')
if [[ -n $suid_dumpable ]]
then
 if [ $suid_dumpable -ne 1 ]
 then
  print "fs.suid_dumpable = $suid_dumpable set incorrectly.  Needs to be set to 1."
 fi
else
 print "fs.suid_dumpable is not defined. Needs to be set to 1."
fi

aio_max_nr=$(grep 'fs\.aio-max-nr' sysctl.txt | awk '{print $NF}')
if [[ -n $aio_max_nr ]]
then
if [ $aio_max_nr -lt 1048576 ]
then
 print "fs.aio-max-nr = $aio_max_nr set incorrectly.  Needs to be set to 1048576."
fi
else
 print "fs.aio-max-nr is not defined. Needs to be set to 1048576."
fi

file_max=$(grep 'fs\.file-max' sysctl.txt | awk '{print $NF}')
if [[ -n $file_max ]]
then
if [ $file_max -lt 6815744 ]
then
 print "fs.file-max = $file_max set incorrectly.  Needs to be set to 6815744."
fi
else
 print "fs.file-max is not defined. Needs to be set to 6815744."
fi

total_memory=$(free -b | grep Mem | awk '{print $2}')
print "Total Memory =  $total_memory"
shmall=$(grep 'kernel\.shmall' sysctl.txt | awk '{print $NF}')

shmmax=$(grep 'kernel\.shmmax' sysctl.txt | awk '{print $NF}')
print "kernel.shmmax = $shmmax"
print "kernel.shmall =  $shmall"

shmmni=$(grep 'kernel\.shmmni' sysctl.txt | awk '{print $NF}')
if [[ -n $shmmni ]]
then
if [ $shmmni -lt 4096 ]
then
 print "kernel.shmmni = $shmmni set incorrectly.  Needs to be set to at least 4096."
fi
else
 print "kernel.shmmni is not defined. Needs to be set to at least 4096."
fi

sem=$(grep 'kernel\.sem' sysctl.txt)
if [[ -n $sem ]]
then
 first=$(print $sem | awk '{print $3}')
 second=$(print $sem | awk '{print $4}')
 third=$(print $sem | awk '{print $5}')
 fourth=$(print $sem | awk '{print $6}')
 (( found_bad=0 ))
 if [[ -n $first ]]
 then
  if [ $first -lt 250 ]
  then
   print "kernel.sem first parameter must be at least 250. It is currently set at $first"
  (( found_bad=1 ))
  fi
 fi
 if [[ -n $second ]]
 then
  if [ $second -lt 32000 ]
  then
   print "kernel.sem second parameter must be at least 32000. It is currently set at $second"
  (( found_bad=1 ))
  fi
 fi 
 if [[ -n $third ]]
 then
  if [ $third -lt 100 ]
  then
   print "kernel.sem third parameter must be at least 100. It is currently set at $third"
  (( found_bad=1 ))
  fi
 fi
 if [[ -n $fourth ]]
 then
  if [ $fourth -lt 128 ]
  then
   print "kernel.sem fourth parameter must be at least 128. It is currently set at $fourth"
  (( found_bad=1 ))
  fi
 fi
 if [ $found_bad -ne 0 ]
 then
  print "kernel.sem set incorrectly. Needs to be set to at least: kernel.sem = 250 32000 100 128"
 fi
else
 print "kernel.sem is not defined. Needs to be set to at least: kernel.sem = 250 32000 100 128"
fi

rmem_default=$(grep 'net\.core\.rmem_default' sysctl.txt | awk '{print $NF}')
if [[ -n $rmem_default ]]
then
if [ $rmem_default -lt 262144 ]
then
 print "net.core.rmem_default = $rmem_default set incorrectly.  Needs to be set to at least 262144."
fi
else
 print "net.core.rmem_default is not defined. Needs to be set to at least 262144."
fi

rmem_max=$(grep 'net\.core\.rmem_max' sysctl.txt | awk '{print $NF}')
if [[ -n $rmem_max ]]
then
if [ $rmem_max -lt 4194304 ]
then
 print "net.core.rmem_max = $rmem_max set incorrectly.  Needs to be set to at least 4194304."
fi
else
 print "net.core.rmem_max is not defined. Needs to be set to at least 4194304."
fi

wmem_default=$(grep 'net\.core\.wmem_default' sysctl.txt | awk '{print $NF}')
if [[ -n $wmem_default ]]
then
if [ $wmem_default -lt 262144 ]
then
 print "net.core.wmem_default = $wmem_default set incorrectly.  Needs to be set to at least 262144."
fi
else
 print "net.core.wmem_default is not defined. Needs to be set to at least 262144."
fi

wmem_max=$(grep 'net\.core\.wmem_max' sysctl.txt | awk '{print $NF}')
if [[ -n $wmem_max ]]
then
if [ $wmem_max -lt 1048586 ]
then
 print "net.core.wmem_max = $wmem_max set incorrectly.  Needs to be set to at least 1048586."
fi
else
 print "net.core.wmem_max is not defined. Needs to be set to at least 1048586."
fi

print "\n\nStart: /etc/security/limits.conf check ..."

soft_nproc=$(grep 'oracle.*soft.*nproc' /etc/security/limits.conf | awk '{print $NF}')
if [[ -n $soft_nproc ]]
then
 if [ $soft_nproc -lt 2047 ]
 then
  print "Found: oracle              soft    nproc  $soft_nproc"
  print "Should be:"
  print "       oracle              soft    nproc  2047"
 fi
else
 print "\"oracle              soft    nproc\" not defined.  Should be:"
 print "oracle              soft    nproc  2047"
fi

hard_nproc=$(grep 'oracle.*hard.*nproc' /etc/security/limits.conf | awk '{print $NF}')
if [[ -n $hard_nproc ]]
then
 if [ $hard_nproc -lt 16384 ]
 then
  print "Found: oracle              hard    nproc  $hard_nproc"
  print "Should be:"
  print "       oracle              hard    nproc  16384"
 fi
else
 print "\"oracle              hard    nproc\" not defined.  Should be:"
 print "oracle              hard    nproc  16384"
fi

soft_nofile=$(grep 'oracle.*soft.*nofile' /etc/security/limits.conf | awk '{print $NF}')
if [[ -n $soft_nofile ]]
then
 if [ $soft_nofile -lt 65536 ]
 then
  print "Found: oracle              soft    nofile  $soft_nofile"
  print "Should be:"
  print "       oracle              soft    nofile  65536"
 fi
else
 print "\"oracle              soft    nofile\" not defined.  Should be:"
 print "oracle              soft    nofile  65536"
fi

hard_nofile=$(grep 'oracle.*hard.*nofile' /etc/security/limits.conf | awk '{print $NF}')
if [[ -n $hard_nofile ]]
then
 if [ $hard_nofile -lt 65536 ]
 then
  print "Found: oracle              hard    nofile  $hard_nofile"
  print "Should be:"
  print "       oracle              hard    nofile  65536"
 fi
else
 print "\"oracle              hard    nofile\" not defined.  Should be:"
 print "oracle              hard    nofile  65536"
fi

soft_stack=$(grep 'oracle.*soft.*stack' /etc/security/limits.conf | awk '{print $NF}')
if [[ -n $soft_stack ]]
then
 if [ $soft_stack -lt 10240 ]
 then
  print "Found: oracle              soft    stack  $soft_stack"
  print "Should be:"
  print "       oracle              soft    stack  10240"
 fi
else
 print "\"oracle              soft    stack\" not defined.  Should be:"
 print "oracle              soft    stack  10240"
fi
print "End: /etc/security/limits.conf check ..."

exit 0
