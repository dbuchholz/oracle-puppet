#!/bin/ksh
# Script Name: is_listener_running_pittsburgh_imp.ksh
mailrecipients="fdavis@eagleaccess.com"

# Run this script every five minutes from cron.
# Runs the Production check every run or every five minutes
# Runs the sub-Production checks every third check or every 15 minutes.

# How many runs of this script before a list of machines are determined from Big Brother
when_to_query=144           # If running job every five minutes, 144 would be 12 hours to get machine list.
# Machine list does not change often, no sense in getting machine list with every five minute run.
print "Start: $(date)"

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging

        PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
                set heading off
                set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=`echo -e $PERFORM_CRON_STATUS|tr -d " "`
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
                PERFORM_CRON_STATUS=0
        fi
}

function check_listener
{
host_file=$1
while read host_name the_ip
do
 ssh -nq $the_ip $script_dir/is_listener_running.ksh > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
mailx -s  "$(hostname): $0 $(date): Oracle listener problem on $host_name" $mailrecipients<<EOF
A "lsnrctl status" on $host_name did not return successful result.
Log into $host_name and run: lsnrctl status
If this hangs, "kill -9" on PID of the listener process and then issue:  lsnrctl start
EOF
 fi
done<$host_file
}

# MAIN

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

ORACLE_SID=$(grep '^[a-zA-Z].*:.*:' $ORATAB | tail -1 | awk -F: '{print $1}')

export ORACLE_SID
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a ${PARFILE} ]]
then
 STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
 LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export PERFORM_CRON_STATUS=1
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_ANYTHING=$(print $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "${PROGRAM_NAME}:")
 if [[ -n $LINE ]]
 then
  DONT_RUN_THIS_JOB=$(print $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g')
 fi
fi
rm -f ${NO_COMMENT_PARFILE}

if [ -s $log_dir/run_count_query.txt ]
then
 (( query_count=$(cat $log_dir/run_count_query.txt) ))
 (( query_count=query_count+1 ))
 print $query_count > $log_dir/run_count_query.txt
else
 print $when_to_query > $log_dir/run_count_query.txt
 (( query_count=$when_to_query ))
fi

if [ $query_count -eq $when_to_query ] || [ ! -s $log_dir/pittsburgh_imp_servers.txt ]
then 
(( query_count=0 ))
print $query_count > $log_dir/run_count_query.txt
$ORACLE_HOME/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
set pages 0 trimspool on feedback off echo off

spool $log_dir/pittsburgh_imp_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='PITTSBURGH IMP'
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='PITTSBURGH IMP';

EOF

# Get rid of any blank lines in files
sed '/^$/d' $log_dir/pittsburgh_imp_servers.txt > $log_dir/pittsburgh_imp_serversx.txt
mv $log_dir/pittsburgh_imp_serversx.txt $log_dir/pittsburgh_imp_servers.txt
fi

check_listener $log_dir/pittsburgh_imp_servers.txt

print "End: $(date)"
exit 0
