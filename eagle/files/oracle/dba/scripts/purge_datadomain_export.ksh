#!/bin/ksh
find /datadomain/export -type f -name "undo_sql_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].sql" -mtime +365 -exec rm -f {} \;
find /datadomain/export -type f -name "rows_effected_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].txt" -mtime +365 -exec rm -f {} \;
find /datadomain/export -type f -name "run_log_*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9].txt" -mtime +365 -exec rm -f {} \;
exit 0
