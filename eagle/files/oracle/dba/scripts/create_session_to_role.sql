undefine the_user
tti off
set newpage 0
set verify off
set echo off
clear breaks
col sidname new_value ora_sid noprint
col u new_value usr noprint
col uu new_value the_user noprint

select name sidname 
from v$database
/

select upper(replace('&&1','$','_')) u,
	upper('&&1') uu
from dual
/

col ins format a6 head 'Insert'
col upd format a6 head 'Update'
col del format a6 head 'Delete'
col sel format a6 head 'Select'
col tab format a30 head 'Table'
col owner format a30 head 'Owner'
col role format a30 head 'Direct Roles for &&the_user'
col arole format a30 head 'All Roles for &&the_user'

set heading on
set pages 55
set lines 132

spool /opt/oracle/dba/logs/usr_privs_${ORACLE_SID}.lis

tti 'Direct Role''s granted to User: &&the_user|In the &&ora_sid Instance'

select granted_role role
from sys.dba_role_privs
where grantee = '&&the_user'
order by role
/

tti 'All Role''s granted to User: &&the_user|In the &&ora_sid Instance'

select rol.name arole
from sys.user$ rol
where rol.type# = 0
and rol.user# in (
	select privilege#
	from sys.sysauth$
	where privilege# > 0 -- many privs are negative !
	connect by prior privilege# = grantee#
	start with grantee# = (
		select user#
		from sys.user$
		where name = '&&the_user'))
order by arole
/

tti 'ALL System Privileges granted to User: &&the_user|Directly granted or via Roles|In the &&ora_sid Instance'

select  privilege	
from sys.dba_sys_privs dsp
where grantee in  (
	select rol.name
	from sys.user$ rol
	where rol.type# = 0
	and rol.user# in (
	   select privilege#
	   from sys.sysauth$
	   where privilege# > 0 -- many privs are negative !
	   connect by prior privilege# = grantee#
	   start with grantee# = (
		select user#
		from sys.user$
		where name = '&&the_user'))
	union
	select '&&the_user'
	from dual
	union 
	select 'PUBLIC'
	from dual)
/
spool off
