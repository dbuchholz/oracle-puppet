#!/usr/bin/perl
$inputfile=$ARGV[0];
open(INPUT,"$inputfile") || die "Could not open file $inputfile : $!\n";
undef $/;
$str=<INPUT>;
$str=~s/\n/ /g;
$str=uc($str);
$str =~ s/\(.*$//;
close (INPUT);
if ($str !~ /\./)
{
 exit 2;
}
if ($str =~ /CREATE .*PACKAGE BODY/i)
{
 $object_type="PACKAGE BODY";
 $package_count = () = $str =~ /$object_type/g;
}
elsif (($str =~ /CREATE .*PACKAGE/i) && ($str !~ /CREATE .*PACKAGE BODY/i))
{
 $object_type="PACKAGE";
 $package_count = () = $str =~ /$object_type/g;
}
elsif ($str =~ /CREATE .*PROCEDURE/i)
{
 $object_type="PROCEDURE";
 $procedure_count = () = $str =~ /$object_type/g;
}
elsif ($str =~ /CREATE .*FUNCTION/i)
{
 $object_type="FUNCTION";
 $function_count = () = $str =~ /$object_type/g;
}
elsif ($str =~ /CREATE .*TRIGGER/i)
{
 $object_type="TRIGGER";
 $trigger_count = () = $str =~ /$object_type/g;
}
elsif ($str =~ /CREATE .*VIEW/i)
{
 $object_type="VIEW";
 $view_count = () = $str =~ /$object_type/g;
}
$string = $str;
$string =~ /$object_type\s*?(\S+)/;
$next_word = $1;
($owner,$object_name)=split(/\./,$next_word);
print "$object_type  $owner  $object_name\n";
exit 0;
