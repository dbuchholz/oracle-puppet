-- run purge_report_list.sql first and give result to Team Install contact and Jim List
-- For Portal Reports:
delete from pace_masterdbo.xml_package_log where report_instance in (select instance from pace_masterdbo.xml_reports_log where app_indicator =3 and upd_datetime < (sysdate-90) );
delete from pace_masterdbo.xml_reports_log_detail where instance in (select instance from pace_masterdbo.xml_reports_log where app_indicator =3 and upd_datetime < (sysdate-90));
delete from pace_masterdbo.xml_report_info where pinstance in (select instance from pace_masterdbo.xml_reports_log where app_indicator =3 and upd_datetime < (sysdate-90));
delete from pace_masterdbo.xml_reports_log where app_indicator =3 and upd_datetime < (sysdate-90);
-- For OLAP Reports:
delete from pace_masterdbo.xml_package_log where report_instance in (select instance from pace_masterdbo.xml_reports_log where app_indicator !=3 and upd_datetime < (sysdate-90) );
delete from pace_masterdbo.xml_reports_log_detail where instance in (select instance from pace_masterdbo.xml_reports_log where app_indicator! =3 and upd_datetime < (sysdate-90));
delete from pace_masterdbo.xml_report_info where pinstance in (select instance from pace_masterdbo.xml_reports_log where app_indicator !=3 and upd_datetime < (sysdate-90));
delete from pace_masterdbo.xml_reports_log where app_indicator !=3 and upd_datetime < (sysdate-90);
