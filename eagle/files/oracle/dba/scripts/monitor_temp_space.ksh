#!/bin/ksh
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 2 ]
then
 print "Must enter the Oracle SID and the temporary tablespace name."
 print "Usage: $0 [Oracle SID] [Temporary Tablespace Name]"
 exit 1
fi

export ORACLE_SID=$1

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi


tbspace_name=$2
tablespace_name=$(print $tbspace_name | tr '[a-z]' '[A-Z]')
print "Temp space usage for $ORACLE_SID Database, Tablespace Name: $tbspace_name at $(date)"
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set lines 200 pages 9999 trimspool on feedback off
col tablespace format a30
select tablespace,sql_id,blocks*8192/1024/1024 "Temp Usage(MB)",username,contents,segtype from v\$sort_usage where tablespace='$tablespace_name' order by "Temp Usage(MB)",SQL_ID;
col "Total Temp Usage(GB)" format 9999.9
select sum(blocks*8192)/1024/1024/1024 "Total Temp Usage(GB)" from v\$sort_usage where tablespace='$tablespace_name';
select sum(blocks*8192/1024/1024) "Temp Usage(MB)",SQL_ID from v\$sort_usage where tablespace='$tablespace_name' group by SQL_ID order by "Temp Usage(MB)" desc;
set lines 200
col machine format a25
col username format a20
col osuser format a10
col tablespace format a20
col sid format 99999
col serial# format 9999999
SELECT   b.tablespace, (b.blocks/1024/1024)*blocksize "Used in MB", a.sid, a.serial#, a.username, a.osuser, a.status,a.machine,a.module,a.sql_id,a.prev_sql_id
FROM     v\$session a,v\$sort_usage b,sys.ts\$ ts
WHERE    a.saddr = b.session_addr and ts.name =b.tablespace and b.tablespace='$tablespace_name'
ORDER BY 2 desc;  
col file_name format a65
col "Size(GB)" format 99.9
col "Max Size(GB)" format 99.9
select file_name,bytes/1024/1024/1024 "Size(GB)",maxbytes/1024/1024/1024 "Max Size(GB)" from dba_temp_files where tablespace_name='$tablespace_name';
EOF
exit 0
