#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set pages 500 lines 200 feedback off echo off trimspool on
col d.sid format a10
col SERVER format a12 
col c.name format a40
col ENVIRONMENT format a12
col c.code format a4
spool db_client_info.txt
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*PROD*',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*PROD*',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*DEV *',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*DEV *',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*TEST *',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*TEST *',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*DR *',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*DR *',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*TRAIN*',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*TRAIN*',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*UAT *',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*UAT *',1,1,'i')) is not null
union
select d.sid,m.name SERVER,
c.name CLIENT,
upper(regexp_substr(d.environment,'*STAGE *',1,1,'i')) ENVIRONMENT,
c.code
from inf_monitor.databases d,inf_monitor.clients c,inf_monitor.machines m
where c.instance=d.client 
and d.mac_instance=m.instance
and upper(regexp_substr(d.environment,'*STAGE *',1,1,'i')) is not null;
EOF
head -3 db_client_info.txt > hold_header.txt
sed '1,3d' db_client_info.txt | sort -k 1.69,1.71 > client_info.txt
cat hold_header.txt client_info.txt > db_client_info.txt
rm -f hold_header.txt client_info.txt
egrep -v '^....................................................................[a-zA-Z]|^$|\-\-\-\-' db_client_info.txt > missing_client_code.txt
egrep '^....................................................................[a-zA-Z]|^$|\-\-\-\-' db_client_info.txt > not_missing_client_code.txt
cat not_missing_client_code.txt missing_client_code.txt > db_client_info.txt
rm -f not_missing_client_code.txt missing_client_code.txt

exit 0
