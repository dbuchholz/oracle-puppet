#!/bin/ksh
# set_sid.ksh
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi
sid_list=`egrep -v '(^#|^$)' $ORATAB | awk -F: '{print $1}'`
for sid_name in $sid_list
do
 print $sid_name
done
print "Enter an Oracle SID to export: \c"
read sid
export ORACLE_SID=$sid
ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
