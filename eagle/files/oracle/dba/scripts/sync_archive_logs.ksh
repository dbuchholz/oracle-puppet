#!/bin/ksh
###
# Usage: sync_archive_logs.ksh < Source Server >  < Source Arch Directory >  < Target Server >  < Target Arch Directory >
#
# This script will synchronize a directory on a source server with the local server
# The source server is where the files are located.
# The local server is this server.
# It is required to set up your ssh keys for passwordless connection to source_server and target_server.
# Put your id_rsa.pub key from hawaii in the both the source_server and target_server /u01/app/oracle/.ssh/authorized_keys file.
# Put the source_server and target server /u01/app/oracle/.ssh/id_rsa.pub contents into your authorized_keys file.
# Test using ssh to verify connectivity to source_server and target_server without prompting for password.
# Source and Target Arch Directory are full path to archive logs such as the value of log_archive_dest_1
# After you have completed the syncing and the intermediate_arch directory on hawaii is no longer needed, please remove.
# Example:
#  sync_archive_logs.ksh oracle@ppld0007 /igiprd1-04/oradata/arch oracle@epld0303 /amlprd1-04/oradata/arch
#
this_pid=$$

source_server=$1
source_arch=$2
target_server=$3
target_arch=$4

# Directory on this server to hold archive logs as an intermediary between source and target servers
arch_combined=$(print "${source_arch}${target_arch}" | sed -e 's|/|_|g' -e 's/oradata//g' -e 's/arch//g' -e 's/archive//g')
intermediate_basename="${source_server}_${target_server}_$arch_combined"
intermediate=$(print $intermediate_basename | sed -e 's/_//g' -e 's/-//g')
intermediate_arch="/u01/app/oracle/arch_stage/$intermediate"
if [[ ! -d $intermediate_arch ]]
then
 print "Make directory: $intermediate_arch"
 mkdir $intermediate_arch
 chmod 775 $intermediate_arch
fi

((process_count=$(ps -ef | grep -c "sync_archive_logs\.ksh $source_server $source_arch $target_server $target_arch")))
if [ $process_count -gt 2 ]
then
 print "Currently syncing $intermediate_arch archive logs.  Aborting script."
 exit 1
fi

rsync -goptr -e ssh --delete --rsync-path=/usr/bin/rsync $source_server:$source_arch/ $intermediate_arch/

rsync -goptr -e ssh --delete --rsync-path=/usr/bin/rsync $intermediate_arch/ $target_server:$target_arch/

exit 0
