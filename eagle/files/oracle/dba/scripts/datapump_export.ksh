#!/bin/ksh

#######################################################################################
#
# Script  : datapump_export.ksh <ORACLE_SID> [P:n] [C]
# Purpose : To export data from an Oracle database using expdp utility.
#
#######################################################################################

function set_parallelism
{
   indop=$(print $1 | awk -F\: '{print $2}')
   if [ -z $indop ]
   then
      DOP=$(mpstat | grep -v '^CPU' | wc -l)
   else
      DOP=$indop
   fi
   print "Setting the degree of parallelism for datapump export to ${DOP}."
   FILENAME=export_${ORACLE_SID}_${TODAY}_%U.dmpdp
   return
}

# Main script starts from here...

if [ $# -lt 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <ORACLE_SID> [P:n] [C]\n${BOFF}"
   print "\n\t\tThe 2nd and 3rd arguments are optional."
   print "\t\tYou can specify them in any order."
   print "\n\t\tP => Parallalize datapump export. Default: No Parallel"
   print "\t\tn => Degree of parallelism.  Default: total number of CPUs\n"
   print "\t\tC => Compress the dump files after the export.  Default: No compress"
   exit 1
fi

# Verify that the first parameter is a valid Oracle SID defined in /var/opt/oracle/oratab
sid_in_oratab=$(grep -v "^#" /var/opt/oracle/oratab | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "Cannot find Instance $1 in /var/opt/oracle/oratab"
   exit 2
fi
 
# Export variable declarations

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
LOGDIR=$SCRIPT_DIR

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export PATH=$PATH:${ORACLE_HOME}/bin
export mailrecipients=team_dba@eagleaccess.com
export mailflag=FALSE
export TODAY=$(date "+%Y%d%m")

export DOP=1
typeset -i DOP
export COMPERSS=""
export FILENAME=export_${ORACLE_SID}_${TODAY}.dmpdp
export FILESIZE=30G   # Set FILESIZE=integer[B | K | M | G]
export LOGFILE=export_${ORACLE_SID}_${TODAY}.log

# Process optional input arguments
for i in $2 $3
do
  typeset -u i
  case $i in
     P*) set_parallelism $i ;;
     C)  export COMPRESS=Y ;;
     *) print "Invalid Arguments" ;;
  esac
done

# Start database if it is down at the start of the export
((db_down=0))
ps -ef | grep "ora_[a-z0-9]\{4\}_${ORACLE_SID}" > /dev/null
if [ $? -ne 0 ]
then
   ((db_down=1))
   print "$ORACLE_SID Database is down"
   print "Starting the $ORACLE_SID database to begin export"
   $ORACLE_HOME/bin/sqlplus / as sysdba<<EOF
startup
EOF
fi

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
   WHENEVER SQLERROR EXIT FAILURE ;
   set echo off feed off pages 0 trim on trims on
   spool $LOGDIR/nls_lang_val.lst
   SELECT lang.value || '_' || terr.value || '.' || chrset.value
   FROM   v\$nls_parameters lang,
          v\$nls_parameters terr,
          v\$nls_parameters chrset
   WHERE  lang.parameter = 'NLS_LANGUAGE'
   AND    terr.parameter = 'NLS_TERRITORY'
   AND    chrset.parameter = 'NLS_CHARACTERSET'  ;
   spool off
EOF
if [ $? -ne 0 ]
then
   print "ERROR: When getting NLS Parameter from database."
else
   export NLS_LANG=$(cat $LOGDIR/nls_lang_val.lst)
fi

print "NLS_LANG is $NLS_LANG"

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF > $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
set head off feed off lines 150 trims on pages 0 sqlp ""
select directory_path from dba_directories where directory_name='DATA_PUMP_DIR';
EOF

datapump_dump_directory=$(sed '/^$/d' $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt)
rm -f $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
if [[ ! -d $datapump_dump_directory ]]
then
   mkdir -p $datapump_dump_directory
   chmod 755 $datapump_dump_directory
   if [ $? -ne 0 ]
   then
      print "Cannot create $datapump_dump_directory on this host."
      print "This is most likely due to the top level directory does not exist."
      print "You need to create a directory object in the database to a directory that exists."
      exit 3
   fi
fi

\rm -f $datapump_dump_directory/export_${ORACLE_SID}_${TODAY}*.dmpdp*

$ORACLE_HOME/bin/expdp / \
     DUMPFILE=${FILENAME} \
     FILESIZE=${FILESIZE} \
     LOGFILE=${LOGFILE} \
     FULL=y \
     JOB_NAME=EXPDP_${TODAY} \
     PARALLEL=$DOP

print "Moving logfile $LOGFILE to ${LOGDIR}..."
mv $datapump_dump_directory/$LOGFILE $LOGDIR
if [ $? -ne 0 ]
then
   print "Failed to move the logfile $LOGFILE to destination ${LOGDIR}!"
fi

grep "^Job .*EXPDP_${TODAY}.* successfully completed" $LOGDIR/$LOGFILE > /dev/null
if [ $? -ne 0 ]
then
   if [ $mailflag = "TRUE" ]
   then
      mailx -s "$(hostname) : Datapump export did not complete successfully for ${ORACLE_SID}. $(date)" $mailrecipients <<-EOF
Script  : $0
Message : Datapump export did not complete successfully for ${ORACLE_SID}.
Action  : Check the logfile $LOGDIR/$LOGFILE for more information.

* * * * *
NOTE : To properly clean up a failed datapump expdp job, follow the steps below:

1) Login to "oracle" on $(hostname)
2) Set the environment for ${ORACLE_SID}
3) Attach to the failed expdp job

	${ORACLE_HOME}/bin/expdp / ATTACH=EXPDP_${TODAY}

4) Once you are inside the client, issue KILL_JOB to clean up the job

	Export> KILL_JOB
	Are you sure you wish to stop this job ([y]/n):y

5) Restart the job
	EOF
   else
      print "Datapump export did not complete successfully for ${ORACLE_SID}!"
      print "Please check the logfile $LOGDIR/$LOGFILE for more information."
   fi
   exit 3
else
   print "Datapump export of $ORACLE_SID completed succesfully."
fi

if [ $db_down -eq 1 ]
then
   print "Shutting down $ORACLE_SID database as it was down before export began at $(date)"
   $ORACLE_HOME/bin/sqlplus / as sysdba<<EOF
shutdown immediate
EOF
   print "Shutting down $ORACLE_SID database completed at $(date)"
fi

if [ "$COMPRESS" = "Y" ]
then
   print "Compressing the datapump export dumps..."
   compress -f $datapump_dump_directory/export_${ORACLE_SID}_${TODAY}*.dmpdp
   if [ $? -ne 0 ]
   then
      print "Failed to compress the dump files in ${datapump_dump_directory}!"
   fi
fi

ls -ltr $datapump_dump_directory

exit 0
