#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

typeset -i no_response
typeset -i count
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines order by ip_address;
EOF

rm -f list_kernel_info.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   break
  fi
  sleep 1
 done
 if [ $no_response -eq 0 ]
 then
  ssh -nq $ip_address /u01/app/oracle/local/dba/scripts/check_kernel_parameters_linux.ksh >> list_kernel_info.txt
 fi
done<machine_info.txt

exit 0
