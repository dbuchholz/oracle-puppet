#!/usr/bin/perl
use Time::Local;
my $timeData = localtime(time);
$time = time();
my $sec=$ARGV[0];
my $min=$ARGV[1];
my $hours=$ARGV[2];
my $day=$ARGV[3];
my $month=$ARGV[4];
my $year=$ARGV[5];
my $testtime = timelocal($sec,$min,$hours,$day,$month,$year); 
my $timediff=($time - $testtime)/60;
printf "Since last archive log applied in minutes is: %3.1f\n",$timediff;
exit 0;
