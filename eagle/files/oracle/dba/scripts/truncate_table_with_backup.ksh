#!/bin/ksh
# ==================================================================================================
# NAME:		truncate_table_with_backup.ksh
#
# USAGE:	truncate_table_with_backup.ksh <ORACLE_SID> <Comma Separated List of Tables Fully Qualified>
# 
# AUTHOR:	Frank Davis
#
# PURPOSE:	This script will export an input list of tables for a given Oracle SID.
#			Oracle Datapump is used for the export.  The export dump file is placed in /datadomain/export/{Oracle SID}
#
#
# Frank Davis  06/10/2013  Initial Development
# ==================================================================================================
if [ $# -lt 2 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!"
 print "\t\tUsage : $0 <ORACLE_SID> <Table List>\n${BOFF}"
 exit 1
fi

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
typeset -x SCRIPT_DIR
export LOGDIR=$SCRIPT_DIR

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 (( CPUS=$(grep processor /proc/cpuinfo | wc -l) ))
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 (( CPUS=$(/usr/bin/mpstat | grep -v '^CPU' | wc -l) ))
fi

export ORACLE_SID=$1
# Verify that the first parameter is a valid Oracle SID defined in oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "Cannot find Instance $ORACLE_SID in $ORATAB"
 exit 2
fi

TABLE_LIST=$2
((periods=$(print $TABLE_LIST | tr -d -c . |wc -c)))
if [ $? -ne 0 ]
then
 print "The number of periods in the table list cannot be found."
 print "Fully qualified table names have periods separating the owner from the table name."
 exit 3
fi
((commas=$(print $TABLE_LIST | tr -d -c , |wc -c)))
if [ $? -ne 0 ]
then
 if [ periods -ne 1 ]
 then
  print "The number of commas in the table list cannot be found."
  print "If more than one table in list, they must have commas separating the fully qualified table names."
  exit 4
 fi
fi
((difference=periods - commas))
if [ $difference -ne 1 ]
then
 print "Table list is not the correct format."
 print "Must use fully qualified table names: owner.table_name"
 exit 5
fi
 
print $TABLE_LIST | sed 's/\,/=/g' | tr "=" "\n" > $LOGDIR/build_truncate_${ORACLE_SID}_list.txt
sed -e 's/^/truncate table /g' -e 's/$/;/g' $LOGDIR/build_truncate_${ORACLE_SID}_list.txt > $LOGDIR/built_truncate_${ORACLE_SID}_list.txt
while read LINE
do
 ((periods=$(print $LINE | tr -d -c . | wc -c)))
 if [ $periods -ne 1 ]
 then
  print "Table name must be fully qualified with:  owner.table_name"
  print "This is what was entered:  $LINE"
  exit 6
 fi
 print $LINE | grep '^\.' > /dev/null
 if [ $? -eq 0 ]
 then
  print "A period was found in the first position, which means there is no table owner specified."
  print "Here is the entry: $LINE"
  exit 7
 fi
 print $LINE | grep '\.$' > /dev/null
 if [ $? -eq 0 ]
 then
  print "A period was found in the last position, which means there is no table name specified."
  print "Here is the entry: $LINE"
  exit 8
 fi
done<$LOGDIR/build_truncate_${ORACLE_SID}_list.txt
rm -f $LOGDIR/build_truncate_${ORACLE_SID}_list.txt

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

now=$(date +%Y_%m%d_%H%M%S)

export PAR_HOME=$HOME/local/dba
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')

if [[ -f $PARFILE ]]
then
 STATUS=$(sed '/^#/d' $PARFILE  > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
 fi
fi

export FILENAME=export_${ORACLE_SID}_${now}_%U

export FILESIZE=20G   # Set FILESIZE=integer[B | K | M | G]

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
   WHENEVER SQLERROR EXIT FAILURE ;
   set echo off feed off pages 0 trim on trims on
   spool $LOGDIR/${ORACLE_SID}_nls_lang_val.lst
   SELECT lang.value || '_' || terr.value || '.' || chrset.value
   FROM   v\$nls_parameters lang,
          v\$nls_parameters terr,
          v\$nls_parameters chrset
   WHERE  lang.parameter = 'NLS_LANGUAGE'
   AND    terr.parameter = 'NLS_TERRITORY'
   AND    chrset.parameter = 'NLS_CHARACTERSET'  ;
   spool off
EOF
if [ $? -ne 0 ]
then
 print "ERROR: When getting NLS Parameter from database."
else
 export NLS_LANG=$(cat $LOGDIR/${ORACLE_SID}_nls_lang_val.lst)
fi

rm -f $LOGDIR/${ORACLE_SID}_nls_lang_val.lst

datapump_dump_directory=/datadomain/export/$ORACLE_SID
mkdir -p $datapump_dump_directory
if [ $? -ne 0 ]
then
 print "Cannot create directory $datapump_dump_directory on this host."
 exit 9
fi
chmod 755 $datapump_dump_directory
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set head off feed off lines 150 trims on pages 0 sqlp ""
drop directory DATA_PUMP_DIR;
WHENEVER SQLERROR EXIT FAILURE
create directory DATA_PUMP_DIR as '$datapump_dump_directory';
EOF
if [ $? -ne 0 ]
then
 print "Cannot create DATA_PUMP_DIR on $ORACLE_SID as $datapump_dump_directory"
 exit 10
fi

((DOP=$(print "scale=0; $CPUS / 4" | bc)))
if [ $DOP -lt 1 ]
then
 DOP=1
fi
if [ $DOP -gt 4 ]
then
 DOP=4
fi

FILENAME=export_${ORACLE_SID}_${now}_%U.dmpdp
LOGFILE=export_${ORACLE_SID}_${now}.log
$ORACLE_HOME/bin/expdp \"/ as sysdba\" DUMPFILE=$FILENAME FILESIZE=$FILESIZE LOGFILE=$LOGFILE JOB_NAME=EXPDP_$now PARALLEL=$DOP TABLES=$TABLE_LIST
grep " successfully completed at " $datapump_dump_directory/$LOGFILE
if [ $? -ne 0 ]
then
 print "Export did not complete successfully."
 print "The truncate tables will not be run."
 print "Aborting here..."
 print "See Logfile $LOGFILE"
 exit 11
fi

ls -ltr $datapump_dump_directory
print $datapump_dump_directory
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
WHENEVER SQLERROR EXIT FAILURE
spool $LOGDIR/truncate_${ORACLE_SID}_tables.log
@$LOGDIR/built_truncate_${ORACLE_SID}_list.txt
EOF
if [ $? -ne 0 ]
then
 print "There was an error in the table truncation section."
 print "The script will be aborted out at the point of failure."
 exit 12
fi
rm -f $LOGDIR/built_truncate_${ORACLE_SID}_list.txt

exit 0
