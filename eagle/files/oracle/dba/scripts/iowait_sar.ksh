#!/bin/ksh
start_time="00:00:00"
end_time="02:30:00"
#file_name=/var/adm/sarcoll/log.1101/log.110110
file_name=/var/adm/sarcoll/log.1012/log.101231
mount_points=$(df -Ph |awk '{print $NF}' | egrep -v '\/datadomain$|\/datadomain2$|\/$|\/boot$|\/dev\/shm$|\/var$|\/tmp$|on$')
df -Ph |grep '^\/dev\/' | awk '{print $1,$NF}' > df_info.txt
rm -f sar_report.txt
for a_mount_point in $mount_points
do
 rm -f device_details.txt
 while read device mp
 do
  if [ $mp = /u01 ]
  then
   device=$(print $device | sed 's/.$//g')
  fi
  major_minor_numbers=$(ls -l $device | awk '{print $5,$6}' | sed 's/,//g')
  print "$mp $device $major_minor_numbers" >> device_details.txt
 done<df_info.txt
 major_no=$(grep "$a_mount_point" device_details.txt | awk '{print $3}')
 minor_no=$(grep "$a_mount_point" device_details.txt | awk '{print $NF}')
 sar -d -f $file_name -s $start_time -e $end_time > sar.out
 head -3 sar.out | tail -1 >> sar_report.txt
 print "*** $a_mount_point ***" >> sar_report.txt
 grep " dev${major_no}-${minor_no} " sar.out >> sar_report.txt
 avg_await=$(tail -2 sar_report.txt | grep 'Average: ' | awk '{print $8}')
 avg_svctm=$(tail -2 sar_report.txt | grep 'Average: ' | awk '{print $9}')
 wait_pct=$(print "scale=2; ($avg_await - $avg_svctm) / $avg_await * 100" | bc | awk -F. '{print $1}')
 if [ $wait_pct -gt 50 ]
 then
  print "Average Service Time(svctm) was less than 50% of total wait(await) time." >> sar_report.txt
 fi
 print " " >> sar_report.txt
done

exit 0
