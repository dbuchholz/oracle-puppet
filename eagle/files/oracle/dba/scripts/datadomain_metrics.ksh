#!/bin/ksh

now=$(date "+%Y%m%d_%H%M%S")
if [ $(dirname $0) = "." ] 
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir/datadomain_logs

typeset -Z2 hour
find /datadomain/prod -name "*" -type f -mtime -31 -exec ls -l {} \; > $log_dir/datadomain_directory_listing_${now}.txt
find /datadomain/imp -name "*" -type f -mtime -31 -exec ls -l {} \; >> $log_dir/datadomain_directory_listing_${now}.txt

# DataDomain Bytes Written by Hour
BYTES_BY_HOUR=$log_dir/datadomain_bytes_by_hour_${now}.txt
print "Hour  GB\n" > $BYTES_BY_HOUR
(( hour = 0 ))
while [ $hour -le 23 ]
do
 bytes=$(grep " ${hour}:[0-9][0-9] " $log_dir/datadomain_directory_listing_${now}.txt | awk '{sum+=$5} END {printf "%6d\n",sum/1000000000}')
 if [[ -z $bytes ]]
 then
  bytes=0
 fi
 print "$hour $bytes" >> $BYTES_BY_HOUR
 (( hour += 1 ))
done

awk '{print $6,$7}' $log_dir/datadomain_directory_listing_${now}.txt | \
sort -u | \
sed 's/ \([0-9]\)$/ 0\1/g' | \
sort -M | \
sed 's/^\([A-Z][a-z]\{2\} \)0\([0-9]\)/\1 \2/g' > $log_dir/dates_in_datadomain_in_past_month.txt


# DataDomain Bytes Written by Date
BYTES_BY_DATE=$log_dir/datadomain_bytes_by_date_${now}.txt
print "Date  GB\n" > $BYTES_BY_DATE
old_IFS="$IFS"
IFS=
while read -r the_date
do
 bytes=$(grep "$the_date" $log_dir/datadomain_directory_listing_${now}.txt | awk '{sum+=$5} END {printf "%6d\n",sum/1000000000}')
 if [[ -z $bytes ]]
 then
  bytes=0
 fi
 one_field_date=$(print "$the_date" | sed 's/ /_/g') 
 print "$one_field_date $bytes" >> $BYTES_BY_DATE
done<$log_dir/dates_in_datadomain_in_past_month.txt
IFS=$old_IFS

rm -f $log_dir/dates_in_datadomain_in_past_month.txt
find $log_dir -name "datadomain_directory_listing_*.txt" -mtime +31 -exec rm -f {} \;

exit 0
