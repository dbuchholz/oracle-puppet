#!/bin/ksh
# Script name: export_full.ksh
# Run this script as oracle UNIX user with SYSDBA Privilege
#
export ORACLE_SID=$1
backup_dir=$2

if [ $(dirname $0) = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ $sid_in_oratab != $ORACLE_SID ]
then
 print "Cannot find Instance $ORACLE_SID in $ORATAB"
 exit 1
fi

umask 137
export today=$(date "+%Y%d%m")

# Verify that there are at least two parameters passed into this script
if [ $# -lt 2 ]
then
 print "Usage:"
 print "$(basename $0) [Oracle SID] [backup directory]"
 exit 2
fi
#
# Verify that backup directory is a valid directory or it is writable by process
if [ ! -d $backup_dir ] && [ ! -w $backup_dir ]
then
 print "${ORACLE_SID}: $backup_dir is not a directory or $(whoami) does not have write permission in directory"
 exit 3
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [[ ! -d $script_dir ]]
then
 mkdir -p $script_dir
fi

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set echo off feed off pages 0 trim on trims on
 spool $script_dir/nls_lang_val.lst
 SELECT lang.value || '_' || terr.value || '.' || chrset.value
 FROM   v\$nls_parameters lang,
        v\$nls_parameters terr,
        v\$nls_parameters chrset
 WHERE  lang.parameter = 'NLS_LANGUAGE'
 AND    terr.parameter = 'NLS_TERRITORY'
 AND    chrset.parameter = 'NLS_CHARACTERSET';
 spool off
EOF
if [ $? -ne 0 ]
then
 export NLS_LANG=$(cat $script_dir/nls_lang_val.lst)
fi

$ORACLE_HOME/bin/exp \"/ as sysdba\" file=$backup_dir/export_${ORACLE_SID}_${today}.dmp direct=Y consistent=Y log=$script_dir/export_normal_${ORACLE_SID}_$today.log full=y
grep "Export terminated successfully without warnings\." $script_dir/export_normal_${ORACLE_SID}_$today.log
if [ $? -ne 0 ]
then
 print "Export may have a problem check log file $script_dir/export_normal_${ORACLE_SID}_$today.log"
 exit 1
fi
print "${0}: Export of $ORACLE_SID finished successfully at $(date)"

ls -ltr $backup_dir
exit 0
