#!/bin/ksh
capacity=85
script_dir=/u01/app/oracle/local/dba
read source_machine?"Enter host of where the source database runs: "
read source_sid?"What is the name of the source Oracle SID: "
read target_machine?"Enter host of where the target database runs: "
read target_sid?"What is the name of the target Oracle SID: "

the_source_code=$(print $source_sid | cut -c1-3 | tr '[a-z]' '[A-Z]')
the_target_code=$(print $target_sid | cut -c1-3 | tr '[a-z]' '[A-Z]')

print "\n\nSource Database $source_sid on $source_machine Storage:"
ssh -nq $source_machine df -lPh | grep $source_sid | sort -nk6 | tee $script_dir/${target_sid}_${target_machine}_df_output.txt

print "Source Database $source_sid Size:"
ssh -nq $source_machine ls $script_dir/size_of_database.ksh > /dev/null 2>&1
if [ $? -ne 0 ]
then
 scp -q $script_dir/size_of_database.ksh ${source_machine}:$script_dir/size_of_database.ksh
fi
ssh -nq $source_machine $script_dir/size_of_database.ksh $source_sid | tee $script_dir/scripts/storage_sizing.txt

if [[ -n $target_machine ]]
then
 if [[ -z $target_sid ]]
 then
  exit 0
 fi
 ssh -nq $target_machine ps -ef | grep ora_smon_${target_sid} | grep -v grep > /dev/null
 if [ $? -eq 0 ]
 then
  print "\n\nTarget $target_sid on $target_machine Storage:"
  ssh -nq $target_machine df -lPh | grep $target_sid
  allocated_size=$(ssh -nq $target_machine df -PB G /${target_sid}* | grep -v Filesystem | sort -nk6 | sed '$d' | awk '{print $2}' | sed 's/G//g' | awk '{sum+=$1} END {printf "%6d\n",sum}')
 fi
fi

if [[ -n $target_sid ]]
then
 ssh -nq $target_machine ps -ef | grep ora_smon_$target_sid | grep -v grep > /dev/null
 if [ $? -eq 0 ]
 then
  ssh -nq $target_machine ls $script_dir/size_of_database.ksh > /dev/null 2>&1
  if [ $? -ne 0 ]
  then
   scp -q $script_dir/size_of_database.ksh ${target_machine}:$script_dir/size_of_database.ksh
  fi
  print "Target Database $target_sid Size:"
  ssh -nq $target_machine $script_dir/size_of_database.ksh $target_sid
 fi
fi

today=$(date "+%m/%d/%Y")
print "\n\nRefresh $the_target_code($target_sid on $target_machine) from $the_source_code($source_sid on $source_machine) $today"

awk '{print $NF,$2}' $script_dir/${target_sid}_${target_machine}_df_output.txt | sed "s/$source_sid/$target_sid/g"
rm -f $script_dir/${target_sid}_${target_machine}_df_output.txt
ssh -nq $target_machine ps -ef | grep ora_smon_${target_sid} | grep -v grep > /dev/null
if [ $? -eq 0 ]
then
 size_of_db=$(cat $script_dir/scripts/storage_sizing.txt | awk '{print $2}')
 used_pct=$(print "scale=2; 100 - ($allocated_size - $size_of_db)/$allocated_size * 100" | bc | awk -F. '{print $1}')
 print "\nSize of Source Database(GB)     =    $size_of_db"
 print "Allocated Size on Target(GB)    = $allocated_size"
 print "Percent Used on Target would be =    $used_pct\n"
 if [ $used_pct -ge $capacity ]
 then
  print "**** Refresh would exceed ${capacity}% used in filesystems.  Expand the LUNs. ****"
 else
  print "$target_sid: ${used_pct}% capacity in target storage.  No LUN expansions required for now."
 fi
fi

rm -f $script_dir/scripts/storage_sizing.txt

export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

source_version=`$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set heading off feedback off
select pace_version from databases where sid='$source_sid' and dataguard='N';
EOF`
source_version=$(print $source_version|tr -d " ")

target_version=`$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set heading off feedback off
select pace_version from databases where sid='$target_sid' and dataguard='N';
EOF`
target_version=$(print $target_version|tr -d " ")

if [[ -n $source_version ]] && [[ -n $target_version ]]
then
 if [ $source_version != $target_version ]
 then
  printf "  \n\n\x1b[5m***** WARNING *****\x1b[25m\nSource and Target Database PACE versions do not match.\n"
  print "Source $source_sid = $source_version"
  print "Target $target_sid = $target_version"
  exit 1
 fi
fi

exit 0
