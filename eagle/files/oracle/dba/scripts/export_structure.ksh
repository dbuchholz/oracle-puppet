#!/bin/ksh
# Script name: export_structure.ksh
# Usage: export_structure.ksh [ Oracle SID ]

function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

function funct_check_db_code
{
 pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
 pcode=$(print $pcode|tr -d " ")

 tnsname_prod=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='N' and status='OPEN';
EOF`
 tnsname_prod=$(print $tnsname_prod|tr -d " ")
}

# MAIN ***************************************************************************************

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
puser=SYSTEM
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 exit 3
fi

funct_check_db_code

print "Connect to infprd1."
syspwd=`$ORACLE_HOME/bin/sqlplus -s $CRON_USER/${CODE}@${CRON_SID}<<EOF
set pages 0
WHENEVER SQLERROR EXIT FAILURE
select  nvl(inf_monitor.bb_get(temp_code),inf_monitor.bb_get(code))
from inf_monitor.user_codes
where username='$puser' and 
db_instance=(select instance from databases where sid='$ORACLE_SID' and dataguard='N');
EOF`
if [ $? -ne 0 ]
then
 print "Error in process."
 exit 4
fi

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE 
 set echo off feed off pages 0 trim on trims on
 spool $script_dir/nls_lang_val.lst
 SELECT lang.value || '_' || terr.value || '.' || chrset.value
 FROM   v\$nls_parameters lang,
        v\$nls_parameters terr,
        v\$nls_parameters chrset
 WHERE  lang.parameter = 'NLS_LANGUAGE'
 AND    terr.parameter = 'NLS_TERRITORY'
 AND    chrset.parameter = 'NLS_CHARACTERSET';
 spool off
EOF
if [ $? -ne 0 ]
then
 export NLS_LANG=$(cat $script_dir/nls_lang_val.lst)
fi
rm -f $script_dir/nls_lang_val.lst

now=$(date +'%Y%m%d_%H%M%S')
$ORACLE_HOME/bin/exp $puser/$syspwd full=y rows=n direct=y file=$script_dir/export_structure_${ORACLE_SID}_${now}.dmp log=$script_dir/export_structure_${ORACLE_SID}_${now}.log

exit 0
