#!/bin/ksh
# Usage: save_source_code.ksh <Oracle SID> <Full Path of SQL Script>
if [ $# -ne 2 ]
then
 print "\nMust enter one parameter into this script."
 print "The parameter must be the Oracle SID and the Full Path to SQL script name that contains the code to run.\n"
 print "$0 <Oracle SID> <Full Path of SQL Script>"
 exit 1
fi
if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
log_dir=/datadomain/export/$ORACLE_SID
mkdir -p $log_dir

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

script_name=$2
if [ ! -f $script_name ]
then
 print "\nFile $script_name does not exist.\n"
 exit 2
fi
now=$(date +'%Y%m%d_%H%M%S')
egrep -v '^--|^.* --' $script_name > $log_dir/hold_script_${ORACLE_SID}_no_comments.sql
output=$($script_dir/save_source_code.pl $log_dir/hold_script_${ORACLE_SID}_no_comments.sql)
the_status=$?
rm -f $log_dir/hold_script_${ORACLE_SID}_no_comments.txt $log_dir/hold_script_${ORACLE_SID}_no_comments.sql
if [ $the_status -eq 2 ]
then
 print "Object need to be fully qualified with OWNER.OBJECT"
 print "Exiting here..."
 exit 3
fi
object_first_two=$(print $output | awk '{print $1,$2}' | tr '[a-z]' '[A-Z]')
if [ "$object_first_two" = "PACKAGE BODY" ]
then
 object_type=$(print $output | awk '{print $1,$2}' | tr '[a-z]' '[A-Z]')
 object_owner=$(print $output | awk '{print $3}' | tr '[a-z]' '[A-Z]')
else
 object_type=$(print $output | awk '{print $1}' | tr '[a-z]' '[A-Z]')
 object_owner=$(print $output | awk '{print $2}' | tr '[a-z]' '[A-Z]')
fi
object_name=$(print $output | awk '{print $NF}' | tr '[a-z]' '[A-Z]')
object_type_no_spaces=$(print $object_type | sed 's/ /_/g')

filename=$log_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_${now}.sql
sql_statement="select text from dba_source where type='$object_type' and owner='$object_owner' and name='$object_name';"

case $object_type in
'PACKAGE BODY'|PACKAGE|PROCEDURE|FUNCTION|TRIGGER)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
WHENEVER SQLERROR EXIT FAILURE
set pagesize 0 feedback off trimspool on linesize 200 echo off
spool $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt
$sql_statement
EOF
if [ $? -ne 0 ]
then
 print "There was an error when extracting $object_type source code for ${object_owner}.${object_name}."
 print "Aborting here..."
 exit 1
fi
sed '/^$/d' $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt > $filename
rm -f $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt
(( file_size=$(ls -l $filename | awk '{print $5}') ))
if [ $file_size -eq 0 ]
then
 print "The object ${object_owner}.${object_name} of type $object_type does not exist in the database.  There is nothing to save."
 rm -f $filename 
 exit 0
fi
;;

VIEW)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
WHENEVER SQLERROR EXIT FAILURE
set long 300000 linesize 2499 pagesize 0
col DDL format a2499
spool $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt
select dbms_metadata.get_ddl('VIEW','$object_name','$object_owner') DDL from dual;
EOF
if [ $? -ne 0 ]
then
 grep 'ORA-31603' $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt > /dev/null
 if [ $? -eq 0 ]
 then
  print "The object ${object_owner}.${object_name} of type $object_type does not exist in the database.  There is nothing to save."
  rm -f $filename $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt
  exit 0
 else
  print "There was an error when extracting $object_type source code for ${object_owner}.${object_name}."
  print "Aborting here..."
  exit 1
 fi
fi
sed '/^$/d' $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt | sed '$s/$/;/g' > $filename
rm -f $script_dir/source_code_${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}.txt
(( file_size=$(ls -l $filename | awk '{print $5}') ))
if [ $file_size -eq 0 ]
then
 print "The object ${object_owner}.${object_name} of type $object_type does not exist in the database.  There is nothing to save."
 rm -f $filename
 exit 0
fi
;;
*) print "Not a valid object type.";;
esac

print "Backup of $object_type $object_owner $object_name is located in:"
print $log_dir
ls -l $filename
if [ "$object_type" = 'VIEW' ]
then
 exit 0
fi

first_word=$(head -1 $filename | awk '{print $1}')
second_word=$(head -1 $filename | awk '{print $2}')
third_word=$(head -1 $filename | awk '{print $3}')
if [[ -n $third_word ]]
then
 fully_qualified="$first_word $second_word ${object_owner}.$third_word AS"
else
 fully_qualified="$first_word ${object_owner}.$second_word"
fi
sed '1d' $filename > $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp.txt
sed "1i $fully_qualified" $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp.txt > $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp2.txt
sed '1i CREATE OR REPLACE' $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp2.txt > $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp.txt

rm -f $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp2.txt
mv $log_dir/${ORACLE_SID}_${object_type_no_spaces}_${object_owner}_${object_name}_source_code_temp.txt $filename
exit 0
