#!/bin/ksh
# Script name:  check_monthly_or_yearly_backup.ksh
if [ $(dirname $0) = "." ] 
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir/logs
#mailrecipients=team_dba@eagleaccess.com
mailrecipients=fdavis@eagleaccess.com

export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

month=`date +%m`
if [ $month = '01' ]
then
 YEAR_END=1
else
 YEAR_END=0
fi

if [ $YEAR_END -eq 0 ]
then
 backup_area=month_end
else
 backup_area=year_end
fi

backup_base=/datadomain2/$backup_area/*/prod?

find $backup_base -mtime -10 -name "*_dbf" | awk -F/ '{print $6}' | sort -u > $log_dir/recent_backups.txt

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/all_dirs.txt
set pages 0 trimspool on feedback off echo off
select d.sid from inf_monitor.databases d,inf_monitor.machines m
where d.mac_instance=m.instance and m.location in ('EVERETT','PITTSBURGH')
and d.dataguard='N'
order by d.sid;
EOF

diff $log_dir/recent_backups.txt $log_dir/all_dirs.txt | grep '>' | sed -e 's/>//g' -e 's/ //g' > $log_dir/not_recent_backups.txt
rm -f $log_dir/recent_backups.txt $log_dir/all_dirs.txt $log_dir/backup_results.txt $log_dir/monthly_backup_status.txt
for a_dir in $(cat $log_dir/not_recent_backups.txt)
do
 ls -1 $backup_base/$a_dir/*_dbf > /dev/null 2>&1
 if [ $? -eq 0 ]
 then
  date_of_last_backup=$(ls -ltr $backup_base/$a_dir/*_dbf | tail -1 | awk '{print $6,$7}')
  print "No recent monthly backup for ${a_dir}.  Most recent backup occurred on: $date_of_last_backup" >> $log_dir/backup_results.txt
 else
  print "No monthly backups for ${a_dir}." >> $log_dir/backup_results.txt
 fi 
done
if [[ -f $log_dir/backup_results.txt ]]
then
 sort $log_dir/backup_results.txt > $log_dir/monthly_backup_status.txt
 grep 'No recent monthly backup for ' $log_dir/monthly_backup_status.txt | awk '{print $6}' | awk -F. '{print $1}' > $log_dir/check_for_current_database.txt
fi
rm -f $log_dir/backup_results.txt $log_dir/not_recent_backups.txt

if [[ -f $log_dir/check_for_current_database.txt ]]
then
for a_db in $(cat $log_dir/check_for_current_database.txt)
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/does_db_exist.txt
set pages 0 trimspool on feedback off echo off
select sid from inf_monitor.databases where sid='$a_db';
EOF
grep $a_db $log_dir/does_db_exist.txt > /dev/null
if [ $? -eq 0 ]
then
 sed "/$a_sid\./s/$/ current database \*/g" $log_dir/monthly_backup_status.txt > $log_dir/temp_monthly_backup_status.txt
 mv $log_dir/temp_monthly_backup_status.txt $log_dir/monthly_backup_status.txt
else
 sed "/$a_sid\./s/$/ not a current database/g" $log_dir/monthly_backup_status.txt > $log_dir/temp_monthly_backup_status.txt
 mv $log_dir/temp_monthly_backup_status.txt $log_dir/monthly_backup_status.txt
fi
done
fi
rm -f $log_dir/does_db_exist.txt $log_dir/check_for_current_database.txt
if [[ -f $log_dir/monthly_backup_status.txt ]]
then
 print "Problem with monthly copy of databases to /datadomain2"
mailx -s "Problem with monthly copy of databases to /datadomain2" $mailrecipients<<EOF
 $(cat $log_dir/monthly_backup_status.txt)
EOF
 exit 1
fi
exit 0
