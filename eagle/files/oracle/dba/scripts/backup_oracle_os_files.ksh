#!/bin/ksh
# ==================================================================================================
# NAME:		backup_oracle_os_files.ksh                               
# 
# AUTHOR:	Jayant Pawar
#
# PURPOSE:	Backs up Oracle OS files to Datadomain.
#			The files can be used during a failover or they are a backup that can be used
#           when a file is changed and requires a older version of the file.
#
# USAGE:	backup_oracle_os_files.ksh
#
# Frank Davis   05/16/2015  Made compatible with ASM Managed Databases
# Frank Davis   11/28/2014  Made compatible with RHEL 6.5
# Frank Davis   09/30/2013  Removed team_dba@eagleaccess.com from email
# Frank Davis   09/24/2013  Removed tech_support@eagleaccess.com from email
# Frank Davis   06/13/2013  Changed Impact to 2 for Prod and Test
# Frank Davis   06/05/2013  Changed Impact and Urgency to 3 for Prod and Test
# Frank Davis   06/04/2013  Added Error Checking
# Frank Davis   06/03/2013  Made script compatible with Linux and Solaris.
# Jayant Pawar  06/01/2013  Initial Development
# ==================================================================================================
# Reports back to Big Brother MACHINE_CRON_JOB_RUNS Table.
# Will keep daily directories of the most recent 32 days of Oracle OS file.
# The location where the backup files will go is of the format:
# /datadomain/pgh|evt/Server_Name/Timestamp
# 
# Will back up server configuration files to DataDomain
# oratab
# crontab
# For each Oracle Home:
#  tnsnames.ora
#  listener.ora
#  sqlnet.ora
#  spfile for each database
#  pwfile for each database
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification
{
 print "$PROGRAM_NAME \n     Machine: $BOX " > $WORKING_DIR/mail_${PROGRAM_NAME}.dat
 print "\nCategory: $CATEGORY" >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
 print "Subcategory: $SUBCATEGORY" >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
 print "Impact: $IMPACT" >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
 print "Urgency: $URGENCY\n" >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat

 if [[ -n $1 ]]
 then
  print "\n$1\n" >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
 fi

 if [[ -f $ERROR_FILE ]]
 then
  cat $ERROR_FILE >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
  rm -f $ERROR_FILE
 fi

 if [[ -n $2 ]]
 then
  if [[ $2 = FATAL ]]
  then
   print "*** This is a FATAL ERROR - $PROGRAM_NAME aborted at this point *** " >> $WORKING_DIR/mail_${PROGRAM_NAME}.dat
  fi
 fi

 cat $WORKING_DIR/mail_${PROGRAM_NAME}.dat | mailx -s "PROBLEM WITH $TYPE Environment -- $PROGRAM_NAME on $BOX" $MAILTO
 rm -f $WORKING_DIR/mail_${PROGRAM_NAME}.dat

 # Update the mail counter
 MAIL_COUNT=${MAIL_COUNT}+1

 return 0
}
# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {
        PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
        set heading off feedback off
        select 1 from dual;
EOF
`
    PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
    if [ $PERFORM_CRON_STATUS -ne 1 ]
    then
     PERFORM_CRON_STATUS=0
    fi
}
# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update
{
 ## ---- Fetching the mac_instance for the BoxName from machines table ------
 Mac_Instance=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from MACHINES where name='${BOX}';
EOF
`
 Mac_Instance=$(print $Mac_Instance|tr -d " ")
 ## ---------------------------------------------------------------------------
 Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 select instance from MACHINE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
 start_time > to_char(sysdate,'DD-MON-YYYY') and mac_instance = ${Mac_Instance};
EOF
`
 Sequence_Number=$(print $Sequence_Number|tr -d " ")
 if [[ -z $Sequence_Number ]]
 then
  Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set serveroutput on size 1000000
  set heading off feedback off
  declare i number;
  begin
   insert into MACHINE_CRON_JOB_RUNS values (machine_cron_job_runs_seq.nextval, '${PROGRAM_NAME}', sysdate, '','',0,'','0',${Mac_Instance}) returning instance into i;
   commit;
   dbms_output.put_line (i);
  end;
/
EOF
`
 else
  STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
  set heading off feedback off
  update MACHINE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
  commit;
EOF
`
 fi
 export Sequence_Number
}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update
{
 # Update DBA Auditing Database with end time and backup size
 STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off feedback off
 update MACHINE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
 commit;
 update MACHINE_CRON_JOB_RUNS set run_time= (select
 trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
 trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
 trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
 from machine_cron_job_runs where instance=${Sequence_Number})
 where instance=${Sequence_Number};
 commit;
/
EOF
`
 print "$PROGRAM_NAME Completed $(date +\"%c\") with a status of $PROCESS_STATUS"
}

# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

# *****************************
#  MAIN
# *****************************
#
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export DATADOMAIN_PATH=/datadomain/oracle_os_files
export BOX=$(print $(hostname) | awk -F. '{print $1}')
export NumericDate=$(date +%Y%m%d)
export DB_INSTANCE
export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number
export PERFORM_CRON_STATUS=0

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv $ORACLE_SID > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/scripts
else
 export WORKING_DIR=/u01/app/oracle/local/dba/scripts
fi

export ERROR_FILE=$WORKING_DIR/ErrorFile_$BOX

if [ $ASM_CLIENT = Y ]
then
 export PAR_HOME=/usr/local/scripts/dba
else
 export PAR_HOME=$HOME/local/dba
fi

export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export CATEGORY=DATABASE
export SUBCATEGORY=ORACLE

rm -f $ERROR_FILE

IPCheck=$(grep $BOX /etc/hosts | awk '{print $1}' | egrep '10\.60\.5|10\.60\.3|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}')
if [[ -n $IPCheck ]]
then
 export TYPE=PROD
 export IMPACT=2
 export URGENCY=3
else
 export TYPE=TEST
 export IMPACT=2
 export URGENCY=3
fi
export MAILTO='eaglecsi@eagleaccess.com'

if [[ -f $PARFILE ]]
then
 STATUS=$(sed '/^#/d' $PARFILE > $NO_COMMENT_PARFILE)
 LINE=$(grep "INFRASTRUCTURE_DATABASE:" $NO_COMMENT_PARFILE)
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$($PAR_HOME/GetDec.cmd $CRON_CONNECT)
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 SendNotification "Cannot reach $CRON_SID Big Brother Database." "FATAL"
 exit 3
fi

# The first number of the second octet of the IP Address represents the data center Everett or Pittsburgh.
# For example:  ###.X##.###.###    When X=7, it is Pittsburgh.  When X=6, it is Everett.
this_host=$(hostname)
((location_number=$(grep $this_host /etc/hosts | awk '{print $1}' | awk -F. '{print $2}' | cut -c1)))
case $location_number in
6) datacenter=evt;;
7) datacenter=pgh;;
*) print "Not a valid data center.  Needs to be Everett or Pittsburgh."
   SendNotification "$datacenter not a valid data center.  Needs to be Everett or Pittsburgh." "FATAL"
   exit 1;;
esac

# Check if CRONLOG directory exists
if [ $ASM_CLIENT = Y ]
then
 if [[ ! -d /home/oracle/CRONLOG ]]
 then
  mkdir -p /home/oracle/CRONLOG
 fi
else
 if [[ ! -d $HOME/CRONLOG ]]
 then
  mkdir -p $HOME/CRONLOG
 fi
fi

# Add entry into DBA Admin database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 funct_initial_audit_update
 if [ $? -ne 0 ]
 then
  PERFORM_CRON_STATUS=0
 fi
 export DB_INSTANCE
fi

export Sequence_Number

export BACKUP_LOC=$DATADOMAIN_PATH/$datacenter/$BOX/$NumericDate
export DATABASES_FILE=$BACKUP_LOC/list_databases.txt
export ORACLE_HOME_PATH=$BACKUP_LOC/list_oracle_home_path.txt

mkdir -p $BACKUP_LOC

# Backup of crontab
/usr/bin/crontab -l > $BACKUP_LOC/crontab.txt
if [ $? -ne 0 ]
then
 print "Could not copy crontab listing" >> $ERROR_FILE
fi

# Backup of oratab 
cat $ORATAB > $BACKUP_LOC/oratab
if [ $? -ne 0 ]
then
 print "Could not copy $ORATAB" >> $ERROR_FILE
fi

# Backup listener and tnsnames files
if [ $ASM_CLIENT = Y ]
then
 grep '^\+ASM:' $ORATAB | awk -F: '{print $2}' | sort -u > $ORACLE_HOME_PATH
else
 egrep -v '^#|^$' $ORATAB | awk -F: '{print $2}' | sort -u > $ORACLE_HOME_PATH
fi
while read THE_HOME
do
 ORA_VER=$(print $THE_HOME | awk -F/ '{print $6}')
 BACKUP_LOC_VER=$BACKUP_LOC/$ORA_VER
 mkdir -p $BACKUP_LOC_VER
 if [[ -f $THE_HOME/network/admin/listener.ora ]]
 then
  cp $THE_HOME/network/admin/listener.ora $BACKUP_LOC_VER/listener.ora
  if [ $? -ne 0 ]
  then
   print "Could not copy $THE_HOME/network/admin/listener.ora" >> $ERROR_FILE
  fi
 fi
 if [[ -f $THE_HOME/network/admin/tnsnames.ora ]]
 then
  cp $THE_HOME/network/admin/tnsnames.ora $BACKUP_LOC_VER/tnsnames.ora
  if [ $? -ne 0 ]
  then
   print "Could not copy $THE_HOME/network/admin/tnsnames.ora" >> $ERROR_FILE
  fi
 fi
 if [[ -f $THE_HOME/network/admin/sqlnet.ora ]]
 then
  cp $THE_HOME/network/admin/sqlnet.ora $BACKUP_LOC_VER/sqlnet.ora
  if [ $? -ne 0 ]
  then
   print "Could not copy $THE_HOME/network/admin/sqlnet.ora" >> $ERROR_FILE
  fi
 fi
done<$ORACLE_HOME_PATH
rm -f $ORACLE_HOME_PATH

egrep -v '^#|^$|^\+ASM:' $ORATAB | awk -F: '{print $1}'> $DATABASES_FILE

while read ORA_SID
do
 dbs_path=$(grep -w $ORA_SID $ORATAB | awk -F: '{print $2}')
 dbs_path_ver=$(print $dbs_path | awk -F/ '{print $6}')
 export dbs_bak_loc=$BACKUP_LOC/$dbs_path_ver
 if [ $ASM_CLIENT = Y ]
 then
  $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
  WHENEVER SQLERROR EXIT FAILURE
  create spfile='$dbs_bak_loc/spfile${ORA_SID}.ora' from pfile;
EOF
  if [ $? -ne 0 ]
  then
   print "Could not copy spfile${ORA_SID}.ora to backup area" >> $ERROR_FILE
  fi
 else
  if [[ -f $dbs_path/dbs/spfile${ORA_SID}.ora ]]
  then
   cp $dbs_path/dbs/spfile${ORA_SID}.ora $dbs_bak_loc/spfile${ORA_SID}.ora
   if [ $? -ne 0 ]
   then
    print "Could not copy $dbs_path/dbs/spfile${ORA_SID}.ora to backup area $dbs_bak_loc" >> $ERROR_FILE
   fi
  fi
 fi

 if [[ -f $dbs_path/dbs/orapw$ORA_SID ]]
 then
  cp $dbs_path/dbs/orapw$ORA_SID $dbs_bak_loc/orapw$ORA_SID
  if [ $? -ne 0 ]
  then
   print "Could not copy $dbs_path/dbs/orapw$ORA_SID to backup area $dbs_bak_loc" >> $ERROR_FILE
  fi
 fi
done<$DATABASES_FILE
rm -f $DATABASES_FILE
find $DATADOMAIN_PATH/$datacenter/$BOX -depth -type d -name "*" -mtime +31 -exec rm -rf {} \;

if [[ -f $ERROR_FILE ]]
then
 SendNotification "Error found in $ERROR_FILE"
fi

# Update Infrastructure database
if [ $PERFORM_CRON_STATUS -eq 1 ]
then
 if [ $MAIL_COUNT -gt 0 ]
 then
  PROCESS_STATUS=WARNING
 else
  PROCESS_STATUS=SUCCESS
 fi
 funct_final_audit_update
fi

exit 0
