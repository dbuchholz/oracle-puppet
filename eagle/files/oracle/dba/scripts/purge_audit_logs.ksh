#!/bin/ksh
# ==================================================================================================
# NAME:         purge_audit_logs.ksh
#
# AUTHOR:       Frank Davis
#
# PURPOSE:      This script will purge Oracle audit logs and keep the most recent logs.
#
# USAGE:        purge_audit_logs.ksh <Number of days to keep --- Optional>
#
# Frank Davis  05/16/2015  Made compatible with ASM Managed Databases
# ==================================================================================================
# ---------------------------------------------------
# funct_check_asm_client (): Is this ASM managed
# ---------------------------------------------------
function funct_check_asm_client
{
export ASM_CLIENT=`${ORACLE_HOME}/bin/sqlplus -s '/ as sysdba' <<EOF
set heading off feedback off
select distinct 'Y' from v\\$asm_client where upper(DB_NAME)=upper('$ORACLE_SID');
EOF
`
}

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$(grep '\/u01\/' $ORATAB | egrep -v '^#|^\+ASM:' | awk -F: '{print $1}' | tail -1)
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

funct_check_asm_client
ASM_CLIENT=$(print "$ASM_CLIENT" | tail -1)
if [[ -n $ASM_CLIENT ]]
then
 if [ $ASM_CLIENT = Y ]
 then
  ASM_CLIENT=Y
 fi
else
 ASM_CLIENT=N
fi

if [ $ASM_CLIENT = Y ]
then
 export WORKING_DIR=/usr/local/scripts/dba/scripts
else
 export WORKING_DIR="$HOME/local/dba/scripts"
fi

sids=$(ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')

for a_sid in $sids
do
 export ORACLE_SID=$a_sid
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH
 . /usr/local/bin/oraenv > /dev/null

 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 set feedback off trimspool on pagesize 0
 spool $WORKING_DIR/audit_file_dest_loction_${ORACLE_SID}.txt
 select value from v\$parameter where name='audit_file_dest';
EOF
 sed '/^$/d' $WORKING_DIR/audit_file_dest_loction_${ORACLE_SID}.txt > $WORKING_DIR/audit_file_dest_location_${ORACLE_SID}.txt
 the_destination=$(cat $WORKING_DIR/audit_file_dest_location_${ORACLE_SID}.txt)
 rm -f $WORKING_DIR/audit_file_dest_loction_${ORACLE_SID}.txt $WORKING_DIR/audit_file_dest_location_${ORACLE_SID}.txt
 find $the_destination -type f -name "*.aud" -mtime +32 -print -exec rm -f {} \;
done

exit 0
