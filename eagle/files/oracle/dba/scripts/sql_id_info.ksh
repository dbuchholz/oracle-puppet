#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

read sql_id?"Enter SQL ID: "
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF> sql_id_${sql_id}.txt
set lines 200 pages 999
SELECT eaglemgr.eagle_util.sql_id_info_line(LEVEL) FROM dual CONNECT BY LEVEL <= (SELECT MAX(eaglemgr.eagle_util.sql_id_info('$sql_id')) from dual);
col NAME format a12
col value_string format a30
col VALUE_ANYDATA format a30
select SQL_ID,NAME,VALUE_STRING,anydata.AccessTimestamp(VALUE_ANYDATA) VALUE_ANYDATA from DBA_HIST_SQLBIND where sql_id='$sql_id' order by snap_id,position;
EOF

exit 0
