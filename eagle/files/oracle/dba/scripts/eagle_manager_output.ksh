#!/bin/ksh
# Script name: eagle_manager_output.ksh
# Usage: eagle_manager_output.ksh <Oracle SID> <SQL ID>

if [ $# -ne 2 ]
then
 print "Must enter the Oracle SID and the SQL ID into this script."
 print "Usage: $0 <Oracle SID> <SQL ID>"
 exit 1
fi

if [ $(dirname $0) = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

export sql_id=$2
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/eagle_manager_${sql_id}.txt
set lines 200 pages 999
SELECT eaglemgr.eagle_util.sql_id_info_line(LEVEL) FROM dual CONNECT BY LEVEL <= (SELECT MAX(eaglemgr.eagle_util.sql_id_info('$sql_id')) from dual);
col NAME format a12
col value_string format a30
col VALUE_ANYDATA format a30
select SQL_ID,NAME,VALUE_STRING,anydata.AccessTimestamp(VALUE_ANYDATA) VALUE_ANYDATA from DBA_HIST_SQLBIND where sql_id='$sql_id' order by snap_id,position;
EOF

exit 0
