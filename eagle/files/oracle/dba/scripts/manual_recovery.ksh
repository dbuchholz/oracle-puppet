#!/bin/ksh
#
#     manual_recovery.ksh
#
#     script to recover database to eliminate gap sequence
#
#     Assumptions:
#	  Logs will be pushed to standby node (as soon as ready)
#	  recovery will be up to the last possible log ...
#
###############################################################################
mailrecipients=team_dba@eagleaccess.com
function usage {
print "Error: Usage: manual_recovery.ksh <ORACLE_SID>"
}
 
function apply_log
{
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set autorecovery on
recover database using backup controlfile until cancel
EOF
}

function check_for_unnamed_datafiles
{
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/looking_for_unnamed_${ORACLE_SID}.txt
WHENEVER SQLERROR EXIT FAILURE
select name from v\$datafile where name like '%UNNAMED%';
EOF
if [ $? -ne 0 ]
then
 return 1
fi
grep 'no rows selected' $script_dir/looking_for_unnamed_${ORACLE_SID}.txt > /dev/null
if [ $? -ne 0 ]
then
 rm -f $script_dir/looking_for_unnamed_${ORACLE_SID}.txt
 return 2
fi
rm -f $script_dir/looking_for_unnamed_${ORACLE_SID}.txt
return 0
}

function check_for_recover_offline_datafiles
{
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/looking_for_offline_${ORACLE_SID}.txt
WHENEVER SQLERROR EXIT FAILURE
select file#,name,status from v\$datafile where status in('RECOVER','OFFLINE');
EOF
if [ $? -ne 0 ]
then
 return 1
fi
grep 'no rows selected' $script_dir/looking_for_offline_${ORACLE_SID}.txt > /dev/null
if [ $? -ne 0 ]
then
 rm -f $script_dir/looking_for_offline_${ORACLE_SID}.txt
 return 2
fi
rm -f $script_dir/looking_for_offline_${ORACLE_SID}.txt
return 0
}
 
#########################
# MAIN
#########################
if [ "$(dirname $0)" = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
 
if [ $# -ne 1 ]
then
 usage
 exit 1
else
 the_sid=$1
 export ORACLE_SID=$the_sid
 print "Using ORACLE_SID=$ORACLE_SID"
 export ORAENV_ASK=NO
 export PATH=/usr/local/bin:$PATH
 . /usr/local/bin/oraenv > /dev/null
fi

proc=$(ps -ef | grep "ora_[a-z]*_${the_sid}" | wc -l)
if [ $proc -eq 0 ]
then
 print "The Oracle instance ${the_sid} is NOT running"
 print "Please run $script_dir/startup_standby.ksh to start the instance"
 exit 2
fi
 
print $(date)
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/${ORACLE_SID}_background_dump_dest.txt
set pages 0 trimspool on
select value from v\$parameter where name='background_dump_dest';
EOF
sed '/^$/d' $script_dir/${ORACLE_SID}_background_dump_dest.txt > $script_dir/${ORACLE_SID}_background_dump_dest.txtxxx
mv $script_dir/${ORACLE_SID}_background_dump_dest.txtxxx $script_dir/${ORACLE_SID}_background_dump_dest.txt
bdump_dest=$(cat $script_dir/${ORACLE_SID}_background_dump_dest.txt)
alert_log=$bdump_dest/alert_${ORACLE_SID}.log
rm -f $script_dir/${ORACLE_SID}_background_dump_dest.txt
grep -i ' offline drop' $alert_log
if [ $? -eq 0 ]
then
 print "There was a datafile that was offlined and dropped in the Manual DR ${ORACLE_SID}."
 print "This must be resolved before any recovery can take place on database."
 print "Once you correct the problem, move alert log to a new name"
 print "before running this script again so the same error is not found again."
mailx -s "$(hostname): Offline datafile dropped in Manual DR $ORACLE_SID" $mailrecipients<<EOF
There was a datafile that was offlined and dropped in the Manual DR ${ORACLE_SID}.
This must be resolved before any recovery can take place on database.
Once you correct the problem, move alert log to a new name
before running this script again so the same error is not found again.
EOF
 exit 3
fi

grep 'Some recovered datafiles maybe left media fuzzy' $alert_log
if [ $? -eq 0 ]
then
 print "It is possible that Block Change Tracking is Enabled in Manual DR ${ORACLE_SID}."
 print "This must be resolved before any recovery can take place on database."
 print "Once you correct the problem, move alert log to a new name"
 print "before running this script again so the same error is not found again."
mailx -s "$(hostname): It is possible that Block Change Tracking is Enabled in Manual DR $ORACLE_SID" $mailrecipients<<EOF
This must be resolved before any recovery can take place on database.
Once you correct the problem, move alert log to a new name
before running this script again so the same error is not found again.
If Block Change Tracking is Enabled, issue:  ALTER DATABASE DISABLE BLOCK CHANGE TRACKING;
It will be required to recreate the Manual DR controlfile using the Source Control File.
On source database, issue:  ALTER DATABASE BACKUP CONTROLFILE TO TRACE RESETLOGS;
Run the text control on target.
EOF
 exit 4
fi

apply_log

check_for_recover_offline_datafiles
the_status=$?
if [ $the_status -ne 0 ]
then
 if [ $the_status -eq 1 ]
 then
  print "There was an error when connecting to $ORACLE_SID Database"
  print "in order to look for datafiles in offline or recovery status, the database needs to be queried."
  print "Check must succeed in order to continue."
mailx -s "$(hostname): Cannot connect to Manual DR $ORACLE_SID" $mailrecipients<<EOF
There was an error when connecting to $ORACLE_SID Database
in order to look for datafiles in offline or recovery status.
Check must succeed in order to continue.
Log into host identified in Subject of this email and determine why a database
connection to the Manual DR is not working.
EOF
 fi
 if [ $the_status -eq 2 ]
 then
  print "There was a datafile in offline or recover status."
mailx -s "$(hostname): Datafile in recover or offline status in $ORACLE_SID" $mailrecipients<<EOF
EOF
 fi
 exit 5
fi

check_for_unnamed_datafiles
the_status=$?
if [ $the_status -ne 0 ]
then
 if [ $the_status -eq 1 ]
 then
  print "There was an error when connecting to $ORACLE_SID Database"
  print "in order to look for datafile with UNNAMED in its name."
  print "Check must succeed in order to continue."
mailx -s "$(hostname): Cannot connect to Manual DR $ORACLE_SID" $mailrecipients<<EOF
There was an error when connecting to $ORACLE_SID Database
in order to look for datafile with UNNAMED in its name.
Check must succeed in order to continue.
Log into host identified in Subject of this email and determine why a database
connection to the Manual DR is not working.
EOF
 fi
 if [ $the_status -eq 2 ]
 then
  print "There was a datafile with the name including UNNAMED."
  print "This means a datafile was added to the source database."
  print "The datafile needs to be manually added to the target database."
  print "See TFS Document: new_datafile_added_during_manual_dr.docx"
mailx -s "$(hostname): Datafile added to Source database and not in Manual DR $ORACLE_SID" $mailrecipients<<EOF
There was a datafile with the name including UNNAMED.
This means a datafile was added to the source database.
The datafile needs to be manually added to the target database.
See TFS Document: new_datafile_added_during_manual_dr.docx
EOF
 fi
 exit 6
fi

exit 0
