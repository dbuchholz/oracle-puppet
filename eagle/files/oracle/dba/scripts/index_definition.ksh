#!/bin/ksh
# Script name: query_plan_hist.ksh
# Usage: query_plan_hist.ksh [ Oracle SID ]

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
read owner?"Enter Owner of Index: "
owner=$(print $owner | tr '[a-z]' '[A-Z]')
read ind_name?"Enter Index Name: "
ind_name=$(print $ind_name | tr '[a-z]' '[A-Z]')


$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set lines 200 feedback off echo off trimspool on pages 0
spool index.txt
select dbms_metadata.get_ddl('INDEX','$ind_name','$owner') from dual;
EOF
sed 's/"//g' index.txt > index1.txt
mv index1.txt index.txt
exit 0
