#!/bin/ksh
#######################################################################################
#
# Script  : datapump_export_schema.ksh <ORACLE_SID> [Parallelism]
# Purpose : To export data from an Oracle database using expdp utility.
#
#######################################################################################

function set_parallelism
{
   indop=$(print $1 | awk -F\: '{print $2}')
   if [ -z $indop ]
   then
      DOP=$(mpstat | grep -v '^CPU' | wc -l)
   else
      DOP=$indop
   fi
   print "Setting the degree of parallelism for datapump export to ${DOP}."
   FILENAME=export_${ORACLE_SID}_${TODAY}_%U.dmpdp
   return
}

# -------------------------------------------------------
# funct_build_parfile():   Build export parameter file
# -------------------------------------------------------
funct_build_parfile() {
	# Uncomment next line for debugging
	# set -x

        user_list=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
                set heading off
                set feedback off
                set pagesize 500
                select username from dba_users
                where (username like '%DBO' or
                      username = 'ESTAR' or
                      username = 'PRICING' or
                      username = 'CTRLCTR' or
                      username = 'ARCH_CTRLCTR' or
                      username = 'EAGLEKB' or
                      username = 'EAGLEMGR' or
                      username = 'ISTAR' or
                      username = 'DATAEXCH') and
                      username != 'PACE_MASTERDBO';
                exit
EOF`
print $user_list | sed -e 's/ /,/g' -e 's/^/SCHEMAS=/g' -e s/$/,PACE_MASTERDBO/g > $EXPORTPARFILE
}

# Main script starts from here...

if [ $# -lt 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <ORACLE_SID> [Parallelism]\n${BOFF}"
   print "\n\t\tThe second argument is optional."
   print "\n\t\t => Parallalize datapump export. Default: No Parallel"
   exit 1
fi

# Verify that the first parameter is a valid Oracle SID defined in /etc/oratab
sid_in_oratab=$(grep -v "^#" /etc/oratab | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "Cannot find Instance $1 in /etc/oratab"
   exit 2
fi
 
# Export variable declarations

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
typeset -x SCRIPT_DIR
export LOGDIR=$SCRIPT_DIR

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
export mailrecipients=fdavis@eagleaccess.com
export mailflag=FALSE
export TODAY=$(date "+%Y%d%m")
now=$(date +%Y_%m%d_%H%M)

DOP=$2
if [[ -z $DOP ]]
then
 DOP=1
fi

typeset -i DOP

export PAR_HOME=$HOME/local/dba
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F "." '{print $1}')

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
 fi
fi

puser=SYSTEM
pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
pcode=$(print $pcode|tr -d " ")

export FILENAME=export_${ORACLE_SID}_${TODAY}_%U
#export FILESIZE=1G   # Set FILESIZE=integer[B | K | M | G]
export FILESIZE=20G   # Set FILESIZE=integer[B | K | M | G]
export LOGFILE=export_${ORACLE_SID}_${now}.log

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
   WHENEVER SQLERROR EXIT FAILURE ;
   set echo off feed off pages 0 trim on trims on
   spool $LOGDIR/nls_lang_val.lst
   SELECT lang.value || '_' || terr.value || '.' || chrset.value
   FROM   v\$nls_parameters lang,
          v\$nls_parameters terr,
          v\$nls_parameters chrset
   WHERE  lang.parameter = 'NLS_LANGUAGE'
   AND    terr.parameter = 'NLS_TERRITORY'
   AND    chrset.parameter = 'NLS_CHARACTERSET'  ;
   spool off
EOF
if [ $? -ne 0 ]
then
   print "ERROR: When getting NLS Parameter from database."
else
   export NLS_LANG=$(cat $LOGDIR/nls_lang_val.lst)
fi

print "NLS_LANG is $NLS_LANG"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
set head off feed off lines 150 trims on pages 0 sqlp ""
select directory_path from dba_directories where directory_name='DATA_PUMP_DIR';
EOF

datapump_dump_directory=$(sed '/^$/d' $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt)
rm -f $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
if [[ ! -d $datapump_dump_directory ]]
then
   mkdir -p $datapump_dump_directory
   chmod 755 $datapump_dump_directory
   if [ $? -ne 0 ]
   then
      print "Cannot create $datapump_dump_directory on this host."
      print "This is most likely due to the top level directory does not exist."
      print "You need to create a directory object in the database to a directory that exists."
      exit 3
   fi
fi

EXPORTPARFILE=$LOGDIR/datapump_par_file_${ORACLE_SID}.par
funct_build_parfile
$ORACLE_HOME/bin/expdp ${puser}/${pcode} DUMPFILE=${FILENAME} FILESIZE=${FILESIZE} LOGFILE=${LOGFILE} PARFILE=$EXPORTPARFILE JOB_NAME=EXPDP_${TODAY} PARALLEL=$DOP

ls -ltr $datapump_dump_directory

exit 0
