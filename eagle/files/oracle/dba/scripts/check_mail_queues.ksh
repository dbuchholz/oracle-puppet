#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>all_machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines order by name;
EOF
while read host_name ip
do
 ssh -nq $ip "mailx -i;hostname" >> outfile.txt
done<all_machine_info.txt
exit 0
