#!/bin/ksh
# Script Name: is_listener_running_everett.ksh
mailrecipients="fdavis@eagleaccess.com,dambekar@eagleinvsys.com"

# Run this script every five minutes from cron.
# Runs the Production check every run or every five minutes
# Runs the sub-Production checks every third check or every 15 minutes.

# How many runs of this script before a list of machines are determined from Big Brother
when_to_query=144           # If running job every five minutes, 144 would be 12 hours to get machine list.
# Machine list does not change often, no sense in getting machine list with every five minute run.
print "Start: $(date)"

function check_listener
{
host_file=$1
while read host_name the_ip
do
 ssh -nq $the_ip $script_dir/is_listener_running.ksh > /dev/null 2>&1
 the_exit_status=$?
 if [ $the_exit_status -ne 0 ]
 then
if [ $the_exit_status -eq 255 ]
then
mailx -s  "$(hostname): $0 $(date): ssh failure to $host_name" $mailrecipients<<EOF
Attempting to "lsnrctl status" could not reach remote server.
This is not an indication that there is a listener problem on ${host_name}.
This is usually due to a transient network problem.
EOF
fi
else
mailx -s  "$(hostname): $0 $(date): Oracle listener problem on $host_name" $mailrecipients<<EOF
A "lsnrctl status" on $host_name did not return successful result.
Log into $host_name and run: lsnrctl status
If this hangs, "kill -9" on PID of the listener process and then issue:  lsnrctl start
EOF
fi
 fi
done<$host_file
}

# MAIN

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ -s $log_dir/run_count_query.txt ]
then
 (( query_count=$(cat $log_dir/run_count_query.txt) ))
 (( query_count=query_count+1 ))
 print $query_count > $log_dir/run_count_query.txt
else
 print $when_to_query > $log_dir/run_count_query.txt
 (( query_count=$when_to_query ))
fi

(( machine_file_count=$(ls -1 $log_dir/everett_*servers.txt | wc -l) ))

if [ $query_count -eq $when_to_query ] || [ $machine_file_count -ne 3 ]
then 
(( query_count=0 ))
print $query_count > $log_dir/run_count_query.txt
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pages 0 trimspool on feedback off echo off

spool $log_dir/everett_prod1_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT' and m.instance<(select median(instance) from inf_monitor.machines where location='EVERETT')
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT' and m.instance<(select median(instance) from inf_monitor.machines where location='EVERETT');

spool $log_dir/everett_prod2_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT' and m.instance>=(select median(instance) from inf_monitor.machines where location='EVERETT')
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT' and m.instance>=(select median(instance) from inf_monitor.machines where location='EVERETT');

spool $log_dir/everett_imp_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT IMP'
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT IMP';
EOF

# Get rid of any blank lines in files
sed '/^$/d' $log_dir/everett_prod1_servers.txt > $log_dir/everett_prod1_serversx.txt
mv $log_dir/everett_prod1_serversx.txt $log_dir/everett_prod1_servers.txt
sed '/^$/d' $log_dir/everett_prod2_servers.txt > $log_dir/everett_prod2_serversx.txt
mv $log_dir/everett_prod2_serversx.txt $log_dir/everett_prod2_servers.txt
sed '/^$/d' $log_dir/everett_imp_servers.txt > $log_dir/everett_imp_serversx.txt
mv $log_dir/everett_imp_serversx.txt $log_dir/everett_imp_servers.txt
fi

# Run every time for PROD
check_listener $log_dir/everett_prod1_servers.txt &
check_listener $log_dir/everett_prod2_servers.txt &

if [ -s $log_dir/run_count_listener.txt ]
then
 (( run_count=$(cat $log_dir/run_count_listener.txt) ))
 (( run_count=run_count+1 ))
 print $run_count > $log_dir/run_count_listener.txt
else
 print 0 > $log_dir/run_count_listener.txt
 (( run_count=0 ))
fi

# Skip running in IMP this many runs.  So run every third time.
if [ $run_count -eq 2 ]
then 
 check_listener $log_dir/everett_imp_servers.txt &
 rm -f $log_dir/run_count_listener.txt
fi
wait
print "End: $(date)"
exit 0
