#!/bin/ksh
MAILTO=team_dba@eagleaccess.com
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where OS_TYPE<>'SunOS' order by name;
EOF

rm -f $log_dir/timezone_info.txt
while read name ip_address
do
 print $name >> $log_dir/timezone_info.txt
 ssh -nq $ip_address ls -al /etc/localtime >> $log_dir/timezone_info.txt
 print "\n" >> $log_dir/timezone_info.txt
done<machine_info.txt

exit 0
