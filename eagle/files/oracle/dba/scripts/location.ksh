#!/bin/ksh
this_host=$(hostname)
((location_number=$(grep $this_host /etc/hosts | awk '{print $1}' | awk -F. '{print $2}' | cut -c1)))
case $location_number in
6) print "Everett";;
7) print "Pittsburgh";;
*) print "Not a valid location.  Needs to be Everett or Pittsburgh";;
esac
exit 0
