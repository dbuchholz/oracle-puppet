#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

typeset -i no_response
typeset -i count
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines order by ip_address;
EOF

rm -f db_server_info.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   break
  fi
  sleep 1
 done
 if [ $no_response -eq 0 ]
 then
  ssh -nq $ip_address ps -ef | grep ora_smon_ | awk '{print $NF}' | awk -F_ '{print $NF}'  > db_server_info_line.txt
  database_list=$(sed ':a;N;$!ba;s/\n/,/g' < db_server_info_line.txt)
  print "$ip_address $database_list" >> db_server_info.txt
 fi
done<machine_info.txt
sort db_server_info.txt > sorted_db_server_info.txt
rm -f db_server_info.txt

rm -f list_server_info.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   break
  fi
  sleep 1
 done
 if [ $no_response -eq 0 ]
 then
  ssh -nq $ip_address grep $ip_address /etc/hosts >> list_server_info.txt
 fi
done<machine_info.txt

rm -f machine_info.txt
awk '{print $1,$2,$3}' list_server_info.txt > server_info_list.txt
rm -f server_info.txt list_server_info.txt
while read ip_address host_domain host
do
 found=0
 grep -w $host server_info_list.txt | egrep '10\.60\.5|10\.60\.3|10\.60\.99' > /dev/null
 if [ $? -eq 0 ]
 then
  print "$ip_address $host Production_Everett" >> server_info.txt
  found=1
 fi
 if [ $found -eq 0 ]
 then
  grep -w $host server_info_list.txt | egrep '10\.60\.105\.|10\.60\.103\.' > /dev/null
  if [ $? -eq 0 ]
  then
   print "$ip_address $host IMP_Everett" >> server_info.txt
   found=1
  fi
 fi
 if [ $found -eq 0 ]
 then
  grep -w $host server_info_list.txt | egrep '10\.70\.40\.|10\.70\.41\.|10\.70\.32\.' > /dev/null
  if [ $? -eq 0 ]
  then
   print "$ip_address $host Production_Pittsburgh" >> server_info.txt
   found=1
  fi
 fi
 if [ $found -eq 0 ]
 then
  grep -w $host server_info_list.txt | egrep '10\.70\.140\.|10\.70\.141\.|10\.70\.142\.' > /dev/null
  if [ $? -eq 0 ]
  then
   print "$ip_address $host IMP_Pittsburgh" >> server_info.txt
   found=1
  fi
 fi
 if [ $found -eq 0 ]
 then
  print "$ip_address $host" >> server_info.txt
 fi
done<server_info_list.txt

sort server_info.txt > sorted_server_info.txt

join -j1 1 -j2 1 -o 1.1 1.2 1.3 2.2 sorted_server_info.txt sorted_db_server_info.txt > server_info_list.txt
sort -k3 server_info_list.txt > server_info.txt

awk '{printf "%-17s%-16s%-25s%-50s\n",$1,$2,$3,$4}' server_info.txt > server_info_list.txt
rm -f server_info.txt sorted_db_server_info.txt db_server_info_line.txt sorted_server_info.txt

exit 0
10.70.142
