accept xDBname    default 'eaglestar'          prompt 'Enter the DATABASE Name (default: eaglestar) : '
accept xPacePWD   default 'pace_masterdbo'     prompt 'Enter the Pace_masterdbo password  (default: pace_masterdbo) : '
connect pace_masterdbo/&xPacePWD@&xDBname

-- Create temporary table
begin
  execute immediate ('drop table TEMP_EXPORT_DATA');
exception when others then
  null;
end;
/

create global temporary table TEMP_EXPORT_DATA
(
  REC_ORDER     NUMBER(20),
  SQL_STATEMENT VARCHAR2(4000)
);

-- Create temp sequence 
begin
  execute immediate ('drop sequence temp_order');
exception when others then
 null;
end;
/

create sequence temp_order
minvalue 1
start with 1
increment by 1;


CREATE OR REPLACE FUNCTION GET_EAGLE_VERSION
RETURN NUMBER DETERMINISTIC
IS 
  my_version NUMBER;
BEGIN
  BEGIN
    SELECT to_number(case when instr(version, '.')>0
                            then substr(version, 1, instr(version, '.') - 1)
                          else version
                      end)
      INTO my_version
      FROM pace_masterdbo.eagle_product_version epv
     WHERE epv.update_date = (select max(update_date) from pace_masterdbo.eagle_product_version v
                               where v.product_type like '%-DB')
       AND epv.product_type LIKE '%-DB'
       AND rownum < 2;
  EXCEPTION WHEN others THEN
    my_version := 0;
  END;

  RETURN my_version;
END;
/

CREATE OR REPLACE PROCEDURE EXPORT_DATA(
 in_table_owner   IN VARCHAR2,
 in_table_name    IN VARCHAR2
) AS
  sql_statment   VARCHAR2(32767) := NULL;
  temp_sql       VARCHAR2(32767) := NULL;
  columns_list   VARCHAR2(32767) := NULL;
  insert_string  varchar2(32767) := null;
  val_string     varchar2(32767) := null;
  str_max_pos    NUMBER;
BEGIN
   columns_list := '(';
   temp_sql := '';

   BEGIN
     SELECT '' INTO temp_sql
       FROM sys.all_tab_columns col
      WHERE owner = upper(in_table_owner)
        AND table_name = upper(in_table_name)
        AND rownum < 2;
   EXCEPTION WHEN others THEN
     RETURN;
   END;

   FOR my_list IN ( SELECT column_name, data_length FROM sys.all_tab_columns col
                    WHERE owner = upper(in_table_owner)
                    AND table_name = upper(in_table_name)
                    ORDER BY col.column_id)
   LOOP
     columns_list := columns_list || my_list.column_name || ',';
     temp_sql := '';
   END LOOP;

   columns_list := substr(columns_list,1,length(columns_list)-1)||')';

  sql_statment := 
  ' DECLARE '||
  ' BEGIN  '||
  ' INSERT INTO TEMP_EXPORT_DATA VALUES (temp_order.nextval,''SET SCAN OFF veri off feed off trimspool on timing off tab on trimout on echo off'');'||
    
--    veri off feed off trimspool on timing off
    ' insert into TEMP_EXPORT_DATA select temp_order.nextval, ''TRUNCATE TABLE '||upper(in_table_owner||'.'||in_table_name)||';'' from dual; '||
    ' insert into TEMP_EXPORT_DATA select temp_order.nextval, ''INSERT INTO '||upper(in_table_owner||'.'||in_table_name)||'''||'''||columns_list||'''||chr(10)||''VALUES(''';

   val_string := '';
	 FOR my_list IN ( SELECT column_name, data_type FROM sys.all_tab_columns col
                    WHERE owner = upper(in_table_owner)
                    AND table_name = upper(in_table_name)
                    ORDER BY col.column_id)
	 LOOP
	   IF rtrim(my_list.data_type) IN ('CHAR','VARCHAR2') THEN
--  		 val_string := val_string || ' '' ||RTRIM(replace('||my_list.column_name||', '''''''', '' '' '' '' ))|| '' '||' , ';
 		 val_string := val_string || ' '' ||RTRIM( replace( replace(replace('||my_list.column_name||', '''''''', '' '' '' '' ), chr(10), chr(32)), chr(13), chr(32)) )|| '' '||' , ';
		 ELSIF rtrim(my_list.data_type) = 'DATE' THEN
		   val_string := val_string || 'to_date(nvl( '' '' || to_char('||my_list.column_name||',''YYYY-MM-DD HH24:MI:SS'')|| '' '',NULL), '' YYYY-MM-DD HH24:MI:SS '' )''|| '', ';
		 ELSE
		   val_string := val_string || '|| '||my_list.column_name||'|| , ';
		 END IF;
	 END LOOP;

   val_string := replace(val_string, ' '' ', '''''');
   val_string := replace(val_string, ''', ', ''',''');
   val_string := replace(val_string, ' , ', ''',''');
   val_string := replace(val_string, '''to_date(', 'to_date(');

	 str_max_pos := instr(val_string, '||', -3, 1);

	 IF substr(val_string,str_max_pos + 2, 3) = '''''''' THEN
     val_string := substr(val_string, 1, str_max_pos + 5 - 1) || '''';
	 ELSE
     val_string := substr(val_string, 1, str_max_pos - 1);
	 END IF;

   sql_statment := sql_statment || val_string || '||'');'''||
   ' from '||upper(in_table_owner||'.'||in_table_name)||';'||
   '   INSERT INTO TEMP_EXPORT_DATA VALUES (temp_order.nextval, ''SET SCAN ON'');'||
   '   INSERT INTO TEMP_EXPORT_DATA VALUES (temp_order.nextval, ''commit;'');'||
  ' END; ';			

   sql_statment := replace(sql_statment, 'VALUES(''to_date', 'VALUES(''''to_date');

  EXECUTE IMMEDIATE sql_statment;

  update TEMP_EXPORT_DATA
  set sql_statement = REPLACE(sql_statement, '(,','(NULL,');

  update TEMP_EXPORT_DATA
  set sql_statement = REPLACE(sql_statement, ',,',',NULL,');

  update TEMP_EXPORT_DATA
  set sql_statement = REPLACE(sql_statement, ',,',',NULL,');

  update TEMP_EXPORT_DATA
  set sql_statement = REPLACE(sql_statement, ',)',',NULL)');

END EXPORT_DATA;
/

CREATE OR REPLACE FUNCTION SPLIT_TEXT(
    in_text           VARCHAR2,
    in_line_size      INTEGER,
    in_delimiter      VARCHAR2
) RETURN VARCHAR2 
IS
  lines_count         NUMBER          := 1;
  i                   NUMBER          := 1;
  pos_begin           NUMBER          := 1;
  pos_end             NUMBER          := 1;
  pos_comma           NUMBER          := 0;
  text_length         NUMBER          := 0;
  next_line_delimiter VARCHAR2(3)     := chr(10);
  result              VARCHAR2(4000):= '';
BEGIN
-- Raise error if line size is too small
-- IF in_line_size < 100 THEN 
--    raise_application_error(-20201,'Minimum line size should be greater then 100.');
--  END IF;
  
  text_length := length(in_text);
  IF text_length > in_line_size THEN
    lines_count := trunc(text_length/in_line_size);
    IF lines_count <> text_length/in_line_size THEN 
      lines_count := lines_count + 1;
    END IF;
    
    FOR i IN 1..lines_count LOOP
      pos_end := i * in_line_size;
      IF pos_end >= text_length THEN
        result := result || substr(in_text, pos_begin, (text_length - pos_begin + 1)) || next_line_delimiter;
      ELSE
        pos_comma := instr(in_text, in_delimiter, pos_end, 1) + 1;
        result := result || substr(in_text, pos_begin, (pos_comma - pos_begin)) || next_line_delimiter;
        pos_begin := pos_comma;
      END IF;
    END LOOP;
  ELSE
    result := in_text;
  END IF;
 
  RETURN(result);
END SPLIT_TEXT;
/

set linesize 4000
set pagesize 0
set feedback off
set echo off
set heading off
set trimspool on
set tab on
set trimout on

column sql_statement FORMAT A2000 HEADING 'SQL Statements'

accept xInsert_path  prompt 'Enter the the full path for scripts location(eg. /home/oracle/ - for Unix, c:\oracle - for Win):'

set termout off

spool &xInsert_path\insert_pace_users.sql
exec EXPORT_DATA('pace_masterdbo','pace_users')
select SPLIT_TEXT(sql_statement, 3990, ',') from TEMP_EXPORT_DATA
order by REC_ORDER;
select 'UPDATE pace_masterdbo.next_instance SET next_inst = (SELECT nvl(MAX(instance) + 1, 1) from PACE_MASTERDBO.PACE_USERS) WHERE table_name = ''PACE_MASTERDBO.PACE_USERS'';' from dual;
select 'commit;' from dual;
select 'TRUNCATE TABLE PACE_MASTERDBO.PACE_USERS_HIST;' from dual;
commit;

spool &xInsert_path\insert_pace_user_role_details.sql
exec EXPORT_DATA('pace_masterdbo','pace_user_role_details')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
select 'UPDATE pace_masterdbo.next_instance SET next_inst = (SELECT nvl(MAX(instance) + 1, 1) from PACE_MASTERDBO.PACE_USER_ROLE_DETAILS) WHERE table_name = ''PACE_MASTERDBO.PACE_USER_ROLE_DETAILS'';' from dual;
select 'commit;' from dual;
commit;

spool &xInsert_path\insert_starsec_user_detail.sql
exec EXPORT_DATA('pace_masterdbo','starsec_user_detail')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
commit;

spool &xInsert_path\insert_starsec_user_group.sql
exec EXPORT_DATA('pace_masterdbo','starsec_user_group')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
select 'UPDATE pace_masterdbo.next_instance SET next_inst = (SELECT nvl(MAX(star_group_id) + 1, 1) from PACE_MASTERDBO.STARSEC_USER_GROUP) WHERE table_name = ''PACE_MASTERDBO.STARSEC_USER_GROUP'';' from dual;
select 'commit;' from dual;
commit;

spool &xInsert_path\insert_starsec_group_detail.sql
exec EXPORT_DATA('pace_masterdbo','starsec_group_detail')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
commit;

spool &xInsert_path\insert_starsec_group_sum.sql
exec EXPORT_DATA('pace_masterdbo','starsec_group_sum')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
select 'UPDATE pace_masterdbo.next_instance SET next_inst = (SELECT nvl(MAX(id) + 1, 1) from PACE_MASTERDBO.STARSEC_GROUP_SUM) WHERE table_name = ''PACE_MASTERDBO.STARSEC_GROUP_SUM'';' from dual;
select 'commit;' from dual;
commit;

spool &xInsert_path\insert_starsec_group_rel.sql
exec EXPORT_DATA('pace_masterdbo','starsec_group_rel')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
commit;

spool &xInsert_path\insert_user_client_maintenance.sql
exec EXPORT_DATA('pace_masterdbo','user_client_maintenance')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
commit;

spool &xInsert_path\insert_user_entity_maintenance.sql
exec EXPORT_DATA('pace_masterdbo','user_entity_maintenance')
select sql_statement from TEMP_EXPORT_DATA
order by REC_ORDER;
commit;

spool &xInsert_path\insert_pace_user_groups.sql
exec EXPORT_DATA('pace_masterdbo','pace_user_groups')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
select 'UPDATE pace_masterdbo.next_instance SET next_inst = (SELECT nvl(MAX(GROUP_ID) + 1, 1) from PACE_MASTERDBO.PACE_USER_GROUPS) WHERE table_name = ''PACE_MASTERDBO.PACE_USER_GROUPS'';' from dual where GET_EAGLE_VERSION() >= 9;
select 'commit;' from dual where GET_EAGLE_VERSION() >= 9;
commit;

spool &xInsert_path\insert_business_group_details.sql
exec EXPORT_DATA('pace_masterdbo','business_group_details')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;

spool &xInsert_path\insert_group_clients.sql
exec EXPORT_DATA('pace_masterdbo','group_clients')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;

spool &xInsert_path\insert_group_entities.sql
exec EXPORT_DATA('pace_masterdbo','group_entities')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;

spool &xInsert_path\insert_group_reports.sql
exec EXPORT_DATA('pace_masterdbo','group_reports')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;

spool &xInsert_path\insert_group_sources.sql
exec EXPORT_DATA('pace_masterdbo','group_sources')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;

spool &xInsert_path\insert_group_codes.sql
exec EXPORT_DATA('pace_masterdbo','group_codes')
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() >= 9 ORDER BY REC_ORDER;
commit;


declare 
  sql_text varchar2(4000);
--  next_line_delimiter VARCHAR2(3)        := chr(10)||chr(13);
  next_line_delimiter VARCHAR2(3)        := chr(10);
begin
sql_text := 'DECLARE '||next_line_delimiter||
'     CURSOR star_fund_groups_cur IS'||next_line_delimiter||
'     SELECT sug.star_group_id Star_group_id, pug.group_id Pace_group_id'||next_line_delimiter||
'     FROM starsec_user_group sug, starsec_group_sum sgs, '||next_line_delimiter||
'          starsec_group_rel sgr, starsec_group_sum sgs1, pace_user_groups pug'||next_line_delimiter||
'     WHERE sug.star_group_id = sgs.star_group_id'||next_line_delimiter||
'       AND sgs.TYPE = ''SECURITY'''||next_line_delimiter||
'       AND sgs.id = sgr.sec_grp_id'||next_line_delimiter||
'       AND sgr.group_id = sgs1.id'||next_line_delimiter||
'       AND sgs1.TYPE = ''FUND'''||next_line_delimiter||
'       AND rtrim(sgs1.NAME) = rtrim(pug.group_name);'||next_line_delimiter||
'     CURSOR star_source_groups_cur IS'||next_line_delimiter||
'     SELECT sug.star_group_id Star_group_id, pug.group_id Pace_group_id'||next_line_delimiter||
'     FROM starsec_user_group sug, starsec_group_sum sgs, '||next_line_delimiter||
'          starsec_group_rel sgr, starsec_group_sum sgs1, pace_user_groups pug'||next_line_delimiter||
'     WHERE sug.star_group_id = sgs.star_group_id'||next_line_delimiter||
'       AND sgs.TYPE = ''SECURITY'''||next_line_delimiter||
'       AND sgs.id = sgr.sec_grp_id'||next_line_delimiter||
'       AND sgr.group_id = sgs1.id'||next_line_delimiter||
'       AND sgs1.TYPE = ''SOURCE'''||next_line_delimiter||
'       AND rtrim(sgs1.NAME) = rtrim(pug.group_name);'||next_line_delimiter||
'     CURSOR star_client_groups_cur IS'||next_line_delimiter||
'     SELECT sug.star_group_id Star_group_id, pug.group_id Pace_group_id'||next_line_delimiter||
'     FROM starsec_user_group sug, starsec_group_sum sgs, '||next_line_delimiter||
'          starsec_group_rel sgr, starsec_group_sum sgs1, pace_user_groups pug'||next_line_delimiter||
'     WHERE sug.star_group_id = sgs.star_group_id'||next_line_delimiter||
'       AND sgs.TYPE = ''SECURITY'''||next_line_delimiter||
'       AND sgs.id = sgr.sec_grp_id'||next_line_delimiter||
'       AND sgr.group_id = sgs1.id'||next_line_delimiter||
'       AND sgs1.TYPE = ''CLIENT'''||next_line_delimiter||
'       AND rtrim(sgs1.NAME) = rtrim(pug.group_name);'||next_line_delimiter||
'     CURSOR star_repoption_groups_cur IS'||next_line_delimiter||
'     SELECT sug.star_group_id Star_group_id, pug.group_id Pace_group_id'||next_line_delimiter||
'     FROM starsec_user_group sug, starsec_group_sum sgs, '||next_line_delimiter||
'          starsec_group_rel sgr, starsec_group_sum sgs1, pace_user_groups pug'||next_line_delimiter||
'     WHERE sug.star_group_id = sgs.star_group_id'||next_line_delimiter||
'       AND sgs.TYPE = ''SECURITY'''||next_line_delimiter||
'       AND sgs.id = sgr.sec_grp_id'||next_line_delimiter||
'       AND sgr.group_id = sgs1.id'||next_line_delimiter||
'       AND sgs1.TYPE = ''REPORTOPTION'''||next_line_delimiter||
'       AND rtrim(sgs1.NAME) = rtrim(pug.group_name);'||next_line_delimiter||
' BEGIN'||next_line_delimiter||
'   FOR star_fund_group_rec IN star_fund_groups_cur '||next_line_delimiter||
'   LOOP '||next_line_delimiter||
'     UPDATE starsec_user_group t'||next_line_delimiter||
'     SET t.pace_fund_group_id = star_fund_group_rec.Pace_group_id'||next_line_delimiter||
'     WHERE t.star_group_id = star_fund_group_rec.Star_group_id;'||next_line_delimiter||
'   END LOOP;'||next_line_delimiter||
'   COMMIT;'||next_line_delimiter||
'   FOR star_source_group_rec IN star_source_groups_cur'||next_line_delimiter||
'   LOOP '||next_line_delimiter||
'     UPDATE starsec_user_group t'||next_line_delimiter||
'     SET t.pace_source_group_id = star_source_group_rec.Pace_group_id'||next_line_delimiter||
'     WHERE t.star_group_id = star_source_group_rec.Star_group_id;'||next_line_delimiter||
'   END LOOP;'||next_line_delimiter||
'   COMMIT;'||next_line_delimiter||
'   FOR star_client_group_rec IN star_client_groups_cur'||next_line_delimiter||
'   LOOP '||next_line_delimiter||
'     UPDATE starsec_user_group t'||next_line_delimiter||
'     SET t.pace_client_group_id= star_client_group_rec.Pace_group_id'||next_line_delimiter||
'     WHERE t.star_group_id = star_client_group_rec.Star_group_id;'||next_line_delimiter||
'   END LOOP;'||next_line_delimiter||
'   COMMIT;'||next_line_delimiter||
'   FOR star_repoption_group_rec IN star_repoption_groups_cur '||next_line_delimiter||
'   LOOP '||next_line_delimiter||
'     UPDATE starsec_user_group t'||next_line_delimiter||
'     SET t.pace_reportoption_group_id = star_repoption_group_rec.Pace_group_id'||next_line_delimiter||
'     WHERE t.star_group_id = star_repoption_group_rec.Star_group_id;'||next_line_delimiter||
'   END LOOP;'||next_line_delimiter||
'   COMMIT; '||next_line_delimiter||
'END;'||next_line_delimiter||
'/'||next_line_delimiter;
COMMIT;
INSERT INTO TEMP_EXPORT_DATA VALUES (temp_order.nextval, sql_text);
end;
/

spool &xInsert_path\update_star_group_ids.sql
select sql_statement from TEMP_EXPORT_DATA where GET_EAGLE_VERSION() < 9
order by REC_ORDER;

spool &xInsert_path\run_import_user.sql

set veri off
set scan off

select 'accept xDBname    default ''eaglestar''          prompt ''Enter the DATABASE Name (default: eaglestar) : ''' from dual;
select 'accept xPacePWD   default ''pace_masterdbo''     prompt ''Enter the Pace_masterdbo password  (default: pace_masterdbo) : ''' from dual;
select 'accept xScripts_path  prompt ''Enter the the full path for scripts location(eg. /home/oracle/ - for Unix, c:\oracle - for Win):''' from dual;

select 'connect pace_masterdbo/&xPacePWD@&xDBname ' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_pace_users.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_pace_users.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_pace_user_role_details.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_pace_user_role_details.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_starsec_group_detail.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_starsec_group_detail.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_starsec_group_rel.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_starsec_group_rel.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_starsec_group_sum.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_starsec_group_sum.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_starsec_user_detail.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_starsec_user_detail.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_starsec_user_group.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_starsec_user_group.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_user_client_maintenance.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_user_client_maintenance.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_user_entity_maintenance.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_user_entity_maintenance.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_pace_user_groups.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_pace_user_groups.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_business_group_details.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_business_group_details.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_group_clients.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_group_clients.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_group_entities.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_group_entities.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_group_reports.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_group_reports.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_group_sources.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_group_sources.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\insert_group_codes.out' from dual;
select '@'|| '&xScripts_path' ||'\insert_group_codes.sql' from dual;
select 'spool off' from dual;

select 'spool '|| '&xScripts_path' ||'\update_star_group_ids.out' from dual;
select '@'|| '&xScripts_path' ||'\update_star_group_ids.sql' from dual;
select 'spool off' from dual;

select 'set heading off'  from dual;
select 'select ''UMGR data import finished. Please check *.out files to get information about errors.'' from dual; '  from dual;
select 'exit'  from dual;

spool off
SET SCAN ON

drop procedure EXPORT_DATA;
drop function SPLIT_TEXT;
drop sequence temp_order;
drop table TEMP_EXPORT_DATA;

set feedback on
set heading on
set trimspool on
set tab off
set trimout off
set echo on

clear BUFFER
clear COLUMNS
clear SQL

exit
