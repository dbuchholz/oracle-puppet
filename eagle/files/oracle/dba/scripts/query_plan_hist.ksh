#!/bin/ksh
# Script name: query_plan_hist.ksh
# Usage: query_plan_hist.ksh [ Oracle SID ]

if [ $# -ne 1 ]
then
   print "\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <Oracle SID>\n"
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
read sql_id?"Enter SQL ID: "

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set echo off trimspool on feedback off lines 200
select SQL_ID,NAME,LAST_CAPTURED,VALUE_STRING from v\$sql_bind_capture where SQL_ID='$sql_id';
select snap_id,sql_id,PLAN_HASH_VALUE from dba_hist_sqlstat where sql_id='$sql_id' order by snap_id;
EOF
exit 0
