#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
script_dir=/u01/app/oracle/local/dba/scripts
log_dir=$script_dir

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$log_dir/oracle_10_or_greater_sids.txt
set pages 0 trimspool on feedback off echo off
WHENEVER SQLERROR EXIT FAILURE
select m.name,m.ip_address,d.sid
from inf_monitor.machines m,inf_monitor.databases d
where m.instance=d.mac_instance
and d.oracle_version not like '9%'
and d.dataguard='N'
and status='OPEN';
EOF
if [ $? -ne 0 ]
then
 print "Could not get results from $ORACLE_SID Database."
 exit 1
fi

while read name ip_address sid
do
 ssh -nq $ip_address ls $script_dir/awr_setup.ksh > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  scp -nq $script_dir/awr_setup.ksh ${ip_address}:$script_dir
 fi
 print "$name $sid"
 ssh -nq $ip_address $script_dir/awr_setup.ksh $sid
done<$log_dir/oracle_10_or_greater_sids.txt

rm -f $log_dir/oracle_10_or_greater_sids.txt

exit 0
