#!/bin/ksh
# Script name: awr_snapshot.ksh
# Usage: awr_snapshot.ksh <Oracle SID>
# Takes an AWR Snapshot

export ORAENV_ASK=NO
export ORACLE_SID=$1
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

if [ $# -ne 1 ]
then
 print "Invalid arguments!"
 print "Usage : $0 <Oracle SID>"
 exit 1
fi

print "" > ${log_dir}/db_stat_${ORACLE_SID}.log
 $ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>${log_dir}/db_stat_${ORACLE_SID}.log 2>&1
 WHENEVER SQLERROR EXIT FAILURE;
 set echo on feed on
 execute dbms_workload_repository.create_snapshot;
EOF
if [ $? -ne 0 ]
then
 print "Error while taking the snapshot..."
 print "View ${logdir}/db_stat_${ORACLE_SID}.log for details..."
 exit 1
fi
print "Snapshot Executed."
exit 0
