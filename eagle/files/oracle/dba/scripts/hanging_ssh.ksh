#!/bin/ksh
ssh -nq pnsd0001 ls > a.log 2>&1 &
the_pid=$!
sleep 3
((count = 0))
while true
do
 ps -ef | grep $the_pid | grep -v grep > /dev/null
 if [ $? -ne 0 ]
 then
  break
 fi
 ((count=count+1))
 if [ $count -gt 2 ]
 then
  print "Did not respond in five seconds.  Killing the ssh command."
  kill -9 $the_pid
  break
 fi
 sleep 1
done
exit 0
