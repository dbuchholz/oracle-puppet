#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>mac_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines order by name;
EOF

while read name ip_address
do
 ssh -nq $ip_address "find /u01/app/oracle/product -type f -name listener.log -exec ls -l {} \;;hostname"
done<mac_info.txt
exit 0
