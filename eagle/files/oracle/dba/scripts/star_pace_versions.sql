select version "PACE VERSION" from pace_masterdbo.eagle_product_version where instance=(select max(instance) from pace_masterdbo.eagle_product_version where upper(product_type) in ('EDM','PACE')
and instance not in (select instance from pace_masterdbo.eagle_product_version where upper(comments) like '%ADDENDUM%'));

select version "STAR VERSION" from pace_masterdbo.eagle_product_version where instance=(select max(instance) from pace_masterdbo.eagle_product_version where upper(product_type) in ('EDM','STAR-DB')
and instance not in (select instance from pace_masterdbo.eagle_product_version where upper(comments) like '%ADDENDUM%'));
