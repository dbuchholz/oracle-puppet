#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

rm -f list_archive_log_two_day_max.txt

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>prod_dbs.txt
set pages 0 trimspool on feedback off echo off
select sid from databases where environment='PROD';
EOF

for a_db in $(cat prod_dbs.txt)
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>archive_info.txt
set pages 0 trimspool on feedback off echo off
select ceil(a.archive_size_g) from archive_log_size a,databases d
where d.instance=a.db_instance and d.sid='$a_db' order by a.archive_date desc;
EOF

previous_value=0
largest_pair=0
while read arch_size
do
 current_value=$arch_size
 ((current_pair=$previous_value + $current_value))
 if [ $current_pair -gt $largest_pair ]
 then
  largest_pair=$current_pair
 fi
 previous_value=$current_value
done<archive_info.txt
print "$a_db $largest_pair" >> list_archive_log_two_day_max.txt
done
rm -f archive_info.txt
exit 0
