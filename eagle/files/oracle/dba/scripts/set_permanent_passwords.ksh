#!/bin/ksh
# Script Name: set_permanent_passwords.ksh
# Usage: set_permanent_passwords.ksh [Oracle SID]

if [ $# -ne 1 ]
then
 print "\nMust enter one parameter into this script."
 print "The parameter must be the Oracle SID.\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
now=$(date +'%Y%m%d_%H%M%S')

read usn?"Enter your infprd2 Database Username: "
print " "

stty -echo
read pwd?"Enter your infprd2 Database Password: "
stty echo
clear

pord=P
env_count=`$ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
set pagesize 0
select count(environment) from inf_monitor.databases where sid='$ORACLE_SID';
EOF
`
if [ "$env_count" -gt 1 ]
then
 read pord?"Is the database you want to set to Permanent password a [P]rod or [D]R. Enter: P or D: "
 if [[ -n $pord ]]
 then
  pord=$(print $pord | cut -c1 | tr '[a-z]' '[A-Z]')
 else
  print "The database is a Production or DR.  You must make a choice on which one you are working on."
  print "Aborting the script here..."
  exit 1
 fi
 if [ "$pord" != "P" ] && [ "$pord" != "D" ]
 then
  print "\nMust enter P or D when prompted."
  print "P = Prod and D = DR"
  print "Try running script again and answer prompt with P or D"
  exit 2
 fi
fi

if [ "$pord" = "P" ]
then
 $ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 update inf_monitor.user_codes set temp_code='' where db_instance=(select instance from inf_monitor.databases where sid='$ORACLE_SID' and dataguard='N');
EOF
 if [ $? -ne 0 ]
 then
  print "Could not make a connection to Big Brother database."
  print "Account credentials may be incorrect such as the password or username."
  exit 3
 fi
fi

$ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
WHENEVER SQLERROR EXIT FAILURE
set pagesize 0
spool set_to_perm_${ORACLE_SID}_${now}.sql
select 'alter user '||u.username||' identified by '||inf_monitor.bb_get(u.code)||';'
from inf_monitor.user_codes u,inf_monitor.databases d where u.db_instance=d.instance and d.sid='$ORACLE_SID' and d.dataguard='N'
order by u.username;
EOF
if [ $? -ne 0 ]
then
 print "There was an error when trying to generate the \"alter user\" commands to set the database passwords to \"eagle\"."
 exit 4
fi

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
WHENEVER SQLERROR EXIT FAILURE
@set_to_perm_${ORACLE_SID}_${now}.sql
select name from v\$database;
EOF
if [ $? -ne 0 ]
then
 print "There was an error when setting database passwords for $ORACLE_SID to permanent values."
 exit 5
fi
print "Passwords successfully changed to permanent passwords."
exit 0
