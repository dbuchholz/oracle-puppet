#!/bin/ksh
clear
export ORACLE_SID=$1
export ORAENV_ASK=NO
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set linesize 130 pagesize 40 feedback off
col object_name format a25
col oracle_username format a20
col lock_type format a26
col mode_held format a13
col mode_requested format a12
col lock_id1 format a8
col lock_id2 format a8
  select
	sid session_id,
	decode(type,
		'MR', 'Media Recovery',
		'RT', 'Redo Thread',
		'UN', 'User Name',
		'TX', 'Transaction',
		'TM', 'DML',
		'UL', 'PL/SQL User Lock',
		'DX', 'Distributed Xaction',
		'CF', 'Control File',
		'IS', 'Instance State',
		'FS', 'File Set',
		'IR', 'Instance Recovery',
		'ST', 'Disk Space Transaction',
		'TS', 'Temp Segment',
		'IV', 'Library Cache Invalidation',
		'LS', 'Log Start or Switch',
		'RW', 'Row Wait',
		'SQ', 'Sequence Number',
		'TE', 'Extend Table',
		'TT', 'Temp Table',
		type) lock_type,
	decode(lmode,
		0, 'None',           /* Mon Lock equivalent */
		1, 'Null',           /* N */
		2, 'Row-S (SS)',     /* L */
		3, 'Row-X (SX)',     /* R */
		4, 'Share',          /* S */
		5, 'S/Row-X (SSX)',  /* C */
		6, 'Exclusive',      /* X */
		to_char(lmode)) mode_held,
         decode(request,
		0, 'None',           /* Mon Lock equivalent */
		1, 'Null',           /* N */
		2, 'Row-S (SS)',     /* L */
		3, 'Row-X (SX)',     /* R */
		4, 'Share',          /* S */
		5, 'S/Row-X (SSX)',  /* C */
		6, 'Exclusive',      /* X */
		to_char(request)) mode_requested,
         to_char(id1) lock_id1, to_char(id2) lock_id2
      from v\$lock where type not in ('MR','RT');
break on session_id skip 1; 
column oracle_username format a15 heading "User Name"; 
column session_id format 9999 heading "SID"; 
column os_user_name format a12 heading "OS User"; 
column Object format a35 heading "Object"; 
column lckmode format a12 heading "Locked Mode"; 
column xidusn format 999 heading "Undo SegNo"; 
column xidslot format 999 heading "Slot No"; 
column xidsqn format 99999999 heading "Seq No"; 
select session_id,oracle_username,os_user_name,process, 
b.owner||'.'||b.object_name Object, xidusn,xidslot,xidsqn, 
decode(locked_mode,2,'Row-S(SS)',3,'Row-X(SX)',4,'Share',5,'S/Row-X(SSX)',6,'Exclusive','Null') lckmode 
from v\$locked_object a,dba_objects b 
where a.object_id=b.object_id;
EOF
exit 0
