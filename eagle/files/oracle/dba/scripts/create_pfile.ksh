#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>/dev/null
set pagesize 0 trimspool on echo off lines 200 feedback off
create pfile='/tmp/pfile_${ORACLE_SID}.txt' from spfile;
EOF

exit 0
