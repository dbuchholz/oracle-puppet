#!/bin/ksh
# Script name: get_standby_max_arch_seq_run_on_dr.ksh
# Usage: get_standby_max_arch_seq.ksh [ Oracle SID ]
# This script is to be run from the DR or Manual DR host.
#
# The script will make a local sqlplus connection to the DR database.
# The script will make a sqlplus connection using SQL*Net with a connect string to the Production host.
# It compares the maximum log sequence number on both databases and compares how far Production is ahead of DR.
# If the DR is more than $behind_threshold behind, it will send out an email alert to $mailrecipients.
# Script to check archive log gap.

mailrecipients=fdavis@eagleinvsys.com
(( behind_threshold = 10 ))

function funct_check_inf_database
{
 PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select 1 from dual;
 exit
EOF`
 PERFORM_CRON_STATUS=$(print $PERFORM_CRON_STATUS|tr -d " ")
 if [ $PERFORM_CRON_STATUS -ne 1 ]
 then
  PERFORM_CRON_STATUS=0
 fi
}

function funct_check_db_code
{
 pcode=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select bb_get(code) from user_codes c,databases d where c.db_instance=d.instance and d.sid='$ORACLE_SID' and c.username='$puser' and d.dataguard='N' and d.status='OPEN';
EOF`
 pcode=$(print $pcode|tr -d " ")

 tnsname_prod=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
 set heading off
 set feedback off
 select tnsname from databases where sid='$ORACLE_SID' and dataguard='N' and status='OPEN';
EOF`
 tnsname_prod=$(print $tnsname_prod|tr -d " ")
}

function get_standby_max_seq_Manual_DR
{
 dr_seq=`$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set echo off veri off feed off pages 0 sqlp "" trim on trims on lines 80
 SELECT sequence# FROM(SELECT rownum rn, al.* FROM v\\$archived_log al order by COMPLETION_TIME) where rn > (select count(*)-1 from v\\$archived_log);
EOF`
 if [ $? -ne 0 ]
 then
  return 1
 fi
 return 0
}

function get_standby_max_seq_Prod
{
 prod_seq=`$ORACLE_HOME/bin/sqlplus -s "$puser/${pcode}@$tnsname_prod as sysdba"<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set echo off veri off feed off pages 0 sqlp "" trim on trims on lines 80
 SELECT sequence# FROM(SELECT rownum rn, al.* FROM v\\$archived_log al order by COMPLETION_TIME) where rn > (select count(*)-1 from v\\$archived_log);
EOF`
 if [ $? -ne 0 ]
 then
  return 1
 fi
 return 0
}

# Main script starts here...

if [ $# -ne 1 ]
then
   echo "Invalid Arguments!"
   echo "Usage : $0 <ORACLE_SID>" 
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export CONSTR=$2

export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export PROGRAM_NAME=$(print $0 | sed 's/.*\///g')
export PROGRAM_NAME_FIRST=$(print $PROGRAM_NAME | awk -F. '{print $1}')
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
puser=SYS
export PERFORM_CRON_STATUS=0

if [[ -a ${PARFILE} ]]
then
 STATUS=$(cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE})
 LINE=$(cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:")
 rm -f $NO_COMMENT_PARFILE
 if [[ -n $LINE ]]
 then
  INFO=$(print $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g')
  export CRON_SID=$(print $INFO | awk '{print $1}')
  export CRON_USER=$(print $INFO | awk '{print $2}')
  CRON_CONNECT=$(print $INFO | awk '{print $3}')
  export CODE=$(${PAR_HOME}/GetDec.cmd ${CRON_CONNECT})
  # Make sure you can get to the infrastructure database
  funct_check_inf_database
 fi
fi
if [ $PERFORM_CRON_STATUS -ne 1 ]
then
 print "Cannot reach $CRON_SID Big Brother Database."
 print "This can mean the tnsnames.ora entry is missing or the password and username are not available."
 print "Aborting here."
 exit 3
fi

funct_check_db_code

get_standby_max_seq_Manual_DR
if [ $? -ne 0 ]
then
 mailx -s "$(hostname): $ORACLE_SID Manual DR cannot get max log sequence number at $(date)" $mailrecipients<<EOF
 This is from $(hostname).
 Cannot get the maximum log sequence number from Manual DR database
 It is possible that the $ORACLE_SID Manual DR database is shut down.
EOF
 exit 4
fi

(( dr_sequence_no = $(print $dr_seq | sed 's/ //g') ))

get_standby_max_seq_Prod
if [ $? -ne 0 ]
then
 mailx -s "$(hostname): $ORACLE_SID Prod cannot get max log sequence number at $(date)" $mailrecipients<<EOF
 This is from $(hostname).
 Cannot get the maximum log sequence number from Prod database
EOF
 exit 5
fi

(( prod_sequence_no = $(print $prod_seq | sed 's/ //g') ))

(( how_far_behind = $prod_sequence_no - $dr_sequence_no ))
if [ $how_far_behind -gt $behind_threshold ]
then
 mailx -s "$(hostname): $ORACLE_SID Manual DR behind Prod by $how_far_behind archive logs at $(date)" $mailrecipients<<EOF
 The threshold is $behind_threshold archive logs behind.
 This is from $(hostname).
EOF
 exit 6
fi

exit 0
