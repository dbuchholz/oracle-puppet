#!/usr/bin/ksh
# This K-Shell script will give you the SQL Statement of the unix pid
if [ $# -ne 1 ]
then
   echo "Invalid Arguments."
   echo "Usage : $0 <UNIX PID>"
   exit 1
fi

export BIN_DIR=/bin
export UNIX_PID=$1

ORACLE_SID=$(${BIN_DIR}/ps -ef | grep -w ${UNIX_PID} | grep -v grep | grep -v $0 | /usr/bin/tail -1 | ${BIN_DIR}/awk '{print $8}' | ${BIN_DIR}/sed -e 's/oracle//' -e 's/ora_...._//')
export ORACLE_SID

if [ -z ${ORACLE_SID} ]
then
   print "Unix PID : $UNIX_PID doesn't exist!"
   exit 1
fi

print "\nORACLE_SID : $ORACLE_SID	UNIX_PID : $UNIX_PID\n"
${BIN_DIR}/ps -eaf | ${BIN_DIR}/egrep " ${UNIX_PID} | PPID " | ${BIN_DIR}/egrep -v "grep|$0"

export ORAENV_ASK=NO
. /usr/local/bin/oraenv
export ORAENV_ASK=YES
export EXEDIR=$ORACLE_HOME/bin

print "\nRetrieving SQL Statistics and SQL Statement from the database..."

${EXEDIR}/sqlplus -s '/ as sysdba' <<EOF
set heading off echo off feed off veri off pages 9999 lines 140
   SELECT 'OS PID                     : ' || TO_CHAR(p.spid) || CHR(10) ||
          'SID                        : ' || TO_CHAR(s.sid) || CHR(10) ||
          'SERIAL#                    : ' || TO_CHAR(s.serial#) || CHR(10) ||
          'STATUS                     : ' || s.status || CHR(10) ||
          'USERNAME                   : ' || s.username || CHR(10) ||
          'OS USER                    : ' || s.osuser || CHR(10) ||
          'PROGRAM                    : ' || s.program || CHR(10) ||
          'SQL ID                     : ' || s.sql_id || CHR(10) ||
          'FIRST LOAD TIME            : ' || a.first_load_time || CHR(10) ||
          'PROGRAM ID                 : ' || a.program_id || CHR(10) ||
          'PLAN HASH VALUE            : ' || a.plan_hash_value || CHR(10) ||
          'ROWS PROCESSED             : ' || TO_CHAR(a.rows_processed,'999G999G999G999D99') || CHR(10) ||
          'EXECUTIONS                 : ' || TO_CHAR(a.executions,'999G999G999G999D99') || CHR(10) ||
          'PHY READS                  : ' || TO_CHAR(a.disk_reads,'999G999G999G999D99') || CHR(10) ||
          'BUFFER GETS                : ' || TO_CHAR(a.buffer_gets,'999G999G999G999D99') || CHR(10) ||
          'PARSE CALLS                : ' || TO_CHAR(a.parse_calls,'999G999G999G999D99') || CHR(10) ||
          'ROWS PROCESSED/EXECUTION   : ' || TO_CHAR(a.rows_processed/a.executions,'999G999G999G999D99') || CHR(10) ||
          'PHY READS/EXECUTION        : ' || TO_CHAR(a.disk_reads/a.executions,'999G999G999G999D99') || CHR(10) ||
          'BUFFER GETS/EXECUTION      : ' || TO_CHAR(a.buffer_gets/a.executions,'999G999G999G999D99') || CHR(10) ||
          'PARSE CALLS/EXECUTION      : ' || TO_CHAR(a.parse_calls/a.executions,'999G999G999G999D99') || CHR(10) ||
          'BUFFER GETS/ROWS PROCESSED : ' || TO_CHAR(a.buffer_gets/(a.rows_processed+1),'999G999G999G999D99') || CHR(10) ||
          'PHY READS/ROWS PROCESSED   : ' || TO_CHAR(a.disk_reads/(a.rows_processed+1),'999G999G999G999D99')
   FROM   v\$session s,
          v\$process p,
          v\$sqlarea a
   WHERE  s.paddr = p.addr
   AND    p.spid  = $UNIX_PID
   AND    a.address(+) = s.sql_address
   AND    a.executions(+) > 0
   ORDER BY s.sid,p.spid;
select a.sql_text
from v\$process p,v\$session s,v\$sqltext_with_newlines a
where s.paddr=p.addr
and p.spid=$UNIX_PID
and a.address(+)=s.sql_address
order by a.piece;
EOF
if [ $? -ne 0 ]
then
 echo "Error while getting the SQL Statement..."
 exit 2
fi
exit 0
