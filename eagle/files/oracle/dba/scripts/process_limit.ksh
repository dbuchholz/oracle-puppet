#!/bin/ksh
host=$(hostname | awk -F. '{print $1}')
export ORAENV_ASK=NO
sids=$(ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
for a_sid in $sids
do
export ORACLE_SID=$a_sid
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set trimspool on pagesize 0 echo off feedback off
select '$ORACLE_SID $host '||max_utilization||' '||limit_value||' '||to_char(max_utilization/limit_value*100,999.9)
from v\$resource_limit where resource_name='processes';
EOF
done
exit 0
