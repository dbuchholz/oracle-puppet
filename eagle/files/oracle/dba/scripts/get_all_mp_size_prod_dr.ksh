#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/u01/app/oracle/local/dba/scripts/mp_size_machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where OS_TYPE<>'SunOS' and location in ('EVERETT','PITTSBURGH') order by name;
EOF

while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   break
  fi
  sleep 1
 done
 rm -f /u01/app/oracle/local/dba/scripts/mount_point_size_prod_dr.txt
 if [ $no_response -eq 0 ]
 then
  hst=$(hostname | awk -F. '{print $1}')
  ssh -nq $ip_address /u01/app/oracle/local/dba/scripts/mount_point_sizes_prod_dr.ksh >> /u01/app/oracle/local/dba/scripts/mount_point_size_prod_dr.log
 fi
done</u01/app/oracle/local/dba/scripts/mp_size_machine_info.txt

sort -k4 /u01/app/oracle/local/dba/scripts/mount_point_size_prod_dr.log > /u01/app/oracle/local/dba/scripts/mount_point_size_prod_dr.txt
rm -f /u01/app/oracle/local/dba/scripts/mount_point_size_prod_dr.log

exit 0
