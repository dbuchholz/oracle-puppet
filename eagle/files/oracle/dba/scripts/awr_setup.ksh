#!/bin/ksh
# Script Name: awr_setup.ksh
# Usage: awr_setup.ksh [Oracle SID]
# This script will check to see if AWR retention and interval are set correctly.
# If not set according to retention_days and interval_minutes defined here, it will set.
#
retention_days=31
interval_minutes=30

if [ $# -ne 1 ]
then
 print "Usage: $(basename $0) [Oracle SID]"
 exit 1
fi

export ORACLE_SID=$1

if [ $(uname) = Linux ]
then
 ORATAB=/etc/oratab
else
 ORATAB=/var/opt/oracle/oratab
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "Cannot find Instance $ORACLE_SID in $ORATAB"
 exit 2
fi

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

interval=`$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set pages 0 trimspool on echo off
WHENEVER SQLERROR EXIT FAILURE
select
extract(day from a.snap_interval)*24*60+extract(hour from a.snap_interval)*60+extract(minute from a.snap_interval)
from dba_hist_wr_control a,v\\$database b
where a.dbid=b.dbid;
EOF`
if [ $? -ne 0 ]
then
 print "There is a problem getting AWR Snapshot Interval in $ORACLE_SID"
 print "Will abort now."
 exit 3
fi
interval=$(print $interval|tr -d " ")

retention=`$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set pages 0 trimspool on echo off
WHENEVER SQLERROR EXIT FAILURE
select
extract(day from a.retention)
from dba_hist_wr_control a,v\\$database b
where a.dbid=b.dbid;
EOF`
if [ $? -ne 0 ]
then
 print "There is a problem getting AWR Snapshot Retention in $ORACLE_SID"
 print "Will abort now."
 exit 4
fi
retention=$(print $retention|tr -d " ")

(( retention_minutes=$retention_days*24*60 ))
if [ $interval -ne $interval_minutes ] || [ $retention -ne $retention_days ]
then
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
 execute dbms_workload_repository.modify_snapshot_settings(interval => $interval_minutes,retention => $retention_minutes);
EOF
fi

exit 0
