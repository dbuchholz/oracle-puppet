#!/bin/ksh
# Script Name: network_file_transfer_rate.ksh
#### Change these values
file_name=/brwprd2-05/oradata/file_copy_test_478MB.txt
target_server=ppld0002
file_destination=/brwprd2-05/oradata/exports
####
#
sync
sync
#
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir
#
file_bytes=$(ls -l $file_name | awk '{print $5}')
actual_file_size=$(print "scale=0; $file_bytes / 1.024 / 1.024" | bc)
size_in_gb=$(print "scale=9; $actual_file_size / 1000000000" | bc)
now=$(date "+%Y%m%d_%H%M")
time -f "%e\n" scp -q $file_name ${target_server}:$file_destination > $log_dir/how_long_sec.txt 2>&1
how_long_sec=$(sed '/^$/d' $log_dir/how_long_sec.txt)
how_long_hours=$(print "scale=5; $how_long_sec / 3600" | bc)
gb_per_hour=$(print "scale=1; $size_in_gb / $how_long_hours" | bc)
ssh -q $target_server rm -f $file_destination/$file_name

print "$now $gb_per_hour" >> $log_dir/network_file_transfer_rate_history.txt
exit 0
