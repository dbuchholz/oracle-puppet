#!/bin/ksh
sids=$(ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}')
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
host=$(hostname | awk -F. '{print $1}')
for a_sid in $sids
do
export ORACLE_SID=$a_sid
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pages 0 echo off feedback off
select '$host'||' '||(select name from v\$database)||' '||substr(file_name,instr(file_name,'/',-10)+1) from dba_data_files group by substr(file_name,instr(file_name,'/',-10)+1) having count(*) > 1;
EOF
done
exit 0
