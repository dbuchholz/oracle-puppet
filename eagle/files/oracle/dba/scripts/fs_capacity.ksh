#!/bin/ksh
# Code to run on the last day of the month
# Need to run every day and will only get past this code if the last day of month
#if [ $(/bin/date -d +1day +%d) -ne 1 ]
#then
# exit 1
#fi
mailrecipients="vlau@eagleaccess.com,rpatel@eagleinvsys.com,cmayo@eagleaccess.com"
maildbas="team_dba@eagleaccess.com"
threshold=85
if [ $# -ne 1 ]
then
 print "Must enter a valid Oracle SID for this host."
 exit 2
fi
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then 
 ORATAB=/var/opt/oracle/oratab
fi

orasid=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $orasid | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $orasid entry in $ORATAB file"
 exit 2 
fi
used_gb=$(df -P /${orasid}-0? | awk '{sum+=$3} END {printf "%6d\n",sum/1024/1024}')
allocated_gb=$(df -P /${orasid}-0? | awk '{sum+=$2} END {printf "%6d\n",sum/1024/1024}')
cap_value=$(print "scale=4; $used_gb / $allocated_gb * 100" | bc | awk -F. '{print $1}')
if [ $cap_value -gt $threshold ]
then
  print "The capacity of $orasid Storage at ${cap_value}% has exceeded threshold of ${threshold}%."
  print "Expand storage."
mailx -s "Alert: CBC Prod Storage at Threshold for Capacity" $maildbas<<EOF
$(print "The capacity of $orasid Storage at ${cap_value}% has exceeded threshold of ${threshold}%.")
$(print "Expand storage.")
EOF
fi
capacity=$(print "scale=4; $used_gb / $allocated_gb * 100" | bc | sed -e 's/0+*$//g' -e 's/0$//g' -e 's/$/% capacity/g')

print "Oracle Database: $orasid  Storage at $capacity at $(date)"
mailx -s "CBC Prod Storage Capacity" $mailrecipients<<EOF
$(print "Oracle Database: $orasid  Storage at $capacity at $(date)")
$(print "Used(GB): $used_gb")
$(print "Allocated(GB): $allocated_gb")
$(print "Threshold(%): $threshold")
EOF
exit 0
