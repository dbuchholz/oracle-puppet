#!/bin/ksh
typeset -RZ2 hour
ls -Rl /datadomain/pgh | grep '^\-r' > x
date_of_interest=$(date '+%b %d')
date_of_interest='Sep  1'
for hour in {0..23}
do
 sum_total=$(grep "$date_of_interest" x | grep " ${hour}:[0-9][0-9] " | awk '{sum+=$5} END {printf "%6d\n",sum/1000000}')
 print "$hour $sum_total"
done
rm -f x
exit 0
