#!/bin/ksh
# Script name: sync_archive_logs_oracle.ksh
#
# script to keep remote server archive logs in sync with local server ...
# This script should be run every 15 minutes
# You can put --delete after the ssh and before the --rsync to remove destination files not at source.
this_pid=$$
db=senprd2
source_arch=/senprd2-04/oradata/arch
target_arch=/senprd3-04/oradata/arch
remote_server=ppld1605

ps -ef | grep "sync_archive_logs_oracle\.ksh $db" |grep -v grep | grep -v $this_pid
if [ $? -eq 0 ]
then
 print "Currently syncing $db archive logs.  Aborting script."
 exit 1
fi

rsync -goptr -e ssh --delete --rsync-path=/usr/bin/rsync $source_arch/* $remote_server:$target_arch/

exit 0
