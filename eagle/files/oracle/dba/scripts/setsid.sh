#!/usr/bin/ksh
# *******************************************************************
# NAME:         setsid.sh
#
# AUTHOR:       Maureen Buotte
#
# PURPOSE:      This Utility will set the Oracle Environment variables
#               $ORACLE_HOME and $ORACLE_SID
#
# USAGE:        setsid.sh SID
# ********************************************************************
#
# Uncomment for debug
# set -x

NARG=$#
if [ $NARG != 1 ] ; then
        echo -e "setsid.sh FAIL: Incorrect number of arguments - must pass SID"
        export ORACLE_SID=''
        export ORACLE_HOME=''
        /usr/bin/showsid.sh
        return 1
fi
ORA_SID=$1
ORATAB=/etc/oratab
ORA_HOME=`sed  /#/d  ${ORATAB}|grep -i  ${ORA_SID}'\b'|awk -F ":"  '{print $2}'`
if [ x$ORA_HOME = 'x' ] ; then
        echo -e "setsid.sh FAIL:  Invalid SID\n"
        export ORACLE_SID=''
        export ORACLE_HOME=''
        /usr/local/bin/showsid.sh
    return 1
fi

export ORACLE_SID=${ORA_SID}
export ORACLE_HOME=${ORA_HOME}
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
export ORACLE_BASE=$HOME
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib

# Reset the original PATH
PATH=/usr/kerberos/bin:.:/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin
export PATH=$PATH:.:/usr/sbin:$ORACLE_HOME/bin:/usr/openwin/bin:$HOME/utils:$ORACLE_HOME/rdbms/admin:$ORACLE_HOME/network/admin:$ORACLE_HOME/OPatch
/usr/local/bin/showsid.sh
return 0
