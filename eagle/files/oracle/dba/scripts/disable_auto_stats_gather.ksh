#!/bin/ksh
#Script name: disable_auto_stats_gather.ksh
#Usage: disable_auto_stats_gather.ksh <Oracle SID>
#
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

if [ $# -ne 1 ]
then
 print "Must enter the Oracle SID of database you want to disable automatic stats gathering."
 print "Usage: $0 [Oracle SID]"
 exit 1
fi
export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$ORACLE_SID" ]
then
 print "Cannot find Instance $ORACLE_SID in $ORATAB"
 exit 2
fi

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF>$log_dir/hold_banner_in_oracle.txt
select * from v\$version;
EOF
(( ora_version=$(grep '^CORE' $log_dir/hold_banner_in_oracle.txt | awk '{print $2}' | awk -F. '{print $1}') ))
rm -f $log_dir/hold_banner_in_oracle.txt
if [ $ora_version = 11 ]
then 
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
BEGIN
  DBMS_AUTO_TASK_ADMIN.DISABLE(
    client_name => 'auto optimizer stats collection',
    operation => NULL,
    window_name => NULL);
END;
/
EOF
fi
if [ $ora_version = 10 ]
then 
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
exec dbms_scheduler.disable('GATHER_STATS_JOB');
EOF
fi
exit 0
