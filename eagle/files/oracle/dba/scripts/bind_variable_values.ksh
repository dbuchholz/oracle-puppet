#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null

read sql_id?"Enter SQL ID: "
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 trimspool on feedback off
col value_string format a20
col name format a5
col TIMESTAMP format a20
break on snap_id skip 1
select snap_id,to_char(LAST_CAPTURED,'MM-DD-YYYY HH24:MI:SS') as TIMESTAMP,NAME,VALUE_STRING,DATATYPE_STRING from dba_hist_sqlbind where SQL_ID='$sql_id' order by snap_id,TIMESTAMP,NAME;
EOF

exit 0
