#!/bin/ksh
# Script Name: KillPaceJobs.sh
# Usage: KillPaceJobs.sh [Oracle SID] [Database Username] [Database Password]  or User interactive
# Autor: Poornima Krishnan

function password_entry
{
stty -echo
read PW1?"Enter Your Password: "
stty echo
if [[ -z $PW1 ]]
then
 print "\n\nYou did not enter a password.  Aborting here..."
 exit 12
fi
}

function check_sid
{
 sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
 if [[ -z $sid_in_oratab ]]
 then
  print "There is no $ORACLE_SID entry in $ORATAB file"
  exit 13
 fi
}

## MAIN ##

clear
print "\n"
print "This program provides sql output to kill or mark pace jobs as complete"
read press_continue?"Press Enter to continue... or CTRL/C to exit "
print "\n\n"

if [ "$(dirname $0)" = "." ]
then
 script_dir=$(pwd)
else
 script_dir=$(dirname $0)
fi
log_dir=$script_dir/scripts

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH

if [ $# -lt 4 ]
then
 print "The list of Oracle SID are:\n"
 ps -ef | grep ora_smon_ | grep -v grep | awk '{print $NF}' | awk -F_ '{print $NF}'
 print "\n"
 read ORACLE_SID?"Enter Oracle SID: "
 if [[ -z $ORACLE_SID ]]
 then
  print "You did not enter an Oracle SID.  Aborting here..."
  exit 1
 fi
 export ORACLE_SID
 check_sid
 print "\n"
 read UN?"Enter Your Database User Name: "
 if [[ -z $UN ]]
 then
  print "You did not enter your database username.  Aborting here..."
  exit 2
 fi
 print "\nNext, you will enter your database password."
 print "For security, password will not be reflected to terminal.\n"
 password_entry
 print "\n"
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
connect ${UN}/$PW1
EOF
if [ $? -ne 0 ]
then
 print "Cannot log into $ORACLE_SID Database with $UN Account."
 print "The password may not be correct or the user does not exist."
 exit 3
fi
 clear


else 
 ORACLE_SID=$1
 UN=$2
 PW1=$3
 check_sid
 export ORACLE_SID
 . /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
connect ${UN}/$PW1
EOF
 if [ $? -ne 0 ]
 then
  print "Cannot log into $ORACLE_SID Database with $UN Account."
  print "The password may not be correct or the user does not exist."
  exit 5
 fi
fi

clear
now=$(date +'%Y%m%d_%H%M%S')


print "\n"
 print " press 1, if you want to kill specific pace events that are running \n"
 print " press 2, if you want to clear events that were killed by the clients still shows as running \n"
 print " press 3, if you want to kill all events that is running \n"
 print " press 4, if you want to kill all running events and any event that could possibly be in queue \n"

 read option?" "

if [ $option -eq 1 ]
 then
  $ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF
  set pages 200 linesize 200 feedback on echo on serveroutput on
  col CUSTOM_PROC_NAME format a150
  select instance,custom_proc_name,freq_type  from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P');
EOF
  print "\n"
  print " N, S -> Adhoc events; D -> scheduled Daily ; M -> scheduled Monthly ; C -> Continuous events"
  print "\n" 
  read job_instance?"Enter comma seperated list of jobs to be killed: "

  if [[ -n $job_instance ]]
   then
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print " Copy Paste the below stataments in to tracking program file and run"
   print " Ensure pace engines have been stopped before you run these statements"
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print "update pace_masterdbo.feed_results set status_flag='Y' where sched_def_inst in ($job_instance) and status_flag in ('P','p');"
   print "delete from pace_masterdbo.schedule_def_details where schedule_def_instance in ($job_instance);"
   print "delete from pace_masterdbo.schedule_def where freq_type in ('N','S') and instance in ($job_instance);"
   print "update pace_masterdbo.schedule_queue set status='E' where status='P' and pinstance in ($job_instance);"
   print "\n"
  else
  exit 1
 fi
fi

if [ $option -eq 2 ]
 then
 $ORACLE_HOME/bin/sqlplus -s ${UN}/${PW1}<<EOF
  set pages 200 linesize 200 feedback on echo on serveroutput on
  col CUSTOM_PROC_NAME format a150
  select instance,custom_proc_name,freq_type from pace_masterdbo.schedule_def where instance in (select sched_def_inst from pace_masterdbo.feed_results where status_flag='P' and sched_queue_inst in (select sq.instance from pace_masterdbo.schedule_queue sq where sq.status !='P'));
EOF
  print "\n"
  print " N, S -> Adhoc events; D -> scheduled Daily ; M -> scheduled Monthly ; C -> Continuous events"
  print "\n"
  read feed_res_inst?"Enter comma seperated list of events to be cleared: "

  if [[ -n $feed_res_inst ]]
  then
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print " Copy Paste the below stataments in to tracking program file and run"
   print " Ensure pace engines have been stopped before you run these statements"
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print "update pace_masterdbo.feed_results set status_flag='Y' where sched_def_inst in ($feed_res_inst) and status_flag in ('P','p');"
   print "\n"
  else
  exit 1
  fi
fi

if [ $option -eq 3 ]
 then
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print " Copy Paste the below stataments in to tracking program file and run"
   print " Ensure pace engines have been stopped before you run these statements"
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print "update pace_masterdbo.feed_results set status_flag = 'Y' where sched_def_inst in (select instance from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P'));"
   print "delete from pace_masterdbo.schedule_def_details where schedule_def_instance in (select instance from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P'));"
   print "delete from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P') and freq_type in('N','S');"
   print "update pace_masterdbo.schedule_queue  set status='E' where status='P';"
   print "\n"
fi

if [ $option -eq 4 ]
 then
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print " Copy Paste the below stataments in to tracking program file and run"
   print " Ensure pace engines have been stopped before you run these statements"
   print "\n"
   print "***********************************************************************************************"
   print "\n"
   print "update pace_masterdbo.feed_results set status_flag = 'Y' where sched_def_inst in (select instance from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P'));"
   print "delete from pace_masterdbo.schedule_def_details where schedule_def_instance in (select instance from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P'));"
   print "delete from pace_masterdbo.schedule_def where instance in (select pinstance from pace_masterdbo.schedule_queue where status='P') and freq_type in('N','S');"
   print "update pace_masterdbo.schedule_queue  set status='E' where status='P';"
   print "delete from pace_masterdbo.schedule_def_details where active_flag=0;"
   print "\n"
fi
#else
# print "Invalid Option"
# exit 2
#fi
