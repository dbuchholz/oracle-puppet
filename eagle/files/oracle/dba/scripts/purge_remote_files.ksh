#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
script_dir=/u01/app/oracle/local/dba/scripts
log_dir=$script_dir
read directory_specifier?"Enter the full path to where the files are located to be removed: "
if [[ ! -d $directory_specifier ]]
then
 print "The directory path you specified does not exist on this host."
 print "Aborting here."
 exit 1
fi
read file_specifier?"Enter file pattern(with wildcards) to remove on all database servers: "

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$log_dir/machine_information.txt
set pages 0 trimspool on feedback off echo off
WHENEVER SQLERROR EXIT FAILURE
select name,ip_address from inf_monitor.machines where name<>'hawaii';
EOF
if [ $? -ne 0 ]
then
 print "Could not get results from $ORACLE_SID Database."
 exit 1
fi

while read name ip_address
do
 print "$name"
 ssh -nq $ip_address "find ${directory_specifier}/* -prune -name \"$file_specifier\" -type f -exec rm -f {} \;"
done<$log_dir/machine_information.txt

rm -f $log_dir/machine_information.txt

exit 0
