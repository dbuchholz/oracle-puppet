#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
script_dir=/u01/app/oracle/local/dba/scripts
log_dir=$script_dir
username=$1

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$log_dir/machine_ip_sids.txt
set pages 0 trimspool on feedback off echo off
WHENEVER SQLERROR EXIT FAILURE
select m.name,m.ip_address,d.sid
from inf_monitor.machines m,inf_monitor.databases d
where m.instance=d.mac_instance
and d.dataguard='N'
and status='OPEN';
EOF
if [ $? -ne 0 ]
then
 print "Could not get results from $ORACLE_SID Database."
 exit 1
fi

rm -f $script_dir/where_user_exists.txt
while read name ip_address sid
do
 ssh -nq $ip_address ls $script_dir/db_user_exists.ksh > /dev/null 2>&1
 if [ $? -ne 0 ]
 then
  scp -nq $script_dir/db_user_exists.ksh ${ip_address}:$script_dir
 fi
 ssh -nq $ip_address $script_dir/db_user_exists.ksh $sid $username >> $script_dir/where_user_exists.txt
done<$log_dir/machine_ip_sids.txt

rm -f $log_dir/machine_ip_sids.txt
