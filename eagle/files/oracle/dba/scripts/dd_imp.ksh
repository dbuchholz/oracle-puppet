#!/bin/ksh
log_dir=/u01/app/oracle/local/dba/scripts/rmanlogs
ssh -nq 10.60.105.75 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh slidev1 > $log_dir/slidev1.log 2>&1 &
ssh -nq 10.60.105.75 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ixptst1 > $log_dir/ixptst1.log 2>&1 &
ssh -nq 10.60.105.45 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dgadev1 > $log_dir/dgadev1.log 2>&1 &
ssh -nq 10.60.105.92 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh eastst5 > $log_dir/eastst5.log 2>&1 &
ssh -nq 10.60.105.75 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh qiadev1 > $log_dir/qiadev1.log 2>&1 &
ssh -nq 10.60.105.107 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh pnedev1 > $log_dir/pnedev1.log 2>&1 &
ssh -nq 10.60.105.45 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh pritst1 > $log_dir/pritst1.log 2>&1 &
ssh -nq 10.60.105.35 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh pocdev1 > $log_dir/pocdev1.log 2>&1 &
ssh -nq 10.60.105.45 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tcftst1 > $log_dir/tcftst1.log 2>&1 &
ssh -nq 10.60.105.203 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mimtst1  > $log_dir/mimtst1.log 2>&1 &
wait
ssh -nq 10.60.105.1 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccdrsi > $log_dir/ccdrsi.log 2>&1 &
ssh -nq 10.60.105.107 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh v10tst1 > $log_dir/v10tst1.log 2>&1 &
ssh -nq 10.60.105.75 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfctst2 > $log_dir/mfctst2.log 2>&1 &
ssh -nq 10.60.105.92 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tbcdev2 > $log_dir/tbcdev2.log 2>&1 &
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh brwdev1 > $log_dir/brwdev1.log 2>&1 &
ssh -nq 10.60.105.69 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh agfdev1 > $log_dir/agfdev1.log 2>&1 &
ssh -nq 10.60.105.35 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh slitst1 > $log_dir/slitst1.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrkhfl > $log_dir/lrkhfl.log 2>&1 &
ssh -nq 10.60.105.75 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfcstg1 > $log_dir/mfcstg1.log 2>&1 &
ssh -nq 10.60.105.203 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh prptst2 > $log_dir/prptst2.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh brwtst1 > $log_dir/brwtst1.log 2>&1 &
ssh -nq 10.60.105.1 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ctsdev1 > $log_dir/ctsdev1.log 2>&1 &
wait
ssh -nq 10.60.105.7 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nktiim > $log_dir/nktiim.log 2>&1 &
ssh -nq 10.60.105.45 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dgatst1 > $log_dir/dgatst1.log 2>&1 &
ssh -nq 10.60.105.107 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh bactst1 > $log_dir/bactst1.log 2>&1 &
ssh -nq 10.60.105.69 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfcdev1 > $log_dir/mfcdev1.log 2>&1 &
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cdpdev1 > $log_dir/cdpdev1.log 2>&1
ssh -nq 10.60.105.7 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nktcci > $log_dir/nktcci.log 2>&1 &
ssh -nq 10.60.105.35 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nlitst1 > $log_dir/nlitst1.log 2>&1 &
ssh -nq 10.60.105.1 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccdiim > $log_dir/ccdiim.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrklim > $log_dir/lrklim.log 2>&1 &
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh isncca > $log_dir/isncca.log 2>&1
ssh -nq 10.60.105.69 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh terdev1 > $log_dir/terdev1.log 2>&1 &
wait
ssh -nq 10.60.105.203 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccmtst1 > $log_dir/ccmtst1.log 2>&1 &
ssh -nq 10.60.105.7 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nktwsp > $log_dir/nktwsp.log 2>&1 &
ssh -nq 10.60.105.107 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh xlctst1 > $log_dir/xlctst1.log 2>&1 &
ssh -nq 10.60.105.65 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh pimtst1 > $log_dir/pimtst1.log 2>&1 &
ssh -nq 10.60.105.1 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dacdev1 > $log_dir/dacdev1.log 2>&1 &
ssh -nq 10.60.105.35 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh hcitst1 > $log_dir/hcitst1.log 2>&1 &
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh isndmf > $log_dir/isndmf.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrkibc > $log_dir/lrkibc.log 2>&1 &
ssh -nq 10.60.105.65 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfcdev2 > $log_dir/mfcdev2.log 2>&1 &
ssh -nq 10.60.105.32 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh prptst1 > $log_dir/prptst1.log 2>&1 &
ssh -nq 10.60.105.7 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nktwspt > $log_dir/nktwspt.log 2>&1 &
ssh -nq 10.60.105.92 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dbsdev1 > $log_dir/dbsdev1.log 2>&1 &
wait
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dbstst1 > $log_dir/dbstst1.log 2>&1 &
ssh -nq 10.60.105.69 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfctst1 > $log_dir/mfctst1.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrkaci > $log_dir/lrkaci.log 2>&1 &
ssh -nq 10.60.105.65 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tbctst2 > $log_dir/tbctst2.log 2>&1 &
ssh -nq 10.60.105.203 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tcfdev1 > $log_dir/tcfdev1.log 2>&1 &
ssh -nq 10.60.105.69 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tertst1 > $log_dir/tertst1.log 2>&1 &
ssh -nq 10.60.105.32 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lbrdev1 > $log_dir/lbrdev1.log 2>&1 &
ssh -nq 10.60.105.92 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lcmtst1 > $log_dir/lcmtst1.log 2>&1 &
ssh -nq 10.60.105.20 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh isndmft > $log_dir/isndmft.log 2>&1 &
ssh -nq 10.60.105.65 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh agftst1 > $log_dir/agftst1.log 2>&1 &
wait
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrkine > $log_dir/lrkine.log 2>&1 &
ssh -nq 10.60.105.39 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lrkacid > $log_dir/lrkacid.log 2>&1 &
ssh -nq 10.60.105.164 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh brwdev2 > $log_dir/brwdev2.log 2>&1 &
ssh -nq 10.60.105.123 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh inftst1 > $log_dir/inftst1.log 2>&1 &
ssh -nq 10.60.105.164 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cgcdev1 > $log_dir/cgcdev1.log 2>&1 &
ssh -nq 10.60.105.92 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh xlcdev1 > $log_dir/xlcdev1.log 2>&1 &
exit 0
