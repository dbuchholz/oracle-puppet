#!/bin/ksh
# Script name:  fastcopy_files.ksh
#
clear
print "\n\nThis script will prompt for a source and target directory where you will fastcopy from and to."
print "You will choose the files in the source directory you want to copy based on their age."
print "You will choose the oldest file in the set and then choose the newest file in the set."
print "The newest file is optional.  If you do not choose the newest file, it will choose all files in"
print "the source directory up until the newest file in the directory.\n"

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=/tmp
read the_directory?"Enter the directory where files are located: "
print "Here is the directory contents sorted by the newest at the bottom."
print "Waiting 10 seconds so you can finish reading this."
sleep 10
ls -ltr $the_directory
print "You can use the scroll bar to see all of the files."
read file_newer?"Including this file and all files newer than it: "
read file_upto?"Including up to this file in the file set[Default is most recent file]: "
read target_dir?"Enter the target directory where you want to fastcopy the files: "
if [[ -n $file_upto ]]
then
 find $the_directory -type f \( -newer $the_directory/$file_newer -a ! -newer $the_directory/$file_upto \) > $log_dir/file_list.txt
else
 find $the_directory -type f -newer $the_directory/$file_newer > $log_dir/file_list.txt
fi
print "$the_directory/$file_newer" >> $log_dir/file_list.txt
awk -F/ '{print $NF}' $log_dir/file_list.txt > $log_dir/base_name_of_files.txt
sed "s|^|$target_dir/|g" $log_dir/base_name_of_files.txt > $log_dir/full_name_of_files.txt
paste -d" " $log_dir/file_list.txt $log_dir/full_name_of_files.txt > $log_dir/files_list.txt
rm -f $log_dir/file_list.txt
sed -e 's|/datadomain/|/backup/oracle/|g' \
    -e 's|/datadomain2/|/backup/archive/|g' \
    -e 's| /| destination /|g' \
    -e 's/^/ssh sysadmin\@eplb0001 filesys fastcopy source /g' $log_dir/files_list.txt > $log_dir/run_fastcopy_of_files.ksh
ksh -x $log_dir/run_fastcopy_of_files.ksh
rm -f $log_dir/base_name_of_files.txt $log_dir/full_name_of_files.txt $log_dir/files_list.txt
exit 0
