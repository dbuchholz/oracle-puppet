CREATE OR REPLACE
PACKAGE eaglemgr.eagle_util
IS
FUNCTION put_to_dbms_output
  RETURN VARCHAR2;
  -- **************************************************************************
FUNCTION awr_sql_id_history(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION awr_sql_id_session_history(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION info_line(
    in_line pls_integer)
  RETURN VARCHAR2;
  -- **************************************************************************
FUNCTION sql_id_info(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION sql_id_info_with_session(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION table_info(
    in_table_name sys.dba_tables.table_name%type)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION sql_id_info_line(
    in_line pls_integer)
  RETURN VARCHAR2;
-- **************************************************************************************************
FUNCTION do_object_summary(
    in_object_id sys.dba_objects.object_id%type,
    in_program_line# v$sql.program_line#%type)
  RETURN VARCHAR2;
  -- **************************************************************************
FUNCTION do_object_summary(
    in_sql_id v$sqlarea.sql_id%type)
  RETURN VARCHAR2;
  -- **************************************************************************
FUNCTION week_count(
    i_week pls_integer)
  RETURN pls_integer;
  -- **************************************************************************
FUNCTION index_columns(
    i_index_owner sys.dba_ind_columns.index_owner%type,
    i_index_name sys.dba_ind_columns.index_name%type)
  RETURN VARCHAR2;
  -- **************************************************************************
FUNCTION remove_constants(
    p_query IN VARCHAR2)
  RETURN VARCHAR2;
  -- **************************************************************************
PROCEDURE reset_info;
  -- **************************************************************************
PROCEDURE add_one(
    in_line VARCHAR2) ;
  -- **************************************************************************
PROCEDURE add_on(
    in_line VARCHAR2) ;
  -- **************************************************************************
PROCEDURE add_on(
    in_line VARCHAR2,
    in_size pls_integer) ;
  -- **************************************************************************
FUNCTION session_info(
    in_minutes NUMBER)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION session_info_awr(
    in_snapid_start NUMBER,
    in_snapid_end   NUMBER)
  RETURN NUMBER;
  -- **************************************************************************
FUNCTION session_info_awr(
    in_snapids NUMBER)
  RETURN NUMBER;
END eagle_util;
/
CREATE OR REPLACE
PACKAGE body eaglemgr.eagle_util
IS
  l_version VARCHAR2(24) := 'EAGLE_UTIL Version 8';
  l_current_count pls_integer;
  l_last_week pls_integer := 0;
  last_sql_id v$sql.sql_id%type;
  last_table_name sys.dba_tables.table_name%type;
  i_end pls_integer  := 0;
  i pls_integer      := 0;
  i_deep pls_integer := 0;
  l_program_id v$sql.program_id%type;
  l_program_line# v$sql.program_line#%type;
  l_byte_to_meg_conv NUMBER := 1048576;
  l_run_time DATE           := NULL;
  l_include_session BOOLEAN := FALSE;
  -- **************************************************************************************************
type sql_info_type
IS
  TABLE OF VARCHAR2(4000) INDEX BY binary_integer;
  sql_info_table sql_info_type;
type tables_type
IS
  TABLE OF pls_integer INDEX BY VARCHAR2(64) ;
  tables_tbl tables_type;
  -- **************************************************************************************************
  l_number_mask     VARCHAR2(24) := '9,999,999,999';
  l_number_mask_d3  VARCHAR2(24) := '9,999,999.999';
  l_date_mask       VARCHAR2(24) := 'MM/DD/YYYY HH24:MI:SS';
  l_time_mask       VARCHAR2(24) := 'HH24:MI:SS';
  l_date_mask_short VARCHAR2(24) := 'MM/DD/YYYY HH24:MI';
  l_time_mask_short VARCHAR2(24) := 'HH24:MI';
  -- **************************************************************************************************
  CURSOR sql_text_cur(owner_p IN all_source.owner%type, name_p IN all_source.name%type, type_p IN all_source.type%type, line_p IN
    all_source.type%type)
  IS
    SELECT line,
      SUBSTR(ltrim(REPLACE(REPLACE(REPLACE(REPLACE(text, '       ', ' '), '  ', ' '), chr(10), ''), chr(13), '')), 1, 254) text
    FROM sys.dba_source
    WHERE owner = owner_p
    AND name    = name_p
    AND type    = type_p
    AND line   >= line_p
    ORDER BY line;
  sql_text_rec sql_text_cur % rowtype;
  -- **************************************************************************************************
  CURSOR dba_objects_cur(object_id_p IN sys.dba_objects.object_id%type)
  IS
    SELECT object_id,
      owner,
      object_name,
      object_type
    FROM sys.dba_objects
    WHERE object_id = object_id_p;
  dba_objects_rec dba_objects_cur % rowtype;
  -- **************************************************************************************************
  CURSOR find_proc_cur(program_line#_p v$sql.program_line#%type)
  IS
    (SELECT program_line#_p program_line#,
      line proc_line,
      SUBSTR(text, 1, instr(text, ' ') - 1) sub_type,
      SUBSTR(ltrim(SUBSTR(text, instr(text, ' '))), 1, instr(ltrim(SUBSTR(text, instr(text, ' '))) || ' ', ' ') - 1) sub_name
    FROM
      (SELECT line,
        upper(ltrim(REPLACE(REPLACE(REPLACE(REPLACE(text, chr(13), ' '), chr(10), ' '), chr(9), ' '), '(', ' '))) text
      FROM sys.dba_source
      WHERE owner = dba_objects_rec.owner
      AND name    = dba_objects_rec.object_name
      AND type    = dba_objects_rec.object_type
      AND line    =
        (SELECT MAX(line)
        FROM sys.dba_source
        WHERE owner                          = dba_objects_rec.owner
        AND name                             = dba_objects_rec.object_name
        AND type                             = dba_objects_rec.object_type
        AND line                            <= program_line#_p
        AND(SUBSTR(upper(ltrim(text)), 1, 9) = 'PROCEDURE'
        OR SUBSTR(upper(ltrim(text)), 1, 8)  = 'FUNCTION')
        )
      )
    ) ;
    find_proc_rec find_proc_cur % rowtype;
    -- **************************************************************************************************
  type chars_by_number_type
IS
  TABLE OF VARCHAR2(400) INDEX BY pls_integer;
type chars_by_chars_type
IS
  TABLE OF VARCHAR2(400) INDEX BY VARCHAR2(400) ;
type number_by_chars_type
IS
  TABLE OF pls_integer INDEX BY VARCHAR2(400) ;
  -- **************************************************************************
  CURSOR sess_awr_cur(in_snapid_start NUMBER, in_snapid_end NUMBER)
  IS
    SELECT program,
      NVL(module, NVL(program, 'NULL')) id,
      sample_time,
      NVL(SUBSTR(action, 1, 2), 'TX') || ' ' || session_id session_id,
      module,
      action,
      NVL(sql_id, SUBSTR(event, 1, 13)) sql_id
    FROM sys.dba_hist_active_sess_history
    WHERE snap_id BETWEEN in_snapid_start AND in_snapid_end
    AND NVL(sql_id, SUBSTR(event, 1, 13)) IS NOT NULL
    ORDER BY 1,
      2,
      3,
      4;
  CURSOR sess_sample_cur(in_minutes NUMBER)
  IS
    SELECT program,
      NVL(module, NVL(program, 'NULL')) id,
      sample_time,
      NVL(SUBSTR(action, 1, 2), 'TX') || ' ' || session_id session_id,
      module,
      action,
      NVL(sql_id, SUBSTR(event, 1, 13)) sql_id
    FROM v$active_session_history
    WHERE sample_time                      > sysdate -(in_minutes /(24 * 60))
    AND NVL(sql_id, SUBSTR(event, 1, 13)) IS NOT NULL
    ORDER BY 1,
      2,
      3,
      4;
type sess_sample_type
IS
  TABLE OF sess_sample_cur%rowtype;
  sess_sample_tbl sess_sample_type;
  -- **************************************************************************
  -- **************************************************************************
PROCEDURE pop_dba_objects_rec(
    in_object_id sys.dba_objects.object_id%type,
    in_program_line# v$sql.program_line#%type)
IS
BEGIN
  IF(dba_objects_rec.object_id  IS NULL OR dba_objects_rec.object_id <> in_object_id OR in_object_id IS NULL) THEN
    dba_objects_rec.owner       := '';
    dba_objects_rec.object_name := '';
    dba_objects_rec.object_type := '';
    OPEN dba_objects_cur(in_object_id) ;
    FETCH dba_objects_cur
    INTO dba_objects_rec;
    CLOSE dba_objects_cur;
    dba_objects_rec.object_id   := in_object_id;
    find_proc_rec.program_line# := NULL;
    find_proc_rec.sub_type      := '';
    find_proc_rec.sub_name      := '';
  END IF;
  IF(dba_objects_rec.object_type    = 'PACKAGE BODY') THEN
    IF(find_proc_rec.program_line# IS NULL OR find_proc_rec.program_line# <> in_program_line#) THEN
      OPEN find_proc_cur(in_program_line#) ;
      FETCH find_proc_cur
      INTO find_proc_rec;
      CLOSE find_proc_cur;
      find_proc_rec.program_line# := in_program_line#;
    END IF;
  ELSE
    find_proc_rec.sub_type := dba_objects_rec.object_type;
    find_proc_rec.sub_name := dba_objects_rec.object_name;
  END IF;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION do_object_summary(
    in_object_id sys.dba_objects.object_id%type,
    in_program_line# v$sql.program_line#%type)
  RETURN VARCHAR2
IS
BEGIN
  pop_dba_objects_rec(in_object_id, in_program_line#) ;
  IF(dba_objects_rec.object_type = 'PACKAGE BODY') THEN
    RETURN(dba_objects_rec.owner || ' ' || dba_objects_rec.object_name || ' ' || find_proc_rec.sub_type || ' ' || find_proc_rec.sub_name) ;
  ELSE
    RETURN(dba_objects_rec.owner || ' ' || find_proc_rec.sub_type || ' ' || find_proc_rec.sub_name) ;
  END IF;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION do_object_summary(
    in_sql_id v$sqlarea.sql_id%type)
  RETURN VARCHAR2
IS
  l_object_id sys.dba_objects.object_id%type;
  l_program_line# v$sql.program_line#%type;
BEGIN
  SELECT NVL(MIN(program_id), 0),
    NVL(MIN(program_line#), 0)
  INTO l_object_id,
    l_program_line#
  FROM
    (SELECT
      /*+ NO_MERGE */
      program_id,
      program_line#
    FROM v$sql
    WHERE sql_id = in_sql_id
    AND ROWNUM   = 1
    ) ;
  RETURN(do_object_summary(l_object_id, l_program_line#)) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE reset_info
IS
BEGIN
  i_end := 0;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_one(
    in_line VARCHAR2)
IS
BEGIN
  i_end                   := i_end + 1;
  IF(i_end                 = 1) THEN
    sql_info_table(i_end) := ' ';
    i_end                 := i_end + 1;
    sql_info_table(i_end) := l_version;
    i_end                 := i_end + 1;
    sql_info_table(i_end) := '* AS OF ' || TO_CHAR(sysdate, l_date_mask) ;
    FOR rec               IN
    (SELECT instance_name,
      version,
      startup_time
    FROM v$instance
    )
    LOOP
      i_end                 := i_end + 1;
      sql_info_table(i_end) := 'Instance Name: ' || rec.instance_name || ', Oracle Version: ' || rec.version|| ', Startup Time: ' || to_char(rec.startup_time, l_date_mask);
    END LOOP;
    i_end                 := i_end + 1;
    sql_info_table(i_end) := null;
    FOR rec               IN
    (SELECT name || ': ' || value info
    FROM v$parameter
    WHERE name IN('optimizer_features_enable', 'compatible', 'cursor_sharing'))
    LOOP
      IF (sql_info_table(i_end) is null) THEN
        sql_info_table(i_end) := rec.info;
      ELSE
        sql_info_table(i_end) := sql_info_table(i_end) || ',    ' || rec.info;
      END IF;
    END LOOP;

    i_end := i_end + 1;
  END IF;
  sql_info_table(i_end) := rtrim(SUBSTR(in_line, 1, 3999)) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION index_columns(
    i_index_owner sys.dba_ind_columns.index_owner%type,
    i_index_name sys.dba_ind_columns.index_name%type)
  RETURN VARCHAR2
IS
  l_index VARCHAR2(3000) ;
BEGIN
  FOR rec IN
  (SELECT column_name
  FROM sys.dba_ind_columns
  WHERE index_owner = i_index_owner
  AND index_name    = i_index_name
  ORDER BY column_position
  )
  LOOP
    IF(SUBSTR(rec.column_name, 1, 4) = 'SYS_') THEN
      FOR rec_func                  IN
      (SELECT data_default
      FROM sys.dba_tab_cols
      WHERE column_name = rec.column_name
      AND owner         = i_index_owner
      )
      LOOP
        rec.column_name := rec_func.data_default;
      END LOOP;
    END IF;
    IF(l_index IS NULL) THEN
      l_index  := rec.column_name;
    ELSE
      l_index := l_index || ', ' || rec.column_name;
    END IF;
  END LOOP;
  RETURN(l_index) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_on(
    in_line VARCHAR2,
    in_size pls_integer)
IS
BEGIN
  sql_info_table(i_end) := sql_info_table(i_end) || SUBSTR(lpad(NVL(in_line, 'NULL'), in_size), 1, in_size) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_on(
    in_line VARCHAR2)
IS
BEGIN
  sql_info_table(i_end) := sql_info_table(i_end) || in_line;
END;
PROCEDURE add_table_info(
    in_owner sys.dba_tables.owner%type,
    in_table_name sys.dba_tables.table_name %type)
IS
BEGIN
  FOR rec_tab IN
  (SELECT DISTINCT owner,
    table_name,
    TO_CHAR(num_rows, l_number_mask) num_rows,
    TO_CHAR(last_analyzed, l_date_mask) last_analyzed,
    TEMPORARY
  FROM sys.dba_tables
  WHERE owner    = in_owner
  AND table_name = in_table_name
  )
  LOOP
    add_one('') ;
    add_one(rpad(rec_tab.owner || '.' || rec_tab.table_name, 45)) ;
    add_one('Last Analyzed: ' || NVL(rec_tab.last_analyzed, 'NULL')) ;
    add_one('Num Rows:      ' || ltrim(NVL(rec_tab.num_rows, 'NULL'))) ;
    IF(rec_tab.temporary = 'Y') THEN
      add_one('TEMPORARY TABLE') ;
    END IF;
    FOR rec_mod IN
    (SELECT inserts,
      updates,
      deletes,
      TIMESTAMP
    FROM sys.dba_tab_modifications
    WHERE table_owner = rec_tab.owner
    AND table_name    = rec_tab.table_name
    ORDER BY TIMESTAMP
    )
    LOOP
      add_one('Modifications (' || TO_CHAR(rec_mod.timestamp, l_date_mask) || ') - Inserts: ' || TO_CHAR(rec_mod.inserts, l_number_mask) ||
      '  Updates: ' || TO_CHAR(rec_mod.updates, l_number_mask) || '  Deletes: ' || TO_CHAR(rec_mod.deletes, l_number_mask)) ;
    END LOOP;
    add_one('') ;
    FOR rec_ind IN
    (SELECT owner,
      index_name,
      TO_CHAR(distinct_keys, l_number_mask) num_rows,
      TO_CHAR(last_analyzed, l_date_mask) last_analyzed,
      DECODE(uniqueness, 'UNIQUE', 'UNIQ', ' ') uniq
    FROM sys.dba_indexes
    WHERE owner    = rec_tab.owner
    AND table_name = rec_tab.table_name
    )
    LOOP
      add_one(rec_ind.num_rows || ' ' || rpad(rec_ind.index_name, 30) || ' ' || rpad(rec_ind.uniq, 4) || ' : ' || index_columns(
      rec_ind.owner, rec_ind.index_name)) ;
    END LOOP;
  END LOOP;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_view_info(
    in_preface VARCHAR2,
    in_owner sys.dba_objects.owner%type,
    in_object_name sys.dba_objects.object_name%type)
IS
BEGIN
  i_deep := i_deep + 1;
  add_one('') ;
  add_one(in_preface || in_owner || '.' || in_object_name) ;
  FOR rec_tab IN
  (SELECT owner,
    object_name
  FROM sys.dba_objects
  WHERE object_id IN
    (SELECT referenced_object_id
    FROM public_dependency
    WHERE object_id IN
      (SELECT object_id
      FROM sys.dba_objects
      WHERE owner     = in_owner
      AND object_name = in_object_name
      )
    )
  AND object_type = 'TABLE'
  )
  LOOP
    add_one('') ;
    add_one(in_owner || '.' || in_object_name || ' Refers To Table: ' || rec_tab.owner || '.' || rec_tab.object_name) ;
    add_table_info(rec_tab.owner, rec_tab.object_name) ;
  END LOOP;
  IF(i_deep       < 10) THEN
    FOR rec_view IN
    (SELECT owner,
      object_name
    FROM sys.dba_objects
    WHERE object_id IN
      (SELECT referenced_object_id
      FROM public_dependency
      WHERE object_id IN
        (SELECT object_id
        FROM sys.dba_objects
        WHERE owner     = in_owner
        AND object_name = in_object_name
        )
      )
    AND object_type = 'VIEW'
    )
    LOOP
      add_view_info(in_owner || '.' || in_object_name || ' Refers To View: ', rec_view.owner, rec_view.object_name) ;
    END LOOP;
  END IF;
  i_deep := i_deep - 1;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_table_list(
    in_owner sys.dba_tables.owner%type,
    in_table_name sys.dba_tables.table_name%type)
IS
  l_object_id sys.dba_objects.object_id%type;
BEGIN
  IF(NOT tables_tbl.exists(in_owner || '.' || in_table_name)) THEN
    SELECT object_id
    INTO l_object_id
    FROM sys.dba_objects
    WHERE owner                                   = in_owner
    AND object_name                               = in_table_name
    AND object_type                               = 'TABLE';
    tables_tbl(in_owner || '.' || in_table_name) := l_object_id;
  END IF;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_plan_id(
    in_id v$sql_plan.id%type,
    in_object_id v$sql_plan.object#%type,
    in_access VARCHAR2,
    in_filter VARCHAR2)
IS
BEGIN
  IF(in_access IS NOT NULL OR in_filter IS NOT NULL) THEN
    add_one(' ') ;
  END IF;
  FOR rec_o IN
  (SELECT owner,
    object_name,
    object_type
  FROM sys.dba_objects
  WHERE object_id = in_object_id
  )
  LOOP
    IF(rec_o.object_type = 'INDEX') THEN
      FOR rec_ind       IN
      (SELECT owner,
        index_name,
        table_name,
        TO_CHAR(num_rows, l_number_mask) num_rows,
        TO_CHAR(last_analyzed, l_date_mask) last_analyzed,
        DECODE(uniqueness, 'UNIQUE', ' UNIQ', ' ') uniq
      FROM sys.dba_indexes
      WHERE owner    = rec_o.owner
      AND index_name = rec_o.object_name
      )
      LOOP
        add_table_list(rec_ind.owner, rec_ind.table_name) ;
        IF(in_access IS NOT NULL OR in_filter IS NOT NULL) THEN
          add_one(lpad(in_id, 4) || ' - ' || rec_ind.owner || '.' || rec_ind.table_name || ' ' || rec_ind.index_name || rec_ind.uniq ||
          ' : ' || index_columns(rec_ind.owner, rec_ind.index_name)) ;
        END IF;
      END LOOP;
    END IF;
    IF(rec_o.object_type = 'TABLE') THEN
      FOR rec_tab       IN
      (SELECT DISTINCT owner,
        table_name,
        TO_CHAR(num_rows, l_number_mask) num_rows,
        TO_CHAR(last_analyzed, l_date_mask) last_analyzed,
        TEMPORARY
      FROM sys.dba_tables
      WHERE owner    = rec_o.owner
      AND table_name = rec_o.object_name
      )
      LOOP
        add_table_list(rec_tab.owner, rec_tab.table_name) ;
        IF(in_access IS NOT NULL OR in_filter IS NOT NULL) THEN
          add_one(lpad(in_id, 4) || ' - ' || rec_o.owner || '.' || rec_o.object_name || '  ' || rec_tab.num_rows) ;
        END IF;
      END LOOP;
    END IF;
    IF(in_access IS NOT NULL) THEN
      add_one(lpad(' ', 7) || 'access: ' || SUBSTR(in_access, 1, 220)) ;
    END IF;
    IF(in_filter IS NOT NULL) THEN
      add_one(lpad(' ', 7) || 'filter: ' || SUBSTR(in_filter, 1, 220)) ;
    END IF;
  END LOOP;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_plan(
    in_sql_id v$sql.sql_id%type,
    in_child_number v$sql.child_number%type)
IS
  l_plan_table sql_info_type;
  l_owner sys.dba_objects.owner%type;
  l_object_name sys.dba_objects.object_name%type;
  v_idx VARCHAR2(64) ;
BEGIN
  tables_tbl.delete;
  FOR rec IN
  (SELECT cpu_cost,
    io_cost,
    (SELECT MIN(optimizer) optimizer
    FROM v$sql_plan
    WHERE sql_id     = in_sql_id
    AND child_number = in_child_number
    AND optimizer   IS NOT NULL
    ) optimizer,
    id
  FROM v$sql_plan
  WHERE sql_id     = in_sql_id
  AND child_number = in_child_number
  AND id           =
    (SELECT MIN(id)
    FROM v$sql_plan
    WHERE sql_id     = in_sql_id
    AND child_number = in_child_number
    AND cpu_cost    IS NOT NULL
    )
  )
  LOOP
    add_one('') ;
    add_one('') ;
    add_one('Optimizer Mode: ' || rec.optimizer || '   CPU_COST: ' || TO_CHAR(rec.cpu_cost, l_number_mask) || '    IO_COST: ' || TO_CHAR(
    rec.io_cost, l_number_mask)) ;
  END LOOP;
  add_one('') ;
  FOR rec IN
  (SELECT t1.name,
  t1.isdefault,
  t1.value,
  (SELECT t2.value
  FROM v$parameter t2
  WHERE t1.name = t2.name (+)
  ) info
  FROM v$sql_optimizer_env t1
  WHERE t1.sql_id  = in_sql_id
  AND t1.child_number  = in_child_number
  AND t1.isdefault = 'NO')
  LOOP
    add_one(lpad(rec.name, 30)) ;
    add_on(rec.value, 30) ;
    add_on(rec.info, 30) ;
  END LOOP;
  add_one('') ;
  add_one('') ;
  SELECT * bulk collect
  INTO l_plan_table
  FROM TABLE(dbms_xplan.display_cursor(in_sql_id, in_child_number)) ;
  i      := 1;
  WHILE(i < l_plan_table.count AND instr(l_plan_table(i), 'Plan hash') < 1)
  LOOP
    i := i + 1;
  END LOOP;
  FOR indx IN i .. l_plan_table.count
  LOOP
    add_one(l_plan_table(indx)) ;
  END LOOP;
  FOR rec IN
  (SELECT id,
    object# object_id,
    access_predicates,
    filter_predicates
  FROM v$sql_plan
  WHERE sql_id     = in_sql_id
  AND child_number = in_child_number
  AND object#     IS NOT NULL
  ORDER BY id
  )
  LOOP
    add_plan_id(rec.id, rec.object_id, rec.access_predicates, rec.filter_predicates) ;
  END LOOP;
  add_one(' ') ;
  add_one('--------- Indexes --------') ;
  add_one('') ;
  v_idx       := tables_tbl.first;
  WHILE v_idx IS NOT NULL
  LOOP
    SELECT owner,
      object_name
    INTO l_owner,
      l_object_name
    FROM sys.dba_objects
    WHERE object_id = tables_tbl(v_idx) ;
    add_table_info(l_owner, l_object_name) ;
    v_idx := tables_tbl.next(v_idx) ;
  END LOOP display_loop;
  add_one(' ') ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE add_plan_awr(
    in_sql_id v$sql.sql_id%type,
    in_plan_hash_value v$sql.plan_hash_value%type)
IS
  l_plan_table sql_info_type;
BEGIN
  add_one(' ') ;
  add_one('(AWR Plan)') ;
  add_one(' ') ;
  FOR rec IN
  (SELECT cpu_cost,
    io_cost,
    (SELECT MIN(optimizer) optimizer
    FROM sys.dba_hist_sql_plan
    WHERE sql_id        = in_sql_id
    AND plan_hash_value = in_plan_hash_value
    AND optimizer      IS NOT NULL
    ) optimizer,
    id
  FROM sys.dba_hist_sql_plan
  WHERE sql_id        = in_sql_id
  AND plan_hash_value = in_plan_hash_value
  AND id              =
    (SELECT MIN(id)
    FROM sys.dba_hist_sql_plan
    WHERE sql_id        = in_sql_id
    AND plan_hash_value = in_plan_hash_value
    AND cpu_cost       IS NOT NULL
    )
  )
  LOOP
    add_one('Optimizer Mode: ' || rec.optimizer || '   CPU_COST: ' || TO_CHAR(rec.cpu_cost, l_number_mask) || '    IO_COST: ' || TO_CHAR(
    rec.io_cost, l_number_mask)) ;
  END LOOP;
  i := 1;
  SELECT * bulk collect
  INTO l_plan_table
  FROM TABLE(dbms_xplan.display_awr(in_sql_id, in_plan_hash_value)) ;
  IF(in_plan_hash_value IS NOT NULL) THEN
    WHILE(i              < l_plan_table.count AND instr(l_plan_table(i), 'Plan hash') < 1)
    LOOP
      i := i + 1;
    END LOOP;
  END IF;
  FOR indx IN i .. l_plan_table.count
  LOOP
    add_one(l_plan_table(indx)) ;
  END LOOP;
  add_one(' ') ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE awr_history(
    in_sql_id    VARCHAR2,
    display_type VARCHAR2)
IS
BEGIN
  l_current_count := 20;
  add_one('') ;
  IF(display_type = 'RUN1') THEN
    add_one('--------- AWR History --------') ;
  ELSE
    add_one('--------- AWR History Per Execution --------') ;
  END IF;
  FOR rec_awr IN
  (SELECT sn.begin_interval_time awr_begin,
    sn.end_interval_time awr_end,
    sq.executions_delta executions,
    ROUND(sq.cpu_time_delta / 1000000) cpu_time,
    ROUND(sq.elapsed_time_delta / 1000000) elapsed_time,
    sq.buffer_gets_delta buffer_gets,
    sq.disk_reads_delta disk_reads,
    sq.rows_processed_delta rows_processed,
    DECODE(NVL(sq.executions_delta, 0), 0, 1, sq.executions_delta) executions_1,
    sq.plan_hash_value
  FROM sys.dba_hist_sqlstat sq,
    sys.dba_hist_snapshot sn
  WHERE sq.sql_id           = in_sql_id
  AND sn.snap_id            = sq.snap_id
  AND sn.instance_number    = sq.instance_number
  AND sq.elapsed_time_delta > 0
  ORDER BY sn.begin_interval_time
  )
  LOOP
    l_current_count   := l_current_count + 1;
    IF(l_current_count > 20) THEN
      l_current_count := 1;
      add_one('') ;
      add_one(lpad('Beginning', 17)) ;
      add_on('Ending', 7) ;
      add_on('Executions', 15) ;
      add_on('Elpsd (sec)', 15) ;
      add_on('CPU (sec)', 15) ;
      add_on('Buffer Gets', 15) ;
      add_on('Disk Reads', 15) ;
      add_on('Rows Processed', 15) ;
      add_on('Plan Value', 15) ;
    END IF;
    IF(display_type = 'RUN1') THEN
      add_one(lpad(TO_CHAR(rec_awr.awr_begin, l_date_mask_short), 17)) ;
      add_on(TO_CHAR(rec_awr.awr_end, l_time_mask_short), 7) ;
      add_on(TO_CHAR(rec_awr.executions, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.elapsed_time, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.cpu_time, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.buffer_gets, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.disk_reads, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.rows_processed, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.plan_hash_value), 15) ;
    ELSE
      add_one(lpad(TO_CHAR(rec_awr.awr_begin, l_date_mask_short), 17)) ;
      add_on(TO_CHAR(rec_awr.awr_end, l_time_mask_short), 7) ;
      add_on(TO_CHAR(rec_awr.executions, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.elapsed_time / rec_awr.executions_1, l_number_mask_d3), 15) ;
      add_on(TO_CHAR(rec_awr.cpu_time / rec_awr.executions_1, l_number_mask_d3), 15) ;
      add_on(TO_CHAR(rec_awr.buffer_gets / rec_awr.executions_1, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.disk_reads / rec_awr.executions_1, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.rows_processed / rec_awr.executions_1, l_number_mask), 15) ;
      add_on(TO_CHAR(rec_awr.plan_hash_value), 15) ;
    END IF;
  END LOOP;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION info_line(
    in_line pls_integer)
  RETURN VARCHAR2
IS
BEGIN
  IF(in_line <= i_end) THEN
    RETURN(sql_info_table(in_line)) ;
  END IF;
  last_sql_id := NULL;
  i_end       := 0;
  RETURN(' ') ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION sql_id_info_line(
    in_line pls_integer)
  RETURN VARCHAR2
IS
BEGIN
  RETURN(info_line(in_line)) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION table_info(
    in_table_name sys.dba_tables.table_name%type)
  RETURN NUMBER
IS
BEGIN
  IF(last_table_name IS NULL OR last_table_name <> in_table_name) THEN
    i_end            := 0;
    last_table_name  := in_table_name;
    FOR rec          IN
    (SELECT owner,
      table_name
    FROM sys.dba_tables
    WHERE table_name = upper(in_table_name)
    )
    LOOP
      add_table_info(rec.owner, rec.table_name) ;
    END LOOP;
  END IF;
  RETURN(i_end + 1) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
PROCEDURE awr_sql_id_session_history_p(
    in_sql_id v$sql.sql_id%type)
IS
  CURSOR sess_awr_sql_id_cur(in_sql_id VARCHAR2)
  IS
    SELECT sample_time,
      thred,
      COUNT( *) cnt
    FROM
      (SELECT to_date(TO_CHAR(sample_time, 'MM/DD/YYYY HH24'), 'MM/DD/YYYY HH24') sample_time,
        SUBSTR(NVL(module, program), 1, 30) || ': ' || NVL(SUBSTR(action, 1, 2), 'TX') thred
      FROM sys.dba_hist_active_sess_history
      WHERE sql_id = in_sql_id
      ) t1
  GROUP BY sample_time,
    thred
  ORDER BY 1,
    2;
type sess_awr_sql_id_type
IS
  TABLE OF sess_awr_sql_id_cur%rowtype;
  sess_awr_sql_id_tbl sess_awr_sql_id_type;
  rec pls_integer;
  rec_save pls_integer;
  my_sessions_sql chars_by_chars_type;
  l_sample_time DATE;
  l_line    VARCHAR2(4000) ;
  l_session VARCHAR2(4000) ;
BEGIN
  OPEN sess_awr_sql_id_cur(in_sql_id) ;
  FETCH sess_awr_sql_id_cur bulk collect
  INTO sess_awr_sql_id_tbl;
  CLOSE sess_awr_sql_id_cur;
  --
  add_one(' ') ;
  IF(sess_awr_sql_id_tbl.last() > 0) THEN
    l_current_count            := 20;
    add_one('ACTIVE SESSION AWR FOR SQL: ' || in_sql_id) ;
    add_one(' ') ;
    my_sessions_sql.delete;
    --
    FOR rec IN 1 .. sess_awr_sql_id_tbl.last()
    LOOP
      IF(NOT my_sessions_sql.exists(sess_awr_sql_id_tbl(rec) .thred)) THEN
        my_sessions_sql(sess_awr_sql_id_tbl(rec) .thred) := ' ';
      END IF;
    END LOOP;
    --
    rec       := 1;
    WHILE rec <= sess_awr_sql_id_tbl.last()
    LOOP
      l_current_count   := l_current_count + 1;
      IF(l_current_count > 20) THEN
        l_current_count := 1;
        add_one('') ;
        l_line          := lpad(' ', LENGTH(l_date_mask_short)) ;
        l_session       := my_sessions_sql.first;
        WHILE l_session IS NOT NULL
        LOOP
          l_line    := l_line || l_session || '   ';
          l_session := my_sessions_sql.next(l_session) ;
        END LOOP;
        --
        add_one(l_line) ;
      END IF;
      --
      l_sample_time := sess_awr_sql_id_tbl(rec) .sample_time;
      WHILE rec     <= sess_awr_sql_id_tbl.last() AND l_sample_time = sess_awr_sql_id_tbl(rec) .sample_time
      LOOP
        my_sessions_sql(sess_awr_sql_id_tbl(rec) .thred) := sess_awr_sql_id_tbl(rec) .cnt;
        --
        rec := rec + 1;
      END LOOP;
      --
      l_line          := TO_CHAR(l_sample_time, l_date_mask_short) || '  ';
      l_session       := my_sessions_sql.first;
      WHILE l_session IS NOT NULL
      LOOP
        l_line                     := l_line || lpad(my_sessions_sql(l_session), LENGTH(l_session)) || '   ' ;
        my_sessions_sql(l_session) := ' ';
        l_session                  := my_sessions_sql.next(l_session) ;
      END LOOP;
      add_one(l_line) ;
    END LOOP;
  ELSE
    add_one('NO SESSION AWR HISTORY FOR SQL') ;
  END IF;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION awr_sql_id_session_history(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER
IS
BEGIN
  awr_sql_id_session_history_p(in_sql_id) ;
  RETURN(i_end + 1) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION sql_id_info(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER
IS
  i_cnt pls_integer := 0;
  i_found BOOLEAN   := false;
  l_child_number v$sql.child_number%type;
BEGIN
  IF(last_sql_id IS NULL OR last_sql_id <> in_sql_id) THEN
    i_end        := 0;
    BEGIN
      last_sql_id := in_sql_id;
    EXCEPTION
    WHEN OTHERS THEN
      add_one('') ;
      add_one('Error - Bad SQL_ID (' || in_sql_id || ')') ;
      RETURN(i_end + 1) ;
    END;
    FOR rec IN
    (WITH summary_t AS
    (SELECT sql_id,
      COUNT( *) child_count,
      MAX(child_number) child_number,
      MAX(program_id) program_id,
      MAX(program_line#) program_line#,
      SUM(fetches) fetches,
      SUM(executions) executions,
      SUM(cpu_time) / 1000000 cpu_time,
      SUM(elapsed_time) / 1000000 elapsed_time,
      SUM(concurrency_wait_time) / 1000000 concurrency_wait_time,
      SUM(user_io_wait_time) / 1000000 user_io_wait_time,
      SUM(buffer_gets) buffer_gets,
      SUM(rows_processed) rows_processed,
      DECODE(SUM(executions), 0, 1, SUM(executions)) ok_executions,
      rpad(sql_text, 4000, ' ') sql_text,
      MAX(last_active_time) last_active_time,
      ROUND(SUM(sharable_mem) / l_byte_to_meg_conv, 4) sharable_mem,
      ROUND(SUM(persistent_mem) / l_byte_to_meg_conv, 4) persistent_mem,
      ROUND(SUM(runtime_mem) / l_byte_to_meg_conv, 4) runtime_mem
    FROM v$sql
    WHERE sql_id = in_sql_id
    GROUP BY sql_id,
      rpad(sql_text, 4000, ' ')
    )
  SELECT s.sql_id,
    s.child_count,
    s.fetches,
    s.executions,
    s.program_id,
    s.program_line#,
    ROUND(s.cpu_time, 2) cpu_time,
    ROUND(s.elapsed_time, 2) elapsed_time,
    ROUND(s.concurrency_wait_time, 2) concurrency_wait_time,
    ROUND(s.user_io_wait_time, 2) user_io_wait_time,
    s.buffer_gets,
    s.rows_processed,
    ROUND(s.cpu_time / s.ok_executions, 4) cpu_x,
    ROUND(s.elapsed_time / s.ok_executions, 4) elp_x,
    ROUND(s.buffer_gets / s.ok_executions, 0) buf_x,
    ROUND(s.rows_processed / s.ok_executions, 4) rows_x,
    ROUND(s.rows_processed / s.elapsed_time, 2) rows_s,
    sharable_mem,
    persistent_mem,
    runtime_mem,
    TO_CHAR(s.last_active_time, l_date_mask) last_active_time,
    s.child_number,
    do.owner,
    do.object_name,
    do.object_type,
    sql_text
  FROM summary_t s,
    sys.dba_objects DO
  WHERE do.object_id(+) = s.program_id
    )
    LOOP
      i_found := true;
      add_one('') ;
      add_one('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SQL_ID: ' || rec.sql_id) ;
      add_one('') ;
      FOR rec_ses IN
      (SELECT sid,
        SUBSTR(client_info, 1, instr(client_info, ',') - 1) event,
        SUBSTR(client_info, instr(client_info, ',') + 1) event_id,
        NVL(module, program) module,
        action
      FROM v$session
      WHERE sql_id = in_sql_id
      )
      LOOP
        add_one('Current SQL SID (' || rec_ses.sid || ') (' || rec_ses.module || ')  (' || rec_ses.action ||
        ') Client Info Event / Event ID: ' || rec_ses.event || ' / ' || rec_ses.event_id) ;
      END LOOP;
      FOR rec_ses IN
      (SELECT sid,
        SUBSTR(client_info, 1, instr(client_info, ',') - 1) event,
        SUBSTR(client_info, instr(client_info, ',') + 1) event_id,
        NVL(module, program) module,
        action
      FROM v$session
      WHERE prev_sql_id                    = in_sql_id
      AND NVL(sql_id, in_sql_id || 'xxx') <> in_sql_id
      )
      LOOP
        add_one('Previous SQL SID (' || rec_ses.sid || ') (' || rec_ses.module || ')  (' || rec_ses.action ||
        ') Client Info Event / Event ID: ' || rec_ses.event || ' / ' || rec_ses.event_id) ;
      END LOOP;
      add_one('') ;
      add_one('SQL_TEXT:') ;
      add_one('') ;
      FOR rec IN
      (SELECT sql_fulltext,
        TO_CHAR(dbms_lob.substr(sql_fulltext, 4000, 1)) sql_text,
        dbms_lob.getlength(sql_fulltext) text_len,
        0 char_last,
        0 char_at,
        140 max_char
      FROM v$sql
      WHERE sql_id = in_sql_id
      AND rownum   < 2
      )
      LOOP
        WHILE rec.char_last < rec.text_len
        LOOP
          IF(rec.char_last + rec.max_char >= rec.text_len) THEN
            rec.char_at                   := rec.max_char;
          ELSE
            rec.sql_text := dbms_lob.substr(rec.sql_fulltext, rec.max_char, rec.char_last + 1) ;
            SELECT DECODE(MAX(bval), 0, rec.max_char, MAX(bval))
            INTO rec.char_at
            FROM
              (SELECT instr(rec.sql_text, ' ', - 1, 1) bval
              FROM dual
              UNION ALL
              SELECT instr(rec.sql_text, ',', - 1, 1) bval
              FROM dual
              ) ;
          END IF;
          rec.sql_text  := dbms_lob.substr(rec.sql_fulltext, rec.char_at, rec.char_last + 1) ;
          rec.char_last := rec.char_last + rec.char_at;
          add_one(REPLACE(REPLACE(rec.sql_text, chr(10), ' '), chr(13), ' ')) ;
        END LOOP;
      END LOOP;
      add_one('') ;
      add_one('Number Of Children: ' || rec.child_count) ;
      IF(rec.child_count > 1) THEN
        add_one('') ;
        add_one('--- SUMMARY ---- ') ;
      END IF;
      add_one('') ;
      add_one('Last Active: ' || rec.last_active_time) ;
      add_one('') ;
      add_one(lpad('Executions: ', 20) || TO_CHAR(rec.executions, l_number_mask)) ;
      add_one(lpad('Elapsed (sec): ', 20) || TO_CHAR(rec.elapsed_time, l_number_mask)) ;
      add_one(lpad('CPU (sec): ', 20) || TO_CHAR(rec.cpu_time, l_number_mask)) ;
      add_one(lpad('USER IO Wait (sec): ', 20) || TO_CHAR(rec.user_io_wait_time, l_number_mask)) ;
      add_one(lpad('CNCURCY Wait (sec): ', 20) || TO_CHAR(rec.concurrency_wait_time, l_number_mask)) ;
      add_one(lpad('Buffer Gets: ', 20) || TO_CHAR(rec.buffer_gets, l_number_mask)) ;
      add_one(lpad('Rows Processed: ', 20) || TO_CHAR(rec.rows_processed, l_number_mask)) ;
      add_one('') ;
      add_one(lpad('Elapsed Per Execution (sec): ', 32) || rec.elp_x) ;
      add_one(lpad('CPU Per Execution (sec): ', 32) || rec.cpu_x) ;
      add_one(lpad('Buffer Gets Per Execution: ', 32) || rec.buf_x) ;
      add_one(lpad('Rows Processed Per Execution: ', 32) || rec.rows_x) ;
      add_one(lpad('Rows Processed Per Second: ', 32) || rec.rows_s) ;
      add_one('') ;
      add_one(lpad('Sharable Memory (Mega Bytes): ', 32) || rec.sharable_mem) ;
      add_one(lpad('Persistent Memory (Mega Bytes): ', 32) || rec.persistent_mem) ;
      add_one(lpad('Run Time Memory (Mega Bytes): ', 32) || rec.runtime_mem) ;
      add_one('') ;
      add_one(do_object_summary(rec.program_id, rec.program_line#) || ' - ' || NVL(TO_CHAR(rec.program_line#), ' ')) ;
      add_one('') ;
      IF(rec.owner IS NOT NULL) THEN
        OPEN sql_text_cur(rec.owner, rec.object_name, rec.object_type, rec.program_line#) ;
        FETCH sql_text_cur
        INTO sql_text_rec;
        i_cnt      := 0;
        WHILE(i_cnt < 100 AND sql_text_cur % found)
        LOOP
          IF(trim(LENGTH(sql_text_rec.text))    > 0) THEN
            IF(instr(sql_text_rec.text, 'LOOP') > 1) THEN
              i_cnt                            := 100;
            ELSE
              add_one('(' || TO_CHAR(sql_text_rec.line) || ') ' || sql_text_rec.text) ;
            END IF;
            IF(instr(sql_text_rec.text, ';') > 1) THEN
              i_cnt                         := 100;
            END IF;
          END IF;
          FETCH sql_text_cur
          INTO sql_text_rec;
          i_cnt := i_cnt + 1;
        END LOOP;
        CLOSE sql_text_cur;
      END IF;
      add_one('') ;
      IF(rec.child_count > 1) THEN
        FOR rec_child   IN
        (SELECT s.sql_id,
          s.plan_hash_value,
          s.parsing_schema_name,
          COUNT( *) child_count,
          MAX(s.child_number) child_number,
          SUM(s.fetches) fetches,
          SUM(s.executions) executions,
          ROUND((SUM(s.cpu_time) / 1000000), 2) cpu_time,
          ROUND((SUM(s.elapsed_time) / 1000000), 2) elapsed_time,
          SUM(s.buffer_gets) buffer_gets,
          SUM(s.rows_processed) rows_processed,
          ROUND((SUM(s.cpu_time) / 1000000) / DECODE(SUM(s.executions), 0, 1, SUM(s.executions)), 4) cpu_x,
          ROUND((SUM(s.elapsed_time) / 1000000) / DECODE(SUM(s.executions), 0, 1, SUM(s.executions)), 4) elp_x,
          ROUND(SUM(s.buffer_gets) / DECODE(SUM(s.executions), 0, 1, SUM(s.executions)), 0) buf_x,
          ROUND(SUM(s.rows_processed) / DECODE(SUM(s.executions), 0, 1, SUM(s.executions)), 4) rows_x,
          TO_CHAR(MAX(s.last_active_time), l_date_mask) last_active_time
        FROM v$sql s
        WHERE sql_id = in_sql_id
        GROUP BY s.sql_id,
          s.plan_hash_value,
          s.parsing_schema_name
        ORDER BY plan_hash_value
        )
        LOOP
          add_one('') ;
          IF(rec_child.child_count > 1) THEN
            add_one('Number Of Children For This Plan: ' || TO_CHAR(rec_child.child_count)) ;
            add_one('Max Child Number: ' || TO_CHAR(rec_child.child_number)) ;
          ELSE
            add_one('Child Number: ' || TO_CHAR(rec_child.child_number)) ;
          END IF;
          add_one('Schema: ' || rec_child.parsing_schema_name) ;
          add_one('') ;
          add_one('Last Active: ' || rec_child.last_active_time) ;
          add_one('') ;
          add_one(lpad('Executions: ', 20) || TO_CHAR(rec_child.executions, l_number_mask)) ;
          add_one(lpad('Elapsed (sec): ', 20) || TO_CHAR(rec_child.elapsed_time, l_number_mask)) ;
          add_one(lpad('CPU (sec): ', 20) || TO_CHAR(rec_child.cpu_time, l_number_mask)) ;
          add_one(lpad('Buffer Gets: ', 20) || TO_CHAR(rec_child.buffer_gets, l_number_mask)) ;
          add_one(lpad('Rows Processed: ', 20) || TO_CHAR(rec_child.rows_processed, l_number_mask)) ;
          IF(rec_child.executions > 0) THEN
            add_one('') ;
            add_one(lpad('Elapsed Per Execution (sec): ', 32) || rec_child.elp_x) ;
            add_one(lpad('CPU Per Execution (sec): ', 32) || rec_child.cpu_x) ;
            add_one(lpad('Buffer Gets Per Execution: ', 32) || rec_child.buf_x) ;
            add_one(lpad('Rows Processed Per Execution: ', 32) || rec_child.rows_x) ;
          END IF;
          add_plan(in_sql_id, rec_child.child_number) ;
          IF(rec.child_count < 20) THEN
            l_child_number  := - 1;
            FOR v           IN
            (SELECT *
            FROM
              (SELECT sql_id,
                child_number,
                executions,
                last_active_time,
                hash_value,
                child_address
              FROM v$sql
              WHERE sql_id        = in_sql_id
              AND plan_hash_value = rec_child.plan_hash_value
              ORDER BY sql_id,
                child_number
              )
            WHERE rownum < 21
            )
            LOOP
              FOR rec_bind IN
              (SELECT DISTINCT vc.sql_id,
                vc.child_number,
                vc.name,
                vc.value_string,
                vc.position,
                TO_CHAR(v.last_active_time, l_date_mask) last_active_time,
                v.executions,
                datatype_string
              FROM v$sql_bind_capture vc
              WHERE vc.sql_id      = v.sql_id
              AND vc.child_number  = v.child_number
              AND vc.hash_value    = v.hash_value
              AND vc.child_address = v.child_address
              AND vc.was_captured  = 'YES'
              ORDER BY vc.child_number,
                vc.position
              )
              LOOP
                IF(l_child_number <> rec_bind.child_number) THEN
                  add_one('') ;
                  add_one('Binds For Child Number: ' || TO_CHAR(rec_bind.child_number) || '   Last Active: ' || rec_bind.last_active_time
                  || '   Executions: ' || rec_bind.executions) ;
                  add_one('') ;
                  l_child_number := rec_bind.child_number;
                END IF;
                add_one(TO_CHAR(rec_bind.name) || ' (' || rec_bind.datatype_string || ') : ' || rec_bind.value_string) ;
              END LOOP;
            END LOOP;
          END IF;
        END LOOP;
      ELSE
        add_plan(in_sql_id, rec.child_number) ;
      END IF;
      FOR rec_plan IN
      (SELECT DISTINCT plan_hash_value
      FROM sys.dba_hist_sqlstat
      WHERE sql_id = in_sql_id
      MINUS
      SELECT DISTINCT plan_hash_value
      FROM v$sql
      WHERE sql_id = in_sql_id
      )
      LOOP
        add_plan_awr(in_sql_id, rec_plan.plan_hash_value) ;
      END LOOP;
    END LOOP;
    SELECT COUNT( *)
    INTO i_cnt
    FROM sys.dba_hist_sqlstat
    WHERE sql_id = in_sql_id;
    IF(i_cnt     > 0) THEN
      IF(NOT i_found) THEN
        add_one('') ;
        add_one('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SQL_ID: ' || in_sql_id) ;
        FOR rec_plan IN
        (SELECT DISTINCT plan_hash_value
        FROM sys.dba_hist_sqlstat
        WHERE sql_id = in_sql_id
        )
        LOOP
          add_plan_awr(in_sql_id, rec_plan.plan_hash_value) ;
        END LOOP;
        i_found := true;
      END IF;
    END IF;
    IF(i_cnt > 0) THEN
      awr_history(in_sql_id, 'RUN1') ;
      awr_history(in_sql_id, 'RUN2') ;
      IF(l_include_session) THEN
        awr_sql_id_session_history_p(in_sql_id) ;
        l_include_session := FALSE;
      END IF;
      --
      i_found := true;
    END IF;
    IF(NOT i_found) THEN
      add_one('') ;
      add_one('SQL_ID ' || in_sql_id || ' Not Found In V$SQL') ;
    END IF;
  END IF;
  RETURN(i_end + 1) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION sql_id_info_with_session(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER
IS
BEGIN
  l_include_session := TRUE;
  RETURN(sql_id_info(in_sql_id)) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION week_count(
    i_week pls_integer)
  RETURN pls_integer
IS
BEGIN
  IF(l_last_week     = i_week) THEN
    l_current_count := l_current_count + 1;
  ELSE
    l_current_count := 1;
    l_last_week     := i_week;
  END IF;
  RETURN(l_current_count) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION remove_constants(
    p_query IN VARCHAR2)
  RETURN VARCHAR2
AS
  l_query LONG;
  l_char      VARCHAR2(1) ;
  l_in_quotes BOOLEAN DEFAULT false;
BEGIN
  FOR i IN 1 .. LENGTH(p_query)
  LOOP
    l_char        := SUBSTR(p_query, i, 1) ;
    IF(l_char      = '''' AND l_in_quotes) THEN
      l_in_quotes := false;
    elsif(l_char   = '''' AND NOT l_in_quotes) THEN
      l_in_quotes := true;
      l_query     := l_query || '''#';
    END IF;
    IF(NOT l_in_quotes) THEN
      l_query := l_query || l_char;
    END IF;
  END LOOP;
  l_query := TRANSLATE(l_query, '0123456789', '@@@@@@@@@@') ;
  FOR i   IN 0 .. 8
  LOOP
    l_query := REPLACE(l_query, lpad('@', 10 - i, '@'), '@') ;
    l_query := REPLACE(l_query, lpad(' ', 10 - i, ' '), ' ') ;
  END LOOP;
  RETURN upper(l_query) ;
END;
-- ****************************************************************************
-- ****************************************************************************
FUNCTION session_info_sub
  RETURN NUMBER
IS
  my_sessions_sql chars_by_chars_type;
  my_sqls number_by_chars_type;
  --
  my_sql_number chars_by_chars_type;
  l_sql_number pls_integer;
  --
  l_char_list VARCHAR2(4000) ;
  --
  l_sessions_cnt NUMBER;
  --
  l_session VARCHAR2(4000) ;
  --  L_SESSION PLS_INTEGER;
  l_sql_id VARCHAR2(30) ;
  l_sample_time TIMESTAMP;
  l_id VARCHAR2(4000) ;
  rec pls_integer;
  rec_save pls_integer;
BEGIN
  l_run_time := sysdate;
  rec        := 1;
  WHILE rec  <= sess_sample_tbl.last()
  LOOP
    add_one(' ') ;
    add_one('**** ' || NVL(sess_sample_tbl(rec) .program, ' ') || '  **** ' || sess_sample_tbl(rec) .id) ;
    add_one(' ') ;
    l_id := sess_sample_tbl(rec) .id;
    my_sessions_sql.delete;
    my_sqls.delete;
    --
    rec_save  := rec;
    WHILE rec <= sess_sample_tbl.last() AND l_id = sess_sample_tbl(rec) .id
    LOOP
      my_sessions_sql(sess_sample_tbl(rec) .session_id) := ' ';
      --
      IF(my_sqls.exists(sess_sample_tbl(rec) .sql_id)) THEN
        my_sqls(sess_sample_tbl(rec) .sql_id) := my_sqls(sess_sample_tbl(rec) .sql_id) + 1;
      ELSE
        my_sqls(sess_sample_tbl(rec) .sql_id) := 1;
      END IF;
      --
      rec := rec + 1;
    END LOOP;
    --
    my_sql_number.delete;
    my_sql_number(' ') := ' ';
    l_sql_id           := my_sqls.first;
    l_sql_number       := 1;
    WHILE l_sql_id     IS NOT NULL
    LOOP
      my_sql_number(l_sql_id) := TO_CHAR(l_sql_number, '000') ;
      l_sql_id                := my_sqls.next(l_sql_id) ;
      l_sql_number            := l_sql_number + 1;
    END LOOP;
    --
    l_char_list     := rpad('THREAD ->', 11) ;
    l_session       := my_sessions_sql.first;
    WHILE l_session IS NOT NULL
    LOOP
      l_char_list := l_char_list || rpad(SUBSTR(l_session, 1, 3), 3) || ' ';
      l_session   := my_sessions_sql.next(l_session) ;
    END LOOP;
    add_one(l_char_list) ;
    --
    rec       := rec_save;
    WHILE rec <= sess_sample_tbl.last() AND l_id = sess_sample_tbl(rec) .id
    LOOP
      l_sample_time := sess_sample_tbl(rec) .sample_time;
      --
      WHILE rec <= sess_sample_tbl.last() AND l_id = sess_sample_tbl(rec) .id AND l_sample_time = sess_sample_tbl(rec) .sample_time
      LOOP
        my_sessions_sql(sess_sample_tbl(rec) .session_id) := sess_sample_tbl(rec) .sql_id;
        rec                                               := rec + 1;
      END LOOP;
      --
      l_char_list     := '';
      l_session       := my_sessions_sql.first;
      WHILE l_session IS NOT NULL
      LOOP
        --        L_CHAR_LIST                := L_CHAR_LIST || RPAD(MY_SESSIONS_SQL(L_SESSION), '15');
        IF(my_sql_number.exists(my_sessions_sql(l_session))) THEN
          l_char_list := l_char_list || rpad(my_sql_number(my_sessions_sql(l_session)), 4) ;
        ELSE
          l_char_list := l_char_list || '!!!! (' || my_sessions_sql(l_session) || ') !!!!';
        END IF;
        my_sessions_sql(l_session) := ' ';
        l_session                  := my_sessions_sql.next(l_session) ;
      END LOOP;
      --
      add_one(TO_CHAR(l_sample_time, l_time_mask) || '  ' || l_char_list) ;
      --
    END LOOP;
    --  END LOOP;
    --
    add_one(' ') ;
    add_one('SQL Key -') ;
    add_one(' ') ;
    l_sql_id       := my_sqls.first;
    WHILE l_sql_id IS NOT NULL
    LOOP
      add_one(my_sql_number(l_sql_id) || lpad(TO_CHAR(my_sqls(l_sql_id)), 4) || ' ' || l_sql_id) ;
      FOR rec_i IN
      (SELECT rpad(NVL(eaglemgr.eagle_util.do_object_summary(l_sql_id), ' '), 60) || ' ' || sql_text info
      FROM
        (SELECT SUBSTR(sql_text, 1, 140) sql_text
        FROM v$sql
        WHERE sql_id = l_sql_id
        AND rownum   < 2
        )
      )
      LOOP
        add_on(' - ' || rec_i.info) ;
      END LOOP;
      --
      l_sql_id := my_sqls.next(l_sql_id) ;
    END LOOP;
    my_sqls.delete;
  END LOOP;
  RETURN(i_end + 1) ;
END;
-- ****************************************************************************
-- ****************************************************************************
FUNCTION session_info(
    in_minutes NUMBER)
  RETURN NUMBER
IS
  l_id VARCHAR2(4000) := 'xxx';
BEGIN
  i_end := 0;
  add_one(' ') ;
  add_one('ACTIVE SESSION HISTORY - ' || TO_CHAR(in_minutes) || ' MINUTE(S)') ;
  add_one(' ') ;
  FOR rec IN
  (SELECT NVL(module, NVL(program, 'NULL')) || ' - ' || NVL(action, 'NULL') id,
    sid session_id
  FROM v$session
  WHERE rownum < 1
  ORDER BY program,
    1,
    2
  )
  LOOP
    IF(l_id <> rec.id) THEN
      l_id  := rec.id;
      add_one(rec.id || ' :') ;
    END IF;
    add_on('  ' || rec.session_id) ;
  END LOOP;
  add_one(' ') ;
  OPEN sess_sample_cur(in_minutes) ;
  FETCH sess_sample_cur bulk collect
  INTO sess_sample_tbl;
  CLOSE sess_sample_cur;
  --
  RETURN(session_info_sub) ;
END;
-- ****************************************************************************
-- ****************************************************************************
FUNCTION session_info_awr(
    in_snapid_start NUMBER,
    in_snapid_end   NUMBER)
  RETURN NUMBER
IS
BEGIN
  i_end := 0;
  add_one(' ') ;
  add_one('ACTIVE SESSION AWR SNAPSHOT') ;
  FOR rec IN
  (SELECT TO_CHAR(t1.begin_interval_time, l_date_mask_short) sdate,
    TO_CHAR(t2.end_interval_time, l_date_mask_short) edate
  FROM sys.dba_hist_snapshot t1,
    sys.dba_hist_snapshot t2
  WHERE t1.snap_id = in_snapid_start
  AND t2.snap_id   = in_snapid_end
  )
  LOOP
    add_one('Begin Snap Shot: ' || in_snapid_start || '        ' || rec.sdate || ' - ' || rec.edate) ;
  END LOOP;
  OPEN sess_awr_cur(in_snapid_start, in_snapid_end) ;
  FETCH sess_awr_cur bulk collect
  INTO sess_sample_tbl;
  CLOSE sess_awr_cur;
  RETURN(session_info_sub) ;
END;
-- ****************************************************************************
-- ****************************************************************************
FUNCTION session_info_awr(
    in_snapids NUMBER)
  RETURN NUMBER
IS
  l_snapshot NUMBER;
BEGIN
  SELECT MAX(snap_id)
  INTO l_snapshot
  FROM sys.dba_hist_snapshot;
  RETURN(session_info_awr(l_snapshot - in_snapids + 1, l_snapshot)) ;
END;
-- **************************************************************************************************
-- **************************************************************************************************
FUNCTION awr_sql_id_history(
    in_sql_id v$sql.sql_id%type)
  RETURN NUMBER
IS
BEGIN
  awr_history(in_sql_id, 'RUN1') ;
  awr_history(in_sql_id, 'RUN2') ;
  RETURN(i_end + 1) ;
END;
-- ****************************************************************************
-- ****************************************************************************
FUNCTION put_to_dbms_output
  RETURN VARCHAR2
IS
  rec PLS_INTEGER;
BEGIN
  FOR rec IN 1 .. i_end
  LOOP
    dbms_output.put_line(info_line(rec)) ;
  END LOOP;
  RETURN('DONE - ' || i_end || ' Lines Pushed To DBMS_OUTPUT') ;
END;
END eagle_util;
/
