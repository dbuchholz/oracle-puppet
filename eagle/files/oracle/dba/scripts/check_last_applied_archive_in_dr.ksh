#!/bin/ksh
if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/${ORACLE_SID}_completed_in_last_30_minutes.txt
set lines 120 pages 0 echo off feedback off verify off trimspool on
alter session set nls_date_format='HH24:MI:SS MM-DD-YYYY';
WHENEVER SQLERROR EXIT FAILURE
select completion_time from v\$archived_log where sequence#=(select max(sequence#) from v\$archived_log) and applied='YES' and completion_time>sysdate-33/1440;
EOF
if [ $? -ne 0 ]
then
 print "$ORACLE_SID $(hostname)                     FAILED"
 cat $log_dir/${ORACLE_SID}_completed_in_last_30_minutes.txt
 exit 1
fi
if [[ -s $log_dir/${ORACLE_SID}_completed_in_last_30_minutes.txt ]]
then
 time_of_completion=$(cat $log_dir/${ORACLE_SID}_completed_in_last_30_minutes.txt)
 print "$ORACLE_SID $time_of_completion PASSED"
else
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/${ORACLE_SID}_completed_more_than_30_minutes_ago.txt
set lines 120 pages 0 echo off feedback off verify off trimspool on
alter session set nls_date_format='HH24:MI:SS MM-DD-YYYY';
select completion_time from v\$archived_log where sequence#=(select max(sequence#) from v\$archived_log) and applied='YES';
EOF
 time_of_completion=$(cat $log_dir/${ORACLE_SID}_completed_more_than_30_minutes_ago.txt)
 print "$ORACLE_SID $time_of_completion FAILED"
fi
rm -f $log_dir/${ORACLE_SID}_completed_in_last_30_minutes.txt $log_dir/${ORACLE_SID}_completed_more_than_30_minutes_ago.txt
exit 0
