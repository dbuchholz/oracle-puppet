#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
startup nomount
alter database mount standby database;
alter database recover managed standby database disconnect from session;
EOF
exit 0
