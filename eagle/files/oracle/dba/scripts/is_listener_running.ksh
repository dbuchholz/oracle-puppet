#!/bin/ksh
# Script Name: check_listener_log_status.ksh
mailrecipients="team_dba@eagleaccess.com"

the_host=$(hostname | awk -F. '{print $1}')

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)

# The script will not run if it does not find exactly one listener running.
(( number_of_listeners=$(ps -ef |grep tnslsnr | grep -v grep | wc -l | awk '{print $1}') ))
if [ $number_of_listeners -ne 1 ]
then
 print "There must be exactly one listener running on the host $the_host"
 exit 1
fi

# Get the Oracle Home where the current listener is running.
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 export ORACLE_HOME=$(ps -ef |grep tnslsnr | grep -v grep | head -1 | awk '{print $8}' | sed 's/\/bin.*$//g')
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 export ORACLE_HOME=$(ps -ef |grep tnslsnr | grep -v grep | head -1 | awk '{print $9}' | sed 's/\/bin.*$//g')
fi

# Get the location of the listener log.  It is different for Oracle 10g and lower as compared to Oracle 11g.
$ORACLE_HOME/bin/lsnrctl<<EOF>$script_dir/listener_log_file.txt
show log_directory
show log_file
EOF
(( db_version=$(print $ORACLE_HOME | awk -F/ '{print $6}' | awk -F. '{print $1}') ))
if [ $db_version -gt 10 ]
then
 listener_log=$(grep log_file $script_dir/listener_log_file.txt | awk '{print $NF}')
else
 listener_dir=$(grep log_directory $script_dir/listener_log_file.txt | awk '{print $NF}')
 listener_file=$(grep log_file $script_dir/listener_log_file.txt | awk '{print $NF}')
 listener_log="${listener_dir}$listener_file"
fi

# The number of lines in the listener log file
(( lines_in_listener_log=$(wc -l $listener_log | awk '{print $1}') ))
nohup $ORACLE_HOME/bin/lsnrctl status > /dev/null 2>&1 &
# Give it five seconds to complete the listener log entry
sleep 5
# Look at the listener log below the lines found before running listener status
sed "1,${lines_in_listener_log}d" $listener_log > $script_dir/recent_listener_entries.txt
# Look for the pattern that results from running listener status
grep '\* status \* 0$' $script_dir/recent_listener_entries.txt > /dev/null
if [ $? -ne 0 ]
then
 print "Did not find entry in listener log from running listener status command."
 exit 2
fi
rm -f $script_dir/listener_log_file.txt $script_dir/recent_listener_entries.txt
exit 0
