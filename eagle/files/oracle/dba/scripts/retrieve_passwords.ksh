#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
read sid?"Enter instance to retrieve passwords from: "
read machine?"Enter server name to transfer file to: "
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>restore_passwords_to_${sid}.txt
set pages 0 trimspool on feedback off
select 'alter user '||a.username||' identified by '||inf_monitor.bb_get(a.code)||';'
from inf_monitor.user_codes a,inf_monitor.databases b where a.db_instance=b.instance and b.sid='$sid';
EOF
scp restore_passwords_to_${sid}.txt ${machine}:
rm -f restore_passwords_to_${sid}.txt
exit 0
