#!/bin/ksh
if [ $# -ne 1 ]
then
   echo "Invalid Arguments!"
   echo "Usage : $0 <ORACLE_SID>" 
   exit 1
fi

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
set lines 200 feedback off trimspool on
col Start_Date format a15
col Start_Time format a10
col Dbname format a10
select
   Start_Date,
   Start_Time,
   Num_Logs,
   Round(Num_Logs * (Vl.Bytes / (1024 * 1024)), 2) AS Mbytes,
   Vdb.NAME AS Dbname
FROM 
   (SELECT To_Char(Vlh.First_Time, 'YYYY-MM-DD') AS Start_Date, To_Char(Vlh.First_Time, 'HH24') || ':00' AS Start_Time,
   COUNT(Vlh.Thread#) Num_Logs
FROM 
   V\$log_History Vlh
GROUP BY 
   To_Char(Vlh.First_Time, 'YYYY-MM-DD'),
   To_Char(Vlh.First_Time, 'HH24') || ':00') Log_Hist,
   V\$log Vl,
   V\$database Vdb
WHERE 
   Vl.Group# = 1
ORDER BY 
   Log_Hist.Start_Date,
   Log_Hist.Start_Time;
EOF
exit 0
