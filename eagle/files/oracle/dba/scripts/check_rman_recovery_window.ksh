#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/rman target / catalog rman/rman@eabprd1<<EOF
show RETENTION POLICY;
EOF
exit 0
