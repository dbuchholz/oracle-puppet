#!/bin/ksh
# Script name: switch_logfile.ksh
# Usage: switch_logfile.ksh <ORACLE_SID>

if [ $# -ne 1 ]
then
 print "${BOLD}\n\t\tInvalid Arguments!"
 print "\t\tUsage : $0 <ORACLE_SID>\n${BOFF}"
 exit 1
fi

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export PATH=$PATH:${ORACLE_HOME}/bin

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
WHENEVER SQLERROR EXIT FAILURE
ALTER SYSTEM SWITCH LOGFILE;
EOF
if [ $? -ne 0 ]
then
 print "${BOLD}ERROR: When switching logfile in ${ORACLE_SID}. ${BOFF}"
 exit 2
fi

exit 0
