#!/bin/ksh
# Script Name: is_listener_running_all.ksh
mailrecipients="fdavis@eagleaccess.com"

# Run this script every five minutes from cron.
# Runs the Production check every run or every five minutes
# Runs the sub-Production checks every third check or every 15 minutes.

# How many runs of this script before a list of machines are determined from Big Brother
when_to_query=144           # If running job every five minutes, 144 would be 12 hours to get machine list.
# Machine list does not change often, no sense in getting machine list with every five minute run.
print "Start: $(date)"

function check_listener
{
host_file=$1
while read host_name the_ip
do
 ssh -nq $the_ip $script_dir/is_listener_running.ksh > $log_dir/output_from_listener_check.txt
 (( the_exit_status=$? ))
 if [ $the_exit_status -ne 0 ]
 then
if [ $the_exit_status -eq 1 ]
then
mailx -s  "$(hostname): $0 $(date): Oracle listener problem on $host_name" $mailrecipients<<EOF
A "lsnrctl status" on $host_name was hanging for more than five seconds.
Log into $host_name and run: lsnrctl status
If this hangs, "kill -9" on PID of the listener process and then issue:  lsnrctl start

Here is the output of the: lsnrctl status
$(cat $log_dir/output_from_listener_check.txt)

With a hanging listener, there will likely be no output.
EOF
fi
if [ $the_exit_status -eq 2 ]
then
mailx -s  "$(hostname): $0 $(date): Oracle listener problem on $host_name" $mailrecipients<<EOF
A "lsnrctl status" on $host_name did not return a successful result.
A successful result is when the last line of "lsnrctl status" returns: The command completed successfully
Log into $host_name and run: lsnrctl status
If this does not show the listener is running properly, stop the listener(lsnrctl stop) and then issue:  lsnrctl start

Here is the output of the: lsnrctl status
$(cat $log_dir/output_from_listener_check.txt)
EOF
fi
if [ $the_exit_status -eq 3 ]
then
mailx -s  "$(hostname): $0 $(date): Oracle listener problem on $host_name" $mailrecipients<<EOF
A "lsnrctl status" on $host_name did was not able to be obtained.
This is a result of the standard output file not getting creating that contains the output of "lsnrctl status".
Log into $host_name and run: lsnrctl status
If this does not show the listener is running properly, stop the listener(lsnrctl stop) and then issue:  lsnrctl start
EOF
fi
if [ $the_exit_status -eq 127 ]
then
 scp -nq $script_dir/is_listener_running.ksh ${host_name}:$script_dir
 print "Listener check script did not exist on remote host.  Copying now."
 return 0
fi
if [ $the_exit_status -eq 255 ]
then
mailx -s  "$(hostname): $0 $(date): ssh failure to $host_name" $mailrecipients<<EOF
Attempting to "lsnrctl status" could not reach remote server.
This is not an indication that there is a listener problem on ${host_name}.
This is usually due to a transient network problem.
EOF
fi 
if [ $the_exit_status -ne 1 ] && [ $the_exit_status -ne 2 ] && [ $the_exit_status -ne 3 ] && [ $the_exit_status -ne 127 ] && [ $the_exit_status -ne 255 ]
then
mailx -s  "$(hostname): $0 $(date): Unhandled error $the_exit_status to $host_name" $mailrecipients<<EOF
There was an unexpected exit status when trying to "lsnrctl status" to remote host.

Here is the output, if any, of the: lsnrctl status
$(cat $log_dir/output_from_listener_check.txt)
EOF
fi
 fi
done<$host_file
}

if [ "$(dirname $0)" = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ -s $log_dir/run_count_query.txt ]
then
 (( query_count=$(cat $log_dir/run_count_query.txt) ))
 (( query_count=query_count+1 ))
 print $query_count > $log_dir/run_count_query.txt
else
 print $when_to_query > $log_dir/run_count_query.txt
 (( query_count=$when_to_query ))
fi

(( machine_file_count=$(ls -1 $log_dir/pittsburgh_*servers.txt $log_dir/everett_*servers.txt | wc -l) ))

if [ $query_count -eq $when_to_query ] || [ $machine_file_count -ne 5 ]
then 
(( query_count=0 ))
print $query_count > $log_dir/run_count_query.txt
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set pages 0 trimspool on feedback off echo off

spool $log_dir/everett_prod1_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT' and m.instance<(select median(instance) from inf_monitor.machines where location='EVERETT')
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT' and m.instance<(select median(instance) from inf_monitor.machines where location='EVERETT');

spool $log_dir/everett_prod2_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT' and m.instance>=(select median(instance) from inf_monitor.machines where location='EVERETT')
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT' and m.instance>=(select median(instance) from inf_monitor.machines where location='EVERETT');

spool $log_dir/everett_imp_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='EVERETT IMP'
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='EVERETT IMP';

spool $log_dir/pittsburgh_prod_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='PITTSBURGH'
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='PITTSBURGH';

spool $log_dir/pittsburgh_imp_servers.txt
select m.name,m.ip_address from inf_monitor.machines m where location='PITTSBURGH IMP'
intersect
select distinct m.name,m.ip_address from inf_monitor.databases d,inf_monitor.machines m where d.mac_instance=m.instance and m.location='PITTSBURGH IMP';

EOF

# Get rid of any blank lines in files
sed '/^$/d' $log_dir/everett_prod1_servers.txt > $log_dir/everett_prod1_serversx.txt
mv $log_dir/everett_prod1_serversx.txt $log_dir/everett_prod1_servers.txt
sed '/^$/d' $log_dir/everett_prod2_servers.txt > $log_dir/everett_prod2_serversx.txt
mv $log_dir/everett_prod2_serversx.txt $log_dir/everett_prod2_servers.txt
sed '/^$/d' $log_dir/everett_imp_servers.txt > $log_dir/everett_imp_serversx.txt
mv $log_dir/everett_imp_serversx.txt $log_dir/everett_imp_servers.txt
sed '/^$/d' $log_dir/pittsburgh_prod_servers.txt > $log_dir/pittsburgh_prod_serversx.txt
mv $log_dir/pittsburgh_prod_serversx.txt $log_dir/pittsburgh_prod_servers.txt
sed '/^$/d' $log_dir/pittsburgh_imp_servers.txt > $log_dir/pittsburgh_imp_serversx.txt
mv $log_dir/pittsburgh_imp_serversx.txt $log_dir/pittsburgh_imp_servers.txt
fi

# Run every time for PROD
check_listener $log_dir/everett_prod1_servers.txt &
check_listener $log_dir/everett_prod2_servers.txt &
check_listener $log_dir/pittsburgh_prod_servers.txt &

if [ -s $log_dir/run_count_listener.txt ]
then
 (( run_count=$(cat $log_dir/run_count_listener.txt) ))
 (( run_count=run_count+1 ))
 print $run_count > $log_dir/run_count_listener.txt
else
 print 0 > $log_dir/run_count_listener.txt
 (( run_count=0 ))
fi

# Skip running in IMP this many runs.  So run every third time.
if [ $run_count -eq 2 ]
then 
 check_listener $log_dir/everett_imp_servers.txt &
 check_listener $log_dir/pittsburgh_imp_servers.txt &
 rm -f $log_dir/run_count_listener.txt
fi
wait
print "End: $(date)"
exit 0
