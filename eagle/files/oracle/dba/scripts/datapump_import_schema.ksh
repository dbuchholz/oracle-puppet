#!/bin/ksh
######################################################################################
# Usage : import_schema.ksh [Source Oracle SID] [Target Oracle SID] [{From Schema Owner},{To Schema Owner}] [Export Dump File Directory]
# {To Schema Owner} is optional when From and To schema are the same.
#
# Use this script to refresh a schema where the source and target schemas have the same tables.
# This script will truncate all tables in a users target schema specified by Oracle SID and Schema Owner
# It is meant to be used to refresh a schema from one database to another.
# It assumes that there is no additional tables in Schema Owner on the target database.
# If a table exists in Schema Owner on the target side that does not exist on the source side,
# it will be truncated and will remain that way after the refresh.
# Any tables truncated on the target that you did want truncated
# could be imported back from export taken in Step 1 outlined below.
#
# Note : 1. Data has to be exported from the source prior to starting this script
#           The source dump file contains all tables in a schema from a export parfile that looks
#           like this:
#           file=
#           grants=N
#           constraints=N
#           indexes=N
#           tables=(schema.table1,schema.table2,...)
#        2. Source export dump file should be manually copied to the target machine
#           and is the Source Dump File parameter passed into this script.
#
# This script will:
#  1. Export target schemas objects (owner=) just in case it is required to back out refresh.
#  2. Export target databases definitions just in case it is required to take corrective action after refresh.
#  3. Generate scripts to disable/enable Primary Key and Foreign Key constraints.
#  4. Generate scripts to disable/enable triggers.
#  5. Generate script to truncate tables for Schema Owner.
#  6. Extract index definitions from export dump file created in Step 1.
#  7. Take before refresh image of dba_objects and dba_constraints views on target schema.
#  8. Run script to disable Foreign Key and Primary Key constraints.
#  9. Run script to drop indexes.
# 10. Run script to disable triggers.
# 11. Run script to truncate tables.
# 12. Import Schema Owner data from Source Dump File.
# 13. Run script to enable triggers.
# 14. Run script to re-create indexes.
# 15. Run script to enable Primary Key and Foreign Key constraints.
# 16. Recompile all database objects.
# 
#
######################################################################################
# Check for proper usage 
if [ $# -lt 4 ]
then
  print "Invalid Arguments."
  print "Usage :  $0 [Source Oracle SID] [Target Oracle SID] [{From Schema Owner},{To Schema Owner}] [Export Dump File Directory]"
  exit 1
exit 1
fi

source_oracle_sid=$1
typeset -u source_oracle_sid
ORACLE_SID=$2
typeset -xu ORACLE_SID
export ORAENV_ASK=NO
. /usr/local/bin/oraenv
dump_file_dir=/d03
export DATA_PUMP_DIR=DATAPUMP_DIRECTORY

schema_owner=$3
print $schema_owner | grep ',' > /dev/null
if [ $? -eq 0 ]
then
 from_schema_owner=$(print $schema_owner | awk -F, '{print $1}')
 to_schema_owner=$(print $schema_owner | awk -F, '{print $2}')
else
 from_schema_owner=$schema_owner
 to_schema_owner=$from_schema_owner
fi
typeset -u from_schema_owner
typeset -u to_schema_owner

dump_file_dir=$4
if [[ ! -d $dump_file_dir ]]
then
 print "There was no valid export dump file directory specified in fourth parameter"
 exit 1
fi
export TODAY=$(date "+%Y%d%m")
export_dump_file=export_${source_oracle_sid}_${TODAY}_%U.dmpdp


today=$(date "+%d%b%Y")
typeset -u today

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir/logs
#LOGFILE=$log_dir/$ORACLE_SID_${to_schema_owner}_import_${today}.log

echo $ORACLE_SID
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF > $log_dir/datapump_dump_directory_${ORACLE_SID}.txt
set head off feed off lines 150 trims on pages 0 sqlp ""
select directory_path from dba_directories where directory_name='$DATA_PUMP_DIR';
EOF

DATAPUMP_DIRECTORY=$(sed '/^$/d' $log_dir/datapump_dump_directory_${ORACLE_SID}.txt)
echo "the pump is $DATAPUMP_DIRECTORY"

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/cpl_users_${ORACLE_SID}_${to_schema_owner}.txt
set heading off
select username from dba_users where username like '%$to_schema_owner%' and username like '%WL_CPL';
EOF
userlist=$(grep -v '^$' $log_dir/cpl_users_${ORACLE_SID}_${to_schema_owner}.txt)

rm -f $script_dir/lock_them_${ORACLE_SID}_${to_schema_owner}.sql
#for username in $userlist
#do
#print "alter user $username account lock;" >> $script_dir/lock_them_${ORACLE_SID}_${to_schema_owner}.sql
#done
rm -f $log_dir/cpl_users_${ORACLE_SID}_${to_schema_owner}.txt

rm -f $log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.txt
for username in $userlist
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.txt
set heading off feedback off
select 'alter system kill session '||''''||sid||','||serial#||''''||';' from v\$session where username='$username';
EOF
done
grep -v '^$' $log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.txt > $log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.sql
rm -f $log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.txt

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/whos_accessing_${ORACLE_SID}_${to_schema_owner}.txt
set heading off feedback off
select distinct username from  v\$access a,v\$session b where a.sid=b.sid and a.owner='$from_schema_owner';
EOF
sed '/^$/d' $log_dir/whos_accessing_${ORACLE_SID}_${to_schema_owner}.txt > $log_dir/users_accessing_${ORACLE_SID}_${to_schema_owner}.txt
kill_list=$(cat $log_dir/users_accessing_${ORACLE_SID}_${to_schema_owner}.txt)
rm -f $log_dir/whos_accessing_${ORACLE_SID}_${to_schema_owner}.txt
#for a_user in $kill_list
#do
#print "alter user $a_user account lock;" >> $script_dir/lock_them_${ORACLE_SID}_${to_schema_owner}.sql
#done
#sed 's/ lock;/ unlock;/g' $script_dir/lock_them_${ORACLE_SID}_${to_schema_owner}.sql > $script_dir/unlock_them_${ORACLE_SID}_${to_schema_owner}.sql

quoted_kill_list=$(print $kill_list | sed -e "s/,/','/g" -e "s/^/'/g" -e "s/$/'/g")

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/kill_locked_object_sessions_${ORACLE_SID}_${to_schema_owner}.sql
set pagesize 0 heading off feedback off
SELECT /*+ RULE */ distinct 'alter system kill session '||''''||S.SID||','||S.SERIAL#||''''||';' FROM V\$LOCK L, SYS.DBA_OBJECTS O, V\$SESSION S WHERE L.SID = S.SID AND L.ID1=O.OBJECT_ID and O.OWNER='$to_schema_owner';
EOF

# Kill sessions that are accessing schema to be refreshed
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 pagesize 0 heading off echo off trimspool on feedback off
spool $script_dir/kill_sessions_${ORACLE_SID}_${to_schema_owner}.sql
select distinct 'alter system kill session '||''''||sid||','||serial#||''''||';'
from v\$session where username in ($quoted_kill_list);
spool off
@$script_dir/kill_sessions_${ORACLE_SID}_${to_schema_owner}.sql
@$log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.sql
@$script_dir/kill_locked_object_sessions_${ORACLE_SID}_${to_schema_owner}.sql
EOF
rm -f $log_dir/kill_cpl_users_${ORACLE_SID}_${to_schema_owner}.sql
rm -f $script_dir/kill_locked_object_sessions_${ORACLE_SID}_${to_schema_owner}.sql
rm -f $script_dir/kill_sessions_${ORACLE_SID}_${to_schema_owner}.sql

# Set the NLS_LANG env variable from database
echo "Setting NLS_LANG env variable from the database..."
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba' <<EOF
    WHENEVER SQLERROR EXIT FAILURE;
   set echo off feed off pages 0 trim on trims on
   spool $log_dir/nls_lang_val.lst
   SELECT lang.value || '_' || terr.value || '.' || chrset.value FROM v\$nls_parameters lang,v\$nls_parameters terr,v\$nls_parameters chrset
   WHERE lang.parameter='NLS_LANGUAGE' AND terr.parameter='NLS_TERRITORY' AND chrset.parameter='NLS_CHARACTERSET';
   spool off
EOF
if [ $? -ne 0 ]
then
   print "ERROR : When getting the NLS_LANG value from the database!"
   exit 1
fi
export NLS_LANG=$(cat ${log_dir}/nls_lang_val.lst)
print "NLS_LANG is set as $NLS_LANG"

print "Exporting Schema Owner's objects"
$ORACLE_HOME/bin/exp / file=$dump_file_dir/${ORACLE_SID}_${to_schema_owner}_${today}.dmp buffer=81920000 RECORDLENGTH=64000 compress=N direct=y log=$script_dir/${ORACLE_SID}_${to_schema_owner}_export_${today}.log owner=$to_schema_owner
grep "Export terminated successfully without warnings\." $script_dir/${ORACLE_SID}_${to_schema_owner}_export_${today}.log
if [ $? -ne 0 ]
then
 print "Schema Export was not successful.  Aborting process."
 exit 2
fi

find $script_dir -name "${ORACLE_SID}_full_database_definition_export_${today}.log" -mtime -1 > /dev/null
if [ $? -ne 0 ]
then
 print "Exporting FULL database definition to $dump_file_dir"
 $ORACLE_HOME/bin/exp / file=$dump_file_dir/${ORACLE_SID}_full_database_definition_export_${today}.dmp ROWS=N FULL=Y log=$script_dir/${ORACLE_SID}_full_database_definition_export_${today}.log
 grep "Export terminated successfully without warnings\." $script_dir/${ORACLE_SID}_full_database_definition_export_${today}.log
 if [ $? -ne 0 ]
 then
  print "Database Definition Export was not successful.  Aborting process."
  exit 3
 fi
fi

print "Generating disable/enable FK-PK constraints"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > /dev/null
WHENEVER SQLERROR EXIT FAILURE;
REM set echo off feed off veri off sqlp "" termout off 
set pages 0 lines 250 trim on trims on
COL dummy NOPRINT
spool $script_dir/dis_fk_pk_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 1 dummy,'set echo on feed on' FROM dual UNION
SELECT 2 dummy,'spool $script_dir/dis_fk_pk_${ORACLE_SID}_$to_schema_owner' FROM dual UNION
SELECT 3 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||' DISABLE CONSTRAINT '||a.constraint_name||';' FROM dba_constraints a WHERE a.constraint_type='C' and a.owner='$to_schema_owner' and a.status='ENABLED' UNION
SELECT 4 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||' DISABLE CONSTRAINT '||a.constraint_name||';' FROM dba_constraints a WHERE a.constraint_type='R' and a.owner='$to_schema_owner' and a.status='ENABLED' UNION
SELECT 5 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||' DISABLE CONSTRAINT '||a.constraint_name||';' FROM dba_constraints a WHERE (a.r_owner,a.r_constraint_name) in (select b.owner,b.constraint_name from dba_constraints b where b.constraint_type IN ('P','U') and b.owner='$to_schema_owner' and a.status='ENABLED') UNION
SELECT 6 dummy,'REM STATUS : '||c.status||CHR(10)||'ALTER TABLE '||c.owner||'.'||c.table_name||' DISABLE CONSTRAINT '||c.constraint_name||';' FROM dba_constraints c,dba_indexes i WHERE c.owner='$to_schema_owner' AND c.constraint_type IN ('P','U') AND i.owner = c.owner(+) AND i.index_name = c.constraint_name(+) AND c.status = 'ENABLED' AND i.index_type='NORMAL' UNION
SELECT 7 dummy,'spool off' FROM dual ORDER BY dummy;
spool off

spool $script_dir/ena_pk_fk_${ORACLE_SID}_$to_schema_owner.sql
SELECT 1 dummy,'set echo on feed on' FROM dual UNION
SELECT 2 dummy,'spool $script_dir/ena_pk_fk_${ORACLE_SID}_$to_schema_owner' FROM dual UNION
SELECT 3 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||' ENABLE '||DECODE(a.validated,'VALIDATED','VALIDATE','NOVALIDATE')||' CONSTRAINT '||a.constraint_name||';' FROM dba_constraints a WHERE a.constraint_type = 'C' and a.owner='$to_schema_owner' and a.status='ENABLED' UNION
select 4 dummy,'REM STATUS : '||c.status||CHR(10)||'ALTER TABLE '||c.owner||'.'||c.table_name||CHR(10)||DECODE(c.status,'ENABLED',' ENABLE',' DISABLE')||' '||DECODE(c.validated,'VALIDATED','VALIDATE','NOVALIDATE')||'  CONSTRAINT '||c.constraint_name||' USING INDEX INITRANS '||i.ini_trans||' MAXTRANS '||i.max_trans||' TABLESPACE '||i.tablespace_name||' STORAGE (INITIAL '||i.initial_extent||' PCTINCREASE 0    '||' MINEXTENTS '||i.min_extents||' MAXEXTENTS '||i.max_extents||');' from dba_constraints c,dba_indexes i where c.owner='$to_schema_owner' and c.constraint_type IN ('P','U') and c.status='ENABLED' and i.owner=c.owner and    i.index_name = c.constraint_name(+) and i.table_name=c.table_name(+) and i.index_type='NORMAL' and i.initial_extent IS NOT NULL UNION
select 5 dummy,'REM STATUS : '||c.status||CHR(10)||'ALTER TABLE '||c.owner||'.'||c.table_name||CHR(10)||DECODE(c.status,'ENABLED',' ENABLE',' DISABLE')||' '||DECODE(c.validated,'VALIDATED','VALIDATE','NOVALIDATE')||'  CONSTRAINT '||c.constraint_name||';' from dba_constraints c,dba_indexes i where c.owner='$to_schema_owner' and c.constraint_type IN ('P','U') and c.status='ENABLED' and i.owner=c.owner and i.index_name=c.constraint_name(+) and i.table_name=c.table_name(+) and i.index_type='NORMAL' and i.initial_extent IS NULL UNION
select 6 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||DECODE(a.status,'ENABLED',' ENABLE',' DISABLE')||' '||DECODE(a.validated,'VALIDATED','VALIDATE','NOVALIDATE')||'  CONSTRAINT '||a.constraint_name||';' from dba_constraints a where (a.r_owner,a.r_constraint_name) in ( select b.owner,b.constraint_name from dba_constraints b where b.constraint_type IN ('P','U') and b.owner = '$to_schema_owner' and a.status = 'ENABLED') UNION
SELECT 7 dummy,'REM STATUS : '||a.status||CHR(10)||'ALTER TABLE '||a.owner||'.'||a.table_name||DECODE(a.status,'ENABLED',' ENABLE',' DISABLE')||' '||DECODE(a.validated,'VALIDATED','VALIDATE','NOVALIDATE')||' CONSTRAINT '||a.constraint_name||';' FROM dba_constraints a WHERE a.constraint_type='R' and a.owner='$to_schema_owner' and a.status='ENABLED' UNION
SELECT 8 dummy,'spool off' FROM dual ORDER BY dummy;
spool off
EOF
if [ $? -ne 0 ]
then
   print "ERROR: While generating scripts to disable/enable FK/PK constraints!"
   exit 4
fi

print "Generating scripts to disable/enable triggers"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > /dev/null
WHENEVER SQLERROR EXIT FAILURE;
set echo off feed off veri off sqlp "" termout off
set pages 0 lines 250 trim on trims on
spool $script_dir/dis_trig_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual;
SELECT 'spool $script_dir/dis_trig_${ORACLE_SID}_${to_schema_owner}' FROM dual;
SELECT 'ALTER TRIGGER '||owner||'.'||trigger_name||' DISABLE ;' FROM dba_triggers WHERE table_owner='$to_schema_owner' AND status LIKE 'ENABLE%';
SELECT 'spool off' FROM dual;
spool off

spool $script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual;
SELECT 'spool $script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}' FROM dual;
SELECT 'ALTER TRIGGER ' || owner || '.' || trigger_name || ' ENABLE ;' FROM dba_triggers WHERE table_owner='$to_schema_owner' AND status LIKE 'ENABLE%';
SELECT 'spool off' FROM dual;
spool off
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Generating scripts to disable/enable triggers"
 #exit 5 
fi

print "Extracting index definitions from export dump"
$ORACLE_HOME/bin/imp / file=$dump_file_dir/${ORACLE_SID}_${to_schema_owner}_${today}.dmp rows=N buffer=81920000 RECORDLENGTH=64000 full=Y indexes=Y indexfile=$script_dir/temp_${ORACLE_SID}_${to_schema_owner}.sql log=$script_dir/temp_${ORACLE_SID}_${to_schema_owner}.log
grep "Import terminated successfully without warnings\." $script_dir/temp_${ORACLE_SID}_${to_schema_owner}.log
if [ $? -ne 0 ]
then
 print "Index Definition Indexfile was not successful.  Aborting process."
 #exit 6
fi

# Generate scripts to LOGGING/NOLOGGING objects
print "Generating scripts to LOGGING/NOLOGGING objects in ${to_schema_owner}"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
WHENEVER SQLERROR EXIT FAILURE;
set echo off feed off veri off sqlp "" termout off
set pages 0 lines 250 trim on trims on
spool $script_dir/nolog_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual;
SELECT 'spool $script_dir/nolog_${ORACLE_SID}_${to_schema_owner}' FROM dual;
SELECT 'ALTER '||object_type||' '||owner||'.'||object_name||' NOLOGGING ;' FROM dba_objects WHERE owner='${to_schema_owner}' AND object_type IN ('TABLE','INDEX') AND temporary != 'Y' ORDER BY object_name;
spool off

spool $script_dir/setlog_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual;
SELECT 'spool $script_dir/setlog_${ORACLE_SID}_${to_schema_owner}' FROM dual;
SELECT 'ALTER TABLE '||owner||'.'||table_name||DECODE(logging,'YES',' LOGGING;',' NOLOGGING;') FROM dba_tables WHERE owner = '${to_schema_owner}' AND temporary != 'Y' ORDER BY table_name;

SELECT 'ALTER INDEX '||owner||'.'||index_name||DECODE(logging,'YES',' LOGGING ;',' NOLOGGING ;') FROM dba_indexes WHERE owner='${to_schema_owner}' AND temporary != 'Y' ORDER BY index_name;
SELECT 'spool off' FROM dual;
spool off
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Generating scripts to LOGGING/NOLOGGING objects"
fi

# Generate scripts to set INDEX parallelism back to original
print "Generating scripts to set INDEX parallelism ${to_schema_owner}"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>/dev/null
 WHENEVER SQLERROR EXIT FAILURE;
 set echo off feed off veri off sqlp "" termout off
set pages 0 lines 250 trim on trims on
spool $script_dir/setparl_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual ;
SELECT 'spool $script_dir/setparl_${ORACLE_SID}_${to_schema_owner}' FROM dual;
SELECT 'ALTER INDEX '||owner||'.'||index_name||' PARALLEL(DEGREE '||degree||' INSTANCES '||instances||');' FROM dba_indexes WHERE owner='${to_schema_owner}' AND temporary != 'Y' AND index_type='NORMAL' ORDER BY index_name;
SELECT 'spool off' FROM dual;
spool off
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Generating scripts to set INDEX parallelism"
fi

# Set Index Parallelism to 5 in indexfile that creates indexes
print "set echo on feed on" > $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
print "spool $script_dir/index_${ORACLE_SID}_${to_schema_owner}" >> $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
print "ALTER SESSION ENABLE PARALLEL DDL;" >> $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
print "ALTER SESSION SET SORT_AREA_SIZE=50000000;" >> $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
cat $script_dir/temp_${ORACLE_SID}_${to_schema_owner}.sql | sed '/^REM/d' | sed '/^CONNECT/d' | sed '/^$/d' | sed 's/^CREATE/~&/; y/~/\n/' | sed -e 's/ LOGGING / NOLOGGING /g' -e 's/TABLESPACE / PARALLEL 5 TABLESPACE /g' >> $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
print "spool off" >> $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
rm -f $script_dir/temp_${ORACLE_SID}_${to_schema_owner}.sql

print "Taking snapshot of dba_objects and dba_constraints views"
find $script_dir -name "${ORACLE_SID}_${to_schema_owner}_snap_obj_cons.lst" -mtime -1 > /dev/null
if [ $? -ne 0 ]
then
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > $script_dir/${ORACLE_SID}_${to_schema_owner}_snap_obj_cons
WHENEVER SQLERROR EXIT FAILURE;
set echo on feed on
select owner,object_type,object_name,last_ddl_time,status from dba_objects;
select owner,constraint_name,constraint_type,table_name,r_owner,r_constraint_name,
delete_rule,deferrable,deferred,validated,last_change,status from dba_constraints;
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Taking snapshot of dba_objects and dba_constraints views"
 print "LOG FILE : $script_dir/${ORACLE_SID}_${to_schema_owner}_snap_obj_cons.lst"
 exit 7
fi
fi

print "Running script to disable FK-PK constraints"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
WHENEVER SQLERROR EXIT FAILURE ;
purge dba_recyclebin;
@$script_dir/dis_fk_pk_${ORACLE_SID}_${to_schema_owner}.sql
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Running script to disable FK-PK constraints"
 print "SQL FILE : $script_dir/dis_fk_pk_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/dis_fk_pk_${ORACLE_SID}_${to_schema_owner}.lst"
 #exit 8
fi

# NOLOG Objects
print "Running script to NOLOGGING objects in ${to_schema_owner}..."
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF>/dev/null
  @$script_dir/nolog_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep ORA- ${script_dir}/nolog_${ORACLE_SID}_${to_schema_owner}.lst| grep -v ORA-01418
if [ $? -eq 0 ]
then
 print "ERROR: Running script to NOLOGGING objects"
 print "SQL FILE : ${script_dir}/nolog_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : ${script_dir}/nolog_${ORACLE_SID}_${to_schema_owner}.lst"
fi

# Start destructive action on target schema
print "Running script to drop indexes"
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF > /dev/null
set echo off feed off veri off sqlp "" termout off
set pages 0 lines 250 trim on trims on
spool $script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}.sql
SELECT 'set echo on feed on' FROM dual ;
SELECT 'spool $script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}' FROM dual ;
SELECT 'DROP INDEX ${to_schema_owner}.'||index_name||';' FROM dba_indexes WHERE table_owner='$to_schema_owner' AND uniqueness = 'NONUNIQUE';
SELECT 'spool off' FROM dual ;
spool off
@$script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}.sql
purge dba_recyclebin;
EOF
grep ORA- $script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}.lst
if [ $? -eq 0 ]
then
 print "ERROR: Running script to drop indexes"
 print "SQL FILE : $script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/drop_index_${ORACLE_SID}_${to_schema_owner}.lst"
fi

print "Running script to disable triggers"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
WHENEVER SQLERROR EXIT FAILURE ;
@$script_dir/dis_trig_${ORACLE_SID}_${to_schema_owner}.sql
EOF
if [ $? -ne 0 ]
then
 print "ERROR: Running script to disable triggers"
 print "SQL FILE : $script_dir/dis_trig_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/dis_trig_${ORACLE_SID}_${to_schema_owner}.lst"
 #exit 10
fi

print "Running script to truncate tables"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > $script_dir/trunc_${to_schema_owner}.lst
@$dump_file_dir/${ORACLE_SID}_${to_schema_owner}_truncate_table.sql
EOF

$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > $script_dir/unlock_result_${to_schema_owner}.lst
@$script_dir/unlock_them_${ORACLE_SID}_${to_schema_owner}.sql
EOF
#grep 'ORA-' $script_dir/unlock_result_${to_schema_owner}.lst
#if [ $? -eq 0 ]
#then
 #print "ERROR: Unlocking accounts"
 #print "LOG FILE : $script_dir/unlock_result_${to_schema_owner}.lst"
#fi

grep 'ORA-' $script_dir/trunc_${to_schema_owner}.lst | grep -v 'ORA-00942'
if [ $? -eq 0 ]
then
 print "ERROR: Running script to truncate tables"
 print "SQL FILE : $dump_file_dir/${ORACLE_SID}_${to_schema_owner}_truncate_table.sql"
 print "LOG FILE : $script_dir/trunc_${to_schema_owner}.lst"
 #exit 11
fi

print "Importing ${to_schema_owner} data"
#export LOGFILE=import_${ORACLE_SID}_${TODAY}.log
$ORACLE_HOME/bin/impdp /  DIRECTORY=DATA_PUMP_DIR DUMPFILE=$export_dump_file  remap_schema=$from_schema_owner:$to_schema_owner EXCLUDE=INDEX TABLE_EXISTS_ACTION=TRUNCATE LOGFILE=$ORACLE_SID_${to_schema_owner}_import_${today}.log JOB_NAME=IMPDP_${TODAY} 

grep "^Job .*IMPDP_${TODAY}.* successfully completed" $DATAPUMP_DIRECTORY/$ORACLE_SID_${to_schema_owner}_import_${today}.log > /dev/null 2>&1
if [ $? -ne 0 ]
then
 print "Error in importing ${to_schema_owner} data"
 print "See $DATAPUMP_DIRECTORY/$ORACLE_SID_${to_schema_owner}_import_${today}.log"
fi

print "Running script to enable triggers"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
@$script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep 'ORA-' $script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}.lst
if [ $? -eq 0 ]
then
 print "ERROR: Running script to enable triggers"
 print "SQL FILE : $script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/ena_trig_${ORACLE_SID}_${to_schema_owner}.lst"
fi

print "Running script to re-create indexes"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
ALTER SESSION SET CURRENT_SCHEMA=${to_schema_owner} ;
@$script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep 'ORA-' $script_dir/index_${ORACLE_SID}_${to_schema_owner}.lst | grep -v 'ORA-00955'
if [ $? -eq 0 ]
then
 print "ERROR: Running script to re-create indexes"
 print "SQL FILE : $script_dir/index_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/index_${ORACLE_SID}_${to_schema_owner}.lst"
 #exit 13
fi

print "Running script to enable PK-FK constraints"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
@$script_dir/ena_pk_fk_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep 'ORA-' $script_dir/ena_pk_fk_${ORACLE_SID}_${to_schema_owner}.lst
if [ $? -eq 0 ]
then
 print "ERROR: Running script to enable PK-FK constraints"
 print "SQL FILE : $script_dir/ena_pk_fk_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/ena_pk_fk_${ORACLE_SID}_${to_schema_owner}.lst"
fi

# Set original LOGGING option 
print "Running script to set original LOGGING option in ${to_schema_owner}..."
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF>/dev/null
@$script_dir/setlog_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep 'ORA-' $script_dir/setlog_${ORACLE_SID}_${to_schema_owner}.lst|grep -v ORA-22864|grep -v ORA-01418
if [ $? -eq 0 ]
then
 print "ERROR: Running script to set original LOGGING option"
 print "SQL FILE : $script_dir/setlog_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/setlog_${ORACLE_SID}_${to_schema_owner}.lst"
fi

# Set original INDEX parallelism option 
print "Running script to set original INDEX Parallelism in ${to_schema_owner}..."
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF>/dev/null
@${script_dir}/setparl_${ORACLE_SID}_${to_schema_owner}.sql
EOF
grep 'ORA-' $script_dir/setparl_${ORACLE_SID}_${to_schema_owner}.lst
if [ $? -eq 0 ]
then
 print "ERROR: Running script to set original INDEX Parallelism"
 print "SQL FILE : $script_dir/setparl_${ORACLE_SID}_${to_schema_owner}.sql"
 print "LOG FILE : $script_dir/setparl_${ORACLE_SID}_${to_schema_owner}.lst"
fi

print "Refreshing Sequences"
##$script_dir/refresh_seq.ksh $ORACLE_SID $to_schema_owner $source_oracle_sid

print "Recompile all database objects"
#$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF > /dev/null
#@$ORACLE_HOME/rdbms/admin/utlrp.sql
#EOF
if [ $? -ne 0 ]
then
 print "ERROR: Recompiling database objects"
fi

print "End of Refresh at $(date)"
exit 0
