#!/bin/ksh
#### Added by Nirmal
if [ $# -eq 0 ]
then
 clear
 print "\n\nUsage: ./SqlAdvisor.ksh <SID>\n"
 exit 1
fi
ORA_SID=$1

STATUS=$(ps -fu oracle |grep -v grep| grep ora_pmon_${ORA_SID})
if [ $? -ne 0 ]
then
 clear
 print "\n\nDatabase ${ORA_SID} is not running or you typed a wrong SID. Please try with valid SID...\n"
 print "Usage: ./SqlAdvisor.ksh <SID>\n\n\n "
 exit 1
fi
####################################
export ORACLE_SID=$ORA_SID
export ORAENV_ASK=NO
. /usr/local/bin/oraenv > /dev/null
read sql_id?"Enter SQL ID: "

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>dbid.txt
SET SERVEROUTPUT ON
DECLARE
  l_sql_tune_task_id  VARCHAR2(100);
BEGIN
  l_sql_tune_task_id := DBMS_SQLTUNE.create_tuning_task (
                          sql_id      => '${sql_id}',
                          scope       => DBMS_SQLTUNE.scope_comprehensive,
                          time_limit  => 60,
                          task_name   => '${sql_id}_AWR_tuning_task',
                          description => 'Tuning task for statement ${sql_id} in AWR.');
  DBMS_OUTPUT.put_line('l_sql_tune_task_id: ' || l_sql_tune_task_id);
END;
/
EXEC DBMS_SQLTUNE.execute_tuning_task(task_name => '${sql_id}_AWR_tuning_task');
spool /u01/app/oracle/${sql_id}_SQLAdvisor.txt
SET LONG 10000 PAGESIZE 1000 LINESIZE 200
SELECT DBMS_SQLTUNE.report_tuning_task('${sql_id}_AWR_tuning_task') AS recommendations FROM dual;
SET PAGESIZE 24
spool off
BEGIN
  DBMS_SQLTUNE.drop_tuning_task (task_name => '${sql_id}_AWR_tuning_task');
END;
/
EOF
rm -f dbid.txt
exit 0
