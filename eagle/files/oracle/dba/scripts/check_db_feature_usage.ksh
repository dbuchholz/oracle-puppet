#!/bin/ksh
# Script name: check_db_feature_usage.ksh
# Usage: check_db_feature_usage.ksh <Oracle SID>
if [ $# -lt 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <ORACLE_SID>\n${BOFF}"
   exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

# Verify that the first parameter is a valid Oracle SID defined in /var/opt/oracle/oratab
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "Cannot find Instance $1 in $ORATAB"
   exit 2
fi
 
# Export variable declarations

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
LOGDIR=$SCRIPT_DIR

export ORACLE_SID=$1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set linesize 500 pagesize 0 feedback off
spool $LOGDIR/${ORACLE_SID}_feature_usage.txt
select distinct name from DBA_FEATURE_USAGE_STATISTICS
where detected_usages>0 and
CURRENTLY_USED='TRUE' and
dbid=(select dbid from v\$database) order by name;
EOF
exit 0
