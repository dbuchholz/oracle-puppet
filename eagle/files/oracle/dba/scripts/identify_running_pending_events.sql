set lines 200 trimspool on feedback off echo off
col instance format 99999999
col event_inst format 99999999
col user_data noprint
col SCHED_USER_DATA noprint
col time_freq noprint
col custom_proc_name format a120
col upd_user format a14
alter session set nls_date_format='MM-DD-YYYY HH24:MI';
set echo on
select * from pace_masterdbo.schedule_def sd where sd.instance in (select sq.pinstance from pace_masterdbo.schedule_queue sq where sq.status='P') and sd.freq_type='N';
select sq.pinstance,sd.instance,sd.custom_proc_name from pace_masterdbo.schedule_queue sq,pace_masterdbo.schedule_def sd where sq.instance=sd.instance and sq.status='P';
set echo off

set pagesize 0
select 'update pace_masterdbo.schedule_queue set status='||''''||'D'||''''||' where instance in ([comma separated INSTANCE list directly above]);' from dual;
set echo on pagesize 999
/* The INSTANCE in the update statement is where INSTANCE in the first query equals PINSTANCE in the second query */
/* Request that the PACE Engines be shut down before updating the INSTANCE. */
