#!/bin/ksh
mailrecipients=fdavis@eagleaccess.com

if [ $# -eq 0 ]
then
   print "Invalid Arguments!"
   print "Usage : $0 <ORACLE_SID> [Logs Falling Behind By Threshold]"
   exit 1
fi

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

ORACLE_SID=$1
primary_server=$2
dr_server=$3
ARCH_APPLY_THRESH=$4
if [[ -z $ARCH_APPLY_THRESH ]]
then
 ARCH_APPLY_THRESH=2
fi

PRMY_MAX_ARCH=$(ssh -nq $primary_server "$script_dir/check_arch_gap_standby.ksh $ORACLE_SID" | awk '{print $NF}')
STBY_MAX_ARCH=$(ssh -nq $dr_server "$script_dir/get_standby_max_arch_seq.ksh $ORACLE_SID" | awk '{print $NF}')

typeset -i PRMY_MAX_ARCH
if [ $PRMY_MAX_ARCH -eq 0 ]
then
 print 'failure' >> $log_dir/consecutive_failures_primary_${ORACLE_SID}.log
 failure_count_primary=$(wc -l $log_dir/consecutive_failures_primary_${ORACLE_SID}.log | awk '{print $1}')
 if [ $failure_count_primary -gt 1 ]
 then
  mailx -s "$(hostname): ${ORACLE_SID} primary database unreachable" $mailrecipients<<EOF
.
EOF
 fi
 exit 2
fi
rm -f $log_dir/consecutive_failures_primary_${ORACLE_SID}.log

typeset -i STBY_MAX_ARCH
if [ $STBY_MAX_ARCH -eq 0 ]
then
 print 'failure' >> $log_dir/consecutive_failures_standby_${ORACLE_SID}.log
 failure_count_standby=$(wc -l $log_dir/consecutive_failures_standby_${ORACLE_SID}.log | awk '{print $1}')
 if [ $failure_count_standby -gt 1 ]
 then
  mailx -s "$(hostname): ${ORACLE_SID} standby database unreachable" $mailrecipients<<EOF
.
EOF
 fi
 exit 3
fi
rm -f $log_dir/consecutive_failures_standby_${ORACLE_SID}.log

print "Last archived log sequence on Primary is : ${PRMY_MAX_ARCH}"
print "Last archived log sequence on Standby is : ${STBY_MAX_ARCH}"

typeset -i behind

if [ $(( $PRMY_MAX_ARCH - $STBY_MAX_ARCH )) -gt $ARCH_APPLY_THRESH ]
then
# mail -s "$(hostname): ${ORACLE_SID} standby database archive gap at $(date)" $mailrecipients<<EOF
# ERROR: Standby archive is falling too far behind the primary. 
# Primary: $(print $primary_server)
# Standby: $(print $dr_server)
# Number of archive files falling behind : $(( $PRMY_MAX_ARCH - $STBY_MAX_ARCH ))
#EOF
(( behind = $PRMY_MAX_ARCH - $STBY_MAX_ARCH ))
print "$primary_server $dr_server $ORACLE_SID $(date): $behind" >> $log_dir/dr_test_${primary_server}_${dr_server}.log
 exit 4
fi

exit 0
