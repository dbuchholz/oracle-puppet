#!/bin/ksh
##############################################################################################################
#Author: Nirmal S Arri																						 #
#Date  : 01/10/2012																							 #
#Usage : GlobalPasswd_Change_IMP.sh <USERNAME> <PASSWORD> 												     #
#Purpose: Change password of a user on all the IMP environment(TEST/PROD)                                    #
#         /u01/app/oracle/local/dba/Auditing/reports/UM/<SID> directory.                                     #
##############################################################################################################
#set -x

NARG=$#
NumUsers=`expr ${NARG} - 1`
echo $*
if [ ${NARG} -ne 2 ]; then
        clear
        echo ""
        echo "Not enough parameter..."
        echo ""
        echo "Usage: ./GlobalPasswd_Change.sh <USERNAME> <PASSWORD> "
        echo ""
        exit 1
fi

UserName=$1
UserPWD=$2

typeset -u v2
v2=${UserName}
UserName=${v2}

username_str='ARCH CTRLCTR CASHDBO CTRLCTR DATAEXCHDBO DATAMARTDBO EAGLEKB EAGLEMART EAGLEMGR EAGLE TEMP ESTAR HOLDINGDBO LEDGERDBO MSGCENTER DBO PACE MASTERDBO PERFORMDBO PRICING RULESDBO SCRUBDBO SECURITYDBO SYS SYSTEM TRADESDBO'

for line in `echo ${username_str}`; do
typeset -u v1
v1=${line}
s1=${v1}
 if [[ $s1 == *$UserName* ]]
   then
     echo "You cannot have username with word $s1 "
     exit 1
 fi

done

. /usr/bin/setsid.sh infprd1


Sid_List=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!

set heading off
set feedback off
set pagesize 0
select d.sid from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
where m.instance=d.mac_instance and u.db_instance=d.instance
and u.username='SYSTEM' and d.sid not like '%mfc%' and d.dataguard='N' and environment in ('TEST','DEV')
group by d.sid order by 1;

exit
!`
touch UserPWD_${UserName}_Check_Exists.log

for line in `echo ${Sid_List}`; do

#### Get the Password #########

SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
set heading off
set feedback off
set pagesize 0
select decode(inf_monitor.bb_get(u.temp_code),'',inf_monitor.bb_get(u.code),inf_monitor.bb_get(u.temp_code)) as code from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' group by d.sid,inf_monitor.bb_get(u.temp_code),inf_monitor.bb_get(u.code) order by 1;
exit
!`

#SysPwd=`$ORACLE_HOME/bin/sqlplus -s "/ as sysdba" <<!
#set heading off
#set feedback off
#set pagesize 0
#select inf_monitor.bb_get(u.code) from inf_monitor.databases d, inf_monitor.machines m, inf_monitor.user_codes u
#where m.instance=d.mac_instance and u.db_instance=d.instance and u.username='SYSTEM' and d.sid not like '%mfc%' and d.sid='${line}' 
#group by d.sid,inf_monitor.bb_get(u.code) order by 1;
#exit
#!`


#### Create if user doesn't exist

$ORACLE_HOME/bin/sqlplus -s system/${SysPwd}@${line} <<!

spool  UserPWD_${UserName}_Check_Exists.log append
DECLARE
v_count       INTEGER := 0;
sql_stmt   VARCHAR2 (1000);
BEGIN

SELECT COUNT(*) INTO v_count FROM dba_users WHERE username = UPPER ('${UserName}');

IF v_count = 0
THEN

sql_stmt := 'create user ${UserName} PROFILE EA_PROFILE IDENTIFIED BY ${UserPWD} PASSWORD EXPIRE DEFAULT TABLESPACE USERS QUOTA UNLIMITED ON USERS TEMPORARY TABLESPACE  TEMP ACCOUNT UNLOCK';

execute immediate sql_stmt;
execute immediate 'GRANT connect, create session TO ${UserName}';
execute immediate 'GRANT EA_INSTALL_ROLE TO ${UserName}';
execute immediate 'GRANT DBA TO ${UserName}';
execute immediate 'audit all by ${UserName} by access';
execute immediate 'audit update table, insert table, delete table, execute procedure by ${UserName} by access';

ELSE
 sql_stmt := 'alter user ${UserName} identified by ${UserPWD} account unlock password expire';
execute immediate sql_stmt;
END IF;

v_count := 0;

EXCEPTION
 WHEN OTHERS
   THEN
       DBMS_OUTPUT.put_line (SQLERRM);
       DBMS_OUTPUT.put_line ('   ');
END;
/

spool off
!

done


exit 0

