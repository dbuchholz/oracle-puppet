#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines order by ip_address;
EOF

rm -f list_backups_running.txt
while read name ip_address
do
 running=$(ssh -nq $ip_address ps -ef | grep 'RMANBackup\.sh' | grep 'sh -c' | awk '{print $11}')
 if [[ -n $running ]]
 then
  print "$name $running" >> list_backups_running.txt
 fi
done<machine_info.txt
if [[ -f list_backups_running.txt ]]
then
 print "The following backups are still running:\n"
 cat list_backups_running.txt
else
 print "No backups running."
fi
exit 0
