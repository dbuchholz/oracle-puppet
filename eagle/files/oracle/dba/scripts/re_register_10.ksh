#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
script_dir=/u01/app/oracle/local/dba/scripts
rman_dir=/u01/app/oracle/local/dba/backups/rman
rman_script=rman_move_backup_location.ksh

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$script_dir/machine_and_10_db_info.txt
set pages 0 trimspool on feedback off echo off
col ip_address format a17
col name format a15
col sid format a8
select m.ip_address,d.sid,m.name
from inf_monitor.machines m,inf_monitor.databases d
where m.instance=d.mac_instance and
d.oracle_version not like '9%' order by m.ip_address;
EOF

awk '{print $1}' $script_dir/machine_and_10_db_info.txt | sort -u > $script_dir/only_machines.txt

while read machine
do
 scp -q $rman_dir/$rman_script ${machine}:$rman_dir/$rman_script
 scp -q $rman_dir/rman_delete_obsolete.ksh ${machine}:$rman_dir/rman_delete_obsolete.ksh
done<$script_dir/only_machines.txt

while read ip_address sid name
do
 ssh -nq $ip_address $rman_dir/rman_delete_obsolete.ksh $sid
 if [ $? -ne 0 ]
 then
  print "Will not run re-register for $sid as the delete obsolete failed."
  continue
 fi
 ssh -nq $ip_address $rman_dir/$rman_script $sid
done<machine_and_10_db_info.txt

exit 0
