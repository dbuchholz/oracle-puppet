#!/bin/ksh
export ORACLE_SID=usbtst1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
startup nomount
spool create_db.log
CREATE DATABASE usbtst1
   USER SYS IDENTIFIED BY change_on_install
   USER SYSTEM IDENTIFIED BY manager
   LOGFILE GROUP 1 ('/usbtst1-01/oradata/redologs/redo01a.log','/usbtst1-02/oradata/redologs/redo01b.log') SIZE 500M,
           GROUP 2 ('/usbtst1-01/oradata/redologs/redo02a.log','/usbtst1-02/oradata/redologs/redo02b.log') SIZE 500M,
           GROUP 3 ('/usbtst1-01/oradata/redologs/redo03a.log','/usbtst1-02/oradata/redologs/redo03b.log') SIZE 500M,
           GROUP 4 ('/usbtst1-01/oradata/redologs/redo04a.log','/usbtst1-02/oradata/redologs/redo04b.log') SIZE 500M
    MAXLOGFILES 10
    MAXLOGMEMBERS 4
    MAXDATAFILES 1000
    MAXINSTANCES 1
    MAXLOGHISTORY 8764
   CHARACTER SET WE8ISO8859P1
   NATIONAL CHARACTER SET AL16UTF16
   DATAFILE '/usbtst1-03/oradata/system01.dbf' SIZE 200M REUSE AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
   SYSAUX DATAFILE '/usbtst1-01/oradata/sysaux01.dbf' SIZE 20M REUSE AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL
   DEFAULT TEMPORARY TABLESPACE temp TEMPFILE '/usbtst1-01/oradata/temp01.dbf' SIZE 20M REUSE AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED
   UNDO TABLESPACE undotbs1 DATAFILE '/usbtst1-02/oradata/undotbs01.dbf' SIZE 10M REUSE AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED;
@?/rdbms/admin/catalog.sql
@?/rdbms/admin/catproc.sql
connect system/manager
@?/sqlplus/admin/pupbld.sql
spool off
EOF
exit 0
