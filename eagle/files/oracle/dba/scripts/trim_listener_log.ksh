#!/bin/ksh
ostype=$(uname)

# The script will not run if it does not find exactly one listener running.
(( number_of_listeners=$(ps -ef |grep tnslsnr | grep -v grep | wc -l | awk '{print $1}') ))
if [ $number_of_listeners -ne 1 ]
then
 print "There must be exactly one listener running on the host $the_host"
 exit 1
fi

# Get the Oracle Home where the current listener is running.
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 export ORACLE_HOME=$(ps -ef |grep tnslsnr | grep -v grep | head -1 | awk '{print $8}' | sed 's/\/bin.*$//g')
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 export ORACLE_HOME=$(ps -ef |grep tnslsnr | grep -v grep | head -1 | awk '{print $9}' | sed 's/\/bin.*$//g')
fi
# Get the location of the listener log.  It is different for Oracle 10g and lower as compared to Oracle 11g.
$ORACLE_HOME/bin/lsnrctl<<EOF>/tmp/listener_log_file.txt
show log_directory
show log_file
EOF
(( db_version=$(print $ORACLE_HOME | sed 's/^.*product//g' | awk -F/ '{print $2}' | awk -F. '{print $1}') ))
listener_dir=$(grep log_directory /tmp/listener_log_file.txt | awk '{print $NF}')
if [ $db_version -gt 10 ]
then
 listener_log=$(grep log_file /tmp/listener_log_file.txt | awk '{print $NF}')
else
 listener_file=$(grep log_file /tmp/listener_log_file.txt | awk '{print $NF}')
 listener_log="${listener_dir}$listener_file"
fi
tail -200000 $listener_log > ${listener_log}_temp
cat /dev/null > $listener_log
cat ${listener_log}_temp >> $listener_log
rm -f ${listener_log}_temp

if [ $db_version -lt 11 ]
then
$ORACLE_HOME/bin/lsnrctl<<EOF>/dev/null
set log_file dummy.log
set log_file listener.log
EOF
rm ${listener_dir}dummy.log
fi

exit 0
