#!/bin/ksh
time_resolution=288 # 5 minutes between samples = 24 hours X 12 5-minute intervals per hour = 288, 1 min = 1440
how_far_back=288    # From present time. 1 day = 24 hours X 12 5-minute intervals per hour = 288, 2 days = 576, 3 days = 864 etc.
starting_back=0     # Number of days to go back as ending point. 0=the current time as ending point
# So to check from 5 days ago to 3 days ago, how_far_back=5X24X12=1440 and starting_back=3
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
SCRIPT_DIR=/u01/app/oracle/local/dba/scripts
rm -f $SCRIPT_DIR/last_twenty_four_hours.txt $SCRIPT_DIR/reverse_last_twenty_four_hours.txt
for count in {1..$how_far_back}
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$SCRIPT_DIR/reverse_last_twenty_four_hours.txt
set lines 200 pages 0 feedback off trimspool on echo off
alter session set nls_date_format='MM-DD-YYYY HH24:MI:SS';
select sysdate-$starting_back-$count/$time_resolution from dual;
EOF
done
tac $SCRIPT_DIR/reverse_last_twenty_four_hours.txt > $SCRIPT_DIR/last_twenty_four_hours.txt
rm -f $SCRIPT_DIR/reverse_last_twenty_four_hours.txt $SCRIPT_DIR/backup_volume_output.txt
while read the_timestamp
do
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>>$SCRIPT_DIR/backup_volume_output.txt
set lines 200 pages 0 feedback off trimspool on echo off
alter session set nls_date_format='MM-DD-YYYY HH24:MI:SS';
select '$the_timestamp'||' '||count(*) from inf_monitor.backup_runs b,inf_monitor.databases d,inf_monitor.machines m
where b.db_instance=d.instance and d.mac_instance=m.instance and
m.location like 'EVERETT%' and
start_time < '$the_timestamp' and end_time > '$the_timestamp';
EOF
done<$SCRIPT_DIR/last_twenty_four_hours.txt
exit 0
