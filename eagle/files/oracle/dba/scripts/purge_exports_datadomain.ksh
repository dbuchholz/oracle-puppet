#!/bin/ksh
purge_location=/datadomain/export
purge_older_than_days=577
find $purge_location \( -name "*.log" -o -name "*.dmp*" \) -mtime +$purge_older_than_days -print -exec rm -f {} \;
exit 0
