#!/bin/ksh
hst=$(hostname | awk -F. '{print $1}')
df -Ph | \
sed "s/^/$hst /g" | \
egrep -v '\/var|\/tmp|\/boot| tmpfs |\/mnt|\/mnt2|Filesystem| \/$|datadomain|\/u01' | \
awk '{printf "%-12s %-35s %6s %-20s\n",$1,$2,$3,$NF}' | sort -k4
exit 0
