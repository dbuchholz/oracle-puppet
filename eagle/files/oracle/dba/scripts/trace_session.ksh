#!/bin/ksh
# K-Shell is used to set the SQL trace for a session.
# Usage : trace_session.ksh <ORACLE_SID> <UNIXPID> <ON|OFF>
# ORACLE_SID - Oracle Database SID
# UNIXPID - OS PID
# ON|OFF - Set the trace on/off
#
# Author : Senthil Subramanian
# Date   : 16-AUG-2001

# Main script starts from here...

export PATH=$PATH:/usr/bin:/usr/local/bin:/usr/sbin

TODAY=`date "+%Y-%m-%d"`
CURRDIR=`dirname $0`
LOGDIR=$CURRDIR
export ORAENV_ASK=NO
export ORACLE_SID=$1
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
export EXEDIR=${ORACLE_HOME}/bin

if [[ "$2" = "-i" ]]
then
   clear
   print "Starting interactive session tracing..."
   print You can give any search value for
   read SRCH_VAL?"OS_PROCESS, SID, SERIAL#, STATUS, USERNAME,OSUSER, PROGRAM, or MACHINE : "
   
   $EXEDIR/sqlplus -s '/ as sysdba' <<-EOF
	set lines 105
	col username   for a12 trunc
	col osuser     for a10 trunc
	col program    for a25 trunc
	col machine    for a15 trunc
	col sid        for 9990
	col serial#    for 99990
	col spid       for a10 head "ospid"

	select p.spid,
	       s.sid,
	       s.serial#,
	       s.status,
	       s.username,
	       s.osuser,
	       s.program,
	       s.machine
	from  v\$session s,
	      v\$process p
	where s.paddr = p.addr
	and   s.username IS NOT NULL
	and   s.sid != (select sid from v\$mystat where rownum = 1)
	and   (p.spid             like '%${SRCH_VAL}%' or
	       TO_CHAR(s.sid)     like '%${SRCH_VAL}%' or
	       TO_CHAR(s.serial#) like '%${SRCH_VAL}%' or
	       UPPER(s.status)   like UPPER('%${SRCH_VAL}%') or
	       UPPER(s.username) like UPPER('%${SRCH_VAL}%') or
	       UPPER(s.osuser)   like UPPER('%${SRCH_VAL}%') or
	       UPPER(s.program)  like UPPER('%${SRCH_VAL}%') or
	       UPPER(s.machine)  like UPPER('%${SRCH_VAL}%'))
	order by s.sid,p.spid ;
	EOF
   read UNIXPID?"Enter a OSPID to trace from the above result : "
   read TRC?"Enter trace action [ON|OFF] : "
fi

if [[ "${TRC:=$3}" = "ON" ]]
then
   echo "Setting SQL Trace on for SID ${UNIXPID:=$2} in ${ORACLE_SID}"
   $EXEDIR/sqlplus -s '/ as sysdba' <<-EOD
	WHENEVER SQLERROR EXIT FAILURE ;
	oradebug setospid $UNIXPID
	oradebug unlimit
	-- TRUN ON SQLTRACE :
        oradebug event 10046 trace name context forever, level 12
	EOD
   if [ $? -ne 0 ]
   then
      echo "Error...SQL Trace for SID ${UNIXPID} in ${ORACLE_SID} is not set"
      exit 1
   fi
elif [[ "${TRC:=$3}" = "OFF" ]]
then
   echo "Setting SQL Trace off for SID ${UNIXPID:=$2} in ${ORACLE_SID}"
   $EXEDIR/sqlplus -s '/ as sysdba' <<-EOD | tail -1 | grep -v 'Statement processed\.' | tee $LOGDIR/trc_name.lst
	WHENEVER SQLERROR EXIT FAILURE ;
	oradebug setospid $UNIXPID
	-- TURN OFF SQLTRACE 
        oradebug event 10046 trace name context off
        oradebug tracefile_name
	EOD
   if [ $? -ne 0 ]
   then
      echo "Error...SQL Trace for SID ${UNIXPID} in ${ORACLE_SID} is not set"
      exit 1
   fi
else
   print "Invalid Arguments!"
   usage
fi

if [ $TRC = "OFF" ]
then
   if ! [ -s $LOGDIR/trc_name.lst ]
   then
      print "No trace file found.  Session could be INACTIVE during the trace!"
      exit 1
   else
      print "Trace File Name : $(cat $LOGDIR/trc_name.lst)"
      TRCFILE=$(cat $LOGDIR/trc_name.lst)
      \rm -f $LOGDIR/trc_name.lst
   fi
   if [ -f ${TRCFILE} ]
   then
      TRCOUT=$(basename ${TRCFILE} | awk -F\. '{print $1".out"}')
      echo "Generating tkprof output...."
      $EXEDIR/tkprof $TRCFILE $LOGDIR/$TRCOUT sys=no sort=fchela > /dev/null
      if [ $? -ne 0 ]
      then
         echo "Error while runnign TKPROF..."
         exit 1
      fi
      print "TKPROF output stored in the file $LOGDIR/$TRCOUT"
   else
      print "$TRCFILE not found or doesn't exist!"
      exit 2
   fi
fi
exit 0
