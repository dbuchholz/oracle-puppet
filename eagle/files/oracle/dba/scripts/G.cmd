#
=head1  GatherSQLStatements.cmd

=head2  ABSTRACT:
        GatherSQLStatements.cmd keeps track of high resource consuming SQL

=head2  ARGUMENTS:

=head2  REVISIONS:
		FBD 10-28-2011   Initial Development

=cut


use strict;
use DBI;
use Net::FTP;
use Text::ParseWords;
use File::Basename;
use Sys::Hostname;

use lib "$ENV{HOME}/local/inf/lib";
use lib "$ENV{HOME}/local/dba/perllib";
use ITSFatal;
use ITSCode;
use ITSDate;
use ITSTrim;
use DBLib;

# Global Variables
my $ProgramName = "G.cmd";
my $BOX = hostname();
my $USER =  getlogin() || (getpwuid($<))[0] || "N/A";

# ------------------------------------------------------------------------------------------------------------
# Subroutine:  QueryDatabase
#
#	This subroutine creates a list of all open databases running STAR and Pace
#
#	ARGUMENTS:
#        0: Database Handle (input)
#        1: Statement Handle (input)
#        2: Log File Handle (input)
#
# ------------------------------------------------------------------------------------------------------------
sub QueryDatabase ($$$$)
{

	my $dbh = $_[0];			# Database Handle input
	my $sth = $_[1];			# Statement Handle input
	my $fh_logfile = $_[2];			# Logfile Handle
	my $SelectStatement = $_[3];

	my $sql;

	# Prepare the SQL Statement
	$sql = qq{ $SelectStatement };    # Prepare and execute SELECT
	$sth = $dbh->prepare($sql);
	if (!$sth) {
    		print LOGFILE "Unable to prepare sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
    		NotifyFatal ($fh_logfile, $ProgramName, "Unable to prepare sql statement $SelectStatement  \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
    		return (-1);
	}

	# Execute the SQL Statement
	$sth->execute();
	if (!$sth) {
		print LOGFILE "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
		NotifyFatal ($fh_logfile, $ProgramName, "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
		$sth->finish();
		return (-1);
	}
	$_[1] = $sth;
}

# ------------------------------------------------------------------------------------------------------------

# Set up Environment variables
my $HOME_DIR = $ENV{HOME};                              # Home directory
my $WORKING_DIR = $HOME_DIR . "/local/inf/gather";      # Working Directory
my $LOG_DIR = $WORKING_DIR . "/logs";                   # Log Directory
#my $DefaultMail = "TEAM_DBA";
my $DefaultMail = "fdavis";
my $BoxName;
my @BoxList;

@BoxList = split (/\./, $BOX);
$BoxName = $BoxList[0];


# Variables used with DBI
my ($dbh,$dbh2);						# Database Handles
my ($sth,$sth2,$sth3);                        # Statement handles
my $sql; 
my $UpdateStatement;                    # Update statement to run
my $UpdateStatementStart;               # Used to generate Update statement
my $UpdateStatementEnd;               	# Used to generate Update statement
my $InsertStatement;					# Used to insert Large Table records
my $SelectStatement;					# Used to generate Select statments
my $Update;								# Used to indicate if an update is required
my $Message;							# Message strings from database library
my $RetStatus;
$dbh->{'LongTruncOk'} = 1; #use 1 if it is ok to trunc data
$dbh->{'LongReadLen'} = 10000000;

# Database Fields from Databases table
my ($F_OWNER, $F_TABLE_NAME, $F_NUM_ROWS);
my ($F_INSTANCE,$F_SID,$F_MAC_INSTANCE,$F_CLIENT,$F_DATAGUARD,$F_STATUS,$F_TNSNAME,$F_ENVIRONMENT,$F_ORACLE_VERSION,$F_PACE_VERSION,$F_STAR_VERSION,$F_VIP,$F_DBID,$F_CREATION_DATE,$F_UPDATE_DATE,$F_UPDATE_USER,$F_START_TIME);
# Usercode and Tempcode fields from UserCodes table
my ($F_CODE,$F_TEMP_CODE);
my ($F_SQL_ID,$F_ELAPSED,$F_CPU,$F_ROWS_PROCESSED,$F_IOWAIT,$F_APWAIT,$F_CCWAIT,$F_DIRECT_WRITES,$F_DISK_READS,$F_EXECUTIONS,$F_BUFFER_GETS,$F_SQL_TEXT);

my $Password;
my $mac_instance;
# Fields retrieved from client database
my $F_CRON_INSTANCE;

# Database variables
my $DatabaseUser = "inf_monitor";
my $DatabaseCode = "t\\5OT|Q>D]";
my $DatabaseSID = "inftst1";
my $dval;

# Get Codes
$dval = "";
for (my $idx = 33; $idx <= 126; $idx+=7) {
    $dval .= chr($idx);
}
DecodeData ($dval, $DatabaseCode);

# Declare date/time variables and get Current Date
my ($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
$Year += 1900; $Month++;
$Month = $Month < 10 ? "0$Month" : "$Month";
$Day = $Day < 10 ? "0$Day" : "$Day";
my $CurrentDate = $Year.$Month.$Day;
$Hour = $Hour < 10 ? "0$Hour" : "$Hour";
$Minute = $Minute < 10 ? "0$Minute" : "$Minute";
$Second = $Second < 10 ? "0$Second" : "$Second";
my $CurTime = $Hour . ":" . $Minute;
my $CurrentTime6 = $Hour . $Minute . $Second;

# File names and handles
my $LogFileName;			# Log File
my $fh_logfile;				# Log File File handle

# Open Log File
$LogFileName = $LOG_DIR."/".(substr($ProgramName,0,(index($ProgramName,"."))))."_".$CurrentDate."_".$CurrentTime6.".log";
open(LOGFILE, ">> $LogFileName") || NotifyFatal ("", $ProgramName, "Could not open log file $LogFileName", "fdavis", "DIE");
$fh_logfile = *LOGFILE;
$fh_logfile->autoflush(1);

# Connect to the Infrastructure database
$dbh = DBI->connect( 'dbi:Oracle:'.$DatabaseSID,
			$DatabaseUser,
			$DatabaseCode,
			 { PrintError => 1, AutoCommit => 0 }
			   ) ;
if (!$dbh) {
	print LOGFILE "Unable to Connect to $DatabaseSID: \n $DBI::errstr";
	close LOGFILE;
	NotifyFatal ($fh_logfile, $ProgramName, "Unable to Connect to $DatabaseSID: \n $DBI::errstr", $DefaultMail, "DIE");
}

$SelectStatement = "select instance from machines where name = '$BoxName'";
$sql = qq{ $SelectStatement };    # Prepare and execute SELECT
$sth3 = $dbh->prepare($sql);
if (!$sth3) {
    print LOGFILE "Unable to prepare sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
    NotifyFatal ($fh_logfile, $ProgramName, "Unable to prepare sql statement $SelectStatement  \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
    return (-1);
}
# Execute the SQL Statement
$sth3->execute();
if (!$sth3) {
    print LOGFILE "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
    NotifyFatal ($fh_logfile, $ProgramName, "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
    return (-1);

}
$sth3->bind_columns (\$mac_instance);
$sth3->fetch();
$sth3->finish();

# Update Infrastructure Database
# See if this job has run at all this day
$SelectStatement = "select instance from MACHINE_CRON_JOB_RUNS where trim(script_name)='$ProgramName' and start_time > to_char(sysdate,'DD-MON-YYYY') and mac_instance=$mac_instance";
$sql = qq{ $SelectStatement };    # Prepare and execute SELECT
$sth = $dbh->prepare($sql);
if (!$sth) {
    print LOGFILE "Unable to prepare sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
    NotifyFatal ($fh_logfile, $ProgramName, "Unable to prepare sql statement $SelectStatement  \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
    return (-1);
}
# Execute the SQL Statement
$sth->execute();
if (!$sth) {
    print LOGFILE "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
    NotifyFatal ($fh_logfile, $ProgramName, "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
    return (-1);

}
$sth->bind_columns (\$F_CRON_INSTANCE);
$sth->fetch();

if (defined ($F_CRON_INSTANCE)) {
    $UpdateStatement = "update machine_cron_job_runs set start_time=sysdate,number_of_runs=number_of_runs+1,end_time='',run_time='' where instance=$F_CRON_INSTANCE";
}
else {
    $UpdateStatement = "insert into machine_cron_job_runs values (machine_cron_job_runs_seq.nextval,'$ProgramName',sysdate,'','',1,'','',$mac_instance) ";
}
$RetStatus = DBUpdate ($dbh, $UpdateStatement, $Message);
if ($RetStatus < 0) {
    NotifyMessage ($fh_logfile, $ProgramName,"Oracle Error:\n $UpdateStatement - $Message","fdavis");
}

$SelectStatement="Select d.instance,d.sid,d.client,d.dataguard,d.status,d.tnsname,d.environment,d.oracle_version,d.pace_version,d.star_version,d.vip,d.dbid,d.creation_date,d.update_date,d.update_user,bb_get(u.code), bb_get(u.temp_code),to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS') from databases d, user_codes u where d.instance=u.db_instance and upper(u.username)='SYS' and dataguard='N' and d.client not in (36,38,39) and d.sid in ('agfdev1','agftst1')";
QueryDatabase ($dbh, $sth, $fh_logfile,$SelectStatement);
$sth->bind_columns (\$F_INSTANCE,\$F_SID,\$F_CLIENT,\$F_DATAGUARD,\$F_STATUS,\$F_TNSNAME,\$F_ENVIRONMENT,\$F_ORACLE_VERSION,\$F_PACE_VERSION,\$F_STAR_VERSION,\$F_VIP,\$F_DBID,\$F_CREATION_DATE,\$F_UPDATE_DATE,\$F_UPDATE_USER,\$F_CODE,\$F_TEMP_CODE,\$F_START_TIME);

print LOGFILE ("$ProgramName started at: $CurTime \n");
my $Errors=0;
while( $sth->fetch() ) {
	$Update=0;

	if (defined ($F_TEMP_CODE) )  {
		$Password=$F_TEMP_CODE;
	}
	else {
		$Password=$F_CODE;
	}

	# Connect to the client database
	$dbh2 = DBI->connect( 'dbi:Oracle:'.$F_TNSNAME,
				'SYS', $Password, 
				 { ora_session_mode => 2, PrintError => 1, AutoCommit => 0 }
				   ) ;
	if (!defined($dbh2) ) {
		print LOGFILE "Unable to Connect to $F_SID as SYS using tnsname $F_TNSNAME: \n $DBI::errstr \n";
		$Errors += 1;
		next;
	}

	# Query client database to get the values
	$SelectStatement = "select * from (select stat.sql_id as SQL_ID, sum(elapsed_time_delta)/1000000 as ELAPSED,sum(cpu_time_delta)/1000000 as CPU,sum(rows_processed_delta) as ROWS_PROCESSED,sum(iowait_delta)/1000000 as IOWAIT,sum(apwait_delta)/1000000 as APWAIT,sum(ccwait_delta)/1000000 as CCWAIT,sum(direct_writes_delta) as DIRECT_WRITES,sum(disk_reads_delta) as DISK_READS,sum(executions_delta) as EXECUTIONS,NVL(sum(buffer_gets_delta),0) as BUFFER_GETS,(select substr(st.sql_text,1,60) from dba_hist_sqltext st where st.dbid=stat.dbid and st.sql_id=stat.sql_id) as SQL_TEXT from dba_hist_sqlstat stat,dba_hist_sqltext text,dba_hist_snapshot snap where stat.sql_id=text.sql_id and stat.dbid=text.dbid and snap.SNAP_ID = stat.SNAP_ID and snap.begin_interval_time > (sysdate-1) group by stat.dbid, stat.sql_id) where ELAPSED>0 and ROWNUM <= 10";
	QueryDatabase ($dbh2, $sth2, $fh_logfile,$SelectStatement);
	$sth2->bind_columns (\$F_SQL_ID, \$F_ELAPSED, \$F_CPU, \$F_ROWS_PROCESSED, \$F_IOWAIT, \$F_APWAIT, \$F_CCWAIT, \$F_DIRECT_WRITES, \$F_DISK_READS, \$F_EXECUTIONS, \$F_BUFFER_GETS, \$F_SQL_TEXT);
	while ( $sth2->fetch() ) {
		$InsertStatement = "insert into TOP_SQL_DATA values ($F_INSTANCE,'$F_SQL_ID',$F_ELAPSED,$F_CPU,$F_ROWS_PROCESSED,$F_IOWAIT,$F_APWAIT,$F_CCWAIT,$F_DIRECT_WRITES,$F_DISK_READS,$F_EXECUTIONS,$F_BUFFER_GETS,sysdate) ";
		$RetStatus = DBUpdate ($dbh, $InsertStatement, $Message);
		if ($RetStatus < 0) {
			NotifyMessage ($fh_logfile, $ProgramName,"Oracle Error:\n $InsertStatement - $Message","fdavis");
			last;
		}
    }
	QueryDatabase ($dbh2, $sth2, $fh_logfile,$SelectStatement);
	$sth2->bind_columns (\$F_SQL_ID, \$F_ELAPSED, \$F_CPU, \$F_ROWS_PROCESSED, \$F_IOWAIT, \$F_APWAIT, \$F_CCWAIT, \$F_DIRECT_WRITES, \$F_DISK_READS, \$F_EXECUTIONS, \$F_BUFFER_GETS, \$F_SQL_TEXT);
	while ( $sth2->fetch() ) {
        $F_SQL_TEXT =~ s/'/''/g;
		$InsertStatement = "insert into TOP_SQL_TEXT values ('$F_SQL_ID','$F_SQL_TEXT') ";
		$RetStatus = DBUpdate ($dbh, $InsertStatement, $Message);
		if ($RetStatus < 0) {
			NotifyMessage ($fh_logfile, $ProgramName,"Oracle Error:\n $InsertStatement - $Message","fdavis");
			last;
		}
	}
	$sth2->finish();
    $dbh2->disconnect();

}


# Update Infrastructure Database
if (!defined ($F_CRON_INSTANCE)) {
    $SelectStatement = "select instance from MACHINE_CRON_JOB_RUNS where trim(script_name)='$ProgramName' and start_time > to_char(sysdate,'DD-MON-YYYY') and mac_instance=$mac_instance";
    $sql = qq{ $SelectStatement };    # Prepare and execute SELECT
    $sth = $dbh->prepare($sql);
    if (!$sth) {
        print LOGFILE "Unable to prepare sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
        NotifyFatal ($fh_logfile, $ProgramName, "Unable to prepare sql statement $SelectStatement  \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
        return (-1);
    }
    # Execute the SQL Statement
    $sth->execute();
    if (!$sth) {
        print LOGFILE "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n";
        NotifyFatal ($fh_logfile, $ProgramName, "Unable to execute sql statement $SelectStatement \n $DBI::errstr\n Box:  $BOX User:  $USER \n", "fdavis", "NOT");
        return (-1);

    }
    $sth->bind_columns (\$F_CRON_INSTANCE);
    $sth->fetch();
}

if (defined ($F_CRON_INSTANCE)) {
    $UpdateStatement = "update machine_cron_job_runs set end_time=sysdate where instance = $F_CRON_INSTANCE";
    $RetStatus = DBUpdate ($dbh, $UpdateStatement, $Message);
    if ($RetStatus < 0) {
        NotifyMessage ($fh_logfile, $ProgramName,"Oracle Error:\n $UpdateStatement - $Message","fdavis");
    }
    $UpdateStatement = "update machine_cron_job_runs set run_time= trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' || trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':' || trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09')), status='SUCCESS' where instance = $F_CRON_INSTANCE";
    $RetStatus = DBUpdate ($dbh, $UpdateStatement, $Message);
    if ($RetStatus < 0) {
        NotifyMessage ($fh_logfile, $ProgramName,"Oracle Error:\n $UpdateStatement - $Message","fdavis");
    }
}

($Second, $Minute, $Hour, $Day, $Month, $Year) = (localtime)[0..5];
$CurTime = $Hour . ":" . $Minute;
print LOGFILE ("${ProgramName} ended at: $CurTime \n");

close LOGFILE;

$dbh->disconnect();
1;



