#!/bin/ksh
export PATH=/usr/local/bin:$PATH

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
export ORACLE_SID=caltrnd
export ORAENV_ASK=NO
. /usr/local/bin/oraenv > /dev/null
export LOGDIR=/datadomain/export/caltrnd
export NLS_LANG=AMERICAN_AMERICA.WE8ISO8859P1

${ORACLE_HOME}/bin/exp USERID=\"/ as sysdba\" BUFFER=1000000 FILE=$LOGDIR/caltrnd.dmp DIRECT=Y LOG=$LOGDIR/caltrnd.log \
       TABLES=\(rulesdbo.price_status,rulesdbo.price_rule_securities,rulesdbo.price_validation_status_detail,rulesdbo.pricing_batch_process,securitydbo.price_exchange,securitydbo.price,rulesdbo.price_wip,rulesdbo.fx_status,rulesdbo.fx_target,rulesdbo.fx_validation_status_detail,securitydbo.fx_rates_orig,securitydbo.fx_rates,securitydbo.forward_points\)
exit 0
