#!/bin/ksh
export ORAENV_ASK=NO
export ORACLE_SID=nwiprd1
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
stty -echo
read pwd?"Enter password for PACE_MASTERDBO: "
stty echo
$ORACLE_HOME/bin/sqlplus / as sysdba<<EOF
connect pace_masterdbo/$pwd
spool mview_refresh_pace_masterdbo.log
exec dbms_mview.refresh('GET_TRANSLATE_VALUE_FROM','C');
exec dbms_mview.refresh('GET_TRANSLATE_VALUE_TO','C');
exec dbms_mview.refresh('GET_TRANSLATED_VALUE_VIEW','C');
EOF
exit 0
