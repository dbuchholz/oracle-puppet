#!/bin/ksh
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where OS_TYPE<>'SunOS' order by name;
EOF

typeset -i mb_free_mem
typeset -i mb_total_mem
typeset -i mb_used_mem
typeset -i pct_used
typeset -i no_response
rm -f memory_info.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 3
 ((count = 0))
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  ((count=count+1))
  if [ $count -gt 2 ]
  then
   print "Did not respond in five seconds.  Killing the ssh command."
   kill -9 $the_pid
   ((no_response = 1))
   break
  fi
  sleep 1
 done
 if [ $no_response -eq 0 ]
 then
  used_mem=$(ssh -nq $ip_address grep Active /proc/meminfo | awk '{print $2/1000}')
  total_mem=$(ssh -nq $ip_address grep MemTotal /proc/meminfo | awk '{print $2/1000}')
  mb_active_mem=$(print $used_mem | awk -F. '{print $1}')
  mb_total_mem=$(print $total_mem | awk -F. '{print $1}')
  if [ $mb_total_mem != 0 ]
  then
   pct_used=$(($mb_active_mem*100/$mb_total_mem))
   print "$name $mb_total_mem $mb_active_mem $pct_used" >> memory_info.txt
  else
   print "Cannot get total memory for $name"
  fi
 fi
done<machine_info.txt

sort -nrk4 memory_info.txt > active_mem.txt
print "Server       Total       Active" > active_memory.txt
print "Name         Memory(MB)  Memory(MB)  Utilization(%)\n" >> active_memory.txt
awk '{printf "%-13s%4d%12d%14d\n",$1,$2,$3,$4}' active_mem.txt >> active_memory.txt
rm -f active_mem.txt machine_info.txt
mv active_memory.txt memory_info.txt
exit 0
