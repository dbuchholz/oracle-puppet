#!/bin/ksh
# Script Name: set_temp_passwords.ksh
# Usage: set_temp_passwords.ksh [Oracle SID]

if [ $# -ne 1 ]
then
 print "\nMust enter one parameter into this script."
 print "The parameter must be the Oracle SID.\n"
 exit 1
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi

export ORACLE_SID=$1
sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [[ -z $sid_in_oratab ]]
then
 print "There is no $ORACLE_SID entry in $ORATAB file"
 exit 2
fi

export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv > /dev/null
now=$(date +'%Y%m%d_%H%M%S')

read usn?"Enter your infprd2 Database Username: "
print " "

stty -echo
read pwd?"Enter your infprd2 Database Password: "
stty echo
clear

dr_flag=N
env_count=`$ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
set pagesize 0
select count(environment) from inf_monitor.databases where sid='$ORACLE_SID';
EOF
`
if [ "$env_count" -gt 1 ]
then
 read pord?"Is the database you want to set to Temporary password a [P]rod or [D]R. Enter: P or D: "
 if [[ -n $pord ]]
 then
  pord=$(print $pord | cut -c1 | tr '[a-z]' '[A-Z]')
 else
  print "The database is a Production or DR.  You must make a choice on which one you are working on."
  print "Aborting the script here..."
  exit 3
 fi
 if [ "$pord" != "P" ] && [ "$pord" != "D" ]
 then
  print "\nMust enter P or D when prompted."
  print "P = Prod and D = DR"
  print "Try running script again and answer prompt with P or D"
  exit 4
 fi
fi

if [[ -n $pord ]]
then
 if [ "$pord" = "D" ]
 then
  dr_flag=Y
  print "You are working on $ORACLE_SID DR Database."
  print "The passwords will be set to temporary in the DR database but Big Brother USER_CODES Table will not be modified."
 else
  print "You are working on $ORACLE_SID Production Database."
  print "The passwords will be set to temporary in the Prod database and Big Brother USER_CODES Table."
 fi
 read getout?"Do you want to continue?  Enter CTRL/C to abort script and take no action or press Enter Key to continue."
fi

$ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
WHENEVER SQLERROR EXIT FAILURE
set lines 200
col username format a30
col Permanent format a20
col Temporary format a20
select username,inf_monitor.bb_get(code) "Permanent",inf_monitor.bb_get(temp_code) "Temporary" from inf_monitor.user_codes
where db_instance=(select instance from inf_monitor.databases where sid='$ORACLE_SID' and dataguard='N');
EOF
if [ $? -ne 0 ]
then
 print "Could not make a connection to Big Brother database."
 print "Account credentials may be incorrect such as the password or username."
 exit 5
fi

if [ "$dr_flag" = "N" ]
then
 print "Next you will update the Big Brother Temporary values for $ORACLE_SID to set them all to \"eagle\"."
 read continue?"Do want to continue? Enter CTRL/C to quit or Enter to continue. "

 $ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 update inf_monitor.user_codes set temp_code=inf_monitor.bb_store('eagle') where db_instance=(select instance from inf_monitor.databases where sid='$ORACLE_SID' and dataguard='N');
EOF
 if [ $? -ne 0 ]
 then
  print "There was an error when trying to set the Big Brother temporary values for $ORACLE_SID to \"eagle\"."
  exit 6
 fi
fi

$ORACLE_HOME/bin/sqlplus -s ${usn}/${pwd}@infprd2<<EOF
WHENEVER SQLERROR EXIT FAILURE
set pagesize 0
spool set_to_temp_${ORACLE_SID}_${now}.sql
select 'alter user '||u.username||' identified by eagle;'
from inf_monitor.user_codes u,inf_monitor.databases d where u.db_instance=d.instance and d.sid='$ORACLE_SID' and d.dataguard='N'
order by u.username;
EOF
if [ $? -ne 0 ]
then
 print "There was an error when trying to generate the \"alter user\" commands to set the database passwords to \"eagle\"."
 exit 7
fi

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
WHENEVER SQLERROR EXIT FAILURE
set pagesize 999
spool password_${ORACLE_SID}_${now}.log
select name from v\$database;
select name,password from user$ order by name;
spool off
EOF
if [ $? -ne 0 ]
then
 print "There was an error when saving off existing encrypted values of password for $ORACLE_SID"
 exit 8
fi

$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
WHENEVER SQLERROR EXIT FAILURE
@set_to_temp_${ORACLE_SID}_${now}.sql
select name from v\$database;
EOF
if [ $? -ne 0 ]
then
 print "There was an error when setting database passwords for $ORACLE_SID to temporary value."
 exit 9
fi
print "Passwords successfully changed to temporary passwords."
exit 0
