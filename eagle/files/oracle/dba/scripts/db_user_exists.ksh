#!/bin/ksh
export ORACLE_SID=$1
export ORAENV_ASK=NO
username=$2
host=$(hostname | awk -F. '{print $1}')
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
set lines 200 pages 0 feedback off trimspool on echo off
select '$host $ORACLE_SID'||' '||username from dba_users where username='$username';
EOF
exit 0
