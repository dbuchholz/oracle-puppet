#!/bin/ksh
# Script name: export.ksh
# Run this script as oracle UNIX user
#
# Usage: export.ksh [Oracle SID] [Username] [Password] [backup directory] {export type} [split file size] {consistent [y|n]}
# export type is:  [normal|split|compressed|split_compressed]
#
# where split file size is a number with a letter prefix.  The prefix is "k" or "m", where k is kilobytes and 
# m is megabytes.  For example, to create an export dump file with each file of 100MB, specify split_file_size of 100m
# With the split and compressed files, the resulting compressed files will be split_file_size each.  With the split without
# compression, the split file sizes will also be split_file_size each.
# The last two parameters may be omitted; if they are omitted they will default to normal and 1000m
#***************************************************
export ORACLE_SID=$1
username=$2
password=$3
backup_dir=$4
export_type=$5
split_file_size=$6
consistent=$7
#***************************************************

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi

ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
 MKNOD=/bin/mknod
 COMPRESS="/bin/gzip"
 EXTENSION=gz
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
 MKNOD=/etc/mknod
 COMPRESS="compress"
 EXTENSION=Z
fi

sid_in_oratab=$(grep -v "^#" $ORATAB | grep -w $ORACLE_SID | awk -F: '{print $1}')
if [ $sid_in_oratab != $ORACLE_SID ]
then
 print "Cannot find Instance $ORACLE_SID in $ORATAB"
 exit 1
fi

umask 137
export today=$(date "+%Y%d%m")

# Verify that there are at least two parameters passed into this script
if [ $# -lt 4 ]
then
 print "Usage:"
 print "$(basename $0) [Oracle SID] [Username] [Password] [backup directory] {export type} [split file size] [consistent [y|n]"
 print "export type is:  [normal|split|compressed|split_compressed]"
 exit 2
fi
#
# Verify that backup directory is a valid directory or it is writable by process
if [ ! -d $backup_dir ] && [ ! -w $backup_dir ]
then
 print "${ORACLE_SID}: $backup_dir is not a directory or $(whoami) does not have write permission in directory"
 exit 3
fi
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv
if [[ ! -d $script_dir ]]
then
 mkdir -p $script_dir
fi
# Defaults if no parameters are passed into script
fifth_parameter=$5
if [[ -z $fifth_parameter ]]
then
 export_type=normal
fi
# If not a valid export type set it to the default
if [ $export_type != normal ] && [ $export_type != split ] && [ $export_type != compressed ] && [ $export_type != split_compressed ]
then
 export_type=normal
fi
#
sixth_parameter=$6
if [[ -z $sixth_parameter ]]
then
 split_file_size=1000m
fi
seventh_parameter=$7
if [[ -z $seventh_parameter ]]
then
 consistent=n
else
 consistent=y
fi
# Start database if it is down at the start of the export
db_down=0
ps -ef | grep "ora_smon_$ORACLE_SID" | grep -v grep > /dev/null
if [ $? -ne 0 ]
then
db_down=1
print "$ORACLE_SID Database is down"
print "Starting the $ORACLE_SID database to begin export"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
startup
EOF
fi

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF
 WHENEVER SQLERROR EXIT FAILURE
 set echo off feed off pages 0 trim on trims on
 spool $script_dir/nls_lang_val.lst
 SELECT lang.value || '_' || terr.value || '.' || chrset.value
 FROM   v\$nls_parameters lang,
        v\$nls_parameters terr,
        v\$nls_parameters chrset
 WHERE  lang.parameter = 'NLS_LANGUAGE'
 AND    terr.parameter = 'NLS_TERRITORY'
 AND    chrset.parameter = 'NLS_CHARACTERSET';
 spool off
EOF
if [ $? -ne 0 ]
then
 export NLS_LANG=$(cat $script_dir/nls_lang_val.lst)
fi

print "NLS_LANG is $NLS_LANG"

print "${0}: Starting the $ORACLE_SID export at $(date)"
case $export_type in
 normal)
 $ORACLE_HOME/bin/exp $username/$password file=$backup_dir/export_normal_${ORACLE_SID}_${today}.dmp direct=Y consistent=$consistent log=$script_dir/export_normal_${ORACLE_SID}_$today.log full=y;;
 split)
 rm -f /tmp/exp_pipe_$ORACLE_SID
 $MKNOD /tmp/exp_pipe_$ORACLE_SID p
 nohup split -b $split_file_size -a 1 /tmp/exp_pipe_$ORACLE_SID $backup_dir/export_split_${ORACLE_SID}_${today}.dmp_ &
 $ORACLE_HOME/bin/exp $username/$password file=/tmp/exp_pipe_${ORACLE_SID} direct=y consistent=$consistent log=$script_dir/export_split_${ORACLE_SID}_$today.log full=y;;
 compressed)
 rm -f /tmp/exp_pipe_$ORACLE_SID
 $MKNOD /tmp/exp_pipe_$ORACLE_SID p
 nohup $COMPRESS < /tmp/exp_pipe_${ORACLE_SID} > $backup_dir/export_compressed_${ORACLE_SID}_${today}.dmp.$EXTENSION &
 $ORACLE_HOME/bin/exp $username/$password file=/tmp/exp_pipe_${ORACLE_SID} direct=y consistent=$consistent RECORDLENGTH=64000 log=$script_dir/export_compressed_${ORACLE_SID}_$today.log full=y;;
 split_compressed)
 rm -f /tmp/split_pipe_$ORACLE_SID
 rm -f /tmp/compress_pipe_$ORACLE_SID
 $MKNOD /tmp/split_pipe_$ORACLE_SID p
 $MKNOD /tmp/compress_pipe_$ORACLE_SID p 
 nohup split -b $split_file_size -a 1 /tmp/split_pipe_$ORACLE_SID $backup_dir/export_split_compressed_${ORACLE_SID}_${today}.dmp_ & 
 nohup $COMPRESS < /tmp/compress_pipe_$ORACLE_SID > /tmp/split_pipe_$ORACLE_SID &
 $ORACLE_HOME/bin/exp $username/$password full=y direct=y consistent=$consistent file=/tmp/compress_pipe_$ORACLE_SID RECORDLENGTH=64000 log=$script_dir/export_split_compressed_${ORACLE_SID}_$today.log;;
esac
#
print "${0}: Export of $ORACLE_SID finished at $(date)"

if [ $db_down -eq 1 ]
then
print "Shutting down $ORACLE_SID database as it was down before export began at $(date)"
$ORACLE_HOME/bin/sqlplus '/ as sysdba'<<EOF
shutdown immediate
EOF
print "Shutting down $ORACLE_SID database completed at $(date)"
fi
ls -ltr $backup_dir
exit 0
