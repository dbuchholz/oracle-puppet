#!/bin/ksh
# Script  : datapump_schema_import.ksh <ORACLE_SID> [Parallelism]
# Purpose : To import data from an Oracle database using impdp utility.
#######################################################################################
#For schema refresh content=ALL.  For data refresh content=DATA_ONLY
content=ALL

# Dump file name(s).  Comma separate the files.  Do not use full path names, just the basename.
# Put the files in the directory defined as DATAPUMP_DIRECTORY in dba_directories.
dumpfile=export_FIRE_COL_APP_20081104_01.dmp,export_FIRE_COL_APP_20081104_02.dmp,export_FIRE_COL_APP_20081104_03.dmp
#######################################################################################

function set_parallelism
{
   indop=$(print $1 | awk -F\: '{print $2}')
   if [ -z $indop ]
   then
      DOP=$(mpstat | grep -v '^CPU' | wc -l)
   else
      DOP=$indop
   fi
   print "Setting the degree of parallelism for datapump export to ${DOP}."
   FILENAME=export_${ORACLE_SID}_${TODAY}_%U.dmpdp
   return
}

# Main script starts from here...

if [ $# -lt 1 ]
then
   print "${BOLD}\n\t\tInvalid Arguments!"
   print "\t\tUsage : $0 <ORACLE_SID> [Parallelism]\n${BOFF}"
   print "\n\t\tThe 3rd argument is optional."
   print "\n\t\t => Parallalize datapump export. Default: No Parallel"
   exit 1
fi

# Verify that the first parameter is a valid Oracle SID defined in /var/opt/oracle/oratab
sid_in_oratab=$(grep -v "^#" /var/opt/oracle/oratab | grep -w $1 | awk -F: '{print $1}')
if [ "$sid_in_oratab" != "$1" ]
then
   print "Cannot find Instance $1 in /var/opt/oracle/oratab"
   exit 2
fi
 
# Export variable declarations

if [ $(dirname $0) = "." ]
then
   SCRIPT_DIR=$(pwd)
else
   SCRIPT_DIR=$(dirname $0)
fi
typeset -x SCRIPT_DIR

export ORACLE_SID=$1
typeset -ux ORACLE_SID
export ORAENV_ASK=NO
. /usr/local/bin/oraenv
export LOGDIR=${SCRIPT_DIR}/logs/${ORACLE_SID}
export PATH=$PATH:${ORACLE_HOME}/bin
export mailrecipients=DBAS
export mailflag=FALSE
export TODAY=$(date "+%Y%d%m")

DOP=$2
if [[ -z $DOP ]]
then
 DOP=1
fi

typeset -i DOP
export LOGFILE=import_${ORACLE_SID}_${TODAY}.log

# Start database if it is down at the start of the export
((db_down=0))
ps -ef | grep "ora_[a-z0-9]\{4\}_${ORACLE_SID}" > /dev/null
if [ $? -ne 0 ]
then
   ((db_down=1))
   print "$ORACLE_SID Database is down"
   print "Starting the $ORACLE_SID database to begin export"
   $ORACLE_HOME/bin/sqlplus / as sysdba<<EOF
startup
EOF
fi

# Set the NLS_LANG env variable from database
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF
   WHENEVER SQLERROR EXIT FAILURE ;
   set echo off feed off pages 0 trim on trims on
   spool $LOGDIR/nls_lang_val.lst
   SELECT lang.value || '_' || terr.value || '.' || chrset.value
   FROM   v\$nls_parameters lang,
          v\$nls_parameters terr,
          v\$nls_parameters chrset
   WHERE  lang.parameter = 'NLS_LANGUAGE'
   AND    terr.parameter = 'NLS_TERRITORY'
   AND    chrset.parameter = 'NLS_CHARACTERSET'  ;
   spool off
EOF
if [ $? -ne 0 ]
then
   print "ERROR: When getting NLS Parameter from database."
else
   export NLS_LANG=$(cat $LOGDIR/nls_lang_val.lst)
fi

print "NLS_LANG is $NLS_LANG"
$ORACLE_HOME/bin/sqlplus -s / as sysdba<<EOF>$LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
set head off feed off lines 150 trims on pages 0 sqlp ""
select directory_path from dba_directories where directory_name='DATA_PUMP_DIR';
EOF

datapump_dump_directory=$(sed '/^$/d' $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt)
rm -f $LOGDIR/datapump_dump_directory_${ORACLE_SID}.txt
if [[ ! -d $datapump_dump_directory ]]
then
   mkdir -p $datapump_dump_directory
   chmod 755 $datapump_dump_directory
   if [ $? -ne 0 ]
   then
      print "Cannot create $datapump_dump_directory on this host."
      print "This is most likely due to the top level directory does not exist."
      print "You need to create a directory object in the database to a directory that exists."
      exit 3
   fi
fi

\rm -f $datapump_dump_directory/export_${ORACLE_SID}_${TODAY}*.dmpdp*

$ORACLE_HOME/bin/impdp / \
     CONTENT=$content \
     DIRECTORY=DATA_PUMP_DIR \
     DUMPFILE=$dumpfile
     LOGFILE=${LOGFILE} \
     JOB_NAME=IMPDP_${TODAY} \
     TABLE_EXISTS_ACTION=APPEND
#     REMAP_SCHEMA=REF2:REFDATA \
     PARALLEL=$DOP

if [ $db_down -eq 1 ]
then
   print "Shutting down $ORACLE_SID database as it was down before export began at $(date)"
   $ORACLE_HOME/bin/sqlplus / as sysdba<<EOF
shutdown immediate
EOF
   print "Shutting down $ORACLE_SID database completed at $(date)"
fi

ls -ltr $datapump_dump_directory

exit 0
