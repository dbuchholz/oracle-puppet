#!/bin/ksh
log_dir=/u01/app/oracle/local/dba/scripts/rmanlogs
ssh -nq 10.60.5.101 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cciprd1  > $log_dir/cciprd1.log 2>&1 &
ssh -nq 10.60.105.65 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfcppd1  > $log_dir/mfcppd1.log 2>&1 &
ssh -nq 10.60.5.124 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh askwsp  > $log_dir/askwsp.log 2>&1 &
ssh -nq 10.60.5.98 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dmfprd1  > $log_dir/dmfprd1.log 2>&1 &
ssh -nq 10.60.5.124 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh askdem  > $log_dir/askdem.log 2>&1 &
ssh -nq 10.60.5.197 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ixpprd1  > $log_dir/ixpprd1.log 2>&1 &
ssh -nq 10.60.52.40 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tbcprd2  > $log_dir/tbcprd2.log 2>&1 &
ssh -nq 10.60.5.167 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mimprd1  > $log_dir/mimprd1.log 2>&1 &
/u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh infprd1  > $log_dir/infprd1.log 2>&1 &
wait
ssh -nq 10.60.5.101 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh prpprd2  > $log_dir/prpprd2.log 2>&1 &
ssh -nq 10.60.5.83 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cpytrn  > $log_dir/cpytrn.log 2>&1 &
ssh -nq 10.60.5.196 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh limprd1  > $log_dir/limprd1.log 2>&1 &
ssh -nq 10.60.5.98 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccmprd1  > $log_dir/ccmprd1.log 2>&1
ssh -nq 10.60.5.158 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh calibc  > $log_dir/calibc.log 2>&1 &
ssh -nq 10.60.3.5 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dbteas  > $log_dir/dbteas.log 2>&1 &
ssh -nq 10.60.5.28 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh epnigi  > $log_dir/epnigi.log 2>&1
ssh -nq 10.60.5.187 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh sliprd1  > $log_dir/sliprd1.log 2>&1 &
ssh -nq 10.60.5.28 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh epnmdi  > $log_dir/epnmdi.log 2>&1 &
ssh -nq 10.60.5.72 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh gglbac  > $log_dir/gglbac.log 2>&1 &
ssh -nq 10.60.5.197 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh nliprd1  > $log_dir/nliprd1.log 2>&1 &
wait
ssh -nq 10.60.5.158 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh calhfl  > $log_dir/calhfl.log 2>&1 &
ssh -nq 10.60.5.98 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh lcmprd1  > $log_dir/lcmprd1.log 2>&1 &
ssh -nq 10.60.5.196 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh aciprd1  > $log_dir/aciprd1.log 2>&1 &
ssh -nq 10.60.5.167 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh easprd5  > $log_dir/easprd5.log 2>&1 &
ssh -nq 10.60.5.83 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cpyrsi  > $log_dir/cpyrsi.log 2>&1 &
ssh -nq 10.60.5.37 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccotrnp  > $log_dir/ccotrnp.log 2>&1 &
ssh -nq 10.60.5.12 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh sbypat  > $log_dir/sbypat.log 2>&1 &
ssh -nq 10.60.5.101 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh brwprd1  > $log_dir/brwprd1.log 2>&1 &
/u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh eabprd1  > $log_dir/eabprd1.log 2>&1 &
ssh -nq 10.60.5.187 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh agfprd1  > $log_dir/agfprd1.log 2>&1 &
ssh -nq 10.60.5.197 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dgaprd1  > $log_dir/dgaprd1.log 2>&1 &
wait
ssh -nq 10.60.5.187 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh iimprd1  > $log_dir/iimprd1.log 2>&1 &
ssh -nq 10.60.5.83 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cpyprd  > $log_dir/cpyprd.log 2>&1 &
ssh -nq 10.60.5.158 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh caltrnd  > $log_dir/caltrnd.log 2>&1 &
ssh -nq 10.60.5.244 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh tcfprd1  > $log_dir/tcfprd1.log 2>&1 &
ssh -nq 10.60.5.67 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh bdgcca  > $log_dir/bdgcca.log 2>&1 &
ssh -nq 10.60.5.37 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ccoctx  > $log_dir/ccoctx.log 2>&1 &
#ssh -nq 10.60.5.24 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh dacprd1  > $log_dir/dacprd1.log 2>&1 &
ssh -nq 10.60.105.203 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh ifmpoc1  > $log_dir/ifmpoc1.log 2>&1 &
ssh -nq 10.60.5.196 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh cciprd2  > $log_dir/cciprd2.log 2>&1 &
ssh -nq 10.60.5.197 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh terprd1  > $log_dir/terprd1.log 2>&1
ssh -nq 10.60.5.197 /u01/app/oracle/local/dba/backups/rman/rman_delete_obsolete.ksh mfcprd1  > $log_dir/mfcprd1.log 2>&1
exit 0
