sqlplus / as sysdba
set pagesize 30000 linesize 200 heading off feedback off
spool DeleteReports_Portal.txt
select report_location from pace_masterdbo.xml_reports_log where app_indicator =3 and upd_datetime < (sysdate-90);
spool off;
spool DeleteReports_OLAP.txt
select report_location from pace_masterdbo.xml_reports_log where app_indicator !=3 and upd_datetime < (sysdate-90);
spool off;
