CREATE OR REPLACE PROCEDURE cruser(p_user VARCHAR2 DEFAULT '%')
IS
/******************************************************************************
   NAME:       cruser
   PURPOSE:    To generate DDL for Users

   PARAMETERS: p_user
   INPUT:	   Username
   EXAMPLE USE:     exec cruser('SYSTEM%')  from SQL*PLUS
   ASSUMPTIONS: In SQL*Plus must:  set serveroutput on
   LIMITATIONS: This will not extract for TEMPORARY TABLESPACE TYPE
   ALGORITHM:
   NOTES:
******************************************************************************/
   crdt	    VARCHAR2(30) ;
   fn	    VARCHAR2(100) ;
   cstr	    VARCHAR2(4000) := NULL ;
   prevown  VARCHAR2(30) := 'asdf' ;
   exe_flg  BOOLEAN := FALSE ;
   db_name  VARCHAR2(30) := NULL ;

   CURSOR usr_cur IS
      SELECT u.username,
	     s.password,
             u.default_tablespace,
             u.temporary_tablespace,
             u.profile,
             u.external_name
      FROM   dba_users u,user$ s
      WHERE  u.username=s.name and
             username LIKE UPPER(p_user)
      ORDER  BY username ;

   CURSOR usr_quota(p_username IN VARCHAR2) IS
      SELECT tablespace_name,
             username,
             NVL(bytes/1024/1024,0) size_in_mb,
             NVL(max_bytes/1024/1024,-1) max_in_mb
      FROM   dba_ts_quotas
      WHERE  username = UPPER(p_username)
      ORDER  BY tablespace_name ;

   CURSOR tabpriv(p_username IN VARCHAR2) IS
      SELECT OWNER,
             TABLE_NAME,
             PRIVILEGE,
             COLUMN_NAME,
             GRANTEE,
             GRANTABLE
      FROM   SYS.DBA_COL_PRIVS
      WHERE  OWNER LIKE UPPER(NVL(p_username,'%'))
      UNION
      SELECT OWNER,
             TABLE_NAME,
             PRIVILEGE,
             NULL,
             GRANTEE,
             GRANTABLE
      FROM   SYS.DBA_TAB_PRIVS
      WHERE  OWNER LIKE UPPER(NVL(p_username,'%'))
      ORDER  BY 3,6 ;

    CURSOR syspriv(p_username IN VARCHAR2) IS
	 SELECT 1,
                GRANTED_ROLE PRIVILEGE,
	        ADMIN_OPTION,
		GRANTEE,
		DEFAULT_ROLE
	 FROM   dba_role_privs
	 WHERE  GRANTEE LIKE UPPER(NVL(p_username,'%'))
	 UNION
	 SELECT 2,
                PRIVILEGE,
	        ADMIN_OPTION,
		GRANTEE,
		NULL
	 FROM   dba_sys_privs
	 WHERE  GRANTEE LIKE UPPER(NVL(p_username,'%'))
     ORDER  BY 1,4 ;
BEGIN
  SELECT TO_CHAR(sysdate,'DD/MON/YYYY HH24:MI:SS')
  INTO	 crdt
  FROM	 dual ;

  SELECT UPPER(a.value) db_name
  INTO   db_name
  FROM   v$parameter a
  WHERE  UPPER(a.name) = 'DB_NAME' ;

  dbms_output.put_line('');
  dbms_output.put_line('Created On : ' || crdt);
  dbms_output.put_line('');

  FOR usr_rec IN usr_cur
  LOOP
    IF usr_rec.password IS NULL THEN
       dbms_output.put_line('CREATE USER ' || usr_rec.username || ' IDENTIFIED EXTERNALLY' ) ;
    ELSIF usr_rec.external_name IS NOT NULL THEN
       dbms_output.put_line('CREATE USER ' || usr_rec.username || ' IDENTIFIED GLOBALLY AS ' ||
                         '''' || usr_rec.external_name || '''' );
    ELSE
       dbms_output.put_line('CREATE USER ' || usr_rec.username || ' IDENTIFIED BY VALUES ' || '''' ||
                         usr_rec.password || '''' ) ;
    END IF ;
    dbms_output.put_line('   DEFAULT TABLESPACE ' || usr_rec.default_tablespace) ;
    dbms_output.put_line('   TEMPORARY TABLESPACE ' || usr_rec.temporary_tablespace) ;
    FOR quot_rec IN usr_quota(usr_rec.username)
    LOOP
      IF quot_rec.max_in_mb < 0 THEN
         dbms_output.put_line('   QUOTA UNLIMITED ON ' || quot_rec.tablespace_name ) ;
      ELSE
         dbms_output.put_line('   QUOTA ' || TO_CHAR(quot_rec.max_in_mb) || 'M ON ' || quot_rec.tablespace_name ) ;
      END IF ;
    END LOOP ;
    dbms_output.put_line('   PROFILE ' || usr_rec.profile) ;

    dbms_output.put_line('') ;
    dbms_output.put_line('REM ***** Granting System Privs... *****') ;
    FOR sysp IN syspriv(usr_rec.username)
    LOOP
       IF NVL(sysp.admin_option,'NO') = 'NO' THEN
          dbms_output.put_line('GRANT ' || sysp.privilege || ' TO ' ||
                                           sysp.grantee || ' ; ') ;
       ELSE
          dbms_output.put_line('GRANT ' || sysp.privilege || ' TO ' ||
                                           sysp.grantee || ' WITH ADMIN OPTION ; ') ;
       END IF ;
    END LOOP ;

    dbms_output.put_line('') ;
    dbms_output.put_line('REM ***** Granting Object Privs... *****') ;
    FOR tabp IN tabpriv(usr_rec.username)
    LOOP
       cstr := NULL ;
       IF tabp.grantable = 'YES' THEN
          cstr := ' WITH GRANT OPTION ; ' ;
       ELSE
          cstr := ' ;' ;
       END IF ;
       IF tabp.column_name IS NULL THEN
          dbms_output.put_line('GRANT ' || tabp.privilege || ' ON ' || tabp.owner || '.' ||
                               tabp.table_name || ' TO ' || tabp.grantee || cstr );
       ELSE
          dbms_output.put_line('GRANT ' || tabp.privilege || ' ON ' || tabp.owner || '.' ||
                               tabp.table_name || ' ( ' || tabp.column_name ||
                               ') TO ' || tabp.grantee || cstr );
       END IF ;
    END LOOP ;
    dbms_output.put_line('/') ;
  END LOOP ;
EXCEPTION
   WHEN no_data_found THEN
      NULL ;
   WHEN others THEN
      raise_application_error(-20005,SQLERRM) ;
END cruser;
/
