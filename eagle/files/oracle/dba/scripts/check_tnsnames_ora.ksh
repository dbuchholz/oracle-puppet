#!/bin/ksh
MAILTO=team_dba@eagleaccess.com
export ORACLE_SID=infprd1
export ORAENV_ASK=NO
export PATH=/usr/local/bin:$PATH
. /usr/local/bin/oraenv

if [ $(dirname $0) = "." ]
then
   script_dir=$(pwd)
else
   script_dir=$(dirname $0)
fi
log_dir=$script_dir

$ORACLE_HOME/bin/sqlplus -s '/ as sysdba'<<EOF>$log_dir/machine_info.txt
set pages 0 trimspool on feedback off echo off
select name,ip_address from inf_monitor.machines where OS_TYPE<>'SunOS' order by name;
EOF

rm -f $log_dir/incorrect_tnsnames_entries.txt
while read name ip_address
do
 ((no_response = 0))
 ssh -nq $ip_address cat /dev/null &
 the_pid=$!
 sleep 5
 while true
 do
  ps -ef | grep $the_pid | grep -v grep > /dev/null
  if [ $? -ne 0 ]
  then
   break
  fi
  print "$name Did not respond in five seconds.  Killing the ssh command."
  kill -9 $the_pid
  ((no_response = 1))
  break
 done
 if [ $no_response -eq 0 ]
 then
  os_type=$(ssh -nq $ip_address uname)
  ORATAB=/etc/oratab
  if [[ -n $os_type ]]
  then
   if [ $os_type = SunOS ]
   then
    ORATAB=/var/opt/oracle/oratab
   fi
  fi
  tnsnames_file=network/admin/tnsnames.ora
  ora_homes=$(ssh -nq $ip_address grep -v '^\*' $ORATAB | grep -v '^#' | grep -v '^$' | awk -F: '{print $2}' | sort -u)
  for an_ora_home in $ora_homes
  do
   invalid_home=$(print $an_ora_home | grep -v oracle)
   if [[ -n $invalid_home ]]
   then
    continue
   fi
   ssh -nq $ip_address ls -l $an_ora_home/$tnsnames_file > /dev/null 2>&1
   if [ $? -ne 0 ]
   then
    print "$name $an_ora_home/$tnsnames_file does not exist" >> $log_dir/incorrect_tnsnames_entries.txt
    print " " >> $log_dir/incorrect_tnsnames_entries.txt
    continue
   fi
   ssh -nq $ip_address grep -v '.*\(' $an_ora_home/$tnsnames_file | grep = > $log_dir/connect_string_entries.txt
   (( i=0 ))
   while read line
   do
    (( load=0 ))
    (( fail=0 ))
    (( port=0 ))
    (( i=i+1 ))
    if [ $i -gt 1 ]
    then
     ssh -nq $ip_address sed -n \"/$oldone/,/$line/p\" $an_ora_home/$tnsnames_file > $log_dir/before_get_one_tnsnames_entry_to_work_on.txt
     sed -e '$d' -e '/^$/d' -e '/^#/d' $log_dir/before_get_one_tnsnames_entry_to_work_on.txt > $log_dir/get_one_tnsnames_entry_to_work_on.txt
     grep -i LOAD_BALANCE=on $log_dir/get_one_tnsnames_entry_to_work_on.txt > /dev/null
     if [ $? -ne 0 ]
     then
      (( load=1 ))
     fi
     grep -i FAILOVER=on $log_dir/get_one_tnsnames_entry_to_work_on.txt > /dev/null
     if [ $? -ne 0 ]
     then
      (( fail=1 ))
     fi
     ports=$(grep -ic PORT $log_dir/get_one_tnsnames_entry_to_work_on.txt)
     if [[ -n $ports ]]
     then
      if [ $ports -lt 2 ]
      then
       (( port=1 ))
      fi
     fi
    fi
    if [ $load -eq 1 ] || [ $fail -eq 1 ] || [ $port -eq 1 ]
    then
     print "$name $an_ora_home" >> $log_dir/incorrect_tnsnames_entries.txt
     cat $log_dir/get_one_tnsnames_entry_to_work_on.txt >> $log_dir/incorrect_tnsnames_entries.txt
     print " " >> $log_dir/incorrect_tnsnames_entries.txt
    fi
    oldone=$line
   done<$log_dir/connect_string_entries.txt

   (( load=0 ))
   (( fail=0 ))
   (( port=0 ))
   last_one=$(tail -1 $log_dir/connect_string_entries.txt)
   ssh -nq $ip_address "sed \"1,/$last_one/d\" $an_ora_home/$tnsnames_file" > $log_dir/before_get_one_tnsnames_entry_to_work_on.txt
   sed -e '/^$/d' -e '/^#/d' -e "1i$last_one" $log_dir/before_get_one_tnsnames_entry_to_work_on.txt > $log_dir/get_one_tnsnames_entry_to_work_on.txt
   grep -i LOAD_BALANCE=on $log_dir/get_one_tnsnames_entry_to_work_on.txt > /dev/null
   if [ $? -ne 0 ]
   then
    (( load=1 ))
   fi
   grep -i FAILOVER=on $log_dir/get_one_tnsnames_entry_to_work_on.txt > /dev/null
   if [ $? -ne 0 ]
   then
    (( fail=1 ))
   fi
   ports=$(grep -ic PORT $log_dir/get_one_tnsnames_entry_to_work_on.txt)
   if [[ -n $ports ]]
   then
    if [ $ports -lt 2 ]
    then
     (( port=1 ))
    fi
   fi

   if [ $load -eq 1 ] || [ $fail -eq 1 ] || [ $port -eq 1 ]
   then
    print "$name $an_ora_home" >> $log_dir/incorrect_tnsnames_entries.txt
    cat $log_dir/get_one_tnsnames_entry_to_work_on.txt >> $log_dir/incorrect_tnsnames_entries.txt
    print " " >> $log_dir/incorrect_tnsnames_entries.txt
   fi
  done
 fi
done<machine_info.txt

if [[ -s $log_dir/incorrect_tnsnames_entries.txt ]]
then
:
#mailx -s "Inconsistency found in tnsnames.ora file" $MAILTO<<EOF
#The output below shows where the tnsnames.ora files are not what is expected:
#
#$(cat $log_dir/incorrect_tnsnames_entries.txt)
#EOF
fi
exit 0
