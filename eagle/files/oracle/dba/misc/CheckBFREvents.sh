#!/usr/bin/ksh
# ==================================================================================================
# NAME:     CheckBFREvents.sh 
#
# AUTHOR:   Maureen Buotte
# Frank Davis   06/24/2011  Modified IPCheck Variable to include updated VLANs
#
# PURPOSE:  This utility will notifiy when composite security Events do not finish in 100 seconds
#
# USAGE:    CheckBFROvernight.shS
#
# ==================================================================================================
#
# **********************************************************************************
# SendNotification()
#       This function sends mail notifications
# **********************************************************************************
function SendNotification {

	# Uncomment for debug
	 set -x

	echo -e "${PROGRAM_NAME} \n     Machine: $BOX " > mail.dat

	if [[ -a ${ERROR_FILE} ]]; then
		cat $ERROR_FILE >> mail.dat
		rm ${ERROR_FILE}
	fi

	export HEADER="BFR ${TYPE}:  Composite Security Event appears to have hung up "
	cat mail.dat | /bin/mail -s "${HEADER}" ${MAILTO}
	rm mail.dat

	# Update the mail counter
#	MAIL_COUNT=${MAIL_COUNT}+1

	return 0
}

# ---------------------------------------------------------------------------------------------
#  funct_check_inf_database():  Make sure infrastructure database is reachable
# ---------------------------------------------------------------------------------------------
function funct_check_inf_database {

    # Uncomment next line for debugging
     set -x

	PERFORM_CRON_STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
        select 1 from dual;
        exit
EOF`
    PERFORM_CRON_STATUS=`echo -e $PERFORM_CRON_STATUS|tr -d " "`
    if [[ ${PERFORM_CRON_STATUS} != 1 ]]; then
		PERFORM_CRON_STATUS=0
	fi
}

# ---------------------------------------------------------------------------------------------
# funct_initial_audit_update ():  Add or update Audit record
# ---------------------------------------------------------------------------------------------
function funct_initial_audit_update {

	# Uncomment for debug
	 set -x

	DB_INSTANCE=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from databases where sid='${ORACLE_SID}' and mac_instance = (select instance from machines where lower(name)='${BOX}');
		exit
EOF`

	DB_INSTANCE=`echo -e $DB_INSTANCE |tr -d " "`
	export DB_INSTANCE
	if [ "x$DB_INSTANCE" == "x" ]; then
		SendNotification "${ORACLE_SID} on ${BOX} does not exist in ${CRON_SID} -> ${PROGRAM_NAME} will run for ${ORACLE_SID} but ${CRON_SID} will not be updated" 
        PERFORM_CRON_STATUS=0
		return 1	
	fi

	Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		select instance from INSTANCE_CRON_JOB_RUNS where trim(script_name)='${PROGRAM_NAME}' and
		start_time > to_char(sysdate,'DD-MON-YYYY') and db_instance=${DB_INSTANCE};
		exit
EOF`
	Sequence_Number=`echo $Sequence_Number|tr -d " "`
	if [[ x$Sequence_Number = 'x' ]]; then
		Sequence_Number=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set serveroutput on size 1000000
		set heading off
		set feedback off
		declare i number;
		begin
		insert into INSTANCE_CRON_JOB_RUNS values (instance_cron_job_runs_seq.nextval,'${DB_INSTANCE}', '${PROGRAM_NAME}', sysdate, '','',0,'','0') returning instance into i;
		commit;
		dbms_output.put_line (i);
		end;
/
		exit
EOF`
	else
		STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off
		set feedback off
		update INSTANCE_CRON_JOB_RUNS set start_time=sysdate where instance=${Sequence_Number};
		commit;
EOF`

	fi
	export Sequence_Number
	return 0

}
# ---------------------------------------------------------------------------------------------
# funct_final_audit_update ():  If Audit database is being updated, add final process update
# ---------------------------------------------------------------------------------------------
function funct_final_audit_update {

	# Uncomment for debug
	 set -x

    # Update DBA Auditing Database with end time and backup size
	STATUS=`${ORACLE_HOME}/bin/sqlplus -s ${CRON_USER}/${CODE}@${CRON_SID} <<EOF
		set heading off 
		set feedback off 
		update INSTANCE_CRON_JOB_RUNS set end_time=sysdate,status='${PROCESS_STATUS}',number_of_runs=number_of_runs+1,mail_sent=mail_sent+${MAIL_COUNT} where instance=${Sequence_Number};
		commit;

		update INSTANCE_CRON_JOB_RUNS set run_time= (select
		trim(to_char(trunc(((86400*(end_time-start_time))/60)/60)-24*(trunc((((86400*(end_time-start_time))/60)/60)/24)),'09')) || ':' ||
		trim(to_char(trunc((86400*(end_time-start_time))/60)-60*(trunc(((86400*(end_time-start_time))/60)/60)),'09')) || ':'||
		trim(to_char(trunc(86400*(end_time-start_time))-60*(trunc((86400*(end_time-start_time))/60)),'09'))
		from instance_cron_job_runs where instance=${Sequence_Number})
		where instance=${Sequence_Number};
		commit;
/
		exit
EOF`

    echo  "$PROGRAM_NAME Completed `date +\"%c\"`with a status of ${PROCESS_STATUS} "
}

# --------------------------------------------------------------------------------------
# funct_verify_open(): Verify that status of database is OPEN
# ---------------------------------------------------------------------------------------
function funct_verify_open {

	# Uncomment next line for debugging
	 set -x

    typeset -i OPEN=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select case when trim(upper(status))='OPEN' then 1 else 0 end from v\\$instance;
		exit
EOF`

    OPEN=`echo -e $OPEN|tr -d " "`

	if [[ ${OPEN} == 1 ]]; then
		return 1
	else
		return 0
	fi
}

# --------------------------------------------------------------------------------------
# funct_chk_parm(): Check for input parameters
# --------------------------------------------------------------------------------------
funct_chk_parm() {
    # Uncomment next line for debugging

     set -x
    if [ ${NARG} -ne 1 ]; then
        echo -e "${PROGRAM_NAME} Failed: Incorrect number of arguments -> ${PROGRAM_NAME} SID "
        SendNotification "Incorrect number of arguments -> ${PROGRAM_NAME} SID "
        exit 1
    fi
}

# ------------------------------------------------------------------------
# funct_check_bfr_1(): Run Queries to check overnight processing status
# ------------------------------------------------------------------------
funct_check_events (){

	# Uncomment next line for debugging
	 set -x

	RunningCount=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
		set heading off
		set feedback off
		select count(*) from pace_masterdbo.schedule_queue q, pace_masterdbo.schedule_def d
		where q.pinstance = d.instance
		and d.freq_type = 'C'  
		and d.event_inst = 10 
		and d.enable = 'Y' 
		and q.status = 'P'
		and q.run_dt_time < (SYSDATE - 100/86400);
		exit
EOF`
    StatCount=`echo -e $RunningCount|tr -d " "`

	if [[ ${RunningCount} -gt 0 ]]; then
        echo -e "\n\nIt appears that a composite security event is hung, see query results below: \n\n\n " > ${ERROR_FILE}
		echo -e "select q.instance \"Sched Queue Inst\", q.pinstance \"Sched Def Inst\", d.custom_proc_name, q.run_dt_time, q.status " >>${ERROR_FILE}
		echo -e "from pace_masterdbo.schedule_queue q, pace_masterdbo.schedule_def d where q.pinstance = d.instance " >> ${ERROR_FILE}
		echo -e "and d.freq_type = 'C' and d.event_inst = 10 and d.enable = 'Y' and q.status = 'P' and q.run_dt_time < (SYSDATE - 100/86400); \n\n" >> ${ERROR_FILE}

		StatCount=`${ORACLE_HOME}/bin/sqlplus -s '/as sysdba' <<EOF
			spool ${TEMP_FILE}
			col custom_proc_name format a40
			col status format a10
			set line 132
			alter session set nls_date_format = 'DD-MON-YYYY HH24:MI:SS';
			select q.instance "Sched Queue Inst", q.pinstance "Sched Def Inst", d.custom_proc_name, q.run_dt_time, q.status
			from pace_masterdbo.schedule_queue q, pace_masterdbo.schedule_def d
			where q.pinstance = d.instance
			and d.freq_type = 'C'  
			and d.event_inst = 10 
			and d.enable = 'Y' 
			and q.status = 'P'
			and q.run_dt_time < (SYSDATE - 100/86400);
			spool off
			exit
EOF`

		cat ${TEMP_FILE} >> ${ERROR_FILE}
		rm -f ${TEMP_FILE}
		SendNotification 

	fi

    return
}

# *****************************
#  MAIN 
# *****************************
#
# Uncomment for debug
 set -x


NARG=$#

export PROGRAM_NAME=`echo -e  $0 | sed 's/.*\///g'`
export PROGRAM_NAME_FIRST=`echo ${PROGRAM_NAME} | awk -F "." '{print $1}'`
export BOX=`echo -e $(hostname) | awk -F "." '{print $1}'`
export DB_INSTANCE 

export ORACLE_BASE=$HOME
export ORATAB_LOC="/etc"
export WORKING_DIR="${ORACLE_BASE}/local/dba/misc"
export ERROR_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_error.txt
export TEMP_FILE=${WORKING_DIR}/${PROGRAM_NAME_FIRST}_temp.txt

HOSTNAME=`hostname`
IPCheck=`cat /etc/hosts | grep ${HOSTNAME} | awk '{print $1}' | egrep '10\.60\.5\.|10\.60\.3\.|10\.70\.4.\.|10\.70\.3.\.|10\.60\.99\.' | awk '{print $1}'`
if [ "x$IPCheck" != "x" ]; then
    export TYPE=PROD
##	export MAILTO=tech_support@eagleaccess.com
	export MAILTO=team_sys@eagleaccess.com
else
	export TYPE=TEST
	export MAILTO=team_sys@eagleaccess.com
fi
export MAILTO=nyoung@eagleaccess.com,MSathananthavel@eagleinvsys.com,mbuotte@eagleaccess.com

. /usr/bin/setsid.sh $ORA_SID
Status=$?
if [ $Status != 0 ]; then
    echo -e "Invalid SID -> ${ORA_SID}"
    SendNotification "Invalid SID -> ${ORA_SID}"
    exit
fi

funct_chk_parm
ORA_SID=$1

export ORACLE_SID
export ORACLE_HOME
export PATH
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib
export LD_LIBRARY_PATH=$ORACLE_HOME/lib

# Check if CRONLOG directory exists
if [[ ! -d ${ORACLE_BASE}/CRONLOG ]]; then
    mkdir -p ${ORACLE_BASE}/CRONLOG
fi

export PROCESS_STATUS
export MAIL_COUNT=0
export Sequence_Number

export PERFORM_CRON_STATUS=0
export PAR_HOME=$HOME/local/dba
export PARFILE=$PAR_HOME/BigBrother.ini
export NO_COMMENT_PARFILE=$PAR_HOME/${PROGRAM_NAME_FIRST}_$$_temp.ini
if [[ -a ${PARFILE} ]]; then
	STATUS=`cat ${PARFILE} | sed -e '/^#/d'  > ${NO_COMMENT_PARFILE}`
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "INFRASTRUCTURE_DATABASE:" `
	if [[ -n $LINE ]]; then
		INFO=`echo $LINE | sed 's/^[ \t]*INFRASTRUCTURE_DATABASE:[ \t]*//g' `
		export PERFORM_CRON_STATUS=1
		export CRON_SID=`echo -e $INFO | awk '{print $1}' `
		export CRON_USER=`echo -e $INFO | awk '{print $2}' `
		CRON_CONNECT=`echo -e $INFO | awk '{print $3}' `
		export CODE=`${PAR_HOME}/GetDec.cmd ${CRON_CONNECT}`
        # Make sure you can get to the infrastructure database
        funct_check_inf_database
        echo $PERFORM_CRON_STATUS
	fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "DONT_RUN_ANYTHING:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_ANYTHING=`echo $LINE | sed 's/^[ \t]*DONT_RUN_ANYTHING:[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
        fi
    LINE=`cat ${NO_COMMENT_PARFILE} | grep  "${PROGRAM_NAME}:" `
    if [[ -n $LINE ]]; then
        DONT_RUN_THIS_JOB=`echo $LINE | sed 's/^[ \t]*'${PROGRAM_NAME}':[ \t]*//g' | sed 's/ //g' | awk '{print $1,","}' | sed 's/ //g' `
    fi
fi
rm -f ${NO_COMMENT_PARFILE}



# Make sure this database is not in list of SIDs that nothing should be run against
EXCEPTION=`awk -v a="$DONT_RUN_ANYTHING" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
	SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} no jobs should run for this SID "
	continue
fi

# Make sure this database is not in list of SIDs that this job should not be run for
EXCEPTION=`awk -v a="$DONT_RUN_THIS_JOB" -v b="$ORACLE_SID," 'BEGIN{print index(a,b)}' `
if [[ "${EXCEPTION}" != "0" ]]; then
	SendNotification "${PROGRAM_NAME} will not run for ${ORACLE_SID} because it is explicitly excluded in CronStatus.ini file"
	continue
fi

# Make sure database is OPEN if not, skip to the next database
funct_verify_open
Status=$?
if [[ $Status == 0 ]]; then
	exit
fi

# Add entry into DBA Admin database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	funct_initial_audit_update
	Status=$?
	if [[ $Status != 0 ]]; then
		PERFORM_CRON_STATUS=0
	fi
	echo ${DB_INSTANCE}
	export DB_INSTANCE
fi


funct_check_events


# Update Infrastructure database
if [[ ${PERFORM_CRON_STATUS} = 1 ]]; then
	if [[ ${MAIL_COUNT} > 0 ]]; then
		PROCESS_STATUS='WARNING'
	else
		PROCESS_STATUS='SUCCESS'
	fi
	funct_final_audit_update
fi

