#!/bin/bash

CONFIG_FILE="/etc/sysconfig/network-scripts/ifcfg-eth1"

DC=`facter datacentername`

if [[ $DC = "PGH" ]]
then
    NETMASK="255.255.252.0"
fi

if [[ $DC = "EVT" ]]
then
    NETMASK="255.255.0.0"
fi
	   

touch $CONFIG_FILE
echo "HWADDR=$1" > $CONFIG_FILE
echo "NAME=eth1" >> $CONFIG_FILE
echo "DEVICE=eth1" >> $CONFIG_FILE
echo "ONBOOT=yes" >> $CONFIG_FILE
echo "USERCTL=no" >> $CONFIG_FILE
echo "BOOTPROTO=static" >> $CONFIG_FILE
echo "NETMASK=$NETMASK" >> $CONFIG_FILE
echo "IPADDR=$2" >> $CONFIG_FILE
echo "PEERDNS=no" >> $CONFIG_FILE
echo "" >> $CONFIG_FILE
echo "check_link_down() {" >> $CONFIG_FILE
echo " return 1;" >> $CONFIG_FILE
echo "}" >> $CONFIG_FILE
ifup eth1
