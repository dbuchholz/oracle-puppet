#!/bin/bash

DC=`facter datacentername`

if [[ $DC = "PGH" ]]
then
    DDHOST="pplb0001"
fi

if [[ $DC = "EVT" ]]
then
    DDHOST="eplb0001"
fi


echo "$DDHOST-bkup1.eagleaccess.com:/backup/oracle /datadomain     nfs     rw,hard,intr,tcp,rsize=1048576,wsize=1048576,nolock     0       0" >> /etc/fstab
mkdir /datadomain
mount /datadomain
ls /datadomain
