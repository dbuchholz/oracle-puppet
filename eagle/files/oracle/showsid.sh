#!/usr/bin/ksh
# *******************************************************************
# NAME:         showsid.sh
#
# AUTHOR:       Maureen Buotte
#
# PURPOSE:      This Utility will display Oracle Environment variables
#               $ORACLE_HOME and $ORACLE_SID
#
# USAGE:        setsid.sh SID
# ********************************************************************
# Uncomment for debug
# set -x

echo -e "\n"
echo -e "ORACLE_SID = ${ORACLE_SID}"
echo -e "ORACLE_HOME = ${ORACLE_HOME}"
echo -e "PATH = ${PATH}"
echo -e "ORACLE_BASE = ${ORACLE_BASE}"
echo -e "ORA_NLS33 = ${ORA_NLS33}"
echo -e "LD_LIBRARY_PATH = ${LD_LIBRARY_PATH}"

echo -e "\n"
