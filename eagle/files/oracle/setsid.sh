#!/bin/ksh
# *******************************************************************
# NAME:         setsid.sh
#
# AUTHOR:       Maureen Buotte
#               Frank Davis 05/21/2015 Made compatible with Linux and Solaris and CMP
#
# PURPOSE:      This Utility will set the Oracle Environment variables
#               $ORACLE_HOME and $ORACLE_SID
#
# USAGE:        setsid.sh SID
# ********************************************************************
#
if [ $# -ne 1 ]
then
 print "setsid.sh FAIL: Incorrect number of arguments - must pass SID"
 unset ORACLE_SID
 unset ORACLE_HOME
 /usr/bin/showsid.sh
 return 1
fi

ORA_SID=$1
ostype=$(uname)
if [ $ostype = Linux ]
then
 ORATAB=/etc/oratab
fi
if [ $ostype = SunOS ]
then
 ORATAB=/var/opt/oracle/oratab
fi
ORA_HOME=$(grep -v '^#' $ORATAB | grep ${ORA_SID}'\b' | awk -F: '{print $2}')
if [[ -z $ORA_HOME ]]
then
 print "setsid.sh FAIL:  Invalid SID\n"
 unset ORACLE_SID
 unset ORACLE_HOME
 /usr/bin/showsid.sh
 exit 1
fi

ASM_CLIENT=0
ps -ef | grep asm_smon_+ASM | grep -v grep  > /dev/null
if [ $? -eq 0 ]
then
 ASM_CLIENT=1
fi

export ORACLE_SID=$ORA_SID
export ORACLE_HOME=$ORA_HOME
export LD_LIBRARY_PATH=$ORACLE_HOME/lib
if [ $ASM_CLIENT -eq 0 ]
then
 export ORACLE_BASE=$HOME
else
 export ORACLE_BASE=/u01/oracle_11204/app/oracle
fi
export ORA_NLS33=$ORACLE_HOME/ocommon/nls/admin/data
export SHLIB_PATH=$ORACLE_HOME/lib:/usr/lib

# Reset the original PATH
PATH=/usr/kerberos/bin:.:/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin
export PATH=$PATH:.:/usr/sbin:$ORACLE_HOME/bin:/usr/openwin/bin:$ORACLE_HOME/rdbms/admin:$ORACLE_HOME/network/admin:$ORACLE_HOME/OPatch
/usr/bin/showsid.sh
return 0
