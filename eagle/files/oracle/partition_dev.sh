#!/bin/sh
# Example Usage: partition_dev.sh /dev/sdc
 
DRIVE=$1

# Enforce an argument to be passed to the script.

if [ -z $DRIVE ]; then
        echo "Example Usage: partition_dev.sh /dev/sdc"
else
 
# Sanity check ... is anything on the disk? If so let's not do anything.

CHECK1=`/sbin/sfdisk -l $DRIVE | grep ${DRIVE}1 | grep Linux`
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
        echo "This disk has a partition labeled Linux already. Best not to alter it."
        exit 2
elif [ $RETVAL -eq 1 ]; then
        echo "There is no partition on this disk. I will ready it for Oracle."

        /bin/dd if=/dev/zero of=$DRIVE bs=1024 count=1024 > /dev/null

        SIZE=`/sbin/fdisk -l $DRIVE | /bin/grep Disk | /bin/awk '{print $5}'`

        /bin/echo DISK SIZE - $SIZE bytes

        CYLINDERS=`/bin/echo $SIZE/255/63/512 | /usr/bin/bc`

        /bin/echo CYLINDERS - $CYLINDERS

        {
                /bin/echo ,,,-
                } | /sbin/sfdisk --force -D -H 255 -S 2048 -C $CYLINDERS $DRIVE

        fi

fi
