#!/bin/sh
#
# $Header: emagent/install/unix/agentDeploy.sh /main/57 2014/04/01 10:29:21 sallam Exp $
#
# agentDeploy.sh
#
# Copyright (c) 2010, 2014, Oracle and/or its affiliates. All rights reserved.
#
#    NAME
#      agentDeploy.sh - <one-line expansion of the name>
#
#    DESCRIPTION
#      This script is instrumental in all kinds of agent deployment be it install/upgrade
#
#    NOTES
#      <other useful comments, qualifications, etc.>
#
#    MODIFIED   (MM/DD/YY)
#    absuresh    01/05/14 - Discount the space for the core/version directory
#                           if already present
#    absuresh    12/24/13 - Do not move the sbin_new to sbin if -prereqOnly
#                           flag is passed for bug 17940390
#    absuresh    12/13/13 - Adding new option -update to take care of upgrade,
#                           patching of platform, plugin and sbin
#    absuresh    12/02/13 - Changing the help of agentDeploy.sh for bug
#                           16667617
#    armanick    04/05/12 - Bug#13931360: Fix for Number Overflow issue
#                           while calculating available space in hp-ux and
#                           hp-pa
#    absuresh    04/01/12 - XbranchMerge absuresh_bugfix_13599854 from
#                           st_emgc_12.1.0.1bp12.1.0.2pg
#    absuresh    03/15/12 - XbranchMerge absuresh_agtupgrade_proj from
#                           st_emgc_pt-12.1.0.2
#    absuresh    04/26/10 - Creation
#
#    vbhaagav    01/25/14 - Added Ulimit check as part of bug#17630852

UNAME=/bin/uname
GETCONF=/usr/bin/getconf
AWK=/usr/bin/awk
CAT=/usr/bin/cat
CHOWN="/usr/bin/chown"
CHMOD="/usr/bin/chmod"
CHMODR="/usr/bin/chmod -R"
CP=/usr/bin/cp
ECHO=/usr/bin/echo
MKDIR=/usr/bin/mkdir
TEE=/usr/bin/tee
RM=/bin/rm
MV=/bin/mv
GREP=/usr/bin/grep 
CUT=/bin/cut
SED=/usr/bin/sed
DF=/bin/df
TR=/usr/bin/tr

PLATFORM=`uname` 

if [ ${PLATFORM} = "Linux" ] ; then
  CAT=/bin/cat
  CHOWN=/bin/chown
  CHMOD=/bin/chmod
  CHMODR="/bin/chmod -R"
  CP=/bin/cp
  ECHO=/bin/echo
  MKDIR=/bin/mkdir
  GREP=/bin/grep
  if [ ! -f $CUT ] ; then
     CUT=/usr/bin/cut
  fi
  SED=/bin/sed
fi

if [ ${PLATFORM} = "SunOS" ] ; then
   GREP=/usr/xpg4/bin/grep
fi

if [ ${PLATFORM} = "Darwin" ] ; then
  CAT=/bin/cat
  CHOWN="/usr/sbin/chown"
  CHMOD="/bin/chmod"
  CHMODR="/bin/chmod -R"
  CP=/bin/cp
  ECHO=/bin/echo 
  MKDIR=/bin/mkdir
  CUT=/usr/bin/cut
fi

#platulim=`uname -s`
#if [ $platulim = "HP-UX" ] ; then
#SHELL_NOFILE_LIMIT=`/bin/sh -c "/usr/sbin/sysdef | grep maxuprc|tr -s ' '"|awk '{print $2}'`;

#elif [ $platulim = "AIX" ] ; then
#SHELL_NOFILE_LIMIT=`/bin/sh -c "/etc/lsattr -E -l sys0 | grep maxuproc|tr -s ' '"|awk '{print $2}'`;

#elif [ $platulim = "SunOS" ] ; then
#SHELL_NOFILE_LIMIT=`/bin/sh -c "/usr/sbin/sysdef | grep v_maxup|tr -s ' '"|awk '{print $1}'`;
#else
#SHELL_NOFILE_LIMIT=`/bin/sh -c 'ulimit -u'`;
#fi
agentBaseDir=""
instHome=""
logDir=""
args=$*
checkBaseFlag=FALSE
checkArchiveFlag=FALSE
timestamp=`date +%F`_`date +%H_%M_%S`
idx=0
idx1=0
idx2=0
rspLoc=""
umask 022
upgradeUsage()
{
	$ECHO "Usage : agentDeploy.sh AGENT_BASE_DIR AGENT_INSTANCE_HOME OLD_AGENT_VERSION OLDHOME [RESPONSE_FILE -prereqOnly -softwareOnly -invPtrLoc INVENTORY_LOCATION -help -debug -configOnly -ignorePrereqs -ignoreUnzip -ignoreUlimit -noRollBack PLUGIN_RSPFILE PROPERTIES_FILE]"
	$ECHO "" 
        $ECHO -e "\tAGENT_BASE_DIR              - Agent base directory location which is same as the old agent install"	
	$ECHO -e "\tRESPONSE_FILE               - Response file location"
        $ECHO -e "\tAGENT_INSTANCE_HOME - Agent instance home is the location of agent state directory"
	$ECHO -e "\tOLD_AGENT_VERSION 		- Version from which the agent is getting upgraded"
	$ECHO -e "\tOLDHOME			- Old Agent home location "
        $ECHO -e "\tPLUGIN_RSPFILE       - Response file location used for plugin configuration"
	$ECHO -e "\tPROPERTIES_FILE	- Agent properties file location that contains the property name and property value which will be set during the deployment"
	$ECHO -e "\t-prereqOnly         - Agent installer will be invoked in prereq mode"
        $ECHO -e "\t-softwareOnly               - Installs only the files"
        $ECHO -e "\t-invPtrLoc          - Inventory pointer location file"
        $ECHO -e "\tINVENTORY_LOCATION  - Inventory location"
        $ECHO -e "\t-help                       - Usage"
        $ECHO -e "\t-debug                      - More debug messages will be logged\n"
        $ECHO -e "\t-configOnly         - Performs agent configuration only"
        $ECHO -e "\t-forceConfigure             - Performs agent configuration even if oms is unavailable"
        $ECHO -e "\t-ignoreUnzip                - Ignores the unzipping of agent core bits\n"
#        $ECHO -e "\t-ignoreUlimit            - Skips the Ulimit Check"
        $ECHO -e "\t-ignorePrereqs              - Skips the agent prereqs"
	$ECHO -e "Example1:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent AGENT_INSTANCE_HOME=/scratch/agent/agent_inst PLUGIN_RSPFILE=/scratch/plugin.txt\n\tThis command is expected to do the complete agent and plugin upgrade install and configuration with the provided inputs.\n"
	$ECHO -e "Example2:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent AGENT_INSTANCE_HOME=/scratch/agent/agent_inst PLUGIN_RSPFILE=/scratch/plugin.txt\n\tThis command is expected to do the complete agent and plugin upgrade configuration with the provided inputs.\n"
	$ECHO -e "Example3:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent PLUGIN_RSPFILE=/scratch/plugin.txt PROPERTIES_FILE=/scratch/agent.properties\n\t Let's say the content of the /scratch/agent.properties file is enableAutoTuning=false, then this command with deploy and configure the agent with the enableAutoTuning property set."
}
freshUsage()
{	
	$ECHO "Usage : agentDeploy.sh AGENT_BASE_DIR OMS_HOST EM_UPLOAD_PORT [AGENT_REGISTRATION_PASSWORD RESPONSE_FILE AGENT_INSTANCE_HOME -prereqOnly -softwareOnly -invPtrLoc INVENTORY_LOCATION -help -debug -configOnly -ignorePrereqs -ignoreUnzip -ignoreUlimit -noRollBack -executeRootsh PROPERTIES_FILE]" 
	$ECHO "" 
	$ECHO -e "\tAGENT_BASE_DIR		- Agent base directory location"
	$ECHO -e "\tOMS_HOST		- OMS Hostname"
	$ECHO -e "\tEM_UPLOAD_PORT		- OMS Upload Port"
	$ECHO -e "\tAGENT_REGISTRATION_PASSWORD  - Agent registration password to secure the agent"

	$ECHO -e "\tRESPONSE_FILE		- Response file location" 
	$ECHO -e "\tAGENT_INSTANCE_HOME	- Agent instance home is the location of agent state directory"
	$ECHO -e "\tPLUGIN_RSPFILE       - Response file location used for plugin configuration"
	$ECHO -e "\tPROPERTIES_FILE     - Agent properties file location that contains the property name and property value which will be set during the deployment"
	$ECHO -e "\ts_agentHomeName 	- Customized agent home name, if not passed default home name will be assigned"
	$ECHO -e "\t-prereqOnly		- Agent installer will be invoked in prereq mode" 
	$ECHO -e "\t-softwareOnly		- Installs only the files" 
	$ECHO -e "\t-invPtrLoc		- Inventory pointer location file" 
	$ECHO -e "\tINVENTORY_LOCATION	- Inventory location" 
	$ECHO -e "\t-help			- Usage" 
	$ECHO -e "\t-debug			- More debug messages will be logged\n" 
	$ECHO -e "\t-configOnly		- Performs agent configuration only"
	$ECHO -e "\t-forceConfigure		- Performs agent configuration even if oms is unavailable"
	$ECHO -e "\t-ignoreUnzip		- Ignores the unzipping of agent core bits\n"
#        $ECHO -e "\t-ignoreUlimit           - Skips the Ulimit Check"
	$ECHO -e "\t-ignorePrereqs		- Skips the agent prereqs"
	$ECHO -e "Example1:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent OMS_HOST=hostname.domain.com EM_UPLOAD_PORT=1000 INVENTORY_LOCATION=/scratch AGENT_REGISTRATION_PASSWORD=2Bor02B4\n\tThis command is expected to do the complete agent install and configuration with the provided inputs.\n" 
	$ECHO -e "Example2:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agt RESPONSE_FILE=/scratch/agent.rsp -softwareOnly -invPtrLoc /scratch/agent/oraInst.loc -debug\n\tThis command is expected to copy the agent bits to the agent base directory.\n"
	$ECHO -e "Example3:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent OMS_HOST=hostname.domain.com EM_UPLOAD_PORT=1000 -forceConfigure\n\tThis command is expected to do the agent install and also force the agent configuration even though the oms host and port are not available.\n"
	$ECHO -e "Example4:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent AGENT_INSTANCE_HOME=/scratch/agent/agent_inst -configOnly\n\tThis command is expected to do the agent configuration only with the provided inputs.\n"
	$ECHO -e "Example5:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent s_agentHomeName=myAgent -ignorePrereqs\n\tThis command is expected to skip the prereqs and then continue with the agent deployment also notice in the inventory that instead of the default home name, myAgent home name will be assigned for the agent home.\n" 
	$ECHO -e "Example6:\n\t agentDeploy.sh AGENT_BASE_DIR=/scratch/agent PLUGIN_RSPFILE=/scratch/plugin.txt PROPERTIES_FILE=/scratch/agent.properties\n\t Let's say the content of the /scratch/agent.properties file is enableAutoTuning=false, then this command with deploy and configure the agent with the enableAutoTuning property set."
}
usage()
{
	$ECHO -e "\n\tagentDeploy.sh -help\n\tThis option will provide you the help for fresh agent install."
}
 if [ "{`dirname $0`}" = "{.}" ] ; then
	archiveLoc=$PWD
else
	archiveLoc=`dirname $0`
fi

$GREP null "$archiveLoc/agentDeploy.sh" > /dev/null
status=$?
if [ $status -ne 0 ] ; then
$ECHO "Grep utility does not exist in this box. Install the grep utility package and then rerun the agentDeploy.sh"
exit 1
fi

#UlimitCheck()
#{
#if [ ! "$ulimitFlag" ];then
#if [ -z  "$SHELL_NOFILE_LIMIT" -a "$SHELL_NOFILE_LIMIT"="" ]; then
#$ECHO "Ulimit check failed,check the ulimit command to exist in path"
#exit 1
#elif [ $SHELL_NOFILE_LIMIT -le 13312 ] ;then
#$ECHO "Ulimit check failed,please ensure 'ulimit -u' value to be >=13312"
#exit 1
#else
#$ECHO "Ulimit check passed"
#fi
#fi
#}

countArgs=$#

while [ $# -gt 0 ]
do
idx1=`$ECHO $1|$GREP "="|wc -l`
if [ $idx1 -gt 0 ] ; then
key=`$ECHO $1| $CUT -d '=' -f 1`
value=`$ECHO $1| $CUT -d '=' -f 2`
else
key=`$ECHO $1| $CUT -d '=' -f 1`
idx2=`$ECHO $1|$GREP "^-"|wc -l`
if [ $idx2 -eq 0 ] ; then
	$ECHO -e "\n ERROR: $1 is an unsupported argument passed to agentDeploy.sh.\n"	
	usage
	exit 1
fi
	
fi
	case "$key" in
	-help)	if [ $upgradeFlag ] ; then
		upgradeUsage
		else
		freshUsage
		fi
		exit 0;;
	AGENT_BASE_DIR) 
			 agentBaseDir=`$ECHO $value | $SED 's/\/$//'`
			 checkBaseFlag=TRUE;;
	OMS_HOST)	omsHost=$value
			checkOMSHost=TRUE;;
	EM_UPLOAD_PORT)  omsPort=$value
			 checkOMSPort=TRUE;;
	AGENT_INSTANCE_HOME)	instHome=`$ECHO $value | $SED 's/\/$//'`;;
	LOG_DIR)	logDir=`$ECHO $value | $SED 's/\/$//'`;;

	AGENT_REGISTRATION_PASSWORD) pswd=$value;;					s_encrSecurePwd) pswd=$value;;	
	RESPONSE_FILE)   rspFlag=TRUE  
                         rspLoc=$value;;
	OLD_AGENT_VERSION)  oldAgentVersion=$value;;   			
	OLDHOME)	oldHome=$value;;	
	b_sharedAgents)	 sharedAgent=$value;;
	-debug)		 debugSwitch=TRUE;;
	-forceConfigure) forceFlag=TRUE;;
	-configOnly)     configFlag=TRUE	
                         validationFlag=TRUE;;
	-agentImageLocation) archiveLoc=$value
			 checkArchiveFlag=TRUE;;
        -invPtrLoc)	shift
			ptrLoc=$1;;
	-sbinUpgrade)	sbinUpgradeFlag=TRUE
			validFlag=TRUE;;
        INVENTORY_LOCATION)  validFlag=TRUE;;
	b_forceInstCheck)  validFlag=TRUE
			forcefullFlag=TRUE;;
	-prereqOnly) 		validationFlag=TRUE
				prereqFlag=TRUE;;
	-softwareOnly)		validationFlag=TRUE
				softwareFlag=TRUE;;
	-update)		updateFlag=TRUE
				validationFlag=TRUE;;
	-validateParams) 	validateFlag=TRUE;;
	-ignoreUnzip)		unzipFlag=TRUE;;	
#        -ignoreUlimit)          ulimitFlag=TRUE;;
	-ignorePrereqs)		ignorePrereqFlag=TRUE
				validFlag=TRUE;;	
	-ignoreDirPrereq) 	ignoreDirPrereqFlag=TRUE
				validFlag=TRUE;;
	-ignoreCompatibility)	ignoreCompatabilityFlag=TRUE
				validFlag=TRUE;;
	-upgrade)		upgradeFlag=TRUE				
				validFlag=TRUE;;
	-noRollBack)		validFlag=TRUE;;
	-executeRootsh)		validFlag=TRUE;;
	*)	idx=`$ECHO $1|$GREP "^-"|wc -l`
		if [ $idx -eq 0 ] ; then
			validFlag=TRUE
		else
			$ECHO -e "\n ERROR: Invalid argument $key passed."
		        usage
        		exit 1			
		fi
	esac
	shift
done
if [ "$sharedAgent" = "true" ] ; then
	sharedFlag=TRUE
fi

#######################################################
#Subroutines
#######################################################
#validateParams method is used to validate the parameters

validateParams()
{
agentBaseDir=`$ECHO $agentBaseDir | $TR -s " "`
if [ $countArgs -lt 1 ]
then
  usage
  exit 2
fi



#changed ptrloc condition accordingly to work on solaris and other ports.
if   [ "$ptrLoc" = "" ] ;then
$ECHO " "
elif [ ! -f $ptrLoc ] ; then
        $ECHO -e "\n ERROR: Inventory pointer file $ptrLoc does not exists."
        exit 1
fi

#changed rspLoc condition to make it work on ports as part of bug12586548
if [ $rspFlag ] ; then
        if [ "$rspLoc" = "" ] ; then
                 $ECHO -e "\n ERROR: Invalid arguments passed. Pass RESPONSE_FILE value to agentDeploy.sh"
        usage
        exit 1
        fi
fi


if [ "$agentBaseDir" = "" ] ; then
	$ECHO -e "\n ERROR: Invalid invocation. AGENT_BASE_DIR is mandatory."
	usage
	exit 1
fi

if [ ! "$upgradeFlag" ] ; then
if   [ ' ! -e "${agentBaseDir}" ' ] ; then
	$MKDIR -p $agentBaseDir
fi

if [ $? -ne 0 ] ; then
	$ECHO -e "\nERROR: Not able to create the agent base directory. Check the write permissions on the base directory and then retry."
	exit 1
fi

$CHMOD 755 $agentBaseDir
if [ ! -w $agentBaseDir ] ; then
	$ECHO -e "\n ERROR: The agent base directory $agentBaseDir is not writable."
	exit 1
fi


if [ "$logDir" = "" ] ; then

if [ ! -w $archiveLoc ] ; then
        $ECHO -e "\n ERROR: The agent image location $archiveLoc is not writable."
        exit 1
fi
else
if [ ! -w $logDir ] ; then
        $ECHO -e "\n ERROR: The log directory location $logDir is not writable."
        exit 1
fi

fi



fi
}

#readPropFile method is used to read the values from the agentimage.properties

readPropFile()
{
FILE="$archiveLoc/agentimage.properties"
if [ $configFlag ] ; then
FILE="$agentBaseDir/agentimage.properties"
fi

if [ -f $FILE ] ; then
#while read line ;do
#set `$ECHO $line|$CUT -d "=" -f1`
#        if [ $1 = "VERSION" ] ; then
  set `cat $FILE|$GREP -i VERSION|$CUT -d "=" -f2`
                version=$1
                set `cat $FILE|$GREP -i TYPE|$CUT -d "=" -f2`
                type=$1
                set `cat $FILE|grep -i ARUID|cut -d "=" -f2`
                 id=$1
 if [ "$id" = "226" ] ; then
  if [ `$UNAME` = "Linux" ]; then
   if  [  -e $GETCONF ]; then
   value=`$GETCONF LONG_BIT`
     if  [ $value != 64 ]; then
          $ECHO -e "\nERROR: You are attempting to install 64-bit Oracle on a 32-bit operating system.  This is not supported and will not work."
	if [ ! "$ignoreCompatabilityFlag" ]; then
          exit 1
	fi
     fi
fi
 fi
fi

fi
if [ $configFlag ] ; then
FILE="$agentBaseDir/$type/$version/install/eminstall.info"
if [ ! -f $FILE ] ; then
$ECHO -e "\nERROR: Make sure the agentDeploy.sh is executed with -softwareOnly option before running it with -configOnly option."
exit 1
fi
fi
}

#checkFullyConfiguredAgent is used to check if the agentDeploy.sh is invoked second time against the fully configured agent.

checkFullyConfiguredAgent()
{
#This is added for bug 13417273
ORACLE_HOME="$agentBaseDir/$type/$version"
HOMEFILE="$agentBaseDir/$type/$version/install/oragchomelist"
if [ ! "$softwareFlag" -a ! "$prereqFlag" -a ! "$forcefullFlag" -a ! "$upgradeFlag" -a ! "$updateFlag" ] ; then
        if [ "$instHome" = "" ] ; then
        if [ -f "$HOMEFILE" ] ; then
                set `cat $HOMEFILE|grep -i $ORACLE_HOME|cut -d ":" -f2`
                instHome=$1
        else
                instHome="$agentBaseDir/agent_inst"
        fi
        fi
                if [ -d "$instHome" ] ; then
                set `ls -A $instHome|wc -l`
                NOOFFILES=$1
                if [ $NOOFFILES -gt 0 ] ; then
		$ECHO -e "\nERROR: The instance directory $instHome is not empty. If you want to configure the Management Agent in the same instance home, then stop the existing agent, manually delete the targets monitored by this management agent from the Cloud Control console, and then manually delete the agent instance home associated with this Management Agent. If you want to configure the agent in a different instance home, then enter a new instance home location by passing AGENT_INSTANCE_HOME argument."
                        exit 1
                fi
                fi
fi
}

#rspFileValidation validates the mandatory parameters present in the response file

rspFileValidation()
{
if [ ! "$rspLoc" = "" ] ; then
if [ -f $rspLoc ] ; then
idx=`cat $rspLoc|$GREP -v "^#"|$GREP OMS_HOST|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP OMS_HOST|$CUT -d "=" -f2`
omsHost=`$ECHO $value|$TR -d "\""`
fi
idx=`cat $rspLoc|$GREP -v "^#"|$GREP EM_UPLOAD_PORT|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP EM_UPLOAD_PORT|$CUT -d "=" -f2`
omsPort=`$ECHO $value|$TR -d "\""`
fi
idx=`cat $rspLoc|$GREP -v "^#"|$GREP AGENT_REGISTRATION_PASSWORD|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP AGENT_REGISTRATION_PASSWORD|$CUT -d "=" -f2`
pwd=`$ECHO $value|$TR -d "\""`
fi
idx=`cat $rspLoc|$GREP -v "^#"|$GREP AGENT_INSTANCE_HOME|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP AGENT_INSTANCE_HOME|$CUT -d "=" -f2`
instHome=`$ECHO $value|$TR -d "\""`
fi
idx=`cat $rspLoc|$GREP -v "^#"|$GREP OLD_AGENT_VERSION|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP OLD_AGENT_VERSION|$CUT -d "=" -f2`
oldAgentVersion=`$ECHO $value|$TR -d "\""`
fi
idx=`cat $rspLoc|$GREP -v "^#"|$GREP OLDHOME|wc -l`
if [ $idx -gt 0 ] ; then
value=`cat $rspLoc|$GREP -v "^#"|$GREP OLDHOME|$CUT -d "=" -f2`
oldHome=`$ECHO $value|$TR -d "\""`
fi
fi
fi
if [ ! "$rspLoc" = "" ] ; then
if [ ! -f $rspLoc ] ; then
        $ECHO -e "\nERROR: The response file passed to the agentDeploy.sh does not exist."
        exit 1;
fi
fi
	
}
validateMandatoryParams()
{
if [ ! "$validationFlag" -a ! "$upgradeFlag" -a ! "$updateFlag" ] ; then
if [ "$omsHost" = "" ] ; then
        $ECHO -e "\n ERROR: OMS_HOST cannot be null. Pass OMS_HOST value either as command-line arguments or in response file."
        exit 1
fi

if [ "$omsPort" = "" ] ; then
        $ECHO -e "\n ERROR: EM_UPLOAD_PORT cannot be null. Pass EM_UPLOAD_PORT value either as command-line arguments or in response file."
        exit 1
fi
fi
if [ "$upgradeFlag" ] ; then
if [ "$instHome" = "" ] ; then
$ECHO -e "\n ERROR: Invalid invocation. AGENT_INSTANCE_HOME is mandatory for upgrade\n"
upgradeUsage
exit 2
fi
if [ "$oldHome" = "" ] ; then
$ECHO -e "\n ERROR: Invalid invocation. OLDHOME is mandatory for upgrade\n"
upgradeUsage
exit 2
fi
if [ ! "$sbinUpgradeFlag" ] ; then
if [ "$oldAgentVersion" = "" ] ; then
$ECHO -e "\n ERROR: Invalid invocation. OLD_AGENT_VERSION is mandatory for upgrade\n"
upgradeUsage
exit 2	
fi
fi
fi


if [ $configFlag ] ; then
        if [ ! -d "$agentBaseDir/$type/$version" ] ; then
                $ECHO -e "\n ERROR: Oracle home $agentBaseDir/$type/$version does not exist."
                exit 1
        fi
fi

}


spaceCheck()
{
if [ ! "$prereqFlag" ] ; then 
if [ ! "$unzipFlag" ] ; then
if [ ! "$configFlag" ] ; then

if [ ! -d $agentBaseDir ] ; then
        $MKDIR -p $agentBaseDir
fi
	plat=`uname -s`
	if [ $plat = "HP-UX" ] ; then
	set `$DF -k $agentBaseDir |$GREP -i free|$TR -s ' '|$CUT -d " " -f2` 1>>$filename 2>>$filename1
	     elif [ $plat = "AIX" ] ; then
       	set `$DF -k $agentBaseDir | $TR -s ' '|tail -1|$CUT -d " " -f3` 1>>$filename 2>>$filename1
	else
	      set `$DF -k $agentBaseDir | $TR -s ' '|tail -1|$CUT -d " " -f4` 1>>$filename 2>>$filename1
	fi
	if [ $debugSwitch ] ; then
	$ECHO "Executing command : $DF -k $agentBaseDir |$TR -s ' '|$CUT -d \" \" -f4|$CUT -f2 -d \" \" "| $TEE -a $filename
        $ECHO "Output of the above command is $1" | $TEE -a $filename
	fi

	space=`echo $1 / 1024 | bc`
	if [ $debugSwitch ] ; then
        	$ECHO $space | $TEE -a $filename
	fi
	if [ -d "$agentBaseDir/$type/$version" ] ; then
		set `du -s $agentBaseDir/$type/$version`
		addSpace=`echo $1 / 1024 | bc`
	        space=`echo $addSpace + $space | bc`			
		if [ $debugSwitch ] ; then
		$ECHO "Space to be added: $addSpace" | $TEE -a $filename
		$ECHO "Added space: $space" | $TEE -a $filename
		fi
	fi
	if [  "$unzipFlag" = "" ] ; then
	if [ ! "$upgradeFlag" -a ! "$updateFlag" ] ; then
	if [ $space -lt 1024 ] ; then
	        $ECHO -e "\n ERROR: $agentBaseDir Directory has space less than 1 GB. Make sure that the agent base directory has space more than 1 GB." | $TEE -a $filename1
        	exit 1
	fi
	else
	if [ $space -lt 750 ] ; then
        	$ECHO -e "\n ERROR: $agentBaseDir Directory has space less than 750 MB. Make sure that the agent base directory has space more than 750 MB." | $TEE -a $filename1
	        exit 1
	fi	
fi
fi
fi
fi
fi
}

#validateOMS method validates whether the oms host and port is active.
validateOMS()
{
if [ ! "$configFlag" ] ; then
if [ $debugSwitch ] ; then
        $ECHO "Agent Base Directory is : $agentBaseDir" 1>>$filename
        $ECHO "Response File Location is : $rspLoc" 1>>$filename
fi


if [ $debugSwitch ] ; then
        $ECHO "Executing command : $archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip core/$version/jlib/agentInstaller.jar -d $agentBaseDir " | $TEE -a $filename
fi
if [ -f "$archiveLoc/unzip" ] ; then
$archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip core/$version/jlib/agentInstaller.jar -d $agentBaseDir > $filename
$archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip core/$version/oui/jlib/OraInstaller.jar -d $agentBaseDir > $filename
$archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip "core/$version/oui/lib/*" -d $agentBaseDir > $filename

if [ $debugSwitch ] ; then
 $ECHO "Executing command : $archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip core/$version/jdk/* -d $agentBaseDir " | $TEE -a $filename
fi
$archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip "core/$version/jdk/*" -d $agentBaseDir > $filename
$ECHO -e "Validating the OMS_HOST & EM_UPLOAD_PORT"
agentHome="$agentBaseDir/core/$version"
$MKDIR -p $agentHome/cfgtoollogs/agentDeploy
$ECHO "Executing command : $agentHome/jdk/bin/java -classpath $agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/OraInstaller.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir -prereq" | $TEE -a $filename

$agentHome/jdk/bin/java -classpath $agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/OraInstaller.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir $args -prereq

status="$?"
sleep 2

if [ $status -eq 1 ] ; then
        $ECHO -e "Validating the OMS_HOST & EM_UPLOAD_PORT has failed"
        exit 1
fi
else
        $ECHO -e "ERROR: $archiveLoc/unzip does not exist"
        exit 1
fi
fi
}

#unzipImage method unarchives the agent image to the agent base dir

unzipImage()
{
# Unzip the agentcoreimage.zip to the agent base directory
if [ ! "$configFlag" ] ; then
if [  "$unzipFlag" = ""  ] ; then
if [ $debugSwitch ] ; then
        $ECHO "Executing command : $archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip -d $agentBaseDir " | $TEE -a $filename
fi


$ECHO "Unzipping the agentcoreimage.zip to $agentBaseDir ...."

core=$archiveLoc/agentcoreimage.zip
if [ ! -f $core ] ; then
        $ECHO -e "\n ERROR:$core : does not exists" | $TEE -a $filename1
        exit 1
fi

$archiveLoc/unzip -o $archiveLoc/agentcoreimage.zip -d $agentBaseDir > $filename
if [ ! "$upgradeFlag" -a ! "$updateFlag" ] ; then
pluginFile=$version"_PluginsOneoffs_"$id".zip"
if [ ! -f "$archiveLoc/$pluginFile" ] ; then
        pluginFile="plugin.zip"
fi
echo $pluginFile
if [ -f "$archiveLoc/$pluginFile" ] ; then
echo "Executing command : $archiveLoc/unzip -o $archiveLoc/$pluginFile -d $agentBaseDir"  | tee -a $filename
$archiveLoc/unzip -o $archiveLoc/$pluginFile -d $agentBaseDir > $filename
fi
fi
fi
agentHome="$agentBaseDir/$type/$version"
$MKDIR -p "$agentHome/cfgtoollogs/agentDeploy"
fi
if [ ! "$upgradeFlag" -a ! "$prereqFlag" ] ; then
if [ -d "$agentBaseDir/sbin_new" ] ; then
$MV "$agentBaseDir/sbin_new" "$agentBaseDir/sbin"
fi
fi

}

if [ ! "$sharedFlag" ] ; then
if [ "$logDir" = "" ] ; then
if   [ ' ! -d "$archiveLoc/agentDeploy" ' ] ; then
	$MKDIR -p $archiveLoc/agentDeploy
fi

filename="$archiveLoc/agentDeploy/agentDeploy_$timestamp.log"
filename1="$archiveLoc/agentDeploy/agentDeploy_$timestamp.err"
else
if   [ ' ! -d "$logDir/agentDeploy" ' ] ; then
	$MKDIR -p $logDir/agentDeploy
fi
filename="$logDir/agentDeploy/agentDeploy_$timestamp.log"
filename1="$logDir/agentDeploy/agentDeploy_$timestamp.err"
fi

validateParams
fi	

readPropFile
if [ ! "$sharedFlag" ] ; then
rspFileValidation
validateMandatoryParams
checkFullyConfiguredAgent
spaceCheck
#UlimitCheck
if [ $debugSwitch ] ; then
	$ECHO "Agent Base Directory is : $agentBaseDir" 1>>$filename
	$ECHO "Response File Location is : $rspLoc" 1>>$filename
fi
if [ ! "$upgradeFlag" -a ! "$updateFlag" ] ; then
validateOMS
fi
unzipImage
fi

# Calling the java class to perform all the other operations
agentHome="$agentBaseDir/$type/$version"
if [ "$instHome" = "" ]
then
instHome="$agentBaseDir/agent_inst"
fi
platform=`uname -s`
os=`uname -m`
if [ "$platform" = "AIX" ]
then
   os=AIX;
else
   temp=`uname -m`
   os="$platform.$temp"
fi
 if [ "$id" = "197" ] || [ "$id" = "59" ] || [ "$id" = "209" ]  ||  [ "$id" = "23" ] ||  [ "$id" = "267" ]  || [ "$id" = "212" ] ;
then
{
$ECHO "Executing command : $agentHome/jdk/bin/java  -d64 -classpath $agentHome/oui/jlib/OraInstaller.jar:$agentHome/oui/jlib/xmlparserv2.jar:$agentHome/oui/jlib/srvm.jar:$agentHome/oui/jlib/emCfg.jar:$agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/share.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir $instHome AGENT_BASE_DIR=$agentBaseDir" | $TEE -a $filename

$agentHome/jdk/bin/java -d64  -classpath $agentBaseDir/oneoffs/agentInstaller.jar:$agentHome/oui/jlib/OraInstaller.jar:$agentHome/oui/jlib/xmlparserv2.jar:$agentHome/oui/jlib/srvm.jar:$agentHome/oui/jlib/emCfg.jar:$agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/share.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir $instHome AGENT_BASE_DIR=$agentBaseDir $args


#$agentHome/jdk/bin/java -d64 -classpath $agentHome/jlib/OraInstaller.jar:$agentHome/sysman/jlib/emInstaller.jar:$agentHome/jlib/xmlparserv2.jar:$agentHome/jlib/srvm.jar:$agentHome/jlib/emCfg.jar oracle.sysman.agent.installer.AgentInstaller $args 1>>$filename 2>>$filename1
}
else
{
$ECHO "Executing command : $agentHome/jdk/bin/java -classpath $agentHome/oui/jlib/OraInstaller.jar:$agentHome/oui/jlib/xmlparserv2.jar:$agentHome/oui/jlib/srvm.jar:$agentHome/oui/jlib/emCfg.jar:$agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/share.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir $instHome AGENT_BASE_DIR=$agentBaseDir" | $TEE -a $filename

$agentHome/jdk/bin/java -classpath $agentBaseDir/oneoffs/agentInstaller.jar:$agentHome/oui/jlib/OraInstaller.jar:$agentHome/oui/jlib/xmlparserv2.jar:$agentHome/oui/jlib/srvm.jar:$agentHome/oui/jlib/emCfg.jar:$agentHome/jlib/agentInstaller.jar:$agentHome/oui/jlib/share.jar oracle.sysman.agent.installer.AgentInstaller $agentHome $archiveLoc $agentBaseDir $instHome AGENT_BASE_DIR=$agentBaseDir $args


#$agentHome/jdk/bin/java -classpath $agentHome/jlib/OraInstaller.jar:$agentHome/sysman/jlib/emInstaller.jar:$agentHome/jlib/xmlparserv2.jar:$agentHome/jlib/srvm.jar:$agentHome/jlib/emCfg.jar oracle.sysman.agent.installer.AgentInstaller $args 1>>$filename 2>>$filename1
}
fi
exit $?

