class eagle::oracle12c_base {

  # This is the main Oracle 12c class. It manages all the pre-reqs using requires
  # and then creates the DB, creates listener and tns files, and starts them.

  require eagle::linux
#  require eagle::barf
  require eagle::oracle12c_coreos
  require eagle::oracle12c_users
  require eagle::oracle12c_asm
  require eagle::oracle12c_installdb
  require eagle::oracle12c_agent

  Class['eagle::oracle12c_coreos'] -> Class['eagle::oracle12c_users'] -> Class['eagle::oracle12c_asm'] -> Class['eagle::oracle12c_installdb']

#  include eagle::oracle_scripts

  $vcac = hiera('vcac')
  
  $db_domain = 'eagleaccess.com'
  $grid_base = hiera('grid_base')
  $grid_home = hiera('grid_home')
  $oracle_base = hiera('oracle_base')
  $oracle_home = hiera('oracle_home')
  $db_name = inline_template('<%= @clientworkgroup.sub("-","") %>')

  $oracle_dbsize = $vcac['oracleDBSize']
  $db_port1 = $vcac['databasePort1']
  $db_port2 = $vcac['databasePort2']

  case $oracle_dbsize {
    'small':  {
      $eagle_init_ora_source = 'eagle_init_ora12.small.erb'
    }
    'medium': {
      $eagle_init_ora_source = 'eagle_init_ora.medium.erb' 
    }
    'large':  {
      $eagle_init_ora_source = 'eagle_init_ora.large.erb' 
    }
    default:  {
      $eagle_init_ora_source = 'eagle_init_ora12.small.erb' 
    }
  }
  
  $exec_path     = '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:'

  if ($oraclerun != 'true'){  

    file { '/tmp/oratabentry.sh':
      owner   => 'oracle',
      group   => 'oinstall',
      mode    => '0774',
      source  => 'puppet:///modules/eagle/oracle/oratabentry.sh',
      before  => Exec["/tmp/oratabentry.sh ${db_name}"],
    }

    exec { "/tmp/oratabentry.sh ${db_name}":
      returns => 0,
      user    => 'root',
      #      before  => Oradb::Autostartdatabase['oracle_start'],
      before => File["${oracle_home}/sqlplus/admin/glogin.sql"],
    }
    
    file { "${oracle_home}/sqlplus/admin/glogin.sql":
      owner   => 'oracle',
      group   => 'oinstall',
      mode    => '0644',
      source  => 'puppet:///modules/eagle/oracle/glogin.sql',
      before  => Oradb::Tnsnames['LISTENER'],
    }

    oradb::tnsnames{ 'LISTENER':
      oracle_home		=> $grid_home,
      user			=> 'grid',
      group			=> 'oinstall',
      server                    => { myserver => { host => $fqdn, port => $db_port1, protocol => 'TCP' }, myserver2 => { host => $fqdn, port => $db_port2, protocol => 'TCP' } },
#      connect_service_name	=> inline_template('<%= @clientworkgroup.sub("-","") %>'),
      entry_type                => 'listener',
      require                   => File["${oracle_home}/sqlplus/admin/glogin.sql"],
#      before                    => File['/home/oracle/setup_eagledb.ksh'],
      before  => File["${oracle_home}/dbs/init${db_name}.ora"],
    }
    
    file { "${oracle_home}/dbs/init${db_name}.ora":
      owner   => 'oracle',
      group   => 'oinstall',
      mode    => '0644',
      content => template("eagle/oracle/${eagle_init_ora_source}"),
      before  => Oradb::Net['oranet'],
#      require => Oradb::Installdb['eagledb'],
#      before  => Oradb::Database['eagle_db']

#      before  => Exec["/tmp/oratabentry.sh ${db_name}"],
    }


/*   
    oradb::database{ 'eagle_db':
      oracle_base               => $oracle_base,
      oracle_home               => $oracle_home,
      version                   => '12.1',
      user                      => 'oracle',
      group                     => 'oinstall',
      download_dir               => '/tmp',
      action                    => 'create',
      db_name                   => $db_name,
      db_domain                 => $db_domain,
      db_port                   => $db_port1,
      sys_password              => 'Eagle01',
      system_password           => 'Eagle01',
      character_set             => 'AL32UTF8',
      nationalcharacter_set     => 'UTF8',
      sample_schema             => 'FALSE',
#      memory_percentage         => '40',
#      memory_total              => '800',
      database_type             => 'MULTIPURPOSE',
      em_configuration          => 'NONE',
      storage_type              => 'ASM',
      asm_snmp_password         => 'Eagle01',
      asm_diskgroup             => 'DATA',
      recovery_area_destination => 'FRA',
      template                  => 'dbtemplate_12.1_asm',
      container_database        => false, # 12.1 feature for pluggable database
      before 		        => Oradb::Net['oranet'],
    }
*/        
    oradb::net { 'oranet': 
      oracle_home	=> $grid_home,
      version		=> '12.1',
      user		=> 'grid',
      group		=> 'oinstall',
      download_dir	=> '/tmp',
      db_port		=> $db_port1,
#      require		=> Oradb::Database['eagle_db'],
#      require           => Exec['/home/oracle/setup_eagledb.ksh'],
      before		=> Oradb::Tnsnames[$db_name],
    }
    
    oradb::tnsnames{ $db_name:
      oracle_home		=> $oracle_home,
      user			=> 'oracle',
      group			=> 'oinstall',
      server                    => { myserver => { host => $fqdn, port => $db_port1, protocol => 'TCP' }, myserver2 => { host => $fqdn, port => $db_port2, protocol => 'TCP' } },
      loadbalance		=> 'ON',
      failover		        => 'ON',
      connect_service_name	=> inline_template('<%= @clientworkgroup.sub("-","") %>'),
      connect_server		=> 'DEDICATED',
      db_domain                 => $db_domain,
      require			=> Oradb::Net['oranet'],
#      before                    => Oradb::Tnsnames['listener_file'],
      before			=> Oradb::Listener['listener_12102'],
    }

    oradb::listener{'listener_12102':  
      oracle_base	=> $grid_base,
      oracle_home	=> $grid_home,
      user		=> 'grid',
      group		=> 'oinstall',
      action		=> 'start',
      require		=> Oradb::Tnsnames[$db_name],
      before => File['/home/oracle/setup_eagledb.ksh'],
    }

    file { '/home/oracle/setup_eagledb.ksh':
      owner   => 'oracle',
      group   => 'oinstall',
      mode    => '0744',
      source  => 'puppet:///modules/eagle/oracle/setup_eagledb.ksh',
#      require => Oradb::Installdb['eagledb'],
#      before          		=> File['/etc/facter/facts.d/oraclesetup.json'],
      before  => Exec['su - oracle /home/oracle/setup_eagledb.ksh'],
    }

#    exec { '/home/oracle/setup_eagledb.ksh':
    exec { 'su - oracle /home/oracle/setup_eagledb.ksh':
      returns => [0],
      user    => 'root',
      cwd     => '/home/oracle',
      path    =>  $exec_path,
      timeout => 900,
      require => File['/home/oracle/setup_eagledb.ksh'],
      before		=> Oradb::Autostartdatabase['oracle_start'],
    }

    oradb::autostartdatabase{ 'oracle_start':
      oracle_home	=> $oracle_home,
      user		=> 'oracle',
      db_name		=> $db_name,
      before		=> Exec['/etc/init.d/ohasd stop'],
    }

    # Bounce ohasd so that Oracle is started cleanly.
    
    exec { "/etc/init.d/ohasd stop":
      require => Oradb::Autostartdatabase['oracle_start'],
      before => Exec['/etc/init.d/ohasd start && sleep 120'],
    }
    
    exec { "/etc/init.d/ohasd start && sleep 120":
      require => Exec['/etc/init.d/ohasd stop'],
      #before => File['/datadomain'],
      before => File['/etc/facter/facts.d/oraclesetup.json'],
    }

    file {'/etc/facter/facts.d/oraclesetup.json':
      ensure  => 'present',
      require => Exec["/etc/init.d/ohasd start && sleep 120"],
      content => "{\"oraclerun\": true}\n",
    }
    
  }  # end  if($oraclerun != 'true')
    
  file { "/etc/t-shirt":
    owner   => 'oracle',
    group   => 'oinstall',
    mode    => '0775',
    content => "Eagle Database size is: ${oracle_dbsize}",
  }
} #class eagle::oracle12c_base
