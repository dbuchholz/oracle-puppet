class eagle::oracle12c_agent {

  # Enterprise Manager Agent installation (aka OCC)

  $oracle_base = hiera('oracle_base')

  if ($agentsetup != 'true') {
    
  oradb::installem_agent{ 'em12105_agent2':
    version                     => '12.1.0.5',
    install_version             => '12.1.0.5.0',
    source                      => 'https://occprd.eagleaccess.com:7799/em/install/getAgentImage',
    install_type                => 'agentPull',
    oracle_hostname             => $::fqdn,
    oracle_base_dir             => $oracle_base,
    agent_base_dir              => "${oracle_base}/agent",
    agent_registration_password => 'Eagle0001',
    sysman_password             => 'Eagl30001',
    agent_port                  => 3872,
    oms_host                    => 'occprd.eagleaccess.com',
    oms_port                    => 7799,
    em_upload_port              => 4900,
    user                        => 'oracle',
    group                       => 'oinstall',
    download_dir                => '/tmp/eminstall',
    log_output                  => true,
    before          		=> File['/etc/facter/facts.d/agentsetup.json'],
  }

  file {'/etc/facter/facts.d/agentsetup.json':
    ensure          => 'present',
    content         => "{\"agentsetup\": true}\n",
  }
  }
  
}

/*

    version                     => '12.1.0.4',
    install_version             => '12.1.0.4.0',
    source                      => 'https://occprd.eagleaccess.com:7799/em/install/getAgentImage',
    install_type                => 'agentPull',
    oracle_hostname             => $::fqdn,
    oracle_base_dir             => $oracle_base,
    agent_base_dir              => "${oracle_base}/product/12.1.0.4/agent",
    agent_registration_password => 'Eagle0001',
    sysman_password             => 'Eagl30001',
    agent_port                  => 3872,
    oms_host                    => 'occprd.eagleaccess.com',
    oms_port                    => 7799,
    em_upload_port              => 4900,
    user                        => 'oracle',
    group                       => 'oinstall',
    download_dir                => '/tmp/eminstall',
    log_output                  => true,

*/
