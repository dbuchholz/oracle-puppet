class eagle::oracle12c_users {

  # The Oracle and Grid user accounts are managed here. Passwords for those accounts
  # are defined below. This top section runs all the time. Lower sections are once only.

  $vcac = hiera('vcac')
  
  $sys_asm_password = 'Eagle01'
  $asm_monitor_password = 'Eagle01'
  $db_domain = 'eagleaccess.com'
  $grid_base = hiera('grid_base')
  $grid_home = hiera('grid_home')
  $oracle_base = hiera('oracle_base')
  $oracle_home = hiera('oracle_home')
  $ora_inventory_dir = hiera('ora_inventory')
  $db_name = inline_template('<%= @clientworkgroup.sub("-","") %>')

  if $vcac['UpgradeRegion'] == 'Upgrade' {
    $source_sid = $vcac['DatabaseSids']
    } else {
    $source_sid = $vcac['sourceSID']
    }

  $oracle_dbsize = $vcac['oracleDBSize']
  $db_port1 = $vcac['databasePort1']
  $db_port2 = $vcac['databasePort2']

  if $::location == 'PGH' {
    $isilon_host =  'nas-p-nfs01.eagleaccess.com'
    $isilon_root = 'eapgh-nfs'
  }
  elsif $location == 'EVT' {
    $isilon_host = 'nas-e-nfs01.eagleaccess.com'
    $isilon_root = 'eaevt-nfs'
  }
  else {
    fail('Unknown data center! Cannot find the Oracle media.')
  }
    
  $exec_path     = '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:'

  $all_groups = ['oinstall','dba','oper','asmdba','asmadmin','asmoper']

  group { $all_groups :
    ensure => present,
    before => User['oracle'],
  }

  user { 'oracle' :
    ensure           => present,
    uid              => 501,
    gid              => 'oinstall',
    groups           => ['oinstall','dba','oper','asmdba','asmadmin'],
    shell            => '/bin/ksh',
    expiry           => '2035-04-01',
    password_min_age => 0,
    password_max_age => 99999,
    password         => '$1$DSJ51vh6$4XzzwyIOk6Bi/54kglGk3.',
    home             => '/home/oracle',
    comment          => 'The oracle user was created by Puppet',
    managehome       => true,
    require          => Group[$all_groups],
    before           => User['grid'],
  }

  user { 'grid' :
    ensure           => present,
    uid              => 502,
    gid              => 'oinstall',
    groups           => ['oinstall','dba','asmadmin','asmdba','asmoper'],
    shell            => '/bin/ksh',
    expiry           => '2035-04-01',
    password_min_age => 0,
    password_max_age => 99999,
    password         => '$1$DSJ51vh6$4XzzwyIOk6Bi/54kglGk3.',
    home             => '/home/grid',
    comment          => 'The grid user was created by Puppet',
    managehome       => true,
    require          => Group[$all_groups],
    before           => File['/home/oracle/.profile'],
  }

    file { '/home/oracle/.profile':
      ensure  => present,
      content => regsubst(template('eagle/oracle/eagle_oracle_profile.erb'), '\r\n', "\n", 'EMG'),
      mode    => '0775',
      owner   => 'oracle',
      group   => 'oinstall',
      require => User['oracle'],
      before  => File['/home/oracle/.kshrc'],
    }

    file {'/home/oracle/.kshrc':
      ensure  => present,
      content => regsubst(template('eagle/oracle/eagle_oracle_kshrc.erb'), '\r\n', "\n", 'EMG'),
      mode    => '0775',
      owner   => 'oracle',
      group   => 'oinstall',
      require => User['oracle'],
      before  => File['/home/oracle/.aliases'],
    }

    file {'/home/oracle/.aliases':
      ensure  => present,
      content => regsubst(template('eagle/oracle/eagle_oracle_aliases.erb'), '\r\n', "\n", 'EMG'),
      mode    => '0775',
      owner   => 'oracle',
      group   => 'oinstall',
      require => User['oracle'],
      before  => File['/home/grid/.profile'],
    }

    file {'/home/grid/.profile':
      ensure  => present,
      content => regsubst(template('eagle/oracle/grid_ksh_profile.erb'), '\r\n', "\n", 'EMG'),
      mode    => '0775',
      owner   => 'grid',
      group   => 'oinstall',
      require => User['grid'],
      before  => File['/usr/local/scripts/datadomain'],
    }

    file { '/usr/local/scripts/datadomain':
      ensure  => 'directory',
      recurse => 'true',
      owner   => 'root',
      group   => 'root',
      mode    => '0775',
      source  => 'puppet:///modules/eagle/oracle/datadomain',
      before  => File['/usr/local/bin/grid_sshknownhosts.sh'];
    }
    
    file { '/usr/local/bin/grid_sshknownhosts.sh':
      owner  => 'root',
      group  => 'root',
      mode   => '0775',
      source => 'puppet:///modules/eagle/oracle/grid_sshknownhosts.sh',
      before => File['/usr/local/bin/oracle_sshknownhosts.sh'],
    }
    
    file { '/usr/local/bin/oracle_sshknownhosts.sh':
      owner => 'root',
      group => 'root',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/oracle_sshknownhosts.sh',
      before => File['/etc/skel/.eagle.oracle.profile'],
    }
    
    file { '/etc/skel/.eagle.oracle.profile':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle.oracle.profile.sh',
      before => File['/etc/skel/.eagle.oracle.kshrc'],
    }
    
    file { '/etc/skel/.eagle.oracle.kshrc':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle.oracle.kshrc.sh',
      before => File['/home/oracle/.vimrc'],
    }
    
    file { '/home/oracle/.vimrc':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle_vimrc',
      require => User['oracle'],
      before => File['/home/oracle/.exrc'],
    }
    
    file { '/home/oracle/.exrc':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle_exrc',
      require => User['oracle'],
      before => File['/home/grid/.vimrc'],
    }
    
    file { '/home/grid/.vimrc':
      owner => 'grid',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle_vimrc',
      require => User['oracle'],
      before => File['/home/grid/.exrc']
    }
    
    file { '/home/grid/.exrc':
      owner => 'grid',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle_exrc',
      require => User['oracle'],
      before => File['/etc/skel/.eagle.oracle.aliases'],
    }
    
    file { '/etc/skel/.eagle.oracle.aliases':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/eagle.aliases.txt',
      before => File['/home/grid/.aliases'],
    }
    
   file { '/home/grid/.aliases':
     owner => 'grid',
     group => 'oinstall',
     mode => '0775',
     source => 'puppet:///modules/eagle/oracle/eagle.aliases.txt',
     before => File['/home/oracle/.sid.ksh'],
    }
    
    file { '/home/oracle/.sid.ksh':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/sid.ksh',
      before => File['/usr/bin/setsid.sh'],
    }
    
    file { '/usr/bin/setsid.sh':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/setsid.sh',
      before => File['/usr/bin/showsid.sh'],
    }
    
    file { '/usr/bin/showsid.sh':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/showsid.sh',
      before => File['/home/oracle/.ssh'],
    }
    
    file { '/home/oracle/.ssh':
      ensure => 'directory',
      owner => 'oracle',
      group => 'oinstall',
      mode => '0700',
      require => User['oracle'],
      before => File['/home/oracle/.ssh/authorized_keys'],
    }
    
    file { '/home/oracle/.ssh/authorized_keys':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0600',
      source => 'puppet:///modules/eagle/oracle/oracle_authorized_keys',
      before => File['/home/oracle/.ssh/id_rsa.pub'],
    }
    
    file { '/home/oracle/.ssh/id_rsa.pub':,
      owner => 'oracle',
      group => 'oinstall',
      mode => '0600',
      source => 'puppet:///modules/eagle/oracle/oracle_id_rsa.pub',
      before => File['/home/oracle/.ssh/id_rsa'],
    }
    
    file { '/home/oracle/.ssh/id_rsa':
      owner => 'oracle',
      group => 'oinstall',
      mode => '0400',
      source => 'puppet:///modules/eagle/oracle/oracle_id_rsa',
      before  => File['/home/grid/.ssh'],
    }

    file { [ '/home/grid/.ssh' ]:
      ensure => 'directory',
      owner => 'grid',
      group => 'oinstall',
      mode => '0700',
      require => User['grid'],
      before => File['/home/grid/.ssh/authorized_keys'],
    }
    
    file { '/home/grid/.ssh/authorized_keys':
      owner => 'grid',
      group => 'oinstall',
      mode => '0600',
      source => 'puppet:///modules/eagle/oracle/grid_authorized_keys',
      before => File['/home/grid/.ssh/id_rsa.pub'],
    }
    
    file { '/home/grid/.ssh/id_rsa.pub':
      owner => 'grid',
      group => 'oinstall',
      mode => '0600',
      source => 'puppet:///modules/eagle/oracle/grid_id_rsa.pub',
      before => File['/home/grid/.ssh/id_rsa'],
    }

    file { '/home/grid/.ssh/id_rsa':
      owner => 'grid',
      group => 'oinstall',
      mode => '0400',
      source => 'puppet:///modules/eagle/oracle/grid_id_rsa',
      before => File['/var/log/oracle/'],
    }

    file { '/var/log/oracle/':
      ensure => 'directory',
      owner => 'oracle',
      group => 'oinstall',
      mode => '0774',
      require => User['oracle'],
      before => File['/home/oracle/CRONLOG/'],
    }
    
    file { '/home/oracle/CRONLOG/':
      ensure => 'directory',
      owner => 'oracle',
      group => 'oinstall',
      require => User['oracle'],
    }

    if ("${odirs}" != 'true') {

      # Setup of Oracle & Grid dirs - one time stuff

      # Puppet requires you to create each directory before the one beneath it
      # Do not use recurse. Plays havoc with the root.sh stuff that Oracle runs post-install.
      # Do not create the grid home dir. Create everything up to it. Owner / group stuff will
      # break the root.sh stuff that Oracle runs post-install. We no longer create as
      # much of the Oracle dirs as was needed for 11g. 
      
      exec { '/usr/local/bin/oracle_sshknownhosts.sh':
        require => File['/usr/local/bin/oracle_sshknownhosts.sh'],
        before  => File['/usr/local/bin/create_dirs.sh'],
      }
    
      file { '/usr/local/bin/create_dirs.sh':
        owner  => 'root',
        group  => 'root',
        mode   => '0775',
        source => 'puppet:///modules/eagle/oracle/create_dirs.sh',
        require => User['grid'],
        before => Exec["/usr/local/bin/create_dirs.sh /u01/app oracle:oinstall"],
      }
    
      exec { "/usr/local/bin/create_dirs.sh /u01/app oracle:oinstall":
        require => File['/usr/local/bin/create_dirs.sh'],
        before => Exec["/usr/local/bin/create_dirs.sh ${grid_base} grid:oinstall"],
      }

      exec { "/usr/local/bin/create_dirs.sh ${grid_base} grid:oinstall":
        require => File['/usr/local/bin/create_dirs.sh'],
        before => Exec["/usr/local/bin/create_dirs.sh ${grid_home} grid:oinstall"],
      }
      
      exec { "/usr/local/bin/create_dirs.sh ${grid_home} grid:oinstall":
        require => File['/usr/local/bin/create_dirs.sh'],
        before => File['/etc/facter/facts.d/odirs.json'],
      }
      
      file {'/etc/facter/facts.d/odirs.json':
        ensure  => 'present',
        require => Exec["/usr/local/bin/create_dirs.sh ${grid_home} grid:oinstall"],
        content => "{\"odirs\": true}\n",
      }
    }

    if ("${odatamount}" != 'true') {

      # Make sure the Isilon read-only share with the Oracle binaries is mounted

      file { '/media/oracle':
        ensure => directory,
        before => Mount['oracle_media'],
      }

      mount {'oracle_media': 
	device   => "${isilon_host}:/ifs/data/${isilon_root}/EAI/oracle",
	atboot   => yes, 
	fstype   => 'nfs',
	options  => 'ro,vers=3,hard,bg,nolock',
	name     => '/media/oracle',
	ensure   => mounted,
	remounts => false,
	pass     => '0',
	require  => File['/media/oracle'],
	before   => File['/etc/facter/facts.d/odata.json'],
      }
      
      file {'/etc/facter/facts.d/odata.json':
        ensure  => 'present',
        require => Mount['/media/oracle'],
        content => "{\"odatamount\": true}\n",
      }
    }
    
} #base class
