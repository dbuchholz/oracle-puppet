class eagle::oracle12c_installdb {

  $vcac = hiera('vcac')
  
  $sys_asm_password = 'Eagle01'
  $asm_monitor_password = 'Eagle01'
  $db_domain = 'eagleaccess.com'
  $grid_base = hiera('grid_base')
  $grid_home = hiera('grid_home')
  $oracle_base = hiera('oracle_base')
  $oracle_home = hiera('oracle_home')
  $ora_inventory_dir = hiera('ora_inventory')
  $db_name = inline_template('<%= @clientworkgroup.sub("-","") %>')

  if $vcac['UpgradeRegion'] == 'Upgrade' {
    $source_sid = $vcac['DatabaseSids']
  } else {
    $source_sid = $vcac['sourceSID']
  }

  $oracle_dbsize = $vcac['oracleDBSize']
  $db_port1 = $vcac['databasePort1']
  $db_port2 = $vcac['databasePort2']

if ("${odbsetup}" != 'true') {
    
    oradb::installdb {'eagledb':
      version                 => '12.1.0.2',
      file                    => 'V46095-01',
      database_type            => 'EE',
      ora_inventory_dir       => $ora_inventory_dir,
      oracle_base             => $oracle_base,
      oracle_home             => $oracle_home,
      ee_options_selection    => false,
      ee_optional_components  => 'oracle.rdbms.partitioning:12.1.0.2.0,oracle.oraolap:12.1.0.2.0,oracle.rdbms.dm:12.1.0.2.0,oracle.rdbms.dv:12.1.0.2.0,oracle.rdbms.lbac:12.1.0.2.0,oracle.rdbms.rat:12.1.0.2.0',
#      create_user             => false,
      bash_profile            => false,
      user                    => 'oracle',
      user_base_dir           => '/home',
      group                   => 'oinstall',
      group_install           => 'oinstall',
      group_oper              => 'oper',
      download_dir            => '/tmp',
      zip_extract             => true,
      puppet_download_mnt_point => '/media/oracle/12_1_0_2',
      remote_file             => false,
#      require                 => Oradb::Installasm['asm_vg_creation'],
      #      before		      => Oradb::Database['eagle_db']
      before => File['/etc/facter/facts.d/odbsetup.json'],
    }

    file {'/etc/facter/facts.d/odbsetup.json':
      ensure  => 'present',
      require => Oradb::Installdb['eagledb'],
      content => "{\"odbsetup\": true}\n",
    }
  }
} #end class eagle::oracle12c_installdb
