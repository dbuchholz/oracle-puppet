class eagle::linux {

  include eagle::cron
  include eagle::dns
  include eagle::ntp
  include eagle::directories
  include eagle::ldap
  include eagle::issue
  include eagle::motd
  include eagle::nrpe
  include eagle::packages_base
  include eagle::passwd
  include eagle::postfix
  include eagle::puppet_agent
  include eagle::root
  include eagle::rsyslog
  include eagle::satellite
  include eagle::services
  include eagle::sshd
  include eagle::scc
  include eagle::scripts
  include eagle::timezones
  include eagle::usersandgroups
  
  Class['eagle::dns'] ->
  Class['eagle::ntp'] ->
  Class['eagle::satellite'] ->
  Class['eagle::packages_base'] ->
  Class['eagle::usersandgroups'] ->
  Class['eagle::directories'] ->
  Class['eagle::services'] ->
  Class['eagle::ldap'] ->
  Class['eagle::postfix'] ->
  Class['eagle::nrpe'] ->
  Class['eagle::passwd'] ->
  Class['eagle::puppet_agent'] ->
  Class['eagle::rsyslog'] ->
  Class['eagle::scc'] ->
  Class['eagle::timezones']

}
