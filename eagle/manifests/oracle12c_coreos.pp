class eagle::oracle12c_coreos {

  $vcac = hiera('vcac')
  
  $oracle_dbsize = $vcac['oracleDBSize']

  #set size-based variables
  
  case $oracle_dbsize {
    'small':  {
      $nr_hugepages = '4096'
      $shmall = '2883584'
      $shmmax = '8589934592'
      $oracle_memlock_soft = '12580864'
      $oracle_memlock_hard = '12580864'
    }
    'medium': {
      $nr_hugepages = '6144'
      $shmall = '3145728'
      $shmmax = '12884901888'
      $oracle_memlock_soft = '25163776'
      $oracle_memlock_hard = '25163776'
    }
    'large':  {
      $nr_hugepages = '12288'
      $shmall = '6291456'
      $shmmax = '25769803776'
      $oracle_memlock_soft = '12580864'
      $oracle_memlock_hard = '12580864'
      $oracle_memlock_soft = '50329600'
      $oracle_memlock_hard = '50329600'
    }
    default:  {
      $nr_hugepages = '4096'
      $shmall = '2883584'
      $shmmax = '8589934592'
      $oracle_memlock_soft = '12580864'
      $oracle_memlock_hard = '12580864'
    }
  }

  $install = [ 'binutils.x86_64', 'compat-libstdc++-33.x86_64', 'glibc.x86_64','ksh.x86_64','libaio.x86_64',
               'libgcc.x86_64', 'libstdc++.x86_64', 'make.x86_64','compat-libcap1.x86_64', 'gcc.x86_64',
               'gcc-c++.x86_64','glibc-devel.x86_64','libaio-devel.x86_64','libstdc++-devel.x86_64',
               'sysstat.x86_64','unixODBC-devel','glibc.i686','libXext.x86_64','libXtst.x86_64','collectl','ncdu']
    
    sysctl { 'fs.aio-max-nr':                       ensure => 'present', permanent => 'yes', value => '1048576',}
    sysctl { 'fs.file-max':                         ensure => 'present', permanent => 'yes', value => '6815744',}
    sysctl { 'kernel.msgmnb':                       ensure => 'present', permanent => 'yes', value => '65536',}
    sysctl { 'kernel.msgmax':                       ensure => 'present', permanent => 'yes', value => '65536',}
    sysctl { 'kernel.sem':                          ensure => 'present', permanent => 'yes', value => '250 32000 100 128',}
    sysctl { 'kernel.shmall':                       ensure => 'present', permanent => 'yes', value => $shmall,}
    sysctl { 'kernel.shmmax':                       ensure => 'present', permanent => 'yes', value => $shmmax,}
    sysctl { 'kernel.shmmni':                       ensure => 'present', permanent => 'yes', value => '4096', }
    sysctl { 'net.core.netdev_max_backlog':         ensure => 'present', permanent => 'yes', value => '50000',}
    sysctl { 'net.core.optmem_max':                 ensure => 'present', permanent => 'yes', value => '40960',}
    sysctl { 'net.core.rmem_default':               ensure => 'present', permanent => 'yes', value => '262144', }
    sysctl { 'net.core.rmem_max':                   ensure => 'present', permanent => 'yes', value => '4194304', }
    sysctl { 'net.core.wmem_default':               ensure => 'present', permanent => 'yes', value => '262144',}
    sysctl { 'net.core.wmem_max':                   ensure => 'present', permanent => 'yes', value => '1048576',}
    sysctl { 'net.ipv4.conf.all.accept_redirects':  ensure => 'present', permanent => 'yes', value => '0',}
    sysctl { 'net.ipv4.conf.all.send_redirects':    ensure => 'present', permanent => 'yes', value => '0',}
    sysctl { 'net.ipv4.conf.default.rp_filter':     ensure => 'present', permanent => 'yes', value => '1',}
    sysctl { 'net.ipv4.ip_local_port_range':        ensure => 'present', permanent => 'yes', value => '9000 65500',}
    sysctl { 'net.ipv4.tcp_fin_timeout':            ensure => 'present', permanent => 'yes', value => '10',}
    sysctl { 'net.ipv4.tcp_keepalive_intvl':        ensure => 'present', permanent => 'yes', value => '30',}
    sysctl { 'net.ipv4.tcp_keepalive_probes':       ensure => 'present', permanent => 'yes', value => '5',}
    sysctl { 'net.ipv4.tcp_keepalive_time':         ensure => 'present', permanent => 'yes', value => '1800',}
    sysctl { 'net.ipv4.tcp_max_syn_backlog':        ensure => 'present', permanent => 'yes', value => '30000',}
    sysctl { 'net.ipv4.tcp_max_tw_buckets':         ensure => 'present', permanent => 'yes', value => '2000000',}
    sysctl { 'net.ipv4.tcp_rmem':                   ensure => 'present', permanent => 'yes', value => '4096 87380 16777216',}
    sysctl { 'net.ipv4.tcp_slow_start_after_idle':  ensure => 'present', permanent => 'yes', value => '0',}
    sysctl { 'net.ipv4.tcp_tw_reuse':               ensure => 'present', permanent => 'yes', value => '1',}
    sysctl { 'net.ipv4.tcp_wmem':                   ensure => 'present', permanent => 'yes', value => '4096 65536 16777216',}
    sysctl { 'net.ipv4.udp_rmem_min':               ensure => 'present', permanent => 'yes', value => '8192',}
    sysctl { 'net.ipv4.udp_wmem_min':               ensure => 'present', permanent => 'yes', value => '8192',}
    sysctl { 'vm.dirty_background_ratio':           ensure => 'present', permanent => 'yes', value => '3',}
    sysctl { 'vm.dirty_expire_centisecs':           ensure => 'present', permanent => 'yes', value => '500',}
    sysctl { 'vm.dirty_ratio':                      ensure => 'present', permanent => 'yes', value => '15',}
    sysctl { 'vm.dirty_writeback_centisecs':        ensure => 'present', permanent => 'yes', value => '100',}
    sysctl { 'vm.min_free_kbytes':                  ensure => 'present', permanent => 'yes', value => '51200',}
    sysctl { 'vm.nr_hugepages':                     ensure => 'present', permanent => 'yes', value => $nr_hugepages,}
    sysctl { 'vm.swappiness':                       ensure => 'present', permanent => 'yes', value => '0',}

    class { 'limits':
      config => {
        '*'       => { 'nofile'  => { soft => '2048'   , hard => '8192',   },},
        'oracle'  => { 'nofile'  => { soft => '65536'  , hard => '65536',  },
        'nproc'   => { soft => '2048'   , hard => '16384',  },
        'memlock' => { soft => $oracle_memlock_soft  , hard => $oracle_memlock_hard,  },
        'stack'   => { soft => '10240'  ,},},
        'grid'    => { 'nofile'  => { soft => '65536'  , hard => '65536',  },
        'nproc'   => { soft => '2048'   , hard => '16384',  },
        'stack'   => { soft => '10240'  ,},},
      },
      use_hiera => false,
    }
    
    package { $install:
      ensure  => present,
      require => File['/var/tmp/puppetsatrun'],
      before  => File['/usr/local/scripts/datadomain'],
    }

} # class eagle::oracle12c_coreos {
