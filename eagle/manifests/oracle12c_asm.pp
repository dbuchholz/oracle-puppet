class eagle::oracle12c_asm {

  $vcac = hiera('vcac')
  
  $sys_asm_password = 'Eagle01'
  $asm_monitor_password = 'Eagle01'
  $db_domain = 'eagleaccess.com'
  $grid_base = hiera('grid_base')
  $grid_home = hiera('grid_home')
  $oracle_base = hiera('oracle_base')
  $oracle_home = hiera('oracle_home')
  $ora_inventory_dir = hiera('ora_inventory')
  $db_name = inline_template('<%= @clientworkgroup.sub("-","") %>')

  if $vcac['UpgradeRegion'] == 'Upgrade' {
    $source_sid = $vcac['DatabaseSids']
  } else {
    $source_sid = $vcac['sourceSID']
  }

  $oracle_dbsize = $vcac['oracleDBSize']
  $db_port1 = $vcac['databasePort1']
  $db_port2 = $vcac['databasePort2']

  #set size-based variables
  
  case $oracle_dbsize {
    'small':  { $eagle_init_ora_source = 'eagle_init_ora12.small.erb'
      $asm_disk_rollup_source = 'asm-disk-rollup-small.sh' 
      $prepare_asm_source = 'prepare_asm-small.sh'
      $asm_data_disks  =  'ORCL:DATA01,ORCL:DATA02'
      $asm_redo_disks = 'ORCL:REDO01'
      $asm_fra_disks = 'ORCL:FRA01'
    }
    'medium': { $eagle_init_ora_source = 'eagle_init_ora.medium.erb' 
      $asm_disk_rollup_source = 'asm-disk-rollup-medium.sh'
      $prepare_asm_source = 'prepare_asm-medium.sh'
      $asm_data_disks  =  'ORCL:DATA01,ORCL:DATA02,ORCL:DATA03,ORCL:DATA04,ORCL:DATA05'
      $asm_redo_disks = 'ORCL:REDO01'
      $asm_fra_disks = 'ORCL:FRA01,ORCL:FRA02,ORCL:FRA03,ORCL:FRA04'
    }
    'large':  { $eagle_init_ora_source = 'eagle_init_ora.large.erb' 
      $asm_disk_rollup_source = 'asm-disk-rollup-large.sh'
      $prepare_asm_source = 'prepare_asm-large.sh'
      $asm_data_disks  =  'ORCL:DATA01,ORCL:DATA02,ORCL:DATA03,ORCL:DATA04,ORCL:DATA05'
      $asm_redo_disks = 'ORCL:REDO01'
      $asm_fra_disks = 'ORCL:FRA01,ORCL:FRA02,ORCL:FRA03,ORCL:FRA04'
    }
    default:  { $eagle_init_ora_source = 'eagle_init_ora12.small.erb' 
      $asm_disk_rollup_source = 'asm-disk-rollup-small.sh' 
      $prepare_asm_source = 'prepare_asm-small.sh'
      $asm_data_disks  =  'ORCL:DATA01,ORCL:DATA02'
      $asm_redo_disks = 'ORCL:REDO01'
      $asm_fra_disks = 'ORCL:FRA01'
    }
  }
  
  if ("${asmsetup}" != 'true') {

    Exec['/usr/local/bin/asm-disk-rollup.sh'] -> Oradb::Installasm['asm_vg_creation'] ->File['/etc/facter/facts.d/asmsetup.json']
      
    /*
    These scripts are the base disk partitioning and oracleasm scripts
    as well as the sysconfig/oracleasm that makes oracleasm configure
    run quietly.
    */
    
    file { '/usr/local/bin/partition_dev.sh':
      owner => 'root',
      group => 'root',
      mode => '0775',
      source => 'puppet:///modules/eagle/oracle/partition_dev.sh',
      before => File['/usr/local/bin/prepare_asm.sh'],
    }
    
    file { '/usr/local/bin/prepare_asm.sh':
      owner => 'root',
      group => 'root',
      mode => '0775',
      source => "puppet:///modules/eagle/oracle/${prepare_asm_source}",
      before => File['/etc/sysconfig/oracleasm-_dev_oracleasm'],
    }
    
    file { '/etc/sysconfig/oracleasm-_dev_oracleasm':
      ensure => 'present',
      owner => 'root',
      group => 'root',
      mode => '0664',
      source => 'puppet:///modules/eagle/oracle/sysconfig-oracleasm',
      before => File['/etc/sysconfig/oracleasm'],
    }
    
    file { '/etc/sysconfig/oracleasm':
      ensure => 'link',
      target => '/etc/sysconfig/oracleasm-_dev_oracleasm',
      replace => 'true',
      owner => 'root',
      group => 'root',
      mode => '0644',
      before => Exec['/usr/local/bin/asm-disk-rollup.sh'],
    }
    
    file { '/usr/local/bin/asm-disk-rollup.sh':
      owner => 'root',
      group => 'root',
      mode => '0775',
      source => "puppet:///modules/eagle/oracle/${asm_disk_rollup_source}",
      before => Exec['/usr/local/bin/asm-disk-rollup.sh'],
    }
    
    /*    

    This script will zero the disks with dd (should be empty anyway)
    Partition the disks with a single Linux partition (no filesystem)
    Run the oracleasm utility to ready them for ASM.

    */
  
    exec { '/usr/local/bin/asm-disk-rollup.sh':
      returns => [0,2],
#      require => Mount['/media/oracle'],
      before  => Oradb::Installasm['asm_vg_creation'],
    }
      
    oradb::installasm{ 'asm_vg_creation':
      version                   => '12.1.0.2',
      file                      => 'V46096-01',
      grid_type                 => 'CRS_SWONLY',
      grid_base                 => hiera('grid_base'),
      grid_home                 => hiera('grid_home'),
      ora_inventory_dir         => hiera('ora_inventory'),
      user_base_dir             => '/home',
      user                      => 'grid',
      group                     => 'oinstall',
      group_install             => 'oinstall',
      group_oper                => 'oinstall',
      group_asm                 => 'oinstall',
      sys_asm_password          => $sys_asm_password,
      asm_monitor_password      => $asm_monitor_password,
      asm_diskgroup             => 'DATA',
      data_disks                => $asm_data_disks,
      redo_disks                => $asm_redo_disks,
      fra_disks                 => $asm_fra_disks,
      disk_au_size              => 4,
      disk_discovery_string     => 'ORCL:*',
      disk_redundancy           => 'EXTERNAL',
      download_dir              => '/tmp',
      puppet_download_mnt_point => '/media/oracle/12_1_0_2',
      remote_file               => false,
      zip_extract               => true,
      require                   => Exec["/usr/local/bin/asm-disk-rollup.sh"],
      before                    => File['/etc/facter/facts.d/asmsetup.json'],
    }

    file {'/etc/facter/facts.d/asmsetup.json':
      ensure  => 'present',
      require => Oradb::Installasm['asm_vg_creation'],
      content => "{\"asmsetup\": true}\n",
    }
  }
} # end class eagle::oracle12c_asm {
